var fs = require('fs');
var prettyjson = require('prettyjson');
var jsforce = require('jsforce');

var ASSETS = {
    staticResource: 'StaticResource',
    visualforcePage: 'VisualForcePage' // TODO check this is correct for the visualforce page
};

/**
 * Setup login parameters for metadata-api with salesforceAPI.
 * @method SalesforceAPI
 * @param  {String} loginUrl      url for salesforce instance
 * @param  {String} username      salesforce user email
 * @param  {String} password      username password
 * @param  {String} securityToken created in saleforce for an individual instance
 */
var SalesforceAPI = function (loginUrl, username, password, securityToken) {
    this.username = username;
    this.password = password;
    this.securityToken = securityToken;
    this.connection = new jsforce.Connection();
    this.connection.loginUrl = loginUrl;
};

/**
 * metadata-api connection login
 * @method connectToSalesforceInstance
 * @return {Promise} connection promise returned
 */
SalesforceAPI.prototype.connectToSalesforceInstance = function connectToSalesforceInstance () {
    // password and securityToken are combined and passed as a parameter for the connection.
    var password = this.password + '' + this.securityToken;
    return this.connection.login(this.username, password);
};

/**
 * create salesforce static resource in connected instance
 * @method createStaticResource
 * @param  {Object}  metadata object containing the metadata for static resource
 * @return {Promise} return promise of success or error
 */
SalesforceAPI.prototype.createStaticResource = function(metadata) {
    return this.connection.metadata.create(ASSETS.staticResource, metadata);
};

/**
 * update existing salesforce static resource in connected instance
 * @method updateStaticResource
 * @param  {Object}  metadata object containing the metadata for static resource
 * @return {Promise} return promise of success or error
 */
SalesforceAPI.prototype.updateStaticResource = function (metadata) {
    return this.connection.metadata.update(ASSETS.staticResource, metadata);
};

module.exports = SalesforceAPI;
