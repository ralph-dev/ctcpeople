var Promise = require('promise');
var SalesforceAPI = require('./SalesforceAPI');
var MetaData = require('./MetaData');
var prettyjson = require('prettyjson');

var LOGIN = {};
var argv = require('minimist')(process.argv.slice(2));

if (argv.i && argv.s && argv.z && argv.a) {
    var staticResourceFileName = argv.s;
    var zipFile = argv.z;
    var action = argv.a; // update or create
    var salesforceInstance = argv.i;
} else {
    throw new Error('Please enter arguments: -a [action(create or update)] -z [zip file and path] -s [static resource name on salesforce] -i [ saleforce instance]');
}

setLoginDetails(salesforceInstance);

var salesforceAPI = new SalesforceAPI(LOGIN.URL, LOGIN.user, LOGIN.password, LOGIN.securityToken);
var metaData = new MetaData();

var connect = salesforceAPI.connectToSalesforceInstance();

connect.then(function(res) {
    return metaData.createStaticResourceMetadata(staticResourceFileName, zipFile);
}).then(function (staticResourceMetadata) {
    if (action === 'update') {
        salesforceAPI.updateStaticResource(staticResourceMetadata).then(function(res) {
            console.log(res);
        },
        function(err) {
            console.log(err);
        });
    } else if (action === 'create') {
        salesforceAPI.createStaticResource(staticResourceMetadata).then(function(res) {
            console.log(res);
        },
        function(err) {
            console.log(err);
        });
    }
});

function setLoginDetails(salesforceInstance) {

    if (salesforceInstance === 'uisb') {
        LOGIN.URL = process.env.SF_CPE_UISB_URL;
        console.log(process.env);
        console.log(process.env.SF_CPE_UISB_URL);
        LOGIN.user = process.env.SF_CPE_UISB_USER;
        LOGIN.password = process.env.SF_CPE_UISB_PASSWORD;
        LOGIN.securityToken = process.env.SF_CPE_UISB_TOKEN;
    } else if (salesforceInstance === 'test') {
        LOGIN.URL = process.env.SF_CPE_TEST_URL;
        LOGIN.user = process.env.SF_CPE_TEST_USER;
        LOGIN.password = process.env.SF_CPE_TEST_PASSWORD;
        LOGIN.securityToken = process.env.SF_CPE_TEST_TOKEN;
    } else if (salesforceInstance === 'release') {
        LOGIN.URL = process.env.SF_CPE_RELEASE_URL;
        LOGIN.user = process.env.SF_CPE_RELEASE_USER;
        LOGIN.password = process.env.SF_CPE_RELEASE_PASSWORD;
        LOGIN.securityToken = process.env.SF_CPE_RELEASE_TOKEN;
    } else if (salesforceInstance === 'jack') {
        LOGIN.URL = process.env.SF_JACK_URL;
        LOGIN.user = process.env.SF_JACK_USER;
        LOGIN.password = process.env.SF_JACK_PASSWORD;
        LOGIN.securityToken = process.env.SF_JACK_TOKEN;
    } else if (salesforceInstance === 'uidev') {
        LOGIN.URL = process.env.SF_UIDEV_URL;
        LOGIN.user = process.env.SF_UIDEV_USER;
        LOGIN.password = process.env.SF_UIDEV_PASSWORD;
        LOGIN.securityToken = process.env.SF_UIDEV_TOKEN;
    } 

    if (!LOGIN.URL || !LOGIN.user || !LOGIN.password || !LOGIN.securityToken) {
        console.log('Missing login details for', salesforceInstance);
        console.log(LOGIN.URL);
        console.log(LOGIN.user);
        console.log(LOGIN.password);
        console.log(LOGIN.securityToken);
    }

}
