# Build Tools using JSForce

=======

## Set up

Do this for each instance Test, UAT and UI Sandbox. Click on your name in the top right then My Settings
 [Follow this guide to get your security token emailed to you](https://help.salesforce.com/apex/HTViewHelpDoc?id=user_security_token.htm)

Set environment variables for Test, UAT and UI Sandbox:
```
# Salesforce UISB environment variables
export SF_CPE_UISB_URL=""
export SF_CPE_UISB_USER=""
export SF_CPE_UISB_PASSWORD=""
export SF_CPE_UISB_TOKEN=""

# Salesforce Test environment variables
export SF_CPE_TEST_URL=""
export SF_CPE_TEST_USER=""
export SF_CPE_TEST_PASSWORD=""
export SF_CPE_TEST_TOKEN=""

# Salesforce UAT environment variables
export SF_CPE_RELEASE_URL=""
export SF_CPE_RELEASE_USER=""
export SF_CPE_RELEASE_PASSWORD=""
export SF_CPE_RELEASE_TOKEN=""
```

Either in ~/.bash_profile or ~/.zshrc if your using ZSH.

## Example of using script

salesforce instance -i
action (create or update) -a
static resource name on salesforce -s
zip file and path -z

### Create
```
node buildTools/index.js -s ctcTestJSForceUpdate -a create -i uisb -z './buildTools/tests/test.zip'
```
### Update
```
node buildTools/index.js -s ctcTestJSForceUpdate -a update -i uisb -z './buildTools/tests/test.zip'
```

## API Documentation

- [JSForce](http://jsforce.github.io/jsforce/doc/)
- [metadata-api](https://jsforce.github.io/document/#metadata-api)

## Coding Standards

- [Interface Design Patterns](http://bites.goodeggs.com/posts/export-this/)

## Interface Design Pattern for modules

### Exports a Constructor

Define classes in JavaScript with constructor functions and create instances of classes with the new keyword.

```javascript
function Person(name) {
  this.name = name;
}

Person.prototype.greet = function() {
  return "Hi, I'm Jane.";
};

var person = new Person('Jane');
console.log(person.greet()); // prints: Hi, I'm Jane
```

For this pattern implement a class-per-file and export the constructor to make your project organisation clear and to make it easy for other developers to find the implementation of a class.

```javascript
var Person = require('./person');

var person = new Person('Jane');
The implementation might look like:

function Person(name) {
  this.name = name;
}

Person.prototype.greet = function() {
  return "Hi, I'm " + this.name;
};

module.exports = Person;
```
## Recommended plugins

### Atom
- [docblockr for automating JSDoc](https://atom.io/packages/docblockr)
- [node-debugger](https://atom.io/packages/node-debugger)

## TODO

- Validation check if zip file is larger then 2MB
- Move static resource from on instance to another
- Visualforce page create and update
- Update to ESLINT
