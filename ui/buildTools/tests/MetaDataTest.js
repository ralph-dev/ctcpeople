var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

var MetaData = require('../MetaData');

describe('MetaData', function () {

    var metaData;
    // created base64 string from test.zip
    var testZipBase64 = 'UEsDBBQACAAIAOt160gAAAAAAAAAAAAAAAAJABAAUkVBRE1FLm1kVVgMABYlg1fZJINX9QEUAG2QO0/DQAyA9/sVlrJQCInEiMRSIgYklIHCghiOOydxezlHZ6cFfj2XtkgMeLDlx+dXAeuZgocNcxCYhWIPj88PnByau5MYUxQFNOzmEaNaJY7GXMHbuez9YlCdbut6K93iVz3pMH9UxL+R2rOrVwsyolpv1V7biU6c/Av687C6+EusTpts2qY1lzAldihSYdxD5kEHzDYEPiw37G2YUSBwT/ElhTKfhinaEUuYrMiBky9B0M2J9AuUdxiXnjmVVcp1ikmAonJWHj+rreT8qw3kjx8AN6DbAXXwTRN0FBAoj7Opx+MqEW6e1pmw3ud/Nu29+QFQSwcIaWmHQPQAAABqAQAAUEsBAhUDFAAIAAgA63XrSGlph0D0AAAAagEAAAkADAAAAAAAAAAAQKSBAAAAAFJFQURNRS5tZFVYCAAWJYNX2SSDV1BLBQYAAAAAAQABAEMAAAA7AQAAAAA=';

    beforeEach(function(){
        metaData = new MetaData();
    });

    it('addZipfile should create content as a base64 string from a zip file', function () {
        metaData.addZipFile('./tests/test.zip');
        expect(metaData.content).to.equal(testZipBase64);
    });

    it('should create static resource metadata', function () {
        metaData.createStaticResourceMetadata('testStaticResourceName', './tests/test.zip');
        expect(metaData.content).to.equal(testZipBase64);
        expect(metaData.fullName).to.equal('testStaticResourceName');
        expect(metaData.cacheControl).to.equal('Private');
        expect(metaData.contentType).to.equal('application/zip');
    });

});
