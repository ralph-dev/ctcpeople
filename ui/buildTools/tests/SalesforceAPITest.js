var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

var SalesforceAPI = require('../SalesforceAPI');

describe('SalesforceAPI', function () {

    var salesforceAPI;

    var metadata = [{
        fullName: 'testStaticResourceName',
        // content: base64str,
        cacheControl: 'Private',
        contentType: 'application/zip'
    }];

    beforeEach(function(){
        salesforceAPI = new SalesforceAPI();
    });

    it('connectToSalesforceInstance() should return a promise', function () {
        salesforceAPI.connectToSalesforceInstance()
            .then(function (result) {
                expect(result)
                    .to.equal(true);
            });
    });

    it('createStaticResource() should return a promise', function () {

        salesforceAPI.createStaticResource(metadata)
            .then(function (result) {
                expect(result)
                    .to.equal(true);
            });
    });

    it('updateStaticResource() should return a promise', function () {
        salesforceAPI.updateStaticResource(metadata)
            .then(function (result) {
                expect(result)
                    .to.equal(true);
            });
    });

});
