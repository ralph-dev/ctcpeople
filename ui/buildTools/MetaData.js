var fs = require('fs');

var MetaData = function () {
    this.fullName = undefined;
    this.content = undefined;
    this.cacheControl = 'Private';
    this.contentType = undefined;
};

/**
 * encode zip file to base64 and then convert to string
 * @method base64Encode
 * @param  {String} file path of zip file
 * @return {String} base64 string value of zip file
 */
function base64Encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);

    // convert binary data to base64 encoded string using node Buffer
    return new Buffer(bitmap)
        .toString('base64');
}

/**
 * set content to base64 zip file string value
 * @method addZipFile
 * @param  {String} file path of zip file
 */
MetaData.prototype.addZipFile = function (file) {
    // convert image to base64 encoded string
    this.content = base64Encode(file);
};

/**
 * Set metadata values for static resource on salesforce
 * @method createStaticResourceMetadata
 * @param  {String} fullName of static resource
 * @param  {String} filePath of zip file
 */
MetaData.prototype.createStaticResourceMetadata = function (fullName, filePath) {
    this.addZipFile(filePath);
    this.fullName = fullName;
    this.cacheControl = 'Private';
    this.contentType = 'application/zip';

    return [{
        fullName: this.fullName,
        content: this.content,
        cacheControl: this.cacheControl,
        contentType: this.contentType
    }];
};

module.exports = MetaData;
