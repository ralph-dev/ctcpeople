var fs = require("fs"),
    path = require("path"),
    _ = require("lodash"),
    connect = require("connect"),
    concatFiles = require("gulp-concat"),
    cstatic = require("serve-static"),
    cindex = require("serve-index"),
    del = require("del"),
    bower = require("main-bower-files"),
    es = require("event-stream"),
    merge = require("merge-stream"),
    through = require("through2"),
    queue = require("streamqueue"),
    lazypipe = require("lazypipe"),
    gulp = require("gulp"),
    zip = require("gulp-zip"),
    Karma = require('karma').Server,
    plugins = require("gulp-load-plugins")(),
    cors = require('cors'),
    gutil = require('gulp-util'),
    livereload = require('gulp-livereload'), // live reload server
    runSequence = require('run-sequence'),
    exec = require('child_process').exec,
    argv = require('minimist')(process.argv.slice(2)),

    ngmod = {
        concat: require("ngmod-concat").create(),
        stylesheet: require("ngmod-stylesheet").create(),
        templateUrl: require("ngmod-templateurl").create(),
        filterdep: require("ngmod-filterdep")
    },
    target = plugins.util.env.target || process.env.NODE_ENV || "dev",
    config = {
        paths: {
            apps: "src/apps",
            components: "src/components"
        },
        uglify: target !== "dev",
        beautify: target !== "prod"
    },
    pipelines = {
        scripts: lazypipe()
            .pipe(plugins.jshint, ".jshintrc")
            .pipe(plugins.jshint.reporter, "jshint-stylish")
            .pipe(es.map, function(file, next) {
                return (file.jshint.success ? next(null, file) : next());
            })
            .pipe(plugins.ngAnnotate, {
                remove: false,
                add: true
            })
            .pipe(function() {
                return plugins.if(config.uglify, plugins.uglify({
                    output: {
                        beautify: config.beautify
                    }
                }));
            })
    };

// Add gulp-help wrapping
require("gulp-help")(gulp);

// store the target app name
var targetApp = null;


// Clean existing compiled sources
gulp.task("clean", "Wipe target destinations before build if they exist", function(callback) {
    return del(["build", "dist", "test"], callback);
});

// gulp.task("package", "Compile the pages into VisualForce XML and bundle any referenced static resources into a zip for upload", ["build"], function () {
//     //TODO: complete
// });

gulp.task("moveStatic", "move static assests to app build folder for access in files within Visualforce pages.", function() {

    return gulp.src("img/**", {
            cwd: "src/static",
            base: "src"
        })
        .pipe(gulp.dest("build/" + targetApp));

});

gulp.task("package", "Compile the pages into VisualForce XML and bundle any referenced static resources into a zip for upload", ["setTargetApp", "build"], function() {

    // TODO replace hard coded project to be dynamic
    // TODO exclude testing folders

    return gulp.src("**/*", {
            cwd: "build/" + targetApp,
            base: "build/" + targetApp
        })
        .pipe(zip(targetApp + '.zip'))
        .pipe(gulp.dest('dist'));
});

gulp.task('staticResource', "Create or update a new Salesforce static resource", ["package"], function() {
    var zipPath = 'dist/' + targetApp + '.zip';
    var instance = argv.i || 'uisb';
    var action = argv.a || 'update';

    var cmd = 'node buildTools/index.js -s ' + targetApp + ' -a ' + action + ' -i ' + instance +  ' -z ' + zipPath;

    exec(cmd, function(error, stdout, stderr) {
        // command output is in stdout
        gutil.log(gutil.colors.green.bold(stdout));
    });
});

gulp.task('gelinStaticResource', "Create or update a new Salesforce static resource", ["clean", "package"], function() {
    var zipPath = 'dist/' + targetApp + '.zip';
    var instance = argv.i || 'uisb';
    var action = argv.a || 'update';

    var cmd = 'node buildTools/index.js -s ' + targetApp + ' -a ' + action + ' -i ' + instance +  ' -z ' + zipPath;

    exec(cmd, function(error, stdout, stderr) {
        // command output is in stdout
        gutil.log(gutil.colors.green.bold(stdout));
    });
});

gulp.task("packageStatic", "Bundle static folder referenced static resources into a zip for upload", function() {

    // TODO replace hard coded resources to be dynamic

    // STATIC
    var staticFiles = [
        "ctcPropertyGlyphicon/**/*",
        "phoneNumberFormater.js"
    ];

    return gulp.src(staticFiles, {
            cwd: "src/static",
            base: "src"
        })
        .pipe(zip('CTCBaseFrameworkStatic.zip'))
        .pipe(gulp.dest('dist'));

});

gulp.task("packageLib", "Bundle lib folder referenced static resources into a zip for upload", function() {

    // libs files are packaged based on the bower.js files

    return gulp.src(bower(), {
            cwd: "src/lib/",
            base: "src"
        })
        .pipe(zip('CTCBaseFrameworkLib.zip'))
        .pipe(gulp.dest('dist'));
});


gulp.task("packageAll", "Bundle static, lib and ClientCard", ["package", "packageLib", "packageStatic"], function() {});

// Start a server to preview development changes in a rapidly iterative way
gulp.task("serve", "Start a local web server, build source and watch for changes to trigger rebuild", function serve() {

    var server = connect();

    server.use(cstatic("build/" + targetApp, {
        index: "app.html"
    }));

    server.use(cindex("build", {
        icons: true
    }));

    server.use(cstatic("src"));

    server.listen(9001);
});

// Start a server to preview development changes in a rapidly iterative way
gulp.task("serveData", "Start a local web server to host JSON data", function serve() {

    var server = connect();

    server.use(cors());

    // Mock answers to VisualForce remoting calls
    server.use(require("body-parser").json());

    server.use("/data", function(req, res) {

        res.setHeader("Content-Type", "serverlication/json");

        res.end(JSON.stringify((Array.isArray(req.body) ? req.body : [req.body]).map(function(req) {

            // gutil.log(req);

            // Set to write if the request is to write data.
            var write = (Array.isArray(req.ctx.csrf) ? req.ctx.csrf.shift() : false);

            // get path of JSON file
            var path = (write ? req.ctx.csrf.shift() : req.ctx.csrf);

            var file = JSON.parse(fs.readFileSync(path));

            var res = (write ? _.chain((Array.isArray(req.data) ? req.data : [req.data])).compact().flatten().map(function(req) {
                return (((req.toString() === req) && req.match(/^((\{.*})|(\[.*]))$/)) ? JSON.parse(req) : req);
            }).compact().map(function(res) {

                var target = (Array.isArray(file) ? _.chain(file).where({
                    Id: res.Id
                }).first().value() : file);

                return ((target === undefined) ? file[(res.Id = file.push(res)) - 1] : _.merge(target, res));
            }).value() : file);

            // gutil.log(res);

            if (write) {
                fs.writeFileSync(path, JSON.stringify(file, null, 4) + "\n");
            }

            return {
                action: req.action,
                method: req.method,
                ref: false,
                result: JSON.stringify(res),
                statusCode: 200,
                tid: req.tid,
                type: "rpc"
            };
        }), null, 2));
    });

    server.listen(9000);

});

gulp.task("develop", ["setTargetApp", "build", "serveData", "serve", "watch"]);

gulp.task('watch', function () {
    livereload.listen();

    gulp.watch('**/*.less' ,['build:less']);

    gulp.watch([
        'src/**/*.html',
        'src/**/*.js'
    ], ['build']).on('change', reload);

    var timer = null;

    // delay livereload to allow build to finish
    function reload() {
        var reload_args = arguments;

        var time = 2500;

        // Stop timeout function to run livereload if this function is ran within the last 1000ms
        if (timer) {
            clearTimeout(timer);
        }

        // Check if any gulp task is still running
        if (!gulp.isRunning) {
            timer = setTimeout(function() {
                gutil.log(gutil.colors.green.bold('livereload change'));
                livereload.changed.apply(null, reload_args);
            }, time);
        }
    }

});


/*
 * process the user input and set the target app to be compiled
 * */
gulp.task("setTargetApp", function() {
    // todo: update this task to allow setting multiple target apps
    // current implementation support only one app to be build at one time
    var appList = getAppNameListFromParams();
    targetApp = appList[0];
});


// Run the build process without watching and serving
// TODO: implement support for multiple app build
gulp.task("build", "Primary task for compiling source into built code", function(callback) {
    runSequence("clean" , "build:less", "build:common:html", "build:apps:html", "build:common:js", "build:apps:js", "build:index", "moveStatic","build:spec", callback);
});

// Compile less files in-place into CSS files
gulp.task("build:less", function() {
    // compile and concat LESS files for common components
    var commonStyle = gulp.src(['src/common/style/baseStyle.less','src/common/**/*.less'],{base:'src'})
        .pipe(plugins.less())
        .pipe(concatFiles('baseStyle.css'))
        .pipe(gulp.dest('build/' + targetApp+'/common/style/'));


    // compile and contact LESS style file for the app
    var appStyle = gulp.src('**/*.less',{ cwd: 'src/apps/' + targetApp})
        .pipe(plugins.less())
        .pipe(concatFiles('appStyle.css'))
        .pipe(gulp.dest('build/' + targetApp+'/'));

    return merge(commonStyle, appStyle).pipe(livereload());
});

gulp.task("build:apps:js", function () {
    return gulp.src(['app.js', '**/*.js', '!**/*.test.js',  '!**/*.spec.js'], {cwd: 'src/apps/' + targetApp})
        .pipe(concatFiles('app.js'))
        .pipe(gulp.dest('build/' + targetApp))
        .pipe(livereload());
});

gulp.task("build:common:js", function () {
    // concat and move common js files
    return gulp.src(['src/common/components.js','src/common/**/*.js',  '!**/*.test.js',  '!**/*.spec.js'], {base: 'src'})
        .pipe(concatFiles('common.js'))
        .pipe(gulp.dest('build/' + targetApp+'/common/'))
        .pipe(livereload());
});

gulp.task("build:apps:html", function () {
    // move application html template files
    return gulp.src(['**/*.html', '!**/*.js'], {cwd: 'src/apps/' + targetApp})
        .pipe(gulp.dest('build/' + targetApp))
        .pipe(livereload());
});

gulp.task("build:common:html", function () {
    // move common html template files
    return gulp.src(['src/common/**/*.html', '!**/*.js'], {base: 'src'})
        .pipe(gulp.dest('build/' + targetApp))
        .pipe(livereload());
});

gulp.task("build:index", function() {

    /* TODO: add angualr file sort "gulp-angular-filesort" to the inject process recommanded by https://www.npmjs.com/package/gulp-inject */
    var target = gulp.src('build/' + targetApp + '/app.html');
    // It's not necessary to read the files (will speed up things), we're only after their paths:
    var sources = gulp.src(['**/*.js', '**/*.css'], {
        read: false,
        cwd: 'build/' + targetApp
    });

    target.pipe(plugins.inject(sources))
        .pipe(plugins.inject(gulp.src(bower(), {
            read: false,
            cwd: "src"
        }), {
            addRootSlash: false,
            name: "bower",
            endtag: "<!-- endbower -->"
        }))
        .pipe(gulp.dest('build/' + targetApp));

    return target;
});

gulp.task("build:spec", function(){
    // move application html template files
    return gulp.src(['**/*.spec.js'], {cwd: 'src/apps/' + targetApp})
        .pipe(gulp.dest('test/' + targetApp))
        .pipe(livereload());
});

/**
 * Run jasmin unit test using Karma server
 */
gulp.task("test", function(done) {
    return new Karma({
        configFile: __dirname + "/karma.conf.js",
        singleRun: true
    }, function() { done(); }).start();

});

/**
 * process CL arguments looking for app names
 * @param args
 * @returns {Array} a list of app names to be compiled
 */
function getAppNameListFromParams() {
    var appList = processParams();
    console.log("appList", appList);
    var validAppName;
    if (appList.length > 0) {
        validAppName = checkIfAppDirectoryExist(appList);
        if (validAppName) {
            return appList;
        } else {
            invalidAppName();
        }
    }
    if (appList.length < 1) {
        invalidAppName();
    }

}

function processParams() {
    // use __dirname to get the path of the project add gulpfile.js then process args after
    var gulpFilePath = __dirname + "/gulpfile.js";
    var nodeArgs = process.argv;
    var gulpArgIndex = nodeArgs.indexOf(gulpFilePath);
    if (gulpArgIndex !== -1) {
        gulpArgIndex += 1; // add one index to get args only
    } else {
        gulpArgIndex = 2; // defult index of the gulpfile args from Cl
    }
    var gulpArgs = process.argv.slice(gulpArgIndex); // remove system
    var paramPrefix = "--";
    var appList = [];
    if (gulpArgs.length > 0) {
        appList = gulpArgs.filter(function(item) {
            return item.substr(0, 2) === paramPrefix;
        });
        if (appList.length > 0) {
            appList.forEach(function(appName, index, targetArray) {
                var tempArg = appName.substring(2);
                targetArray[index] = tempArg;
            });
        }
    }
    return appList;
}

function checkIfAppDirectoryExist(appNamelist) {
    var result = false;
    // check if the app name folders exist
    appNamelist.forEach(function(appName) {
        if (appName.length === 0) {
            return false;
        }
        var testPath = "src/apps/" + appName;
        var stat = fs.statSync(testPath);

        if (!stat.isDirectory()) {
            console.log("App Directory dose not exist");
            // console.log("App Directory dose not exist", error);
            // return error;
        }
        // run gulp task to compile the app

        if (stat.isDirectory()) {
            result = true;
        }

    });
    return result;

}

function invalidAppName() {
    console.log("Please specify a valid target App to be build");
    console.log('Type the app name to be build with "--" prefix.');
    process.exit(1);
}
