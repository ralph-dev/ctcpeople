#!/bin/sh

GIT_PATH=$PWD/$1

package=\
'<?xml version="1.0" encoding="UTF-8"?>
    <StaticResource xmlns="http://soap.sforce.com/2006/04/metadata">
    <cacheControl>Private</cacheControl>
    <contentType>application/zip</contentType>
</StaticResource>
'

apps=`ls "$GIT_PATH"/ui/src/apps`

cd "$GIT_PATH"/ui/

npm install

gulp clean
gulp packageStatic
cp dist/CTCBaseFrameworkStatic.zip "$GIT_PATH"/src/staticresources/ctcBaseFrameworkStatic.resource
echo cp dist/CTCBaseFrameworkStatic.zip "$GIT_PATH"/src/staticresources/ctcBaseFrameworkStatic.resource
echo $package > "$GIT_PATH"/src/staticresources/ctcBaseFrameworkStatic.resource-meta.xml

for app in $apps; do
	gulp clean
	gulp package --$app
	cp dist/$app.zip "$GIT_PATH"/src/staticresources/$app.resource
    echo cp dist/$app.zip "$GIT_PATH"/src/staticresources/$app.resource
	echo $package > "$GIT_PATH"/src/staticresources/$app.resource-meta.xml
done

