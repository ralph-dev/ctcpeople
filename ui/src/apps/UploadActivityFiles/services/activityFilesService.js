(function () {
    "use strict";
    function activityFilesService($q, RemoteModelObject) {
        var taskExtension = RemoteModelObject.create("Task",{});
        var helperExtension = RemoteModelObject.create("Helper",{});
        var USER_PERMISSION_DATA;
        var DOC_TYPE_PICKLIST;
        var SKILL_GROUPS;


        function ActivityDocObject(DocDetails) {
            var paramOb = DocDetails ? DocDetails : {};
            this.activityId = (paramOb.activityId !== undefined) ? paramOb.activityId : null;
            this.orgId = (paramOb.orgId !== undefined) ? paramOb.orgId : null;
            this.nameSpace = (paramOb.nameSpace !== undefined) ? paramOb.nameSpace : null;
            this.isEmailService = (paramOb.isEmailService !== undefined) ? paramOb.isEmailService : null;
            this.isSelected = (paramOb.isSelected !== undefined) ? paramOb.isSelected : false;
            this.enableSkillParsing = (paramOb.enableSkillParsing !== undefined) ? paramOb.enableSkillParsing : null;
            this.docType = (paramOb.docType !== undefined) ? paramOb.docType : "";
            this.attachmentName = (paramOb.attachmentName !== undefined) ? paramOb.attachmentName : null;
            this.attachmentId = (paramOb.attachmentId !== undefined) ? paramOb.attachmentId : null;
            this.amazonS3Folder = (paramOb.amazonS3Folder !== undefined) ? paramOb.amazonS3Folder : null;
            this.resourceId = (paramOb.resourceId !== undefined) ? paramOb.resourceId : null;
            this.resourceName = (paramOb.resourceName !== undefined) ? paramOb.resourceName : null;
            this.resourceType = (paramOb.resourceType !== undefined) ? paramOb.resourceType : null;
            this.skillGroupIds = (paramOb.skillGroupIds !== undefined) ? paramOb.skillGroupIds : [];
            this.whatMap = (paramOb.whatMap !== undefined) ? paramOb.whatMap : null;
            this.whoMap = (paramOb.whoMap !== undefined) ? paramOb.whoMap : null;
        }
//TODO: remove this function from the service and use the call in helperService
        function getUserPermission(){
            var deferred = $q.defer();
            var result;
            if(USER_PERMISSION_DATA){
                result = angular.copy(USER_PERMISSION_DATA);
                deferred.resolve(result);
            }else {
                helperExtension.actions.getUserPermission()
                    .then(function(data){
                        USER_PERMISSION_DATA = data;
                        result = angular.copy(USER_PERMISSION_DATA);
                        deferred.resolve(result);
                    });
            }

            return deferred.promise;
        }

        function getTaskInfo(taskId) {
            return taskExtension.actions.getTaskInfo(taskId)
                .then(function(data){
                    return data;
                });
        }

        /**
         * retrieve detailed list of documents attached to the activity
         * @param activityId
         * @returns {Array[ActivityDocObject]}
         */
        function getActivityDocs(activityId) {
            return taskExtension.actions.getActivityDocs(activityId)
                .then(function (data) {
                    var result = [];
                    if(Array.isArray(data) && data.length > 0){
                        result = data.map(function(item){return new ActivityDocObject(item);});
                    }
                    return result;
                });
        }

        function getSkillGroups() {
            var deferred = $q.defer();
            var result;
            if(SKILL_GROUPS && Array.isArray(SKILL_GROUPS)){
                result = angular.copy(SKILL_GROUPS);
                deferred.resolve(result);
            }else{
                taskExtension.actions.getSkillGroups()
                    .then(function(data){
                        SKILL_GROUPS = data;
                        result = angular.copy(SKILL_GROUPS);
                        deferred.resolve(result);
                    });
            }
            return deferred.promise;
        }

        function getDocTypePickList() {
            var deferred = $q.defer();
            var result;
            if (DOC_TYPE_PICKLIST && Array.isArray(DOC_TYPE_PICKLIST)) {
                result = angular.copy(DOC_TYPE_PICKLIST);
                deferred.resolve(result);
            } else {
                taskExtension.actions.getDocTypePickList()
                    .then(function (data) {
                        DOC_TYPE_PICKLIST = data;
                        result = angular.copy(DOC_TYPE_PICKLIST);
                        deferred.resolve(result);
                    });
            }
            return deferred.promise;
        }

        function isValidCloudStorageAccount() {
            return getUserPermission()
                .then(function(data){
                    return data.isValidCloudStorageAccount;
                });
        }

        function isEnabledSkillParsing(){
            return getUserPermission()
                .then(function(data){
                    return data.skillParsingEnabled;
                });
        }

        /**
         * upload files to the backend
         * @param fileList
         * @returns {Object} return an object contains upload status message
         */
        function uploadDocuments(fileList) {
            var deferred = $q.defer();

            if(!Array.isArray(fileList)){
                deferred.reject("no Documents selected");
            }
            var docsToUpload = fileList.map(function(item){
                return new ActivityDocObject(item);
            });

            var notValidDocType = docsToUpload.some(function(item){
                return item.docType === null;
            });

            if(notValidDocType){
                deferred.reject("document required file type to be set");
            }

            var notValidSelection = docsToUpload.some(function(item){
                return item.isSelected === null;
            });

            if(notValidSelection){
                deferred.reject("List contain un selected Document");
            }
            /* todo: add validation on return data to handle backend errors code*/
             taskExtension.actions.uploadDocuments(docsToUpload)
                .then(function (data) {
                    if(data.successStatus === true){
                        deferred.resolve(data);
                    }
                    if(data.successStatus === false){
                        deferred.reject(data);
                    }
                });
            return deferred.promise;
        }

        return {
            getTaskInfo: getTaskInfo,
            getDocTypePickList: getDocTypePickList,
            getSkillGroups: getSkillGroups,
            getActivityDocs: getActivityDocs,
            isValidCloudStorageAccount: isValidCloudStorageAccount,
            uploadDocuments: uploadDocuments,
            isEnabledSkillParsing : isEnabledSkillParsing
        };
    }

    angular.module("CTC.People.UploadActivityFiles").service("activityFilesService", ["$q", "RemoteModelObject",activityFilesService]);
}());
