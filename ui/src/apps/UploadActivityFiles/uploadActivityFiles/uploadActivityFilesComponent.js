(function () {
    "use strict";
    function uploadActivityFilesController(busyLoader, $window, $location, $timeout, activityFilesService,helperService) {
        var ctrl = this;
        ctrl.activityId;
        ctrl.skillGroups = [];
        ctrl.docTypePicklist = [];
        ctrl.activityDocList = [];
        ctrl.selectedSkills = [];
        ctrl.activityInfo = {};
        ctrl.checkBefore = false;
        ctrl.showSkillGroup = false;
        ctrl.isSkillParsingEnable = false;
        ctrl.notificationType = {
            info: {
                cssClass: 'alert-info',
                contents: '' +
                '<div class="notificationMessage">' +
                '<ul>' +
                '<li>For the document types of "Resume" or "CV", the parsing function will update the resume content, skills, employment and education history depending on your settings.</li>'+
                '<li>Selected documents will be uploaded against the related records with the chosen document type.</li>' +
                '<li>Once the selected documents are successfully uploaded, the documents in the activity record will be deleted.</li>'+
                '</ul>' +
                '</div>'
            },
            success: {
                cssClass: 'alert-success',
                contentsUrl: 'uploadActivityFiles/successNotification.html'
            },
            warning: {
                cssClass: 'alert-danger',
                contentsUrl: 'uploadActivityFiles/warningNotification.html'
            },
            noValidAccount: {
                cssClass: 'alert-danger',
                contentsUrl: 'uploadActivityFiles/noValidAccountNotification.html'
            },
            exceedFilesLimit: {
                cssClass: 'alert-danger',
                contentsUrl: 'uploadActivityFiles/exceedLimitNotification.html'
            },
            exceedResumeFilesLimit: {
                cssClass: 'alert-danger',
                contentsUrl: 'uploadActivityFiles/exceedResumeFilesNotification.html'
            },
            genericWarning:{
                cssClass: 'alert-danger',
                contentsUrl: 'uploadActivityFiles/genericNotification.html'
            }
        };
        ctrl.notification = ctrl.notificationType.info;

        /**
         * set notification to be showing for 30 sec then reset to default
         * @param type
         */
        function showNotification(type){
            ctrl.notification = type;
            $timeout(function(){
                ctrl.notification = ctrl.notificationType.info;
            },30000);
        }

        function initialise() {
            busyLoader.start();
            ctrl.activityId = $location.query("id");
            activityFilesService.getSkillGroups()
                .then(function (data) {
                    if (data.length > 0) {
                        ctrl.showSkillGroup = true;
                    }
                    ctrl.skillGroups = data.filter(function (skill) {
                        return skill.isSelected === false;
                    });
                    ctrl.selectedSkills = data.filter(function (skill) {
                        return skill.isSelected === true;
                    });
                    return activityFilesService.isEnabledSkillParsing();
                })
                .then(function (data){
                    ctrl.isSkillParsingEnable = data;
                    return activityFilesService.getDocTypePickList();
                })
                .then(function (picklist) {
                    ctrl.docTypePicklist = picklist.filter(function(option){
                        return option.length > 1;
                    });
                    return activityFilesService.getTaskInfo(ctrl.activityId);
                })
                .then(function (activity) {
                    ctrl.activityInfo = activity;
                    return activityFilesService.isValidCloudStorageAccount();
                })
                .then(function(validation){
                    if(validation === false){
                     ctrl.notification = ctrl.notificationType.noValidAccount;
                    }
                    ctrl.isValidS3Account = validation;
                    return activityFilesService.getActivityDocs(ctrl.activityId);
                })
                .then(function (docList) {
                    ctrl.activityDocList = docList;
                    return true;
                })
                .finally(function (data) {
                    busyLoader.end();
                });
        }

        function updateSelectAll() {
            ctrl.activityDocList.forEach(function (item) {
                item.isSelected = ctrl.selectAll;
            });
        }

        function uploadDocuments() {
            ctrl.checkBefore = true;
            var docList = ctrl.activityDocList.filter(function (item) {
                return item.isSelected === true;
            });
            var isInvalidDocType = docList.some(function (item) {
                return item.docType.trim().length < 1;
            });
            var resumeDocs = docList.filter(function(item){
                return item.docType.trim() === "Resume";
            });
            if(!ctrl.isValidS3Account){
                showNotification(ctrl.notificationType.noValidAccount);
                return;
            }
            if(docList.length > 10){
                showNotification(ctrl.notificationType.exceedFilesLimit);
                return;
            }
            if(docList.length < 1){
                showNotification(ctrl.notificationType.warning);
                return;
            }
            if (isInvalidDocType) {
                showNotification(ctrl.notificationType.warning);
                return;
            }
            if(resumeDocs.length > 1){
                showNotification(ctrl.notificationType.exceedResumeFilesLimit);
                return;
            }

            if (!isInvalidDocType) {
                busyLoader.start();
                // add skills to ech doc record
                if (ctrl.selectedSkills.length > 0) {
                    var skills = ctrl.selectedSkills.map(function (skill) {
                        return skill.skillGroupId;
                    });
                    docList.forEach(function (item) {
                        item.skillGroupIds = skills;
                    });
                }
                activityFilesService.uploadDocuments(docList)
                    .then(function (data) {
                        if(data.successStatus === true){
                            showNotification(ctrl.notificationType.success);
                            docList.forEach(function (item) {
                                var itemIndex = ctrl.activityDocList.indexOf(item);
                                if (itemIndex >= 0) {
                                    ctrl.activityDocList.splice(itemIndex, 1);
                                }
                            });
                        }
                        return true;
                    }, function(error){
                        var notification = angular.copy(ctrl.notificationType.genericWarning);
                        notification.message = '<p>'+error.msg +'</p>';
                        showNotification(notification);
                    })
                    .finally(function(data){
                        ctrl.checkBefore = false;
                        busyLoader.end();
                    });
            }
        }

        function exitForm() {
           // $window.location.href = "/" + ctrl.activityId;
        	helperService.locateToSObject($window,ctrl.activityId);
        }
        
        function naveToPage(objId){
        	return helperService.naveToPage($window,objId);
        }
        
       
        

        ctrl.$onInit = initialise;
        ctrl.updateSelectAll = updateSelectAll;
        ctrl.uploadDocuments = uploadDocuments;
        ctrl.exitForm = exitForm;
        ctrl.naveToPage = naveToPage;
    }

    var uploadActivityFiles = {
        templateUrl: "uploadActivityFiles/uploadActivityFiles.html",
        controller: uploadActivityFilesController
    };

    angular.module("CTC.People.UploadActivityFiles").component("uploadActivityFiles", uploadActivityFiles);
}());
