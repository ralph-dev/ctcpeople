angular.module("CTC.People.UploadActivityFiles", ["CTC.Components"]);

//angular.module("CTC.People.UploadActivityDocs")
//    .config(function ($stateProvider, $urlRouterProvider) {
//        "use strict";
//        $urlRouterProvider.otherwise('/');
//
//        $stateProvider
//            .state('search', {
//                url: '/',
//                template: '<upload',
//                controller: 'SearchFormController'
//            })
//            .state('search-result', {
//                url: '/search-result',
//                templateUrl: 'searchResult/searchResultView.html',
//                controller: 'SearchResultController'
//            });
//    });


// OPTIONAL on run option
angular.module("CTC.People.UploadActivityFiles")
    .run(function ($rootScope, urlPrefixService, $location) {
        urlPrefixService.setPrefix();
        $rootScope.returnActivityId = $location.query("id");
    });
