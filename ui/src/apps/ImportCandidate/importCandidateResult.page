<apex:page standardController="Contact" extensions="ImportCandidateResultController" action="{!init}" sidebar="false"  standardStylesheets="false" showHeader="false">
    <head>
        <!-- bootstrap libs -->
        <apex:styleSheet value="{!URLFOR($Resource.ctcBaseFrameworkLib, 'lib/bootstrap/dist/css/bootstrap.css')}"></apex:styleSheet>
        <apex:styleSheet value="{!URLFOR($Resource.ctcBaseFrameworkStatic, 'static/ctcPropertyGlyphicon/style.css')}"></apex:styleSheet>
        <!--- end lib css -->
        <apex:styleSheet value="{!URLFOR($Resource.ImportCandidate, 'common/style/baseStyle.css')}"></apex:styleSheet>
        <apex:styleSheet value="{!URLFOR($Resource.ImportCandidate, 'appStyle.css')}"></apex:styleSheet>
        <apex:includeScript value="{!URLFOR($Resource.ctcBaseFrameworkLib, 'lib/jquery/dist/jquery.js')}"></apex:includeScript>
        <apex:includeScript value="{!URLFOR($Resource.ctcBaseFrameworkLib, 'lib/jquery-ui/jquery-ui.js')}"></apex:includeScript>
        <apex:includeScript value="{!URLFOR($Resource.ctcBaseFrameworkLib, 'lib/bootstrap/dist/js/bootstrap.js')}"></apex:includeScript>
    </head>

    <body  class="ctc-application" >
    <ctc-header>
        <nav class="navbar navbar-dark bg-primary navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand no-padding-left no-padding-top" href="#">
                        <apex:image value="{!URLFOR($Resource.ImportCandidate,'static/img/CTC_logos-People_white.png')}" />
                    </a>
                </div>
                <div class="navbar-right">
                    <h4 class="navbar-text">Import Candidate</h4>
                    <p class="navbar-brand no-margin-bottom">
                        <a href="/" target="_self" class="glyphicon glyphicon-remove ctc-action-btn no-padding-top" style="color:#ffffff"></a>
                    </p>
                </div>
            </div>
        </nav>
    </ctc-header>
    <div class="container">

        <div class="row">
            <div class="panel panel-default importResult">
                <div class="panel-heading clearfix">
                    <div class="panel-title">
                        <span class="ctc-icon-client-manage ctc-md-icon vertical-align-middle"></span>
                        <span class="vertical-align-middle">Import Candidate Result</span>
                    </div>
                </div>
                <div class="panel-body">
                    <apex:form rendered="{!noError}" styleClass="notificationSection alert alert-success">
                        <div class="notificationMessage">
                            <apex:outputText value="<h5><b>Import Successful.</b></h5>" escape="false"/>
                        </div>
                    </apex:form>

                    <apex:form rendered="{!usrError}" styleClass="notificationSection alert alert-warning">
                        <div class="notificationMessage">
                            <apex:outputText value="<h5><b>Warning!</b></h5> <p>This candidate already exists in CTC People.</p>" escape="false"/>
                        </div>
                    </apex:form>

                    <apex:form rendered="{!s3Error}" styleClass="notificationSection alert alert-danger">
                        <div class="notificationMessage">
                            <apex:outputText value="<h5><b>Error!</b></h5> <p>Resume failed to upload as there is an issue with Amozon S3.</p> <p>Please contact support@clicktocloud.com.</p>" escape="false"/>
                        </div>
                    </apex:form>

                    <apex:form rendered="{!noEmailError}" styleClass="notificationSection alert alert-danger">
                        <div class="notificationMessage">
                            <apex:outputText value="<h5><b>Error!</b></h5> <p>The Resume was unable to parse as it could not identify a valid email address.</p>" escape="false"/>
                        </div>
                    </apex:form>

                    <apex:form rendered="{!parserError}" styleClass="notificationSection alert alert-success">
                        <div class="notificationMessage">
                            <apex:outputText value="<h5><b>Import Successful.</b></h5>" escape="false"/>
                        </div>
                    </apex:form>

                    <apex:form rendered="{!sfError}" styleClass="notificationSection alert alert-danger">
                        <div class="notificationMessage">
                            <apex:outputText value="<h5><b>Error!</b></h5><p>Failed to insert the candidate due to an authentication error.</p> <p>Please contact support@clicktocloud.com.</p>" escape="false"/>
                        </div>
                    </apex:form>

                    <apex:form rendered="{!otherError}" styleClass="notificationSection alert alert-danger">
                        <div class="notificationMessage">
                            <apex:outputText value="<h5><b>Error!</b></h5> <p>The resume failed to import.</p>" escape="false"/>
                        </div>
                    </apex:form>

                    <apex:form rendered="{!maintenance}">
                        <div class="notificationMessage">
                            <apex:outputText value="<h5><b>Error!</b></h5> <p>The Import Candidate function is currently down for maintenance.</p> <p>We are currently working on this. Please check periodically. Thank you for your patience.</p>" escape="false"/>
                        </div>
                    </apex:form>
                </div>
                <div class="panel-footer ">
                    <div class="center-block">
                        <apex:form rendered="{!Not(maintenance)}" styleClass="text-center">
                            <a href="#" class="btn btn-default" >Exit</a>
                            <apex:commandLink action="{!importCandidateAgain}" value="Import Another Candidate" styleClass="btn btn-success"/>
                            <apex:commandLink action="{!checkCandidatePage}" value="View Candidate Details" styleClass="btn btn-primary"  rendered="{!noError}"/>
                            <apex:commandLink action="{!checkCandidatePage}" value="View Candidate Details" styleClass="btn btn-primary"  rendered="{!usrError}"/>
                            <apex:commandLink action="{!checkCandidatePage}" value="View Candidate Details" styleClass="btn btn-primary"  rendered="{!parserError}"/>
                            <apex:commandLink action="{!checkCandidatePage}" value="Create Manually" styleClass="btn btn-primary"  rendered="{!otherError}"/>
                            <apex:commandLink action="{!checkCandidatePage}" value="Create Manually" styleClass="btn btn-primary"  rendered="{!noEmailError}"/>
                            <apex:commandLink action="{!checkCandidatePage}" value="Create Manually" styleClass="btn btn-primary"  rendered="{!sfError}"/>
                            <apex:commandLink action="{!checkCandidatePage}" value="Create Manually" styleClass="btn btn-primary"  rendered="{!s3Error}"/>
                        </apex:form>
                        <apex:form rendered="{!maintenance}">
                            <apex:commandLink action="{!checkCandidatePage}" value="Create Manually" styleClass="btn btn-primary" />
                        </apex:form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </body>

</apex:page>
