(function () {
    function importCandidateController(busyLoader, $timeout, $window, $document, $sce, candidateService, helperService, salesForceDataService, urlPrefixService) {
        var ctrl = this;
        ctrl.sfData ={}
        ctrl.resumeFile;
        var urlPrefix;
        ctrl.userId;
        ctrl.skillGroups = [];
        ctrl.selectedSkills = [];
        ctrl.selectedSkillGroups = "";
        ctrl.uploadFiles = [];
        ctrl.checkBefore = false;
        ctrl.showSkillGroup = false;
        ctrl.isSkillParsingEnabled =false;
        ctrl.supportedFileType = ["doc","docx","pdf", "rtf", "txt"];
        ctrl.notificationType = {
            info: {
                cssClass: 'alert-info',
                contents: 'importCandidate/infoNotification.html'
            },
            success: {
                cssClass: 'alert-success',
                contents: 'importCandidate/successNotification.html'
            },
            invalidFormatWarning: {
                cssClass: 'alert-danger',
                contents: 'importCandidate/invalidFormatNotification.html'
            },
            userExist: {
                cssClass: 'alert-danger',
                contents: 'importCandidate/userExistNotification.html'
            }
        };
        ctrl.notification = ctrl.notificationType.info;

        function onInit() {
            ctrl.sfData = salesForceDataService.getData();

            ctrl.formUrl = $sce.trustAsResourceUrl(ctrl.sfData.serviceEndPoint + '/DaxtraParsing/ImportMultiCandidates');
            //console.log(sf);
            busyLoader.start();
            helperService.isSkillParsingEnabled()
                .then(function(permission){
                    ctrl.isSkillParsingEnabled = permission;

                    return candidateService.getSkillGroups();
                })
                .then(function(data){
                    if(data.length > 0){
                        ctrl.showSkillGroup = true;
                    }
                    ctrl.skillGroups = data.filter(function(skill){
                        return skill.isSelected === false;
                    });
                    ctrl.selectedSkills = data.filter(function(skill){
                        return skill.isSelected === true;
                    });
                    if (ctrl.selectedSkills.length > 0 &&  ctrl.isSkillParsingEnabled) {
                        ctrl.selectedSkillGroups = ctrl.selectedSkills.map(function (skill) {
                            return skill.skillGroupId;
                        }).join(":");
                        console.log(ctrl.selectedSkillGroups);
                    }
                })
                .finally(function(data){
                    busyLoader.end();
                });
        }

        function importCandidateDetails(importCandidate) {
            busyLoader.start();
            // get selected skills
            if (ctrl.selectedSkills.length > 0 &&  ctrl.isSkillParsingEnabled) {
                ctrl.selectedSkillGroups = ctrl.selectedSkills.map(function (skill) {
                    return skill.skillGroupId;
                }).join(":");
            }

            // $timeout(function(){
            //     document.getElementById('importCandidate').submit();
            // });

            candidateService.importCandidateDetails(ctrl.uploadFiles[0], ctrl.selectedSkillGroups)
                .then(function (data) {
                    //$window.open(data.data.redirectTo, "_self");
                	helperService.openWindow($window, data.data.redirectTo,"_self");
                    busyLoader.end();
                });

        }


        function exitForm() {
            //$window.location.href = "/";
        	helperService.locateTo($window,"/");
        }

        function onFileUploaded(files){
           // Array.prototype.push.apply(ctrl.uploadFiles , files);
            ctrl.uploadFiles = files;
            console.log("files", files, ctrl.uploadFiles);
        }

        /******* scope mapping **********/
        ctrl.$onInit = onInit;
        ctrl.importCandidateDetails = importCandidateDetails;
        ctrl.exitForm = exitForm;
        ctrl.onFileUploaded = onFileUploaded;
    }

    var importCandidate = {
        templateUrl: "importCandidate/importCandidate.html",
        controller: importCandidateController
    };
    angular.module("CTC.People.ImportCandidate").component("importCandidate", importCandidate);
}());
