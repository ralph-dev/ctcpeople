angular.module("CTC.People.sealsForceData", []);
angular.module("CTC.People.ImportCandidate", ["CTC.Components", "CTC.People.sealsForceData"]);


// OPTIONAL on run option
angular.module("CTC.People.ImportCandidate")
    .run(function ($rootScope, urlPrefixService, $location) {
        urlPrefixService.setPrefix();
        $rootScope.returnUrl = $location.query("id");
    });
