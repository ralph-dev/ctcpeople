(function () {
    "use strict";
    function candidateService($q, $http, RemoteModelObject, salesForceDataService) {
        var taskExtension = RemoteModelObject.create("Task", {});
        var SKILL_GROUPS;

        function getSkillGroups() {
            var deferred = $q.defer();
            var result;
            if (SKILL_GROUPS && Array.isArray(SKILL_GROUPS)) {
                result = angular.copy(SKILL_GROUPS);
                deferred.resolve(result);
            } else {
                taskExtension.actions.getSkillGroups()
                    .then(function (data) {
                        SKILL_GROUPS = data;
                        result = angular.copy(SKILL_GROUPS);
                        deferred.resolve(result);
                    });
            }
            return deferred.promise;
        }

         /***
         * iterate over the list and create a new DocumentObject
         * check that only one or 0 document with type resume
         * check all documents contain type
         * create a FormData and append the values
         * submit the form using post $http request to endpoint
         * return response
         * @param documentObject
         * @returns {*}
         */
        function importCandidateDetails(documentObject, selectedSkills) {
            var deferred = $q.defer();
            var docForm = new FormData();
            var sf_data = salesForceDataService.getData();
            /*** append org required field ***/
            docForm.append("orgId", sf_data.orgId);
            docForm.append("bucketName", sf_data.bucketName);
            docForm.append("userId",sf_data.userId);
            docForm.append("urlprefix",sf_data.urlprefix);
            /**** append document fields *****/
            docForm.append("skillGroups", selectedSkills);
            docForm.append("file", documentObject);
            docForm.append("isReturnJSONObject",true);

            var uploadUrl = sf_data.serviceEndPoint + "/DaxtraParsing/ImportMultiCandidates";
            $http({
                method: 'POST',
                url: uploadUrl,
                headers: {'Content-Type': undefined, 'Accept': 'application/json'},
                data: docForm,
                transformRequest: function (data, headersGetterFunction) {
                    return data;
                }
            }).then(function onSuccess(data) {
                deferred.resolve(data);
            }, function onError(error) {
                // process error code
                deferred.reject(error);
            });

            return deferred.promise;
        }

        return {
            getSkillGroups: getSkillGroups,
            importCandidateDetails: importCandidateDetails
        }

    }

    angular.module("CTC.People.ImportCandidate").service("candidateService", ["$q", "$http", "RemoteModelObject", "salesForceDataService", candidateService]);
}());
