(function(){
    "use strict";
    function documentObjectFactory(){
        /**
         * Supported Salesforce fields type
         * @type {{BOOLEAN: string, DATE: string, DATETIME: string, REFERENCE: string, MULTIPICKLIST: string, PICKLIST: string, DOUBLE: string, NUMBER: string, INTEGER: string, DECIMAL: string, CURRENCY: string, PERCENT: string}}
         */
        var FIELD_TYPE_ENUM = {
            BOOLEAN: "BOOLEAN",
            DATE: "DATE",
            DATETIME: "DATETIME",
            REFERENCE: "REFERENCE",
            MULTIPICKLIST: "MULTIPICKLIST",
            PICKLIST : "PICKLIST",
            DOUBLE: "DOUBLE",
            NUMBER: "NUMBER",
            INTEGER: "INTEGER",
            DECIMAL: "DECIMAL",
            CURRENCY: "CURRENCY",
            PERCENT : "PERCENT",
            TEXTAREA: "TEXTAREA"
        };

        function fieldTypeEnum(){
            return angular.copy(FIELD_TYPE_ENUM);
        }

        /**
         * Document object
         * @property additionalFields the property contain a list of additionalFields
         * e.g:
         *      {
         *      "dbRequired": false,
         *      "fieldPath": "PeopleCloud1__Account__c",
         *      "label": "Account",
         *      "required": false,
         *      "type": "reference",
         *      "typeApex": "REFERENCE"
         *      }
         * @param file ref system file from uploader
         * @constructor
         */
        function DocumentObject(file) {
            this.fileData = file;
            this.documentType = "";
            this.additionalFields = [];
            this.skillGroup = null;
            this.uploadStatus={
                complete:false,
                success:null,
                message:null
            };
        }


        return{
            FieldTypeEnum: fieldTypeEnum,
            DocumentObject:DocumentObject
        }
    }

    angular.module("CTC.People.UploadDocuments").service("documentObjectFactory",documentObjectFactory);
}());
