(function () {
    "use strict";
    function uploadDocumentsService($http, $q, $filter, $location, RemoteModelObject, salesForceDataService) {
        var taskExtension = RemoteModelObject.create("Task", {});
        var ResumeAndFilesExtension = RemoteModelObject.create("ResumeAndFiles", {});
        var helperExtension = RemoteModelObject.create("Helper",{});
        var SKILL_GROUPS;
        var UPLOAD_RESULT_ENUM = {
            success: {
                code: "00000",
                message: "Documents will upload soon, please check the related records when the process is complete."
            },
            unexpected_error: {code: "XXXXX", message: "Unable to process the request. Please try again later."},
            session_expired: {
                code: "10006",
                message: "Unable to process the request due to session expiry. Please reload the page and try again later. "
            }
        };

        function getSkillGroups() {
            var deferred = $q.defer();
            var result;
            if (SKILL_GROUPS && Array.isArray(SKILL_GROUPS)) {
                result = angular.copy(SKILL_GROUPS);
                deferred.resolve(result);
            } else {
                taskExtension.actions.getSkillGroups()
                    .then(function (data) {
                        SKILL_GROUPS = data;
                        result = angular.copy(SKILL_GROUPS);
                        deferred.resolve(result);
                    });
            }
            return deferred.promise;
        }

        function getPicklistValues(item){
            var dataItem = item;
            var deferred = $q.defer();
           return helperExtension.actions.getPicklistMapBySObjectAndFieldName(item.ObjectAPIName, item.data.fieldPath)
                .then(function(data){
                    dataItem.options = data;
                   // deferred.resolve(dataItem);
                    return dataItem;
                });
            //return deferred.promise;
        }

        function getDocumentType() {
            var srcObj = $location.query("srcObj");
            var deferred = $q.defer();
            ResumeAndFilesExtension.actions.getOptionList(srcObj)
                .then(function (data) {
                    var result = {};
                    // to manage data schema send as string to frontend
                    if (typeof data === "string") {
                        result = JSON.parse(data);
                    } else {
                        result = data;
                    }
                    return result;
                })
                .then(function (docType) {
                    var list =[];
                    //iterate over the list of document types to find any picklist fields
                    for( var prop in docType){
                        if(docType[prop].length > 0){
                            docType[prop].forEach(function(item, index){
                                if(item.type =="picklist" || item.type=="multipicklist"){
                                    var picklistOption = {};
                                    picklistOption.data = item;
                                    picklistOption.dataIndex = index;
                                    picklistOption.docType = prop;
                                    picklistOption.ObjectAPIName = "Web_document__c";

                                    list.push(getPicklistValues(picklistOption));
                                }
                            });
                        }
                    }
                    // if picklist fields exist retrieve them and reassign them to docType
                    if(list.length > 0){
                       $q.all(list)
                           .then(function(data){
                               data.forEach(function(option){
                                   docType[option.docType][option.dataIndex].options = option.options;
                               });
                               deferred.resolve(docType);
                           })
                    }else {
                       deferred.resolve(docType);
                    }

                });
            return deferred.promise;
        }


        /***
         * iterate over the list and create a new DocumentObject
         * check that only one or 0 document with type resume
         * check all documents contain type
         * create a FormData and append the values
         * submit the form using post $http request to endpoint
         * return response
         * @param documentObject
         * @returns {*}
         */
        function uploadDocuments(documentObject) {
            var deferred = $q.defer();
            var docForm = new FormData();
            var sf_data = salesForceDataService.getData();
            /*** append org required field ***/
            docForm.append("orgId", sf_data.orgId);
            docForm.append("srcId", sf_data.srcId);
            docForm.append("lookupField", sf_data.lookupField);
            docForm.append("bucketName", sf_data.bucketName);
            docForm.append("nsPrefix", sf_data.nsPrefix);
            //docForm.append("sessionId", sf_data.sessionId);
            docForm.append("serverUrl", sf_data.serverUrl);
            docForm.append("encodedUrl", sf_data.encodedUrl);
            docForm.append("isReturnJSONObject", true);
            docForm.append("userId",sf_data.userId);
            /**** append document fields *****/
            docForm.append("file", documentObject.fileData);
            docForm.append("picklist:file:Document_Type__c", documentObject.documentType);
            if (documentObject.additionalFields.length > 0) {
                /**
                 * add each field to the list of submitted form data
                 * format each field name as "type:file:fieldName"
                 * format date as "dd/MM/yyyy"
                 */
                documentObject.additionalFields.forEach(function (item) {
                    var fieldName = item.type+":file:"+item.fieldPath;
                    var fieldValue = item.values;
                    if(item.type === "date" || item.type === "datetime"){
                        fieldValue =  $filter('date')(item.values, "dd/MM/yyyy");
                    }
                    if(item.type == "multipicklist" || item.type == "picklist"){
                        if(Array.isArray(item.values)){
                            fieldValue = item.values.join(';');
                        }
                    }
                    docForm.append(fieldName, fieldValue);
                });
            }
            if (documentObject.documentType == "Resume") {
                // add skill group input
                var selectedSkills = [];
                if (documentObject.resumeSkillGroups.selectedSkillGroups.length > 0) {
                    selectedSkills = documentObject.resumeSkillGroups.selectedSkillGroups.map(function (skill) {
                        return skill.skillGroupId;
                    }).join(":");
                }
                docForm.append("skillGroups", selectedSkills);
            }
            var uploadUrl = sf_data.serviceEndPoint + "/EnhancedPeopleCloud/FileUploadAction";
            $http({
                method: 'POST',
                url: uploadUrl,
                headers: {'Content-Type': undefined},
                data: docForm,
                transformRequest: function (data, headersGetterFunction) {
                    return data;
                }
            }).then(function onSuccess(data) {
                // process error code
                 switch(data.data.errorCode){
                     case UPLOAD_RESULT_ENUM.success.code:
                         deferred.resolve(UPLOAD_RESULT_ENUM.success.message);
                         break;
                     case UPLOAD_RESULT_ENUM.session_expired.code:
                         deferred.reject( UPLOAD_RESULT_ENUM.session_expired.message);
                         break;
                     case UPLOAD_RESULT_ENUM.unexpected_error.code:
                         deferred.reject( UPLOAD_RESULT_ENUM.unexpected_error.message);
                         break;
                 }
            }, function onError(error) {
                // process error code
                deferred.reject(error);
            });

            return deferred.promise;
        }


        return {
            getSkillGroups: getSkillGroups,
            getDocumentType: getDocumentType,
            uploadDocuments: uploadDocuments
        }
    }

    angular.module("CTC.People.UploadDocuments").service("uploadDocumentsService", ["$http", "$q", "$filter", "$location", "RemoteModelObject", "salesForceDataService", uploadDocumentsService]);
}());
