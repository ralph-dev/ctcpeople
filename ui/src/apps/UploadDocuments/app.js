(function(){
    "use strict";
    angular.module("CTC.People.UploadDocuments", ["CTC.Components"]);

//angular.module("CTC.People.UploadActivityDocs")
//    .config(function ($stateProvider, $urlRouterProvider) {
//        "use strict";
//        $urlRouterProvider.otherwise('/');
//
//        $stateProvider
//            .state('upload', {
//                url: '/',
//                template: '<upload-documents></upload-documents>',
//            });
//    });


// OPTIONAL on run option
    angular.module("CTC.People.UploadDocuments")
        .run(function ($rootScope, urlPrefixService, $location) {
            $rootScope.srcId = $location.query("srcId");
           // urlPrefixService.setPrefix();
            //  $rootScope.returnActivityId = $location.query("id");
        });

}());
