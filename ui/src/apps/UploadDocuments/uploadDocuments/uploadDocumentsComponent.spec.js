describe('uploadDocumentsComponents', function(){

    var $componentController, UploadDocumentCtrl;
    var busyLoader, $q, $timeout, helperService, uploadDocumentsService, documentObjectFactory;
    // inject main module
  //  beforeEach(angular.mock.module('ui.router'));
    beforeEach(angular.mock.module('CTC.People.UploadDocuments'));
    // Inject  services
    beforeEach(inject(function(_$componentController_,_$rootScope_, _$window_, _$location_, _$q_ , _$timeout_, _busyLoader_, _helperService_,_uploadDocumentsService_, _documentObjectFactory_ ) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        $timeout = _$timeout_;
        $window = _$window_;
        $location = _$location_;
        busyLoader = _busyLoader_ ;
        helperService = _helperService_;
        uploadDocumentsService = _uploadDocumentsService_;
        documentObjectFactory = _documentObjectFactory_;
        UploadDocumentCtrl = $componentController('uploadDocuments',{busyLoader:busyLoader, $window:$window, $location:$location, $timeout:$timeout, helperService:helperService, uploadDocumentsService:uploadDocumentsService, documentObjectFactory:documentObjectFactory} );

    }));

    it('should be defined', function(){
        expect(UploadDocumentCtrl).toBeDefined();
    });
    describe('data is set after init', function(){
        var documentType = {"CoverLetter": [],
            "Resume": []};
       beforeEach(function(){
           // mock get documentType
           var documentTypePromise = $q.defer();
           spyOn(uploadDocumentsService, "getDocumentType").and.returnValue(documentTypePromise.promise);
           documentTypePromise.resolve(documentType);
           UploadDocumentCtrl.$onInit();
           $rootScope.$apply();
       });
        it('should call uploadDocumentsService.uploadDocument ', function(){
            expect(uploadDocumentsService.getDocumentType).toHaveBeenCalled();
        });
        it('should set DocumentType in controller' , function(){
            expect(UploadDocumentCtrl.documentType).toEqual(documentType);
        });
    });

});
