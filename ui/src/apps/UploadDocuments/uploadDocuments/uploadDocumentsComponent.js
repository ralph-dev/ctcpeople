(function () {
    function uploadDocumentsController(busyLoader,$window, $location, $timeout, helperService, uploadDocumentsService, documentObjectFactory) {
        var ctrl = this;
        ctrl.documentType = [];
        ctrl.uploadResult = [];
        ctrl.resumeType = "Resume";
        ctrl.showSkillSection = true;
        ctrl.skillGroups = [];
        ctrl.selectedSkillGroups = [];
        ctrl.supportedFileType = ["doc", "docx", "pdf", "rtf", "txt"];
        ctrl.regexSt = "(.*?)\.(" +  ctrl.supportedFileType.join('|') + ")$";
        ctrl.fileTypeRegex = new RegExp(ctrl.regexSt, 'i');
        ctrl.fileList = [];
        ctrl.maxListLength = 5;
        ctrl.maxResumeSize = 3 * 1024 * 1000;

        ctrl.notificationType = {
            info: {
                cssClass: '',
                contents: ''
            },
            success: {
                cssClass: 'alert-success',
                contentsUrl: 'uploadDocuments/successNotification.html'
            },
            invalidDocumentType: {
                cssClass: 'alert-danger',
                contentsUrl: 'uploadDocuments/invalidDocumentType.html'
            },
            noValidAccount: {
                cssClass: 'alert-danger',
                contentsUrl: 'uploadDocuments/noValidAccountNotification.html'
            },
            exceedFilesLimit: {
                cssClass: 'alert-danger',
                contentsUrl: 'uploadDocuments/exceedLimitNotification.html'
            },
            exceedResumeFilesLimit: {
                cssClass: 'alert-danger',
                contents: '<div class="notificationMessage">' +
                '<h5><b>Warning!</b> </h5> ' +
                '<p>Only one document can be submitted as type Resume per upload request.</p> ' +
                '<p>Please update your Document Type selections before uploading.</p> ' +
                '</div>'
                //  contentsUrl: 'uploadActivityFiles/exceedResumeFilesNotification.html'
            },
            genericWarning: {
                cssClass: 'alert-danger',
                contentsUrl: 'uploadDocuments/genericNotification.html'
            },
            noDocumentUploadedWarning: {
                cssClass: 'alert-danger',
                contents: '<div class="notificationMessage">' +
                '<h5><b>Warning!</b> </h5> ' +
                '<p>Please select at least one document</p> ' +
                '</div>'
            },
            containsEmptyRequiredFiles: {
                cssClass: 'alert-danger',
                contents: '<div class="notificationMessage">' +
                '<h5><b>Warning!</b></h5>' +
                '<p>Please complete all the mandatory fields.</p>' +
                '</div>'
            },
            invalidResumeSize:{
                cssClass: 'alert-danger',
                contents: '<div class="notificationMessage">' +
                '<h5><b>Warning!</b></h5>' +
                '<p>Invalid resume file size. Please upload a resume file smaller than 3MB.</p>' +
                '</div>'
            },
            invalidResumeType:{
                cssClass: 'alert-danger',
                contents: '<div class="notificationMessage">' +
                '<h5><b>Warning!</b></h5>' +
                '<p>Invalid resume file format. Please upload a file with the supported format.</p>' +
                '</div>'
            }
        };
        ctrl.notification = ctrl.notificationType.info;
        /**
         * set notification to be showing for 30 sec then reset to default
         * @param type
         */
        function showNotification(type) {
            ctrl.notification = type;
            $window.scrollTo(0, 0);
            $timeout(function () {
                ctrl.notification = ctrl.notificationType.info;
            }, 30000);
        }


        function onFilesUploaded(files) {
            var emptySpaces = ctrl.maxListLength - ctrl.fileList.length;
            var filesToPush = files.splice(0, emptySpaces).map(function (item) {
                return new documentObjectFactory.DocumentObject(item);
            });
            Array.prototype.push.apply(ctrl.fileList, filesToPush);
            console.log("uploadedFiles", files, "files list", ctrl.fileList);

        }

        function removeFile(file) {
            var index = ctrl.fileList.indexOf(file);
            if (index !== -1) {
                ctrl.fileList.splice(index, 1);
            }
        };


        /***
         * Set additionalFields on documentType change
         * @param docData ref document object where type was change
         */
        function onDocumentTypeChange(docData) {
            // get the document type additional fields
            // create a copy to trigger the new value assigned
            var additionalFields = angular.copy(ctrl.documentType[docData.documentType]);
            docData.additionalFields = additionalFields;

            // TODO add logic for resume type
            if (docData.documentType === ctrl.resumeType) {
                docData.resumeSkillGroups = {};
                docData.resumeSkillGroups.skillGroups = angular.copy(ctrl.skillGroups);
                docData.resumeSkillGroups.selectedSkillGroups = angular.copy(ctrl.selectedSkillGroups);
                // check if resume is valid type
                var validType = ctrl.fileTypeRegex.test(docData.fileData.name);
                if(!validType){
                    docData.invalidResumeFormat = true;
                }

                if (docData.fileData.size > ctrl.maxResumeSize) {
                    docData.inValidResumeSize = true;
                } else {
                    docData.inValidResumeSize = false;
                }

            } else {
                // remove resumeSkillGroups
                if (docData.resumeSkillGroups) {
                    delete docData.resumeSkillGroups;
                }
                // remove resume type error
                if(angular.isDefined(docData.invalidResumeFormat)){
                    delete docData.invalidResumeFormat;
                }

                if (docData.inValidResumeSize) {
                    delete docData.inValidResumeSize;
                }
            }

        }

        /**
         * loop through the list and check if any document has invalid document type
         * @param list {Array |DocumentObject}
         * @returns {boolean|true if invalid doc type found}
         */
        function checkInValidDocumentType(list) {
            return list.some(function (item) {
                if(item.documentType === null){
                    return true;
                }
                return item.documentType.length < 1;
            });
        }

        /**
         * check if their is more than one resume type..
         * @param list
         * @returns {boolean}
         */
        function resumeExceedLimit(list) {

            // check if there is more than one resume in the list
            var resumeDocs = list.filter(function (item) {
                return item.documentType.trim() === ctrl.resumeType;
            });
            return resumeDocs.length > 1;
        }

        /**
         * loop over documents and check if contain required fields and if they contain value
         * @param list {Array| DocumentObject}
         * @return {boolean |}
         */
        function containsEmptyRequiredFiles(list) {
            var isEmptyRequiredField = false;
            list.forEach(function (item) {
                if (item.additionalFields) {
                    item.additionalFields.forEach(function (field) {
                        if (field.required && !angular.isDefined(field.values)) {
                            isEmptyRequiredField = true;
                        }
                    });
                }
            });
            return isEmptyRequiredField;
        }

        function invalidResumeFormat(list){
            var invalid = false;
            var resumeDocs = list.filter(function (item) {
                return item.documentType.trim() === ctrl.resumeType;
            });
            if(resumeDocs.length > 0){
                resumeDocs.forEach(function(item){
                    if(item.invalidResumeFormat){
                        invalid = true;
                    }
                });
            }
            return invalid;
        }

        function checkIsValidResumeSize(list) {
            var result = true;
            list.forEach(function (doc) {
                if (doc.inValidResumeSize) {
                    result = false;
                } else {
                    delete doc.inValidResumeSize;
                }
            })

            return result;
        }

        function submitDocuments() {
            ctrl.checkBefore = true;
            /** * validate data before sending */

            /** check if all document contain valid docType **/
            if (checkInValidDocumentType(ctrl.fileList)) {
                showNotification(ctrl.notificationType.invalidDocumentType);
                return;
            }

            if (!ctrl.isValidS3Account) {
                showNotification(ctrl.notificationType.noValidAccount);
                return;
            }
            if (ctrl.fileList.length > 5) {
                showNotification(ctrl.notificationType.exceedFilesLimit);
                return;
            }
            if (ctrl.fileList.length < 1) {
                showNotification(ctrl.notificationType.noDocumentUploadedWarning);
                return;
            }

            if (resumeExceedLimit(ctrl.fileList)) {
                showNotification(ctrl.notificationType.exceedResumeFilesLimit);
                return;
            }
            if(invalidResumeFormat(ctrl.fileList)){
                showNotification(ctrl.notificationType.invalidResumeType);
                return;
            }

            if (containsEmptyRequiredFiles(ctrl.fileList)) {
                showNotification(ctrl.notificationType.containsEmptyRequiredFiles);
                return;
            }

            if (!checkIsValidResumeSize(ctrl.fileList)) {
                showNotification(ctrl.notificationType.invalidResumeSize);
                return;
            }

            busyLoader.start();
            var promiseCounter = ctrl.fileList.length;

            function afterUpload() {
                promiseCounter --;
                if (promiseCounter === 0) {
                    ctrl.uploadedFiles =  ctrl.fileList.filter(function(item){
                        return (item.uploadStatus.complete === true && item.uploadStatus.success === true);
                    });
                    ctrl.fileList = ctrl.fileList.filter(function(item){
                        return (item.uploadStatus.complete === true && item.uploadStatus.success === false);
                    });
                    if( ctrl.uploadedFiles.length > 0){
                        showNotification(ctrl.notificationType.success);
                    }
                    busyLoader.end();
                    console.log("document submitted", ctrl.fileList);
                }
            }

            ctrl.fileList.forEach(function (uploadedDoc) {
                uploadDocumentsService.uploadDocuments(uploadedDoc)
                    .then(function (data) {
                        uploadedDoc.uploadStatus.complete = true;
                        uploadedDoc.uploadStatus.success = true;
                        uploadedDoc.uploadStatus.message = data;
                        afterUpload();

                    }, function (error) {
                        uploadedDoc.uploadStatus.complete = true;
                        uploadedDoc.uploadStatus.success = false;
                        uploadedDoc.uploadStatus.message = error;
                        afterUpload();
                    });
            });
        }

        function onInit() {
            // get list of available documents type
            uploadDocumentsService.getDocumentType()
                .then(function (picklist) {

                    ctrl.documentType = sortObject(picklist);

                    return helperService.getUserPermission();
                })
                .then(function (permission) {
                    ctrl.isValidS3Account = permission.isValidCloudStorageAccount;
                    if(!ctrl.isValidS3Account){
                        showNotification(ctrl.notificationType.noValidAccount);
                    }
                    ctrl.isSkillParsingEnabled = permission.skillParsingEnabled;
                    return uploadDocumentsService.getSkillGroups();
                })
                .then(function (data) {
                    if (data.length > 0 && ctrl.isSkillParsingEnabled) {
                        ctrl.showSkillGroup = true;
                    }
                    ctrl.skillGroups = data.filter(function (skill) {
                        return skill.isSelected === false;
                    });
                    ctrl.selectedSkillGroups = data.filter(function (skill) {
                        return skill.isSelected === true;
                    });

                })
                .finally(function (data) {
                    busyLoader.end();
                });

        }

        //Sort object properties alphabetically
        function sortObject (object) {
             var orderedObject = {};
                    var keys = Object.keys(object);
                    keys.sort();

                    for (var i = 0; i < keys.length; i++ ) {
                        var k = keys[i];
                        orderedObject[k] = object[k];
                    }
            return orderedObject;
        }

        function exitForm() {
            //$window.location.href = "/"+$location.query("srcId");
        	helperService.locateToSObject($window, $location.query("srcId"));
        }

        function removeAllFiles(){
            ctrl.fileList = new Array();
        }

        /******* scope mapping ************/

        ctrl.$onInit = onInit;
        ctrl.onFilesUploaded = onFilesUploaded;
        ctrl.removeFile = removeFile;
        ctrl.onDocumentTypeChange = onDocumentTypeChange;
        ctrl.submitDocuments = submitDocuments;
        ctrl.exitForm = exitForm;
        ctrl.removeAllFiles = removeAllFiles;
    }

    var uploadDocuments = {
        templateUrl: "uploadDocuments/uploadDocuments.html",
        controller: uploadDocumentsController
    };
    angular.module("CTC.People.UploadDocuments").component("uploadDocuments", uploadDocuments);
}());
