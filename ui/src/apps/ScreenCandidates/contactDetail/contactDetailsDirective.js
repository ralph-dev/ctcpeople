(function () {
    "use strict";
    function candidateDetail(placementCandidateService, $window, busyLoader, searchService, redirectService, urlPrefixService, contactService, Notification, $sce) {
        return {
            restrict: "EA",
            scope: {
                targetRecord: "=",
                renderFlag: "=",
                searchKeyword: "=",
                itemsPerPage: "=",
                isScreenCandidateNoteEnabled: "=",
                isBulkEmailEnabled: "=",
                onAdd: "&",
                onRemove: "&",
                onSendContactEmail: "&"
            },
            templateUrl: "contactDetail/contactDetailView.html",
            link: function (scope, element, attr) {

                scope.recordDetailsCopy = {};

                function loadCandidateInfo() {

                    if (scope.targetRecord.candidateInfo) {
                        console.log(">> display data");

                        scope.recordManagement = scope.targetRecord.candidateInfo.recordManagement;
                        scope.activitiesHistory = scope.targetRecord.candidateInfo.activitiesHistory;
                        scope.recordDetails = scope.targetRecord.candidateInfo.recordDetails;

                    } else {

                        console.log(">> getCandidateInfo");
                        busyLoader.start();
                        searchService.getCandidateInfo(scope.targetRecord.Candidate__c)
                            .then(function (data) {
                                scope.recordManagement = data.recordManagement;
                                scope.activitiesHistory = data.activitiesHistory;
                                scope.recordDetails = data.recordDetails;
                                scope.recordDetailsCopy = angular.copy(scope.recordDetails);
                                scope.targetRecord.candidateInfo = data;
                            })
                            .finally(function () {
                                busyLoader.end();
                            });

                    }
                }

                scope.naveToPage = function (targetId) {
                	
                	return redirectService.naveToPage($window,targetId);
//                    var targetUrl = $window.location.origin;
//                    targetUrl += "/" + targetId;
//                    return targetUrl;
//                    // redirectService.openSalesForceObject(targetId, "_blank");
                };

                scope.hideDetails = function () {
                    scope.targetRecord.detailFlag = false;
                    scope.targetRecord = undefined;
                };

                scope.addToShortlist = function (client) {
                    scope.onAdd({client: client});
                };

                scope.logContactCall = function (client) {
                    var url = "00T/e?title=Call&who_id=" + client.Candidate__c + "&followup=1&tsk5=Call&retURL=%2F" + client.Candidate__c;

                    //TODO: Url provided without org prefix. Add org prefix.
                    var targetUrl = $window.location.origin;
                    targetUrl += "/" + url;
                    redirectService.open(targetUrl, "_blank");
                };

                scope.downloadResume = function (client) {
                    var url = "apex/ViewDoc?id=" + client.Web_Documents__r[0].Id;

                    var targetUrl = $window.location.origin;

                    targetUrl += "/" + url;
                    redirectService.open(targetUrl,"_blank");
                };

                scope.sendContactEmail = function (client) {

                    // var targetUrl = $window.location.origin;

                    // var contactList = [];
                    // contactList.push(client.Candidate__c);

                    // var url = "/apex/" + urlPrefixService.getPrefix() + "extract4sms?sourcepage=rostering&cjid=" + JSON.stringify(contactList);
                    // targetUrl += url;
                    // redirectService.open(targetUrl, "_blank");
                    scope.onSendContactEmail({el: client});
                };

                scope.emailDisableNotification = function () {
                    Notification.error("To activate Email functionality, please contact support@clicktocloud.com");
                };

                scope.removeFromShortlist = function (client) {
                    scope.onRemove({client: client});
                };

                scope.$watch('targetRecord', function (newVal, oldVal) {
                    // if (newVal === true) {
                        // init();
                        loadCandidateInfo();
                    // }
                });


                scope.saveCandidateDetails = function (contact) {

                    if (scope.updateCandidateDetails.$invalid) {
                        return;
                    }

                    busyLoader.start();
                    contactService.savePeopleInfo(contact)
                        .then(function (data) {
                            Notification.success("Update Successfully");
                            scope.recordDetailsCopy = angular.copy(scope.recordDetails);
                            busyLoader.end();
                            scope.$modals['Update Candidate Details'].hide();
                        }, function (error) {
                            Notification.warning("Update Failed");
                            busyLoader.end();
                        });
                };

                scope.resetCandidateDetails = function () {
                    scope.recordDetails = angular.copy(scope.recordDetailsCopy);
                };

                scope.trustAsHtml = function(string) {
                    return $sce.trustAsHtml(string);
                };
            }
        };
    }

    angular.module("CTC.People.ScreenCandidates").directive("candidateDetail", ["placementCandidateService", "$window", "busyLoader", "searchService", "redirectService", "urlPrefixService", "contactService", "Notification", "$sce", candidateDetail]);
}());
