
(function () {
    "use strict";
    angular.module('CTC.People.ScreenCandidates').filter("screenCandidateFilter", function ($scope) {
        return function (items, filterArray) {
            var filteredItems = [];
            // 2 dimenional array to store filters
            var filterArraySorted= [];

            //loop through filterArray to check if there are same name filter,
            //insert to the same location,
            //else insert to the end of filterArraySorted
            angular.forEach(filterArray, function(filter) {

                var matchIndex = 0;
                var insertIndex = 0;
                for (var i = 0; i < filterArraySorted.length; i++) {
                    var subArray = filterArraySorted[i];
                    for (var j = 0; j < subArray.length; j++) {
                        if (subArray[j].fieldName === filter.fieldName) {
                            matchIndex = i;
                        }
                    }
                    insertIndex = i;
                }

                if (matchIndex < insertIndex) {
                    filterArraySorted[matchIndex].push(filter);
                } else {
                    filterArraySorted[insertIndex].push(filter);
                }

            });

            //loop through filterArraySorted to filter out items
            angular.forEach(filterArraySorted, function (subFilterArray) {

                filteredItems = items.filter(function(item) {

                    angular.forEach(subFilterArray, function (filter) {

                        var fieldName = filter.fieldName.split(" : ");

                        if (fieldName[0] === $scope.objectLabelNames["Candidate Management"]) {

                            if (item[fieldName[1]]) {

                                if (item[fieldName[1]].indexOf(filter.contains) >= 0) {
                                    return true;
                                }
                            }
                        }else {

                            if(item.Candidate__r[fieldName[1]]) {

                                if (item.Candidate__r[fieldName[1]].indexOf(filter.contains) >= 0) {
                                    return true;
                                }
                            }
                        }
                    });

                    return false;
                });
            });

            return filteredItems;
        };
    });

}());