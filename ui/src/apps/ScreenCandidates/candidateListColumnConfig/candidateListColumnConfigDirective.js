/***
 * partial directive to manage changing the search result grid columns
 *
 * @display-column : list of column used in the result gird
 * @field-List : list of fields can be used as display column
 * @on-update : function  with column list as parameter to be called when column list is changed.
 * */

(function () {
    "use strict";
    function resultGridColumnConfig($q) {
        return {
            restrict: "EA",
            templateUrl: "candidateListColumnConfig/candidateListColumnConfig.html",
            scope: {
                displayColumn: "=",
                selectedObjectList: "=",
                cmFieldList: "=",
                candidateFieldList: "=",
                objectLabelNames: "=",
                onUpdate: "&"
            },
            link: function (scope, element, attr) {

                // TODO refactor to use the some object instead of mapping again.
                scope.columnObjectList = [];

                function values(obj) {
                    var ret = [];
                    Object.keys(obj).forEach(function(key) {
                        ret.push(obj[key]);
                    });

                    return ret;
                }

                var objectLabelNames = values(scope.objectLabelNames);
                angular.forEach(objectLabelNames, function(nanme) {
                    var tempObject = {name: nanme};
                    scope.columnObjectList.push(tempObject);
                });

                scope.configColumnList = [];
                scope.configObjectList = [];
                // configCmFiledList and configCandidateFieldList are dimensions array, each element contains field list for each customised column
                scope.configCmFiledList = [];
                scope.configCandidateFieldList = [];

                var MAX_COL_LENGTH = 4; // set the max of column to 4

                scope.$watch("displayColumn", function (newVal, oldVal) {
                    if (newVal && Array.isArray(newVal)){
                        scope.configColumnList = new Array(MAX_COL_LENGTH);
                        newVal.forEach(function (item, index) {
                            scope.configColumnList[index] = item;
                        });
                    }
                });

                scope.$watch("objectLabelNames", function (newVal, oldVal) {
                     if (newVal) {
                        objectLabelNames = values(scope.objectLabelNames);
                        scope.columnObjectList = [];
                        angular.forEach(objectLabelNames, function(nanme) {
                            var tempObject = {name: nanme};
                            scope.columnObjectList.push(tempObject);
                        });
                     }
                });

                scope.$watchCollection("selectedObjectList", function (newVal, oldVal) {
                    if (newVal && Array.isArray(newVal)) {
                        scope.configObjectList = angular.copy(scope.selectedObjectList);
                    }
                });

                function removeSelectedFields () {
                    scope.configColumnList.forEach(function(column, i) {

                            if (column.columnObject && column.columnObject.name === scope.objectLabelNames["Candidate Management"]) {

                                scope.configCandidateFieldList = scope.configCandidateFieldList.map(function(candidateField, j){
                                    if(j !== i) {
                                        candidateField = candidateField.filter(function (field) {
                                            return field.Field_api_name !== column.Field_api_name;
                                        });
                                    }
                                    return candidateField;
                                });

                            } else {

                                scope.configCmFiledList = scope.configCmFiledList.map(function(cmField, j){
                                    if(j !== i) {
                                        cmField = cmField.filter(function (field) {
                                            return field.Field_api_name !== column.Field_api_name;
                                        });
                                    }
                                    return cmField;
                                });

                            }
                        });
                }

                scope.$watchCollection('configColumnList', function (newVal, oldVal) {

                    // assign object name to custom column
                    for (var i = 0; i < scope.configObjectList.length; i++) {
                        if (scope.configObjectList[i] && scope.configColumnList[i]) {
                            scope.configColumnList[i].columnObject = scope.configObjectList[i];
                        }
                    }

                    // setup field lists
                    resetCmFieldList(scope.cmFieldList);
                    resetCandidateFieldList(scope.candidateFieldList);

                    if (newVal && Array.isArray(newVal)) {
                        removeSelectedFields();
                    }

                });

                function resetCmFieldList(newVal) {
                    var candidateProgressFieldName = "Candidate_Status__c";

                    if (candidateProgressFieldName.indexOf(namespace) < 0) {
                        candidateProgressFieldName = namespace + "Candidate_Status__c";
                    }

                    if (newVal) {
                        for(var i = 0; i < MAX_COL_LENGTH; i++) {
                            scope.configCmFiledList[i] = angular.copy(newVal);

                            // 'Candidate Progress' can only be chosen as field 1
                            if(i !== 0) {
                                scope.configCmFiledList[i] = scope.configCmFiledList[i].filter(function(f){
                                    return f.Field_api_name !== candidateProgressFieldName;
                                });
                            }

                            scope.configCmFiledList[i] = scope.configCmFiledList[i].filter(function(f) {
                                return f.Field_api_name !== "Name" &&
                                    f.Field_api_name !== "Candidate__c" &&
                                    f.Field_api_name !== "Status__c" &&
                                    f.Field_api_name !== "Recruiter_Notes__c";
                            });
                        }
                    }
                }

                function resetCandidateFieldList(newVal) {
                    if (newVal) {
                        for(var i = 0; i < MAX_COL_LENGTH; i++) {
                            scope.configCandidateFieldList[i] = angular.copy(newVal);
                        }
                    }
                }

                scope.$watch('cmFieldList', function (newVal, oldVal) {
                    resetCmFieldList(newVal);
                });

                scope.$watch('candidateFieldList', function (newVal, oldVal) {
                    resetCandidateFieldList(newVal);
                });

                scope.changeConfigObject = function (index) {
                    scope.configColumnList[index] = "";
                };

                scope.updateColumn = function (columnList) {
                    if (angular.isDefined(scope.onUpdate) && angular.isFunction(scope.onUpdate)) {
                        // loop over list and remove $$hashKey
                        var updatedList =[];
                        columnList.forEach(function (item) {
                            if (item && item.$$hashKey) {
                                delete item.$$hashKey;
                            }
                            if (item && item.Field_api_name) {
                                updatedList.push(item);
                            }
                        });

                        var updatedObjectList = [];
                        for (var i = 0; i < scope.configObjectList.length; i++) {
                            if (scope.configColumnList[i] !== undefined && scope.configColumnList[i] !== null && scope.configColumnList[i] !== "") {
                                updatedObjectList.push(scope.configObjectList[i]);
                            }
                        }

                        $q.when(scope.onUpdate({columnlist: updatedList, selectedObjectList: updatedObjectList})).then(function () {
                            console.log("column has been update");
                        });

                    }
                };

                function isCandidateManagementColumn(index) {
                    return scope.configObjectList[index] && scope.configObjectList[index].name === scope.objectLabelNames['Candidate Management'];
                }

                function isContactColumn(index) {
                    return scope.configObjectList[index] && scope.configObjectList[index].name === scope.objectLabelNames['Contact'];
                }

                scope.getConfigFieldList = function(index) {
                    var configFieldList;
                    var selectedFieldNames = [];
                    var i;
                    if(isCandidateManagementColumn(index)) {
                        configFieldList = angular.copy(scope.configCmFiledList[index]);
                        for(i = 0; i < MAX_COL_LENGTH; i++) {
                            if(i !== index &&
                                isCandidateManagementColumn(i) &&
                                !!scope.configColumnList[i]) {
                                selectedFieldNames.push(scope.configColumnList[i].Field_api_name);
                            }
                        }

                        configFieldList = configFieldList.filter(function(f) {
                            return selectedFieldNames.indexOf(f.Field_api_name) < 0;
                        });

                        return configFieldList;
                    }
                    else if(isContactColumn(index)) {
                        configFieldList = angular.copy(scope.configCandidateFieldList[index]);
                        for(i = 0; i < MAX_COL_LENGTH; i++) {
                            if(i !== index &&
                                isContactColumn(i) &&
                                !!scope.configColumnList[i]) {
                                selectedFieldNames.push(scope.configColumnList[i].Field_api_name);
                            }
                        }

                        configFieldList = configFieldList.filter(function(f) {
                            return selectedFieldNames.indexOf(f.Field_api_name) < 0;
                        });

                        return configFieldList;
                    }
                    else {
                        return [];
                    }
                };

            }
        };
    }

    angular.module("CTC.People.ScreenCandidates").directive("columnConfig", ["$q", resultGridColumnConfig]);
}());
