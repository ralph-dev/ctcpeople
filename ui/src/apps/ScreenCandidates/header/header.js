angular.module("CTC.People.ScreenCandidates").directive("screenCandidatesHeader", ["$window","urlPrefixService", "helperService",function ($window, urlPrefixService,helperService) {
    return {
        restrict:"EA",
        scope: {
            headerTitle: "@",
            description: "@",
            returnUrl: "@ctcReturn",
            ctcReturnAlt: "@?",
            confirmUpdateFlag: "="
        },
        templateUrl: "common/directives/header/header.html",
        link: function(scope, element, attrs) {

            scope.logoURL = "static/img/CTC_logos-People_white.png";

            if(angular.isUndefined(scope.logoURL)) {
                urlPrefixService.getDetail().then(function(org) {
                    scope.logoURL = org.companyLogoURL;
                });
            }

            scope.$on('header:updateDescription', function(event, description) {
                scope.description = description;
            });

            function closeApp () {
            	var url = "/";
            	if(scope.returnUrl && scope.returnUrl!=="" ){
            		if(scope.returnUrl.indexOf("/") === 0){
            			url = scope.returnUrl;
            		}else{
            			url += scope.returnUrl;
            		}	
            	}
            	
            	helperService.locateTo($window, url);
 
            }

            scope.$root.$on("CONFIRMED_DISCARD", function (event, args) {
                if (scope.$root.isCloseApp) {
                    closeApp();
                }
            });

            scope.$root.$on("CONFIRM_SAVED", function (event, args) {
                if (scope.$root.isCloseApp) {
                    closeApp();
                }
            });

            scope.returnPage = function () {
                scope.$root.isCloseApp = true;
                if (scope.$root.confirmUpdateFlag) {
                    scope.$root.$broadcast("CLOSE_APP");
                } else {
                    closeApp();
                }
            };

            scope.concatTitleDescription = function(){
                if(angular.isDefined(scope.description)){
                    return [scope.headerTitle,scope.description].join(" - ");
                }else{
                    return scope.headerTitle;
                }
            };

            scope.isLightningEnabled = true;

            var lightningMatch = "lightning.force.com";

            // if(window.location.href.indexOf(lightningMatch) > -1) {
            //     console.log('Lightning is enabled');
            //     scope.isLightningEnabled = false;
            // }
        }
    };
}]);
