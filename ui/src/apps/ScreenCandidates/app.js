angular.module("CTC.People.ScreenCandidates", [
    "CTC.Components"
]);

angular.module("CTC.People.ScreenCandidates")
    .config(function ($stateProvider, $urlRouterProvider) {
        "use strict";

        $urlRouterProvider.otherwise('/screen');

        $stateProvider
            .state('screen', {
                url: '/screen',
                templateUrl: 'screenCandidates/screenCandidatesView.html',
                controller: 'ScreenCandidatesController'
            });

    });


// OPTIONAL on run option
angular.module("CTC.People.ScreenCandidates")
    .run(function ($rootScope, urlPrefixService, $location) {
        urlPrefixService.setPrefix();
        $rootScope.vacancyId = $location.query("id");

        $rootScope.namespace = namespace;
    });
