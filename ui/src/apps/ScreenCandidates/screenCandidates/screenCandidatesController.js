(function () {
    "use strict";
    function screenCandidatesController($scope, $rootScope, $window, $timeout, $uibModal, busyLoader, vacancyService, placementCandidateService, searchService, Notification, redirectService, urlPrefixService, orderByFilter, sortService,helperService) {

        var scope = $scope;

        $scope.vacancyDetail = {};
        $scope.vacancyCandidateList = [];
        //$scope.vacancyCandidateShortlist = [];
        $scope.candidateStatusPicklist = [];
        $scope.searchResumeKeyword = "";
        $scope.currentPageCandidateList = [];
        $rootScope.confirmUpdateFlag = false;
        $rootScope.isCloseApp = false;
        $scope.displayColumn = [];
        $scope.viewOption = {
            New: "New",
            All: "All",
            Shortlist: "Shortlist"
        };
        $scope.uiState = {
            listState: "New",
            ShowConfig: false,
            isChangeView: false
        };

        var vacancyId = $rootScope.vacancyId;
        var nextListView = "";
        $scope.allVacancyCandidateList = [];
        $scope.selectedObjectList = [];
        $scope.propertyName = 'CreatedDate';
        $scope.reverse = true;
        $scope.hasClientSelected = false;
        $scope.isBulkEmailEnabled = false;
        $scope.isBulkSMSEnabled = false;
        $scope.isScreenCandidateNoteEnabled = true;
        $scope.originClientsFiltered = [];

        $scope.objectLabelNames = {
            "Candidate Management": "CandidateM",
            "Contact": "People"
        };

        var defaultFitlerFieldOptions;

        function updateFilterFieldOptions() {
            $scope.fitlerFieldOptions = [
                {
                    fieldName: $scope.objectLabelNames["Candidate Management"] +" : ID",
                    ObjectName: $scope.objectLabelNames["Candidate Management"],
                    Field_api_name:"Name",
                    ColumnName: $scope.objectLabelNames["Candidate Management"] + " ID"
                },
                {
                    fieldName: $scope.objectLabelNames["Contact"] + " : Name",
                    ObjectName: $scope.objectLabelNames["Contact"],
                    Field_api_name:"Name",
                    ColumnName: $scope.objectLabelNames["Contact"] + " Name"
                },
                {
                    fieldName: $scope.objectLabelNames["Candidate Management"] +" : Status",
                    ObjectName: $scope.objectLabelNames["Candidate Management"],
                    Field_api_name:"Status__c",
                    ColumnName:"Status"
                },
            ];

            if($scope.isScreenCandidateNoteEnabled) {
                $scope.fitlerFieldOptions.push({
                        fieldName: $scope.objectLabelNames["Candidate Management"] + " : Recruiter Notes",
                        ObjectName: $scope.objectLabelNames["Candidate Management"],
                        Field_api_name:"Recruiter_Notes__c",
                        ColumnName:"Recruiter Notes"
                    });
            }

            $scope.fitlerFieldOptions = orderByFilter($scope.fitlerFieldOptions, "fieldName", false);

            defaultFitlerFieldOptions = angular.copy($scope.fitlerFieldOptions);
        }

        updateFilterFieldOptions();

        function init() {
            getVacancy(vacancyId);
            getScreenCandidates();
            getCMFieldsList();
            $scope.getUserPermission();
        }

        /**************
         * helperExtension: getUserPermission()
         * get ORG: isBulkEmailEnabled, isBulkSMSEnabled
         *************/

        $scope.getUserPermission = function() {

            searchService.getUserPermission().then(function(data){

                if (data) {
                    $scope.isBulkEmailEnabled = data.isBulkEmailEnabled;
                    $scope.isBulkSMSEnabled = data.isBulkSMSEnabled;
                    $scope.isScreenCandidateNoteEnabled = data.isScreenCandidateNoteEnabled;
                }
            },function(error){
            });

        };

        /**************
         * Swtich View
         *************/
        function applyView() {
            var tempArray = [];
            var i;
            if($scope.viewOption.New === $scope.uiState.listState) {
                for (i = 0; i < $scope.allVacancyCandidateList.length; i++) {
                    if ($scope.allVacancyCandidateList[i].Status__c === $scope.viewOption.New) {
                        tempArray.push($scope.allVacancyCandidateList[i]);
                    }
                }

                $scope.vacancyCandidateList = tempArray;
            }
            else if($scope.viewOption.Shortlist === $scope.uiState.listState) {
               for (i = 0; i < $scope.allVacancyCandidateList.length; i++) {
                   if ($scope.allVacancyCandidateList[i].Shortlist__c === true) {
                       tempArray.push($scope.allVacancyCandidateList[i]);
                   }
               }

               $scope.vacancyCandidateList = tempArray;
            }
        }

        function resetSorting() {
            $scope.propertyName = 'CreatedDate';
            if($scope.viewOption.Shortlist === $scope.uiState.listState) {
                $scope.propertyName = "Shortlist_Time__c";
            }
            $scope.reverse = true;
            $scope.allVacancyCandidateList = sortService.sort($scope.allVacancyCandidateList, $scope.propertyName, $scope.reverse);
            $scope.vacancyCandidateList = $scope.allVacancyCandidateList;
        }

        $scope.switchToAllView = function (){
            $rootScope.$broadcast("SWITCH_VIEW");

            if ($rootScope.confirmUpdateFlag) {
                $scope.uiState.isChangeView = true;
                $rootScope.$broadcast("CHANGE_VIEW");
                nextListView = $scope.viewOption.All;
            }else {
                $scope.uiState.listState  = $scope.viewOption.All;

                $scope.$broadcast("RESET_FILTER");

                resetSorting();

                applyView();

                nextListView = "";
                $scope.uiState.isChangeView = false;
            }

        };

        $scope.switchToNewView = function () {
            $rootScope.$broadcast("SWITCH_VIEW");

            if ($rootScope.confirmUpdateFlag) {
                $scope.uiState.isChangeView = true;
                $rootScope.$broadcast("CHANGE_VIEW");
                nextListView = $scope.viewOption.New;
            } else {
                $scope.uiState.listState  = $scope.viewOption.New;

                $scope.$broadcast("RESET_FILTER");

                //reset order to created date
                $scope.propertyName = 'CreatedDate';
                $scope.reverse = true;

                // $scope.vacancyCandidateList = $scope.allVacancyCandidateList.filter(function(candidate) {
                //     return candidate.Status__c === $scope.viewOption.New;
                // });

                resetSorting();

                applyView();

                nextListView = "";
                $scope.uiState.isChangeView = false;
            }

        };

        $scope.switchToShortlistView = function () {
            $rootScope.$broadcast("SWITCH_VIEW");

            if ($rootScope.confirmUpdateFlag) {
                $scope.uiState.isChangeView = true;
                $rootScope.$broadcast("CHANGE_VIEW");
                nextListView = $scope.viewOption.Shortlist;
            } else {
                $scope.uiState.listState  = $scope.viewOption.Shortlist;

                $scope.$broadcast("RESET_FILTER");

                // $scope.vacancyCandidateList =  $scope.allVacancyCandidateList.filter(function(candidate){
                //     return candidate.Shortlist__c === true;
                // });

                resetSorting();

                applyView();

                nextListView = "";
                $scope.uiState.isChangeView = false;
            }

        };

        /*************
         * Action
         ************/

        $scope.onAddToShortlist = function (client) {
            //change the selected contact's shortlist status
            client.Shortlist__c = true;
            var shortlistDateTime = new Date();
            client.Shortlist_Time__c = Date.parse(shortlistDateTime);

            $scope.vacancyCandidateList.forEach(function(candidate) {
                if (candidate.Id === client.Id) {
                    candidate.Shortlist__c = true;
                    candidate.Shortlist_Time__c = Date.parse(shortlistDateTime);

                    if (client.detailFlag) {
                        candidate.detailFlag = true;
                    }
                }
            });

            $scope.allVacancyCandidateList.forEach(function(candidate) {
                if (candidate.Id === client.Id) {
                    candidate.Shortlist__c = true;
                    candidate.Shortlist_Time__c = Date.parse(shortlistDateTime);
                }
            });

            placementCandidateService.updateRecords([client])
                .then(function(data){
                    Notification.success({message:"Added to Shortlist"});
                });
        };

        $scope.onRemoveFromShortlist = function (client) {
             //change the selected contact's shortlist status
            client.Shortlist__c = false;

            $scope.vacancyCandidateList.forEach(function(candidate) {
                if (candidate.Id === client.Id) {
                    candidate.Shortlist__c = false;

                    if (client.detailFlag) {
                        candidate.detailFlag = true;
                    }
                }
            });

            $scope.allVacancyCandidateList.forEach(function(candidate) {
                if (candidate.Id === client.Id) {
                    candidate.Shortlist__c = false;
                }
            });

            // $scope.vacancyCandidateShortlist = $scope.vacancyCandidateShortlist.filter(function(candidate) {
            //     return candidate.Id !== client.Id;
            // });

            if ($scope.uiState.listState  === $scope.viewOption.Shortlist) {
                //$scope.vacancyCandidateList =$scope.vacancyCandidateShortlist;
                $scope.vacancyCandidateList =  $scope.vacancyCandidateList.filter(function(candidate){
                        return candidate.Shortlist__c === true;
                });
            }

            placementCandidateService.updateRecords([client])
                .then(function(data){
                    Notification.success("Removed from Shortlist");
                });
        };

        /*****************
         *  Action Post Multi-Selection
         * ***************
        */


        function getClientAccountIds () {
            var selectedClients = [];

            angular.forEach($scope.currentPageCandidateList, function (client, key) {
                if (client.isSelect) {
                    selectedClients.push(client.Candidate__c);
                }
            });

            return selectedClients;
        }

        function getCandidateManagementIds () {
            var selectedCandidates = [];

            angular.forEach($scope.currentPageCandidateList, function (client, key) {
                if (client.isSelect) {
                    selectedCandidates.push(client.Id);
                }
            });

            return selectedCandidates;
        }

         /**
         * open new tab for email service. take a list of contact
         * @param contactsList array of contact id
         */
        $scope.sendEmailToContactList = function (contactList) {
            var rootURL = $window.location.origin;
            searchService.screenCandidateGenerateBulkEmailURI(contactList, {
                "Placement__c": $rootScope.vacancyId,
            })
                .then(function (data) {
                   // var targetUrl = rootURL + data;
                	//console.log(data);
                	//var targetUrl = helperService.buildLightningUrl(rootURL, data);
                	var targetUrl = data;
                	//console.log("send email url : " + targetUrl);
                     //var newTab = $window.open(target);
                    var newTab = redirectService.open(targetUrl, "_blank");

                    // check if the window open then alert user if popup is blocked
                    $timeout(function () {
                        // Check if popup blocker is enabled by verifying the height of the new poup
                        if (!newTab || newTab.outerHeight === 0) {
                            alert("Please disable the popup blocker");
                        }
                    }, 1000);
                });
        };

        // No more than 100 candidates should be selected for bulk email and bulk sms
        // Currently, page will clear selection and only 50 candidaetes per page, so no restrction on number in here
        $scope.sendBulkEmail = function() {

            var contactList = getClientAccountIds();

            if (contactList.length > 0) {
                $scope.sendEmailToContactList(contactList);
            } else {
                Notification.warning("Please select candidates to send bulk email.");
            }

        };

         function sendSMS(contactList) {

            var targetUrl = $window.location.origin;

            var url = "/apex/" + urlPrefixService.getPrefix() + "extract4sms?sourcepage=rostering&cjid=" + JSON.stringify(contactList);
            targetUrl += url;
            redirectService.open(url, "_blank");
        }

        // No more than 100 candidates should be selected for bulk email and bulk sms
        // Currently, page will clear selection and only 50 candidaetes per page, so no restrction on number in here
        $scope.sendBulkSms = function() {

            var contactList = getClientAccountIds();

            if (contactList.length > 0) {
                sendSMS(contactList);
            } else {
                Notification.warning("Please select candidates to send bulk SMS.");
            }
        };

        $scope.sendBulkResume = function() {

            var candidateManagementList = getCandidateManagementIds();
            if (candidateManagementList.length > 0) {
                //var targetUrl = $window.location.origin;
                var url =  "/apex/" + urlPrefixService.getPrefix() + "extract4SR?sourcepage=screencandidate&cjid=" + JSON.stringify(candidateManagementList);
                //targetUrl += url;
                //redirectService.open(targetUrl, "_blank");
                redirectService.open(url, "_blank");
            } else {
                Notification.warning("Please select candidates to send resume.");
            }
        };

        $scope.searchResume = function (keyword) {
            $scope.searchResumeKeyword = keyword;
        };

        $scope.emailDisableNotification = function () {
            Notification.error("To activate Email functionality, please contact support@clicktocloud.com");
        };

        $scope.smsDisableNotification = function () {
            Notification.error("To activate Bulk SMS functionality, please contact support@clicktocloud.com");
        };

        $scope.addOtherVacancyDisableNotification = function () {
             Notification.warning("Please select candidates to add to other vacancy");
        };

        //TODO: change this function to allow only update only clients that have changed.
        $scope.confirmBtn = function () {
            var updateItem = [];
            for (var i=0; i<$scope.currentPageCandidateList.length; i++) {

                var cm = $scope.currentPageCandidateList[i];
                var obj = {};
                obj.Id = cm.Id;
                if (cm.Status__c) {
                    obj.Status__c = cm.Status__c;
                }
                if (cm.Recruiter_Notes__c !== undefined) {
                    obj.Recruiter_Notes__c = cm.Recruiter_Notes__c;
                }
                if (cm.Candidate_Status__c !== undefined) {
                    obj.Candidate_Status__c = cm.Candidate_Status__c;
                }
                if (cm.Rating__c) {
                    obj.Rating__c = cm.Rating__c;
                }
                updateItem.push(obj);

            }

            if (updateItem.length > 0) {
                busyLoader.start();
                placementCandidateService.updateRecords(updateItem).then(function(data){
                    Notification.success('Update Saved Successfully');
                    $rootScope.confirmUpdateFlag = false;
                    //update local records
                    angular.forEach(data, function (currentRecord, key) {

                        angular.forEach($scope.vacancyCandidateList, function (localRecord, key) {

                            if (localRecord.Id === currentRecord.Id) {
                                localRecord.Status__c = currentRecord.Status__c;
                                localRecord.Recruiter_Notes__c = currentRecord.Recruiter_Notes__c;
                                if (currentRecord.Candidate_Status__c) {
                                    localRecord.Candidate_Status__c = currentRecord.Candidate_Status__c;
                                }
                                if (currentRecord.Rating__c) {
                                    localRecord.Rating__c = currentRecord.Rating__c;
                                }
                            }
                        });

                    });

                    //will trigger watchcollection fire which runs after filter function runs
                    // // reset new view in case a status change 
                    // if ( $scope.uiState.listState  === $scope.viewOption.New) {

                    //     $scope.vacancyCandidateList = $scope.allVacancyCandidateList.filter(function(candidate) {
                    //         return candidate.Status__c === $scope.viewOption.New;
                    //     });
                    // }
                    $rootScope.$broadcast("CONFIRM_SAVED");
                    busyLoader.end();
                }, function(error) {
                    Notification.error('Update Failed');
                    busyLoader.end();
                });
            }
        };

        $scope.discardChangesBtn = function() {
            $rootScope.$broadcast("CHANGE_PAGE_DISCARD", "EMIT");
        };

        $scope.exitBtn = function () {
            if (vacancyId) {
            	
                    //$window.location.href = ["/",vacancyId].join("");
            		helperService.locateToSObject($window, vacancyId);
                }else {
                    //$window.location.href = "/";
                	helperService.locateTo($window, "/");
            }
        };

        function getVacancy(vacancyId) {

            busyLoader.start();
            vacancyService.getVacancy(vacancyId).then(function(data){

                $scope.vacancyDetail = data;

            },function(error){
                busyLoader.end();
            });
        }

        function getCMFieldsList() {
            //busyLoader.start();
            searchService.getCMFieldsList()
                .then(function(fieldsList) {
                    $scope.columnFieldList = fieldsList;
                    //busyLoader.end();
                });
        }

        function processCMList(candidateManagements) {
            candidateManagements.forEach(function(candidateManagement){
                candidateManagement.isSelect = false;
            });

            return candidateManagements;
        }

        function mapCustomColumn (data) {
            if (data) {
                angular.forEach(data, function(value, key){
                    value.forEach(function(column) {
                        var result = angular.copy(column);
                        result.columnObject = {};
                        result.columnObject.name = key;
                        $scope.displayColumn.push(result);
                    });
                });
            }

            $scope.displayColumn = orderByFilter($scope.displayColumn, "Order", false);

            for (var i = 0; i < $scope.displayColumn.length; i++) {
                if ($scope.displayColumn[i].columnObject) {
                    $scope.selectedObjectList[i] = $scope.displayColumn[i].columnObject;
                }
            }

        }

        function mapFilterOption (data) {
            if (data) {
                $scope.fitlerFieldOptions = angular.copy(defaultFitlerFieldOptions);

                angular.forEach(data, function(value, key){
                    value.forEach(function(column) {

                        var filterFieldOption = {};
                        filterFieldOption.Field_api_name = column.Field_api_name;
                        filterFieldOption.referenceFieldName = column.referenceFieldName;
                        filterFieldOption.ObjectName = key;
                        filterFieldOption.ColumnName = column.ColumnName;
                        filterFieldOption.fieldName = key + " : " + column.ColumnName;
                        filterFieldOption.fieldType = column.fieldtype;

                        if(filterFieldOption.fieldType === "DATE" || filterFieldOption.fieldType === "DATETIME") {
                            return;
                        }

                        $scope.fitlerFieldOptions.push(filterFieldOption);
                    });
                });

                $scope.fitlerFieldOptions = orderByFilter($scope.fitlerFieldOptions, "fieldName", false);
            }
        }

        function getScreenCandidates() {

            searchService.getContactFieldsList()
                .then(function(fieldsList){
                    $scope.candidateFieldList = fieldsList;

                    return searchService.getObjectsLabelName();
                })
                .then(function(objectLabelNames) {

                    $scope.objectLabelNames = objectLabelNames;

                    updateFilterFieldOptions();

                    return searchService.getScreenCandidatesDisplayColumns();
                })
                .then(function(data){

                    mapCustomColumn(data);

                    mapFilterOption(data);

                    var customColumns = JSON.stringify(data);

                    if ($rootScope.cmIdList && $rootScope.cmIdList.length > 0) {
                        return placementCandidateService.getCMListByVacancyIdWithCMId(vacancyId, customColumns, $rootScope.cmIdList);
                    }

                    return placementCandidateService.getCMListByVacancyId(vacancyId, customColumns);
                })
                .then(function(data){

                    $scope.allVacancyCandidateList = sortService.sort(data, $scope.propertyName, $scope.reverse);
                    $scope.vacancyCandidateList = angular.copy($scope.allVacancyCandidateList);
                    //assign different status picklist by records
                    for(var i=0; i<$scope.allVacancyCandidateList.length; i++){
                        var recId = $scope.allVacancyCandidateList[i].RecordTypeId;
                        var picklistValuesByRT =  getPicklistValuesByRT(recId);
                        $scope.allVacancyCandidateList[i].statusPickList = picklistValuesByRT; 
                    }

                    if ($rootScope.cmIdList && $rootScope.cmIdList.length > 0) {
                        $scope.switchToAllView();
                    } else {
                        $scope.switchToNewView();
                    }

                    busyLoader.end();
                });
        }


        $scope.confirmUpdateDisplayColumn = function(columnlist, updatedObjectList) {
            if ($rootScope.confirmUpdateFlag) {
                $scope.confirmUnsavedChangedForCustomColumns(columnlist, updatedObjectList);
            }
            else {
                $scope.updateDisplayColumn(columnlist, updatedObjectList);
            }
        };

        $scope.updateDisplayColumn = function(columnlist, updatedObjectList) {

            busyLoader.start();
            $scope.uiState.ShowConfig = false;
            $scope.displayColumn = [];

            $scope.selectedObjectList = updatedObjectList;

            var columnMap = {};
            // loop over column list remove $$hashkey add order number
            columnlist.forEach(function (item, index) {
                if (item.$$hashKey) {
                    delete item.$$hashKey;
                }
                item.Order = index;

                // map to backend data format
                var result = angular.copy(item);
                if (!columnMap[item.columnObject.name]) {
                    columnMap[item.columnObject.name] = [];
                    delete result.columnObject;
                    columnMap[item.columnObject.name].push(result);
                } else {
                    columnMap[item.columnObject.name].push(result);
                }

            });

            searchService.saveScreenCandidatesDisplayColumns(columnMap)
                .then(function (data) {

                    mapCustomColumn(data);
                    mapFilterOption(data);
                    // var customColumns = generateCustomColumn();
                    var customColumns = JSON.stringify(data);

                    if ($rootScope.cmIdList && $rootScope.cmIdList.length > 0) {
                        return placementCandidateService.getCMListByVacancyIdWithCMId(vacancyId, customColumns, $rootScope.cmIdList);
                    }

                    return placementCandidateService.getCMListByVacancyId(vacancyId, customColumns);
                }, function (data) {
                    console.log("error on save display columns", data);
                    busyLoader.end();
                })
                .then(function(data){
                    $scope.allVacancyCandidateList = sortService.sort(data, $scope.propertyName, $scope.reverse);
                    $scope.vacancyCandidateList = angular.copy($scope.allVacancyCandidateList);

                    $scope.$broadcast("RESET_FILTER");

                    resetSorting();

                    applyView();

                    busyLoader.end();

                });

            // required by the directive
            return true;
        };

        function gotoSalesForceHomePage() {
            //$window.location.href = "/";
        	helperService.locateTo($window,"/");
        }

        function changeView () {
             if (nextListView === $scope.viewOption.New) {
                $scope.switchToNewView();
            } else if (nextListView === $scope.viewOption.All) {
                $scope.switchToAllView();
            } else if (nextListView === $scope.viewOption.Shortlist) {
                $scope.switchToShortlistView();
            }
        }
        
        function addOriginalStatusPickList(client){
	        $scope.candidateStatusPicklist.forEach(function(item) {
	            if (item.value === client.Status__c) {
	                client.candidateStatusPicklists = item.picklist;
	                if (client.candidateStatusPicklists.length === 0) {
	                    client.Candidate_Status__c = "";
	                }
	            }
	        });
	    }

        function discardViewChanges () {
            for(var i = 0; i < $scope.originClientsFiltered.length; i++) {
                for(var j = 0; j < $scope.allVacancyCandidateList.length; j++) {
                    if ($scope.allVacancyCandidateList[j].Id === $scope.originClientsFiltered[i].Id) {
                        $scope.allVacancyCandidateList[j].Status__c = $scope.originClientsFiltered[i].Status__c;

                        if($scope.allVacancyCandidateList[j].Recruiter_Notes__c !== undefined && $scope.originClientsFiltered[i].Recruiter_Notes__c) {
                            $scope.allVacancyCandidateList[j].Recruiter_Notes__c = $scope.originClientsFiltered[i].Recruiter_Notes__c;
                        } else if ($scope.allVacancyCandidateList[j].Recruiter_Notes__c && $scope.originClientsFiltered[i].Recruiter_Notes__c === undefined) {
                            delete $scope.allVacancyCandidateList[j].Recruiter_Notes__c;
                        }

                        if($scope.originClientsFiltered[i].Candidate_Status__c) {
                        	addOriginalStatusPickList($scope.allVacancyCandidateList[j]);
                            $scope.allVacancyCandidateList[j].Candidate_Status__c = $scope.originClientsFiltered[i].Candidate_Status__c;
                        } else if ($scope.allVacancyCandidateList[j].Candidate_Status__c && $scope.originClientsFiltered[i].Candidate_Status__c === undefined) {
                            delete $scope.allVacancyCandidateList[j].Candidate_Status__c;
                            $scope.allVacancyCandidateList[j].candidateStatusPicklists = null;
                        } else{
                            $scope.allVacancyCandidateList[j].candidateStatusPicklists = null; 
                        }

                        if($scope.allVacancyCandidateList[j].Rating__c && $scope.originClientsFiltered[i].Rating__c) {
                            $scope.allVacancyCandidateList[j].Rating__c = $scope.originClientsFiltered[i].Rating__c;
                        }

                    }
                }
            }
        }

        $scope.modalInstance = undefined;
        $scope.confirmUnsavedChangedForCustomColumns = function (columnlist, updatedObjectList) {

            var confirmed = false;
            scope.$on("CONFIRM_SAVED", function() {
                if(confirmed) {
                    scope.updateDisplayColumn(columnlist, updatedObjectList);
                }
                confirmed = false;
            });
            scope.$on("CONFIRMED_DISCARD", function() {
                if(confirmed) {
                    scope.updateDisplayColumn(columnlist, updatedObjectList);
                }
                confirmed = false;
            });

            $scope.modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: function ($scope, $rootScope, $uibModalInstance) {

                    $scope.cancelChangesBtn = function () {
                        $rootScope.isCloseApp = false;
                        $rootScope.$emit("CHANGE_PAGE_CANCEL", "EMIT");
                        $uibModalInstance.dismiss('cancel');
                    };

                    $scope.discardChangesBtn = function () {
                        confirmed = true;
                        $rootScope.$emit("CHANGE_PAGE_DISCARD", "EMIT");
                        $uibModalInstance.dismiss('cancel');
                    };

                    $scope.saveChangesBtn = function () {
                        confirmed = true;
                        $rootScope.$emit("CONFIRM_SAVE_CHANGE", "EMIT");
                        $uibModalInstance.dismiss('cancel');
                    };
                }

            });
        };

        $rootScope.$on("CHANGE_PAGE_DISCARD", function (event, args) {
            $rootScope.confirmUpdateFlag = false;

            if ($scope.uiState.isChangeView) {
                discardViewChanges();
                changeView();
            }
        });

        $rootScope.$on('CONFIRM_SAVE_CHANGE', function () {

            $scope.confirmBtn();
            if ($scope.uiState.isChangeView) {

                $rootScope.confirmUpdateFlag = false;
                changeView();
            }
        });

        /**** manage other vacancy search ****/

        $rootScope.addOtherVacancyBtn = function() {
            console.log("addShortListToOtherVacancy");

            var selectedCandidateManagement = [];
            for (var i = 0; i < $scope.currentPageCandidateList.length; i++) {
                var item = $scope.currentPageCandidateList[i];
                if (item.isSelect && item.Candidate__r) {
                    selectedCandidateManagement.push(item.Candidate__r);
                }
            }

            // nave to other vacancy tap
            var addToOtherVacancyDialog = $uibModal.open({
                animation: true,
                templateUrl: 'addToOtherVacancy/addToOtherVacancyView.html',
                controller: 'AddToOtherVacancyController',
                size: 'lg',
                backdrop: false,
                resolve: {
                    shortList: function () {
                        return selectedCandidateManagement;
                    }
                }
            });
            addToOtherVacancyDialog.result.then(function (data) {
                console.log("other vacancy is closed ");
            }, function () {
                console.log('other vacancy is dismissed');
            });

        };

        init();

    }

    angular.module('CTC.People.ScreenCandidates')
        .controller('ScreenCandidatesController', ['$scope', '$rootScope', '$window', '$timeout', '$uibModal', 'busyLoader', 'vacancyService', 'placementCandidateService', 'searchService', 'Notification','redirectService', "urlPrefixService","orderByFilter", "sortService", "helperService",screenCandidatesController]);

})();
