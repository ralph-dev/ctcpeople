
(function () {
    "use strict";
    function contactService($q, RemoteModelObject) {
        var contactExtension = RemoteModelObject.create("Contact", {});

        function getCompactPeopleInfo(vacancyId) {

            var deferred = $q.defer();
            contactExtension.actions.getCompactPeopleInfo(vacancyId)
                .then(function (data) {
                    deferred.resolve(data);
                }, function (error) {
                    deferred.reject(error);
                });
            return deferred.promise;

        }

        function savePeopleInfo(contact) {

            var deferred = $q.defer();
            contactExtension.actions.savePeopleInfo([contact])
                .then(function (data) {
                    deferred.resolve(data);
                }, function (error) {
                    deferred.reject(error);
                });
            return deferred.promise;

        }

        return {
            getCompactPeopleInfo: getCompactPeopleInfo,
            savePeopleInfo: savePeopleInfo
        };
    }

    angular.module("CTC.People.ScreenCandidates").service("contactService", ["$q", "RemoteModelObject", contactService]);
}());
