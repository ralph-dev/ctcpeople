/**
 * manage placement candidate related remote action calls
 */
(function(){
    "use strict";
    function placementCandidateService($q, RemoteModelObject){
        var placementCandidateExtension = RemoteModelObject.create("PlacementCandidate", {});
        var HelperExtension = RemoteModelObject.create("Helper", {});

        /**
         * get placment candidate by vacancy id
         * @param vacancyId String Id
         * @returns {Function} promise
         */
        function getCMListByVacancyId(vacancyId, customFields) {

            var deferred = $q.defer();
            placementCandidateExtension.actions.getCMListByVacancyId(vacancyId, customFields)
                .then(function (data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        }

        function getCMListByVacancyIdWithCMId(vacancyId, customFields, cmListId) {

            var deferred = $q.defer();
            placementCandidateExtension.actions.getCMListByVacancyIdWithCMId(vacancyId, customFields, cmListId)
                .then(function (data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        }

        function createRecords(recordList, vacancyList) {

            var deferred = $q.defer();
            placementCandidateExtension.actions.createRecords(recordList, vacancyList)
                .then(function (data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        }

        function updateRecords(recordList) {

            var deferred = $q.defer();
            placementCandidateExtension.actions.updateRecords(recordList)
                .then(function (data) {
                    deferred.resolve(data);
                }, function (error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        function getTop5Records(candidateId) {

            var deferred = $q.defer();
            placementCandidateExtension.actions.getTop5Records(candidateId)
                .then(function (data) {
                    deferred.resolve(data);
                });
            return deferred.promise;

        }

        function getPicklistMapBySObjectAndFieldName (sObjectName, fieldName) {

            var deferred = $q.defer();

            HelperExtension.actions.getPicklistMapBySObjectAndFieldName(sObjectName, fieldName)
                .then(function (data) {
                    deferred.resolve(data);
                });

            return deferred.promise;
        }

        return {
            getCMListByVacancyId: getCMListByVacancyId,
            getCMListByVacancyIdWithCMId: getCMListByVacancyIdWithCMId,
            getTop5Records: getTop5Records,
            getPicklistMapBySObjectAndFieldName: getPicklistMapBySObjectAndFieldName,
            updateRecords: updateRecords,
            createRecords: createRecords
        };
    }
    angular.module("CTC.People.ScreenCandidates").service("placementCandidateService", ["$q", "RemoteModelObject", placementCandidateService]);
}());
