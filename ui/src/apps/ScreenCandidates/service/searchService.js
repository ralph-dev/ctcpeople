/***
 * searchService: manage retrieve criteria fields and process search query
 */
(function (){
    "use strict";
    function searchService($rootScope, $q, $filter, RemoteModelObject){

        var searchExtension =  RemoteModelObject.create("PeopleSearch", {});
        var StyleCategoryExtension = RemoteModelObject.create("StyleCategory", {});
        var PlacementCandidateExtension = RemoteModelObject.create("PlacementCandidate", {});
        var taskExtension = RemoteModelObject.create("Task", {});
        var helperExtension = RemoteModelObject.create("Helper", {});
        var ScreenCandidatesExtension = RemoteModelObject.create("ScreenCandidates", {});
        var ContactExtension = RemoteModelObject.create("Contact", {});

        var USER_PERMISSION;
        var FIELDS_LIST = null; // store list of fields available for search
        var FIELD_TYPE_LIST = null; // store fields type definitions from backend
        var fieldTypeEnum = ""; //searchObjectsFactory.FieldTypeEnum();
        var SEARCH_RESULT = null;
        /***** search template *********/
        var CURRENT_SEARCH_TEMPLATE = "";//searchObjectsFactory.defaultSearchTemplate();
        var CURRENT_SEARCH_QUERY = null;
        var EDIT_SEARCH_FLAG = false;



        function getUserPermission(){
            var deferred = $q.defer();
            var result;
            if(USER_PERMISSION){
                result = angular.copy(USER_PERMISSION);
                deferred.resolve(result);
            }else{
                helperExtension.actions.getUserPermission()
                    .then(function(data){
                        USER_PERMISSION = data;
                        result = angular.copy(USER_PERMISSION);
                        deferred.resolve(result);
                    })
            }
            return deferred.promise;
        }

        function isBulkEmailEnabled (){
            return getUserPermission()
                .then(function(data){
                    return data.isBulkEmailEnabled;
                });
        }

        function isBulkSMSEnabled(){
            return getUserPermission()
                .then(function(data){
                    return data.isBulkSMSEnabled;
                });
        }
        /**
         * add fieldTypeObject to the fields based on there type value
         * @param fieldsList {Array} list of fields
         * @param typeList {Array} list of supported fieldTypeObject
         */
        function precessFieldType(fieldsList, typeList) {

            function setFieldTypeObject(fileType, fieldTypeList) {
                var resultType = {};
                for (var i = 0; i < fieldTypeList.length; i++) {
                    if (fieldTypeList[i].type === fileType) {
                        resultType = fieldTypeList[i];
                        break;
                    }
                }
                return resultType;
            }

            var resultFieldsList = fieldsList.map(function (field) {
                field.typeObject = setFieldTypeObject(field.type, typeList);
                return field;
            });
            return resultFieldsList;
        }

        /**
         * get the list of fields type and thier supported operators
         * @returns {Array} list of FieldTypeObject
         */
        function getFieldTypeList() {
            var deferred = $q.defer();
            if (FIELD_TYPE_LIST && FIELD_TYPE_LIST.length > 0) {
                deferred.resolve(FIELD_TYPE_LIST);
            } else {
                searchExtension.actions.getFieldTypeList()
                    .then(function (data) {
                        FIELD_TYPE_LIST = data;
                        deferred.resolve(FIELD_TYPE_LIST);
                    });
            }
            return deferred.promise;
        }


        function getRawFieldsList() {
            return searchExtension.actions.getContactCriteriaList()
                .then(function (data) {
                    return data;
                });
        }

        /**
         * get the list of metaData fields from the backend add typeObject (contains field type definition)
         * @returns {function} list of FieldObject
         */
        function getContactCriteriaList() {
            var deferred = $q.defer();
            if (FIELDS_LIST && FIELDS_LIST.length > 0) {
                deferred.resolve(FIELDS_LIST);
            } else {
                getFieldTypeList()
                    .then(function (fieldTypeList) {
                        getRawFieldsList()
                            .then(function (data) {
                                FIELDS_LIST = precessFieldType(data, fieldTypeList);
                                deferred.resolve(FIELDS_LIST);
                            });
                    });
            }

            return deferred.promise;
        }

        /**
         * create a list of criteria fields based on the default fields list from backend
         */
        function getDefaultCriteriaFieldList() {
            var defaultList = [];
            var deferred = $q.defer();
            getRawFieldsList()
                .then(function (data) {
                    defaultList = data.map(function (field) {
                        var criteria = searchObjectsFactory.createCriteria(field.apiName, field.type);
                        return criteria;
                    });
                    deferred.resolve(defaultList);
                });
            return deferred.promise;
        }

        /**
         * get field object based on apiName
          * @param fieldName apiName
         * @param fieldsList list of fields
         * @returns {Object} fieldObject
         */
        function setFieldObject(fieldName, fieldsList) {
            var fieldObject = {};
            for (var i = 0; i < fieldsList.length; i++) {
                if (fieldsList[i].apiName === fieldName) {
                    fieldObject = fieldsList[i];
                    break;
                }
            }
            return fieldObject;
        }

        /**
         * get the list search criteria based on loaded admin config than process and add fieldObject to each criteria field
         * @returns {Function} promise contains list of search criteria fields
         */
        function getSearchCriteriaTemplate() {
            var deferred = $q.defer();
            var criteriaTemplate =[];

            getDefaultCriteriaFieldList()
                .then(function (data) {
                    criteriaTemplate = data;
                    return getContactCriteriaList();
                })
                .then(function (fieldList) {
                    // MAP fields to field list
                    var criteriaList = criteriaTemplate.map(function (criteria) {
                        criteria.fieldObject = setFieldObject(criteria.field, fieldList);
                        /** set default operator if type is checkbox **/
                        // code was updated to allow user to choice operator
                        if (criteria.type === fieldTypeEnum.BOOLEAN) {
                            criteria.operator = "eq";
                        }
                        /** set default operator if type is date (aka dateRange) **/
                        //if (criteria.type === fieldTypeEnum.DATE || criteria.type === fieldTypeEnum.DATETIME ) {
                        //    criteria.operator = "between";
                        //}
                        return criteria;
                    });
                    deferred.resolve(criteriaList);
                });
            return deferred.promise;
        }

        /**
         * loop through search result and stringify values before send to backend
         * @param queryObject SearchQuery object
         * @returns {SearchQuery} with criteria values stringify
         */
        function stringifyQuery(queryObject){
            var result = angular.copy(queryObject);
             result.criteria.forEach(function(item){
                 item.values = JSON.stringify(item.values);
             });
            return result;
        }

        /**
         * process and submit search query to backend, save a copy of
         * @param resumeKeywords
         * @param criteriaList
         * @param skills
         * @param availability
         * @returns {Function} search result
         */
        function submitSearchQuery(resumeKeywords, criteriaList, skills, availability) {
            var deferred = $q.defer();
            // store search object
            CURRENT_SEARCH_QUERY = null;
            SEARCH_RESULT = null;
            CURRENT_SEARCH_QUERY = searchObjectsFactory.createSearchQuery(resumeKeywords, criteriaList, skills, availability);
            if (CURRENT_SEARCH_QUERY.resumeKeywords == null && CURRENT_SEARCH_QUERY.criteria.length < 1 && CURRENT_SEARCH_QUERY.skillsSearch.query.length < 1) {
                deferred.reject("empty query. data error");
            } else {

                var submitQuery = stringifyQuery(CURRENT_SEARCH_QUERY);
                if( submitQuery.skillsSearch && submitQuery.skillsSearch.query.length < 1){
                    submitQuery.skillsSearch = null;
                }
                console.log("search query", submitQuery);
                searchExtension.actions.searchCandidate(submitQuery)
                    .then(function (data) {
                        SEARCH_RESULT = angular.copy(data);
                        deferred.resolve(data);
                    }, function(data){
                        deferred.reject(data);
                    });
            }

            return deferred.promise;

        }

        /**
         * re-submit saved search and return result
         * @returns {Function} promise search result
         */
        function reSubmitSearch(){
            var deferred =  $q.defer();
            var submitQuery = stringifyQuery(CURRENT_SEARCH_QUERY);
            console.log("search query", submitQuery);
            searchExtension.actions.searchCandidate(submitQuery)
                .then(function (data) {
                    SEARCH_RESULT = angular.copy(data);
                    deferred.resolve(data);
                });
            return deferred.promise;
        }



        /**
         * get the list of search template saved for current user, then add default empty template to the list
         * @return (promise) list of template with Default added;
         */
        function getSearchTemplateList(){
            var result = [];
            var defaultTemplate = searchObjectsFactory.defaultSearchTemplate();
           return StyleCategoryExtension.actions.getSaveSearchRecordsByUser().then(function(data){
                if(Array.isArray(data)){
                    result = data;
                }
                result.unshift(defaultTemplate);
                return result;

            });
        }

        /**
         * process criteria object to capture legacy code and convert string to js object
         * @param template
         */
        function processSavedSearchCriteria(template) {
            if (template) {
                if (template.criteria) {
                    template.criteria.forEach(function (item) {
                        if (item.values && typeof item.values == "string") {
                            switch (item.type) {
                                case fieldTypeEnum.PICKLIST:
                                case fieldTypeEnum.MULTIPICKLIST:
                                case fieldTypeEnum.BOOLEAN:
                                case fieldTypeEnum.NUMBER:
                                case fieldTypeEnum.DECIMAL:
                                case fieldTypeEnum.DOUBLE:
                                case fieldTypeEnum.INTEGER:
                                case fieldTypeEnum.CURRENCY:
                                case fieldTypeEnum.PERCENT:
                                    item.values = JSON.parse(item.values);
                                    break;
                                case fieldTypeEnum.DATE:
                                case fieldTypeEnum.DATETIME:
                                    item.values = JSON.parse(item.values);
                                    if (item.values.toDate) {
                                        item.values.toDate = new Date(item.values.toDate);
                                    }
                                    if (!item.values.toDate) {
                                        item.values.toDate = null;
                                    }
                                    if (item.values.fromDate) {
                                        item.values.fromDate = new Date(item.values.fromDate);
                                    }
                                    if (!item.values.fromDate) {
                                        item.values.fromDate = null;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                        // if new template and date type need to convert the values to date object
                        if (item.values && (item.type === fieldTypeEnum.DATE || item.type === fieldTypeEnum.DATETIME)) {
                            if (item.values.toDate) {
                                item.values.toDate = new Date(item.values.toDate);
                            }
                            if (!item.values.toDate) {
                                item.values.toDate = null;
                            }
                            if (item.values.fromDate) {
                                item.values.fromDate = new Date(item.values.fromDate);
                            }
                            if (!item.values.fromDate) {
                                item.values.fromDate = null;
                            }
                        }
                    }); // end template.criteria.forEach()
                }
                if (template.skillsSearch) {
                    var skillSearchObject = searchObjectsFactory.createSkillsSearchObject();
                    var temp = angular.copy(template.skillsSearch);
                    template.skillsSearch = skillSearchObject;
                    if (temp.query) {
                        template.skillsSearch.query = temp.query;
                    }
                    if (temp.verifiedSkills) {
                        template.skillsSearch.verifiedSkills = temp.verifiedSkills;
                    }
                }
            }

            return template;
        }

        /**
         * set the current search template and current search query.
         * use before loading form with new data.
         * @param searchTemplate search template object
         * @return (function) promise selected template Id.
         */
        function setSearchTemplate(searchTemplate){
            // set the CURRENT_TEMPLATE_ID
            CURRENT_SEARCH_TEMPLATE = searchObjectsFactory.defaultSearchTemplate(searchTemplate);
            // set CURRENT_SEARCH_QUERY to searchTemplate.ascomponents__c (parse to object)
            if(typeof searchTemplate.ascomponents__c === "string"){
                CURRENT_SEARCH_QUERY = processSavedSearchCriteria(JSON.parse(searchTemplate.ascomponents__c));
                // store converted object to maintain legacy stored data
                CURRENT_SEARCH_TEMPLATE.ascomponents__c = processSavedSearchCriteria(JSON.parse(searchTemplate.ascomponents__c));
            }else{
                CURRENT_SEARCH_QUERY = processSavedSearchCriteria(searchTemplate.ascomponents__c);
               // store converted object to maintain legacy stored data
                CURRENT_SEARCH_TEMPLATE.ascomponents__c = processSavedSearchCriteria(searchTemplate.ascomponents__c);
            }

            var deferred = $q.defer();
            deferred.resolve(searchTemplate);
            return deferred.promise;

        }


        function getCurrentTemplate(){
            return angular.copy(CURRENT_SEARCH_TEMPLATE);
        }

        function clearCurrentTemplate(){
            CURRENT_SEARCH_TEMPLATE= searchObjectsFactory.defaultSearchTemplate();
        }

        /**
         * create a string of the search query object
         * @return (string)
         */
        function captureSearchQuery(resumeKeywords, criteriaList, skills,  availability){
            var captureQuery = null;
            var captureQueryString = "";
            captureQuery = searchObjectsFactory.createSearchQueryForTemplate(resumeKeywords, criteriaList, skills, availability);
            captureQueryString = JSON.stringify(captureQuery);
            return captureQueryString;
        }

        /**
         * save a new search template
         * @param searchTemplate
         * @returns {function} promise first object in the returned array
         */
        function createSearchTemplate(searchTemplate){
            if(searchTemplate.Id){
                delete searchTemplate.Id;
            }
            return StyleCategoryExtension.actions.createRecords([searchTemplate])
                .then(function(data){
                   return data[0];
                });

        }

        function updateSearchTemplate(searchTemplate){
            console.log("tempalte", searchTemplate);
            return StyleCategoryExtension.actions.updateRecords([searchTemplate])
                .then(function(data){
                    return data[0];
                });
        }

        /**
         * delete a saved tempalte from record
         * @param searchTemplate
         * @returns {function} Promise the deleted object
         */
        function deleteSearchTemplate(searchTemplate){
            var deleteTemplate = searchObjectsFactory.defaultSearchTemplate(searchTemplate);
            return StyleCategoryExtension.actions.deleteRecords([deleteTemplate]).then(function(data){
               // return the first element of the returned array
                return data[0];
            });
        }

        /**
         * return a copy of the search result
         * @returns {Function} promise
         */
        function getSearchResult(){
            var deferred = $q.defer();
            var result = angular.copy(SEARCH_RESULT);
            deferred.resolve(result);
            return deferred.promise;
        }

        function getSearchQuery(){
            return angular.copy(CURRENT_SEARCH_QUERY);
        }

        function setEditSearchFlag(){
            EDIT_SEARCH_FLAG = true;
        }

        function loadSearchTemplate() {
            var deferred = $q.defer();
            var searchTemplate = searchObjectsFactory.createSearchQuery();
            if (EDIT_SEARCH_FLAG || CURRENT_SEARCH_TEMPLATE.Id !== -1) {
                /**
                 * load current search data in to the search template
                 * TODO: add skills and availability later
                 * **/
                searchTemplate.resumeKeywords = CURRENT_SEARCH_QUERY.resumeKeywords;
                if(CURRENT_SEARCH_QUERY.skillsSearch){
                    if(CURRENT_SEARCH_QUERY.skillsSearch.query){
                        searchTemplate.skillsSearch.query = CURRENT_SEARCH_QUERY.skillsSearch.query;
                    }
                    if(CURRENT_SEARCH_QUERY.skillsSearch.verifiedSkills){
                        searchTemplate.skillsSearch.verifiedSkills = CURRENT_SEARCH_QUERY.skillsSearch.verifiedSkills;
                    }
                }

                /* TODO: update later for now we load default template than fill the values from saved search. when multiple template allowed we will only need to load saved search criteria. */
                getSearchCriteriaTemplate()
                    .then(function (data) {
                        var currentCriteria = angular.copy(CURRENT_SEARCH_QUERY.criteria);
                        searchTemplate.criteria = data.map(function (item) {
                            for (var i = 0; i < currentCriteria.length; i++) {
                                if (item.field === currentCriteria[i].field) {
                                    item.operator = currentCriteria[i].operator;
                                    item.values = currentCriteria[i].values;
                                    break;
                                }
                            }
                            return item;
                        });
                        deferred.resolve(searchTemplate);
                    });
            } else {
                /* TODO: manage loading template from backend in the future */
                // loadTemplate
                getSearchCriteriaTemplate()
                    .then(function (data) {
                        searchTemplate.criteria = data;
                        deferred.resolve(searchTemplate);
                    });

            }
            EDIT_SEARCH_FLAG = false;
            return deferred.promise;
        }

        /************** search result functions *************************/
        /**
         * get the user display column or the default list of column, with static field set to be at the beginning
         * @returns {promise} list of columns for display grid
         */
        function getContactDisplayColumns(){
            var deferred = $q.defer();
            var result = [];
            // convert json string into js object
            searchExtension.actions.getContactDisplayColumns()
                .then(function(data){
                    if(!data || data === null){
                        searchExtension.actions.getContactDefaultDisplayColumn()
                            .then(function(defaultColumns){
                                if(typeof(defaultColumns) === "string") {
                                    result = JSON.parse(defaultColumns);
                                }else{
                                    result = defaultColumns;
                                }

                                deferred.resolve(result);
                            });
                    }else {
                        if(typeof(data) === "string") {
                            result = JSON.parse(data);
                        }else{
                            result = data;
                        }
                        deferred.resolve(result);
                    }
                });
            return deferred.promise;
        }

        /**
         * get a list of field as display column option
         * @returns {promise} list of DisplayColumn objects
         */
        function getContactFieldsList(){
            var result =[];
            return searchExtension.actions.getContactFieldsList()
                .then(function(data){
                    if(typeof(data) === "string") {
                      result = JSON.parse(data);
                    }else{
                        result = data;
                    }
                    result =  $filter('orderBy')(result, 'ColumnName');
                    result.unshift({ColumnName:" --Select Column--"});
                return result;
            });
        }

        /**
         * save display column based on UI config data will be converted to string before update
         * @param columnList list of columns from UI
         * @returns {*}
         */
        function updateDisplayColumn(columnList){
            var result=[];
            var displayColumn = JSON.stringify(columnList); // convert data to a JSON string before sending
            return searchExtension.actions.saveContactDisplayColumn(displayColumn)
                .then(function(data){
                    if(typeof(data) === "string"){
                       result = JSON.parse(data);
                    }else{
                       result = data;
                    }

                    return result;
                });
        }

        /**
        *
        * @param contactsList list of contact Id
        * @returns url
        */
        function generateBulkEmailURI(contactsList) {
            var deferred = $q.defer();
            searchExtension.actions.generateBulkEmailURI(contactsList)
                .then(function(data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        }

        function generateBulkEmailURIWithTargetObjectsMap(contactsList, targetObjects) {
            var deferred = $q.defer();
            searchExtension.actions.generateBulkEmailURIWithTargetObjectsMap(contactsList, targetObjects)
                .then(function(data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        }


        //****************************************
        /**
         *
         * @param contact Id
         * @return contact object
         */
        function getPeopleInfo(contactId) {
            var deferred = $q.defer();
            searchExtension.actions.getPeopleInfo(contactId)
                .then(function(data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        }

        /**
         *
         * @param contact Id
         * @return contact resume
         */
        function getContactLongTextAreaFields(contactId) {
            var deferred = $q.defer();
            searchExtension.actions.getContactLongTextAreaFields(contactId)
                .then(function(data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        }

        /**
         *
         * @param contact Id
         * @return list of management record
         */
        function getTop5Records(contactId) {
            var deferred = $q.defer();
            PlacementCandidateExtension.actions.getTop5Records(contactId)
                .then(function(data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        }

        /**
         *
         * @param contact Id
         * @return list activities history
         */
        function getTop5TasksByContactId(contactId) {
            var deferred = $q.defer();
            taskExtension.actions.getTop5TasksByContactId(contactId)
                .then(function(data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        }

        /**
         * get contact details from backend resume, history and management record
         * @param contactId
         * @returns {Function} promise result object contains {longTextField:{}, recordManagement:[],activitiesHistory:[]}
         */
        function getCandidateInfo(contactId){
            var deferred  = $q.defer();
            var result = {
                recordDetails:{},
                recordManagement:[],
                activitiesHistory:[]

            };
            ContactExtension.actions.getCompactPeopleInfo(contactId)
                .then(function(info){
                    result.recordDetails = info;
                    return taskExtension.actions.getTop5TasksByContactId(contactId);
                })
                .then(function(taskList){
                    result.activitiesHistory = taskList;
                    return PlacementCandidateExtension.actions.getTop5Records(contactId);
                })
                .then(function(managementList){
                    result.recordManagement = managementList;
                    deferred.resolve(result);
                });
            return deferred.promise;
        }

        /**
         * get contact details from backend resume, history and management record
         * @param contactId
         * @returns {Function} promise result object contains {longTextField:{}, recordManagement:[],activitiesHistory:[]}
         */
        function getContactDetail(contactId){
            var deferred  = $q.defer();
            var result = {
                recordDetails:{},
                longTextField:{},
                recordManagement:[],
                activitiesHistory:[]

            };
            searchExtension.actions.getPeopleInfo(contactId)
                .then(function(info){
                   result.recordDetails = info;
                    return searchExtension.actions.getContactLongTextAreaFields(contactId);
                })
                .then(function(data){
                    result.longTextField = data;
                    return taskExtension.actions.getTop5TasksByContactId(contactId);
                })
                .then(function(taskList){
                    result.activitiesHistory = taskList;
                    return PlacementCandidateExtension.actions.getTop5Records(contactId);
                })
                .then(function(managementList){
                    result.recordManagement = managementList;
                    deferred.resolve(result);
                });
            return deferred.promise;
        }

        function getCMFieldsList() {
            var deferred = $q.defer();

            PlacementCandidateExtension.actions.getCMFieldsList()
                .then(function(data){
                    deferred.resolve(data);
                },function(error){
                    deferred.reject(error);
                })

            return deferred.promise;  
        }

        function getObjectsLabelName () {
            var deferred = $q.defer();

            ScreenCandidatesExtension.actions.getObjectsLabelName()
                .then(function(data){
                    deferred.resolve(data);
                },function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function getScreenCandidatesDisplayColumns() {
            var deferred = $q.defer();

            ScreenCandidatesExtension.actions.getScreenCandidatesDisplayColumns()
                .then(function(data){
                    deferred.resolve(data);
                },function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function saveScreenCandidatesDisplayColumns(columnlist) {
            var deferred = $q.defer();

            columnlist = JSON.stringify(columnlist);
            ScreenCandidatesExtension.actions.saveScreenCandidatesDisplayColumns(columnlist)
                .then(function(data){
                    deferred.resolve(data);
                },function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

         /**
        *
        * @param contactsList list of contact Id
        * @returns url
        */
        function screenCandidateGenerateBulkEmailURI(contactsList, targetObjects) {
            var deferred = $q.defer();
            ScreenCandidatesExtension.actions.generateBulkEmailURIWithTargetObjectsMap(contactsList, targetObjects)
                .then(function(data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        }


        /********* end of search result function *******************/

        /**** public functions  ********/
        return{
            getContactCriteriaList:getContactCriteriaList,
            getFieldTypeList : getFieldTypeList,
            getSearchCriteriaTemplate: getSearchCriteriaTemplate,
            loadSearchTemplate : loadSearchTemplate,
            submitSearchQuery : submitSearchQuery,
            reSubmitSearch : reSubmitSearch,
            getSearchResult : getSearchResult,
            getSearchQuery : getSearchQuery,
            captureSearchQuery:captureSearchQuery,
            setEditSearchFlag: setEditSearchFlag,
            getSearchTemplateList : getSearchTemplateList,
            setSearchTemplate : setSearchTemplate,
            getCurrentTemplate : getCurrentTemplate,
            clearCurrentTemplate : clearCurrentTemplate,
            createSearchTemplate : createSearchTemplate,
            updateSearchTemplate : updateSearchTemplate,
            deleteSearchTemplate : deleteSearchTemplate,
            getContactDisplayColumns:getContactDisplayColumns,
            getContactFieldsList:getContactFieldsList,
            updateDisplayColumn : updateDisplayColumn,
            generateBulkEmailURI : generateBulkEmailURI,
            generateBulkEmailURIWithTargetObjectsMap : generateBulkEmailURIWithTargetObjectsMap,
            getContactDetail : getContactDetail,
            getUserPermission : getUserPermission,
            getPeopleInfo : getPeopleInfo,
            getContactLongTextAreaFields : getContactLongTextAreaFields,
            getTop5TasksByContactId : getTop5TasksByContactId,
            getTop5Records : getTop5Records,
            getCandidateInfo: getCandidateInfo,
            getCMFieldsList: getCMFieldsList,
            getObjectsLabelName: getObjectsLabelName,
            getScreenCandidatesDisplayColumns: getScreenCandidatesDisplayColumns,
            saveScreenCandidatesDisplayColumns: saveScreenCandidatesDisplayColumns,
            screenCandidateGenerateBulkEmailURI: screenCandidateGenerateBulkEmailURI

        };
    }
    angular.module("CTC.People.ScreenCandidates").service("searchService",["$rootScope", "$q", "$filter", "RemoteModelObject", searchService]);
}());
