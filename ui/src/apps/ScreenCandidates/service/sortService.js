(function() {
    "use strict";

    angular.module("CTC.People.ScreenCandidates").service("sortService", sortService);

    sortService.$inject = [];

    function sortService() {
        function evaluate(object, property) {
            if(!object || !property) {
                return;
            }

            var pos = property.indexOf(".");
            if(pos > 0) {
                var subProperty = property.substring(0, pos);
                var other = property.substring(pos + 1);
                var subObject = object[subProperty];
                if(!subObject) {
                    return subObject;
                }
                else {
                    return evaluate(subObject, other);
                }
            }
            else {
                return object[property];
            }
        }

        function sort(list, property, reverse) {
            return list.sort(function(a, b) {
                var ret = 0;
                var val_a = evaluate(a, property);
                var val_b = evaluate(b, property);
                if(angular.isNumber(val_a) && angular.isNumber(val_b)) {
                    ret = val_a - val_b;
                }
                else {
                    val_a = val_a || "";
                    val_b = val_b || "";
                    ret = val_a.toString().localeCompare(val_b.toString());
                }

                if(property === "Candidate__r.Name") {
                    return reverse ? -ret : ret;
                }
                else {
                    if(ret !== 0) {
                        return reverse ? -ret : ret;
                    }
                    else {
                        var name_a = evaluate(a, "Candidate__r.Name") || "";
                        var name_b = evaluate(b, "Candidate__r.Name") || "";
                        return name_a.localeCompare(name_b);
                    }
                }
            });
        }

        return {
            sort: sort,
        };
    }
})();
