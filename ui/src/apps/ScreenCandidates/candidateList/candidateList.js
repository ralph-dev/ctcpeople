function candidateListController($scope, $rootScope, $uibModal, orderByFilter, $filter, Notification, $window, redirectService, sortService, urlPrefixService) {
    var scope = $scope;

    // fix for angular issue with scope on a checkbox
    $scope.checkbox = {
        hasClientSelectAll: false
    };

    $scope.clientSelected = 0;
    $scope.hasClientSelected = false;
    $scope.selectedContactDetail = undefined;
    $scope.statusPicklist = [];
    $scope.candidateStatusPicklist = [];
    // $scope.updateStatus = undefined;
    // $scope.updateRecruiterNote = "";
    $scope.isEditingShortlist = false;
    // $scope.lastChecked = undefined;
    // $scope.searchResumeInput ="";
    $scope.searchKeyword = "";
    $scope.updateObj = {
        updateStatus: null,
        updateRecruiterNote: null
    };

    $scope.itemsPerPageOptions = [
        {value: 25, label: "Show 25"},
        {value: 50, label: "Show 50"},
        {value: 100, label: "Show 100"}
    ];

    $scope.filterArray = [];
    $scope.filterStrings = [];
    $scope.isFilterApplied = false;

    var isStatusChanged = false;
    var isCandidateStatusChanged = false;
    var fieldTypeEnum = {
        BOOLEAN: "BOOLEAN",
        DATE: "DATE",
        DATETIME: "DATETIME",
        REFERENCE: "REFERENCE",
        MULTIPICKLIST: "MULTIPICKLIST",
        PICKLIST : "PICKLIST",
        DOUBLE: "DOUBLE",
        NUMBER: "NUMBER",
        INTEGER: "INTEGER",
        DECIMAL: "DECIMAL",
        CURRENCY: "CURRENCY",
        PERCENT : "PERCENT"
    };

    var isLightningEnabled = false;
    var lightningMatch = "lightning.force.com";

    if ($window.location.href.indexOf(lightningMatch) > -1) {
        isLightningEnabled = true;
    }

    $scope.isExpanded = function(el) {
        return el.detailFlag && $scope.selectedContactDetail && el.Id === $scope.selectedContactDetail.Id;
    };

    //Get dependant picklist for each field
    function init() {
        // loadPickList();
        resetFilterArray();
        $scope.statusPicklist = getPicklistValues("Status__c");

        var candidateStatusController =  getControllerName("Candidate_Status__c");
        if (candidateStatusController.success) {
            $scope.statusPicklist.values.forEach(function(status) {
                var obj = {};
                var picklistObject = {};
                obj.value = status.value;
                picklistObject = getDependentValues("Candidate_Status__c",  status.value);
                obj.picklist = picklistObject.values;
                $scope.candidateStatusPicklist.push(obj);
            });
        }
    }

    $scope.isCandidateStatus = function (column) {
        return column.Field_api_name ===  namespace + 'Candidate_Status__c';
    }

    $scope.isRating = function (column) {
        return column.Field_api_name === namespace + 'Rating__c';
    }

    $scope.isCandidateStatusOrRating = function(column) {
        return $scope.isRating(column) || $scope.isCandidateStatus(column);
    }


    /*********************
     * PAGINATION
     * ********************
     */

    //use object to define the pagination value to resolve directive scope issue
    $scope.paginationObject = {
        currentPage: 1,
        previousPage: 1,
        itemsPerPage: 25,
        lastPage: 1,
        lastItemsPerPage: 25,
    };

    $scope.clientsFiltered = [];
    $scope.hasPagination = false;
    $scope.maxSize = 5;

    $scope.$on("CHANGE_VIEW", function(){
        if ($rootScope.confirmUpdateFlag) {
            $scope.confirmUnsaveChanges();
        }
    });

    $scope.$on("SWITCH_VIEW", function() {
        clearSelection();
    });

    function getFiltersAsString() {
        var data = {};
        $scope.filterStrings.forEach(function(str) {
            var items = str.split(" : ");
            var object = items[0];
            var field = items[1];
            var fields = data[object];
            fields = fields || [];
            field = "'" + field + "'";
            if(fields.indexOf(field) < 0) {
                fields.push(field);
            }
            data[object] = fields;
        });

        var str = "";
        Object.keys(data).forEach(function(key) {
            var fields = data[key];
            var part = key + ": ";
            if(fields.length >= 3) {
                part += fields.slice(0, fields.length - 1).join(", ") + " and " + fields[fields.length - 1];
            }
            else {
                part += fields.join(" and ");
            }

            if(str) {
                str += " AND ";
            }

            str += part;
        });

        return str;
    }

    $scope.getFiltersAsString = getFiltersAsString;

    function setCurrentPageItems() {
        // If it's not edit shortlist in contact detail or in shortlist view, refresh selected contact detail
        if ($scope.isShortlistView || !$scope.isEditingShortlist) {
            $scope.selectedContactDetail = undefined;
        } else {
            $scope.isEditingShortlist = false;
        }

        $rootScope.confirmUpdateFlag = false;


        // Business logic, when change page clear selection in case clients forget what they select before.
        clearSelection();

        var begin = (($scope.paginationObject.currentPage - 1) * $scope.paginationObject.itemsPerPage);
        var end = begin + $scope.paginationObject.itemsPerPage;

        // Shallow copy of $scope.clients
        $scope.clientsFiltered = $scope.clients.slice(begin, end);
        $scope.originClientsFiltered = angular.copy($scope.clientsFiltered);
        $scope.currentPageCandidateList = $scope.clientsFiltered;

        if ($scope.searchKeyword && $scope.searchKeyword !== "") {
            // searchCandiateListResume();
            searchCandiateListResume();
            // PC.EnhancedMassRating.searchAndHighlight($scope.searchKeyword);
        }

        $scope.paginationObject.lastPage = $scope.paginationObject.currentPage;
        $scope.paginationObject.lastItemsPerPage = $scope.paginationObject.itemsPerPage;
    }

    $scope.setCurrentPageItems = setCurrentPageItems;

    $scope.$watch('paginationObject.currentPage', function (newValue, oldValue) {

        if (newValue !== oldValue) {
            $scope.paginationObject.previousPage = oldValue;
        }

    });

    $scope.$watch('uiState.listState', function (newValue, oldValue) {

        if (newValue !== oldValue) {
            $scope.paginationObject.currentPage = 1;
        }

    });

    /*******************
     * Filter function
     *******************/

    function resetFilterArray () {
        $scope.filterArray = [];
        for (var i = 0; i < 6; i++) {
            var tempObject = {};
            tempObject.fieldName = null;
            tempObject.contains = "";
            $scope.filterArray.push(tempObject);
        }
    }

    function customFilter() {

        var filteredItems = $scope.clients;
        var filterArray =  $scope.filterArray.filter(function(filter) {
            if (filter.contains === "" || filter.fieldName === null ){
                return false;
            }

            return true;
        });

        //2 dimenional array to store filters
        var filterArraySorted= [];
        //To show result
        $scope.filterStrings = [];

        //loop through filterArray to check if there are same name filter,
        //insert to the same location,
        //else insert to the end of filterArraySorted
        angular.forEach(filterArray, function(filter) {

            var insertIndex = 0;
            var isMatch = false;

            for (var i = 0; i < filterArraySorted.length; i++) {
                var subArray = filterArraySorted[i];
                for (var j = 0; j < subArray.length; j++) {
                    if (subArray[j].fieldName.fieldName === filter.fieldName.fieldName) {
                        isMatch = true;
                        insertIndex = i;
                    }
                }
            }

            if (isMatch) {
                filterArraySorted[insertIndex].push(filter);
            } else {
                filterArraySorted.push([filter]);
            }

            // var fieldNames = filter.fieldName.split(" : ");
            // $scope.filterStrings.push(fieldNames[1]);
            $scope.filterStrings.push(filter.fieldName.fieldName);
        });

        //loop through filterArraySorted to filter out items
        angular.forEach(filterArraySorted, function (subFilterArray) {

            filteredItems = filteredItems.filter(function(item) {
                return containsItem(item, subFilterArray);
            });
        });

        $scope.clients = filteredItems;
        setCurrentPageItems();
    }

    function containsItem(item, subFilterArray) {
        for (var i = 0; i < subFilterArray.length; i++) {
            var fieldAPIName = subFilterArray[i].fieldName.Field_api_name;
            var objectName = subFilterArray[i].fieldName.ObjectName;

            var booleanValue;
            var value = "";
            if (objectName === $scope.objectLabelNames["Candidate Management"]) {
                value = item[fieldAPIName] || "";
                if(subFilterArray[i].fieldName.fieldType === fieldTypeEnum.BOOLEAN) {
                    booleanValue = "true" === subFilterArray[i].contains.toLowerCase() ? true : false;
                    if(value == booleanValue) {
                        return true;
                    }
                }
                else if(subFilterArray[i].fieldName.fieldType === fieldTypeEnum.REFERENCE) {
                    var refAPIName = subFilterArray[i].fieldName.referenceFieldName;
                    if(refAPIName) {
                        value = item[refAPIName].Name;
                        if (value.toLowerCase().indexOf(subFilterArray[i].contains.toLowerCase()) >= 0) {
                            return true;
                        }
                    }
                }
                else {
                    if (value.toLowerCase().indexOf(subFilterArray[i].contains.toLowerCase()) >= 0) {
                        return true;
                    }
                }
            }else {
                if(item.Candidate__r && item.Candidate__r[fieldAPIName]) {
                    value = item.Candidate__r[fieldAPIName] || "";
                }

                if(subFilterArray[i].fieldName.fieldType === fieldTypeEnum.BOOLEAN) {
                    booleanValue = "true" === subFilterArray[i].contains.toLowerCase() ? true : false;
                    if(value == booleanValue) {
                        return true;
                    }
                }
                else if(subFilterArray[i].fieldName.fieldType === fieldTypeEnum.REFERENCE) {
                    var refAPIName = subFilterArray[i].fieldName.referenceFieldName;
                    if(refAPIName) {
                        value = item.Candidate__r[refAPIName].Name;
                        if (value.toLowerCase().indexOf(subFilterArray[i].contains.toLowerCase()) >= 0) {
                            return true;
                        }
                    }
                }
                else {
                    if (value.toLowerCase().indexOf(subFilterArray[i].contains.toLowerCase()) >= 0) {
                        return true;
                    }
                }
            }
        }

        return false;
    }


    // is vaild filter and clear invalid filter option
    function isValidFilter () {
        var isValid = false;

        $scope.filterArray.forEach(function(filter) {
            if (filter.contains !== "" && filter.fieldName !== null) {
                isValid = true;
            } else {
                filter.contains = "";
                filter.fieldName = null;
            }
        });

        return isValid;
    }

    $scope.showFilterModal = function(modal) {
        $scope.modalFilterArray = angular.copy($scope.filterArray);

        modal.show();
    };

    $scope.changePagination = function() {
        if ($rootScope.confirmUpdateFlag) {
            $scope.confirmUnsaveChangesForPagination();
        } else {
            setCurrentPageItems();
        }
    };

    $scope.applyFilter = function () {
        $scope.$modals['Filter'].hide();

        //if there are changes to confirm, show confirm dialog
        if ($rootScope.confirmUpdateFlag) {
            // $rootScope.$emit("SHOW_CONFIRM_POPUP");
            $scope.confirmUnsaveChangesForFilter();
        } else {
            $scope.filterArray = angular.copy($scope.modalFilterArray);

            if (isValidFilter()) {
                    $scope.isFilterApplied = true;
                } else {
                    $scope.isFilterApplied = false;
            }

            resetSorting();

            if ($scope.uiState.listState === "Shortlist") {
                $scope.clients =  $scope.allVacancyCandidateList.filter(function(candidate){
                    return candidate.Shortlist__c === true;
                });
            } else if ($scope.uiState.listState === "New"){
                $scope.clients =  $scope.allVacancyCandidateList.filter(function(candidate){
                    return candidate.Status__c === "New";
                });
            } else {
                $scope.clients = angular.copy($scope.allVacancyCandidateList);
            }

            if ($scope.isFilterApplied) {
                customFilter();
            }
        }
    };

    $scope.resetFilter = function () {
        // $scope.isFilterApplied = false;
        // resetFilterArray();
    };

    $scope.$on("RESET_FILTER", function() {
        $scope.isFilterApplied = false;
        resetFilterArray();
    });

    /*********************
     * Table selection
     * ********************
     */

    function resetClientTable() {
        $scope.checkbox.hasClientSelectAll = false;
        $scope.hasClientSelected = false;
        $scope.clientsFiltered = [];
        $scope.hasNoResults = true;
        $scope.hasPagination = false;
        $scope.clientSelected = 0;
    }

    function clearSelection() {

        $scope.clientSelected = 0;
        $scope.checkbox.hasClientSelectAll = false;
        $scope.hasClientSelected = false;
        angular.forEach($scope.clients, function (client, key) {
            client.isSelect = false;
        });

        // $scope.lastChecked = undefined;
    }

    function changeClientSelectedStatus() {
        //var dfd = $q.defer();

        var clientIsSelected = false;
        var hasAllSelect = true;

        $scope.clientSelected = 0;
        // angular.forEach($scope.clientsCopy, function (client, key) {
        angular.forEach($scope.clientsFiltered, function (client, key) {
            if (client.isSelect) {
                clientIsSelected  = true;
                $scope.clientSelected++;
            }

            if(!client.isSelect) {
                hasAllSelect = false;
            }
        });

        $scope.checkbox.hasClientSelectAll = hasAllSelect;
        $scope.hasClientSelected = clientIsSelected;

        resetBulkUpdateFileds();
        // dfd.resolve(clientIsSelected);
        // return dfd.promise;
    }

    // $scope.clientSelect = function ($index, $event) {

    //     if(typeof $scope.lastChecked !== 'undefined' && $event.shiftKey){
    //         for(i=$scope.lastChecked; i<=$index; i++){
    //             $scope.clientsFiltered[i].isSelect=true;
    //         }
    //     }
    //     $scope.lastChecked = $index;

    //     changeClientSelectedStatus();
    // };

    var selectedRowsIndexes = [];
    var unselectedRowIndexes = [];

    $scope.selectRow = function(event, rowIndex) {
        if (event.ctrlKey) {
            // changeSelectionStatus(rowIndex);
        } else if(event.shiftKey) {
            if (!$scope.clientsFiltered[rowIndex].isSelect) {
                unselectWithShift(rowIndex);
            } else {
                selectWithShift(rowIndex);
                angular.forEach(selectedRowsIndexes, function(index) {
                    $scope.clientsFiltered[index].isSelect = true;
                });
            }
        } else {
            if ($scope.isRowSelected(rowIndex)) {
                unselect(rowIndex);
                unselectedRowIndexes = [rowIndex];
            } else {
                selectedRowsIndexes = [rowIndex];
            }
        }

        changeClientSelectedStatus();
        //console.log(unselectedRowIndexes);
        //console.log(selectedRowsIndexes);
        // console.log(getSelectedRows());
        // console.log(getFirstSelectedRow());
    };

    function selectWithShift(rowIndex) {
        var lastSelectedRowIndexInSelectedRowsList = selectedRowsIndexes.length - 1;
        var lastSelectedRowIndex = selectedRowsIndexes[lastSelectedRowIndexInSelectedRowsList];
        var selectFromIndex = Math.min(rowIndex, lastSelectedRowIndex);
        var selectToIndex = Math.max(rowIndex, lastSelectedRowIndex);
        selectRows(selectFromIndex, selectToIndex);
    }

    function unselectWithShift(rowIndex) {
        var lastSelectedRowIndexInSelectedRowsList = selectedRowsIndexes.length - 1;
        var lastSelectedRowIndex = selectedRowsIndexes[lastSelectedRowIndexInSelectedRowsList];
        var unselectFromIndex = Math.min(unselectedRowIndexes[0],rowIndex);
        var unselectToIndex = Math.max(unselectedRowIndexes[0],rowIndex);
        unselectRowsFromClientsFiltered(unselectFromIndex, unselectToIndex);
    }

    function getSelectedRows() {
        var selectedRows = [];
        angular.forEach(selectedRowsIndexes, function(rowIndex) {
            selectedRows.push($scope.clientsFiltered[rowIndex]);
        });
        return selectedRows;
    }

    function getFirstSelectedRow() {
        var firstSelectedRowIndex = selectedRowsIndexes[0];
        return $scope.clientsFiltered[firstSelectedRowIndex];
    }

    function selectRows(selectFromIndex, selectToIndex) {
        for(var rowToSelect = selectFromIndex; rowToSelect <= selectToIndex; rowToSelect++) {
            select(rowToSelect);
        }
    }

    // function unselectRows(unselectFromIndex, unselectToIndex) {
    //     for(var rowToUnselect = unselectFromIndex; rowToUnselect <= unselectToIndex; rowToUnselect++) {
    //         unselect(rowToUnselect);
    //     }
    // }

    function unselectRowsFromClientsFiltered(unselectFromIndex, unselectToIndex) {
        for(var rowToUnselect = unselectFromIndex; rowToUnselect <= unselectToIndex; rowToUnselect++) {
            $scope.clientsFiltered[rowToUnselect].isSelect = false;
            unselect(rowToUnselect);
        }
    }

    function changeSelectionStatus(rowIndex) {
        if($scope.isRowSelected(rowIndex)) {
            unselect(rowIndex);
        } else {
            select(rowIndex);
        }
    }

    function select(rowIndex) {
        if(!$scope.isRowSelected(rowIndex)) {
            selectedRowsIndexes.push(rowIndex);
        }
    }

    function unselect(rowIndex) {
        var rowIndexInSelectedRowsList = selectedRowsIndexes.indexOf(rowIndex);
        var unselectOnlyOneRow = 1;
        selectedRowsIndexes.splice(rowIndexInSelectedRowsList, unselectOnlyOneRow);
    }

    function resetSelection() {
        selectedRowsIndexes = [];
    }

    $scope.isRowSelected = function(rowIndex) {
        return selectedRowsIndexes.indexOf(rowIndex) > -1;
    };



    $scope.clientSelectAll = function (currentPage) {

        if (!$scope.checkbox.hasClientSelectAll) {
            $scope.hasClientSelected = false;
            $scope.clientSelected = 0;
        } else {
            $scope.hasClientSelected = true;
            $scope.clientSelected = $scope.clientsFiltered.length;
        }

        angular.forEach($scope.clientsFiltered, function (client, key) {
            client.isSelect = $scope.checkbox.hasClientSelectAll;
        });

        // $scope.lastChecked = undefined;

    };

    // Account ID
    // $scope.getClientAccountIds = function () {
    //     var selectedClients = [];

    //     angular.forEach($scope.clientsFiltered, function (client, key) {
    //         if (client.isSelect) {
    //             selectedClients.push(client.Id);
    //         }
    //     });

    //     return selectedClients;
    // };


    // $scope.redirecToClientCard = function (clientId) {
    //     var namespace = urlPrefixService.getPrefix();
    //     var targetUrl = $window.location.origin + "/apex/" + namespace + "ClientCardPage?id=" + clientId + "";
    //     $window.open(targetUrl,"_Blank");
    // };



    /****************************
     * Detail Panel & Highlighted
     * **************************
     */

    $scope.candidatePanel = function (pcItem) {


        if ($scope.selectedContactDetail && ($scope.selectedContactDetail.Id === pcItem.Id)) {
            $scope.selectedContactDetail = undefined;

        } else {
            displayResume(pcItem);
            $scope.selectedContactDetail = pcItem;
            $scope.selectedContactDetail.detailFlag = true;

        }

    };

    function displayResume(pcItem) {

        // if (pcItem.Candidate__r && pcItem.Candidate__r.Resume__c) {
        //     if (!pcItem.displayResume) {
        //
        //         var processResume = pcItem.Candidate__r.Resume__c;
        //         processResume = processResume.replace(/\n/g, '<br>');
        //         // pcItem.Candidate__r.processedResume = processResume;
        //         pcItem.Candidate__r.displayResume = processResume;
        //
        //     } else {
        //         pcItem.Candidate__r.displayResume = pcItem.displayResume;
        //
        //     }
        // }
        //
        // return;



        if (pcItem.Candidate__r && pcItem.Candidate__r.Resume__c) {

            if ($scope.searchKeyword.length > 1) {

                if (pcItem.Candidate__r.keywordSearch && pcItem.Candidate__r.keywordSearch === $scope.searchKeyword) {
                    // pcItem.Candidate__r.displayResume = pcItem.processedResume;

                } else {

                    searchCandiateListResume();

                    // var tempResume = highlightResume(pcItem.Candidate__r.Resume__c, $scope.searchKeyword);
                    // var highlighted = tempResume[0];
                    // var numberOfMatch = tempResume[1];
                    // highlighted = highlighted.replace(/\n/g, '<br>');
                    //
                    // pcItem.Candidate__r.keywordMatch = numberOfMatch;
                    // pcItem.Candidate__r.displayResume = highlighted;
                    // pcItem.Candidate__r.keywordSearch = $scope.searchKeyword;
                }

            } else if (!pcItem.Candidate__r.processedResume) {

                var processResume = pcItem.Candidate__r.Resume__c;
                processResume = processResume.replace(/\n/g, '<br>');
                pcItem.Candidate__r.processedResume = processResume;
                pcItem.Candidate__r.displayResume = processResume;

            } else if (pcItem.processedResume) {
                pcItem.Candidate__r.displayResume = pcItem.processedResume;

            } else {
                pcItem.Candidate__r.displayResume = pcItem.Candidate__r.processedResume;
            }

        }

    }


    /***************************
     * Search resume for keyword
     * *************************
     */

    $scope.searchResume = function (keyword) {
        $scope.searchKeyword = keyword;
    };

    function searchCandiateListResumeTEMP() {

        if ($scope.clientsFiltered) {
            for (var i=0; i<$scope.clientsFiltered.length; i++) {

                var tempResume = [];
                var pcItem = $scope.clientsFiltered[i];
                if (pcItem && pcItem.Candidate__r && pcItem.Candidate__r.Resume__c) {
                    tempResume = highlightResume(pcItem.Candidate__r.Resume__c, $scope.searchKeyword);
                    var processResume = tempResume[0];
                    processResume = processResume.replace(/\n/g, '<br>');
                    pcItem.Candidate__r.displayResume = processResume;
                    pcItem.Candidate__r.keywordMatch = tempResume[1];
                    pcItem.Candidate__r.keywordSearch = $scope.searchKeyword;
                }

            }
        }
    }

    //watch for search resume keyword
    $scope.$watch('searchKeyword', function (newValue, oldValue) {

        if (typeof newValue !== oldValue) {
            //Do search resume
            searchCandiateListResume();
        }

        if(newValue === "") {

            angular.forEach($scope.clients, function(client) {
                if (client.Candidate__r && client.Candidate__r.keywordMatch) {
                    client.Candidate__r.keywordMatch = 0;
                }
            });
        }

    }, true);


    /*********************
     * CONFIRM Change POPUP
     * ********************
     */

    $scope.modalInstance = undefined;
    $scope.confirmUnsaveChanges = function () {

        $scope.modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'myModalContent.html',
            controller: function ($scope, $rootScope, $uibModalInstance) {

                $scope.cancelChangesBtn = function () {
                    $rootScope.isCloseApp = false;
                    $rootScope.$emit("CHANGE_PAGE_CANCEL", "EMIT");
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.discardChangesBtn = function () {
                    $rootScope.$emit("CHANGE_PAGE_DISCARD", "EMIT");
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.saveChangesBtn = function () {
                    $rootScope.$emit("CONFIRM_SAVE_CHANGE", "EMIT");
                    $uibModalInstance.dismiss('cancel');
                };
            }

        });
    };

    $scope.modalInstanceForFilter = undefined;
    $scope.confirmUnsaveChangesForFilter = function () {

        $scope.modalInstanceForFilter = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'myModalContent.html',
            controller: function ($scope, $rootScope, $uibModalInstance) {

                $scope.cancelChangesBtn = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.discardChangesBtn = function () {
                    scope.filterArray = angular.copy(scope.modalFilterArray);

                    if (isValidFilter()) {
                        scope.isFilterApplied = true;
                    } else {
                        scope.isFilterApplied = false;
                    }

                    $rootScope.$emit("CHANGE_PAGE_DISCARD", "EMIT");
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.saveChangesBtn = function () {
                    scope.filterArray = angular.copy(scope.modalFilterArray);

                    if (isValidFilter()) {
                        scope.isFilterApplied = true;
                    } else {
                        scope.isFilterApplied = false;
                    }

                    $rootScope.$emit("CONFIRM_SAVE_CHANGE", "EMIT");
                    $uibModalInstance.dismiss('cancel');
                };
            }

        });
    };

    $scope.modalInstanceForPagination = undefined;
    $scope.confirmUnsaveChangesForPagination = function () {

        $scope.modalInstanceForPagination = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'myModalContent.html',
            controller: function ($scope, $rootScope, $uibModalInstance) {

                $scope.cancelChangesBtn = function () {
                    scope.paginationObject.currentPage = scope.paginationObject.lastPage;
                    scope.paginationObject.itemsPerPage = scope.paginationObject.lastItemsPerPage;
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.discardChangesBtn = function () {
                    $rootScope.$emit("CHANGE_PAGE_DISCARD", "EMIT");
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.saveChangesBtn = function () {
                    $rootScope.$emit("CONFIRM_SAVE_CHANGE", "EMIT");
                    $uibModalInstance.dismiss('cancel');
                };
            }

        });
    };

    $rootScope.$on("CHANGE_PAGE_CANCEL", function (event, args) {
        if (!$scope.isChangeView) {
            $scope.paginationObject.currentPage = $scope.paginationObject.previousPage;
        } else {
             // cancel page change
            $scope.uiState.isChangeView = false;
        }
    });

    function discardPageChanges () {

        for(var i = 0; i < $scope.originClientsFiltered.length; i++) {
            for(var j = 0; j < $scope.clients.length; j++) {
                if ($scope.clients[j].Id === $scope.originClientsFiltered[i].Id) {
                    $scope.clients[j].Status__c = $scope.originClientsFiltered[i].Status__c;

                    if($scope.clients[j].Recruiter_Notes__c !== undefined && $scope.originClientsFiltered[i].Recruiter_Notes__c) {
                        $scope.clients[j].Recruiter_Notes__c = $scope.originClientsFiltered[i].Recruiter_Notes__c;
                    } else if ($scope.clients[j].Recruiter_Notes__c && $scope.originClientsFiltered[i].Recruiter_Notes__c === undefined) {
                        delete $scope.clients[j].Recruiter_Notes__c;
                    }

                    if($scope.originClientsFiltered[i].Candidate_Status__c) {
                        addOriginalStatusPickList($scope.clients[j]);
                        $scope.clients[j].Candidate_Status__c = $scope.originClientsFiltered[i].Candidate_Status__c;
                    } else if ($scope.clients[j].Candidate_Status__c && $scope.originClientsFiltered[i].Candidate_Status__c === undefined) {
                        delete $scope.clients[j].Candidate_Status__c;
                        $scope.clients[j].candidateStatusPicklists = null;
                    }else{
                        $scope.clients[j].candidateStatusPicklists = null;
                    }

                    if($scope.clients[j].Rating__c && $scope.originClientsFiltered[i].Rating__c) {
                        $scope.clients[j].Rating__c = $scope.originClientsFiltered[i].Rating__c;
                    }

                }
            }
        }
    }

    function addOriginalStatusPickList(client){
        $scope.candidateStatusPicklist.forEach(function(item) {
            if (item.value === client.Status__c) {
                client.candidateStatusPicklists = item.picklist;
                if (client.candidateStatusPicklists.length === 0) {
                    client.Candidate_Status__c = "";
                }
            }
        });
    }

    function resetBulkUpdateFileds () {
        $scope.updateObj = {
            updateStatus: null,
            updateRecruiterNote: null
        };
    }

    //TODO refactor discard change event both in candidateList.js and screenCandidateController.js
    $rootScope.$on("CHANGE_PAGE_DISCARD", function (event, args) {
        $rootScope.confirmUpdateFlag = false;

        if ($scope.isFilterApplied) {

            discardPageChanges();
            customFilter();
        } else if (!$scope.isChangeView) {

            discardPageChanges();
            setCurrentPageItems();
        } else {

            $scope.isChangeView = false;
        }

        resetBulkUpdateFileds();
        $scope.clientSelected = 0;

        $rootScope.$broadcast("CONFIRMED_DISCARD", "broadcast");
    });

    //TODO refactor save change event both in candidateList.js and screenCandidateController.js
    $rootScope.$on("CONFIRM_SAVED", function (event, args) {
        $rootScope.confirmUpdateFlag = false;

        // reset new view in case a status change 
        if ($scope.uiState.listState === "New"){
            $scope.clients =  $scope.allVacancyCandidateList.filter(function(candidate){
                return candidate.Status__c === "New";
            });
        }

        if ($scope.isFilterApplied) {

            customFilter();
        } else {
            setCurrentPageItems();
        }

        resetBulkUpdateFileds();
        $scope.clientSelected = 0;
    });

    $rootScope.$on("CLOSE_APP", function (event, args) {
        if ($scope.confirmUnsaveChanges) {
            $scope.confirmUnsaveChanges();
        }
    });


    /*********************
     * TEXT HIGHLIGHT
     * ********************
     */

    function removeExcludedWords(targetString){
        var excludeList = "\\b(NOT|AND|OR)\\b";
        var re = new RegExp(excludeList, "ig");
        targetString.replace(re, "");
        return targetString;
    }

    function captureQuotedText(str){
        var re = /(\"[\w\s]+\")/g;
        var m;
        var result = [];
        while ((m = re.exec(str)) !== null) {
            if (m.index === re.lastIndex) {
                re.lastIndex++;
            }
            // View your result using the m-variable.
            // eg m[0] etc.
            // remove double quotation.
            var match = m[0].replace(/(\")/g, "");
            result.push(match);
        }

        console.log("result", result);
        return result;
    }

    /**
     * use to add escape char to strings contain illegal chars
     * @param stringToGoIntoTheRegex
     * @returns {*}
     */
    function escapeRegExp(string){
        return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
    }

    function highlightResume(targetString, searchKeywords) {
        // precess keywords
        var result = targetString;
        // remove exclude keywords
        var keywords = removeExcludedWords(searchKeywords);
        // capture quoted texts
        var quotedText = captureQuotedText(keywords);
        // remove quoted text
        keywords = keywords.replace(/(\"[\w\s]+\")/g, "");
        // remove excluded operator
        keywords = keywords.replace(/\b(NOT|OR|AND)\b/ig, "");
        // remove brackets
        keywords = keywords.replace(/(\)|\()/ig, "");
        keywords = escapeRegExp(keywords);
        // remove extra space and add | as divider
        keywords = keywords.replace(/[\s]+/g, "|");
        // remove || from string
        keywords = keywords.replace(/^\||\|$/g, "");

        var combineText = "";
        if(keywords.length > 1){
            combineText += keywords;
        }
        if(quotedText.length > 0){
            var joinQuotedText = quotedText.join("|");
            if(combineText.length > 1){
                combineText += "|" + joinQuotedText;
            }
            else {
                combineText += joinQuotedText;
            }
        }
        var numberOfMatch = 0;
        if(combineText.length > 1){
            var queryText = "(\\b|(?!\\w))(" + combineText.trim() + ")(\\b|(?!\\w))";
            console.log("query text", queryText);
            //  var queryText = "([^\\w])(" + combineText.trim() + ")([^\\w])";
            var regexQuery = new RegExp(queryText, "ig");
            result = targetString.replace(regexQuery, function(match){
                numberOfMatch ++;
                return "<mark> " + match +"</mark>";
            });
        }

        //snippets.forEach(function (snippet) {
        //    var formattedSir = escapeRegExp(snippet);
        //    var  snippetReg = new RegExp(formattedSir, "gim");
        //    var foundIndex = str.search(snippetReg);
        //    // todo add the new process
        //    // use replace in place of substring
        //    if (foundIndex !== -1) {
        //        str = str.substring(0, foundIndex) + "<mark>" + str.substring(foundIndex, (foundIndex + snippet.length)) + "</mark>" + str.substring((foundIndex + snippet.length));
        //    }
        //});

        return [result, numberOfMatch];
    }

    /*********************
     * Form control function
     * ********************
     */

        function isClientExisted(Id, myArray){
        for (var i=0; i < myArray.length; i++) {
            if (myArray[i].Id === Id) {
                return true;
            }
        }
        return false;
    }

    $scope.updateChanges = function () {

        angular.forEach($scope.clientsFiltered, function (client, key) {
            if (client.isSelect) {

                // if(!isClientExisted(client.Id, clientsHaveChanges)) {
                //     var tempClient = angular.copy(client);
                //     clientsHaveChanges.push(tempClient);
                // }

                if ($scope.updateObj.updateStatus) {
                    client.Status__c = $scope.updateObj.updateStatus;
                    $rootScope.confirmUpdateFlag = true;
                }
                if ($scope.updateObj.updateRecruiterNote !== null) {
                    client.Recruiter_Notes__c = $scope.updateObj.updateRecruiterNote;
                    $rootScope.confirmUpdateFlag = true;
                }

            }
        });

    };

    $scope.processStatusChange = function () {
        var showError = false;
        angular.forEach($scope.clientsFiltered, function (client, key) {
            if (client.isSelect) {
                var hasValue = false;
                if ($scope.updateObj.updateStatus) {
                    //check the client's option contain this status
                    angular.forEach(client.statusPickList, function(option, key){
                        if(option.value === $scope.updateObj.updateStatus){
                            client.Status__c = $scope.updateObj.updateStatus;
                            $rootScope.confirmUpdateFlag = true;
                            $scope.candidateStatusPicklist.forEach(function(item) {
                                if (item.value === client.Status__c) {
                                    client.candidateStatusPicklists = item.picklist;
                                    if (client.candidateStatusPicklists.length === 0) {
                                        client.Candidate_Status__c = "";
                                    }
                                }
                            });
                            hasValue = true;
                        }
                    });
                }
                if(!hasValue){
                    showError = true;
                }
            }
        });
        if(showError){
             Notification.warning("Some records cannot be updated because the status value is not available for the record type.");
        }
    };

    $scope.processRecruiterNoteChange = function () {
        angular.forEach($scope.clientsFiltered, function (client, key) {
            if (client.isSelect) {

                if ($scope.updateObj.updateRecruiterNote !== null) {
                    client.Recruiter_Notes__c = $scope.updateObj.updateRecruiterNote;
                    $rootScope.confirmUpdateFlag = true;
                }

            }
        });
    };

    $scope.naveToPage = function (objectId) {
    	return redirectService.naveToPage($window,objectId);
//    	var idurl = redirectService.urlfor(objectId);
//        var targetUrl = $window.location.origin;
//        //targetUrl += "/" + objectId;
//        targetUrl += idurl;
//        return targetUrl;
        //redirectService.openSalesForceObject(targetId, "_blank");
    };

    function setCandidateStatusPicklist(record) {
        $scope.candidateStatusPicklist.forEach(function(item) {
            if (item.value === record.Status__c) {
                record.candidateStatusPicklists = item.picklist;
            }
        });
    }

    function allocateCandidateStatusPicklist() {
        $scope.clients.forEach(function(client) {
            $scope.candidateStatusPicklist.forEach(function(item) {
                    if (item.value === client.Status__c) {
                        client.candidateStatusPicklists = item.picklist;
                    }
                });
        });
    }

    $scope.changeStatus = function (client) {

        $scope.originClientsFiltered.forEach(function(originClient) {

            if (client.Id === originClient.Id && client.Status__c !== originClient.Status__c) {
                isStatusChanged = true;
            } else if (client.Id === originClient.Id && client.Status__c === originClient.Status__c){
                isStatusChanged = false;
            }
        });

        if (isStatusChanged) {
            $rootScope.confirmUpdateFlag = true;
        } else if(!isStatusChanged && (isCandidateStatusChanged || isRecruiterNotesChanged)) {
            $rootScope.confirmUpdateFlag = true;
        } else {
            $rootScope.confirmUpdateFlag = false;
        }
        
        $scope.clientsFiltered.forEach(function(clientFiltered) {

            if (client.Id === clientFiltered.Id) {
                    $scope.candidateStatusPicklist.forEach(function(item) {
                    if (item.value === client.Status__c) {
                        client.candidateStatusPicklists = item.picklist;
                        if (client.candidateStatusPicklists.length === 0) {
                            client.Candidate_Status__c = "";
                        }
                    }
                });
            }
        });
    };

    $scope.changeCandidateStatus = function (client) {
        
        $scope.originClientsFiltered.forEach(function(originClient) {

            if (client.Id === originClient.Id && client.Candidate_Status__c !== originClient.Candidate_Status__c) {
                isCandidateStatusChanged = true;
            } else if (client.Id === originClient.Id && originClient.Candidate_Status__c && client.Candidate_Status__c === originClient.Candidate_Status__c){
                isCandidateStatusChanged = false;
            }
        });

        if (isCandidateStatusChanged) {
            $rootScope.confirmUpdateFlag = true;
        }else if(!isCandidateStatusChanged && (isStatusChanged || isRecruiterNotesChanged)) {
            $rootScope.confirmUpdateFlag = true;
        } else {
            $rootScope.confirmUpdateFlag = false;
        }

    };

    /****************
     *  Action
     ****************/

    $scope.addToShortlist = function (el) {
        $scope.isEditingShortlist = true;
        $scope.onAddToShortlist({client: el});
    };

    $scope.removeFromShortlist = function (el) {
        $scope.isEditingShortlist = true;
        $scope.onRemoveFromShortlist({client: el});
    };

    $scope.logContactCall = function (record) {
        var url = "00T/e?title=Call&who_id=" + record.Candidate__c + "&followup=1&tsk5=Call&retURL=%2F" + record.Candidate__c;

        //TODO  Url provided without org prefix. Add org prefix.
        //var targetUrl = $window.location.origin;

        var targetUrl = "/" + url;
        //$window.open(targetUrl);
        redirectService.open(targetUrl, "_blank");
    };

    $scope.sendContactEmail = function(record) {

        $scope.onSendEmail({contactsList: [record.Candidate__c]});
    };

    $scope.emailDisableNotification = function () {
        Notification.error("To activate Email functionality, please contact support@clicktocloud.com");
    };

    /*******************
     * Customized Column
     *******************/

    /***
     * get display value based on the column
     * @param record
     * @param column
     * @returns {string} display value
     */
    $scope.displayRecordValue = function(record, column, index) {
        var displayValue = "";

        if ($scope.selectedObjectList[index].name === $scope.objectLabelNames['Candidate Management']) {
            switch (column.fieldtype) {
                case fieldTypeEnum.REFERENCE:
                    if (record.hasOwnProperty(column.referenceFieldName)) {
                        displayValue = record[column.referenceFieldName].Name;

                    } else {
                        displayValue = record[column.Field_api_name];
                    }
                    break;
                case fieldTypeEnum.DATE:
                case fieldTypeEnum.DATETIME:
                    var tempValue = record[column.Field_api_name];
                    displayValue = $filter("date")(tempValue, "dd/MM/yyyy");
                    break;
                case fieldTypeEnum.CURRENCY:
                    displayValue = $filter('currency')(record[column.Field_api_name], '$', 2);
                    break;
                case fieldTypeEnum.PERCENT:
                    if (record[column.Field_api_name] && (record[column.Field_api_name] !== undefined || record[column.Field_api_name] !== null)) {
                        displayValue = record[column.Field_api_name] + " %";
                    }
                    break;
                default:
                    displayValue = record[column.Field_api_name];
                    break;
            }

        } else if ($scope.selectedObjectList[index].name === $scope.objectLabelNames['Contact']&& record.Candidate__r) {
            switch (column.fieldtype) {
                case fieldTypeEnum.REFERENCE:
                    if (record.Candidate__r.hasOwnProperty(column.referenceFieldName)) {
                        displayValue = record.Candidate__r[column.referenceFieldName].Name;

                    } else {
                        displayValue = record.Candidate__r[column.Field_api_name];
                    }
                    break;
                case fieldTypeEnum.DATE:
                case fieldTypeEnum.DATETIME:
                    var tempValue = record.Candidate__r[column.Field_api_name];
                    displayValue = $filter("date")(tempValue, "dd/MM/yyyy");
                    break;
                case fieldTypeEnum.CURRENCY:
                    displayValue = $filter('currency')(record.Candidate__r[column.Field_api_name], '$', 2);
                    break;
                case fieldTypeEnum.PERCENT:
                    if (record.Candidate__r[column.Field_api_name] && (record.Candidate__r[column.Field_api_name] !== undefined || record.Candidate__r[column.Field_api_name] !== null)) {
                        displayValue = record.Candidate__r[column.Field_api_name] + " %";
                    }
                    break;
                default:
                    displayValue = record.Candidate__r[column.Field_api_name];
                    break;
            }

        }

        return displayValue;
    };

    $scope.changeRating = function (client, rating) {
        client.Rating__c = rating;
        $rootScope.confirmUpdateFlag = true;
    };

    /*********************
     * Column Sorting
     ********************/

    $scope.sortBy = function(propertyName) {
        if($rootScope.confirmUpdateFlag) {
            return;
        }
        $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
        $scope.clients = sortService.sort($scope.clients, $scope.propertyName, $scope.reverse);
    };

    function resetSorting() {
        $scope.propertyName = 'CreatedDate';
        if("Shortlist" === $scope.uiState.listState) {
            $scope.propertyName = "Shortlist_Time__c";
        }
        $scope.reverse = true;
        $scope.allVacancyCandidateList = sortService.sort($scope.allVacancyCandidateList, $scope.propertyName, $scope.reverse);
        $scope.clients = $scope.allVacancyCandidateList;
    }

    function dynamicSort(property, reverse) {
        var sortOrder = 1;
        if(!reverse) {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a,b) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        };
    }

    $scope.customSortBy = function(column) {
        if($rootScope.confirmUpdateFlag) {
            return;
        }
        if(column.columnObject.name === $scope.objectLabelNames['Candidate Management']) {
            $scope.reverse = (column.Field_api_name !== null && $scope.propertyName === column.Field_api_name) ? !$scope.reverse : false;
            $scope.propertyName = column.Field_api_name;
            $scope.clients = sortService.sort($scope.clients, $scope.propertyName, $scope.reverse);
            // $scope.clients.sort(dynamicSort($scope.propertyName, $scope.reverse));
        } else {
            var propertyName = "Candidate__r."+column.Field_api_name;
            $scope.reverse = (column.Field_api_name !== null && $scope.propertyName === propertyName) ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
            $scope.clients = sortService.sort($scope.clients, $scope.propertyName, $scope.reverse);
            // $scope.clients.sort(dynamicSort($scope.propertyName, $scope.reverse));
        }

    };

    /******************
     * Inline editing
     ****************** 
     */

    $scope.editedItem = null;
    var originNotesValue = "";
    var isRecruiterNotesChanged = false;

    $scope.startEditing = function(item){
        originNotesValue = null;
        item.editing = true;
        $scope.editedItem = item;
        if (item.Recruiter_Notes__c) {
            originNotesValue = item.Recruiter_Notes__c;
        }
    };

    $scope.notesEdited = function(item) {
        isRecruiterNotesChanged = true;
        $rootScope.confirmUpdateFlag = true;
    };

    // TODO: doneEditing is called twice currently, need to reduce to once. 
    $scope.doneEditing = function(item){

        item.editing = false;
        $scope.editedItem = null;

        if(originNotesValue !== null && originNotesValue !== item.Recruiter_Notes__c) {
            isRecruiterNotesChanged = true;
            $rootScope.confirmUpdateFlag = true;
        } else if (originNotesValue === null && item.Recruiter_Notes__c) {
            isRecruiterNotesChanged = true;
            $rootScope.confirmUpdateFlag = true;
        } else if(originNotesValue !== "" && (!item.Recruiter_Notes__c || originNotesValue === item.Recruiter_Notes__c) && (isStatusChanged || isCandidateStatusChanged) ){
            isRecruiterNotesChanged = false;
            $rootScope.confirmUpdateFlag = true;
        } else if(originNotesValue !== "" && (originNotesValue === item.Recruiter_Notes__c) && !isStatusChanged && !isCandidateStatusChanged && !$rootScope.confirmUpdateFlag){
            isRecruiterNotesChanged = false;
        }
        
    };

    /*********************
     * INIT
     * ********************
     */

    // Only trigger change if on switch view, clients list change
    $scope.$watchCollection('clients', function (newValue, oldValue) {

        if (newValue.length === 0) {
            $scope.resultFlag = false;
            resetClientTable();
        } else {
            $scope.resultFlag = true;
            $scope.hasPagination = true;
            $scope.paginationObject.currentPage = 1;
            setCurrentPageItems();
            allocateCandidateStatusPicklist();
        }

    });

    init();


    //**********************
    // Search & Highlight
    //**********************


    function searchCandiateListResume() {
        $scope.searchKeyword = $scope.searchKeyword || "";

        PC.EnhancedMassRating.searchAndHighlight($scope.clientsFiltered, $scope.searchKeyword);

    }


    var PC={};    // Initialize namespace for people cloud js functions
    PC.EnhancedMassRating={};    // Initialize namespace for enhanced mass rating js functions
    PC.EnhancedMassRating.Msg={}; // Store messages that will show on screen
    PC.EnhancedMassRating.searchExp={}; // Global variable which is used to store
    PC.EnhancedMassRating.inputWarning=false; // Indicate if warning of inputing lower case search logic operator is on or not
    PC.EnhancedMassRating.contactMap=null;
    PC.EnhancedMassRating.highLightColors=[
        "#c9643b",//terra cotta
        "#fe420f",//orangered
        "#fac205",//goldenrod
        "#95d0fc",//light blue
        "#96f97b",//light green
        "#fe83cc",//aquamarine
        "#fe83cc",//bubblegum pink
        "#ef1de7",//pink/purple
        "#632de9",//purple blue
        "#bbf90f",//yellowgreen
        "#b5ce08",//green/yellow
        "#23c48b",//greenblue
        "#137e6d",//blue green
        "#85a3b2",//bluegrey
        "#86a17d"//grey/green
    ];
    PC.EnhancedMassRating.keywords=""; // a global variable for enhancedMassRating.page, used to store the input keywords
    PC.EnhancedMassRating.Msg.WARN_LOWER_CASE_LOGIC='Are you trying to input "AND" or "OR"? Search logic operators are case sensitive.';
    PC.EnhancedMassRating.Msg.WARN_MINI_CHARACTERS = 'At least 3 characters of keywords required.';
    PC.namespace = "";


    /**
     * A function that check input string to see if it contain lower case logic operators.
     *
     * @param {String} strToTest
     * @return {boolean} true: input string contains lower case logic operator
     */
    PC.EnhancedMassRating.isContainLowerCaseLogicOpt=function(strToTest){
        var result=false,
            op="",
            i=0,
            caseInsensitiveMatch=null,
            caseSensitiveMatch=null;
        for(i=0;i< PC.EnhancedMassRating.SearchExpressionGenerator.prototype.logicOperatorPrecedence.length; i++){
            op = PC.EnhancedMassRating.SearchExpressionGenerator.prototype.logicOperatorPrecedence[i];
            caseInsensitiveMatch = new RegExp("\\b"+op+"\\b","ig");
            caseSensitiveMatch = new RegExp("\\b"+op+"\\b","g");
            if(strToTest.match(caseInsensitiveMatch)==null)
                continue;
            if(strToTest.match(caseInsensitiveMatch)!=null && strToTest.match(caseSensitiveMatch)==null
                || strToTest.match(caseInsensitiveMatch).length != strToTest.match(caseSensitiveMatch).length){
                result=true;
                break;
            }
        }
        return result;
    };

    /**
     *  This method will be search db based on user input keyword then highlight them in resumes
     *
     * @param {Object} event
     */
    PC.EnhancedMassRating.searchAndHighlight=function(clientsFiltered, searchStr){
        //console.log("Search for: "+ PC.EnhancedMassRating.keywords);

        if(PC.EnhancedMassRating.isContainLowerCaseLogicOpt(searchStr) && !PC.EnhancedMassRating.inputWarning){
            PC.EnhancedMassRating.inputWarning = true;
            // PC.EnhancedMassRating.Msg.WARN_LOWER_CASE_LOGIC;
        }

        PC.EnhancedMassRating.keywords = searchStr.replace(/;/ig," ");

        var id = null;
        // var contactMap = PC.EnhancedMassRating.contactMap;
        // var idList = [];

        PC.EnhancedMassRating.removeHighligh();
        var keywords=PC.EnhancedMassRating.keywords;
        var gen = new PC.EnhancedMassRating.SearchExpressionGenerator();
        PC.EnhancedMassRating.searchExp = gen.generateSearchExpression(keywords);

        if(!PC.EnhancedMassRating.searchExp){

            alert(PC.global.errorMsg["JAVASCRIPT_ERROR"]);
            console.error("Invalid Search Expression, info PC.EnhancedMassRating.searchExp:"+PC.EnhancedMassRating.searchExp);

        } else if (keywords && keywords.replace(/\W/g,"").length>1) {

            for (var i=0; i<clientsFiltered.length; i++) {
                var candidate = clientsFiltered[i].Candidate__r;
                if (candidate) {
                    var resumeText = candidate[PC.namespace + "Resume__c"];
                    if (resumeText) {
                        if (PC.EnhancedMassRating.searchExp.isContainedIn(resumeText)) {

                            candidate.keywordMatch = "Match";
                            // idList.push(contactMap[i].Id);
                            //console.log("Found match: " + candidate.Name);
                        } else {
                            candidate.keywordMatch = "";
                        }
                    }
                }
            }

            // console.log("idList: "+idList);
            PC.EnhancedMassRating.highightKeyWords(clientsFiltered);

        }else{
            // do nothing
        }
    };

    PC.EnhancedMassRating.highightKeyWords = function(clientsFiltered){
        var keywords=PC.EnhancedMassRating.keywords,
            resumeContent="",
            gen = new PC.EnhancedMassRating.SearchExpressionGenerator(),
            se =gen.generateSearchExpression(keywords),
            highlightRegExp = null; // This regexp is used to find out the content to be highlighted
        // var contactMap = PC.EnhancedMassRating.contactMap;


        // Remove existing highlight html to avoid breaking them
        PC.EnhancedMassRating.removeHighligh();
        if(keywords.trim().length<=1) {
            return;
        }

        // Use the evaluated match expression to highlight all matched words
        for(var i=0;i<clientsFiltered.length;i++){
            var numOfMatch = 0;
            var candidate = clientsFiltered[i].Candidate__r;
            if (candidate) {
                var resumeContent = candidate[PC.namespace + "Resume__c"];
                if (resumeContent) {
                    for (var j = 0; j < se.profixNotation.length; j++) {
                        if (!gen.isALogicOperator(se.profixNotation[j])) {
                            highlightRegExp = new RegExp("([^\\w<])(" + se.translationMap[se.profixNotation[j]] + ")([^\\w>])", "ig");
                            resumeContent = resumeContent.replace(highlightRegExp, '$1<' + j + '>$2</!>$3');
                        }
                    }
                    for (var k = 0; k < se.profixNotation.length; k++) {
                        if (!gen.isALogicOperator(se.profixNotation[k])) {
                            highlightRegExp = new RegExp("<" + k + ">", "ig");
                            // resumeContent = resumeContent.replace(highlightRegExp, '<span class="highlighted" style="background-color:' + PC.EnhancedMassRating.colorPicker(k) + '">');
                            //resumeContent = resumeContent.replace(highlightRegExp, "<mark>");
                            resumeContent = resumeContent.replace(highlightRegExp,function(match){
                                numOfMatch++;
                                return '<span  class="highlighted" style="background-color:' + PC.EnhancedMassRating.colorPicker(k) + '">';
                            });
                        }
                    }
                    //console.log(resumeContent);
                    resumeContent = resumeContent.replace(/<\/!>/ig, "</span>");
                    //console.log(resumeContent);
                    resumeContent = resumeContent.replace(/\n/g, '<br>');
                    //console.log(resumeContent);
                }
                candidate.displayResume = resumeContent;
               
                if (candidate.keywordMatch === "Match") {
                    //console.log("number of match : " + candidate.Name+ " "+ numOfMatch);
                    candidate.keywordMatch = numOfMatch;
                }
            }

        }
    };

    PC.EnhancedMassRating.afterRemoteSearchAction=function(result,event){
        PC.EnhancedMassRating.filterResults(result,event);
        $("#searchingIcon").hide();
        if(Object.keys(PC.EnhancedMassRating.detailSections).length>0){
            PC.EnhancedMassRating.highightKeyWords();
        }

    };

    PC.EnhancedMassRating.colorPicker=function(colorIndex){
        return PC.EnhancedMassRating.highLightColors[colorIndex % PC.EnhancedMassRating.highLightColors.length];
    };

    PC.EnhancedMassRating.removeHighligh = function(){

        // var contactMap = PC.EnhancedMassRating.contactMap;
        // for(var i=0;i<contactMap.length;i++){
        //     contactMap[i].displayResume = contactMap[i][PC.namespace + "Resume__c"];
        // }

    };

    /**
     *
     * @param {Object} profixNotation
     * @param {Object} translationMap
     */
    PC.EnhancedMassRating.SearchExpression = function(profixNotation,translationMap){
        this.profixNotation = profixNotation;
        this.translationMap = translationMap;
    };

    PC.EnhancedMassRating.SearchExpression.prototype={
        profixNotation:[],
        translationMap:{},
        isContainedIn:function(str){
            var seGen = new PC.EnhancedMassRating.SearchExpressionGenerator(),
                pnCopy = this.profixNotation.slice(),
                subMatchingStatus = new Array();
                matchingItem = null,
                jsLogicOp="&&",
                boolVal1=false,
                boolVal2=false,
                searchRegExp=null,
                finalMatchingStatus=true,
                sglSubMatchingStatus = false;
            // If string is empty or undefined, return false
            if(!str)
                return false;
            pnCopy = pnCopy.reverse();
            while(pnCopy.length!=0){
                matchingItem = pnCopy.pop();
                if(seGen.isALogicOperator(matchingItem)){
                    jsLogicOp = seGen.getJSLogicOperator(matchingItem);
                    boolVal1 = subMatchingStatus.pop();
                    boolVal2 = subMatchingStatus.pop();
                    subMatchingStatus.push(eval(boolVal1+" "+jsLogicOp+" "+boolVal2));
                }else{
                    var regExpString = this.translationMap[matchingItem];
                    searchRegExp = new RegExp("[^\\w]"+this.translationMap[matchingItem]+"[^\\w]","ig");
                    subMatchingStatus.push(str.search(searchRegExp)!=-1)
                }
            }
            if(subMatchingStatus.length>1){
                while(subMatchingStatus.length>0){
                    sglSubMatchingStatus = subMatchingStatus.pop();
                    finalMatchingStatus = eval(finalMatchingStatus +" && "+sglSubMatchingStatus);
                }
            }else{
                finalMatchingStatus = subMatchingStatus.pop()
            }
            return finalMatchingStatus;
        }
    };

    /**
     * Search expression generator constructor which will generate search expression that be used to match against
     * candidate resume for matched records.
     *
     * The returned search expression which relays on regular expression will be able to handle search operators like:
     *
     *   wildcards operators: *, ?
     *   char escape operator: \
     *   boolean logic operator: AND, OR, AND NOT, ( )
     *   quotation operators: " "
     *
     * Other non-word characters (which can be matched with \W in JS), will be treated as normal characters.
     * Those characters include: #, ., +
     *
     * @return PC.EnhancedMassRating.SearchExpression
     *
     */
    PC.EnhancedMassRating.SearchExpressionGenerator    = function(){
        // Parse tree is used for translating SF search operator to the ones used in JS regular expression
        // Parse tree is a map where key is the SF search operator input by user, and value is
        // the translated value. If translated value is an object, it means we are expecting the
        // way we pare next char is related to the recent char. Eg. escaping character in search result.
        this.parseTree = {
            " ":" ",
            "\\":{"*":"\\*",
                "?":"\\?"

            },
            "?":"\\w?",
            "*":"\\w*"
        }
        // As the parsing tree will be changed under different input characters,
        // this map is to show which parse tree should be or is using to translate input
        // character at the moment
        this.recentLookupMap = this.parseTree
    }

    /**
     *
     */
    PC.EnhancedMassRating.SearchExpressionGenerator.prototype={
        parseTree:{},
        escapeTree: {},
        logicOperatorPrecedence:["OR","AND"],
        strOperators:["\"","\\","*","?"],
        logicOperatorMap:{"OR":"||","AND":"&&"},
        /*
            *  This map will be used to prevent valide keyword like '+' in 'c++', '#' in 'c#', '.' in 'node.js' from
            *  being treated as operators when program is splitting the string.
            */
        validNonStrKeywordsMap:{"\\.":"11DOT11","#":"11SHARP11","\\+":"11PLUS11"},
        generateSearchExpression:function(exprStr){
            // Initialize exprStr to make sure it's not null
            exprStr = exprStr ?exprStr:"";
            exprStr = this.encodeValideNonStrKeywords(exprStr);
            var profixNotation = this.generatePostfixNotation(exprStr.split(/\b/g)),
                translationMap = this.translateProfixNotation(profixNotation);
            return new PC.EnhancedMassRating.SearchExpression(profixNotation,translationMap);
        },
        // Translate the input search string to a regular expression search string
        translateProfixNotation:function(profixNotation){
            var notationItem="",
                translationMap={};
            for(var i=0;i<profixNotation.length;i++){
                notationItem = profixNotation[i];
                if(!this.isALogicOperator(notationItem)){
                    translationMap[notationItem]=this.translateSglPhrase(notationItem);
                    //profixNotation[i]=translationMap[notationItem];
                }
            }
            return translationMap;
        },
        // Transalte each phrase to a regular expression search phrase
        translateSglPhrase:function(word){
            var newWord="";
            for(var i=0;i<word.length;i++){
                newWord=newWord+this.translateSglChar(word[i]);
            }
            return newWord;
        },
        // Transalte each char
        translateSglChar:function(ch){
            var result = this.recentLookupMap[ch];
            this.recentLookupMap = this.parseTree
            if(!result){
                result=ch;
            }else if(typeof result === "object"){
                this.recentLookupMap=result;
                result ="";
            }
            return result;
        },
        encodeValideNonStrKeywords:function(strToEncode){
            for(var key in this.validNonStrKeywordsMap){
                strToEncode = strToEncode.replace(new RegExp(key,"g"),this.validNonStrKeywordsMap[key]);
            }
            return strToEncode;
        },
        decodeValidNonStrKeywords:function(strToDecode){
            for(var key in this.validNonStrKeywordsMap){
                strToDecode = strToDecode.replace(new RegExp(this.validNonStrKeywordsMap[key],"g"),key.replace(/\\/g,"\\\\"));
            }
            return strToDecode;
        },
        /*
            * As search allow phrase search like "project manager", "java developer", but
            * before SearchExpressionGenerator translate input string, it will split and handle input
            * by sparated words, this function is used to recreated those phrases.
            */
        createPhraseList:function(words){
            var phraseList = new Array();
            var isFindingPhrase=false;
            var tmpPhrase="",completedPhrase=""
            for(var i=0;i<words.length;i++){
                if(isFindingPhrase){
                    if(words[i][words[i].length-1]=='"'){
                        completedPhrase=tmpPhrase+" "+words[i]
                        phraseList.push(completedPhrase.replace(/^"|"$/g,""))
                        tmpPhrase="";
                        isFindingPhrase = false;
                    }else{
                        tmpPhrase+=" "+words[i];
                    }
                }else{
                    if (words[i][0] == '"') {
                        isFindingPhrase=true;
                        tmpPhrase=words[i];
                    }else{
                        phraseList.push(words[i])
                    }
                }
            }
            if(tmpPhrase.length!=0)
                phraseList.push(tmpPhrase.replace(/^"|"$/g,""));
            return phraseList;
        },
        /*
            * Generate Postfix notation according to input string array.
            * Each
            *
            */
        generatePostfixNotation:function(expr){
            var processingQueue = new Array();
            var processingUnit = "";
            var encodedProfixNotationQueue = new Array();
            var decodedProfixNotationQueue = new Array();
            var operatorStack = new Array();
            var meetOpenQuote = false;
            var quotedPhrase = ""; // Processing unit can be an operator or an operand
            for (var i=0;i<expr.length;i++){
                processingQueue = this.generateProcessingQueue(expr[i]);
                for(var j=0;j<processingQueue.length;j++){
                    processingUnit = processingQueue[j];
                    if (this.isAnOperand(processingUnit)){
                        // If whitespace is not quoted, ignore it.
                        if(!processingUnit.match(/\s+/g) || meetOpenQuote)
                            encodedProfixNotationQueue.push(processingUnit);
                    }
                    if(processingUnit == "("){
                        operatorStack.push(processingUnit);
                    }else if(processingUnit == ")"){
                        while(operatorStack.length>0 && operatorStack[operatorStack.length-1]!="("){
                            encodedProfixNotationQueue.push(operatorStack.pop())
                        }
                        operatorStack.pop() // Pop the left parenthesis from stack and discard it.
                    }else if(this.isAStringOperator(processingUnit)){
                        if(meetOpenQuote==false && processingUnit == '\"'){
                            encodedProfixNotationQueue.push(processingUnit);
                            meetOpenQuote=true
                        }else if(meetOpenQuote==true && processingUnit == '\"'){
                            quotedPhrase="";
                            while(encodedProfixNotationQueue.length>0 && encodedProfixNotationQueue[encodedProfixNotationQueue.length-1]!='"'){
                                quotedPhrase = encodedProfixNotationQueue.pop() + quotedPhrase;
                            }
                            encodedProfixNotationQueue.pop() // Discard open quote
                            encodedProfixNotationQueue.push(quotedPhrase);
                            meetOpenQuote=false
                        }else if(encodedProfixNotationQueue.length>0 && (j==0 && !expr[i].match(/^\s+/) || j>0)){
                            // If input key word is "java\*", "java" and "\*" will be separately as keyword will be
                            // split by program using word boundary \b before constructing the match expression.
                            //
                            // In order to reconstruct this evaluation unit, which is concatenating "java" and "\*",
                            // we use following code.
                            encodedProfixNotationQueue.push(encodedProfixNotationQueue.pop()+processingUnit);
                        }else if(encodedProfixNotationQueue.length==0){
                            encodedProfixNotationQueue.push(processingUnit);
                        }
                    }else if(this.isALogicOperator(processingUnit)){
                        if(operatorStack.length==0 || operatorStack[operatorStack.length-1]=="("){
                            operatorStack.push(processingUnit);
                        }else{
                            while(operatorStack.length>0 && operatorStack[operatorStack.length-1]=="("
                            && this.logicOperatorPrecedence.indexOf(processingUnit)<=this.logicOperatorPrecedence.indexOf(operatorStack[operatorStack.length-1])){
                                encodedProfixNotationQueue.push(operatorStack.pop());
                            }
                            operatorStack.push(processingUnit) // Push the latest operator on to stack
                        }
                    }

                }
            }
            while(operatorStack.length>0){
                encodedProfixNotationQueue.push(operatorStack.pop());
            }
            for(var i=0;i<encodedProfixNotationQueue.length;i++){
                decodedProfixNotationQueue.push(this.decodeValidNonStrKeywords(encodedProfixNotationQueue[i]))
            }
            return decodedProfixNotationQueue;
        },
        /*
            * Generate proccessing queue from each token
            */
        generateProcessingQueue:function(exprToken){
            var afterTrimStr="",
                processingQueue=new Array();
            /*
                * When the input is 'java developer', exprToken comes in as 'java' or 'developer'. For those tokens, as they don't have
                * operator meaning, so just need to add them straight into the processing Queue.
                *
                * When the input is '( java\*) AND ( Developer )', exprToken comes in as ' \*) '. As for each char in ' \*) ',
                * it is an operator, we need to push them separately into processing queue to make sure each of them will be properly
                * handled in the later stage.
                *
                * Whitespace in the exprToken will be removed. eg. ' \*)' will be trim to '\*)' before start generating
                * processing queue.
                */
            if(exprToken.match(/\s*[^\w\s]\s*/g)){
                afterTrimStr = exprToken.trim();
                for(var i=0;i<afterTrimStr.length;i++){
                    processingQueue.push(afterTrimStr[i]);
                }
            }else{
                processingQueue.push(exprToken); // Otherwise add whole token to processingQueue
            }
            return processingQueue;
        },
        isAStringOperator:function(op){
            return this.strOperators.indexOf(op)>=0
        },
        isALogicOperator:function(op){
            return this.logicOperatorPrecedence.indexOf(op)>=0
        },
        isAnOperand:function(op){
            return !this.isAStringOperator(op) && !this.isALogicOperator(op) && op!="(" && op!=")";
        },
        getJSLogicOperator:function(op){
            return this.logicOperatorMap[op];
        }

    };



}

angular.module('CTC.People.ScreenCandidates')
    .directive('candidateList',[
        'urlPrefixService',
        '$uibModal',
        '$window',
        '$q',
        'placementCandidateService',
        '$filter',
        'Notification',
        'redirectService',
        'orderByFilter',
        function (urlPrefixService, $uibModal, $window, $q, placementCandidateService, $filter, Notification, redirectService, orderByFilter) {
        return {
            restrict: 'E',
            scope: {
                clients: '=',
                allVacancyCandidateList: '=',
                //searchKeyword: '=',
                candidateStatusPicklist: '=',
                currentPageCandidateList: '=',
                confirmUpdateFlag: '=',
                displayColumn: '=',
                fitlerFieldOptions: '=',
                selectedObjectList: '=',
                isShortlistView: '=',
                uiState:'=',
                propertyName: '=',
                objectLabelNames: '=',
                reverse: '=',
                originClientsFiltered: '=',
                isChangeView: '=',
                onAddToShortlist: '&',
                onRemoveFromShortlist: '&',
                onSendEmail: '&',
                hasClientSelected: '=',
                isBulkEmailEnabled: '=',
                isScreenCandidateNoteEnabled: '='
            },
            templateUrl: 'candidateList/candidateList.html',
            link: function (scope, element, attr) {
                scope.setColumnClass = function (colIndex, columnList) {
                    var numberOfCol = columnList.length;
                    var columnClass = "col-sm-2";
                    switch (numberOfCol) {
                        case 1:
                            columnClass = "col-sm-8";
                            break;
                        case 2:
                            columnClass = "col-sm-4";
                            break;
                        case 3:
                            if (colIndex === 0) {
                                columnClass = "col-sm-2";
                            } else {
                                columnClass = "col-sm-3";
                            }
                            break;
                        case 4 :
                        default:
                            columnClass = "col-sm-2";
                            break;
                    }
                    return columnClass;
                };
            },
            controller: candidateListController
        };
    }]);

angular.module('CTC.People.ScreenCandidates').directive('ngBlur', function() {
  return function( scope, elem, attrs ) {
    elem.bind('blur', function() {
      scope.$apply(attrs.ngBlur);
    });
  };
});

angular.module('CTC.People.ScreenCandidates').directive('ngFocus', function( $timeout ) {
  return function( scope, elem, attrs ) {
    scope.$watch(attrs.ngFocus, function( newval ) {
      if ( newval ) {
        $timeout(function() {
          elem[0].focus();
        }, 0, false);
      }
    });
  };
});

