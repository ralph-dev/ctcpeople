/**** manage other vacancy search ****/
(function () {
    "use strict";
    function addToOtherVacancyController($scope, $uibModalInstance, searchService, busyLoader, vacancyService, shortList, orderByFilter, $window,helperService) {
        $scope.propertyName = 'Name';
        $scope.reverse = true;

        function init() {
            $scope.shortList = shortList;
            checkAll($scope.selectAllCandidate, $scope.shortList);
        }
        /**
         * search for vacancy by name
         * @param searchText
         */
        function searchForVacancy(searchText) {
            busyLoader.start();
            vacancyService.searchVacancyByName(searchText)
                .then(function (data) {
                    $scope.vacancySearchResult = orderByFilter(data, $scope.propertyName, $scope.reverse);
                    $scope.propertyName = 'Name';
                    $scope.reverse = true;
                    busyLoader.end();
                });
        }

        function addToOtherVacancy(vacancyList){
            busyLoader.start();
            var selectedList = vacancyList.filter(function (item) {
                return item.selected === true ;
            });
            var targetVacancyList = selectedList.map(function (item) {
                return item.Id;
            });
            var shortList = $scope.shortList.filter(function (item) {
                return item.selected === true;
            });
            vacancyService.addContactToVacancy(targetVacancyList, shortList)
                .then(function (data) {
                    console.log("add to other vacancy success");
                    $scope.addSuccessFlag = true;
                    busyLoader.end();
                });
        }

        function checkAll(selectAll, targetList){
            if (selectAll) {
                selectAll = true;
            } else {
                selectAll = false;
            }
            targetList.forEach(function (item) {
               item.selected = selectAll;
            });

        }

        function changeShortlistItemSelection(item){
            if (!item.selected){
                $scope.selectAllCandidate = false;
            }
        }

        function checkIfContainSelected(list) {
            if (!Array.isArray(list)) {
                return false;
            } else {
                return list.some(function (item) {
                    return item.selected === true;
                });
            }
        }

        function naveToPage(objectId) {
        	return helperService.naveToPage($window,objectId);
//            var targetUrl = $window.location.origin;
//            targetUrl += "/" + objectId;
//            return targetUrl;
        }

         /*********************
         * Column Sorting
         ********************/

        function sortBy(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName) ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
            $scope.vacancySearchResult = orderByFilter($scope.vacancySearchResult, $scope.propertyName, $scope.reverse);
        }

        /******* map scope data **********/
        $scope.shortList = [];
        $scope.addSuccessFlag = false;
        $scope.selectAllCandidate = true;
        $scope.addToOtherVacancy = addToOtherVacancy;
        $scope.searchForVacancy = searchForVacancy;
        $scope.checkAll = checkAll;
        $scope.changeShortlistItemSelection = changeShortlistItemSelection;
        $scope.checkIfContainSelected = checkIfContainSelected;
        $scope.sortBy = sortBy;
        $scope.naveToPage = naveToPage;
        init();

    }

    angular.module("CTC.People.ScreenCandidates").controller("AddToOtherVacancyController", ["$scope", "$uibModalInstance", "searchService", "busyLoader", "vacancyService", "shortList", "orderByFilter", "$window","helperService", addToOtherVacancyController]);
}());
