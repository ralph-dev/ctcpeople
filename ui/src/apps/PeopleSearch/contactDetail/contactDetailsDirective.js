(function () {
    "use strict";
    function contactDetail(searchService, $window, busyLoader,helperService) {
        return {
            restrict: "EA",
            scope: {
                targetRecord: "=",
                renderFlag: "=",
                searchKeyword: "="
            },
            templateUrl: "contactDetail/contactDetailView.html",
            link: function (scope, element, attr) {


                function removeExcludedWords(targetString){
                    var excludeList = "\\b(NOT|AND|OR)\\b";
                    var re = new RegExp(excludeList, "ig");
                    targetString.replace(re, "");
                    return targetString;
                }

                function captureQuotedText(str){
                    var re = /(\"[\w\s]+\")/g;
                    var m;
                    var result = [];
                    while ((m = re.exec(str)) !== null) {
                        if (m.index === re.lastIndex) {
                            re.lastIndex++;
                        }
                        // View your result using the m-variable.
                        // eg m[0] etc.
                        // remove double quotation.
                        var match = m[0].replace(/(\")/g, "");
                        result.push(match);
                    }

                    console.log("result", result);
                    return result;
                }

                /**
                 * use to add escape char to strings contain illegal chars
                 * @param stringToGoIntoTheRegex
                 * @returns {*}
                 */
                function escapeRegExp(string){
                    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
                }

                function highlightResume(targetString, snippets, searchKeywords) {
                    // precess keywords
                    var result = targetString;
                    // remove exclude keywords
                    var keywords = removeExcludedWords(searchKeywords);
                    // capture quoted texts
                    var quotedText = captureQuotedText(keywords);
                    // remove quoted text
                    keywords = keywords.replace(/(\"[\w\s]+\")/g, "");
                    // remove excluded operator
                    keywords = keywords.replace(/\b(NOT|OR|AND)\b/ig, "");
                    // remove brackets
                    keywords = keywords.replace(/(\)|\()/ig, "");
                    keywords = escapeRegExp(keywords);
                    // remove extra space and add | as divider
                    keywords = keywords.replace(/[\s]+/g, "|");
                    // remove || from string
                    keywords = keywords.replace(/^\||\|$/g, "");

                    var combineText = "";
                    if(keywords.length > 1){
                        combineText += keywords;
                    }
                    if(quotedText.length > 0){
                        var joinQuotedText = quotedText.join("|");
                        if(combineText.length > 1){
                            combineText += "|" + joinQuotedText;
                        }
                        else {
                            combineText += joinQuotedText;
                        }
                    }
                    if(combineText.length > 1){
                        var queryText = "(\\b|(?!\\w))(" + combineText.trim() + ")(\\b|(?!\\w))";
                        console.log("query text", queryText);
                      //  var queryText = "([^\\w])(" + combineText.trim() + ")([^\\w])";
                        var regexQuery = new RegExp(queryText, "ig");
                        result = targetString.replace(regexQuery, function(match){
                            return "<mark> " + match +"</mark>";
                        });
                    }

                    //snippets.forEach(function (snippet) {
                    //    var formattedSir = escapeRegExp(snippet);
                    //    var  snippetReg = new RegExp(formattedSir, "gim");
                    //    var foundIndex = str.search(snippetReg);
                    //    // todo add the new process
                    //    // use replace in place of substring
                    //    if (foundIndex !== -1) {
                    //        str = str.substring(0, foundIndex) + "<mark>" + str.substring(foundIndex, (foundIndex + snippet.length)) + "</mark>" + str.substring((foundIndex + snippet.length));
                    //    }
                    //});

                    return result;
                }


                function init() {
                    var parentWidth = element.parent().parent().width();
                    element.find("div.recordDetailsTabs").css("width", (parentWidth - 5));
                    // get details
                    busyLoader.start();
                    searchService.getContactDetail(scope.targetRecord.Contact.Id)
                        .then(function (data) {
                            if (angular.isDefined(data.longTextField.Resume__c)) {
                                var temp = "";
                                //
                                if (angular.isDefined(scope.targetRecord.MatchResult.snippets) || (scope.searchKeyword && scope.searchKeyword.length > 1)) {
                                    // add formatted data
                                    temp = highlightResume(data.longTextField.Resume__c, scope.targetRecord.MatchResult.snippets, scope.searchKeyword);
                                } else {
                                    temp = data.longTextField.Resume__c;
                                }
                                //replace newLine with html linebreak tag
                                scope.resume = temp.replace(/\n/g, '<br>');
                            }
                            scope.recordManagement = data.recordManagement;
                            scope.activitiesHistory = data.activitiesHistory;
                            scope.recordDetails = data.recordDetails;
                        })
                        .finally(function () {
                            busyLoader.end();
                        });
                }

                scope.naveToPage = function (targetId) {
//                    var targetUrl = $window.location.origin;
//                    targetUrl += "/" + targetId;
                    
                    //$window.open(targetUrl);
                    helperService.openWindowForSObject($window, targetId,"_blank");

                };

                scope.$watch('renderFlag', function (newVal, oldVal) {
                    if (newVal === true) {
                        init();
                    }
                });
            }
        };
    }

    angular.module("CTC.People.PeopleSearch").directive("contactDetail", ["searchService", "$window", "busyLoader", "helperService",contactDetail]);
}());
