(function () {
    "use strict";
    function skillSearchService($rootScope, $q, $filter, RemoteModelObject) {
        var peopleSearchExtension = RemoteModelObject.create("PeopleSearch", {});
        var SKILLS_LIST = [];

        function getSkillsList() {
            return peopleSearchExtension.actions.getAllSkillsList()
                .then(function (data) {
                    return data;
                });
        }

        function isSkillSearchEnabled() {
            return peopleSearchExtension.actions.isSkillSearchEnabled()
                .then(function (data) {
                    return data;
                });
        }

        return {
            getSkillsList: getSkillsList,
            isSkillSearchEnabled: isSkillSearchEnabled
        };
    }

    angular.module("CTC.People.PeopleSearch").service("skillSearchService", ["$rootScope", "$q", "$filter", "RemoteModelObject", skillSearchService]);
}());
