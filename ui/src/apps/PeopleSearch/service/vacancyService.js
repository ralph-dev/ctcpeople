/**
 * manage vacancy related remote action calls
 */
(function(){
    "use strict";
    function vacancyService($q, RemoteModelObject){
        var vacancyExtension = RemoteModelObject.create("Vacancy", {});
        var placementCandidateExtension = RemoteModelObject.create("PlacementCandidate", {});

        function getVacancyDetail(vacancyId) {
            return vacancyExtension.actions.getVacancy(vacancyId)
                .then(function (data) {
                    return data;
                });
        }

        /**
         * add contact list to vacancy
         * @param vacancyId String Id
         * @param contactList Array of Objects contains the property Id
         * @returns {Function} promise
         */
        function addContactToVacancy(vacancyId, contactList) {
            var deferred = $q.defer();
            var vacancyList = [];
            var recordList = [];
            if (Array.isArray(contactList)) {
                contactList.forEach(function (record) {
                    if (record.Id) {
                        recordList.push(record.Id);
                    }
                });
            }

            if(Array.isArray(vacancyId)){
                vacancyList = vacancyId;
            }else {
                vacancyList.push(vacancyId);
            }
            // add data if the list is not empty
            if (recordList.length > 0 && vacancyList.length > 0) {
                placementCandidateExtension.actions.createRecords(recordList, vacancyList)
                    .then(function (data) {
                        deferred.resolve(data);
                    });
            }
            if (!Array.isArray(contactList) || !vacancyId || recordList.length < 1) {
                deferred.reject("data error");
            }
            return deferred.promise;

        }

        function searchVacancyByName(stringName){
            return vacancyExtension.actions.searchVacancyByName(stringName)
                .then(function(data){
                    return data;
                });
        }

        function getVacancyAndRelatedCandidates(vacancyId) {
            return vacancyExtension.actions.getVacancyAndRelatedCandidates(vacancyId)
                .then(function(data) {
                    return data;
                });
        }

        return {
            getVacancyDetail: getVacancyDetail,
            addContactToVacancy: addContactToVacancy,
            searchVacancyByName : searchVacancyByName,
            getVacancyAndRelatedCandidates: getVacancyAndRelatedCandidates
        };
    }
    angular.module("CTC.People.PeopleSearch").service("vacancyService", ["$q", "RemoteModelObject", vacancyService]);
}());
