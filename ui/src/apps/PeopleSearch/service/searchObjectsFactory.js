/***
 * searchObjectsService: manage search objects definitions with there getter and setter
 */
(function () {
    "use strict";
    function searchObjectsService($rootScope) {

        var FIELD_TYPE_ENUM = {
            BOOLEAN: "BOOLEAN",
            DATE: "DATE",
            DATETIME: "DATETIME",
            REFERENCE: "REFERENCE",
            MULTIPICKLIST: "MULTIPICKLIST",
            PICKLIST : "PICKLIST",
            DOUBLE: "DOUBLE",
            NUMBER: "NUMBER",
            INTEGER: "INTEGER",
            DECIMAL: "DECIMAL",
            CURRENCY: "CURRENCY",
            PERCENT : "PERCENT"
        };

        function FieldTypeEnum(){
            return FIELD_TYPE_ENUM;
        }
        /**
         * object reference to the metadata fields
         * @constructor
         */
        function FieldObject() {
            this.apiName = null;
            /* reference to SF Field name */
            this.label = null;
            /* field display name */
            this.type = null;
            /* field type (e.g; picklist)*/
            this.picklistValues = null;
            /* optional if type require set values (e.g; picklist)*/
            this.sequence = null;
            /*field sequence for display legacy may be remove later */
            // this.typeObject = null; /* contains the type and the available operators to be add later in the process*/
        }

        /**
         * object represent field type supported by the system
         * @constructor
         */
        function FieldTypeObject() {
            this.type = null;
            /* the type we supports e.g: pickList, Date, String, multiplePickList */
            this.operators = [];
            /* list of operators supported. operator object {label: "Operator name", value: "operator value } */
        }

        /**
         * represent a saved search template object
         * @constructor
         */
        function DefaultSearchTemplate(){
            this.Id = -1;
            this.Name =  "Default Search";
            this.Unique_Search_Record_Name__c = "Default Search";
            this.ascomponents__c =  null;
        }

        /**
         * represent the  metadata field in the search form and search template
         * @constructor
         */
        function CriteriaObject() {
            this.field = null;
            /*field name ref apiName form FieldObject */
            this.type = null;
            /* type of the field */
            this.operator = null;
            /* selected operator*/
            this.values = null;
            /* selected value */
            // this.fieldObject = null; /* map to field definition to be add later*/
        }

        /**
         * refrence the structure of the SkillsQuery Object required by frontend and  send to backend
         * @constructor
         */
        function SkillsSearchObject(){
            this.query = "";
            this.verifiedSkills =  false;
        }

        function createSkillsSearchObject(){
            return new SkillsSearchObject();
        }

        function QueryObject() {
            this.criteria = null;
            /*list of criteria objects see CriteriaObject for ref  */
            this.resumeKeywords = null;
            /*string contain the value from search resume by keyword Daxtra */
            this.availability = null;
            /* contain availability search object structure to be confirmed */
            this.skillsSearch = new SkillsSearchObject();
            /* list of skills to be parsed object structure {query:"text skills query", verifiedSkills: boolean}   */
        }

        function createCriteria(field, type, operator, values) {
            var criteria = new CriteriaObject();
            if (angular.isDefined(field)) {
                criteria.field = field;
            }
            if (angular.isDefined(type)) {
                criteria.type = type;
            }
            if (angular.isDefined(operator)) {
                criteria.operator = operator;
            }
            if (angular.isDefined(values)) {
                criteria.values = values;
            }
            return criteria;
        }

        /**
         * reference search result display column object
         * @constructor
         */
        function DisplayColumn() {
            this.sortable = null;
            this.referenceFieldName = null;
            this.Order = null;
            this.isCustomField = false;
            this.fromasc = null;
            this.fieldtype = null; /* field type e.g: STRING */
            this.Field_api_name = null;
            this.defaultorder = null;
            this.ColumnName = null;
        }

        /**
         * Filter criteria list from display to only include fields with valid value and operator
         * @param criteriaList send from display
         * @returns {Array} list of valid criteria
         */
        function filterCriteriaForQuery(criteriaList) {
            /**
             * check if the item contains valid operator
             * @param item criteria object
             * @returns {boolean} true if operator exist
             */
            function isValidOperator(item){
                if (typeof item.operator !== 'undefined' && item.operator !== "" && item.operator !== null) {
                    return true;
                }
                return false;
            }
            var result = [];
            // remove all criteria with empty operator
            var tempCriteria = criteriaList.filter(isValidOperator);

            tempCriteria.forEach(function (item) {
                var validCriteria = false;
                // check against the value
                if (typeof item.values !== 'undefined' && item.values !== "" && item.values !== null) {

                    if ((item.type === FIELD_TYPE_ENUM.DATE || item.type === FIELD_TYPE_ENUM.DATETIME)) {
                        if ((item.values.fromDate !== "" && item.values.fromDate !== null)) {
                            if((item.values.toDate !== "" && item.values.toDate !== null)){
                                validCriteria = true;
                            }
                        }
                    }else if ((item.type === FIELD_TYPE_ENUM.PICKLIST || item.type === FIELD_TYPE_ENUM.MULTIPICKLIST)){
                        // find if contains null value and remove it
                        var nullIndex = -1;
                        var foundNullVal = item.values.some(function(val, index){
                            if(val === null || val === "null"){
                                nullIndex = index;
                                return true;
                            }
                            return false;
                        });
                        if(foundNullVal){
                            item.values.splice(nullIndex, 1);
                        }
                        if(item.values.length > 0){
                            validCriteria = true;
                        }
                    }else{
                        validCriteria = true;
                    }
                }
                // check for spacial operators
                if(item.operator === "ie" || item.operator === "ine"){
                    item.values = null;
                    validCriteria = true;
                }
                // if valid criteria add to result
                if (validCriteria) {
                    result.push(createCriteria(item.field, item.type, item.operator, item.values));
                }
            });
            return result;
        }

        /**
         * filter the criteria list for template, return criteria if value or operator is set
         * @param criteriaList
         */
        function filterCriteriaForTemplate(criteriaList){
            // filter criteria list to return only fields with value
            var list = criteriaList.filter(function(item){
                if(item.operator || item.values){
                    if(item.type === FIELD_TYPE_ENUM.BOOLEAN && item.values === null){
                        return false;
                    }else {
                        return true;
                    }
                }
            });
            // clear un necessary data and return criteria object
            return list.map(function(item){
                return createCriteria(item.field, item.type, item.operator, item.values);
            });

        }

        /**
         * create a search Query for submit search, remove any empty data.
         * @param keywords
         * @param criteriaList
         * @param skills
         * @param availability
         * @returns {QueryObject}
         */
        function createSearchQuery(keywords, criteriaList, skills, availability) {
            var searchQuery = new QueryObject();
            if (angular.isDefined(keywords) && keywords) {
                searchQuery.resumeKeywords = keywords;
            }
            if (angular.isDefined(criteriaList) && criteriaList.length > 0) {
                searchQuery.criteria = filterCriteriaForQuery(criteriaList);
            }
            /*TODO: complete this section to handle availibility */
            if (angular.isDefined(availability)) {
                searchQuery.availability = availability;
            }
            /*TODO: complete this section to handle skill search */
            if (angular.isDefined(skills)) {
                searchQuery.skillsSearch = skills;
            }

            return searchQuery;
        }

        /**
         * create a search query for template
         * @param keywords
         * @param criteriaList
         * @param availability
         * @param skills
         * @returns {QueryObject}
         */
        function createSearchQueryForTemplate(keywords, criteriaList, skills, availability) {
            var searchQuery = new QueryObject();
            if (angular.isDefined(keywords) && keywords) {
                searchQuery.resumeKeywords = keywords;
            }
            if (angular.isDefined(criteriaList) && criteriaList.length > 0) {
                searchQuery.criteria = filterCriteriaForTemplate(criteriaList);
            }
            /*TODO: complete this section to handle availibility */
            if (angular.isDefined(availability)) {
                searchQuery.availability = availability;
            }
            /*TODO: complete this section to handle skill search */
            if (angular.isDefined(skills)) {
                searchQuery.skillsSearch = skills;
            }

            return searchQuery;
        }

        function defaultSearchTemplate(templateData){
            var template = new DefaultSearchTemplate();
            if(templateData !== undefined){
                for (var prop in template){
                    if(templateData.hasOwnProperty(prop)){
                        template[prop] = templateData[prop];
                    }
                }

            }
            return template;
        }

        /**** public functions  ********/
        return {
            createCriteria: createCriteria,
            createSearchQuery: createSearchQuery,
            createSkillsSearchObject : createSkillsSearchObject,
            defaultSearchTemplate : defaultSearchTemplate,
            createSearchQueryForTemplate:createSearchQueryForTemplate,
            FieldTypeEnum : FieldTypeEnum
        };
    }

    angular.module("CTC.People.PeopleSearch").factory("searchObjectsFactory", ["$rootScope", searchObjectsService]);
}());


