(function(){

    function searchTemplateDirective(){
        return{
            strict: "EA",
            templateUrl: "searchTemplate/searchTemplate.html",
            scope:{
                onDelete: "&",
                onSelect: "&",
                sourceLists: "=",
                preSelectedItem: "=?"
            },
            link: function(scope, element, attr){

                scope.selectedItem =  -1;
                scope.$watch("preSelectedItem", function(newVal){
                    if(newVal !== undefined){
                        scope.selectedItem = scope.preSelectedItem;
                    }

                });

                scope.deleteItem = function(ob){
                    if(angular.isFunction(scope.onDelete)){
                        scope.onDelete()(ob);
                    }
                };

                scope.selectItem = function(ob) {
                    if(angular.isFunction(scope.onSelect)){
                        scope.onSelect()(ob);
                    }
                };
            }
        };
    }
    angular.module("CTC.People.PeopleSearch").directive("searchTemplate",[searchTemplateDirective]);
}());
