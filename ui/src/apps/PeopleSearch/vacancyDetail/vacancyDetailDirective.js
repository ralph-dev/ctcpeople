(function(){

    function vacancyDetailDirective(vacancyService){
        return{
            strict: "EA",
            templateUrl: "vacancyDetail/vacancyDetailView.html",
            scope:{
                vacancyId: "="
            },
            link: function(scope, element, attr){
                function init(vacancyId){
                    vacancyService.getVacancyDetail(vacancyId).then(function(data){
                        scope.vacancyDetail = data;
                    });
                }

                /* TODO: manage rendering data if vacancy is not defined ***/
                if (scope.vacancyId){
                    init(scope.vacancyId);
                }


            }
        };
    }
    angular.module("CTC.People.PeopleSearch").directive("vacancyDetail",["vacancyService", vacancyDetailDirective]);
}());
