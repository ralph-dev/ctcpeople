(function () {
    "use strict";
    function skillsFoundCtrl($filter) {
        var ctrl = this;

        function selectSkill(item) {
            ctrl.onSelect({skill: item});
        }

        function onSkillsListChange(change) {
            console.log("change", change);
            if (change.skillsList) {
                ctrl.skillsSet = $filter('groupBy')(change.skillsList.currentValue, 'skillGroup.groupName');
            }
        }

        /******** public mapping ***********/
        ctrl.selectSkill = selectSkill;
        ctrl.$onChanges = onSkillsListChange;
    }

    angular.module("CTC.People.PeopleSearch").component("skillsFound", {
        templateUrl: "skillSearch/skillsFoundComponent.html",
        controller: skillsFoundCtrl,
        bindings: {
            skillsList: "<",
            onSelect: "&"
        }
    });
}());
