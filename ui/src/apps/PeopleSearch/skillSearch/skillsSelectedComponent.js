(function () {
    "use strict";
    function skillsSelectedCtrl($scope) {
        var ctrl = this;

        var operatorEnum = {
            AND:"AND",
            OR:"OR",
            NOT:"NOT"
        };

        /**
         * remove skill and associated operator
         * @param item
         */
        function removeSkill(item) {
            console.log(item);
            ctrl.onSkillRemoved({skill: item});
        }

        function onSkillsListChange(change) {
            console.log("change selected", change);
            if (change.skillsList) {
                //   angular.copy(change.skillsList.currentValue,  ctrl.skillsQuery);
                ctrl.skillsQuery = angular.copy(ctrl.skillsList);
                console.log("changed Data", ctrl.skillsQuery, ctrl.skillsList);
                //ctrl.skillsQuery = change.skillsList.currentValue;
            }
        }

        function toggleOperator(skillOb){
            var nextOperator = null;
            switch (skillOb.operator){
                case operatorEnum.AND:
                    nextOperator = operatorEnum.OR;
                    break;
                case operatorEnum.OR:
                    nextOperator = operatorEnum.NOT;
                    break;
                case operatorEnum.NOT:
                    nextOperator = operatorEnum.AND;
                    break;
            }
            console.log("next op", nextOperator);

            skillOb.operator = nextOperator;
            ctrl.onSkillUpdate({skill: skillOb, operator: nextOperator});
        }

        function updateOrder(){
            // TODO: check if syntactics is correct and valid other wise reload original
            // reload original
            // ctrl.skillsQuery = angular.copy(ctrl.skillsList);
            // else update original data
            // on reOrder set first item operator to And
            var skillOb =  ctrl.skillsQuery[0];
           skillOb.operator = operatorEnum.AND;
            console.log("new order", ctrl.skillsQuery);
            var temp = angular.copy(ctrl.skillsQuery);
            $scope.$apply(function(){
                ctrl.onSkillsReorder({list: ctrl.skillsQuery});
            });

        }

        /******** public mapping ***********/
        ctrl.$onChanges = onSkillsListChange;
        ctrl.removeSkill =  removeSkill;
        ctrl.toggleOperator = toggleOperator;
        ctrl.updateOrder = updateOrder;

    }

    angular.module("CTC.People.PeopleSearch").component("skillsSelected", {
        templateUrl: "skillSearch/skillsSelectedComponent.html",
        controller: skillsSelectedCtrl,
        bindings: {
            skillsList: "<",
            onSkillRemoved: "&",
            onSkillUpdate: "&",
            onSkillsReorder: "&"
        }
    });
}());
