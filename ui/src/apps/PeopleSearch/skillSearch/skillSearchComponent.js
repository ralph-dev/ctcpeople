(function () {
    "use strict";
    function skillSearchController($filter) {
        var ctrl = this;
        var operatorEnum = {
            AND: "AND",
            OR: "OR",
            NOT: "NOT"
        };

        var completeSkillsList = [];

        function searchSkills() {
            if (ctrl.skillKeyword.trim().length < 2) {
                ctrl.skillsFound = [];
            } else {
                var keyword = ctrl.skillKeyword.trim().toLowerCase();
                ctrl.skillsFound = ctrl.skillsList.filter(function (item) {
                   var skillName = item.skillName.toLowerCase();
                    var groupName = item.skillGroup.groupName.toLowerCase();
                    if (skillName.indexOf(keyword) !== -1) {
                        return true;
                    }
                    if (groupName.indexOf(keyword) !== -1) {
                        return true;
                    }
                    return false;
                });
              //  ctrl.skillsFound = $filter('filter')(ctrl.skillsList, ctrl.skillKeyword);
            }
        }

        /**
         *  get skill from skillsList and return
         * @param skillId
         * @returns {*}
         */
        function getSkillFromSkillsList(skillId){
            var skill = null;
            var idx = -1;
            var found = ctrl.skillsList.some(function (item, index) {
                if (item.skillId === skillId) {
                    idx = index;
                    return true;
                }
                return false;
            });
            if (found) {
                skill = angular.copy(ctrl.skillsList[idx]);
                ctrl.skillsList.splice(idx, 1);
            }
            return skill;
        }

        /**
         * convert string query into objects
         * @returns {Array}
         */
        function processSkillsQuery() {
            var result = [];
            if(ctrl.skillsCriteria.query) {
                if (ctrl.skillsCriteria.query.length > 1) {
                    var tempList = ctrl.skillsCriteria.query.trim().split(" ");
                    console.log("query array", tempList);
                    // add default operator to the first item
                    //TODO change logic to handle brackets
                    tempList.unshift(operatorEnum.AND);
                    for (var i = 0; i < tempList.length; i += 2) {
                        var skill = getSkillFromSkillsList(tempList[i + 1]);
                        if(skill){
                            var skillOb = new SelectedSkill(skill, tempList[i]);
                            result.push(skillOb);
                        }
                    }
                }
            }

            return result;
        }

        function resetSelectedSkills() {
            // clear search keyword
            ctrl.skillKeyword = "";
            // clear skills found / selected
            ctrl.skillsFound = [];
            // reset skills list
            ctrl.skillsList = angular.copy(completeSkillsList);
            ctrl.skillsSelected = processSkillsQuery();
        }

        function onChanges(change) {
            // assign values
            if (change.skillsCriteria) {
                console.log(ctrl.skillsCriteria);
                ctrl.skillsCriteria = angular.copy(ctrl.skillsCriteria);
                resetSelectedSkills();
            }
            if (change.skillsList) {
                completeSkillsList = angular.copy(ctrl.skillsList);
                ctrl.skillsList = angular.copy(ctrl.skillsList);
            }

        }

        /**
         * constructor the skill query string based on selected skills
         */
        function updateSkillsQuery() {
            var tempQuery = "";
            ctrl.skillsCriteria.query = '';
            console.log( ctrl.skillsSelected);
            for (var i = 0; i < ctrl.skillsSelected.length; i++) {
                var item = ctrl.skillsSelected[i];
                if (i === 0) {
                    tempQuery = item.skill.skillId;
                } else {
                    tempQuery += " " + item.operator + " " + item.skill.skillId;
                }
            }
            console.log("query", tempQuery);
            ctrl.skillsCriteria.query = tempQuery;
            updateSkillsCriteria();
        }


        function SelectedSkill(skill, operator) {
            this.skill = skill;
            this.operator = operator;
        }

        function addSkillToSelected(skill) {
            // add operator before skill
            var selectedSkill = new SelectedSkill(skill, operatorEnum.AND);
            var temp = angular.copy(ctrl.skillsSelected);
            temp.push(selectedSkill);
            ctrl.skillsSelected = temp;
            updateSkillsQuery();
        }

        function addSkill(skill) {
            console.log("remove", skill);
            var skillsList = angular.copy(ctrl.skillsList);
            var idx = -1;
            var found = skillsList.some(function (item, index) {
                if (item.skillId === skill.skillId) {
                    idx = index;
                    return true;
                }
                return false;
            });
            if (found) {
                skillsList.splice(idx, 1);
                ctrl.skillsList = skillsList;
                searchSkills();
                addSkillToSelected(skill);
            }
        }

        function removeSkill(skill) {
            var temp_selectedSkills = angular.copy(ctrl.skillsSelected);
            var idx = -1;
            var found = temp_selectedSkills.some(function (item, index) {
                if (item.skill.skillId === skill.skill.skillId) {
                    idx = index;
                    return true;
                }
                return false;
            });
            if (found) {
                temp_selectedSkills.splice(idx, 1);
                ctrl.skillsSelected = temp_selectedSkills;
                var temp_skillsList = angular.copy(ctrl.skillsList);
                temp_skillsList.push(skill.skill);
                ctrl.skillsList = temp_skillsList;
                searchSkills();
                updateSkillsQuery();
            }
        }

        function updateSkill(skillOb, operator) {
            var idx = -1;
            var found = ctrl.skillsSelected.some(function (item, index) {
                if (item.skill.skillId === skillOb.skill.skillId) {
                    idx = index;
                    return true;
                }
                return false;
            });
            if (found) {
                ctrl.skillsSelected[idx].operator = operator;
            }
            updateSkillsQuery();
        }

        function updateVerifiedValue() {
            // ctrl.skillsCriteria.verifiedSkills = ctrl.verifiedSkillsFlag;
            updateSkillsCriteria();
        }

        /**
         * iterate over skillsSelected array and reset all element based on provided list
         * @param skillsList
         */
        function reOrderSelectedSkills(skillsList) {
            for (var i = 0; i < ctrl.skillsSelected.length; i++) {
                var tempSkill = skillsList[i];
                ctrl.skillsSelected[i] = new SelectedSkill(tempSkill.skill, tempSkill.operator);
            }

            updateSkillsQuery();

        }

        function updateSkillsCriteria() {
            ctrl.onUpdateSkillsCriteria({
                skillQuery: {
                    skillsCriteria: ctrl.skillsCriteria
                }
            });
        }

        /**
         * clear selected skills list and reset found skills
         */
        function clearSelectedSkills(){
            // empty the selected skills
            ctrl.skillsSelected = new Array();
            ctrl.skillsList = angular.copy(completeSkillsList);
            searchSkills();
            updateSkillsQuery();
        }

        /**************/
        ctrl.$onChanges = onChanges;
        ctrl.skillKeyword = "";
        ctrl.skillsFound = [];
        // ctrl.skillsList = [];
        ctrl.skillsSelected = [];
        ctrl.verifiedSkillsFlag = false;
        ctrl.searchSkills = searchSkills;
        ctrl.addSkill = addSkill;
        ctrl.removeSkill = removeSkill;
        ctrl.updateSkill = updateSkill;
        ctrl.updateVerifiedValue = updateVerifiedValue;
        ctrl.reOrderSelectedSkills = reOrderSelectedSkills;
        ctrl.clearSelectedSkills = clearSelectedSkills;

    }

    angular.module("CTC.People.PeopleSearch").component("skillSearch", {
        bindings: {
            skillsCriteria: "<",
            skillsList: "<",
            onUpdateSkillsCriteria: "&"
        },
        templateUrl: "skillSearch/skillSearch.html",
        controller: skillSearchController
    });
}());
