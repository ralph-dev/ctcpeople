angular.module("CTC.People.PeopleSearch", ["CTC.Components"]);

angular.module("CTC.People.PeopleSearch")
    .config(function ($stateProvider, $urlRouterProvider) {
        "use strict";
        $urlRouterProvider.otherwise('/search');

        $stateProvider
            .state('search', {
                url: '/search',
                templateUrl: 'searchForm/searchFormView.html',
                controller: 'SearchFormController'
            })
            .state('search-result', {
                url: '/search-result',
                templateUrl: 'searchResult/searchResultView.html',
                controller: 'SearchResultController'
            });
    });


// OPTIONAL on run option
angular.module("CTC.People.PeopleSearch")
    .run(function ($rootScope, urlPrefixService, $location) {
        urlPrefixService.setPrefix();
        $rootScope.returnVacancyId = $location.query("id");
    });
