/***
 * partial directive to manage changing the search result grid columns
 *
 * @display-column : list of column used in the result gird
 * @field-List : list of fields can be used as display column
 * @on-update : function  with column list as parameter to be called when column list is changed.
 * */

(function () {
    "use strict";
    function resultGridColumnConfig($q) {
        return {
            restrict: "EA",
            templateUrl: "searchResult/searchResultColumnConfig.html",
            scope: {
                displayColumn: "=",
                fieldList: "=",
                onUpdate: "&"
            },
            link: function (scope, element, attr) {
                var MAX_COL_LENGTH = 4; // set the max of column to 4
                scope.$watch("displayColumn", function (newVal, oldVal) {
                    if (newVal && Array.isArray(newVal)){
                        scope.configColumnList = new Array(MAX_COL_LENGTH);
                        newVal.forEach(function(item, index){
                            scope.configColumnList[index] = item;
                        });
                    }
                });

                scope.updateColumn = function (columnList) {
                    if (angular.isDefined(scope.onUpdate) && angular.isFunction(scope.onUpdate)) {
                        // loop over list and remove $$hashKey
                        var updatedList =[];
                        columnList.forEach(function (item) {
                            if (item.$$hashKey) {
                                delete item.$$hashKey;
                            }
                            if(item.Field_api_name){
                                updatedList.push(item);
                            }
                        });

                        $q.when(scope.onUpdate()(updatedList)).then(function () {
                            console.log("column has been update");
                        });

                    }
                };
                scope.configColumnList = [];

            }
        };
    }

    angular.module("CTC.People.PeopleSearch").directive("columnConfig", ["$q", resultGridColumnConfig]);
}());
