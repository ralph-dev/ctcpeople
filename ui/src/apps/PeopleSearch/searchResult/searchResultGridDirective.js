/***
 * partial directive to display html only
 * manage result grid view
 * display result based on column config fix number of fields is seven plus action fields
 *
 * @attribute config-column list of column field and labels object description : {"Field_api_name":"API_NAME", "ColumnName":"LABEL", "sortable":null,  "Order":null,"fieldtype":"TYPE",}
 * */
(function () {
    "use strict";
    function resultGridView($compile, $templateRequest, $filter) {
        return {
            restrict: "EA",
            templateUrl: "searchResult/searchResultGrid.html",
            link: function (scope, element, attr) {
                //  var maxColumnNumber = 7;
                scope.showScoreField = false;
                // set sorting predictor
               scope.orderPredictor = {column: " ", reverse:false}; // default orderBy

                // if result contain keyword search show the score and set order by score
                scope.$watch("searchResult", function(newVal, oldVal){
                    if(newVal){
                        if(scope.searchResult.length > 0 && scope.searchResult[0].MatchResult.score){
                            /** code was added based on request from backend dev */
                            scope.orderPredictor.column = "MatchResult.score"; // this will cause the sorting to be ascending
                            scope.updateOrderBy("MatchResult.score");
                            scope.showScoreField = true;
                        }
                    }
                });

                scope.setColumnClass = function (colIndex, columnList) {
                    var numberOfCol = columnList.length;
                    var columnClass = "col-sm-2";
                    switch (numberOfCol) {
                        case 1:
                            columnClass = "col-sm-8";
                            break;
                        case 2:
                            columnClass = "col-sm-4";
                            break;
                        case 3:
                            if (colIndex === 0) {
                                columnClass = "col-sm-2";
                            } else {
                                columnClass = "col-sm-3";
                            }
                            break;
                        case 4 :
                        default:
                            columnClass = "col-sm-2";
                            break;
                    }
                    return columnClass;
                };

                scope.updateOrderBy = function(colName){
                    if(scope.orderPredictor.column === colName){
                        scope.orderPredictor.reverse = !scope.orderPredictor.reverse;
                    }
                    else{
                        scope.orderPredictor.reverse = false;
                    }
                    scope.orderPredictor.column = colName;
                };

                scope.setOrderByClass = function(column, orderPredictor){
                    var ascending =  "glyphicon-sort-by-attributes-alt";
                    var descending = "glyphicon-sort-by-attributes";
                    var result = "";
                    if(column === orderPredictor.column){
                        if(orderPredictor.reverse){
                            result = ascending;
                        }else{
                            result = descending;
                        }
                    }
                    return result;
                };

                // compile the template and create the directive
                //scope.$on(attr.reloadEvent, function () {
                //    // remove the directive content
                //    // re compile the grid template
                //    // reload the template
                //});

            }
        };
    }

    angular.module("CTC.People.PeopleSearch").directive("resultGrid", ["$compile", "$templateRequest", "$filter", resultGridView]);
}());
