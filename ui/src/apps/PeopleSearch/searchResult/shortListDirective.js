/***
 * partial directive to display shortList
 * @attribute config-column list of column field and labels object description : {"Field_api_name":"API_NAME", "ColumnName":"LABEL", "sortable":null,  "Order":null,"fieldtype":"TYPE",}
 * */

(function () {
    "use strict";
    function shortListDirective() {
        return {
            restrict: "EA",
            templateUrl: "searchResult/shortList.html",
            link: function (scope, element, attr) {
                // manage the list
            }
        };
    }

    angular.module("CTC.People.PeopleSearch").directive("shortList", [shortListDirective]);
}());
