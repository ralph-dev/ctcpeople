(function () {
    "use strict";
    function searchResultController($scope, $filter, $location, $window, $timeout, $state, $uibModal, searchService, searchObjectsFactory, vacancyService, busyLoader, urlPrefixService,helperService) {

        var SEARCH_RESULT = [];
        // TODO: add this to service
        var fieldTypeEnum = searchObjectsFactory.FieldTypeEnum();
        /******** pagination *************/
        $scope.totalItems = 1;
        $scope.currentPage = 1;
        $scope.maxSize = 3;
        $scope.itemsPerPage = 25;
        $scope.itemsPerPageOptions = [
            {value: 25, label: "Show 25"},
            {value: 50, label: "Show 50"},
            {value: 100, label: "Show 100"}
        ];
        $scope.currentCheckAllPage = [];
        $scope.endOfSearchResult = 0;
        $scope.isBulkEmailEnabled = false;
        $scope.isBulkSMSEnabled = false;
        //$scope.isCheckAll = false;
        /******* end of pagination *************/

        function init() {
            // get vacancy Id
            busyLoader.start();
            var vacancyId = $location.query("id");
            $scope.vacancyId = (angular.isDefined(vacancyId) ? vacancyId : null);

            searchService.getSearchResult()
                .then(function (result) {
                    if (!result || result.length < 1) {
                        // if no result redirect to search form
                        console.log("no result redirect to search");
                        $state.go("search");
                        return;
                    }
                    else {
                        searchService.getUserPermission()
                            .then(function (userPermission) {
                                $scope.isBulkEmailEnabled = userPermission.isBulkEmailEnabled;
                                $scope.isBulkSMSEnabled = userPermission.isBulkSMSEnabled;
                                return searchService.getContactFieldsList();
                            })
                            .then(function (fieldsList) {
                                // order result alphabetically
                                // add empty or null option
                                $scope.columnFieldList = fieldsList;
                                return searchService.getContactDisplayColumns();
                            })
                            .then(function (data) {
                                if (data.length > 4) {
                                    $scope.displayColumn = data.slice(0, 4);
                                } else {
                                    $scope.displayColumn = data;
                                }

                                return searchService.getSearchQuery();
                            })
                            .then(function (data) {
                                $scope.keywordSearch = data.resumeKeywords;
                                // get displayColumn
                                return searchService.getSearchResult();
                            })
                            .then(function (result) {
                                SEARCH_RESULT = result;
                                if (!SEARCH_RESULT || SEARCH_RESULT.length < 1) {
                                    // if no result redirect to search form
                                    console.log("no result redirect to search");
                                    $state.go("search");
                                    return;
                                }
                                $scope.totalItems = SEARCH_RESULT.length;
                                $scope.searchResult = SEARCH_RESULT;
                                //if (SEARCH_RESULT[0].MatchResult.score) {
                                //    $scope.searchResult = $filter('orderBy')(SEARCH_RESULT, "-MatchResult.score");
                                //} else {
                                //    $scope.searchResult = SEARCH_RESULT;
                                //}
                                // get vacancy details if vacancyId is set
                                if ($scope.vacancyId) {
                                    getVacancyAndRelatedCandidates($scope.vacancyId)
                                        .then(function (list) {
                                            $scope.vacancyAndRelatedCandidates = list;

                                        })
                                        .finally(busyLoader.end);
                                } else {
                                    busyLoader.end();
                                }
                            });
                    }
                });

        }


        function updateDisplayColumn(columnlist) {
            busyLoader.start();
            $scope.showConfig = false;
            $scope.displayColumn = [];
            // loop over column list remove $$hashkey add order number
            columnlist.forEach(function (item, index) {
                if (item.$$hashKey) {
                    delete item.$$hashKey;
                }
                item.Order = index;
            });
            searchService.updateDisplayColumn(columnlist)
                .then(function (data) {
                    if (data.length > 4) {
                        $scope.displayColumn = data.slice(0, 4);
                    } else {
                        $scope.displayColumn = data;
                    }
                    return searchService.reSubmitSearch();
                })
                .then(function (result) {
                    SEARCH_RESULT = result;
                    if (!SEARCH_RESULT || SEARCH_RESULT.length < 1) {
                        // if no result redirect to search form
                        $state.go("search");
                        return;
                    }
                    $scope.totalItems = SEARCH_RESULT.length;
                    if (SEARCH_RESULT[0].MatchResult.score) {
                        $scope.searchResult = $filter('orderBy')(SEARCH_RESULT, "-MatchResult.score");
                    } else {
                        $scope.searchResult = SEARCH_RESULT;
                    }
                    reSelectRecordsInResult();
                    busyLoader.end();
                }, function (data) {
                    console.log("error on search result", data);
                    busyLoader.end();
                });

            // required by the directive
            return true;
        }


        /***
         * get display value based on the column
         * @param record
         * @param column
         * @returns {string} display value
         */
        function displayRecordValue(record, column) {
            var displayValue = "";
            switch (column.fieldtype) {
                case fieldTypeEnum.REFERENCE:
                    if (record.Contact.hasOwnProperty(column.referenceFieldName)) {
                        displayValue = record.Contact[column.referenceFieldName].Name;

                    } else {
                        displayValue = record.Contact[column.Field_api_name];
                    }
                    break;
                case fieldTypeEnum.DATE:
                case fieldTypeEnum.DATETIME:
                    var tempValue = record.Contact[column.Field_api_name];
                    displayValue = $filter("date")(tempValue, "dd/MM/yyyy");
                    break;
                case fieldTypeEnum.CURRENCY:
                    displayValue = $filter('currency')(record.Contact[column.Field_api_name], '$', 2);
                    break;
                case fieldTypeEnum.PERCENT:
                    if (record.Contact[column.Field_api_name] && (record.Contact[column.Field_api_name] !== undefined || record.Contact[column.Field_api_name] !== null)) {
                        displayValue = record.Contact[column.Field_api_name] + " %";
                    }
                    break;
                default:
                    displayValue = record.Contact[column.Field_api_name];
                    break;
            }
            return displayValue;
        }

        /**
         * handel selecting current page result
         */
        function checkAll(recordList) {

            //$scope.isCheckAll = true;

            if ($scope.selectAll) {

                $scope.selectAll = true;

            } else {
                $scope.selectAll = false;

            }
            if (Array.isArray(recordList)) {
                recordList.forEach(function (item) {
                    item.selected = $scope.selectAll;
                    changeRecordSelection(item);
                });
            }

        }


        /**
         * handel deselecting all result
         */
        function deselectAllResult() {

            angular.forEach($scope.searchResult, function (item) {
                item.selected = false;
                changeRecordSelection(item);
            });

            $scope.selectAll = false;
        }

        /**
         * handel selecting all result
         */
        function selectAllResult() {

            $scope.selectAll = true;

            angular.forEach($scope.searchResult, function (item) {
                item.selected = true;
                changeRecordSelection(item);
            });
        }

        /**
         * change page of pagination
         */
        function pageChanged() {
            $scope.selectAll = false;
            $("result-grid tbody").scrollTop(0);
        }

        /**
         * convert record into short list item
         * @param record searchResult object
         * @constructor
         */
        function ShortListItem(record) {
            this.Name = record.Contact.Name;
            this.Id = record.Contact.Id;
        }

        /**
         * add / remove record from short list
         * @param record SearchResultObject
         */
        function changeRecordSelection(record) {
            var ContactId = record.Contact.Id;
            if (record.selected) {
                // add to short list
                var item = new ShortListItem(record);
                // check if item in list
                var foundItem = $scope.shortList.some(function (item) {
                    return item.Id === ContactId;
                });
                // only add if item does not exist
                if (!foundItem) {
                    $scope.shortList.push(item);

                    if ($scope.shortList.length > 100) {

                        $scope.exceedCandidateLimit = true;
                    }
                }
            }

            if (!record.selected) {

                // remove the item from short list
                var itemIndex = -1;
                var foundIndex = $scope.shortList.some(function (item, index) {
                    if (item.Id === ContactId) {
                        itemIndex = index;
                    }
                    return item.Id === ContactId;
                });
                if (foundIndex) {
                    $scope.shortList.splice(itemIndex, 1);

                    if ($scope.shortList.length <= 100) {

                        $scope.exceedCandidateLimit = false;
                    }
                }

                $scope.selectAll = false;

            }
        }

        /**
         * remove item from shortlist by search and deselect the item from the search result
         * @param item shortList item
         */
        function removeFromShortList(item) {
            // search item on searchResult
            var targetRecord = null;
            var foundIndex = $scope.searchResult.some(function (record, index) {
                if (record.Contact.Id === item.Id) {
                    targetRecord = record;
                }
                return record.Contact.Id === item.Id;
            });
            if (foundIndex) {
                targetRecord.selected = false;
                changeRecordSelection(targetRecord);
            }
        }

        /**
         * reselect records in search result based on selected shortlist
         */
        function reSelectRecordsInResult() {
            if ($scope.shortList.length > 0) {
                var recordToSelect = $scope.searchResult.filter(function (record) {
                    var found = $scope.shortList.some(function (selectedRecord) {
                        return record.Contact.Id === selectedRecord.Id;
                    });
                    if (found) {
                        return record;
                    }
                });
                if (recordToSelect.length > 0) {
                    // set item to be selected
                    recordToSelect.forEach(function (targetRecord) {
                        targetRecord.selected = true;
                        // changeRecordSelection(targetRecord);
                    });
                }

            }
        }

        function naveToVacancyPage(targetId) {
            console.log("nave to vacancy");
            //$window.location.href = "/" + targetId;
            helperService.locateToSObject($window, targetId);

        }


        function confirmNavigation(targetId) {

            var confirmationDialog = $uibModal.open({
                animation: true,
                templateUrl: 'searchResult/confirmAddToVacancyDialog.html',
                controller: 'SearchResultController',
                size: 'md'
            });
            confirmationDialog.result.then(function () {
                naveToVacancyPage(targetId);
            }, function () {
                console.log('stay here');
                if ($scope.vacancyId) {
                    busyLoader.start();
                    getVacancyAndRelatedCandidates($scope.vacancyId)
                        .then(function (list) {
                            $scope.vacancyAndRelatedCandidates = list;
                            busyLoader.end();
                        });
                }
            });

        }


        function addShortListToVacancy(vacancyId) {
            console.log("addShortListToVacancy");
            busyLoader.start();
            var shortList = angular.copy($scope.shortList);
            vacancyService.addContactToVacancy(vacancyId, shortList)
                .then(function (data) {
                    busyLoader.end();
                    confirmNavigation(vacancyId);

                });
        }

        function loadContactDetails(record) {

        }

        /**** manage other vacancy search ****/

        function findOtherVacancy() {
            console.log("addShortListToOtherVacancy");
            // nave to other vacancy tap
            var addToOtherVacancyDialog = $uibModal.open({
                animation: true,
                templateUrl: 'addToOtherVacancy/addToOtherVacancyView.html',
                controller: 'AddToOtherVacancyController',
                size: 'lg',
                backdrop: false,
                resolve: {
                    shortList: function () {
                        return $scope.shortList;
                    }
                }
            });
            addToOtherVacancyDialog.result.then(function (data) {
                console.log("other vacancy is closed ");
            }, function () {
                console.log('other vacancy is dismissed');
            });

        }

        /******** end of other vacancy functions *************/

        /******* communication functions ***********/
        function logContactCall(contact) {

            var url = "00T/e?title=Call&who_id=" + contact.Contact.Id + "&followup=1&tsk5=Call&retURL=%2F" + contact.Contact.Id;

            //TODO  Url provided without org prefix. Add org prefix.
            var targetUrl = $window.location.origin;

            targetUrl += "/" + url;
            //$window.open(targetUrl);
            helperService.openWindow($window, targetUrl);
        }

        function sendContactEmail(contact) {

            sendEmailToContactList([contact.Contact.Id]);
        }

        function sendShortListEmail(contacts) {

            var contactsList = contacts.map(function (contact) {
                return contact.Id;
            });

            sendEmailToContactList(contactsList);

        }

        function sendAllEmail(contacts) {

            var contactsList = contacts.map(function (contact) {
                return contact.Contact.Id;
            });
            sendEmailToContactList(contactsList);

        }

        /**
         * open new tab for email service. take a list of contact
         * @param contactsList array of contact id
         */
        function sendEmailToContactList(contactsList) {
            var rootURL = $window.location.origin;
            searchService.generateBulkEmailURIWithTargetObjectsMap(contactsList, {
                "Placement__c": $scope.vacancyId,
            })
                .then(function (data) {
                    var target = rootURL + data;
                    
                    var newTab = helperService.openWindow($window,data, "_blank");
                    $timeout(function () {
                      // Check if popup blocker is enabled by verifying the height of the new poup
                      if (!newTab || newTab.outerHeight === 0) {
                          alert("Please disable the popup blocker");
                      }
                    }, 1000);
                    
//                    if(helperService.hasForceOne()){
//                		sforce.one.navigateToURL(target);
//                	}else{
//                		 var newTab = $window.open(target);
//                         // check if the window open then alert user if popup is blocked
//                         $timeout(function () {
//                             // Check if popup blocker is enabled by verifying the height of the new poup
//                             if (!newTab || newTab.outerHeight === 0) {
//                                 alert("Please disable the popup blocker");
//                             }
//                         }, 1000);
//                	}
//                    
                   
                });
        }

        function sendAllSms(contacts) {
            var contactsList = contacts.map(function (contact) {
                return contact.Contact.Id;
            });

            sendSMS(contactsList);

        }

        function sendShortListSms(contacts) {
            var contactsList = contacts.map(function (contact) {
                return contact.Id;
            });
            sendSMS(contactsList);

        }

        function sendSMS(contactList) {

            var targetUrl = $window.location.origin;

            var url = "/apex/" + urlPrefixService.getPrefix() + "extract4sms?sourcepage=rostering&cjid=" + JSON.stringify(contactList);
            targetUrl += url;
            //$window.open(targetUrl);
            helperService.openWindow($window,url,"_blank");
        }

        function getVacancyAndRelatedCandidates(vacancyId) {
            return vacancyService.getVacancyAndRelatedCandidates(vacancyId)
                .then(function (data) {
                    var vacancyCandidate = [];
                    if (Array.isArray(data.Candidates__r) && data.Candidates__r.length > 0) {
                        vacancyCandidate = data.Candidates__r.map(function (item) {
                            return item.Candidate__c;
                        });
                    }
                    return vacancyCandidate;
                });
        }

        // return a href url to nave user page
        function naveToPage(targetId) {
        	return helperService.naveToPage($window, targetId);
//            var targetUrl = $window.location.origin;
//            targetUrl += "/" + targetId;
//            //  $window.open(targetUrl);
//
//            return targetUrl

        }

        /******************/

        /******* map scope data **********/
        $scope.displayColumn = [];
        $scope.configColumn = [];
        $scope.searchResult = [];
        $scope.searchQuery = {};
        $scope.shortList = [];
        $scope.showConfig = false;
        $scope.exceedCandidateLimit = false;
        $scope.keywordSearch = "";
        $scope.updateDisplayColumn = updateDisplayColumn;
        $scope.checkAll = checkAll;
        $scope.deselectAllResult = deselectAllResult;
        $scope.selectAllResult = selectAllResult;
        $scope.pageChanged = pageChanged;
        $scope.changeRecordSelection = changeRecordSelection;
        $scope.addShortListToVacancy = addShortListToVacancy;
        $scope.findOtherVacancy = findOtherVacancy;
        $scope.removeFromShortList = removeFromShortList;
        $scope.sendContactEmail = sendContactEmail;
        $scope.logContactCall = logContactCall;
        $scope.sendShortListEmail = sendShortListEmail;
        $scope.sendAllEmail = sendAllEmail;
        $scope.sendAllSms = sendAllSms;
        $scope.sendShortListSms = sendShortListSms;
        $scope.loadContactDetails = loadContactDetails;
        $scope.displayRecordValue = displayRecordValue;
        $scope.naveToPage = naveToPage;
        init();

    }

    angular.module("CTC.People.PeopleSearch").controller("SearchResultController", ["$scope", "$filter", "$location", "$window", "$timeout", "$state", "$uibModal", "searchService", "searchObjectsFactory", "vacancyService", "busyLoader", "urlPrefixService", "helperService", searchResultController]);
}());
