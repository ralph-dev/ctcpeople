/**
 * display a formatted view for the search query summary
 * @name searchSummary
 */
(function () {
    function searchSummary(searchService, searchObjectsFactory, skillSearchService, $state, $filter) {
        return {
            restrict: "EA",
            templateUrl: "searchSummary/searchSummaryView.html",
            link: function (scope, element, attr) {
                var fieldsList = [];
                var fieldTypeList = [];
                var fieldTypeEnum = searchObjectsFactory.FieldTypeEnum();
                var skillsList = [];

                function init() {
                    searchService.getContactCriteriaList()
                        .then(function (data) {
                            fieldsList = data;
                            return searchService.getFieldTypeList();
                        })
                        .then(function (typeList) {
                            fieldTypeList = typeList;
                            return searchService.getSearchQuery();
                        })
                        .then(function (query) {
                            scope.searchQuery = query;
                            return skillSearchService.getSkillsList();
                        })
                        .then(function (list) {
                            skillsList = list;
                        });
                }

                function displayFieldName(fieldName) {
                    var displayValue = "";
                    for (var i = 0; i < fieldsList.length; i++) {
                        if (fieldsList[i].apiName === fieldName) {
                            displayValue = fieldsList[i].label;
                            break;
                        }
                    }
                    return displayValue;
                }

                function displayOperator(field) {
                    var operatorLabel = "";
                    for (var n = 0; n < fieldTypeList.length; n++) {
                        if (fieldTypeList[n].type === field.type) {
                            var opList = fieldTypeList[n].operators;
                            for (var i = 0; i < opList.length; i++) {
                                if (opList[i].value === field.operator) {
                                    operatorLabel = opList[i].label;
                                }
                            }
                            break;
                        }
                    }
                    return operatorLabel;
                }

                function displayValue(field) {
                    var stringValue = "";
                    switch(field.type){
                        case fieldTypeEnum.DATE:
                        case fieldTypeEnum.DATETIME:
                            if(field.values){
                                var fromDate = $filter('date')(field.values.fromDate, "dd/MM/yyyy");
                                var toDate = $filter('date')(field.values.toDate, "dd/MM/yyyy");
                                stringValue = fromDate + " - " + toDate;
                            }else{
                                stringValue = "";
                            }

                            break;
                        case fieldTypeEnum.MULTIPICKLIST:
                            if(field.values && Array.isArray(field.values)){
                                stringValue = field.values.join(", ");
                            }else{
                                stringValue = "";
                            }

                            break;
                        case fieldTypeEnum.PICKLIST:
                            // get the picklist label based on value
                            if(field.values && Array.isArray(field.values)){
                                stringValue = getPicklistLabelValue(field).join(", ");
                            }else{
                                stringValue = "";
                            }


                            break;
                        default:
                            stringValue = field.values;
                            break;
                    }

                    return stringValue;
                }

                /**
                 * get labels associate with picklist values
                 * require fieldsList to be defined
                 * @param field
                 * @returns {Array}
                 */
                function getPicklistLabelValue(field) {
                    var fieldObj = null;
                    var result = [];
                    var foundValue = [];
                    for (var i = 0; i < fieldsList.length; i++) {
                        if (fieldsList[i].apiName === field.field) {
                            fieldObj = fieldsList[i];
                            foundValue = fieldObj.picklistValues.filter(function (option) {
                                return field.values.some(function (val) {
                                    return option.value === val;
                                });
                            });
                            break;
                        }
                    }
                    if(foundValue.length > 0){
                        result = foundValue.map(function(item){ return item.label});
                    }
                    return result;
                }

                function editSearchQuery() {
                    searchService.setEditSearchFlag();
                    $state.go("search");
                }

                function newSearchQuery(){
                    searchService.clearCurrentTemplate();
                    $state.go("search");
                }

                function getSkillNameById(id){
                    var skillName =" ";
                    var found = skillsList.some(function(item){
                        if(item.skillId === id){
                            skillName = item.skillName;
                            return true;
                        }
                        return false;
                    });

                    return skillName;
                }

                function displaySkillsSearch(skillsSearch){
                    // todo filter skillsList and return labels
                    var formattedQuery = "";
                    var tempQuery = skillsSearch.query.split(" ");
                    for(var i = 0; i < tempQuery.length; i+=2){
                        var skillName = getSkillNameById(tempQuery[i]);
                        if(i === 0){
                            formattedQuery += skillName;
                        }else
                        formattedQuery += " "+tempQuery[i-1] + " " + skillName;
                    }
                    return formattedQuery;
                }

                /** scope mapping ***/
                scope.displayFieldName = displayFieldName;
                scope.displayOperator = displayOperator;
                scope.displayValue = displayValue;
                scope.editSearchQuery = editSearchQuery;
                scope.newSearchQuery = newSearchQuery;
                scope.displaySkillsSearch = displaySkillsSearch;
                init();
            }
        };
    }

    angular.module("CTC.People.PeopleSearch").directive("searchSummary", ["searchService", "searchObjectsFactory", "skillSearchService", "$state", "$filter", searchSummary]);
}());
