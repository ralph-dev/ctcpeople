(function () {
    "use strict";
    function searchInputTemplate($compile, $templateRequest, searchObjectsFactory) {
        return {
            restrict: "EA",
            link: function (scope, element, attr) {
                // Load the html through $templateRequest
                // a list of html template based on field type object
                var fieldTypeEnum = searchObjectsFactory.FieldTypeEnum();
                var templateList = {
                   PICKLIST: "inputTemplate/inputTemplatePicklist.html",
                    MULTIPICKLIST: "inputTemplate/inputTemplateMultiPicklist.html",
                    STRING: "inputTemplate/inputTemplateString.html",
                    DATE: "inputTemplate/inputTemplateDateRange.html",
                    NUMBER: "inputTemplate/inputTemplateNumber.html",
                    BOOLEAN: "inputTemplate/inputTemplateCheckbox.html"
                };

                function setFieldTemplate(fieldType){
                    var template = templateList.STRING;
                    switch(fieldType){
                        case  fieldTypeEnum.MULTIPICKLIST:
                        case fieldTypeEnum.PICKLIST:
                          template = templateList.MULTIPICKLIST;
                            break;
                        case fieldTypeEnum.NUMBER:
                        case fieldTypeEnum.PERCENT:
                        case fieldTypeEnum.CURRENCY:
                        case fieldTypeEnum.DOUBLE:
                            template = templateList.NUMBER;
                            break;
                        case fieldTypeEnum.BOOLEAN:
                            template = templateList.BOOLEAN;
                            break;
                        case fieldTypeEnum.DATE:
                        case fieldTypeEnum.DATETIME:
                            template = templateList.DATE;
                            break;
                        case fieldTypeEnum.STRING:
                        default:
                            template = templateList.STRING;
                            break;
                    }
                    return template;
                }
                var templateUrl =  setFieldTemplate(attr.fieldType);

                $templateRequest(templateUrl).then(function (html) {
                    // Convert the html to an actual DOM node
                    var template = angular.element(html);
                    // Append it to the directive element
                    element.append(template);
                    // And let Angular $compile it
                    $compile(template)(scope);
                });

                // relaod template if type change
                //attr.$observe('fieldType', function(value){
                //    console.log(value);
                //    var templateUrl = templateList.hasOwnProperty(attr.fieldType) ? templateList[attr.fieldType] : templateList.STRING;
                //
                //    $templateRequest(templateUrl).then(function (html) {
                //        // Convert the html to an actual DOM node
                //        var template = angular.element(html);
                //        // compile html to current scope
                //        var scopeTemplate = $compile(template)(scope);
                //        // replace current directive content with new one
                //        element.replaceWith(scopeTemplate);
                //    });
                    /* TODO: combine loading template on attr change and on load function */
             //   });

                /******* manage date input ********/
                scope.dateOptions = {
                    formatYear: 'yy',
                    //maxDate: new Date(2020, 5, 22),
                    //minDate: new Date(),
                    startingDay: 1
                };
                scope.altInputFormats = ['d!-MM!-yy!'];
                scope.datePickerFlag = false;
                scope.dateFormat = "dd/MM/yyyy";


            }
        };
    }

    angular.module("CTC.People.PeopleSearch").directive("inputTemplate", searchInputTemplate);
}());
