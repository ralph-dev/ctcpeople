/**** manage other vacancy search ****/
(function () {
    "use strict";
    function addToOtherVacancyController($scope, $uibModalInstance, searchService, busyLoader, vacancyService, shortList) {
        function init() {
            $scope.shortList = shortList;
            checkAll($scope.selectAllCandidate, $scope.shortList);
        }
        /**
         * search for vacancy by name
         * @param searchText
         */
        function searchForVacancy(searchText){
            busyLoader.start();
            vacancyService.searchVacancyByName(searchText)
                .then(function(data){
                    $scope.vacancySearchResult = data;
                    busyLoader.end();
                });
        }

        function addToOtherVacancy(vacancyList){
            busyLoader.start();
            var selectedList = vacancyList.filter(function(item){
                return item.selected === true ;
            });
            var targetVacancyList = selectedList.map(function(item){
                return item.Id;
            });
            var shortList = $scope.shortList.filter(function(item){
                return item.selected === true;
            });
            vacancyService.addContactToVacancy(targetVacancyList, shortList)
                .then(function(data){
                    console.log("add to other vacancy success");
                    $scope.addSuccessFlag = true;
                    busyLoader.end();
                });
        }

        function checkAll(selectAll, targetList){
            if(selectAll){
                selectAll = true;
            }else{
                selectAll = false;
            }
            targetList.forEach(function(item){
               item.selected = selectAll;
            });

        }

        function changeShortlistItemSelection(item){
            if(!item.selected){
                $scope.selectAllCandidate = false;
            }
        }

        function checkIfContainSelected(list) {
            if(!Array.isArray(list)){
                return false;
            }else {
                return list.some(function (item) {
                    return item.selected === true;
                });
            }

        }



        /******* map scope data **********/
        $scope.shortList = [];
        $scope.addSuccessFlag = false;
        $scope.selectAllCandidate = true;
        $scope.addToOtherVacancy = addToOtherVacancy;
        $scope.searchForVacancy = searchForVacancy;
        $scope.checkAll = checkAll;
        $scope.changeShortlistItemSelection = changeShortlistItemSelection;
        $scope.checkIfContainSelected = checkIfContainSelected;
        init();

    }

    angular.module("CTC.People.PeopleSearch").controller("AddToOtherVacancyController", ["$scope", "$uibModalInstance", "searchService", "busyLoader", "vacancyService", "shortList", addToOtherVacancyController]);
}());
