(function(){

    function highlightRecordDirective(){
        return{
            strict: "A",
            scope:{
                refValue: "=",
                refLists: "="
            },
            link: function(scope, element, attr){

                scope.$watch("refLists", function(newValue, oldValue) {

                    if(newValue !== undefined ) {
                        if(newValue.some(function(item) {return item === scope.refValue;})){
                            angular.element(element).addClass("highlightColor");
                        }

                    }
                });
            }
        };
    }
    angular.module("CTC.People.PeopleSearch").directive("highlightRecord",[highlightRecordDirective]);
}());
