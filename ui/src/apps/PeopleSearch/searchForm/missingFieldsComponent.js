(function () {
    "use strict";
    function missingFieldsController() {
        var ctrl = this;

        function onChange(change) {
            console.log("change missing field list", change);
            if (change.templateFields) {
                ctrl.templateFields = angular.copy(ctrl.templateFields);
            }
            if (change.criteriaFields) {
                ctrl.criteriaFields = angular.copy(ctrl.criteriaFields);
            }

            if( ctrl.templateFields && ctrl.criteriaFields){
                checkMissingFields();
            }
        }

        /***
         * template field = {\"values\":\"rr\",\"operator\":\"ne\",\"type\":\"ID\",\"field\":\"Id\"} ;
         * criteria field = {apiName:"Id", label:"ID"}
         *
         */

        function checkMissingFields() {
            var missing = [];
            for (var i=0; i < ctrl.templateFields.length; i++){
                var field = ctrl.templateFields[i];
                var found = ctrl.criteriaFields.some(function(item){
                    return item.apiName === field.field;
                });
                if(!found){
                    missing.push(field);
                }
            }
            ctrl.missingFields = missing;
            if(missing.length > 0){
                ctrl.showMissingFieldsAlert = true;
            }else {
                ctrl.showMissingFieldsAlert = false;
            }
            console.log("check missing", missing);
        }

        ctrl.$onChanges = onChange;
        ctrl.showMissingFieldsAlert = false;

    }

    var missingFieldsComponent = {
        templateUrl: "searchForm/missingFieldsComponent.html",
        controller: missingFieldsController,
        bindings: {
            templateFields: "<",
            criteriaFields: "<"
        }
    };
    angular.module("CTC.People.PeopleSearch").component("missingFields", missingFieldsComponent);
}());
