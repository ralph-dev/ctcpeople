(function () {
    "use strict";
    function searchFormController($scope, $state, $location, $uibModal, searchService, skillSearchService, busyLoader) {

        function init() {
            // check if search come from vacancy
            busyLoader.start();
            var vacancyId = $location.query("id");
            $scope.vacancyId = (angular.isDefined(vacancyId) ? vacancyId : null);
            skillSearchService.isSkillSearchEnabled()
                .then(function(isEnableFlag){
                    $scope.isSkillsSearchEnableFlag = isEnableFlag;
                    return skillSearchService.getSkillsList();
                })
                .then(function(skillsList){
                    $scope.skillsList = skillsList;
                    return searchService.getContactCriteriaList();
                })
                .then(function (data) {
                    $scope.fieldsList = data;
                    return searchService.getSearchTemplateList();
                })
                .then(function (searchTemplates) {
                    $scope.savedSearchList = searchTemplates;
                    $scope.selectedSearchTemplate = searchService.getCurrentTemplate();
                    return loadSearchFrom();
                })
                .then(function ( data){
                    // stop busyLoader
                    busyLoader.end();
                });
        }

        /**
         * process current template data to get list of criteria field for field warning
         */
        function setTemplateCriteriaList(){
            var templateData = $scope.selectedSearchTemplate.ascomponents__c;
            var list = [];
            if(templateData){
                if(typeof templateData === "string"){
                    var parsedData = JSON.parse(templateData);
                   list = parsedData.criteria;
                }
                else if(templateData.criteria){
                        list = templateData.criteria;
                }
            }
            $scope.templateFields = list;

        }

        /**
         * get and load current search form
         * @returns {*}
         */
        function loadSearchFrom(){
            return searchService.loadSearchTemplate()
                .then(function (searchForm) {
                    $scope.searchKeywordString = searchForm.resumeKeywords;
                    $scope.searchCriteriaList = searchForm.criteria;
                    $scope.searchSkillsCriteria = searchForm.skillsSearch;
                    if($scope.searchSkillsCriteria.query.length > 1){
                        $scope.isCollapsedSkillSection = false;
                    }else {
                        $scope.isCollapsedSkillSection = true;
                    }

                    setTemplateCriteriaList();
                    return true;
                });
        }

        function changeFieldType(criteria) {
            // update object data
            criteria.type = criteria.fieldObject.type;
            criteria.values = ""; // set to empty value if field type change
            criteria.operator = null; // clear selected operator
        }

        /**
         * show modal dialog on screen if no data is found
         *
         */
        function noResultDialog() {

            var noResultModal = $uibModal.open({
                animation: true,
                templateUrl: 'searchForm/noResultWarning.html',
                size: 'md'
            });
            noResultModal.result.then(function () {
                console.log('alert acknowledge');
            }, function () {
                console.log('alert acknowledge');
            });

        }

        /**
         * validate search from and submit query on success re-direct to search result view
         * @param searchForm
         */
        function submitSearchQuery(searchForm, event) {
            if (event.type === "enter") {
                event.preventDefault();
            }
            busyLoader.start();
            console.log($scope.searchSkillsCriteria);
            var skills = null;
            if($scope.isSkillsSearchEnableFlag){
                skills = $scope.searchSkillsCriteria;
            }
            searchService.submitSearchQuery($scope.searchKeywordString, $scope.searchCriteriaList, skills)
                .then(function (data) {
                    busyLoader.end();
                    /* on success if search result is positive  re-direct to search-result view */
                    if (data && data.length > 0) {
                        if ($scope.draftSearchTemplateFalg) {
                            searchService.clearCurrentTemplate();
                        }
                        $state.go("search-result");
                    } else {
                        noResultDialog();
                    }
                }, function (error) {
                    console.log("error on search result", error);
                    busyLoader.end();
                    noResultDialog();
                });
        }

        function clearKeywordString() {
            $scope.searchKeywordString = "";
        }

        function clearCriteriaString() {
            $scope.searchCriteriaList.forEach(function (criteria) {
                criteria.values = null;
                if (criteria.type === "BOOLEAN") {
                    criteria.operator = criteria.operator;
                } else {
                    criteria.operator = null;
                }

            });
        }

        /**
         * clear form errors and reset form status.
         * @param searchForm
         */
        function clearSearchFormErrors(searchForm) {
            for (var property in searchForm.$error) {
                if (searchForm.$error.hasOwnProperty(property)) {

                    // get the number of errors for each error type
                    var errorLength = searchForm.$error[property].length;

                    for (var i = 0; i < errorLength; i++) {
                        //clear the contents of the filed has error for angularjs not clearing it
                        var fieldName = searchForm.$error[property][0].$name;

                        if (property === "number") {
                            searchForm[fieldName].$viewValue = "ValueToBeEmpty";
                            searchForm[fieldName].$$lastCommittedViewValue = "ValueToBeEmpty";
                        } else {
                            searchForm[fieldName].$viewValue = undefined;
                            searchForm[fieldName].$$lastCommittedViewValue = undefined;
                        }

                        searchForm[fieldName].$setValidity(property, true);
                    }
                }
            }

            searchForm.$setPristine(true);
            searchForm.$setUntouched(true);
            searchForm.$rollbackViewValue();
        }

        /**
         * clear all section of the search from
         * @param searchForm
         */
        function clearSearchForm(searchForm) {
            clearKeywordString();
            clearCriteriaString();
            //TODO add handler to clear availability and skill search
            // searchForm.$setPristine();
            clearSearchFormErrors(searchForm);

        }

        /******** manage saved search template *********/

        function checkForMissingFields() {
        }

        function selectDefaultSearchTemplate() {
            // reset the template to default
            searchService.clearCurrentTemplate();
            $scope.selectedSearchTemplate = searchService.getCurrentTemplate();
            selectSavedSearch($scope.selectedSearchTemplate);
        }

        function selectSavedSearch(searchTemplate) {
            // remove any draft template
            busyLoader.start();
            clearSearchForm($scope.searchForm);
            $scope.savedSearchList.forEach(function (item) {
                item.draft = false;
            });
            $scope.draftSearchTemplateFalg = false;
            searchService.setSearchTemplate(searchTemplate)
                .then(function (data) {
                    $scope.selectedSearchTemplate = data;
                    return loadSearchFrom(); // reload the search from

                })
                .then(function (data) {
                    // set the from to be untouched
                    $scope.searchForm.$setPristine();
                    busyLoader.end();
                });
        }

        function deleteSavedSearch(searchTemplate) {
            // start busyLoader
            /**check if deleted item == current template  clear current template **/
            busyLoader.start();
            searchService.deleteSearchTemplate(searchTemplate)
                .then(function (data) {
                    return searchService.getSearchTemplateList();
                })
                .then(function (templateList) {
                    $scope.savedSearchList = templateList;
                    // remove template data if deleted temp is selected temp
                    if (searchTemplate.Id === $scope.selectedSearchTemplate.Id) {
                        selectDefaultSearchTemplate();
                    } else {
                        $scope.selectedSearchTemplate = searchService.getCurrentTemplate();
                    }
                    // stop busyLoader
                    busyLoader.end();
                });
        }

        function showSaveSearchDialog() {
            var saveSearchModal = $uibModal.open({
                animation: true,
                size: 'md',
                backdrop: false,
                templateUrl: 'searchForm/saveSearchTemplate.html',
                controller: function ($scope, templateOb, templateList) {
                    $scope.searchTemplate = templateOb;
                    $scope.templateList = templateList;
                    $scope.saveSearchTemplateForm = {templateName: {}};
                    // set validation using ngChange
                    // TODO: add check for Default template and check on load
                    $scope.changeName = function (modelValue, template, templateList) {
                        var duplicate = templateList.some(function (item) {
                            if (item.Name === modelValue.$modelValue && item.Id !== template.Id) {
                                return true;
                            } else {
                                return false;
                            }
                        });
                        if (duplicate) {
                            modelValue.$setValidity("duplicateError", false);
                        } else {
                            modelValue.$setValidity("duplicateError", true);
                        }
                    };

                },
                resolve: {
                    templateOb: function () {
                        return $scope.searchTemplate;
                    },
                    templateList: function () {
                        return $scope.savedSearchList;
                    }
                }
            });
            saveSearchModal.result
                .then(function (data) {
                    $scope.saveSearchTemlate(data)
                }, function () {
                    console.log('alert acknowledge');
                });
        }

        function newSearchTemplate() {
            $scope.searchTemplate = {Id: -1};
            $scope.searchTemplate.ascomponents__c = searchService.captureSearchQuery($scope.searchKeywordString, $scope.searchCriteriaList, $scope.searchSkillsCriteria);
            console.log($scope.searchTemplate);
            showSaveSearchDialog();
        }

        function updateSearchTemplate() {
            $scope.searchTemplate = searchService.getCurrentTemplate();

            $scope.searchTemplate.ascomponents__c = searchService.captureSearchQuery($scope.searchKeywordString, $scope.searchCriteriaList, $scope.searchSkillsCriteria, $scope.searchTemplate.ascomponents__c.availability);
            // set edit flag to false
            console.log($scope.searchTemplate);
            showSaveSearchDialog();
        }


        function saveSearchTemplate(searchTemplate) {
            // start busyLoader
            busyLoader.start();
            if (searchTemplate.Id === -1) {
                delete searchTemplate.Id;
                searchService.createSearchTemplate(searchTemplate)
                    .then(function (data) {
                        searchService.setSearchTemplate(data);
                        return searchService.getSearchTemplateList();
                    })
                    .then(function (templateList) {
                        $scope.savedSearchList = templateList;
                        $scope.selectedSearchTemplate = searchService.getCurrentTemplate();
                        $scope.searchForm.$setPristine();
                        // stop busyLoader
                        busyLoader.end();
                    });
            } else {
                searchService.updateSearchTemplate(searchTemplate)
                    .then(function (data) {
                        return searchService.getSearchTemplateList();
                    })
                    .then(function (templateList) {
                        $scope.savedSearchList = templateList;
                        $scope.selectedSearchTemplate = searchService.getCurrentTemplate();
                        $scope.draftSearchTemplateFalg = false;
                        // stop busyLoader
                        $scope.searchForm.$setPristine();
                        busyLoader.end();
                    });
            }
        }


        $scope.$watch('searchForm.$dirty', function (newVal, oldVal) {
            if (newVal) {
                if ($scope.selectedSearchTemplate.Id !== -1) {
                    for (var i = 0; i < $scope.savedSearchList.length; i++) {
                        if ($scope.savedSearchList[i].Id === $scope.selectedSearchTemplate.Id) {
                            $scope.savedSearchList[i].draft = true;
                            $scope.draftSearchTemplateFalg = true;
                        }
                    }
                }
            }
        });

        function updateSkillsCriteria(event){
            console.log(event.skillsCriteria);
         //   $scope.searchSkillsCriteria = event.skillsCriteria;
            /* update internal property to prevent data reload in the component*/
            $scope.searchSkillsCriteria.query = event.skillsCriteria.query;
            $scope.searchSkillsCriteria.verifiedSkills = event.skillsCriteria.verifiedSkills;
            $scope.searchForm.skillQuery.$setDirty();
        }

        function stopSubmit($event){
            console.log("try to stop submit");
            $event.preventDefault();
            window.stop(); // Works in all browsers but IE...
            document.execCommand('Stop'); // Works in IE
            return false; // Don't even know why it's here. Does nothing.
        }

        function toggleCollapsedSkillSection(){
            $scope.isCollapsedSkillSection = ! $scope.isCollapsedSkillSection;
        }

        /****** $scope mapping ***********/
        $scope.selectedSearchTemplate = {};
        $scope.draftSearchTemplateFalg = false;
        $scope.searchCriteriaList = [];
        $scope.searchForm = {};
        $scope.searchKeywordString = "";
        $scope.searchSkillsCriteria = {};
        $scope.vacancyId = null;
        $scope.savedSearchList = [];
        $scope.isCollapsedCriteriaSection = false;
        $scope.isCollapsedSkillSection = true;
        $scope.skillsList =[];
        $scope.isSkillsSearchEnableFlag = false;
        $scope.changeFieldType = changeFieldType;
        $scope.submitSearchQuery = submitSearchQuery;
        $scope.clearSearchForm = clearSearchForm;
        $scope.selectDefaultSearchTemplate = selectDefaultSearchTemplate;
        $scope.selectSavedSearch = selectSavedSearch;
        $scope.deleteSavedSearch = deleteSavedSearch;
        $scope.saveSearchTemlate = saveSearchTemplate;
        $scope.newSearchTemplate = newSearchTemplate;
        $scope.updateSearchTemplate = updateSearchTemplate;
        $scope.updateSkillsCriteria = updateSkillsCriteria;
        $scope.stopSubmit = stopSubmit;
        $scope.toggleCollapsedSkillSection = toggleCollapsedSkillSection;
        init();
    }

    angular.module("CTC.People.PeopleSearch").controller("SearchFormController", ["$scope", "$state", "$location", "$uibModal", "searchService", "skillSearchService", "busyLoader", searchFormController]);
}());
