Visualforce.remoting.Manager.add(new $VFRM.RemotingProviderImpl({
    "service": "data",
    "vf": {
        "xhr": false,
        "tm": 1426639304140,
        "ovrprm": false,
        "dev": false,
        "dbg": false,
        "vid": "0",
        "tst": false
    },
    "actions": {
        "AuthExtension": {
            "prm": 0,
            "ms": [
                {
                    "name": "getRPSession",
                    "len": 0,
                    "csrf": "src/static/mockData/authExtension.getRPSession.json",
                    "ver": 31.0,
                    "ns": ""
                }
            ]
        },
        "HelperExtension": {
            "prm": 0,
            "ms": [
                {
                    "name": "getNamespace",
                    "len": 1,
                    "csrf": " '' ",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getUserData",
                    "len": 0,
                    "csrf": "src/static/mockData/helperExtension.getUserData.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getUserPermission",
                    "len": 0,
                    "csrf": "src/static/mockData/helperExtension.getUserPermission.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getPicklistMapBySObjectAndFieldName",
                    "len": 2,
                    "csrf": "src/static/mockData/helperExtension.getPicklistMapBySObjectAndFieldName.json",
                    "ver": 31.0,
                    "ns": ""
                }
            ]
        },
        "PeopleSearchExtension":{
            "prm":0,
            "ms":[
                {
                    "name": "getContactCriteriaList",
                    "len": 0,
                    "csrf": "src/static/mockData/peopleSearchExtension.getContactCriteriaList.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getFieldTypeList",
                    "len": 0,
                    "csrf": "src/static/mockData/peopleSearchExtension.getFieldTypeList.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "searchCandidate",
                    "len": 1,
                    "csrf": "src/static/mockData/peopleSearchExtension.searchCandidate.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getContactDisplayColumns",
                    "len": 0,
                    "csrf": "src/static/mockData/peopleSearchExtension.getContactDisplayColumns.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getContactDefaultDisplayColumn",
                    "len": 0,
                    "csrf": "src/static/mockData/peopleSearchExtension.getContactDefaultDisplayColumn.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getContactFieldsList",
                    "len": 0,
                    "csrf": "src/static/mockData/peopleSearchExtension.getContactFieldsList.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "saveContactDisplayColumn",
                    "len": 1,
                    "csrf": "src/static/mockData/peopleSearchExtension.saveContactDisplayColumn.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "generateBulkEmailURI",
                    "len": 1,
                    "csrf": "src/static/mockData/peopleSearchExtension.generateBulkEmailURI.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getContactLongTextAreaFields",
                    "len": 1,
                    "csrf": "src/static/mockData/peopleSearchExtension.getContactLongTextAreaFields.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getPeopleInfo",
                    "len": 1,
                    "csrf": "src/static/mockData/peopleSearchExtension.getPeopleInfo.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getAllSkillsList",
                    "len": 0,
                    "csrf": "src/static/mockData/peopleSearchExtension.getAllSkillsList.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "isSkillSearchEnabled",
                    "len": 0,
                    "csrf": "src/static/mockData/peopleSearchExtension.isSkillSearchEnabled.json",
                    "ver": 31.0,
                    "ns": ""
                }


            ]
        },
        "VacancyExtension":{
            "prm":0,
            "ms":[
                {
                    "name": "getVacancy",
                    "len": 1,
                    "csrf": "src/static/mockData/vacancyExtension.getVacancy.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "searchVacancyByName",
                    "len": 1,
                    "csrf": "src/static/mockData/vacancyExtension.searchVacancyByName.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getVacancyAndRelatedCandidates",
                    "len": 1,
                    "csrf": "src/static/mockData/vacancyExtension.getVacancyAndRelatedCandidates.json",
                    "ver": 31.0,
                    "ns": ""
                }
            ]
        },
        "PlacementCandidateExtension": {
            "prm": 0,
            "ms": [
                {
                    "name": "createRecords",
                    "len": 2,
                    "csrf": "src/static/mockData/placementCandidateExtension.createRecords.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getTop5Records",
                    "len": 1,
                    "csrf": "src/static/mockData/placementCandidateExtension.getTop5Records.json",
                    "ver": 31.0,
                    "ns": ""
                }
            ]
        },
        "StyleCategoryExtension":{
            "prm": 0,
            "ms": [
                {
                    "name": "getSaveSearchRecordsByUser",
                    "len": 0,
                    "csrf": "src/static/mockData/styleCategoryExtension.getSaveSearchRecordsByUser.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "createRecords",
                    "len": 0,
                    "csrf": "src/static/mockData/styleCategoryExtension.createRecords.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "updateRecords",
                    "len": 0,
                    "csrf": "src/static/mockData/styleCategoryExtension.createRecords.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "deleteRecords",
                    "len": 0,
                    "csrf": "src/static/mockData/styleCategoryExtension.createRecords.json",
                    "ver": 31.0,
                    "ns": ""
                }
            ]
        },
        "TaskExtension": {
            "prm": 0,
            "ms": [
                {
                    "name": "getTop5TasksByContactId",
                    "len": 1,
                    "csrf": "src/static/mockData/taskExtension.getTop5TasksByContactId.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "isValidCloudStorageAccount",
                    "len": 0,
                    "csrf": "src/static/mockData/taskExtension.isValidCloudStorageAccount.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getSkillGroups",
                    "len": 0,
                    "csrf": "src/static/mockData/taskExtension.getSkillGroups.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getDocTypePickList",
                    "len": 0,
                    "csrf": "src/static/mockData/taskExtension.getDocTypePickList.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getTaskInfo",
                    "len": 1,
                    "csrf": "src/static/mockData/taskExtension.getTaskInfo.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "getActivityDocs",
                    "len": 1,
                    "csrf": "src/static/mockData/taskExtension.getActivityDocs.json",
                    "ver": 31.0,
                    "ns": ""
                },
                {
                    "name": "uploadDocuments",
                    "len": 1,
                    "csrf": "src/static/mockData/taskExtension.uploadDocuments.json",
                    "ver": 31.0,
                    "ns": ""
                }
            ]
        },
        "ResumeAndFilesExtension":{
            "prm": 0,
            "ms": [
                {
                    "name": "getOptionList",
                    "len": 1,
                    "csrf": "src/static/mockData/resumeAndFilesExtension.getOptionList.json",
                    "ver": 31.0,
                    "ns": ""
                }
            ]}
    }
}));
