(function () {
    "use strict";
    function ctcFilesUploaderController() {
        var ctrl = this;
        ctrl.files = [];
        ctrl.fileSize = null;
        ctrl.multiple = false;
        ctrl.fileTypeRegex;
        ctrl.fileTypes;
        ctrl.invalidFileType = false;
        function registerFiles(files) {
            if(files.length < 1 ){
                return;
            }
            ctrl.invalidFileType = !isValidFileType(files);
            ctrl.invalidFileSize = !isValidFileSize(files);
            if (!ctrl.invalidFileType &&  !ctrl.invalidFileSize) {
                var targetFiles = [];
                ctrl.invalidFileType = false;
                // check if multiple
                if (ctrl.multiple) {
                    Array.prototype.push.apply(targetFiles, files);
                } else {
                    ctrl.files[0] = files[0];
                }
                if (ctrl.onUpload) {
                    ctrl.onUpload({files: targetFiles});
                }
            }
        }

        /**
         * loop over the list of files to check if the file size is smaller than max file size
         * @param files
         * @returns {boolean} true if no invalid size found
         */
        function isValidFileSize(files){
            var invalid = false;
            var fileList = [];
            Array.prototype.push.apply(fileList, files);
            if(ctrl.maxFileSize){
                 invalid = fileList.some(function(file){
                    return file.size > ctrl.maxFileSize ;
                });
            }
            return !invalid;
        }

        function uploadFiles() {
            // link to upload files form parent
            console.log("files list", ctrl.files, ctrl, $scope);
        }

        function setSupportedFileTypes(typesList) {
            // process attr string then create a sting with | separator then remove and dots at the start of the extension
            ctrl.fileTypes = typesList.split(',').join('|').replace(/\|\./g, '|');
            var regexSt = "(.*?)\.(" + ctrl.fileTypes + ")$";

            ctrl.fileTypeRegex = new RegExp(regexSt);
        }

        function setMaxFileSize(size){
            // convert the size to bytes from KB
            var fileSize = size * 1024;
            ctrl.maxFileSize = fileSize;
        }

        /**
         * check if files has valid type based on pre-Set extension. if not types is set return true
         * @param files array of file objects
         * @returns {boolean}
         */
        function isValidFileType(files) {
            var fileList = [];
            Array.prototype.push.apply(fileList, files);
            var invalidFormat = false; // default validation is true.
            if (ctrl.fileTypes) {
                // loop through files and test if for invalid file extensions
                invalidFormat = fileList.some(function (fileData) {
                    return !ctrl.fileTypeRegex.test(fileData.name);
                });
            }
            return !invalidFormat;
        }


        ctrl.uploadFiles = uploadFiles;
        ctrl.registerFiles = registerFiles;
        ctrl.setSupportedFileTypes = setSupportedFileTypes;
        ctrl.isValidFileType = isValidFileType;
        ctrl.setMaxFileSize = setMaxFileSize;

    }

    function ctcFilesUploaderDirective($window) {
        return {
            scope: {
                onUpload: "&",
                multiple: "=?",
                acceptedFileType:"@?",
                maxFileSize:"@?",
                inputName:"@name",
                warningMessage:"@?"
            },
            templateUrl: 'common/directives/filesUploader/filesUploader.html',
            controller: ctcFilesUploaderController,
            controllerAs: "uploader",
            link: function (scope, element, attrs, $ctrl) {
                var upload = element[0].querySelector('div.upload-zone');
                var uploadInput = element[0].querySelector('input.upload-input');
                var browseBtn = element[0].querySelector('.upload-btn');
                var preventClick = true;

                if (attrs.$attr.multiple) {
                    $ctrl.multiple = true;
                    $(uploadInput).attr('multiple', 'multiple');
                }


                if(attrs.$attr.acceptedFileType ){
                    if(scope.acceptedFileType.length > 0){
                        $ctrl.setSupportedFileTypes(scope.acceptedFileType);
                    }
                }

                attrs.$observe("acceptedFileType", function(value){
                    if(value.length > 0){
                        console.log(value,scope.acceptedFileType );
                        $ctrl.setSupportedFileTypes(scope.acceptedFileType);
                    }
                });

                if(attrs.$attr.maxFileSize){
                    $ctrl.setMaxFileSize(scope.maxFileSize);
                }

                attrs.$observe("maxFileSize", function(value){
                    if(value.length > 0){
                        $ctrl.setMaxFileSize(scope.maxFileSize);
                    }
                });

                // set on file Upload function
                if (attrs.$attr.onUpload) {
                    $ctrl.onUpload = scope.onUpload;
                }
                if(attrs.$attr.showList){
                    scope.showList = true;
                }


                function onUploadInput(event) {
                    $ctrl.registerFiles(event.target.files);
                    scope.$apply();
                }

                uploadInput.addEventListener('change', onUploadInput);

                function onDrop(e) {
                    if (e.dataTransfer && e.dataTransfer.files) {
                        $ctrl.registerFiles(e.dataTransfer.files);
                        scope.$apply();
                    }
                }

                //browseBtn.addEventListener('click', function(){
                //    uploadInput.click();
                //}, true);
                upload.addEventListener('click', function(){
                    uploadInput.click();
                }, false);

                upload.addEventListener('drop', onDrop, false);

                /***** prevent default behavior *******/
                function preventDefault(e){
                    e.preventDefault();
                }
                upload.addEventListener('dragenter', preventDefault, false);
                upload.addEventListener('dragleave', preventDefault, false);
                upload.addEventListener('dragover', preventDefault, false);
                upload.addEventListener('drop', preventDefault, false);
            }

        };
    }

    angular.module("CTC.Components").directive('ctcFilesUploader', ctcFilesUploaderDirective);
}());
