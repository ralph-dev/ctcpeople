/**
 * service to manage busy loading
 */
(function () {
    "use strict";
    angular.module("CTC.Components").factory("BusyBlockingService", function ($q, $timeout) {
        var block = false,
            delay = null,
            promises = [];

        return {
            start: function () {
                block = true;
            },
            end: function () {
                block = false;
            },
            block: function () {
                return promises.length > 0 || block;
            },
            until: function (value) {
                var promise = promises[promises.push($q.when(value)) - 1];

                promise.finally(function () {
                    promises.splice(promises.indexOf(promise), 1);
                    block = false;
                });

                if (!delay) {
                    delay = $timeout(function () {
                        block = true;
                    }, 100);
                }

                return promise;
            }
        };

    });

    angular.module("CTC.Components").directive("ctcStatus", function (BusyBlockingService) {
        return {
            scope: {},
            templateUrl: "common/directives/status/status.html",
            controllerAs: "$busy",
            controller: function () {
                angular.extend(this, BusyBlockingService);
            }
        };

    });

}());
