(function () {
    function reloadPageDialogController($scope, $attrs, $window) {
        var dialogTitle = "RPData error"; /*TODO: evaluate attrs to get the user set value */
        $scope.dialogTitle = dialogTitle;
        var RP_DATA_ERROR = "RP_DATA_ERROR";
        $scope.$on(RP_DATA_ERROR, function () {
            $scope.$modals[dialogTitle].show();
        });


        function acknowledgeRPSessionAlert() {
            $scope.$modals[dialogTitle].hide();
            $window.location.reload();
        }

        $scope.acknowledgeRPSessionAlert = acknowledgeRPSessionAlert;

    }

    angular.module("CTC.Components").controller("reloadPageDialogController", ["$scope", "$attrs", "$window", reloadPageDialogController]);

    function reloadPageDialog() {
        return {
            restrict: "EA",
            scope: {},
            controller: "reloadPageDialogController",
            templateUrl: "common/directives/reloadPageDialog/reloadPageDialog.html"
        };
    }

    angular.module("CTC.Components").directive("reloadPageDialog", reloadPageDialog);

}());
