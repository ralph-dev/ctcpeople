/**
 * used to add sortable animation to list element
 * @usage: ctc-sortable-list {atter} add as attribute to dome element
 * @sortable-record: {array} (required) link to array to be effected by the sorting ability
 * @on-reorder: {function} (optional)
 *
 */
(function () {
    "use strict";
    function sortableList() {
        return {
            restrict: "EA",
            scope: {
                sortableRecord: "=",
                onReorder: "&?"
            },
            link: function (scope, element, attrs) {

                scope.dragStart = function (e, ui) {
                    ui.item.data('start', ui.item.index());
                };
                scope.dragEnd = function (e, ui) {
                    var start = ui.item.data('start'),
                        end = ui.item.index();

                    scope.sortableRecord.splice(end, 0,
                        scope.sortableRecord.splice(start, 1)[0]);

                    scope.$apply();

                    if (angular.isDefined(scope.onReorder)) {
                        scope.onReorder();
                    }
                };

                // set element to be sortable based on ui-sortable jquery
                element.sortable({
                    start: scope.dragStart,
                    update: scope.dragEnd,
                    containment: "parent",
                  /*  placeholder: "ui-state-highlight" */
                });
            }

        };
    }

    angular.module("CTC.Components").directive('ctcSortableList', sortableList);
}());
