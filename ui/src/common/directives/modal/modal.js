angular.module("CTC.Components").directive("ctcModal", function () {
    return {
        scope: {
            title: "@title",
            dynamicTitle: "@?",
            save: "&ctcModal",
            onModalCancel: "&",
            saveButtonText: "@",
            saveButtonClass: "@",
            conformationDialog : "=?",
            hideSave: "=",
            modalSize: "@?"
        },
        transclude: true,
        templateUrl: "common/directives/modal/modal.html",
        controllerAs: "modal",
        controller: function ($scope, $element) {

            var modal = angular.element($element).find(".modal");

            if(!$scope.hasOwnProperty('conformationDialog')){
                $scope.conformationDialog = false;
            }

            if(!$scope.hasOwnProperty('saveButtonText')) {
                $scope.saveButtonText = 'Save Changes';
            }

            if(!$scope.hasOwnProperty("saveButtonClass") || $scope.saveButtonClass === undefined){
                $scope.saveButtonClass = "btn-success";
            }

            this.conformationDialog =  $scope.conformationDialog;
            this.saveButtonText = $scope.saveButtonText;
            this.saveButtonClass = $scope.saveButtonClass;

            $scope.$parent.$modals = angular.extend({}, $scope.$parent.$modals);
            $scope.$parent.$modals[$scope.title] = this;

            modal.on("shown.bs.modal", function () {
                angular.element(this).find(":input:enabled:visible").filter("input, select, textarea").first().focus();
            });

            modal.on("hidden.bs.modal", function () {
                (angular.isFunction($scope.onCancel) ? $scope.onCancel : angular.noop).call();
            });

            this.show = function show() {
                modal.modal("show");
            };

            this.hide = function () {
                if(angular.isDefined($scope.onModalCancel)&& angular.isFunction($scope.onModalCancel)){
                    $scope.onModalCancel();
                }
                modal.modal("hide");
            };

            if ($scope.hideSave) {
                $scope.hideSaveButton = true;
            }

            if (!$scope.hideSave) {

                this.save = function () {
                    if (angular.isFunction($scope.save) ? $scope.save() : true) {
                        this.hide();
                    }
                };

            }

            this.onModalCancel = function () {
                if (angular.isFunction($scope.onModalCancel)) {
                    $scope.onModalCancel();
                }
            };

        }
    };
});
