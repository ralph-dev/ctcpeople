/***
 * a wrapper directive to manage showing and hiding group of ctcDisplayManage directive
 *
 * @ closeOthers {boolean} = (default False)  close other open directive when new one is opened
 * @ singleOpen {boolean}  = (default False) allow one child directive to be open at any given time
 * @ isOpen {boolean} = communication atter set to true if any child element is open
 */

/***
 * ctcDisplayManager directive to manage hiding and showing element
 * @ ctcDisplayManager (boolean) expression to open and close element (true = show | False = hide )
 *
 */
angular.module("CTC.Components")
    .directive(
        "ctcGroupDisplayManager",
        function () {
            "use strict";
            return {
                restrict: "A",
                controller: ['$scope', '$attrs', function ($scope, $attrs) {
                    var self = this;
                    self.group = [];

                    function addChild(childDirective) {
                        self.group.push(childDirective);

                        // on directive destroy remove reference from the watcher group
                        childDirective.$on('$destroy', function (event) {
                            self.removeChild(childDirective);
                        });
                    }

                    function removeChild(childDirective) {
                        var index = self.group.indexOf(childDirective);
                        if (index !== -1) {
                            self.group.splice(index, 1);
                        }
                    }


                    function processOpening (targetChild, callback) {
                        var closeOthers = angular.isDefined($attrs.closeOthers)? $scope.$eval($attrs.closeOthers):false;
                        var singleOpen = angular.isDefined($attrs.singleOpen)?$scope.$eval($attrs.singleOpen):false;
                        if(closeOthers){
                            angular.forEach(self.group, function(childScope){
                                if(childScope !== targetChild){
                                    childScope.toggleOpen(false);
                                }
                            });
                            callback();
                        }
                        if(singleOpen){
                          var openedFlag = self.group.some(function(item){
                              if(item !== targetChild){
                                  return item.isOpen();
                              }
                              else {
                                  return false;
                              }
                          });
                            if(openedFlag){
                                targetChild.toggleOpen(false);
                            }
                            else{
                                callback();
                            }
                        }

                    }

                    /********* scope mapping ***********/
                    self.addChild = addChild;
                    self.removeChild = removeChild;
                    self.processOpening = processOpening;
                }]
            };
        });


/**
 * managing opening and closing of element..
 * @ onOpen link to a function to be run on element open
 */
angular.module("CTC.Components")
    .directive(
        "ctcDisplayManager",
        function ($parse) {
            "use strict";
            return {
                restrict: "A",
                require: "^?ctcGroupDisplayManager",
                link: function (scope, elem, attr, groupCtrl) {
                    // use directive declaration as expression holder
                    var expression = attr.ctcDisplayManager;
                    var onOpen = attr.ctcDisplayManager;

                    /*
                    * add directive to the group controller
                    * */
                    groupCtrl.addChild(scope);

                    if (!scope.$eval(expression)) {
                        elem.hide();
                    }

                    scope.toggleOpen = function (value) {
                        var targetExpression = $parse(attr.ctcDisplayManager);
                        targetExpression.assign(scope, value);
                    };

                    scope.isOpen = function(){
                        return scope.$eval(expression);
                    };

                    scope.$watch(expression, function (newValue, oldValue) {
                        /* TODO: add option to use directive only without parent controller  */
                        if (newValue === oldValue) {
                            return;
                        }
                        if (newValue) {
                            groupCtrl.processOpening(scope , function(){
                                elem.stop(true, true).show();
                                if(angular.isDefined(attr.onOpen)){
                                    // if on open is defined evaluate the function to run it
                                    scope.$eval(attr.onOpen);
                                }
                            });

                        }
                        else {
                            elem.stop(true, true).hide();
                        }

                    });

                }
            };
        });
