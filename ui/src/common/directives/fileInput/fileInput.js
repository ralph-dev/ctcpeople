(function () {
    "use strict";
    function fileInputDirective() {
        return {
            scope: {
                onUpload: "&",
                multiple: "=?",
                acceptedFileType:"=",
                inputName:"@name"
            },
            templateUrl: 'common/directives/fileInput/fileInput.html',
            require: ['^form', '^ngModel'],
            link: function (scope, element, attrs, $ctrl) {
                var form = $ctrl[0];
                var ngModel = $ctrl[1];
                scope.files = [];
                scope.inputModel;
                var multiple = false;
                var upload = element[0].querySelector('div.upload-zone');
                var uploadInput = element[0].querySelector('input.upload-input');

                if (attrs.$attr.multiple) {
                    scope.isMultiple = true;
                    $(uploadInput).attr('multiple', 'multiple');
                }

                /* reference the input in the form controller to access validation flag */
                // scope.inputCtrl = form[scope.inputName];
                scope.inputCtrl = form;
                function registerFiles(files) {
                    scope.files = files;
                    if (scope.inputCtrl.$valid) {
                        if (scope.onUpload) {
                            scope.onUpload({files: scope.files});
                        }
                    }
                }

                function onUploadInput(e) {
                    if (e.target && e.target.files) {
                        var files = updateModelWithFile(e.target.files);

                        if (files) {
                            ngModel.$setViewValue(files, e);

                            registerFiles(files);
                            scope.$apply();
                        }
                    }
                }

                function onDrop(e) {
                    if (e.dataTransfer && e.dataTransfer.files) {
                        var files = updateModelWithFile(e.dataTransfer.files);
                        if (files) {
                            ngModel.$setViewValue(files, e);

                            registerFiles(files);
                            scope.$apply();
                        }
                    }
                }

                uploadInput.addEventListener('drop', onDrop, false);
                uploadInput.addEventListener('change', onUploadInput, false);

                /***** prevent default behavior *******/
                function preventDefault(e) {
                    e.preventDefault();
                }
                upload.addEventListener('dragenter', preventDefault, false);
                upload.addEventListener('dragleave', preventDefault, false);
                upload.addEventListener('dragover', preventDefault, false);
                upload.addEventListener('drop', preventDefault, false);


                var fileTypeRegex;
                var fileTypes;

                function updateModelWithFile(files) {
                    if (!angular.isDefined(attrs.multiple)) {
                        if (files.length > 1) {
                            // disallow multiple files
                            return false;
                        } else {
                            files = files[0];
                        }
                    } else {
                        files = Array.prototype.slice.apply(files);
                    }

                    if (attrs.$attr.maxSize) {
                        ngModel.$validators.maxSize = function (modelValue, viewValue) {
                            var result = true;

                            if (ngModel.$isEmpty(modelValue)) {
                                // consider empty models to be valid
                                return result;
                            }

                            var value = modelValue || viewValue;

                            if (!angular.isArray(value)) {
                                value = [value];
                            }

                            result = value.some(function (file) {
                                return file.size <= attrs.maxSize * 1048576;
                            });

                            return result;
                        };
                    }

                    if (attrs.$attr.accept) {
                        var accept = JSON.parse(attrs.accept.split(','));
                        setSupportedFileTypes(accept);
                        // ngModel.$parsers.push(myValidation);
                        ngModel.$validators.accept = function (modelValue, viewValue) {
                            if (ngModel.$isEmpty(modelValue)) {
                                // consider empty models to be valid
                                return true;
                            }
                            var value = modelValue || viewValue;
                            if (!angular.isArray(value)) {
                                value = [value];
                            }
                            var result = isValidFileType(value);
                            return result;
                        };
                    }

                    // scope.$apply();
                    return [files];
                }

                function setSupportedFileTypes(typesList) {
                    // create a sting with | separator then remove and dots at the start of the extension
                    fileTypes = typesList.join('|').replace(/\|\./g, '|');
                    var regexSt = "(.*?)\.(" + fileTypes + ")$";

                    fileTypeRegex = new RegExp(regexSt, 'i');
                }

                /***
                 * test if any of the files are invalid
                 * @param files
                 * @returns {boolean}
                 */
                function isValidFileType(files) {
                    var fileList = [];
                    Array.prototype.push.apply(fileList, files);
                    var invalidFormat = false; // default validation is true.
                    if (fileTypes) {
                        // loop through files and test for invalid file extensions
                        invalidFormat = fileList.some(function (fileData) {
                            return !fileTypeRegex.test(fileData.name);
                        });
                    }
                    return !invalidFormat;
                }

                attrs.$observe(
                    "accept",
                    function innerObserveFunction(newValue, oldValue) {
                        var accept = JSON.parse(attrs.accept.split(','));
                        setSupportedFileTypes(accept);
                        if (newValue != oldValue) {
                            ngModel.$validators.accept = function (modelValue, viewValue) {
                                if (ngModel.$isEmpty(modelValue)) {
                                    // consider empty models to be valid
                                    return true;
                                }
                                var value = modelValue || viewValue;
                                if (!angular.isArray(value)) {
                                    value = [value];
                                }
                                var result = isValidFileType(value);
                                return result;
                            };
                        }
                    }
                );


                // if (attrs.$attr.accept) {
                //     var accept = attrs.accept.split(',');
                //     setSupportedFileTypes(accept);
                //    // ngModel.$parsers.push(myValidation);
                //     ngModel.$validators.accept = function (modelValue, viewValue) {
                //         if (ngModel.$isEmpty(modelValue)) {
                //             // consider empty models to be valid
                //             return true;
                //         }
                //         var value = modelValue || viewValue;
                //         if (!angular.isArray(value)) {
                //             value = [value];
                //         }
                //         var result = isValidFileType(value);
                //         return result;
                //     };
                // }

                // if (attrs.$attr.maxSize) {
                //     ngModel.$validators.maxSize = function (modelValue, viewValue) {
                //         var result = true;

                //         if (ngModel.$isEmpty(modelValue)) {
                //             // consider empty models to be valid
                //             return result;
                //         }

                //         var value = modelValue || viewValue;

                //         if (!angular.isArray(value)) {
                //             value = [value];
                //         }

                //         result = value.some(function (file) {
                //             return file.size <= attrs.maxSize * 1048576;
                //         });

                //         return result;
                //     };
                // }
            }

        };
    }

    angular.module("CTC.Components").directive('fileInput', fileInputDirective);
}());

// (function(){
//     "use strict";
//     function fileInputValidator() {
//         return {
//             restrict: "A",
//             require: '^ngModel',
//             link: function (scope, element, attrs, ngModel) {
//                // var ngModel = ctrls[1];
//                 var fileTypeRegex;
//                 var fileTypes;
//                 element.on('change', updateModelWithFile);

//                 function updateModelWithFile(event) {
//                     var files = event.target.files;
//                     if (!angular.isDefined(attrs.multiple)) {
//                         files = files[0];
//                     } else {
//                         files = Array.prototype.slice.apply(files);
//                     }
//                     ngModel.$setViewValue(files, event);
//                 }

//                 function setSupportedFileTypes(typesList) {
//                     // create a sting with | separator then remove and dots at the start of the extension
//                     fileTypes = typesList.join('|').replace(/\|\./g, '|');
//                     var regexSt = "(.*?)\.(" + fileTypes + ")$";

//                     fileTypeRegex = new RegExp(regexSt);
//                 }

//                 /***
//                  * test if any of the files are invalid
//                  * @param files
//                  * @returns {boolean}
//                  */
//                 function isValidFileType(files) {
//                     var fileList = [];
//                     Array.prototype.push.apply(fileList, files);
//                     var invalidFormat = false; // default validation is true.
//                     if (fileTypes) {
//                         // loop through files and test for invalid file extensions
//                         invalidFormat = fileList.some(function (fileData) {
//                             return !fileTypeRegex.test(fileData.name);
//                         });
//                     }
//                     return !invalidFormat;
//                 }

//                 attrs.$observe(
//                     "accept",
//                     function innerObserveFunction(newValue, oldValue) {
//                         var accept = attrs.accept.split(',');
//                         setSupportedFileTypes(accept);
//                         if(newValue != oldValue){
//                             ngModel.$validators.accept = function (modelValue, viewValue) {
//                                 if (ngModel.$isEmpty(modelValue)) {
//                                     // consider empty models to be valid
//                                     return true;
//                                 }
//                                 var value = modelValue || viewValue;
//                                 if (!angular.isArray(value)) {
//                                     value = [value];
//                                 }
//                                 var result = isValidFileType(value);
//                                 return result;
//                             };
//                         }
//                     }
//                 );


//                 if (attrs.$attr.accept) {
//                     var accept = attrs.accept.split(',');
//                     setSupportedFileTypes(accept);
//                    // ngModel.$parsers.push(myValidation);
//                     ngModel.$validators.accept = function (modelValue, viewValue) {
//                         if (ngModel.$isEmpty(modelValue)) {
//                             // consider empty models to be valid
//                             return true;
//                         }
//                         var value = modelValue || viewValue;
//                         if (!angular.isArray(value)) {
//                             value = [value];
//                         }
//                         var result = isValidFileType(value);
//                         return result;
//                     };
//                 }

//                 if (attrs.$attr.maxSize) {
//                     ngModel.$validators.maxSize = function (modelValue, viewValue) {
//                         var result = true;

//                         if (ngModel.$isEmpty(modelValue)) {
//                             // consider empty models to be valid
//                             return result;
//                         }

//                         var value = modelValue || viewValue;

//                         if (!angular.isArray(value)) {
//                             value = [value];
//                         }

//                         result = value.some(function (file) {

//                             return file.size <= attrs.maxSize * 1048576;
//                         });


//                         return result;
//                     };
//                 }

//             }
//         };
//     }
//     angular.module("CTC.Components").directive('fileInputValidator', fileInputValidator);

// }());
