/**
 * create a selectable list , build based on jquery-ui selectable
 * @usage selectable-list
 * @source-list {array} (required) list of object used to generate the list
 * @selected-list {Array} (required) array store the selected items from sourceList
 * @displayLabel {String} (required) ref to the property used as display label for the list items support nested property expression (dot notation and array notation) for nested property
 */
(function () {
    function selectableList() {
        return {
            restrict: "EA",
            templateUrl: "common/directives/selectableList/selectableList.html",
            scope: {
                sourceList: "=",
                selectedList: "=",
                displayLabel: "@"
            },
            link: function (scope, element, attr) {
                scope.selectableList = [];

                /**
                 * retrieve the value of object property or nested property
                 * @param targetOb
                 * @param propPath {string} name or nested name of the property
                 * @returns {*}
                 */
                function getObjectPropValue(targetOb, propPath) {
                    try {
                        var separator = '.';
                        // remove and replace array declaration with "." separator and remove first "." if any
                        var path = propPath.replace(/\[/ig, separator).replace(/\]/ig, '').replace(/^\./, '');
                        var props = path.split(separator);
                        var result = props.reduce(function (obj, property) {
                            return obj[property];
                        }, targetOb)
                        return result;
                    } catch (error) {
                        return undefined;
                    }
                }

                function SelectableListObject(dataObj, label, index) {
                    this.id = "item_" + index;
                    this.label = getObjectPropValue(dataObj, label);
                    this.selected = false;
                    this.source = dataObj;
                }

                function setSelectableList() {
                    scope.selectableList = scope.sourceList.map(function (item, index) {
                        return new SelectableListObject(item, scope.displayLabel, index);
                    });
                }

                function updateSelectedList() {
                    scope.selectedList = scope.selectableList.filter(function (item) {
                        return item.selected === true
                    }).map(function (item) {
                        return item.source;
                    });
                }

                function onSelected(event, ui) {
                    var scopeRef = angular.element(ui.selected).scope();
                    scopeRef.item.selected = true;
                    scope.$apply(function () {
                        updateSelectedList();
                    });
                }

                function onUnselected(event, ui) {
                    var scopeRef = angular.element(ui.unselected).scope();
                    scopeRef.item.selected = false;
                    scope.$apply(function () {
                        updateSelectedList();
                    });
                }

                // set the list to jquery selectable
                var el = element[0].querySelector("ol");
                var listElem = angular.element(el);
                listElem.selectable({
                    selected: onSelected,
                    unselected: onUnselected
                });

                // set the directive data
                scope.$watchCollection('sourceList', function (newVal, oldVal) {
                    if (newVal) {
                        setSelectableList();
                    }
                });

            }
        }
    }

    angular.module("CTC.Components").directive('selectableList', selectableList);

}());
