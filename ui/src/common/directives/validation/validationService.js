(function () {

    'use strict';

    function validationService() {

        function validatePhoneNumber(phoneNumber, countryCode) {

            if (typeof(phoneNumber) !== 'string' || typeof(countryCode) !== 'string') {
                return false;
            }

            if (phoneNumber.length > 0) {

                if (phoneUtils.isValidNumber(phoneNumber, countryCode)) {

                    // return mobile phone as internation format.
                    return phoneUtils.formatInternational(phoneNumber, countryCode);

                }
            }

            // Invalid phone number
            return false;

        }

        function validateFixedLinePhoneNumber(phoneNumber, countryCode) {

            if (typeof(phoneNumber) !== 'string' || typeof(countryCode) !== 'string') {
                return false;
            }

            if (phoneNumber.length > 0) {

                if (phoneUtils.isValidNumber(phoneNumber, countryCode)) {

                    var fixedLine = phoneUtils.formatInternational(phoneNumber, countryCode);

                    if (phoneUtils.getNumberType(fixedLine) !== "FIXED_LINE") {
                        return false;
                    }

                    // return fixed phone line as international format.
                    return fixedLine;

                }
            }

            // Invalid phone number
            return false;

        }

        function validateMobilePhoneNumber(phoneNumber, countryCode) {

            if (typeof(phoneNumber) !== 'string' || typeof(countryCode) !== 'string') {
                return false;
            }

            if (phoneNumber.length > 0) {

                if (phoneUtils.isValidNumber(phoneNumber, countryCode)) {

                    var mobile = phoneUtils.formatInternational(phoneNumber, countryCode);

                    if (phoneUtils.getNumberType(mobile) !== "MOBILE") {
                        return false;
                    }

                    // return mobile phone as international format.
                    return mobile;

                }
            }

            // Invalid phone number
            return false;

        }


        function validateEmail(email) {

            if (email !== undefined && email.length > 0) {

                var emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (emailRegEx.test(email)) {
                    return true;
                }

            }

            return false;

        }

        function validateABN(abnString) {

            // var ABNRex = /^(\d *?){11}$/;

            var weights = new Array(10, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19);

            while (abnString.indexOf(/[^\d]/) !== -1) {
                abnString = abnString.replace(/[^\d]/, '');
            }

            var ABNsum = 0;

            if (abnString.length === 11) {

                var sum = 0;

                for (var i = 0; i < abnString.length; i = i + 1) {

                    if (i === abnString.length - 1) {
                        ABNsum += abnString.substring(i) * weights[i];
                    } else {

                        if (i === 0) {
                            ABNsum += (abnString.substring(i, i + 1) - 1) * weights[i];
                        } else {
                            ABNsum += abnString.substring(i, i + 1) * weights[i];
                        }

                    }

                }

                if (ABNsum % 89 === 0) {
                    return true;
                } else {
                    return false;
                }
            }

            return false;
        }

        //This is a temp fix for getting the actual country code required for this validation service
        function getCountryCode(countryString) {
            var countryCode = "";
            if (countryString === "+61 - Australia") {
                countryCode = "AU";
            } else if (countryString === "+86 - China") {
                countryCode = "CN";
            } else if (countryString === "+65 - Singapore") {
                countryCode = "SG";
            }
            return countryCode;
        }

        return {
            'phoneNumber': validatePhoneNumber,
            'fixedLineNumber': validateFixedLinePhoneNumber,
            'mobileNumber': validateMobilePhoneNumber,
            'email': validateEmail,
            'ABN': validateABN,
            'getCountryCode': getCountryCode
        };

    }

    angular.module('CTC.Components').factory('validationService', validationService);

    angular.module('CTC.Components').directive('showErrors', function ($timeout) {
        return {
            restrict: 'A',
            require: '^form',
            link: function (scope, el, attrs, formCtrl) {
                // find the text box element, which has the 'name' attribute
                var inputEl = el[0].querySelector("[name]");
                // convert the native text box element to an angular element
                var inputNgEl = angular.element(inputEl);
                // get the name on the text box
                var inputName = inputNgEl.attr('name');

                // only apply the has-error class after the user leaves the text box
                inputNgEl.bind('blur', function () {
                    el.toggleClass('has-error', formCtrl[inputName].$invalid);
                });

                scope.$on('show-errors-check-validity', function () {
                    el.toggleClass('has-error', formCtrl[inputName].$invalid);
                });

                scope.$on('show-errors-reset', function () {
                    $timeout(function () {
                        el.removeClass('has-error');
                    }, 0, false);
                });
            }
        };
    });


    angular.module('CTC.Components').directive('dateRequired', [function () {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {

                var checkDateRequired = function (inputValue) {
                    var isValid = true;

                    if (inputValue === "" || inputValue === undefined || inputValue === null || inputValue === "Invalid Date") {
                        isValid = false;
                    }

                    ctrl.$setValidity("isRequired", isValid);
                    return inputValue;
                };
                ctrl.$parsers.unshift(checkDateRequired);
                ctrl.$formatters.push(checkDateRequired);
                attrs.$observe("isRequired", function () {
                    checkDateRequired(ctrl.$viewValue);
                });
            }
        };
    }]);

    angular.module('CTC.Components').directive('dateFormatValidator', [function () {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                var validateDateFormat = function (inputValue) {
                    var regexp = /^(?:(?:31(\/)(?:0[13578]|1[02]))\1|(?:(?:29|30)(\/)(?:0[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0[1-9]|1\d|2[0-8])(\/)(?:(?:0[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
                    var isValid = regexp.test(inputValue);

                    if (inputValue === "" || inputValue === undefined || inputValue === null || inputValue === "Invalid date") {
                        isValid = true;
                    }

                    ctrl.$setValidity("isValidFormatDate", isValid);
                    return inputValue;
                };
                ctrl.$parsers.unshift(validateDateFormat);
                ctrl.$formatters.push(validateDateFormat);
                attrs.$observe("isValidFormatDate", function () {
                    validateDateFormat(ctrl.$viewValue);
                });
            }
        };
    }]);

    angular.module('CTC.Components').directive('dateNotInPast', [function () {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                var validateDateFormat = function (inputValue) {

                    var isValid = false;

                    // Setting Yesterday date for validation on greater than datepickers
                    var yesterday = new Date();
                    yesterday.setDate(yesterday.getDate() - 1);

                    var dateDiff = moment(inputValue, "DD/MM/YYYY").valueOf() - moment(yesterday).valueOf();

                    if (dateDiff > 0 && dateDiff !== undefined) {
                        isValid = true;
                    }

                    ctrl.$setValidity("isPastDate", isValid);
                    return inputValue;
                };
                ctrl.$parsers.unshift(validateDateFormat);
                ctrl.$formatters.push(validateDateFormat);
                attrs.$observe("isPastDate", function () {
                    validateDateFormat(ctrl.$viewValue);
                });
            }
        };
    }]);

    angular.module('CTC.Components').directive("dateGreaterThan", ["$filter", function ($filter) {
        return {
            require: "ngModel",
            link: function (scope, elm, attrs, ctrl) {
                var validateDateRange = function (inputValue) {
                    var fromDate = attrs.dateGreaterThan;

                    var toDate;
                    if(moment(inputValue, "DD/MM/YYYY", true).isValid()) {
                        toDate = inputValue;
                    }else {
                        toDate = moment(inputValue).format('DD/MM/YYYY');
                    }

                    var isValid = isValidDateRange(fromDate, toDate);
                    ctrl.$setValidity("dateGreaterThan", isValid);
                    return inputValue;
                };

                ctrl.$parsers.unshift(validateDateRange);
                ctrl.$formatters.push(validateDateRange);
                attrs.$observe('dateGreaterThan', function () {
                    validateDateRange(ctrl.$viewValue);
                });
            }
        };
    }]);

    angular.module('CTC.Components').directive('dateLowerThan', ['$filter', function ($filter) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                var validateDateRange = function (inputValue) {

                    var fromDate;
                    if(moment(inputValue, "DD/MM/YYYY", true).isValid()) {
                        fromDate = inputValue;
                    }else {
                        fromDate = moment(inputValue).format('DD/MM/YYYY');
                    }
                    var toDate = attrs.dateLowerThan;
                    var isValid = isValidDateRange(fromDate, toDate);
                    ctrl.$setValidity('dateLowerThan', isValid);
                    return inputValue;
                };
                ctrl.$parsers.unshift(validateDateRange);
                ctrl.$formatters.push(validateDateRange);
                attrs.$observe('dateLowerThan', function () {
                    validateDateRange(ctrl.$viewValue);
                });
            }
        };
    }]);

    angular.module('CTC.Components').directive('dateRequiredBy', ['$filter', function ($filter) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                function validateRequiredBy (inputValue) {
                    // if valid requiredByDate
                    if(attrs.dateRequiredBy && moment(attrs.dateRequiredBy, "DD/MM/YYYY", true).isValid()){
                        if(!inputValue){
                            ctrl.$setValidity('requiredBy', false);
                        }else {
                            ctrl.$setValidity('requiredBy', true);
                        }
                    }else {
                        // if both fields are empty then its valid
                        ctrl.$setValidity('requiredBy', true);
                    }

                    return inputValue;
                }

                ctrl.$parsers.unshift(validateRequiredBy);
                ctrl.$formatters.push(validateRequiredBy);
                attrs.$observe('dateRequiredBy', function () {
                    validateRequiredBy(ctrl.$modelValue);
                });
            }
        };
    }]);

    angular.module('CTC.Components').directive('timeGreaterThan', function () {
        return {
            require: 'ngModel',

            link: function (scope, elm, attrs, ctrl) {

                var validateTime = function (inputValue) {

                    var formTime = attrs.timeGreaterThan;

                    var toTime = inputValue;

                    var isValid = isValidTimeRange(formTime, toTime);

                    ctrl.$setValidity('timeGreaterThan', isValid);

                    return inputValue;
                };

                ctrl.$parsers.unshift(validateTime);

                ctrl.$formatters.push(validateTime);

                attrs.$observe('timeGreaterThan', function () {
                    validateTime(ctrl.$modelValue);
                });
            }
        };
    });

    angular.module('CTC.Components').directive('timeLowerThan', function () {
        return {
            require: 'ngModel',

            link: function (scope, elm, attrs, ctrl) {

                var validateTime = function (inputValue) {

                    var formTime = inputValue;

                    var toTime = attrs.timeLowerThan;

                    var isValid = isValidTimeRange(formTime, toTime);

                    ctrl.$setValidity('timeLowerThan', isValid);

                    return inputValue;
                };

                ctrl.$parsers.unshift(validateTime);

                ctrl.$formatters.push(validateTime);

                attrs.$observe('timeLowerThan', function () {

                    validateTime(ctrl.$modelValue);

                });
            }
        };
    });

    angular.module('CTC.Components').directive('timeFormatValidator', function () {
        return {
            require: 'ngModel',

            link: function (scope, elm, attrs, ctrl) {

                var validateTime = function (inputValue) {

                    var regexp = /((^(0)(7|8|9))|(^(10|11|12|13|14|15|16|17|18|19))):((((00)|(30))))\W{0}$/;

                    var isValid = regexp.test(inputValue);

                    if (inputValue === '' || inputValue === undefined || inputValue === null) {
                        isValid = true;
                    }

                    ctrl.$setValidity('isValidFormatTime', isValid);

                    return inputValue;
                };

                ctrl.$parsers.unshift(validateTime);

                ctrl.$formatters.push(validateTime);

                attrs.$observe('isValidFormatTime', function () {

                    validateTime(ctrl.$modelValue);

                });
            }
        };
    })

    angular.module('CTC.Components').directive('numberGreaterThan', function () {
        return {
            require: 'ngModel',

            link: function (scope, elm, attrs, ctrl) {

                var validateNumber = function (inputValue) {
                    var toNumber = inputValue;
                    var fromNumber = attrs.numberGreaterThan;

                    var isValid = isValidNumberRange(fromNumber, toNumber);

                    if (toNumber === "" || toNumber === undefined || fromNumber === "" || fromNumber === undefined) {
                        isValid = true;
                    }

                    ctrl.$setValidity('isNumberGreaterThan', isValid);

                    return inputValue;
                };

                attrs.$observe("numberGreaterThan", function (newValue, oldValue) {
                    if (newValue) {
                        validateNumber(ctrl.$modelValue);
                    }
                });

                ctrl.$parsers.unshift(validateNumber);

                ctrl.$formatters.push(validateNumber);

                attrs.$observe('isNumberGreaterThan', function () {
                    validateNumber(ctrl.$modelValue);
                });
            }
        };
    });

    angular.module('CTC.Components').directive('numberLowerThan', function () {
        return {
            require: 'ngModel',

            link: function (scope, elm, attrs, ctrl) {

                var validateNumber = function (inputValue) {
                    var toNumber = attrs.numberLowerThan;
                    var fromNumber = inputValue;

                    var isValid = isValidNumberRange(fromNumber, toNumber);

                    if (toNumber === "" || toNumber === undefined || fromNumber === "" || fromNumber === undefined) {
                        isValid = true;
                    }

                    ctrl.$setValidity('isNumberLowerThan', isValid);

                    return inputValue;
                };

                attrs.$observe("numberLowerThan", function (newValue, oldValue) {
                    if (newValue) {
                        validateNumber(ctrl.$modelValue);
                    }
                });

                ctrl.$parsers.unshift(validateNumber);

                ctrl.$formatters.push(validateNumber);

                attrs.$observe('isNumberLowerThan', function () {
                    validateNumber(ctrl.$modelValue);
                });
            }
        };
    });
    angular.module('CTC.Components').directive("currencyGreaterThan", function () {
        return {
            require: 'ngModel',

            link: function (scope, elm, attrs, ctrl) {

                var validateNumber = function (inputValue) {
                    var toNumber = convertCurrencyToNumber(inputValue);
                    var fromNumber = convertCurrencyToNumber(attrs.currencyGreaterThan);

                    var isValid = isValidNumberRange(fromNumber, toNumber);

                    if (isNaN(toNumber) || isNaN(fromNumber) || toNumber === "" || fromNumber === "") {
                        isValid = true;
                    }

                    ctrl.$setValidity('isCurrencyGreaterThan', isValid);

                    return inputValue;
                };

                attrs.$observe("currencyGreaterThan", function (newValue, oldValue) {
                    if (newValue) {
                        validateNumber(ctrl.$modelValue);
                    }
                });

                ctrl.$parsers.unshift(validateNumber);

                ctrl.$formatters.push(validateNumber);

                attrs.$observe('isCurrencyGreaterThan', function () {
                    validateNumber(ctrl.$modelValue);
                });
            }
        };
    });

    angular.module('CTC.Components').directive("currencyLowerThan", function () {
        return {
            require: 'ngModel',

            link: function (scope, elm, attrs, ctrl) {

                var validateNumber = function (inputValue) {
                    var toNumber = convertCurrencyToNumber(attrs.currencyLowerThan);
                    var fromNumber = convertCurrencyToNumber(inputValue);

                    var isValid = isValidNumberRange(fromNumber, toNumber);

                    if (isNaN(toNumber) || isNaN(fromNumber) || toNumber === "" || fromNumber === "") {
                        isValid = true;
                    }

                    ctrl.$setValidity('isCurrencyLowerThan', isValid);

                    return inputValue;
                };

                attrs.$observe("currencyLowerThan", function (newValue, oldValue) {
                    if (newValue) {
                        validateNumber(ctrl.$modelValue);
                    }
                });

                ctrl.$parsers.unshift(validateNumber);

                ctrl.$formatters.push(validateNumber);

                attrs.$observe('isCurrencyLowerThan', function () {
                    validateNumber(ctrl.$modelValue);
                });
            }
        };
    });


    /****** global functions used by multiple directives *****/
    //TODO: add this function to a util service
    var isValidNumberRange = function (number1, number2) {
        return parseInt(number1) < parseInt(number2);
    };
    // Kendo Date Validation Directives

    var isValidDate = function (dateStr) {
        if (dateStr === undefined) {
            return false;
        }
        if(!moment(dateStr, "DD/MM/YYYY", true).isValid()) {
            return false;
        }
        var dateTime = new moment(dateStr, "DD/MM/YYYY");
        if (isNaN(dateTime)) {
            return false;
        }
        return true;
    };

    var getDateDifference = function (fromDate, toDate) {
        return moment(toDate, "DD/MM/YYYY").valueOf() - moment(fromDate, "DD/MM/YYYY").valueOf();
    };

    var isValidDateRange = function (fromDate, toDate) {
        if (fromDate === "" || toDate === "") {
            return true;
        }

        if (fromDate === "Invalid date" || toDate === "Invalid date") {
            return true;
        }

        if (fromDate === "0000-01-01" || toDate === "0000-01-01") {
            return true;
        }

        if (fromDate === null || toDate === null) {
            return true;
        }

        if (isValidDate(fromDate) === false) {
            return false;
        }

        if (isValidDate(toDate) === false) {
            return false;
        }else if (isValidDate(toDate) === true) {
            var days = getDateDifference(fromDate, toDate);
            if (days < 0 && days !== undefined) {
                return false;
            }
        }
        return true;
    };

    var isValidTimeRange = function (fromTime, toTime) {

        if (fromTime === "" || toTime === "") {
            return true;
        }

        if (fromTime === undefined || toTime === undefined) {
            return true;
        }

        if (fromTime === null || toTime === null) {
            return true;
        }

        if (fromTime === "Invalid date" || toTime === "Invalid date") {
            return true;
        }

        var fromTimeMilliseconds = moment(fromTime, "h:mm A").valueOf();

        var toTimeMilliseconds = moment(toTime, "h:mm A").valueOf();

        if (fromTimeMilliseconds > toTimeMilliseconds) {
            return false;
        }

        return true;

    };

    var convertCurrencyToNumber = function (currencyString) {
        if (currencyString === undefined) {
            return "";
        } else {
            return parseInt(currencyString.replace(/[$,]/g, ""));
        }
    };

    /****** end of global functions *****/

})();
