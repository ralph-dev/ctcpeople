(function () {
    "use strict";
    /**
     * component for multi Select picklist two selectable lists with "Add" and "Remove" buttons to move data between.
     * this component manipulate source list. both lists items most be of the same object type
     * @left-list {array} list of object to be displayed in the left side selectable list
     * @right-list {array} list of objects to be displayed in the right side selectable list
     * @left-list-label {String} label for the left selectable list
     * @right-list-label {String} label for the right selectable list
     * @displayLabel {String} reference to the property used as  display label in the selectable list.
     */
    function multiSelectPicklistController() {
        var ctrl = this;

        ctrl.leftList_selected = [];
        ctrl.rightList_selected = [];

        function moveSelected(selectedList, fromList, toList) {
            selectedList.forEach(function (item) {
                var itemIndex = fromList.indexOf(item);
                if (itemIndex >= 0) {
                    toList.push(item);
                    fromList.splice(itemIndex, 1);
                }
            });
        }

        ctrl.moveSelected = moveSelected;
    }

    var multiSelectPicklist = {
        templateUrl: 'common/directives/multiSelectPicklist/multiSelectPicklist.html',
        controller: multiSelectPicklistController,
        bindings: {
            leftList: "=",
            rightList: "=",
            displayLabel: "@",
            leftListLabel: "@",
            rightListLabel: "@"
        }

    };
    angular.module("CTC.Components").component('multiSelectPicklist', multiSelectPicklist);
}());
