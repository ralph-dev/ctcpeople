/**
 * managing busy loading animation
 * @ startFinishFlag
 */
angular.module("CTC.Components").service("busyLoader",function($rootScope){
    var BUSY_FLAG = false;
    var brodcastStart = "BUSY_LOADER_START";
    var brodcastEnd = "BUSY_LOADER_END";
    function busyFlag(){
        return BUSY_FLAG;
    }

    function start(){
        BUSY_FLAG = true;
        console.log("start service");
        $rootScope.$broadcast('busyLoader:start',BUSY_FLAG);
    }

    function end(){
        BUSY_FLAG = false;
        $rootScope.$broadcast('busyLoader:start',BUSY_FLAG);
    }

    return{
        busyFlag:busyFlag,
        start: start,
        end: end
    };
});

/**
 * style is based on status.less in status directive
 */
angular.module("CTC.Components").directive("busyLoadingView", function ($templateRequest,$compile, busyLoader ) {
    return {
        restrict: "EA",
        templateUrl: "common/directives/busyLoading/busyLoading.html",
        controllerAs: "$busy",
        controller: function () {
            angular.extend(this, busyLoader);
        }
    };
});
