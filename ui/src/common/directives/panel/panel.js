(function () {
    "use strict";
    angular.module("CTC.Components").directive("ctcPanel", function () {
        return {
            transclude: true,
            scope: {
                name: "@ctcPanel",
                icon: "@ctcIcon"
            },
            templateUrl: "common/directives/panel/panel.html",
            link: function (scope, elem, attrs) {
                // TODO update this directive to use new panel format
            }
        };
    });
}());
