angular
    .module("CTC.Components")

    .directive('map', function() {

        return {
            restrict: 'A',
            scope: {
                latitude: '@',
                longitude: '@',
                random: '@',
                address: '@',
                mapHeight: '@',
                mapWidth: '@',
                dynamicDimention:'='
            },
            replace: true,
            template: '<div id="google-map"  class="center-block"></div>',
            link: function(scope, element, attr) {

                var map, infoWindow, markers = [];
                var mapHeight = 200, mapWidth = 300;

                function setDimentions() {
                    if (angular.isDefined(attr.dynamicDimention) && scope.dynamicDimention) {
                        var tempWidth = element.parent().width();
                        if (tempWidth) {
                            mapHeight = Math.floor(tempWidth * 0.7);
                            mapWidth = Math.floor(tempWidth * 0.8);
                        }
                    }
                    if (angular.isDefined(attr.mapWidth)) {
                        mapWidth = parseInt(scope.mapWidth);
                    }
                    if (angular.isDefined(attr.mapHeight)) {
                        mapHeight = parseInt(scope.mapHeight);
                    }

                    element.width(mapWidth);
                    element.height(mapHeight);
                }

                // init the map
                function initMap(options) {
                    map = new google.maps.Map(element[0], options);
                }

                // place a marker
                function setMarker(map, position, title, content) {
                    var marker;

                    var markerOptions = {
                        position: position,
                        map: map,
                        title: title,
                        icon: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png'
                    };

                    marker = new google.maps.Marker(markerOptions);
                    markers.push(marker); // add marker to array

                    google.maps.event.addListener(marker, 'click', function () {
                        // close window if not undefined
                        if (infoWindow !== void 0) {
                            infoWindow.close();
                        }
                        // create new window
                        var infoWindowOptions = {
                            content: content
                        };

                        infoWindow = new google.maps.InfoWindow(infoWindowOptions);
                        infoWindow.open(map, marker);
                    });
                }

                function setupMap () {
                    // convert strings to float
                    var lng = parseFloat(scope.longitude);
                    var lat = parseFloat(scope.latitude);

                    var mapOptions = {
                        center: new google.maps.LatLng(lat, lng),
                        zoom: 15,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    setDimentions();
                    initMap(mapOptions);
                    setMarker(map, new google.maps.LatLng(lat, lng), scope.address, scope.address);
                }

                scope.$watch("longitude + latitude", function() {

                    if(scope.longitude !== '' && scope.latitude !== '') {
                        setupMap();
                    }

                });

                // random used to force refresh the map when map is hidden.
                scope.$watch("random", function() {
                    if(scope.longitude !== '' && scope.latitude !== '') {
                        setupMap();
                    }
                });

            }
        };

    });
