angular.module("CTC.Components").directive("ctcHeader", ["$window","urlPrefixService", "helperService",function ($window, urlPrefixService,helperService) {
    return {
        restrict:"EA",
        scope: {
            headerTitle: "@",
            description: "@",
            returnUrl: "@ctcReturn",
            ctcReturnAlt: "@?"
        },
        templateUrl: "common/directives/header/header.html",
        link: function(scope, element, attrs) {
            scope.logoURL = "static/img/CTC_logos-People_white.png";


            if(angular.isUndefined(scope.logoURL)) {
                urlPrefixService.getDetail().then(function(org) {
                    scope.logoURL = org.companyLogoURL;
                });
            }

            scope.$on('header:updateDescription', function(event, description) {
                scope.description = description;
            });

            scope.returnPage = function(){
            	
            	var url = "/";
            	if(scope.returnUrl && scope.returnUrl!=="" ){
            		if(scope.returnUrl.indexOf("/") === 0){
            			url = scope.returnUrl;
            		}else{
            			url += scope.returnUrl;
            		}	
            	}
            	
            	helperService.locateTo($window, url);
            	
               
            };

            scope.concatTitleDescription = function(){
                if(angular.isDefined(scope.description)){
                    return [scope.headerTitle,scope.description].join(" - ");
                }else{
                    return scope.headerTitle;
                }
            };

            scope.isLightningEnabled = true;
            var lightningMatch = "lightning.force.com";

            // if(window.location.href.indexOf(lightningMatch) > -1) {
            //     console.log('Lightning is enabled');
            //     scope.isLightningEnabled = false;
            // }
        }
    };
}]);
