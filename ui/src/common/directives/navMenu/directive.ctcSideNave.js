/**
 * side menu directive
 * implementation options
 * @ menuItems = array of menu item objects  {title:"", icon:"", url:"", action:""}
 **/
(function () {
    "use strict";
    angular.module("CTC.Components").directive("ctcSideNav", ["$location", "$anchorScroll", function ($location, $anchorScroll) {
        return {
            restrict: "AE",
            replace: "true",
            scope: {
                menuItems: "= menuItems"
            },
            templateUrl: "common/directives/navMenu/ctcSideNav.html",
            link: function (scope, element, attars) {
                //TODO: handle location change

                function navigateToAnchor(url) {
                    var newHash = url.trim().substring(1);
                    $anchorScroll.yOffset = 100;   // always scroll by 50 extra pixels
                    if ($location.hash() !== newHash) {
                        // set the $location.hash to `newHash` and
                        // $anchorScroll will automatically scroll to it
                        $location.hash(newHash);
                    } else {
                        // call $anchorScroll() explicitly,
                        // since $location.hash hasn't changed
                        $anchorScroll();
                    }
                }

                function navigateToLocation(url) {
                    if ($location.path() !== url) {
                        $location.path(url);
                    }
                }

                /**
                 * preform nav action
                 * check type of action attached to the menu Item (url, anchor, function)
                 */
                scope.navAction = function (menuItem) {
                    // check the path and set to "/" root if no path exist
                    if (!$location.path()) {
                        $location.path("/");
                    }
                    if (menuItem.url) {
                        // check if its anchor
                        if (menuItem.url.trim().substring(0, 1) === "#") {
                            navigateToAnchor(menuItem.url);
                        } else {
                            navigateToLocation(menuItem.url);
                        }
                    } else if (menuItem.linkAction) {
                        // run attached function
                        //TODO: manage linkAction option
                    }
                };
            }
        };
    }]);
}());
