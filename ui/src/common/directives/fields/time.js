angular.module("CTC.Components").directive("ngModel", function () {
    function parseTime(val) {
        return angular.isUndefined(val) ? val : moment(val).format("HH:mm");
    }

    function formatTime(val) {
        return angular.isUndefined(val) ? val : (angular.isString(val) ? moment(val, "HH:mm") : moment(val)).set("ms", 0).toDate();
    }

    return {
        scope: false,
        restrict: "A",
        require: "ngModel",
        link: function (scope, element, attrs, ngModel) {
            if (angular.isDefined(ngModel) && attrs.type === "time") {
                ngModel.$parsers.push(parseTime);
                ngModel.$formatters.push(formatTime);
            }
        }
    };
});
