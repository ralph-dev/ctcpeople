angular.module("CTC.Components").filter("toDate", function () {
    return function (val) {
        return moment(val, ["x", "L", "YYYY-MM-DD"]).toDate();
    };
}).filter("fromDate", function () {
    return function (val, format) {
        return moment(val).format(format || "L");
    };
}).directive("ngModel", ["$filter", function ($filter) {
    return {
        scope: false,
        restrict: "A",
        require: "ngModel",
        link: function (scope, element, attrs, ngModel) {
            if (angular.isDefined(ngModel) && attrs.type === "date") {
                ngModel.$parsers.push($filter("fromDate"));
                ngModel.$formatters.push($filter("toDate"));
            }
        }
    };
}]);
