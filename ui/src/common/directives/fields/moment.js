angular.module("CTC.Components").directive("ngModel", function () {
    var types = ["date", "time", "datetime"];

    return {
        scope: false,
        restrict: "A",
        require: "ngModel",
        link: function (scope, element, attrs, ngModel) {
            if (angular.isDefined(ngModel) && types.indexOf(attrs.type) >= 0) {
                if (attrs.momentMin) {
                    ngModel.$validators.min = function (modelValue, viewValue) {
                        return moment(scope.$eval(attrs.momentMin, {
                            today: moment().subtract(1, 'd').set({h: 23, m: 59, s: 59, ms: 99}),
                            tomorrow: moment().set({h: 23, m: 59, s: 59, ms: 99})
                        })).isBefore(viewValue);
                    };
                }

                if (attrs.momentMax) {
                    ngModel.$validators.max = function (modelValue, viewValue) {
                        return moment(attrs.momentMax).add(1, 'd').set({h: 0, m: 0, s: 0, ms: 0}).isAfter(viewValue);
                    };
                }
            }
        }
    };
});
