(function () {
    "use strict";
    /**
     * filter to group array by given field and return an object with group as keys and items as value;
     * Note this filter is used by controller only .. use in view cause( $rootScope:infdig Error)
     * @collection [Array]
     * @group [String]
     * @return Object with the groups as property
     */
    angular.module("CTC.Components").filter("groupBy", function ($parse) {
        return function (input, group) {
            var result = {};
            var collection = input.slice();
            if (typeof collection === 'object') {
                if (!Array.isArray(collection)) {
                    return collection;
                }
            }
            if (group === undefined) {
                return collection;
            }
            var getter = $parse(group);
            for (var i = 0; i < collection.length; i++) {
                var item = collection[i];
                var comparator = getter(item);
                // check if group in array and add if new group
                if (!result[comparator]) {
                    result[comparator] = [];
                }
                result[comparator].push(item);
            }
            return result;
        };
    });
}());
