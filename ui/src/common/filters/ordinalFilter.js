/**
 * filter to add ordinal number extenstion to natural numbers
 */
(function () {
    "use strict";
    angular.module("CTC.Components").filter('ordinal', function () {
        "use strict";
        // Create the return function
        // set the required parameter name to **number**
        return function (number) {

            // Ensure that the passed in data is a number
            if (isNaN(number) || number < 1) {
                // If the data is not a number or is less than one (thus not having a cardinal value) return it unmodified.
                return number;
            }
            // If the data we are applying the filter to is a number, perform the actions to check it's ordinal suffix and apply it.

            var lastDigit = number % 10;

            if (lastDigit === 1) {
                return number + 'st';
            }
            if (lastDigit === 2) {
                return number + 'nd';
            }
            if (lastDigit === 3) {
                return number + 'rd';
            }
            if (lastDigit > 3) {
                return number + 'th';
            }

        };
    });

}());
