angular.module("CTC.Components", ["ngMessages", "ngAnimate", "ui.bootstrap", "ui.router", "ngSanitize", "ui-notification"]);

angular.module("CTC.Components").config(function ($provide, NotificationProvider) {

    moment.locale("en", {
        longDateFormat: {
            L: "DD/MM/YYYY"
        }
    });

    $provide.decorator("$location", function ($window, $delegate) {
        var params = $window.location.search.substr(1).split("&").map(function (query) {
            return query.split("=");
        }).reduce(function (params, val) {
            params[val.shift()] = val.join("=");
            return params;
        }, {});

        $delegate.query = function (name) {
            return params[name];
        };

        return $delegate;
    });


    NotificationProvider.setOptions({
        delay: 3500,
        positionY: 'top',
        positionX: 'center',
        templateUrl: 'common/notification.html'
    });
});
