/***
 urlPrefix service use to set the sales force url prefix after packaging
 service set the prefix at app run time
 **/
(function () {
    "use strict";
    angular.module("CTC.Components").service("urlPrefixService", ["RemoteModelObject","$q", function (RemoteModelObject,$q) {

        var HelperExtension = RemoteModelObject.create("Helper", {});
        var OrganisationDetailExtension = RemoteModelObject.create("OrganisationDetail",{});
        var prefixValue = "";
        var hasPerfix = true;
        function getPrefix() {
            return prefixValue;
        }

        /***
         * get the url prefix from helperExtension
         **/
        function setPrefix() {
            HelperExtension.actions.getNamespace()
                .then(function (data) {
                    prefixValue = data;
                });
        }

        function getDetail() {
            return OrganisationDetailExtension.actions.getDetail();
        }

        return {
            getPrefix: getPrefix,
            setPrefix: setPrefix,
            getDetail: getDetail
        };
    }]);
}());
