(function(){
    function helperService($q, RemoteModelObject){
        var helperExtension = RemoteModelObject.create("Helper",{});
        var USER_PERMISSION_DATA;
        
        var LIGHTNING_URL_ROOT = "one/one.app#";
        var LIGHTNING_URL_SOBJECT = "/sObject/";
        var LIGHTNING_URL_APEX = "/alohaRedirect/";
        

        /**
         * get system config permissions set by admin
         * @returns {Object}
         */
        function getUserPermission(){
            var deferred = $q.defer();
            var result;
            if(USER_PERMISSION_DATA){
                result = angular.copy(USER_PERMISSION_DATA);
                deferred.resolve(result);
            }else {
                helperExtension.actions.getUserPermission()
                    .then(function(data){
                        USER_PERMISSION_DATA = data;
                        result = angular.copy(USER_PERMISSION_DATA);
                        deferred.resolve(result);
                    });
            }

            return deferred.promise;
        }

        function isValidCloudStorageAccount() {
            return getUserPermission()
                .then(function(data){
                    return data.isValidCloudStorageAccount;
                });
        }

        function isSkillParsingEnabled(){
            return getUserPermission()
                .then(function(data){
                    return data.skillParsingEnabled;
                });
        }
        
        function hasForceOne(){
        	return typeof sforce !=="undefined" && typeof sforce.one !=="undefined" &&  typeof sforce.one.navigateToURL !=="undefined";
        }
        
        function navigateTo(url){
        	
        	
        	if(hasForceOne()){
        		if(url){
        			sforce.one.navigateToURL(url);
        		}
        		else{
        			sforce.one.navigateToURL("/"+LIGHTNING_URL_ROOT +'/home', true);
        			
        		}
        		
        	}else{
        		if(url){
        			location.href = url;
        		}else{
        			location.href = "/";
        		}
        		
        	}
        	
        }
        

        function navigateToSObject(objId){
        	
        	
        	if(hasForceOne()){
        		sforce.one.navigateToSObject(objId);
        	}else{
        		location.href ="/"+ url;
        	}
        	
        }
        
        function openWindow($win,url,target){
        	
        	var result = "";

        	if(target && target !== "" ){
        		
        		if(hasForceOne()){
        			
            		if(url.indexOf("/") === 0 && url.indexOf("/" + LIGHTNING_URL_ROOT) !== 0){
            			url  = buildLightningUrl("/", url);
            			
            		}  
            		
            		if(target.toLowerCase() ==="_self"){
            			result = sforce.one.navigateToURL(url);
            		}
        		}
        		
        		
        		result = $win.open(url,target);
        		
        	}else{
        		if(hasForceOne()){
        			
            		result = sforce.one.navigateToURL(url);
            	}else{
            		result = $win.open(url);
            	}
        		
        	}

        	return result;
        }
        
        function openWindowForSObject($win,objectId,target){
        	
        	var result = "";
        	
        	
        	if(hasForceOne() ){
        	    if(target && target!==""){
        	        var url = urlfor(objectId);
        	        result = $win.open(url,target);

        	    }else{
        	    	result = sforce.one.navigateToSObject(objectId);
        	    }
        		
        	}else{
        		if(target){
        			
        			$win.open( "/" + objectId,target);
        			
        		}else{
        			$win.open( "/" + objectId);
        			
        		}
        		
        	}
        	
        	return result;
        	
        }
        
        function locateTo($win,url){
        	
        	
        	if(hasForceOne()){
        		if(url==="" || url ==="/"){
        			url = "/"+ LIGHTNING_URL_ROOT +'/home';
        		}
        		sforce.one.navigateToURL(url);
        	}else{
        		$win.location.href = url;
        		
        	}
        	
        }
        function locateToSObject($win,objectId){
        	
        	if(hasForceOne()){
        		sforce.one.navigateToSObject(objectId);
        	}else{
        		$win.location.href = "/"+objectId;
        		
        	}
        	
        }
        
        function buildLightningUrl(domain, relativeUrl){
        	
        	var oneapp = "";
        	if(hasForceOne()){
        		oneapp = LIGHTNING_URL_ROOT + LIGHTNING_URL_APEX;
        	}else{
        		oneapp = "";
        	}
        	
        	
        	if(domain==="" ||  domain.lastIndexOf("/") !== domain.length - 1){
        		domain += "/";
        	}
        	
        	
    		if(relativeUrl.indexOf("/") === 0){
    			return domain  +oneapp + relativeUrl.substring(1);
    		}else{
    			return domain + oneapp +  relativeUrl;
    		}
        	
        
        }
        
        function urlfor(objectId,action){
            var url = "";
            objectId = objectId.replace("/","");
        	if( hasForceOne() ){
        	    url = "/" + LIGHTNING_URL_ROOT + LIGHTNING_URL_SOBJECT +objectId;
        	
        	}else{
        		url = "/" + objectId;
        	}
        	
        	if(typeof action !=="undefined" && action !== ""){
        	    url +="/" +action;
        	}
        	
        	return url;
        	
        }
        
        function naveToPage($win, objectId) {
        	if(typeof objectId === "undefined" || objectId === null )
        		return "";
        	var idurl = urlfor(objectId);
            var targetUrl = $win.location.origin;
            targetUrl +=  idurl;
            return targetUrl;
        }




        return {
            getUserPermission:getUserPermission,
            isSkillParsingEnabled:isSkillParsingEnabled,
            isValidCloudStorageAccount:isValidCloudStorageAccount,
            hasForceOne:hasForceOne,
            navigateTo:navigateTo,
            navigateToSObject:navigateToSObject,
            openWindow:openWindow,
            openWindowForSObject:openWindowForSObject,
            locateTo:locateTo,
            locateToSObject:locateToSObject,
            urlfor:urlfor,
            naveToPage:naveToPage,
            buildLightningUrl:buildLightningUrl
            
        }

    }
    angular.module("CTC.Components").service("helperService", ["$q", "RemoteModelObject",helperService]);
}());
