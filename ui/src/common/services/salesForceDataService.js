(function () {
    "use strict";
    angular.module("CTC.Components").service("salesForceDataService", function () {
        var SF_DATA = {
            serviceEndPoint: "http://localhost:8080",
            orgId: "00D90000000gtFz",
            srcId:"00390000022vM2m",
            urlprefix: "",
            bucketName: "temp003",
            lookupField: "account",
            nsPrefix: "",
            sessionId: "00D90000000gtFz!ARYAQG13q34kW_p2HZkj01rwCOF7c.uITVDgBi07ar6Z6v2ehMf7PP6pTUwPapBlmkIV_D.3FZefWPoqVtbbajlq7wni3Ff6",
            serverUrl: "https://c.ap1.visual.force.com/services/Soap/u/28.0/00D90000000gtFz",
            encodedUrl:"localhost",
            userId: "00590000003ekZ2AAI"
        };

        function setData(obj) {
            SF_DATA = obj;
        }

        function getData() {
            return angular.copy(SF_DATA);
        }

        return {
            setData: setData,
            getData: getData
        }
    });
}());
