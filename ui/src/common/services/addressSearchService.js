(function () {
    "use strict";
    /* Created Address Helper service to keep appraisal service as only a remote model object. */
    angular.module("CTC.Components").factory("RPDataHelperService", ["$q", "RemoteActionInterface", "$location", function ($q, RemoteAction, $location) {

        var RPSESSION = null;

        function getRPSession() {

            var dfd = $q.defer();

            if (RPSESSION === null) {
// TODO: update to RemoteModelObject
                RemoteAction
                    .find("getRPSession")()
                    .then(function (session) {
                        RPSESSION = session;
                        dfd.resolve(RPSESSION);
                    });

            } else {
                dfd.resolve(RPSESSION);
            }

            return dfd.promise;
        }

        return {
            getRPSession: getRPSession
        };

    }]);

    /* Set RPData property selected from step 1 search. */
    angular.module("CTC.Components").factory("RPDataPropertyService", function () {

        var property = {};

        function set(data) {
            property = data;
        }

        function get() {
            return property;
        }

        return {
            'get': get,
            'set': set
        };
    });

    angular.module("CTC.Components").factory("RPDataAddressLookupService", ["$q", "RemoteActionInterface", "BusyBlockingService", "$http", "RPDataHelperService", function ($q, RemoteAction, Busy, $http, RPDataHelperService) {

        var selectedProperty;

        function getPropertyData(remoteActionName) {

            var defer = $q.defer();

            RemoteAction
                .find(remoteActionName)()
                .then(function (data) {
                    defer.resolve(data);
                });

            return defer.promise;

        }

        function getPropertyTypes() {
            return getPropertyData('getPropertyTypes');
        }

        function getPropertyCategories() {
            return getPropertyData('getPropertyCategories');
        }

        function getPropertyAppraisalTypes() {
            return getPropertyData('getAppraisalTypes');
        }

        function rpDataJSONPURLBuilder(type, searchString) {

            var dfd = $q.defer();

            // type
            // address : for address search
            // property : get property details

            // Build JSONP URL

            RPDataHelperService.getRPSession().then(function (session) {

                var url = "https://burwood.clicktocloud.com/CTCPropertyService";

                if (type === "property") {
                    url += "/v2/properties";
                } else {
                    // Default for type address and suburb search.
                    url += "/properties/addresses";
                }

                url += "?searchString=" + encodeURIComponent(searchString);

                url += "&auth=" + session;

                if (type === "address") {

                    /*
                     Search categories:
                     1 - An exact address search
                     2 - A street search
                     3 - A suburb search
                     4 - A postcode search
                     */

                    url += "&searchCategories=1&searchCategories=2&searchCategories=3&searchCategories=4";
                }

                if (type === "suburb") {
                    url += "&searchCategories=3";
                }

                url += "&maxSuggestionResults=10";

                url += "&callback=JSON_CALLBACK";

                dfd.resolve(url);

            });

            return dfd.promise;

        }

        function rpDataJSONP(type, searchString) {

            var dfd = $q.defer();

            rpDataJSONPURLBuilder(type, searchString).then(function (url) {

                $http.jsonp(url)
                    .success(function (data, status, headers, config) {
                        if (data.length > 0) {
                            dfd.resolve(data);
                        } else {
                            dfd.reject('No results found');
                        }
                    })
                    .error(function (data, status) {
                        dfd.reject('Invalid format');
                    });

            });

            return dfd.promise;
        }

        function getAddress(searchString) {

            var defer = $q.defer();

            rpDataJSONP('address', searchString).then(function (data) {
                defer.resolve(data);
            });

            return defer.promise;

        }

        function getSuburb(searchString) {

            var defer = $q.defer();

            rpDataJSONP('suburb', searchString).then(function (data) {
                defer.resolve(data);
            });

            return defer.promise;

        }

        function getProperty(searchString) {

            var defer = $q.defer();

            rpDataJSONP('property', searchString).then(function (data) {
                defer.resolve(data);
            });

            return defer.promise;

        }

        return {
            getPropertyTypes: getPropertyTypes,
            getPropertyCategories: getPropertyCategories,
            getPropertyAppraisalTypes: getPropertyAppraisalTypes,
            getAddress: getAddress,
            getProperty: getProperty,
            getSuburb: getSuburb
        };
    }]);

    angular.module("CTC.Components").factory("StreetTypeLookupService", ['StreetTypes', function (StreetTypes) {

        function searchStreetType(value) {

            value = value.toLowerCase();

            return StreetTypes.filter(function (obj) {

                if (obj.type.toLowerCase() === value || obj.abbr.toLowerCase() === value) {
                    return obj;
                }

            }).map(function (currentObj, index) {
                return currentObj.type;
            });

        }

        return {
            searchStreetType: searchStreetType
        };

    }]);

    /**
     * fixed list of street types
     * */
    angular.module("CTC.Components").constant('StreetTypes', [
        {
            "type": "Access",
            "abbr": "accs"
        },
        {
            "type": "Alley",
            "abbr": "ally"
        },
        {
            "type": "Alleyway",
            "abbr": "alwy"
        },
        {
            "type": "Amble",
            "abbr": "ambl"
        },
        {
            "type": "Anchorage",
            "abbr": "ancg"
        },
        {
            "type": "Approach",
            "abbr": "app"
        },
        {
            "type": "Arcade",
            "abbr": "arc"
        },
        {
            "type": "Artery",
            "abbr": "art"
        },
        {
            "type": "Avenue",
            "abbr": "ave"
        },
        {
            "type": "Basin",
            "abbr": "basn"
        },
        {
            "type": "Beach",
            "abbr": "bch"
        },
        {
            "type": "Bend",
            "abbr": "bend"
        },
        {
            "type": "Block",
            "abbr": "blk"
        },
        {
            "type": "Boulevard",
            "abbr": "bvd"
        },
        {
            "type": "Brace",
            "abbr": "brce"
        },
        {
            "type": "Brae",
            "abbr": "brae"
        },
        {
            "type": "Break",
            "abbr": "brk"
        },
        {
            "type": "Bridge",
            "abbr": "bdge"
        },
        {
            "type": "Broadway",
            "abbr": "bdwy"
        },
        {
            "type": "Brow",
            "abbr": "brow"
        },
        {
            "type": "Bypass",
            "abbr": "bypa"
        },
        {
            "type": "Byway",
            "abbr": "bywy"
        },
        {
            "type": "Causeway",
            "abbr": "caus"
        },
        {
            "type": "Centre",
            "abbr": "ctr"
        },
        {
            "type": "Centreway",
            "abbr": "cnwy"
        },
        {
            "type": "Chase",
            "abbr": "ch"
        },
        {
            "type": "Circle",
            "abbr": "cir"
        },
        {
            "type": "Circlet",
            "abbr": "clt"
        },
        {
            "type": "Circuit",
            "abbr": "cct"
        },
        {
            "type": "Circus",
            "abbr": "crcs"
        },
        {
            "type": "Close",
            "abbr": "cl"
        },
        {
            "type": "Colonnade",
            "abbr": "clde"
        },
        {
            "type": "Common",
            "abbr": "cmmn"
        },
        {
            "type": "Concourse",
            "abbr": "con"
        },
        {
            "type": "Copse",
            "abbr": "cps"
        },
        {
            "type": "Corner",
            "abbr": "cnr"
        },
        {
            "type": "Corso",
            "abbr": "cso"
        },
        {
            "type": "Court",
            "abbr": "ct"
        },
        {
            "type": "Courtyard",
            "abbr": "ctyd"
        },
        {
            "type": "Cove",
            "abbr": "cove"
        },
        {
            "type": "Crescent",
            "abbr": "cres"
        },
        {
            "type": "Crest",
            "abbr": "crst"
        },
        {
            "type": "Cross",
            "abbr": "crss"
        },
        {
            "type": "Crossing",
            "abbr": "crsg"
        },
        {
            "type": "Crossroad",
            "abbr": "crd"
        },
        {
            "type": "Crossway",
            "abbr": "cowy"
        },
        {
            "type": "Cruiseway",
            "abbr": "cuwy"
        },
        {
            "type": "Cul-de-sac",
            "abbr": "cds"
        },
        {
            "type": "Cutting",
            "abbr": "cttg"
        },
        {
            "type": "Dale",
            "abbr": "dale"
        },
        {
            "type": "Dell",
            "abbr": "dell"
        },
        {
            "type": "Deviation",
            "abbr": "devn"
        },
        {
            "type": "Dip",
            "abbr": "dip"
        },
        {
            "type": "Distributor",
            "abbr": "dstr"
        },
        {
            "type": "Drive",
            "abbr": "dr"
        },
        {
            "type": "Driveway",
            "abbr": "drwy"
        },
        {
            "type": "Edge",
            "abbr": "edge"
        },
        {
            "type": "Elbow",
            "abbr": "elb"
        },
        {
            "type": "End",
            "abbr": "end"
        },
        {
            "type": "Entrance",
            "abbr": "ent"
        },
        {
            "type": "Esplanade",
            "abbr": "esp"
        },
        {
            "type": "Estate",
            "abbr": "est"
        },
        {
            "type": "Expressway",
            "abbr": "exp"
        },
        {
            "type": "Extension",
            "abbr": "extn"
        },
        {
            "type": "Fairway",
            "abbr": "fawy"
        },
        {
            "type": "Fire track",
            "abbr": "ftrk"
        },
        {
            "type": "Firetrail",
            "abbr": "fitr"
        },
        {
            "type": "Flat",
            "abbr": "flat"
        },
        {
            "type": "Follow",
            "abbr": "folw"
        },
        {
            "type": "Footway",
            "abbr": "ftwy"
        },
        {
            "type": "Foreshore",
            "abbr": "fshr"
        },
        {
            "type": "Formation",
            "abbr": "form"
        },
        {
            "type": "Freeway",
            "abbr": "fwy"
        },
        {
            "type": "Front",
            "abbr": "frnt"
        },
        {
            "type": "Frontage",
            "abbr": "frtg"
        },
        {
            "type": "Gap",
            "abbr": "gap"
        },
        {
            "type": "Garden",
            "abbr": "gdn"
        },
        {
            "type": "Gardens",
            "abbr": "gdns"
        },
        {
            "type": "Gate",
            "abbr": "gte"
        },
        {
            "type": "Gates",
            "abbr": "gtes"
        },
        {
            "type": "Glade",
            "abbr": "gld"
        },
        {
            "type": "Glen",
            "abbr": "glen"
        },
        {
            "type": "Grange",
            "abbr": "gra"
        },
        {
            "type": "Green",
            "abbr": "grn"
        },
        {
            "type": "Ground",
            "abbr": "grnd"
        },
        {
            "type": "Grove",
            "abbr": "gr"
        },
        {
            "type": "Gully",
            "abbr": "gly"
        },
        {
            "type": "Heights",
            "abbr": "hts"
        },
        {
            "type": "Highroad",
            "abbr": "hrd"
        },
        {
            "type": "Highway",
            "abbr": "hwy"
        },
        {
            "type": "Hill",
            "abbr": "hill"
        },
        {
            "type": "Interchange",
            "abbr": "intg"
        },
        {
            "type": "Intersection",
            "abbr": "intn"
        },
        {
            "type": "Junction",
            "abbr": "jnc"
        },
        {
            "type": "Key",
            "abbr": "key"
        },
        {
            "type": "Landing",
            "abbr": "ldg"
        },
        {
            "type": "Lane",
            "abbr": "lane"
        },
        {
            "type": "Laneway",
            "abbr": "lnwy"
        },
        {
            "type": "Lees",
            "abbr": "lees"
        },
        {
            "type": "Line",
            "abbr": "line"
        },
        {
            "type": "Link",
            "abbr": "link"
        },
        {
            "type": "Little",
            "abbr": "lt"
        },
        {
            "type": "Lookout",
            "abbr": "lkt"
        },
        {
            "type": "Loop",
            "abbr": "loop"
        },
        {
            "type": "Lower",
            "abbr": "lwr"
        },
        {
            "type": "Mall",
            "abbr": "mall"
        },
        {
            "type": "Meander",
            "abbr": "mndr"
        },
        {
            "type": "Mew",
            "abbr": "mew"
        },
        {
            "type": "Mews",
            "abbr": "mews"
        },
        {
            "type": "Motorway",
            "abbr": "mwy"
        },
        {
            "type": "Mount",
            "abbr": "mt"
        },
        {
            "type": "Nook",
            "abbr": "nook"
        },
        {
            "type": "Outlook",
            "abbr": "otlk"
        },
        {
            "type": "Parade",
            "abbr": "pde"
        },
        {
            "type": "Park",
            "abbr": "park"
        },
        {
            "type": "Parklands",
            "abbr": "pkld"
        },
        {
            "type": "Parkway",
            "abbr": "pkwy"
        },
        {
            "type": "Part",
            "abbr": "part"
        },
        {
            "type": "Pass",
            "abbr": "pass"
        },
        {
            "type": "Path",
            "abbr": "path"
        },
        {
            "type": "Pathway",
            "abbr": "phwy"
        },
        {
            "type": "Piazza",
            "abbr": "piaz"
        },
        {
            "type": "Place",
            "abbr": "pl"
        },
        {
            "type": "Plateau",
            "abbr": "plat"
        },
        {
            "type": "Plaza",
            "abbr": "plza"
        },
        {
            "type": "Pocket",
            "abbr": "pkt"
        },
        {
            "type": "Point",
            "abbr": "pnt"
        },
        {
            "type": "Port",
            "abbr": "port"
        },
        {
            "type": "Promenade",
            "abbr": "prom"
        },
        {
            "type": "Quad",
            "abbr": "quad"
        },
        {
            "type": "Quadrangle",
            "abbr": "qdgl"
        },
        {
            "type": "Quadrant",
            "abbr": "qdrt"
        },
        {
            "type": "Quay",
            "abbr": "qy"
        },
        {
            "type": "Quays",
            "abbr": "qys"
        },
        {
            "type": "Ramble",
            "abbr": "rmbl"
        },
        {
            "type": "Ramp",
            "abbr": "ramp"
        },
        {
            "type": "Range",
            "abbr": "rnge"
        },
        {
            "type": "Reach",
            "abbr": "rch"
        },
        {
            "type": "Reserve",
            "abbr": "res"
        },
        {
            "type": "Rest",
            "abbr": "rest"
        },
        {
            "type": "Retreat",
            "abbr": "rtt"
        },
        {
            "type": "Ride",
            "abbr": "ride"
        },
        {
            "type": "Ridge",
            "abbr": "rdge"
        },
        {
            "type": "Ridgeway",
            "abbr": "rgwy"
        },
        {
            "type": "Right of way",
            "abbr": "rowy"
        },
        {
            "type": "Ring",
            "abbr": "ring"
        },
        {
            "type": "Rise",
            "abbr": "rise"
        },
        {
            "type": "River",
            "abbr": "rvr"
        },
        {
            "type": "Riverway",
            "abbr": "rvwy"
        },
        {
            "type": "Riviera",
            "abbr": "rvra"
        },
        {
            "type": "Road",
            "abbr": "rd"
        },
        {
            "type": "Roads",
            "abbr": "rds"
        },
        {
            "type": "Roadside",
            "abbr": "rdsd"
        },
        {
            "type": "Roadway",
            "abbr": "rdwy"
        },
        {
            "type": "Ronde",
            "abbr": "rnde"
        },
        {
            "type": "Rosebowl",
            "abbr": "rsbl"
        },
        {
            "type": "Rotary",
            "abbr": "rty"
        },
        {
            "type": "Round",
            "abbr": "rnd"
        },
        {
            "type": "Route",
            "abbr": "rte"
        },
        {
            "type": "Row",
            "abbr": "row"
        },
        {
            "type": "Rue",
            "abbr": "rue"
        },
        {
            "type": "Run",
            "abbr": "run"
        },
        {
            "type": "Service way",
            "abbr": "swy"
        },
        {
            "type": "Siding",
            "abbr": "sdng"
        },
        {
            "type": "Slope",
            "abbr": "slpe"
        },
        {
            "type": "Sound",
            "abbr": "snd"
        },
        {
            "type": "Spur",
            "abbr": "spur"
        },
        {
            "type": "Square",
            "abbr": "sq"
        },
        {
            "type": "Stairs",
            "abbr": "strs"
        },
        {
            "type": "State highway",
            "abbr": "shwy"
        },
        {
            "type": "Steps",
            "abbr": "stps"
        },
        {
            "type": "Strand",
            "abbr": "stra"
        },
        {
            "type": "Street",
            "abbr": "st"
        },
        {
            "type": "Strip",
            "abbr": "strp"
        },
        {
            "type": "Subway",
            "abbr": "sbwy"
        },
        {
            "type": "Tarn",
            "abbr": "tarn"
        },
        {
            "type": "Terrace",
            "abbr": "tce"
        },
        {
            "type": "Thoroughfare",
            "abbr": "thor"
        },
        {
            "type": "Tollway",
            "abbr": "tlwy"
        },
        {
            "type": "Top",
            "abbr": "top"
        },
        {
            "type": "Tor",
            "abbr": "tor"
        },
        {
            "type": "Towers",
            "abbr": "twrs"
        },
        {
            "type": "Track",
            "abbr": "trk"
        },
        {
            "type": "Trail",
            "abbr": "trl"
        },
        {
            "type": "Trailer",
            "abbr": "trlr"
        },
        {
            "type": "Triangle",
            "abbr": "tri"
        },
        {
            "type": "Trunkway",
            "abbr": "tkwy"
        },
        {
            "type": "Turn",
            "abbr": "turn"
        },
        {
            "type": "Underpass",
            "abbr": "upas"
        },
        {
            "type": "Upper",
            "abbr": "upr"
        },
        {
            "type": "Vale",
            "abbr": "vale"
        },
        {
            "type": "Viaduct",
            "abbr": "vdct"
        },
        {
            "type": "View",
            "abbr": "view"
        },
        {
            "type": "Villas",
            "abbr": "vlls"
        },
        {
            "type": "Vista",
            "abbr": "vsta"
        },
        {
            "type": "Wade",
            "abbr": "wade"
        },
        {
            "type": "Walk",
            "abbr": "walk"
        },
        {
            "type": "Walkway",
            "abbr": "wkwy"
        },
        {
            "type": "Way",
            "abbr": "way"
        },
        {
            "type": "Wharf",
            "abbr": "whrf"
        },
        {
            "type": "Wynd",
            "abbr": "wynd"
        },
        {
            "type": "Yard",
            "abbr": "yard"
        }
    ]);

}());
