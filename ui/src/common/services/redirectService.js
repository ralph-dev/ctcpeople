/***
 urlRedirect service use to redirect to new url page or replace current page with a new url
 **/

angular.module("CTC.Components")
    .service("redirectService", ["RemoteModelObject", "$window", "helperService", function (RemoteModelObject, $window,helperService) {

        var HelperExtension = RemoteModelObject.create("Helper", {});
        var prefixValue = "";
        var isLightningEnabled = helperService.hasForceOne();;
        //var lightningMatch = "lightning.force.com";
        
//        if ($window.location.href.indexOf(lightningMatch) > -1) {
//            isLightningEnabled = true;
//        }

        this.getPrefix = function () {
            return prefixValue;
        };

        /***
         * get the url prefix from helperExtension
         **/
        this.loadPrefix = function () {
            HelperExtension.actions.getNamespace()
                .then(function (data) {
                    prefixValue = data;
                });
        };
        // this.loadPrefix();

        this.urlfor = function(objectId,action){
        	return helperService.urlfor(objectId,action);
        };
        
        this.naveToPage = function($win, objectId) {
        	return helperService.naveToPage($win, objectId);
        }

        /***
         * this function use sforce.one function to redirect to new page
         **/
        this.navigateToUrl = function (urlData) {

            var baseUrl = $window.location.origin + "/apex/";
            //console.log("sforce.one.navigateToUrl: "+baseUrl+urlData);
            //sforce.one.navigateToURL(baseUrl + urlData)
            helperService.navigateTo(baseUrl + urlData);

        };

        /***
         * this function use sforce.one to open salesforce object
         **/
        this.navigateToSObject = function (objectId) {
            if (isLightningEnabled) {
                console.log("sforce.one.navigateToSObject: " + objectId);
                sforce.one.navigateToSObject(objectId);
            } else {
                var targetUrl = $window.location.origin;
                targetUrl += "/" + objectId;
                $window.open(targetUrl);
            } 

        };


        /***
         * this function set the current document location to the new url
         **/
        this.gotoAppUrl = function (appName, urlData) {

            var namespace = this.getPrefix();
            var targetUrl = namespace + appName +"?" + urlData;
            this.navigateToUrl(targetUrl);
            // var namespace = this.getPrefix();
            // var queryString = $window.document.location.search;
            // var newUrl = $window.location.origin + "/apex/" + namespace + appName + queryString +"&" + urlData;
            // $window.document.location.href = newUrl;

        };

        /***
         * this function open a new page replace current page or new page with a salesforce object id
         **/
        this.openSalesForceObject = function (objectId, type) {
            if (!type || (type && type.toLowerCase() === "_blank")) {
                type = "_blank";
                
                helperService.openWindowForSObject($window, objectId,type);
                
//                if(helperService.hasForceOne()){
//                	helperService.navigateToSObject(objectId);
//                }else{
//                	$window.open("/"+objectId, type);
//                }
                
               // return;
            }else{
            	this.navigateToSObject(objectId);
            }

            
        };


        /***
         * this function open a new page with a new url
         **/
        this.open = function (url, type) {
        	
        	

            var result  = "";
            
            result = helperService.openWindow($window, url,type);
//            if (type) {
//                result = $window.open(url, type);
//                //$window.open(url, type);
//                return result;
//            }
//
//            result = $window.open(url);

            return result;
        };

    }]);
