/** @class RemoteActionInterface */
angular.module("CTC.Components").service("RemoteActionInterface", function ($window, ActionInterface, MockActionInterface) {
    var Manager = ($window["$VFRM"] || {}).Manager,
        Provider = ($window["$VFRM"] || {}).last || {actions: {}},
        controllerNames = Object.keys(Provider.actions),
        actionInterfaces = {};
        
    var namespace="unknown";

    /**
     * @function RemoteActionInterface#find
     * @description Retrieve the first promisifed VisualForce RemoteAction function on any controller matching the given name
     * @param {String} action - Name of the remote action to find
     * @returns {Function} The wrapped VisualForce RemoteAction function
     */
    this.find = function (action) {
        return (this.get(controllerNames.filter(function (name) {
            return !!actionInterfaces[name][action];
        }).pop()) || new MockActionInterface(action))[action];
    };
    
    

    /**
     * @function RemoteActionInterface#get
     * @description Retrieve a promisified VisualForce RemoteAction controller by name
     * @param {String} name - Name of the Apex class which exposes the remote actions to search for
     * @returns {ActionInterface} The wrapped VisualForce RemoteAction controller object with functions that return promises
     */
    this.get = function (name) {
        // get with namespace first
        // TODO change from static value for namespace
    	if(namespace==="unknown" ){
    		namespace = getNamespace( );
    		console.log("namespace : " + namespace);
    	}
    	
    	var fullname = name;
    	
    	if(namespace !== "" && namespace!=="unknown"){
    		//name = "PeopleCloud1." + name;
    		fullname = namespace + "." + name;
    	}
    	
        
        var nameSpaceActionInteface = getActionInterface(fullname);

        if (nameSpaceActionInteface) {
            return nameSpaceActionInteface;
        } else {
        	
        	nameSpaceActionInteface = getActionInterface(name);
        	if(nameSpaceActionInteface){
        		namespace ="";
        		return nameSpaceActionInteface;
        	}else{
        		namespace = "PeopleCloudS";
            	fullname = namespace + "." + name;
            	return  getActionInterface(fullname);
        	}
 
        }

    };

    function getActionInterface (name) {
        if (angular.isDefined(actionInterfaces[name])) {
            return actionInterfaces[name];
        } else if (controllerNames.indexOf(name) >= 0) {
            return (actionInterfaces[name] = new ActionInterface(Manager.getController(name), Provider.actions[name].ms));
        } else {
            return false;
        }
    }
    
    function getNamespace( ){
    	
    	if(typeof(__sfdcSessionId) != "undefined"){
    		
    		var result = sforce.connection.query("Select NamespacePrefix From ApexClass Where Name = 'HelperExtension'");
            var records = result.getArray("records");
          
            if(records[0].NamespacePrefix !== ""){
                return records[0].NamespacePrefix;
            }
    		
           
    	}else{
    		return "PeopleCloud1";
    	}

        return "";
        
    }

    // Load wrapped interfaces for available controllers
    controllerNames.forEach(this.get);
});

/*** Action interface ***/
angular.module("CTC.Components").factory("ActionInterface", function ($q) {
    /**
     * Creates function that returns a promise which is resolved or rejected with the results from the RemoteAction call
     * @param {Function} actionfn - The VisualForce RemoteAction function to call
     * @param {Object} ms - RemoteAction configuration object with action's namespace and expected arguments length
     * @returns {Function} The wrapped action function
     * @private
     */
    function promisifyActionFn(actionfn, ms) {
        return function() {
            var deferred = $q.defer();

            if ((arguments.length !== ms.len) || (!actionfn)) {
                deferred.reject(new Error(["RemoteAction", ms.name, "expecting", ms.len, "argument"+(ms.len===1 ? "" : "s"), "but got", arguments.length].join(" ")));
            } else {
                actionfn.apply(this, Array.prototype.slice.call(arguments).concat([function (val, event) {
                    if (event.status) {
                        try {
                            val = normalise(ms.ns, angular.fromJson(val));
                        } catch (ex) {
                            // Nothing to do here
                        }

                        deferred.resolve(val);
                    } else {
                        deferred.reject(new Error(event.message));
                    }
                }, {escape: false}]));
            }

            return deferred.promise;
        };
    }

    /**
     * Remove managed package namespaces from object keys of RemoteAction return value
     * @param {String} ns - The RemoteAction's namespace to be removed from object keys
     * @param {*} val - Object to remove namespace from
     * @returns {(Object|Array|*)} Object or array of objects with namespaces stripped from keys, or original value
     * @private
     */
    function normalise(ns, val) {
        if (!ns || !angular.isObject(val)) {
            return val;
        } else {
            return angular.forEach(val, function (val, key, obj) {
                obj[(angular.isArray(obj) ? key : key.toString().replace([ns, "__"].join(""), ""))] = normalise(ns, val);
            });
        }
    }

    /**
     * @class ActionInterface
     * @description Wraps a given VisualForce RemoteAction controller's action functions with promisified functions
     * @property {...Function} * - Wrapped functions copied from original controller object
     * @property {String} *.ns - Namespace for the original remote action function
     * @param {Object} controller - Controller object containing raw action functions to be wrapped
     * @param {Array<Object>} actions - Array of remote action definition objects for the controller
     * @param {String} actions.ns - Namespace of the original remote action function
     */
    function ActionInterface(controller, actions) {
        for (var i = 0, ilen = actions.length; i < ilen; ++i) {
            this[actions[i].name] = promisifyActionFn(controller[actions[i].name], actions[i]);
            this[actions[i].name].ns = actions[i].ns;
        }
    }

    return ActionInterface;
});

angular.module("CTC.Components").factory("MockActionInterface", function ($http, $log, ActionInterface) {
    /**
     * @class MockActionInterface
     * @description Constructs a mocked RemoteAction controller for
     * @param {String} name - The name of the RemoteAction controller class to be mocked
     * @private
     */
    function MockActionInterface(name) {
        var controller = {};

        controller[name] = function httpAction(path, onSuccess) {
            $log.warn(["RemoteAction '", name, "' not found, fell back to $http service instead :("].join(""));
            return $http.get(path).success(onSuccess);
        };

        ActionInterface.call(this, controller, {ms: [{len: 1, name: name}]});
    }

    return MockActionInterface;
});
