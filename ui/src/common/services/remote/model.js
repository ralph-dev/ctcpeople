(function () {
    angular.module("CTC.Components").factory("RemoteModel", function ($log, $q, ModelObject, RemoteActionInterface) {
        /**
         * @class RemoteModel
         * @classdesc
         * Loads data from a given remote action and creates a bindable data model.
         * Facilitates easy bidirectional data binding across multiple directives with isolated scopes.
         * @deprecated In favour of {@link ModelObject} and {@link RemoteModelObject}
         * @see {@link ModelObject} for creating locally synced data models from arbitrary sources.
         * @see {@link RemoteModelObject} for creating ModelObject data models bound to remote actions for CRUD methods.
         */
        return {
            /**
             * @function RemoteModel.create
             * @description
             * Constructs an instance of RemoteModel with data loaded from the specified remote action.
             * If the specified action name is null, the model source will be the supplied argument.
             * @param {(String|null)} name - Name of the remote action function to use as model source
             * @param {*} args - Arguments to be supplied to the remote action
             * @returns {ModelObject} Local model object loaded from data retrieved in remote action callout
             */
            create: function (name, args) {
                $log.warn(new Error("RemoteModel is deprecated, please use RemoteModelObject instead"));

                return new ModelObject($q.when(args).then(function (args) {
                    // Create a new model object in the same way the old RemoteModel was created
                    return (angular.isString(name) ? RemoteActionInterface.find(["get", name].join("")) : angular.identity).apply(this, angular.isDefined(args) ? [args] : []);
                }));
            }
        };
    });
}());
