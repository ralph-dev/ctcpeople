angular.module("CTC.Components").factory("ModelObject",
    function ($q, $exceptionHandler) {
        /**
         * @class ModelObject
         * @classdesc Local model storage factory.
         * @description This class should be used in angular factories.
         * It facilitates easy bidirectional data binding across multiple directives with isolated scopes.
         * @property {Array} dests - Collection of extension targets to be updated when source model changes
         * @property {Promise} promise - Promise which, when resolved, will become the model's data source
         * @param {(Array|Object|Promise)} src - The data to be used as the model source
         */
        function ModelObject(src) {
            this.dests = [];
            this.promise = $q.when(src).then(angular.identity, function (reason) {
                $exceptionHandler(new Error(reason));
                return {};
            });
        }

        /**
         * @function ModelObject~get
         * @description Retrieve data from the model after it has loaded
         * @param {...String} [arguments] - Names of model properties to retrieve
         * @returns {Promise<Array>|Promise.Error}
         * An array of property values from the model for each provided string.
         * Nonexistent property values will resolve to undefined.
         */
        ModelObject.prototype.get = function () {
            return this.promise.then(angular.bind(this, get, Array.prototype.slice.call(arguments)));
        };

        /**
         * @function ModelObject~set
         * @description Set values on the model after it has loaded
         * @param {Object} mappings - Object from which values will be copied to the model
         * @returns {Promise} Promise which is resolved after the model values have been set
         */
        ModelObject.prototype.set = function (mappings) {
            return this.promise.then(angular.bind(this, set, mappings));
        };

        /**
         * @function ModelObject~insert
         * @description
         * Insert a new record into a collection of records, provided the model is a collection.
         * Also, update any array destinations which the model has extended to.
         * @param {(Object|Array)} records - Array or object to be inserted into the collection of records
         * @returns {Promise} Promise which is resolved when the records have been inserted
         */
        ModelObject.prototype.insert = function (records) {
            return this.promise.then(angular.bind(this, insert, this.dests, records));
        };

        /**
         * @function ModelObject~remove
         * @description
         * Remove a record with the given id from a collection of records, provided the model is a collection.
         * Also, remove the record from any array destinations which the model has extended to.
         * @param {String} id - Id of the record to remove from the collection
         * @returns {Promise} Promise which is resolved when the record has been removed
         */
        ModelObject.prototype.remove = function (id) {
            return this.promise.then(angular.bind(this, remove, id, this.dests));
        };

        /**
         * @function ModelObject~extend
         * @description
         * Bind the model values to the given destination as getter/setter functions which retrieve the specified keys.
         * Getter/setters will be bound to the destination object with the key names taken from the provided mappings object.
         * If no mappings are provided, all keys from the model source will be bound to the destination.
         * Additionally, all extensions will automatically include a bound getter/setter for the record's 'Id' key value.
         * @param {(Object|Array)} dest - Destination object or collection to extend to
         * @param {Object<String>} [mappings] - Keys to be mapped to destination as functions resolving to model values
         * @returns {Promise} Promise which is resolved with the original destination input object when binding is complete
         */
        ModelObject.prototype.extend = function (dest, mappings) {
            return this.promise.then(angular.bind(this, extend, (this.dests.filter(function (target) {
                return target.dest === dest;
            }).pop() || this.dests[this.dests.push({
                dest: dest,
                mappings: mappings
            }) - 1]), mappings));
        };

        function get(names, model) {
            if (!!names && (angular.isString(names) || names.length > 1)) {
                return [].concat(names).map(function (name) {
                    var names = name.split("."),
                        target = names.pop();

                    while (names.length > 0) {
                        model = model[names.shift()];
                    }

                    return model[target];
                });
            } else {
                return (names.length === 1 ? get(names.pop(), model).pop() : model);
            }
        }

        function set(mappings, model) {
            return $q.all([].concat(mappings)).then(function (mappings) {
                return $q.all(angular.isArray(model) ? mappings.map(function (mappings, index) {
                    return set(mappings, model[index]);
                }) : Object.keys(mappings = mappings.shift()).map(function (name) {
                    return $q.when(mappings[name]).then(function (val) {
                        var names = name.split("."),
                            target = names.pop();

                        return get(names.join("."), model)[target] = val;
                    });
                }));
            }).then(function () {
                return model;
            });
        }

        function insert(dests, records, model) {
            if (!(angular.isArray(model))) {
                return set(records, model);
            } else if (angular.isDefined(records)) {
                return [].concat(records).map(function (record) {
                    dests.forEach(function (target) {
                        target.dest.push(map(record, {}, target.mappings || Object.keys(record)));
                    });

                    return model.push(record);
                });
            }
        }

        function remove(ids, dests, model) {
            if (angular.isArray(model)) {
                return [].concat(ids).map(function (id) {
                    return (angular.isString(id) ? id : id.Id);
                }).map(function (id) {
                    return dests.map(function (target) {
                        var record = target.dest.filter(function (record) {
                                return record.id() === id;
                            }).pop(),
                            index = target.dest.indexOf(record),
                            recordId = (index >= 0 ? target.dest.splice(index, 1).pop().id() : null);

                        return model.indexOf(model.filter(function (record) {
                            return record[target.mappings.id] === recordId;
                        }).pop());
                    }).filter(function (index) {
                        return index >= 0;
                    }).map(function (index) {
                        return model.splice(index, 1).pop().Id;
                    });
                });
            }
        }

        function extend(target, mappings, model) {
            target.mappings = (angular.isDefined(mappings) ? angular.extend({id: "Id"}, mappings) : mappings);

            if (angular.isArray(model) && angular.isArray(target.dest)) {
                model.forEach(function (entry) {
                    target.dest.push(map(entry, {}, target.mappings || Object.keys(entry)));
                });
            } else if (!angular.isArray(model)) {
                map(model, target.dest, target.mappings || Object.keys(model));
            } else {
                throw new TypeError("RemoteObject expecting array destination for extension of array source");
            }

            return target.dest;
        }

        function map(model, dest, mappings) {
            (angular.isArray(mappings) ? mappings : Object.keys(mappings)).forEach(function (name) {
                dest[name] = bind(model, (angular.isArray(mappings) ? name : mappings[name]) || name);
            });

            return dest;
        }

        function bind(model, mapping) {
            var mappings = mapping.split("."),
                target = mappings.pop();

            while (mappings.length > 0) {
                mapping = mappings.shift();
                model = model[mapping] = (model[mapping] || {});
            }

            return function (val) {
                return (angular.isDefined(val) ? (model[target] = val) : model[target]);
            };
        }

        return ModelObject;
    }
);
