/******
 * RemoteModelObject
 * **********/
(function () {
    angular.module("CTC.Components").factory("RemoteModelObject",
        function ($q, ModelObject, RemoteActionInterface) {
            /**
             * @class RemoteModelObject
             * @classdesc Local model storage integrated with VisualForce CRUD remote actions provided by an apex class.
             * See {@link ModelObject} for more information.
             * @description This class should be used in angular factories.
             * Like the ModelStorage class, it facilitates easy bidirectional data binding across multiple directives with isolated scopes.
             * Additionally, data model manipulation functions will perform CRUD remote actions before acting on local data.
             * @property {ActionInterface} actions - Collection of remote actions to be used for synchronising with SalesForce
             * @property {ModelObject} model - Local model object loaded from remote action.
             * @param {String} name - The name of the Apex class to use as the ModelObject source
             * @param {*} args - Arguments to be provided to the remote action when loading model from remote action result
             */
            function RemoteModelObject(name, args) {
                this.actions = RemoteActionInterface.get(name);
                this.model = new ModelObject((angular.isArray(args) ? $q.all(args) : $q.when(args)).then(angular.bind(this, function (args) {
                    var lazy = (angular.isArray(args) ? args.length === 0 : angular.isObject(args)),
                        query = (angular.isArray(args) ? args : [args]).filter(angular.isDefined);

                    return lazy ? args : resolve(this.actions, "get").apply(this, (query.length > 0 ? [query] : query)).then(function (result) {
                        return ((angular.isArray(result) && !(angular.isArray(args) || angular.isUndefined(args))) ? result.pop() : result);
                    });
                })));
            }

            /**
             * @function RemoteModelObject~get
             * @description Retrieve data from the model after it has loaded.
             * @param {...String} [arguments] - Names of model properties to retrieve
             * @returns {Promise<Array>|Promise.Error}
             * An array of property values from the model for each provided string.
             * Nonexistent property values will resolve to undefined.
             */
            RemoteModelObject.prototype.get = function () {
                return this.model.get.apply(this.model, Array.prototype.slice.call(arguments));
            };

            /**
             * @function RemoteModelObject~set
             * @description Set values on the model after it has loaded.
             * @param {Object} mappings - Object from which values will be copied to the model
             * @returns {Promise} Promise which is resolved after the model values have been set.
             */
            RemoteModelObject.prototype.set = function (mappings) {
                return this.model.set(mappings);
            };

            /**
             * @function RemoteModelObject~create
             * @description
             * Perform remote action callout to create the new record, then insert the result into the model.
             * Also, update any array destinations which the model has extended to.
             * @param {(Object|Array)} records - Array or object to be inserted into the collection of records
             * @returns {Promise} Promise which is resolved when the records have been inserted.
             */
            RemoteModelObject.prototype.create = function (records) {
                return $q.all([].concat(records))
                    .then(resolve(this.actions, "create"))
                    .then(angular.bind(this.model, this.model.insert));
            };

            /**
             * @function RemoteModelObject~retrieve
             * @description Retrieve a record, or list of records via the apex class remote action, and copy it to the model.
             * @param {(Array<String>|String)} args - Argument to be passed to the remote action
             * @returns {Promise} Promise which is resolved when the records have been retrieved and applied to the model.
             */
            RemoteModelObject.prototype.retrieve = function (args) {
                return $q.all([].concat(args))
                    .then(resolve(this.actions, "get"))
                    .then(angular.bind(this.model, this.model.set));
            };

            /**
             * @function RemoteModelObject~update
             * @description Perform a remote action callout using the local model to save the data.
             * @returns {Promise} Promise which is resolved when the data has been sent and a result has been received.
             */
            RemoteModelObject.prototype.update = function (records) {
                return (angular.isUndefined(records) ? this.model.get().then(angular.bind($q, $q.all)) : $q.all(records))
                    .then(resolve(this.actions, "update"))
                    .then(angular.bind(this.model, this.model.set));
            };

            /**
             * @function RemoteModelObject~delete
             * @description
             * Perform remote action callout to delete records from SalesForce with the given id.
             * Also, remove the record from any array destinations which the model has extended to.
             * @param {String} ids - Id of the record to remove from the collection
             * @returns {Promise} Promise which is resolved when the record has been removed.
             */
            RemoteModelObject.prototype.delete = function (ids) {
                return $q.all([].concat(ids))
                    .then(function (ids) {
                        return ids.filter(angular.identity).map(function (id) {
                            return (angular.isString(id) ? {Id: id} : id);
                        });
                    })
                    .then(resolve(this.actions, "delete"))
                    .then(angular.bind(this.model, this.model.remove, ids));
            };

            /**
             * @function RemoteModelObject~extend
             * @description
             * Bind the model values to the given destination as getter/setter functions which retrieve the specified keys.
             * Getter/setters will be bound to the destination object with the key names taken from the provided mappings object.
             * If no mappings are provided, all keys from the model source will be bound to the destination.
             * Additionally, all extensions will automatically include a bound getter/setter for the record's 'Id' key value.
             * @param {(Object|Array)} dest - Destination object or collection to extend to
             * @param {Object<String>} [mappings] - Keys to be mapped to destination as functions resolving to model values
             * @returns {Promise} Promise which is resolved with the original destination input object when binding is complete.
             */
            RemoteModelObject.prototype.extend = function (dest, mappings) {
                return this.model.extend(dest, mappings);
            };

            /**
             * Resolve a remote action with a given action name from a set of actions, or return identity function.
             * @param {ActionInterface} actions - Collection of actions to source the specified action from
             * @param {String} name - Name of the action to be found
             * @returns {Function} Action function with specified name from controller action interface, or identity function.
             * @private
             */
            function resolve(actions, name) {
                return (angular.isFunction(actions[(name = [name, "Records"].join(""))]) ? actions[name] : angular.identity);
            }

            return {
                /**
                 * @function RemoteModelObject.create
                 * @description
                 * Constructor function for instantiating RemoteModelObject class.
                 * This function will automatically append 'Extension' to the provided class name.
                 * @param {String} name - The name of the Apex class to use as the ModelObject source
                 * @param {*} [args] - Arguments to be provided to the remote action when loading model from remote action result
                 */
                create: function (name, args) {
                    if (angular.isString(name)) {
                        return new RemoteModelObject([name, "Extension"].join(""), args);
                    } else {
                        throw new Error("RemoteModelObject: apex class name is required");
                    }
                }
            };
        }
    );
}());
