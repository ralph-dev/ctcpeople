// Karma configuration
// Generated on Wed Jun 01 2016 17:37:40 GMT-0400 (EDT)

module.exports = function(config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],


        // list of files / patterns to load in the browser
        // app files point to build directory and spec files point to test directory
        files: [
            './src/lib/jquery/dist/jquery.js',
            './src/lib/jquery-ui/jquery-ui.js',
            './src/lib/bootstrap/dist/js/bootstrap.js',
            './src/lib/angular/angular.js',
            './src/lib/angular-mocks/angular-mocks.js',
            './src/lib/angular-ui-router/release/angular-ui-router.js',
            './src/lib/angular-animate/angular-animate.js',
            './src/lib/angular-bootstrap/ui-bootstrap-tpls.js',
            './src/lib/angular-messages/angular-messages.js',
            './src/lib/angular-mocks/angular-mocks.js',
            './src/lib/angular-sanitize/angular-sanitize.js',
            './src/lib/tinymce/tinymce.min.js',
            './src/lib/tinymce/themes/modern/theme.min.js',
            './src/lib/tinymce/plugins/paste/plugin.min.js',
            './src/lib/moment/moment.js',
            './src/lib/ng-tags-input/ng-tags-input.min.js',
            './src/lib/karma-read-json/karma-read-json.js',
            './src/lib/pdfjs-dist/build/pdf.js',
            './src/lib/pdfjs-dist/build/pdf.worker.js',
            './src/lib/angular-ui-notification/dist/angular-ui-notification.js',
            './src/lib/fullcalendar/dist/fullcalendar.js',
            './src/lib/angular-ui-tinymce/src/tinymce.js',
            './src/lib/angular-ui-calendar/src/calendar.js',
            './src/static/VisualForce/VFRemote.min.js',
            './src/static/VisualForce/RemoteActions.js',
            './build/**/*.js',
            './test/**/*.spec.js'
        ],


        // list of files to exclude
        exclude: [
        ],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
        },


        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['spec'],


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true,

        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity
    })
}
