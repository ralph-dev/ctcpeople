# CTC Base Framework

### Single Page Application framework

This framework exists to enable rapid development of VisualForce pages which leverage the [AngularJS](https://angularjs.org/)
client-side web application framework, without depending on transmitting application code back to the SalesForce platform
before being able to preview any changes made during development. 
Utilising [gulp](https://github.com/gulpjs/gulp), the streaming build system, and various third party plugins,
this framework intends to ensure consistency of front end code styling and structure.


## Installation

1. Download node at [nodejs.org](http://nodejs.org) and install it, if you haven't already.
2. Install gulp
    
        # On Windows, with administrator rights
        npm install -g gulp
        
        # On linux/mac
        sudo npm install -g gulp 
    
3. Check out this repository and install supporting node packages
    
        git clone https://bitbucket.org/clicktocloud/ctcbaseframework.git CTCBaseFramework
        cd ./CTCWizardFramework
        npm install
        
4. Make sure you configure the Git repository with your name and email so your commits can be identified
    
        git config user.name "John Smith"
        git config user.email john@clicktocloud.com



## Framework Usage

The gulp file is configured with three primary tasks: build, package and develop.
Each of these tasks are depend on a number of secondary tasks to achieve their outcomes 
(i.e. cleaning project artifacts, compiling stylesheets, linting sources).

### Developing
To begin developing, call the develop task from the command line (using ```gulp develop```).

This task will compile the single page application wizard forms, as well as all related components, launch a web server
(accessible from <http://localhost:9001/>), and watch for any changes to the source files which require applications or
components to be recompiled. Compiled content is served from the build task's output directory.
Supporting content (i.e. third-party libraries and static resources) is served from the source root directory.

Additionally, the task includes a live reload plugin which is configured to automatically reload any wizard pages
which are open in browser windows.
This reload will only be triggered when the build task has successfully compiled any new changes which may have been
made since the last successful build.
 
### Building
The build task is used to produce standalone single page application wizard form HTML pages and related components.
The gulpfile is configured such that a failure of any task along this process will cause the build task to discard the file,
and subsequent tasks in the chain will not be called (e.g. if JSHint fails to lint a file, the devutils task will not be called).

To begin, the LESS task will compile any component or application LESS files into CSS files of the same name alongside the LESS files.
Next, all component and application JavaScript files will be run through JSHint to ensure consistent code styling and structure.

Finally, application pages and related components will be compiled and written out into the build directory.
Application pages are constructed by embedding application body files inside the skeleton HTML document (currently main.html)
and writing the newly standalone page to the build task's output directory. Related components are then constructed into
standalone modules by merging all scripts and styles in the module component script and style files  into singular 
concatenated files, excluding descendant modules. The same process is repeated for any descendant submodules 
HTML files contained in the module or its descendant modules referenced by an angular directive's templateUrl property 
are automatically assumed to be module templates/partials, and are copied into their directive's expected destination.

### Packaging
This task is used to translate standalone HTML application pages into VisualForce XML pages to be uploaded into a SalesForce environment.
Additionally, any supporting content, such as compiled components, third party libraries or static resources which are referenced
by a standalone page will be bundled into a zip file for upload to SalesForce as a static resource. References to this content
in an application page will be replaced with an equivalent VisualForce resource reference to the content in the zip.
The zip file must then be uploaded with the same base name into a SalesForce environment as a static resource pack.

### Editor Config
When developing on this project you must use the [editorconfig](http://editorconfig.org/) in the project to keep the code consistant and clean.

[SublimeText Setup](https://github.com/sindresorhus/editorconfig-sublime#readme) Install [Package Control](https://packagecontrol.io/) first to install editorconfig for sublime text.



### Add app to apps Directory
For each App create a directory with the app name
The app directory most contains the app.html and app.js files

app.html
most contains the following structure

```HTML
<!DOCTYPE html>
<html>

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">

        <base href="/">


        <!-- bower:css -->
        <!-- endbower -->

        <link href="static/ctcPropertyGlyphicon/style.css" type="text/css" rel="stylesheet">
        <script src="static/phoneNumberFormater.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTA-F0hq7J01nkYE1mgu7j2ynIxJ9sb0U"></script>

        <!-- inject:css -->
        <!-- endinject -->

        <!-- bower:js -->
        <!-- endbower -->


        <!-- inject:js -->
        <!-- endinject -->

        <!-- build:remove:prod -->
        <script src="static/VisualForce/VFRemote.min.js"></script>
        <script src="static/VisualForce/RemoteActions.js"></script>
        <script src="//localhost:35729/livereload.js?snipver=1"></script>
        <!-- /build -->
    </head>

    <body data-ng-app="APP_NAME" class="ctc-application">
        <div id="content" class="container-fluid">
             <!-- app main section -->
            <div class="row">
                <div data-ctc-status="true"></div>
                <div data-ctc-header="CARD_TITLE"></div> <!- optional structure -->
                <div class="ctc-breadcrumbs-navbar"></div> <!- optional structure -->
                <ctc-side-nav menu-items="REF_TO_NAV_LIST"></ctc-side-nav> <!- optional structure -->
                <div class="activeArea">
                    <ui-view></ui-view>
                </div>
            </div>  <!-- end of App  main section -->
        </div>
    </body>
</html>

```

app.js : structure example
``` javascript

angular.module("CTC.APP-NAME", ["CTC.Components"]);

/**** app configuration ****/

// OPTIONAL navigation option
angular.module("CTC.APP-NAME")
.config(function($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/DEFAULT-STATE');

    $stateProvider
    .state('STATE-NAME',{
        url:'/STATE-URL',
        template:'STATE-TEMPLATE'
    });
});

angular.module("CTC.APP-NAME")
.constant('appNavMenu', [
    {title:'LINK TITLE ', icon:'CTC ICON', url:'STATUS_REF'}
    ]);

// OPTIONAL on run option
angular.module("CTC.APP-NAME")
.run(function ($rootScope, appNavMenu, urlPrefixService) {
        $rootScope.appNavMenu = appNavMenu;
        urlPrefixService.setPrefix();
    });

```
