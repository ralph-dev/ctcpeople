/*
 *  Description: This is a helper class to send request to SQS service for update candidate in Daxtra Search Database
 *  Date: 23.05.2016
 */
public with sharing class SQSHelper {
    // SQS Credential
	private static String queueUrl;
	private static String queueUri;
	private static String url;
	private static String accessKey;
	private static String secretKey;
    
    public static void sendSQSRequests(List<PersonFeed> personFeedList) {
        setDaxtraFeedSQSCredential();
        List<String> requestBodyList = new List<String>();
		for(PersonFeed feed : personFeedList) {
 			String requestBody = createSQSRequestBody(feed);
            requestBodyList.add(requestBody);
		}
 		if (!Test.isRunningTest()){
 			sendRequest(requestBodyList, url);
 		}
	}
	
	public static void setDaxtraFeedSQSCredential() {
	    SQSCredentialSelector selector = new SQSCredentialSelector();
	    SQSCredential__c sqsCred = selector.getSQSCredential('Daxtra Feed SQS');
	    if (sqsCred != null) {
	        List<String> s = sqsCred.URL__c.split('/');
            queueUrl = s[2];
	        queueUri = '/' + s[3] + '/' + s[4];
	        url = sqsCred.URL__c;
	        accessKey = sqsCred.Access_Key__c;
	        secretKey = sqsCred.Secret_Key__c;	        
	    } 
        
	}
    
    public static String createSQSRequestBody(PersonFeed feed) {
        List<SQSMessageAttributeDTO> messageAttributes = generateMessageAttributes(feed);
        String messageBody = feed.action + ' Candidate [' + feed.candidateId + ']';
        String parameter = 'AWSAccessKeyId='+ encode(accessKey) +'&Action=SendMessage';
        for (Integer i = 0; i < messageAttributes.size(); i++) {
            parameter += '&MessageAttribute.' + (i + 1) + '.Name=' + messageAttributes.get(i).getAttributeName();
            parameter += '&MessageAttribute.' + (i + 1) + '.Value.DataType=' + messageAttributes.get(i).getAttributeDataType();
            parameter += '&MessageAttribute.' + (i + 1) + '.Value.StringValue=' + messageAttributes.get(i).getAttributeDataValue();      
        }
        parameter += '&MessageBody=' + encode(messageBody)
            + '&SignatureMethod=HmacSHA1'
            + '&SignatureVersion=2'
            + '&Timestamp=' + encode(getCurrentDate());
        String toSign = 'POST\n' + queueUrl + '\n' + queueUri + '\n' + parameter;
        String signatureStr = getMac(toSign, secretKey);        
        String body = parameter + '&Signature=' + signatureStr;
        return body;
    }
    
    /**
     * Method to send request to SQS 
     * @param requestBody
     */ 
    @Future(callout = true)
	public static void sendRequest(List<String> bodyList, String sqsUrl) {
        for (String body : bodyList) {
	       HttpRequest req = new HttpRequest();
	       req.setMethod('POST');
	       req.setEndpoint(sqsUrl);
	       req.setBody(body);
            Http http = new Http();
		  try {
                HttpResponse res = http.send(req);
            }
            catch (System.CalloutException e) {
                System.debug('ERROR: ' + e);
            }
        }
	}
	
    /*
     *  Generate message attributes for sqs
     */
    public static List<SQSMessageAttributeDTO> generateMessageAttributes(PersonFeed feed){     
        List<SQSMessageAttributeDTO> messageAttributes=new list<SQSMessageAttributeDTO>();
        SQSMessageAttributeDTO actionMsg = new SQSMessageAttributeDTO('Action', feed.action);
        messageAttributes.add(actionMsg);
        SQSMessageAttributeDTO orgIdMsg = new SQSMessageAttributeDTO('OrgId', feed.orgId);
        messageAttributes.add(orgIdMsg);
        SQSMessageAttributeDTO candIdMsg = new SQSMessageAttributeDTO('CandidateId', feed.candidateId);
        messageAttributes.add(candIdMsg);
        SQSMessageAttributeDTO timestampMsg = new SQSMessageAttributeDTO('Timestamp', String.valueOf(DateTime.now().getTime()));
        messageAttributes.add(timestampMsg);
        if (feed.action != 'Delete') {
            SQSMessageAttributeDTO lastNameMsg = new SQSMessageAttributeDTO('LastName', feed.LastName);
            messageAttributes.add(lastNameMsg);
            SQSMessageAttributeDTO resumeNameMsg = new SQSMessageAttributeDTO('ResumeName', feed.resumeName);
            messageAttributes.add(resumeNameMsg);
            SQSMessageAttributeDTO resumeBucketMsg = new SQSMessageAttributeDTO('ResumeBucket', feed.resumeBucket);
            messageAttributes.add(resumeBucketMsg);
        }
        return messageAttributes;
    }
    
    public static String getCurrentDate() {
        return DateTime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
    }
    
    
    public static String encode(String message){
        return EncodingUtil.urlEncode(message,'UTF-8').replace('+', '%20').replace('*', '%2A').replace('%7E','~');
    }
 
    public static String getMac(String RequestString, String secret_key) {
        String algorithmName = 'hmacSHA1';
        Blob input = Blob.valueOf(RequestString);
        Blob key = Blob.valueOf(secret_key);
        Blob signing =Crypto.generateMac(algorithmName, input, key);
        return EncodingUtil.urlEncode(EncodingUtil.base64Encode(signing), 'UTF-8');
    }    
}