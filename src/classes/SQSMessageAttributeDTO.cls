/**
 * POJO Class for SQS Message Attribute
 **/

public with sharing class SQSMessageAttributeDTO {
	private String attributeName;
	private String attributeDataType='String';
	private String attributeDataValue;
	
	public SQSMessageAttributeDTO (String attributeName, String attributeDataType,String attributeDataValue){
		this(attributeName,attributeDataValue);
		setAttributeDataType(attributeDataType);
	}
	
	public SQSMessageAttributeDTO(String attributeName,String attributeDataValue){
		setAttributeName(attributeName);
		setAttributeDataValue(attributeDataValue);
		 
	}
	
	public void setAttributeName(String attributeName){
		this.attributeName=attributeName;
	}
	
	public void setAttributeDataType(String attributeDataType){
		this.attributeDataType=attributeDataType;
	}
	
	public void setAttributeDataValue(String attributeDataValue){
		if(attributeDataValue!=null){
			attributeDataValue=EncodingUtil.urlEncode(attributeDataValue, 'UTF-8');
			attributeDataValue=attributeDataValue.replace('+','%20');
			this.attributeDataValue=attributeDataValue;
		}
	}
	
	public String getAttributeName(){
		return attributeName;
	}
	
	public String getAttributeDataType(){
		return attributeDataType;
	}
	public String getAttributeDataValue(){
		return attributeDataValue;
	}

}