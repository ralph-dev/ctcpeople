@isTest
private class ScheduledPullRateCardsTest {
	
	@isTest static void test_method_one() {
	  DataTestFactory.createCTCPeopleSettings();
      ScheduledPullRateCards scheduledRateCards=new ScheduledPullRateCards();
      String CRON_EXP = '0 0 0 3 9 ?';
      String jobId = System.schedule('TestApex',CRON_EXP,scheduledRateCards);
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
        NextFireTime
        FROM CronTrigger WHERE id = :jobId];
         System.assertEquals(CRON_EXP, 
         ct.CronExpression);	
         
        system.assert(true);
	}
	
}