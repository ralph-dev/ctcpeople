@isTest
public class ExtractData4SBEControllerTest {

    static testmethod void unitTest() {
    	
    	System.runAs(DummyRecordCreator.platformUser) {
        
        // Create Data
        List<Contact> selectedCon = new List<Contact>();
        RecordType candRT = DaoRecordType.indCandidateRT;
        Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand1@test.ctc.test');
        Contact cand2 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 2',Email='Cand2@test.ctc.test');
        Contact cand3 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 3',Email='Cand3@test.ctc.test');
        selectedCon.add(cand1);
        selectedCon.add(cand2);
        selectedCon.add(cand3);
        
        Test.startTest();
        insert selectedCon;
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(selectedCon);
        ssc.setSelected(selectedCon);
        ExtractData4SBEController sbe = new ExtractData4SBEController(ssc);
        PageReference testpr = sbe.inits();
        Test.stopTest();
        
        system.assert(testpr.getUrl().contains('BulkEmailCandidateListPage'));
        Map<String,String> params = testpr.getParameters();
        system.assertEquals('peopleListView', params.get('sourcepage'));
        system.assertEquals(selectedCon[0].Id, params.get('arg0'));
        system.assertEquals(selectedCon[1].Id, params.get('arg1'));
        system.assertEquals(selectedCon[2].Id, params.get('arg2'));
    	}

    }
    
    // test if no candidate being selected, the page should show error
    static testmethod void testNoCandSelected() {
    	System.runAs(DummyRecordCreator.platformUser) {
        List<Contact> selectedCon = new List<Contact>();
        
        Test.startTest();
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(selectedCon);
        ExtractData4SBEController sbe = new ExtractData4SBEController(ssc);
        PageReference testpr = sbe.inits();
        Test.stopTest();
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean b = false;
        PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(PeopleCloudErrorInfo.ERR_NO_CONTACT_SELECTED, true); 
        for(Apexpages.Message msg:msgs){
            System.debug('Error Message = ' + msg);
            for (Apexpages.Message errormsg:errormesg.errorMessage) {
	            if (msg == errormsg) b = true;
            }
	    }
 	    system.assert(b);
 	    system.assertEquals(null, testpr);
    	}
    }
    
    // test if more than 100 candidate being selected, the page should show error
    static testmethod void testTooManyCandSelected() {
        System.runAs(DummyRecordCreator.platformUser) {
        List<Contact> selectedCon = new List<Contact>();
        RecordType candRT = DaoRecordType.indCandidateRT;
        for (Integer i = 0; i < 101; i ++ ) {
            Contact cand = new Contact(RecordTypeId=candRT.Id,LastName='test1',Email='Cand1@test.ctc.test');
            selectedCon.add(cand);
        }
        
        Test.startTest();
        insert selectedCon;
        system.debug(selectedCon.size());

        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(selectedCon);
        ssc.setSelected(selectedCon);
        ExtractData4SBEController sbe = new ExtractData4SBEController(ssc);
        PageReference testpr = sbe.inits();
        Test.stopTest();
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean b = false;
        PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(PeopleCloudErrorInfo.ERROR_BULK_EMAIL_TooManyCandSelectError, true); 
        for(Apexpages.Message msg:msgs){
            System.debug('Error Message = ' + msg);
            for (Apexpages.Message errormsg:errormesg.errorMessage) {
	            if (msg == errormsg) b = true;
            }
	    }
 	    system.assert(b);
 	    system.assertEquals(null, testpr);
        }
    }
    
    // Test to return the previous page
    static testmethod void testReturnPage() {
    	System.runAs(DummyRecordCreator.platformUser) {
        List<Contact> selectedCon = new List<Contact>();
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(selectedCon);
        Apexpages.currentPage().getParameters().put('retURL', 'testURL');
        ExtractData4SBEController sbe = new ExtractData4SBEController(ssc);
        PageReference testpr = sbe.inits();
        system.assertEquals(null, testpr);
        
        system.assert(sbe.retURL.contains('testURL'));
    	}
    }
    
    
    

    

}