public class TriggerDetail{

	String accountid;
	//Map<String,Map<String,String>> trigger_detail = new Map<String,Map<String,String>>();
	Map<String,DetailRecord> triggerdetails = new Map<String,DetailRecord>();
	xmldom theXMLDom;
	public class DetailRecord{
        public String apiName{get;set;}
        public Boolean Active{get;set;}
        public String targetObject{get;set;}
        public String AccountId{get;set;}
        public String ConRecordTypeId{get;set;}
        public String Note{get;set;}
        DetailRecord(String a, Boolean b, String t, String aid, String rtid, String note){
            this.apiName = a;
            this.targetObject = t;
            this.AccountId = aid;
            this.ConRecordTypeId = rtid;
            this.Active = b;
            this.Note = note;        
        }
	} 
	public List<DetailRecord> details = new List<DetailRecord>();
	
	public List<DetailRecord> getDetails(){	    
	    return details ;	
	}

	public String getAccountid(String NameString){
	    if(triggerdetails.get(NameString).AccountId != null){
	    	return triggerdetails.get(NameString).AccountId;
	    }
	    return null;
	}
	
	public String getRecordTypeId(String NameString){
	    if(triggerdetails.get(NameString).ConRecordTypeId != null)
	    {return triggerdetails.get(NameString).ConRecordTypeId;}
	    return null;
	}
	
	public boolean IsActive(String NameString){
		if(triggerdetails.size()==0){
			return false;
		}
	
	    if(triggerdetails.get(NameString).Active != null){
	        if(triggerdetails.get(NameString).Active)
	        {return true;}
	    }
		return false;
	}
	
public String getTriggerNote(String NameString){
	if(triggerdetails.get(NameString).Note != null){
        return triggerdetails.get(NameString).Note;
    }
    return null;
}

public void getTriggerDetails(){
    String status, target, aid, rtid, nt;
    DetailRecord dr;
    Blob bodyBlob ;
     
    Document d; 
    Map<String,String> tempMap = new Map<String,String>();
    String tempStr;    

    String apiname, recordtypeid, targetobj, note,accid, thestatus; 
    d = DaoDocument.getCTCConfigFileByDevName('Trigger_Admin');
    bodyBlob=null;
    if(d!=null){
    	bodyBlob = d.Body;
    }
        if(bodyBlob != null)
        {    
            if(bodyBlob.toString() != null)
            {    
                this.theXMLDom = new xmldom(bodyBlob.toString());

                if (this.theXMLDom != null )
                {
                    List<xmldom.Element> trigger_elements = this.theXMLDom.getElementsByTagName('trigger');
                    //system.debug('trigger_elements='+trigger_elements);
                    if(trigger_elements != null)
                    {   for(xmldom.Element e :trigger_elements )
                        {    tempMap.clear();
                             tempStr = '';
                             status = '';
                             target = '';
                             aid = '';
                             rtid = '';
                             dr = null;
                             nt = '';
                            if(e.hasChildNodes())
                            {
                                List<xmldom.Element> objectfields = e.childNodes;
                                
                                for(xmldom.Element ee: objectfields)
                                {
                                    if(ee.nodeName.toLowerCase() == 'apiname') 
                                    {

                                        tempStr = ee.nodeValue;                                         
                                        
                                    }
                                    if(ee.nodeName.toLowerCase() == 'targetobject') 
                                    {
                                        //tempMap.put(ee.nodeName.toLowerCase() ,ee.nodeValue);
                                        target = ee.nodeValue;
                                    }
                                    if(ee.nodeName.toLowerCase() == 'status') 
                                    {
                                        //tempMap.put(ee.nodeName.toLowerCase() ,ee.nodeValue);
                                        status = ee.nodeValue;
                                    }
                                    if(ee.nodeName.toLowerCase() == 'accountid') 
                                    {
                                        //tempMap.put(ee.nodeName.toLowerCase() ,ee.nodeValue);
                                        aid = ee.nodeValue;
                                    }
                                    if(ee.nodeName.toLowerCase() == 'recordtypeid') 
                                    {
                                        //tempMap.put(ee.nodeName.toLowerCase() ,ee.nodeValue);
                                        rtid = ee.nodeValue;
                                    }
                                    if(ee.nodeName.toLowerCase() == 'note') 
                                    {
                                    
                                        //tempMap.put(ee.nodeName.toLowerCase() ,ee.nodeValue);
                                        nt = ee.nodeValue;
                                    }
                                }
                                                                
                            }
                            if(status == null)
                            {
                                dr = new DetailRecord(tempStr,false,target, aid, rtid, nt);
                            }
                            else
                            {
                                if(status.toLowerCase()=='active'){
                                    dr = new DetailRecord(tempStr,true,target, aid, rtid, nt);
                                }
                                else
                                {
                                    dr = new DetailRecord(tempStr,false,target, aid, rtid, nt);
                                }
                            }
                            
                            if(dr!=null)
                            {
                                details.add(dr);
                            }
                            //trigger_detail.put(tempStr,tempMap);
                        
                        }
                    }// if (trigger_elements != null)
                }
            }

        
        for(DetailRecord detail_record: details)
        {
            triggerdetails.put(detail_record.apiName,detail_record); 
        }
       // system.debug('triggerdetails='+ triggerdetails);
    }
    
    
    
}
public static testMethod void test1() {
	System.runAs(DummyRecordCreator.platformUser) {
	RecordType testaccountrecordtype = DaoRecordtype.candidatePoolRT;
    Account testaccount = new Account(Name='test', Recordtypeid= testaccountrecordtype.id);
    insert testaccount;
    
    TriggerDetail thecontroller = new TriggerDetail();
    thecontroller.getDetails();
    thecontroller.getTriggerDetails();
    system.assert(thecontroller.triggerdetails.size()>0);
    try{
	    system.assert(thecontroller.getAccountid('test')== null);
	    system.assert(thecontroller.getRecordTypeId('test') == null);
	    system.assertEquals(thecontroller.IsActive('test'),false);
    }
    catch(Exception ex){
    system.debug(ex);
    
    }
	}
    
    
}
}