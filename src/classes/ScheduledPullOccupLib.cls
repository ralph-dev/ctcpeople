global class ScheduledPullOccupLib implements Schedulable {
    global void execute(SchedulableContext sc) {
       actScheduledJobs();
    }
    @future(callout=true)
    public static void actScheduledJobs(){
         APSynchronizeServices apSynServices=new APSynchronizeServices();
         apSynServices.synchroniseOccupLib();
    }
}