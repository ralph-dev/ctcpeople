/**
 * This is the test class for JobBoardPostingController
 *
 * This test class only includes test of generic method
 * For test of JobBoard specific method
 * please check test class for concrete JobBoard class
 *
 * Created by: Lina Wei
 * Created on: 31/03/2017
 */

@isTest
private class JobBoardPostingControllerTest {

    public static Advertisement__c ad;
    public static JobBoardPostingController controller;

    static {
        System.runAs(DummyRecordCreator.platformUser) {
            ad = AdvertisementDummyRecordCreator.generateSeekAdDummyRecord();
            insert ad;
            ApexPages.StandardController stdController = new ApexPages.StandardController(ad);
            controller = new JobBoardPostingController(stdController);
            controller.jobBoard = JobBoardFactory.getJobBoard(ad);

            PCAppSwitch__c cs = new PCAppSwitch__c();
            cs.Enable_Daxtra_Skill_Parsing__c = true;
            insert cs;
        }
    }

    static testMethod void testJobBoardPostMethod() {
        User u = DummyRecordCreator.platformUser;
        System.runAs(u) {
            Advertisement_Configuration__c account = AdConfigDummyRecordCreator.createDummySeekAccount();
            u.Seek_Account__c = account.Id;

            ApexPages.StandardController stdController = new ApexPages.StandardController(ad);
            JobBoardPostingController con = new JobBoardPostingController(stdController);
            con.postInit();
            con.adToPost.Advertisement_Account__c = account.Id;
            con.postAd();

            System.assert(true);
        }
    }

    static testMethod void testJobBoardOtherMethod() {
        User u = DummyRecordCreator.platformUser;
        System.runAs(u) {
            Advertisement_Configuration__c account = AdConfigDummyRecordCreator.createDummySeekAccount();
            u.Seek_Account__c = account.Id;

            ApexPages.StandardController stdController = new ApexPages.StandardController(ad);
            JobBoardPostingController con = new JobBoardPostingController(stdController);

            con.ad.Status__c = 'Clone to Post';
            con.updateInit();
            con.ad.Advertisement_Account__c = account.Id;
            con.updateAd();

            con.cloneInit();
            con.cloneAd();

            con.archiveInit();
            con.archiveAd();

            System.assert(true);
        }
    }

    static testMethod void testCancelMethod() {
    	System.runAs(DummyRecordCreator.platformUser) {
        System.assertNotEquals(null, controller.cancelArchive());
        System.assertNotEquals(null, controller.cancelClone());
    	}
    }

    static testMethod void testGetSkillGroupSelectOptions() {
    	System.runAs(DummyRecordCreator.platformUser) {
        DataTestFactory.createSkillGroup();
        System.assertEquals(DataTestFactory.recordNumber, controller.getSkillGroupSelectOptions().size());
    	}
    }

    static testMethod void testGetApplicationStyleSelectOptions() {
    	System.runAs(DummyRecordCreator.platformUser) {
        List<SelectOption> options = controller.getApplicationStyleSelectOptions();
        System.assert(options.size() > 0);
    	}
    }

    static testMethod void testGetVideoPositionSelectOptions() {
    	System.runAs(DummyRecordCreator.platformUser) {
        List<SelectOption> options = controller.getVideoPositionSelectOptions();
        System.assert(options.size() > 0);
    	}
    }

    static testMethod void testBackToAdDetailPage() {
    	System.runAs(DummyRecordCreator.platformUser) {
        controller.jobBoard.returnPage = Page.seekEdit;
        System.assertNotEquals(null, controller.backToAdDetailPage());
    	}
    }
}