public with sharing class StyleCategoryExtension {
	public StyleCategoryExtension(Object obj) {
		
	}

	@RemoteAction
	public static List<StyleCategory__c> getSaveSearchRecordsByUser() {
	    List<StyleCategory__c> styleCategories = new StyleCategorySelector().getUserSaveSearchRecord();
	    return SaveSearchConvertor.convertGoogleSearchToPeopleSearch(styleCategories);
	}

	@RemoteAction
	public static List<StyleCategory__c> createRecords(List<Map<String, Object>> styleCategories) {
		StyleCategoryService sService = new StyleCategoryService();
		return sService.createSaveSearchRecords(styleCategories);
	}

	@RemoteAction
	public static List<StyleCategory__c> updateRecords(List<Map<String, Object>> styleCategories) {
		StyleCategoryService sService = new StyleCategoryService();
		return sService.updateSaveSearchRecords(styleCategories);
	}  
	
	@RemoteAction
	public static List<Id> deleteRecords(List<Map<String, Object>> styleCategories) {
	    StyleCategoryService sService = new StyleCategoryService();
		return sService.deleteRecords(styleCategories);
	}

	@RemoteAction
    public static Map<String, RemoteAPIDescriptor.RemoteAction> apiDescriptor(){
        RemoteAPIDescriptor descriptor = new RemoteAPIDescriptor();
        descriptor.description('CRUD & describe remote action for StyleCategory.');
        descriptor.action('getSaveSearchRecordsByUser');
        descriptor.action('createRecords').param(List<Map<String, Object>>.class);
        descriptor.action('updateRecords').param(List<Map<String, Object>>.class);
        descriptor.action('deleteRecords').param(List<Map<String, Object>>.class);
        return descriptor.paramTypesMap;
    }    
}