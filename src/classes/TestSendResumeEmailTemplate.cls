@isTest
private class TestSendResumeEmailTemplate {
	private static testmethod void TestSendResumeEmailTemplate(){
		System.runAs(DummyRecordCreator.platformUser) {
		List<EmailTemplate> templates = [Select FolderId From EmailTemplate where ((TemplateStyle='none' and TemplateType='text')
            or( TemplateStyle='freeForm' and TemplateType='html')) and isactive=true order by Name];
        Set<String> ids = new Set<String>();
        for (EmailTemplate template : templates) {
            ids.add(template.Id);
        }
		SendResumeEmailTemplate testresult = new SendResumeEmailTemplate();
		testresult.initEmailTemplate();
		List<SendResumeEmailTem> listnewSendResTem = testresult.newSendResTem;
		if (ids.contains(UserInfo.getUserId())) {
		    SendResumeEmailTem onenewSendResTem = listnewSendResTem[0];
		    system.assertEquals((String)onenewSendResTem.emailWrapperfolder.folderId, UserInfo.getUserId());
		}
		if (ids.contains(UserInfo.getOrganizationId())) {
		    SendResumeEmailTem lastnewSendResTem = listnewSendResTem[listnewSendResTem.size()-1];
		    system.assertEquals((String)lastnewSendResTem.emailWrapperfolder.folderId, UserInfo.getOrganizationId());
		}
		}
	}
}