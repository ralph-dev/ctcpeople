/**
 * This is the DTO class to for Seek Job Content
 *
 * Created by: Lina Wei
 * Created On: 03/03/2017
 */

public class SeekJobContent {
    public ThirdParties thirdParties;
    public String advertisementType;
    public String jobTitle;
    public String searchJobTitle;
    public Location location;
    public String subclassificationId;
    public String workType;
    public Salary salary;
    public String jobSummary;
    public String advertisementDetails;
    public SeekAdContact contact;
    public Video video;
    public String applicationEmail;
    public String applicationFormUrl;
    public String endApplicationUrl;
    public String screenId;
    public String jobReference;
    public String agentJobReference;
    public Template template;
    public Standout standout;
    public Recruiter recruiter;
    public List<String> additionalProperties;
    public String creationId;

    public SeekJobContent() {}

    public class ThirdParties {
        public String advertiserId;
        public thirdParties(String adId) {
            advertiserId = adId;
        }
    }

    public class Location {
        public String id;
        public String areaId;
        public Location(String lId, String aId) {
            Id = lId;
            areaId = aId;
        }
    }

    public class Salary {
        public String type;
        public String minimum;
        public String maximum;
        public String details;
        public Salary(String sType, String sMin, String sMax, String sDetails) {
            type = sType;
            minimum = sMin;
            maximum = sMax;
            details = sDetails;
        }
    }

    public class SeekAdContact {
        public String name;
        public String phone;
        public String email;
        public SeekAdContact(String contactName, String contactPhone, String contactEmail) {
            name = contactName;
            phone = contactPhone;
            email = contactEmail;
        }
    }

    public class Video {
        public String url;
        public String position;
        public Video(String videoUrl, String videoPostion) {
            url = videoUrl;
            position = videoPostion;
        }
    }

    public class Template {
        public String id;
        public List<Item> items;
        public Template(String templateId) {
            id = templateId;
        }
    }

    public class Item {
        public String name;
        public String value;
        public Item(String itemName, String itemValue) {
            name = itemName;
            value = itemValue;
        }
    }

    public class Standout {
        public String logoId;
        public List<String> bullets;
        public Standout(String sLogoId) {
            logoId = sLogoId;
        }
    }

    public class Recruiter {
        public String fullName;
        public String email;
        public String teamName;
        public Recruiter() {}
    }

}