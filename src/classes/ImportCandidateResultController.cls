/**
This is a contoller for showing error info occured in "Import Candidate"
function.
**/
public with sharing class ImportCandidateResultController {
    public Map<String, String> errorsMap =  new Map<String, String>();
    
    public String errorInfo{get; set;}
    public boolean parserError {get;set;}
    public boolean sfError{get;set;}
    public boolean s3Error{get;set;}
    public boolean usrError{get;set;}
    public boolean noEmailError {get;set;}
    public boolean noError{get;set;}
    public boolean otherError{get;set;}
    public boolean maintenance{get;set;}
    public String candidateId{get;set;}
    public String errorCode{get;set;} 
    public String orgId{get;set;}
    public String bucketName {get;set;}
    public String urlPrefix{get;set;}
    public Contact con{get; set;}
    
    public ImportCandidateResultController(ApexPages.StandardController stdController){
        con =(Contact) stdController.getRecord();
        errorsMap.put('10000','The user already exists in CTC People.');
        errorsMap.put('10001','Your document failed to parse successfully and the Candidate record was not create. Click to Cloud have been automatically notified. These failures may occur from time to time and are normally due to issues related to the resume document content. Please create the Candidate record manually and update accordingly.');
        errorsMap.put('10002','Please contact your administrator to check amazon S3 Account');
        errorsMap.put('10003','Fail to insert contact to Salesforce database. Please manully create this candidate.');
        errorsMap.put('10004','This Ad. has been deleted/Expired from website. You are not allowed to update and repost it!');
        errorsMap.put('10005','This Ad. has been in queue. You are not allowed to update and repost it!');
        errorsMap.put('99999','An error occurs during this request. the package provider are notified.');
        /*
        Document doc = [Select Id, Body From Document where Name='peopleCloudErrorJson' limit 1];
        //system.debug(doc.Body.toString());
        JSONObject jobj = new JSONObject(doc.Body.toString());
        */
        //String errorCode = ApexPages.currentPage().getParameters().get('code');
        //this.errorInfo = jobj.getString(errorCode)+additionalInfo(errorCode);
        //this.errorInfo = errorsMap.get(errorCode)+additionalInfo(errorCode);
    }
    
    public void init(){
        parserError = false;
        sfError = false;
        s3Error = false;
        usrError = false;
        noError = false;
        otherError = false;
        maintenance = false;
        noEmailError = false;
        
        this.errorCode = ApexPages.currentPage().getParameters().get('code');
        this.candidateId = ApexPages.currentPage().getParameters().get('id');
        this.orgId = ApexPages.currentPage().getParameters().get('oid');
        this.bucketName = ApexPages.currentPage().getParameters().get('bn');
        
        if(errorCode == '00000'){
            noError = true;
        }else if(errorCode == '10000'){
            usrError = true;
        }else if(errorCode == '10001'){
            noEmailError = true;
        }else if(errorCode =='10002'){
            s3Error = true;
        }else if(errorCode == '10003'){
            sfError = true;
        }else if(errorCode == '10004'){
            parserError = true;
        }else if(errorCode == '88888'){
            maintenance = true;
        }else{
            errorCode = '99999';
            otherError = true;
        }
    }

    public PageReference checkCandidatePage(){
        if(errorCode == '00000' || errorCode == '10000' || errorCode == '10004'){
            PageReference pageRef = new PageReference('/'+candidateId);
            pageRef.setRedirect(true);
            return pageRef;
            
        }else if(errorCode == '10003' || errorCode == '10001' || errorCode == '88888' || errorCode == '10002' || errorCode == '99999'){
            PageReference pageRef = new PageReference('/003/o');
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }
       

    public PageReference importCandidateAgain(){
        PageReference pageRef = new PageReference('/apex/importCandidate?orgid='+this.orgId+'&bn='+this.bucketName);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    
    public static testmethod void test10000(){
        Test.startTest();
        Test.setCurrentPage(Page.ImportCandidate);
        ApexPages.currentPage().getParameters().put('code','10000');
        ApexPages.currentPage().getParameters().put('id','aaaaaaaaaaaaaaa');
        ApexPages.currentPage().getParameters().put('oid','bbbbbbbbbbbbbbb');
        ApexPages.currentPage().getParameters().put('bn','test003');
        Contact c1 = new Contact(LastName='zhangsan');
        ApexPages.StandardController stdController = new ApexPages.StandardController(c1);
        ImportCandidateResultController icc = new ImportCandidateResultController(stdController);
        icc.init();
        PageReference temppage = icc.checkCandidatePage();
        system.assert(temppage.getURL().contains('aaaaaaaaaaaaaaa'));
        PageReference temppagereimport = icc.importCandidateAgain();
        system.assert(temppagereimport.getURL().contains('bbbbbbbbbbbbbbb'));
    }
    
    public static testmethod void test00000(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Test.startTest();
        Test.setCurrentPage(Page.ImportCandidate);
        ApexPages.currentPage().getParameters().put('code','00000');
        ApexPages.currentPage().getParameters().put('id','aaaaaaaaaaaaaaa');
        ApexPages.currentPage().getParameters().put('oid','bbbbbbbbbbbbbbb');
        ApexPages.currentPage().getParameters().put('bn','test003');
        Contact c1 = new Contact(LastName='zhangsan');
        ApexPages.StandardController stdController = new ApexPages.StandardController(c1);
        ImportCandidateResultController icc = new ImportCandidateResultController(stdController);
        icc.init();
        PageReference temppage = icc.checkCandidatePage();
        system.assert(temppage.getURL().contains('aaaaaaaaaaaaaaa'));
        PageReference temppagereimport = icc.importCandidateAgain();
        system.assert(temppagereimport.getURL().contains('bbbbbbbbbbbbbbb'));
    	}
    }
    
    public static testmethod void test10002(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Test.startTest();
        Test.setCurrentPage(Page.ImportCandidate);
        ApexPages.currentPage().getParameters().put('code','10002');
        ApexPages.currentPage().getParameters().put('id','aaaaaaaaaaaaaaa');
        ApexPages.currentPage().getParameters().put('oid','bbbbbbbbbbbbbbb');
        ApexPages.currentPage().getParameters().put('bn','test003');
        Contact c1 = new Contact(LastName='zhangsan');
        ApexPages.StandardController stdController = new ApexPages.StandardController(c1);
        ImportCandidateResultController icc = new ImportCandidateResultController(stdController);
        icc.init();
        PageReference temppage = icc.checkCandidatePage();
        system.assert(temppage.getURL().contains('003'));
        PageReference temppagereimport = icc.importCandidateAgain();
        system.assert(temppagereimport.getURL().contains('bbbbbbbbbbbbbbb'));
    	}
    }
    
    public static testmethod void test10003(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Test.startTest();
        Test.setCurrentPage(Page.ImportCandidate);
        ApexPages.currentPage().getParameters().put('code','10003');
        ApexPages.currentPage().getParameters().put('id','aaaaaaaaaaaaaaa');
        ApexPages.currentPage().getParameters().put('oid','bbbbbbbbbbbbbbb');
        ApexPages.currentPage().getParameters().put('bn','test003');
        Contact c1 = new Contact(LastName='zhangsan');
        ApexPages.StandardController stdController = new ApexPages.StandardController(c1);
        ImportCandidateResultController icc = new ImportCandidateResultController(stdController);
        icc.init();
        PageReference temppage = icc.checkCandidatePage();
        system.assert(temppage.getURL().contains('003'));
        PageReference temppagereimport = icc.importCandidateAgain();
        system.assert(temppagereimport.getURL().contains('bbbbbbbbbbbbbbb'));
    	}
    }
    
    public static testmethod void test88888(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Test.startTest();
        Test.setCurrentPage(Page.ImportCandidate);
        ApexPages.currentPage().getParameters().put('code','88888');
        ApexPages.currentPage().getParameters().put('id','aaaaaaaaaaaaaaa');
        ApexPages.currentPage().getParameters().put('oid','bbbbbbbbbbbbbbb');
        ApexPages.currentPage().getParameters().put('bn','test003');
        Contact c1 = new Contact(LastName='zhangsan');
        ApexPages.StandardController stdController = new ApexPages.StandardController(c1);
        ImportCandidateResultController icc = new ImportCandidateResultController(stdController);
        icc.init();
        PageReference temppage = icc.checkCandidatePage();
        system.assert(temppage.getURL().contains('003'));
        PageReference temppagereimport = icc.importCandidateAgain();
        system.assert(temppagereimport.getURL().contains('bbbbbbbbbbbbbbb'));
    	}
    }
    
    public static testmethod void test99999(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Test.startTest();
        Test.setCurrentPage(Page.ImportCandidate);
        ApexPages.currentPage().getParameters().put('code','99999');
        ApexPages.currentPage().getParameters().put('id','aaaaaaaaaaaaaaa');
        ApexPages.currentPage().getParameters().put('oid','bbbbbbbbbbbbbbb');
        ApexPages.currentPage().getParameters().put('bn','test003');
        Contact c1 = new Contact(LastName='zhangsan');
        ApexPages.StandardController stdController = new ApexPages.StandardController(c1);
        ImportCandidateResultController icc = new ImportCandidateResultController(stdController);
        icc.init();
        PageReference temppage = icc.checkCandidatePage();
        system.assert(temppage.getURL().contains('003'));
        PageReference temppagereimport = icc.importCandidateAgain();
        system.assert(temppagereimport.getURL().contains('bbbbbbbbbbbbbbb'));
    	}
    }
    
    
    
}