/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestOperatorsHandle {
  static testMethod void myUnitTest() {
  	System.runAs(DummyRecordCreator.platformUser) {
    List<Object> jsonlist=new List<Object>();
    String jsonvalues='[{"fieldname":"Direct_Reports__c","fieldtype":"DOUBLE","operator":"not equals","values":"58"},{"fieldname":"Candidate_From_Web__c","fieldtype":"BOOLEAN","operator":"equals","values":"true"},{"fieldname":"HomePhone","fieldtype":"PHONE","operator":"contains","values":"42628072"},{"fieldname":"CreatedById","fieldtype":"REFERENCE","operator":"equals","values":"00590000001T8YjAAK"},{"fieldname":"Birthdate","fieldtype":"DATE","operator":"greater than","values":"3/08/1981"},{"fieldname":"Status__c","fieldtype":"PICKLIST","operator":"not equals","values":"Not Actively Searching"},{"fieldname":"AccountId","fieldtype":"REFERENCE","operator":"equals","values":"0019000000QB0YQAA1"},{"fieldname":"RecordTypeId","fieldtype":"REFERENCE","operator":"equals","values":"01290000000WwX8AAK"},{"fieldname":"Skills__c","fieldtype":"MULTIPICKLIST","operator":"contains","values":"Problem solving skills;OOP"}]';
    jsonlist=(List<Object>)JSON.deserializeUntyped(jsonvalues);
    String queryclause='';
    boolean firsttime=true;
    if(jsonlist.size()!=0){
      for(Integer i=0;i<jsonlist.size();i++){
         Map<String,Object> obj=(Map<String,Object>)jsonlist.get(i);             
                String value=(String)obj.get('values');
                String fieldname=(String)obj.get('fieldname');
                String fieldtype=(String)obj.get('fieldtype');
                String operator=(String)obj.get('operator');
                OperatorsHandle operh=new OperatorsHandle(fieldname,operator,fieldtype,value);
                if(firsttime){
                    queryclause=operh.getQuery();
                    firsttime=false;
                }else{
                    queryclause=queryclause+' AND '+operh.getQuery();
                }
        
      }
      queryclause='select id from Contact where '+queryclause;
    }
    system.assertEquals(queryclause,'select id from Contact where Direct_Reports__c  != 58 AND Candidate_From_Web__c  =  true AND HomePhone  like  \'%42628072%\' AND CreatedById  =  \'00590000001T8YjAAK\' AND Birthdate  >  1981-08-03 AND (Status__c  != \'Not Actively Searching\') AND AccountId  =  \'0019000000QB0YQAA1\' AND RecordTypeId  =  \'01290000000WwX8AAK\' AND (Skills__c includes (\'Problem solving skills;OOP\'))');
    
    String values='[{"fieldname":"Direct_Reports__c","fieldtype":"DOUBLE","operator":"equals","values":""},{"fieldname":"AccountId","fieldtype":"REFERENCE","operator":"not equals","values":"0019000000PF5LGAA1"},{"fieldname":"Name","fieldtype":"STRING","operator":"equals","values":"Andy Young"}]';
    OperatorsHandle operh=new OperatorsHandle(values);
    String queriesstr=operh.getqueries();
    System.debug('the query is '+queriesstr);
    System.assertEquals(queriesstr,'select id from Contact where Direct_Reports__c  =  NULL AND AccountId  != \'0019000000PF5LGAA1\' AND Name  =  \'Andy Young\'');
    values='[{"fieldname":"AccountId","fieldtype":"REFERENCE","operator":"not equals","values":"0019000000PF5LFAA1"},{"fieldname":"HomePhone","fieldtype":"PHONE","operator":"contains","values":"042628"},{"fieldname":"Direct_Reports__c","fieldtype":"DOUBLE","operator":"less than","values":"11111"},{"fieldname":"Status__c","fieldtype":"PICKLIST","operator":"not equals","values":"Actively Searching,New Registration"},{"fieldname":"Skills__c","fieldtype":"MULTIPICKLIST","operator":"not contains","values":"Basic knowledge of Linux command HTML,Basic knowledge of SDLC,Agile"},{"fieldname":"CreatedDate","fieldtype":"DATETIME","operator":"greater or equal","values":"23/07/2013"},{"fieldname":"Birthdate","fieldtype":"DATE","operator":"greater than","values":"23/08/1995"}]';
	operh=new OperatorsHandle(values);   
	queriesstr=operh.getqueries();
    System.assertEquals(queriesstr,'select id from Contact where AccountId  != \'0019000000PF5LFAA1\' AND HomePhone  like  \'%042628%\' AND Direct_Reports__c  <  11111 AND (Status__c  != \'Actively Searching\' OR Status__c  != \'New Registration\') AND (Skills__c excludes (\'Basic knowledge of Linux command HTML\',\'Basic knowledge of SDLC\',\'Agile\')) AND CreatedDate  >=  2013-07-23T00:00:00Z AND Birthdate  >  1995-08-23');
  	}
  }

}