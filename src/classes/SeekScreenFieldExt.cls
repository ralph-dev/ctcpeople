public with sharing class SeekScreenFieldExt {

  FieldsClass ObjectFields; 
  List<FieldsClass.AvailableField> allFields = new List<FieldsClass.AvailableField>{};

  private final Application_Question__c  ssf ;

  public SeekScreenFieldExt(ApexPages.StandardController controller) {
        this.ssf = (Application_Question__c) controller.getRecord();
        
        ObjectFields = new FieldsClass('Contact');
        allFields = ObjectFields.getAvailableFields();   
        
        //==== Ordering allFields
        Map<String, FieldsClass.AvailableField> mapFieldNames = new Map<String, FieldsClass.AvailableField>();
        for(FieldsClass.AvailableField field1 : allFields) {
            mapFieldNames.put(field1.getFieldLabel(), field1);
        }
                
        //..
        List<String> fieldNames = new List<String>();
        fieldNames.addAll(mapFieldNames.keySet());     
        fieldNames.sort();
        
        //..
        List<FieldsClass.AvailableField> allFields2 = new List<FieldsClass.AvailableField>{};
        for(String fieldName : fieldNames) {
            allFields2.add(mapFieldNames.get(fieldName));
        }       
        
        allFields = allFields2;
  }
    
  public List<FieldsClass.AvailableField> getAllfields(){    
        return allFields;
  }
  
  public PageReference save() {
        if(ssf.Questions__c == null || ssf.Questions__c == '') {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Question Id is required.'));
            return null;
        }
        Boolean anyFieldNameSelected = false;
        if(ssf.Questions__c != null && ssf.Questions__c != '') {
            for(FieldsClass.AvailableField field1 : allFields) {
                if(field1.getFieldApi() == ssf.Name) anyFieldNameSelected = true;
            }
        }
               
        if(! anyFieldNameSelected) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Field Name is required.'));
            return null;
        }
        //system.debug('ssf ='+ ssf);
        ssf.RecordTypeId = (DaoRecordType.SeekScreenFieldRT).Id;
        try{
            fflib_SecurityUtils.checkFieldIsInsertable(Application_Question__c.SObjectType, Application_Question__c.RecordTypeId);
            fflib_SecurityUtils.checkFieldIsUpdateable(Application_Question__c.SObjectType, Application_Question__c.RecordTypeId);
            upsert ssf;
        }catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Failed to save question set - ' + e.getMessage()));
            return null;
        }
        
        
        //PageReference pageRef = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        PageReference pageRef = new PageReference(PeopleCloudHelper.makeRetURL(ApexPages.currentPage()));
        return pageRef;
  }
}