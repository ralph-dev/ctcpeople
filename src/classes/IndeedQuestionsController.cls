public with sharing class IndeedQuestionsController {
	
	public list<list<FieldsClass.AvailableField>> fieldLists{set;get;}
	public String optionJsonStr{Set;get;}
	public String labelValueStr{set;get;}
	public Application_Question_Set__c applicationQuestionSet{set;get;}
	public String applicationQuestionSetId{set;get;}
	public String existingApplicationQuestionJSONStr{set;get;}
	public IndeedQuestionsController(ApexPages.StandardController acon) {
		this.applicationQuestionSet = (Application_Question_Set__c)acon.getRecord();
		applicationQuestionSetId=this.applicationQuestionSet.Id;
		existingApplicationQuestionJSONStr='{}';
		if(applicationQuestionSetId!=null){
			getApplicationQuestionSet(applicationQuestionSetId);
		}
		prepareFieldList(); 
		preparedPicklistFieldAndOption();
	}
	
	public void getApplicationQuestionSet(String applicationSetId){
		IndeedApplicationQuestionServices applicationService=new IndeedApplicationQuestionServices();
		list<IndeedScreenQuestionFeed> applicationQuestion=applicationService.getPrePopulateApplicationQuestions(applicationSetId);
		if(applicationQuestion.size()!=0){
			existingApplicationQuestionJSONStr=JSON.serialize(applicationQuestion);
		}
	}
	
	//THIS METHOD IS USED TO OBTAIN ALL EDTIABLE FIELDS UNDER CONTACT
	public void prepareFieldList(){
			fieldLists=new list<list<FieldsClass.AvailableField>>();
			FieldsClass fieldCl=new FieldsClass();
			List<FieldsClass.AvailableField> rawAvailableFields=fieldCl.getAvailableFields();
			System.debug('The total number is '+rawAvailableFields.size());
			list<FieldsClass.AvailableField> availableFields=new list<FieldsClass.AvailableField>();
			
			for(FieldsClass.AvailableField singleField: rawAvailableFields){
				if(singleField.getIsUpdateable() && singleField.getFieldType()!='REFERENCE' &&  singleField.getFieldType()!='BOOLEAN'){
					availableFields.add(singleField);
				}
			}
			Integer totalAmount=availableFields.size();
			System.debug('The total number is '+totalAmount);
			Integer columnAmount=totalAmount/6;
			Integer lastAmount=math.mod(totalAmount, 6);
			for(Integer i=0;i<columnAmount;i++){
				Integer currentAmount=i*6;
				list<FieldsClass.AvailableField> currentAvailableFields=new list<FieldsClass.AvailableField>();
				for(Integer j=0;j<6;j++){
					FieldsClass.AvailableField currentField=availableFields.get(currentAmount+j);
					currentAvailableFields.add(currentField);
				}
				fieldLists.add(currentAvailableFields);
			}
			list<FieldsClass.AvailableField> currentAvailableFields=new list<FieldsClass.AvailableField>();
			for(Integer i=0;i<lastAmount;i++){
				currentAvailableFields.add(availableFields.get(columnAmount*6+i));
			}
			fieldLists.add(currentAvailableFields);
		}
	public void preparedPicklistFieldAndOption(){
		
        SObjectType objToken = Schema.getGlobalDescribe().get('Contact'); //get Contact Object SobjectType
        DescribeSObjectResult objDef = objToken.getDescribe();  //get contact object description,such as getLabel=People;getLabelPlural=People;          
        Map<string, SObjectField> fields = objDef.fields.getMap();
        map<String, List <Schema.PicklistEntry>> picklistOptions=new map<String, List <Schema.PicklistEntry>>();
        map<String,String> labelValueJSON=new map<String,String>();
        
        for(Schema.SObjectField f : fields.values()){
        	 DescribeFieldResult fdr = f.getDescribe();
        	 String typeForField=String.valueOf(fdr.getType());
        	 if(typeForField.contains('PICKLIST')){
        	 	List <Schema.PicklistEntry> pickValList=fdr.getPicklistValues();
        	 	picklistOptions.put(fdr.getName(), pickValList);
        	 	labelValueJSON.put(fdr.getName(),fdr.getLabel());
        	 }
        }
        labelValueStr=JSON.serialize(labelValueJSON);
        optionJsonStr=JSON.serialize(picklistOptions);
	}
	
	@RemoteAction
	public static void saveRecords(String jsonStr,String applicationQuestionSetId){
		//System.debug('The json str is '+jsonStr);
		list<IndeedScreenQuestionFeed> indeedScreenQuestionFeed=(list<IndeedScreenQuestionFeed>)JSON.deserialize(jsonStr,list<IndeedScreenQuestionFeed>.class);
		//System.debug('applicationQuestionSetId '+applicationQuestionSetId);
		IndeedApplicationQuestionServices applicationQservice=new IndeedApplicationQuestionServices();
		applicationQservice.doAction(indeedScreenQuestionFeed,applicationQuestionSetId);
		
	}
	
}