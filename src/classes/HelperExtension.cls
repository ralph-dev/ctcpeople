public with sharing class HelperExtension{
    public HelperExtension(){
        
    }
    
    public HelperExtension(Object obj){
        
    }
    
    @RemoteAction
    public static String getNamespace(){
        return PeopleCloudHelper.getPackageNamespace();
    }

    //Get User permission
    @RemoteAction
    public static UserPermission getUserPermission() {
        return new UserPermission();
    }

    @RemoteAction
    public static Integer saveNumOfRecordsShowing(Integer numOfRecords) {
        return new UserServices().updateNumberOfRecordsShowing(numOfRecords);
    }

    /*
    @Author: Tom Lee 01.09.2016
    @Description: Return pickList Map<Label,Value>
    @Param: SObjectName
    @Param: PickList field Name
    @Return : Map <Label, Value>
    */
    @RemoteAction
    public static Map<String,String> getPicklistMapBySObjectAndFieldName(String objectName, String fieldName){
        return DescribeHelper.getPickListValueMap(objectName, fieldName);
    }
    
    @RemoteAction
    public static Map<String, RemoteAPIDescriptor.RemoteAction> apiDescriptor(){
        RemoteAPIDescriptor descriptor = new RemoteAPIDescriptor();
        descriptor.description('Helper remote actions.');
        descriptor.action('getNamespace');
        descriptor.action('getUserPermission');
        descriptor.action('saveNumOfRecordsShowing').param(Integer.class, 'numOfRecords', 'Number of Records Showing');
        descriptor.action('getPicklistMapBySObjectAndFieldName').param(String.class,'Text').param(String.class,'Text');
        return descriptor.paramTypesMap;
    }

}