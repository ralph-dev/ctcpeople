@isTest
public class LinkedInCSAProfileControllerTest {
    
    static testMethod void testLinkedInCSA() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Create data
        RecordType candRT = DaoRecordType.indCandidateRT;
        Contact con = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand1@test.ctc.test');
		insert con;
		LinkedIn_CSA__c csa = new LinkedIn_CSA__c();
        csa.API_Key__c = 'test';
        csa.Customer_Id__c = 'test';
        csa.Name = UserInfo.getOrganizationName();
        insert csa;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        LinkedInCSAProfileController controller = new LinkedInCSAProfileController(sc);
        system.assertEquals('test', controller.csa.API_Key__c);
		system.assertEquals('test', controller.csa.Customer_Id__c);
        system.assertEquals(con.Id, controller.con.Id);
    	}
    }
    
    static testMethod void testSaveLinkedInId() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Create data
        RecordType candRT = DaoRecordType.indCandidateRT;
        Contact con = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand1@test.ctc.test');
        insert con;

        String LinkedInId = '12345';
        LinkedInCSAProfileController.saveLinkedInId(linkedInId, con.Id);
        
        ContactSelector conSelector = new ContactSelector();
        Contact candidate = conSelector.getContactForLinkedIn(con.Id);
        system.assertEquals('12345', candidate.LinkedIn_Member_Id__c);
    	}
    }
    
}