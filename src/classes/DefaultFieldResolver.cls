public with sharing class DefaultFieldResolver extends FieldResolver{
    // Field lists that are linked to specific string identity 
    private Map<String, List<String>> fieldListById;

    public DefaultFieldResolver() {
        fieldListById = new Map<String, List<String>>();
    }

    public override List<String> getDisplayFieldsInStringList(String id) {
        return fieldListById.get(id);
    }

    public override List<Schema.DescribeFieldResult> getDisplayFields(String id) {
        return null;
    }
}