@isTest
public with sharing class CreateCustomerSettingTestData {
	public static JobPostingSettings__c createCustomerSettingTestData() {
		JobPostingSettings__c jobPostingSetting = new JobPostingSettings__c(SetupOwnerId = Userinfo.getuserId());
		jobPostingSetting.Posting_Seek_Ad_with_User_Email__c = true;
		insert jobPostingSetting;
		return jobPostingSetting;
	}
	
	public static JobPostingSettings__c createCustomerSettingTestDataUncheckEmailFunction() {
		JobPostingSettings__c jobPostingSetting = new JobPostingSettings__c(SetupOwnerId = Userinfo.getuserId());
		insert jobPostingSetting;
		return jobPostingSetting;
	}
}