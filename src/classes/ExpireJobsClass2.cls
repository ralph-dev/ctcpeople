public class ExpireJobsClass2{
// this class is used to expire jobs which reaches the closing date or >= 30days limit
// used for Careerone jobs.
// this class will be run by a scheduler daily 11:00 pm
// hardcode: yes, careerone ad. recordtype name

    Advertisement__c[] alljobs = new Advertisement__c[]{};
    public Map<Id,Advertisement__c> jobs_map_seek ; 
    public Map<Id,Advertisement__c> jobs_map_careerone ; 
    public Map<Id,Advertisement__c> jobs_map_trademe ; 
    public Map<Id, Advertisement__c> jobs_map_jxt;
    public Map<Id, Advertisement__c> jobs_map_jxtnz;
    public Map<Id, Advertisement__c> jobs_map_indeed;
    
    Map<Id,Advertisement__c> jobs_map ; 
    String[] Ad_ids = new String[]{}; //** this array is used as EC2webservice argument which stores records updated succesffully
    Set<Id> Ad_ids_fail = new Set<Id>(); 
    Set<Id> all_updated_ids = new Set<Id>();
    String orgid = String.valueOf(UserInfo.getOrganizationId()).substring(0,15);
    String website ;

	public ExpireJobsClass2(){
	    CareeroneAids();
	    jobs_map = jobs_map_careerone;
	    website = 'CareerOne';
	    checkExpire();
	   
	    SeekAids();
	    jobs_map  =null;
	    website = 'Seek.com';
	    jobs_map = jobs_map_seek; 
	    checkExpire();
	    
	    TrademeAids();
	    jobs_map = jobs_map_trademe;
	    website = 'Trademe.com.nz';
	    checkExpire();
	    
	    jxtAids();
	    jobs_map = jobs_map_jxt;
	    website = 'JXT';
	    checkExpire();
	    
	    jxtnzAids();
	    jobs_map = jobs_map_jxtnz;
	    website = 'JXT_NZ';
	    checkExpire();
	    
	    Date linkedInpostdate = System.Today().addDays(-29); 
		String linkedInrecordtypeid = (DaoRecordType.linkedInAdRT).id;
	    jobs_map = expiredjobadsmap(linkedInrecordtypeid, linkedInpostdate);
	    website = 'LinkedIn';
	    checkExpire();

	    Date indeedPostDate = System.Today().addDays(-119); 
	    String indeedRecordTypeId = (DaoRecordType.IndeedRT).id;
	    jobs_map = expiredjobadsmap(indeedRecordTypeId, indeedPostDate);
	    website = 'Indeed';
	    checkExpire();
	}
	public void CareeroneAids(){
	
	    //Date myDate = date.newinstance(2010, 4, 29);
	    //Date postdate = myDate.addDays(-28); 
	    Date postdate = System.Today().addDays(-28); 
	    //** for CareerOne.com 30 day limit
	    //** apex scheduler will run daily night 11pm to expire jobs on -28th.
	    //** if placement_Date__c = null, that means this job has never been posted online far so far.  
	    //** criteria : careerone jobs, have not expired in sf.com
	    //**            posting date = Today().addDays(-28) if today=2010/04/29, posting date = 2010/04/01
	    //**            closing date = today() if user fill in the closing date which is less than 30 day limit. 
		CommonSelector.checkRead(Advertisement__c.SObjectType,'Id,Vacancy__c,Vacancy__r.Name, Status__c,Ad_Closing_Date__c,Placement_Date__c, Owner.Name');
	    jobs_map_careerone  = new Map<ID, Advertisement__c>([select Id,Vacancy__c,Vacancy__r.Name, Status__c, 
	                Ad_Closing_Date__c, 
	                Placement_Date__c, Owner.Name  
	                from Advertisement__c
	                where Status__c <>'Expired' and RecordType.DeveloperName like '%careerone%' 
	                and (Placement_Date__c <=: postdate or MyCareer_Closing_Date__c <=: system.today()) 
	                limit 1000 ]);
	}
	public void TrademeAids(){
	    Date postdate = System.Today().addDays(-28); 
		CommonSelector.checkRead(Advertisement__c.SObjectType,'Id,Vacancy__c,Vacancy__r.Name, Status__c,Ad_Closing_Date__c,Placement_Date__c, Owner.Name');
	    jobs_map_trademe  = new Map<ID, Advertisement__c>([select Id,Vacancy__c,Vacancy__r.Name, Status__c, 
	                Ad_Closing_Date__c, 
	                Placement_Date__c, Owner.Name  
	                from Advertisement__c
	                where Status__c <>'Expired' and RecordType.DeveloperName like '%trade_me%' 
	                and (Placement_Date__c <=: postdate or MyCareer_Closing_Date__c <=: system.today()) 
	                limit 1000 ]);
	}
	
	public static Map<ID, Advertisement__c> expiredjobadsmap(String recordtypeid, Date postdate){
		Map<Id, Advertisement__c> jobs_map_linkedin = new Map<Id, Advertisement__c>();
		CommonSelector.checkRead(Advertisement__c.SObjectType,'Id,Vacancy__c,Vacancy__r.Name, Status__c,Ad_Closing_Date__c, Placement_Date__c, Owner.Name');
		jobs_map_linkedin = new Map<ID, Advertisement__c>([select Id,Vacancy__c,Vacancy__r.Name, Status__c, 
	                Ad_Closing_Date__c, Placement_Date__c, Owner.Name from Advertisement__c
	                where (Status__c = 'Active') and RecordTypeId =: recordtypeid 
	                and (Placement_Date__c <=: postdate or MyCareer_Closing_Date__c <=: system.today()) 
	                limit 1000 ]);
		return jobs_map_linkedin;
	}
	
	public void jxtAids(){
		Date postdate = System.Today().addDays(-28); 
		CommonSelector.checkRead(Advertisement__c.SObjectType,'Id,Vacancy__c,Vacancy__r.Name, Status__c,Ad_Closing_Date__c, Placement_Date__c, Owner.Name');
	    jobs_map_jxt  = new Map<ID, Advertisement__c>([select Id,Vacancy__c,Vacancy__r.Name, Status__c, 
	                Ad_Closing_Date__c, 
	                Placement_Date__c, Owner.Name  
	                from Advertisement__c
	                where Status__c <>'Expired' and RecordType.DeveloperName = 'JXT' 
	                and (Placement_Date__c <=: postdate or MyCareer_Closing_Date__c <=: system.today()) 
	                limit 1000 ]);
	}
	
	public void jxtnzAids(){
		Date postdate = System.Today().addDays(-28); 
		CommonSelector.checkRead(Advertisement__c.SObjectType,'Id,Vacancy__c,Vacancy__r.Name, Status__c,Ad_Closing_Date__c, Placement_Date__c, Owner.Name');
	    jobs_map_jxtnz  = new Map<ID, Advertisement__c>([select Id,Vacancy__c,Vacancy__r.Name, Status__c, 
	                Ad_Closing_Date__c, 
	                Placement_Date__c, Owner.Name  
	                from Advertisement__c
	                where Status__c <>'Expired' and RecordType.DeveloperName = 'JXT_NZ' 
	                and (Placement_Date__c <=: postdate or MyCareer_Closing_Date__c <=: system.today()) 
	                limit 1000 ]);
	}
	
	public void SeekAids(){
	    Date postdate = System.Today().addDays(-28); 
	    //** for Seek.com 30 day limit
		CommonSelector.checkRead(Advertisement__c.SObjectType,'Id,Vacancy__c,Vacancy__r.Name, Status__c,Ad_Closing_Date__c, Placement_Date__c, Owner.Name');
	    jobs_map_seek = new Map<ID, Advertisement__c>([select Id,Vacancy__c,Vacancy__r.Name, Status__c, 
	                Ad_Closing_Date__c, 
	                Placement_Date__c, Owner.Name  
	                from Advertisement__c
	                where Status__c <>'Expired' and RecordType.DeveloperName like '%seek%' 
	                and (Placement_Date__c <=: postdate or MyCareer_Closing_Date__c <=: system.today()) 
	                limit 1000 ]);
	}
	public Boolean checkExpire(){
		
	    alljobs.clear();
	    try{
	        for(Advertisement__c a : jobs_map.values())
	        {
	        	a.Status__c = 'Expired';
	            alljobs.add(a);
	        }
	        
	        if(alljobs==null || alljobs.size()==0) return true;
	        // to get expire notification setting
	        PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
	        if(alljobs.size()>0)
	        {
	        	fflib_SecurityUtils.checkFieldIsUpdateable(Advertisement__c.SObjectType, Advertisement__c.Status__c);
	            Database.SaveResult[] srs = Database.update(alljobs);
	            //** send notification email to peoplecloud admin if disable ads. expire notification is off
				CommonSelector.checkRead(User.SObjectType,'Id, Email, Name');
	            User[] AdminUsers = [select Id, Email, Name from User where isActive = true and View_CTC_Admin__c = true];
	            if(AdminUsers != null && cs != null && !cs.Disable_Notification_of_Expired_Ads__c)
	            {   
	                String[] toAddresses = new String[]{};
	                for(User u:AdminUsers)
	                {
	                    toAddresses.add(u.Email);
	                }
	                String body = 'Dear User,<br/><br/>';
	                body = body + 'Those '+website+' advertisments have been expired. Please check below.<br/><br/>';
	                body = body + '<table><tr><th>SF.com Vacancy</th><th>Ad. Title</th><th>Ad. Owner</th><th>Posting Date</th><th>Closing Date</th></tr>';
	
	                Id thejobid;
	                Advertisement__c ad ;
	                for(Database.SaveResult sr : srs ){
	                    if(sr.isSuccess())
	                    {
	                        thejobid = sr.getID();
	                        Ad_ids.add(String.valueOf(thejobid).substring(0,15));
	                        ad = jobs_map.get(thejobid);
	                        all_updated_ids.add(sr.getID()); // id set
	                        if(ad != null)
	                        {
	                            
	                            body = body + '<tr><td>'+ad.Vacancy__r.Name+'</td><td>'+ad.Job_Title__c+'</td><td>'+ad.Owner.Name+'</td><td>'+ad.Placement_Date__c+'</td><td>'+ad.Ad_Closing_Date__c+'</td></tr>';
	                        }
	                    }
	                    
	                }//for
	                
	
	                body = body + '</table><br/><br/>';
	                
	                if(all_updated_ids.size() <> jobs_map.keySet().size())
	                {
	                    Ad_ids_fail.clear();
	                    Ad_ids_fail.addAll(jobs_map.keySet());
	                    Ad_ids_fail.removeAll(all_updated_ids);
	                
	                }
	                if(Ad_ids_fail.size()>0)
	                {
	                     body = body + '<font color="red">-----Some advertisements below could NOT be expired successfully by system.-----</font><br/>';
	                     body = body + Ad_ids_fail + '<br/>---------------------------------------------------<br/>';
	                }
	                body = body + 'System Admin<br/><br/>Powered by ClicktoCloud.com<br/>';
	                body = body + system.now().format();
	
	                if(toAddresses.size()>0)//&& (limits.getLimitEmailInvocations()-limits.getEmailInvocations())>toAddresses.size()
	                {
	                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	                    
	                    mail.setToAddresses(toAddresses);
	                    mail.setSubject('Your Record(s) Updates For !');
	                    mail.setHtmlBody(body);
	                    if(!Test.isRunningTest()){
	                    	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	                    }else{
	                    	//system.debug(mail);
	                    }
	                }
	            }           
	            
	        }
	         return true;
	    }
	    catch(system.Exception e){
	    	//** scheduler error email will be sent to log@clicktocloud.com, shawn@sqwarepeg.com
	        String[] toAddresses = new String[]{'log@clicktocloud.com','shawn@sqwarepeg.com'};
	        Messaging.SingleEmailMessage errormail = new Messaging.SingleEmailMessage();
	       	errormail.setToAddresses(toAddresses);
	        errormail.setSubject('Scheduler Error for company:' +UserInfo.getOrganizationName()+ '!');
	        String body = 'Hi CTC Support Team, <br/> Error happened for instance -' +orgid+ '. ApexClass: ExpireJobsClass2 <br/> ';
	        body = body + 'Error: ' + e.getMessage() + '<br/><br/> Regards!<br/>' + system.now().format();
	        errormail.setHtmlBody(body);
	                
	        if(!Test.isRunningTest()){
	        	try{
	            	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { errormail });
	            }catch(Exception ex){
	            	//system.debug('errormail ='+ errormail + '. Exception ='+ ex);
	            }
	        }else{
	            //system.debug(errormail);
	        }
	        //system.debug(e);
	    }
	    
	    return false;
	}

	
	@future(callout = true)
		static void CallEC2(String orgid, List<String> adids) {
		    CTCWS.PeopleCloudWSDaoPort EC2ws = new CTCWS.PeopleCloudWSDaoPort();
		    integer calloutres = EC2ws.deleteJobFromDetailByAID(orgid, adids); 
		}
	}