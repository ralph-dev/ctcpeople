public class JXTPostingCallWebService {
    
    public static final String ORGNISATION_ID_15 = UserInfo.getOrganizationId().subString(0, 15);
    
    public static void callJxtFeedWebService(Advertisement__c ad, String excutionType) {
        

        JXTAd jxtAd = new JXTAd();
        
        if(ad.JobXML__c!=null) {
            jxtAd.jobContent = ad.JobXML__c;
        }
        
	    AdvertisementConfigurationSelector acs = new AdvertisementConfigurationSelector();
        //commented by andy for security review II
        //Advertisement_Configuration__c advConfig = acs.getJXTAccount();
        
        //added by andy for security review II
        ProtectedAccount pa = acs.getJXTProtectedAccount();
        Advertisement_Configuration__c advConfig = pa!=null ? (Advertisement_Configuration__c) pa.account:null;
	    //commented by andy for security review II
	    //jxtAd.userName = advConfig!=null ? advConfig.UserName__c : '';
	    //jxtAd.Password = advConfig!=null ? advConfig.Password__c : '';
	    
	    //added by andy for security review II
	    jxtAd.userName = advConfig!=null ? pa.getUsername() : '';
	    jxtAd.Password = advConfig!=null ? pa.getPassword() : '';
	    
	    jxtAd.advertiserId = advConfig!=null ? advConfig.Advertisement_Account__c : '';
	    
	    jxtAd.orgId = ORGNISATION_ID_15;
	    jxtAd.referenceNo = ORGNISATION_ID_15 + ':' + ad.id;
	    jxtAd.jobBoard = 'JXT';
	    jxtAd.jobBoardSfApiName='JXT__c';
	    jxtAd.namespace = PeopleCloudHelper.getPackageNamespace();
	    jxtAd.postingStatus = ad.Job_Posting_Status__c;
	    
	    //serialize JXTAds to json object
        String jsonString = JSON.serialize(jxtAd);

        //remove all unsupported characters
        while(!jsonString.isAsciiPrintable()){
            String sub = findchar(jsonString);
            jsonString = jsonString.replaceAll(sub, '');
        }

        
        //encode session id
        String sessionId = UserInfo.getSessionId();
        if(sessionId != null){
        	sessionId = EncodingUtil.urlEncode(UserInfo.getSessionId(), 'UTF-8');
        } else{
        	sessionId = '';
        }
        //build and encode url
        String surl = EncodingUtil.urlEncode(URL.getSalesforceBaseUrl().toExternalForm() +'/services/Soap/u/25.0/'+UserInfo.getOrganizationId(),'UTF-8');
       
        //invoke web service
        if(! Test.isRunningTest()){
            if(excutionType.equalsIgnoreCase('Insert') || excutionType.equalsIgnoreCase('Update')) {
                postingAds(jsonString, sessionId, surl);
            } else if(excutionType.equalsIgnoreCase('Archive')) {
                ArchiveAds(jsonString, sessionId, surl);
            } 
        	
        }
       
	}

    //return the unsupport string
    private static String findChar(String str){
        str = str.escapeUnicode();
        Integer start = str.indexOf('\\u');
        String sub = str.substring(start, start+6);
        return sub;      
    }   
	
	 @future(callout=true) 
     public static void postingAds(String jsonString, String sessionid,String surl){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        String searchEndpoint = EndPoint.getJXTDefaultEndpoint()+'/jxtfeedweb/job/postJsonJobAd';
       
        req.setEndpoint(searchEndpoint);
        req.setMethod('POST');
        //Commented by Ralph, no longer use sessionId        
        //req.setHeader('salesforcesession', sessionid);
        req.setHeader('surl', surl);
        //req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setBody(jsonString); 
        HttpResponse res = new HttpResponse();
        if(!Test.isRunningTest()) {
            res = h.send(req);
        }else {
            res.setStatusCode(200);
            res.setBody('Successfully');
        }          
        Integer responsecode = res.getStatusCode();
    }
    
    @future(callout=true) 
     public static void ArchiveAds(String jsonString, String sessionid,String surl){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        String searchEndpoint = EndPoint.getJXTDefaultEndpoint()+'/jxtfeedweb/job/archiveJobAd';
       
        req.setEndpoint(searchEndpoint);
        req.setMethod('POST');
        //Commented by Ralph, no longer use sessionId        
        //req.setHeader('salesforcesession', sessionid);
        req.setHeader('surl', surl);
        //req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(120000);
        req.setBody(jsonString); 
        HttpResponse res = new HttpResponse();
        if(!Test.isRunningTest()) {
            res = h.send(req);
        }else {
            res.setStatusCode(200);
            res.setBody('Successfully');
        }          
        Integer responsecode = res.getStatusCode();
    }

}