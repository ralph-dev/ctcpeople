public with sharing class AdvertisementHandler implements ITrigger {
    
    public List<Advertisement__c> adsNeedArchiveList;
    public List<Advertisement__c> adsPostFailed;

	// Constructor
    public AdvertisementHandler() {}

    public void bulkBefore() {
        adsNeedArchiveList = new List<Advertisement__c>();
        adsPostFailed = new List<Advertisement__c>();
    }

    public void bulkAfter() {}

    public void beforeInsert(SObject so) {}

    public void beforeUpdate(SObject oldSo, SObject so) {
        Advertisement__c adOld = (Advertisement__c) oldSo;
        Advertisement__c adNew = (Advertisement__c) so;

        if(adNew.JXT_Auto_Archive__c && !adOld.JXT_Auto_Archive__c) {
            adsNeedArchiveList.add(adNew);
            adNew.JXT_Auto_Archive__c = false;
            adNew.Status__c = 'Expired';
        }

        // If posting failed, need to add back job posting quota
        if (String.isNotEmpty(adNew.Job_Posting_Status__c) &&
                adNew.Job_Posting_Status__c.equalsIgnoreCase('Posting Failed')) {
            adsPostFailed.add(adNew);
        }
    } 
    
    public void beforeDelete(SObject so) {}

    public void afterInsert(SObject so) {}

    public void afterUpdate(SObject oldSo, SObject so) {}

    public void afterDelete(SObject so) {}
    
    public void afterUndelete(SObject so){} 

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
        if (adsNeedArchiveList != null && adsNeedArchiveList.size() > 0) {
            AdvertisementHandlerService.archiveAds(adsNeedArchiveList);
        }
        if (adsPostFailed != null && adsPostFailed.size() > 0) {
            AdvertisementHandlerService.updateUserUsage(adsPostFailed);
        }
    }
}