global with sharing class SearchExtensionGlobal {
	private static Integer MAX_FIELDS_LIST_SIZE = 6;
    private static String OPERATOR_JSON_STRING  = '[{"active": true,"defaultValue": true,"label": "Equals","validFor": null,"value": "eq"},'
        + '{"active": true,"defaultValue": false,"label": "Not equal to","validFor": null,"value": "ne"},'
        + '{"active": true,"defaultValue": false,"label": "Includes","validFor": null,"value": "inc"},'
        + '{"active": true,"defaultValue": false,"label": "Excludes","validFor": null,"value": "exc"},'
        + '{"active": true,"defaultValue": false,"label": "Is","validFor": null,"value": "in"},'
        + '{"active": true,"defaultValue": false,"label": "Is Not","validFor": null,"value": "notIn"},'
        + '{"active": true,"defaultValue": false,"label": "Less than","validFor": null,"value": "lt"},'
        + '{"active": true,"defaultValue": false,"label": "Greater than","validFor": null,"value": "gt"},'
        + '{"active": true,"defaultValue": false,"label": "Less or equal to","validFor": null,"value": "le"},'
        + '{"active": true,"defaultValue": false,"label": "Greater or equal to","validFor": null,"value": "ge"},'
        + '{"active": true,"defaultValue": false,"label": "Contains","validFor": null,"value": "like"},'
        + '{"active": true,"defaultValue": false,"label": "Not Contain","validFor": null,"value": "notLike"},'
        + '{"active": true,"defaultValue": false,"label": "Starts with","validFor": null,"value": "stLike"},'
        + '{"active": true,"defaultValue": false,"label": "Ends with","validFor": null,"value": "endLike"},'
        + '{"active": true,"defaultValue": false,"label": "Is Empty","validFor": null,"value": "ie"},'
        + '{"active": true,"defaultValue": false,"label": "Is Not Empty","validFor": null,"value": "ine"}]';
    private static String NAME_SPACE_PREFIX = PeopleCloudHelper.getPackageNamespace();
    global SearchExtensionGlobal(Object obj){
    }

    global SearchExtensionGlobal(){
    }
    //get record type list by objectName
    @RemoteAction
    global static List<RecordType> fetchRecordType(String objectName) {
        List<RecordType> recordTypeList = new List<RecordType>();
        RecordTypeServices rTypeServices = new RecordTypeServices();
        rTypeServices.setObjectName(objectName);
        recordTypeList = rTypeServices.getRecordTypeList();
        return recordTypeList;
    }

    @RemoteAction
    global static List<Schema.DescribeFieldResult> selectField() {
        List<Schema.DescribeFieldResult> searchFieldsList =  new List<Schema.DescribeFieldResult>();

        List<Schema.FieldSetMember> fieldsList = 
                Schema.SObjectType.Contact.fieldSets.getMap().get(NAME_SPACE_PREFIX+'AdvancedPeopleSearchFields').getFields();
        List<String> fieldsAPIName = new List<String>();

        for(Schema.FieldSetMember sFieldSetMember : fieldsList) {
            if(sFieldSetMember.getType().name()!='TEXTAREA'&&sFieldSetMember.getType().name()!='Reference'&&fieldsAPIName.size()<MAX_FIELDS_LIST_SIZE) {
                fieldsAPIName.add(sFieldSetMember.getFieldPath());
            }
        }

        if(fieldsAPIName==null || fieldsAPIName.size()<1) {
            fieldsAPIName.add('Name');
            fieldsAPIName.add('RecordTypeId');
        }
        Map<String, Schema.DescribeFieldResult> searchFieldsMap = DescribeHelper.getDescribeSelectFieldResult(Contact.sObjectType.getDescribe().fields.getMap().values(), fieldsAPIName);
        
        searchFieldsList = searchFieldsMap.values();
        return searchFieldsList;
    }

    @RemoteAction
    global static List<SearchOperator> operator() {
        List<SearchOperator> sOperatorList = new List<SearchOperator>();
        sOperatorList = (List<SearchOperator>)System.JSON.deserialize(OPERATOR_JSON_STRING, List<SearchOperator>.class);
        return sOperatorList;
    }
    @RemoteAction
    global static List<candidateDTO> candidatesJSON(List<String> paramList) {

        String shiftsJson = paramList[0];
        String fieldsOperatorJson = paramList[1];
        String fieldsJson = paramList[2]; 
        String fieldsTypeJson = paramList[3];  
        String skillsJson = paramList[4]; 
        String radiusRangeString = paramList[5]; 
        String locationLatitudeString = paramList[6]; 
        String locaitonLongitudeString = paramList[7]; 
    	
        List<ShiftDTO> shiftObjs = new List<ShiftDTO>();
    	Map<String, String> fieldsOperator = new Map<String, String>();
        Map<String, String> fields = new Map<String, String>();
        Map<String, String> skillsMap = new Map<String, String>();
        List<candidateDTO> candidateDTOList = new List<candidateDTO>();
        Map<String, String> fieldsType = new Map<String, String>();
        Map<String, candidateDTO> candidateSearchResultMap = new Map<String, candidateDTO>();    	

        Integer randiusRange = Integer.valueOf(radiusRangeString);
        double locationLatitude = double.valueOf(locationLatitudeString);
        double locaitonLongitude = double.valueOf(locaitonLongitudeString);

        try{
            if(shiftsJson!='')
    		  shiftObjs = (List<ShiftDTO>)JSON.deserialize(shiftsJson,List<ShiftDTO>.class);

            if(fieldsOperatorJson!='')  
                fieldsOperator = (Map<String,String>)JSON.deserialize(fieldsOperatorJson, Map<String, String>.class);
            
            if(fieldsJson!='')  
                fields = (Map<String,String>)JSON.deserialize(fieldsJson, Map<String, String>.class);
            
            if(skillsJson!='')
                skillsMap = (Map<String,String>)JSON.deserialize(skillsJson, Map<String, String>.class);
            
            if(fieldsTypeJson!='')
                fieldsType = (Map<String,String>)JSON.deserialize(fieldsTypeJson, Map<String, String>.class);

            //system.debug('list ='+ shiftObjs + ';'+ fieldsOperator + ';'+ fields+ ';'+ skillsMap+ ';'+ fieldsType+ ';'+ randiusRange+ ';'+ locationLatitude+ ';'+locaitonLongitude);
            ContactServices cServices = new ContactServices();
            
            candidateDTOList = cServices.getCandidateByCriteria(fieldsOperator, fields, fieldsType, skillsMap, shiftObjs, randiusRange, locationLatitude, locaitonLongitude);

            candidateDTOList.sort();
        }catch(Exception e) {
             NotificationClass.notifyErr2Dev('search error', e);
        }

    	return candidateDTOList;
    }

    //Create Rostering Records
    @RemoteAction
    global static void createCMAndRostering(List<String> paramList){
        String shiftsJson = paramList[0];
        String candidateDTOListJSON = paramList[1];
        String vacancyId = paramList[2];

        List<shift__c> shiftsList = new List<shift__c>();
        List<CandidateDTO> cDTOList = new List<CandidateDTO>();
        
        shiftsList = (List<Shift__c>)JSON.deserialize(shiftsJson,List<Shift__c>.class);
        cDTOList = (List<candidateDTO>)JSON.deserialize(candidateDTOListJSON, List<candidateDTO>.class);
        
        RosterDomain rDomain = new RosterDomain();
        rDomain.createRosteringRecords(vacancyId, shiftsList, cDTOList);
    }

    //Get PCAppSwitch Record
    @RemoteAction
    global static PCAppSwitch__c getCTCAppSwitch() {
        PCAppSwitch__c pcAppSwitch = PCAppSwitch__c.getInstance();
        return pcAppSwitch;
    }
}