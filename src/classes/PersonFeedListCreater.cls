public with sharing class PersonFeedListCreater {
	public static List<PersonFeed> PERSONFEEDLIST;
	public static List<Web_Document__c> webDocList;
	private static List<Id> contactIdList;
    private static String orgId;
	
	// Possible ACTION  
	public enum Actions {ADD, MODIFY, REMOVE} 
	//private Actions action;
	
	public PersonFeedListCreater() {
		
	}

	static{
		if(PERSONFEEDLIST == null) {
			PERSONFEEDLIST = new List<PersonFeed>();
		}
		if (webDocList == null) {
			webDocList = new List<Web_Document__c>();
		}
		if(String.isBlank(orgId)) {
		    orgId = String.valueOf(UserInfo.getOrganizationId()).substring(0, 15);
		}
	}

	public static void addPersonFeedToList(Web_Document__c webDoc, Map<Id, Web_Document__c> candResumeMap, Map<Id, String> candLastNameMap) {
	    PersonFeed feed = new PersonFeed();
	    // If candidate has existing resume
	    if (candResumeMap.get(webDoc.Document_Related_To__c) != null) {
	        feed = createPersonFeed(webDoc, Actions.MODIFY);
        // If candidate does not have existing resume
	    } else {
	        feed = createPersonFeed(webDoc, Actions.ADD);
	    }
	    feed.lastName = candLastNameMap.get(webDoc.Document_Related_To__c);
		PERSONFEEDLIST.add(feed);
	}
	
	// For after delete trigger
	public static void addDeletePersonFeedToList(Web_Document__c webDoc, map<Id, Web_Document__c> candResumeMap, Map<Id, String> candLastNameMap) {
	    PersonFeed feed = new PersonFeed();
	    // If candidate has existing resume
	    if (candResumeMap.get(webDoc.Document_Related_To__c) != null) {
	        feed = createPersonFeed(candResumeMap.get(webDoc.Document_Related_To__c), Actions.MODIFY);
	        feed.lastName = candLastNameMap.get(webDoc.Document_Related_To__c);
	        webDocList.add(candResumeMap.get(webDoc.Document_Related_To__c));
        // If candidate does not have existing resume
	    } else {
	        feed = createPersonFeed(webDoc, Actions.REMOVE);
	    }
		PERSONFEEDLIST.add(feed);
	}
	
	// For delete candidate trigger 
	public static List<PersonFeed> getDeleteCandidatePersonFeedList(List<Contact> conList) {
	    List<PersonFeed> feedList = new List<PersonFeed>();
	    for (Contact con : conList) {
	        PersonFeed feed = new PersonFeed();
	        feed.orgId = orgId;
    	    feed.candidateId = con.Id;
    	    feed.action = 'Delete';
    	    feedList.add(feed);
	    }
	    return feedList;
	}
	
	public static PersonFeed createPersonFeed(Web_Document__c webDoc, Actions action) {
	    PersonFeed feed = new PersonFeed();
	    feed.orgId = orgId;
    	feed.candidateId = webDoc.Document_Related_To__c;
		if (action == Actions.ADD) {
			feed.action = 'Add';
    		feed.resumeBucket = webDoc.S3_Folder__c;
    		feed.resumeName = webDoc.ObjectKey__c;
		}else if(action == Actions.MODIFY) {
		    feed.action = 'Update';
		    feed.resumeBucket = webDoc.S3_Folder__c;
    		feed.resumeName = webDoc.ObjectKey__c;
		}else{
		    feed.action = 'Delete';
		}
		return feed;
	}
}