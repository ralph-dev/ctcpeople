/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

@isTest
public with sharing class TestGoogleSearchValidate {
	/*static testmethod void testcheckUserPerformanceValidate(){
		System.runAs(DummyRecordCreator.platformUser) {
		User_Preference__c tempuserperformance = GoogleSearchValidate.checkUserPerformanceValidate();
		system.assertnotEquals(tempuserperformance, null);
		}
	}
	
	static testmethod void testupdatecheckUserPerformanceValidate(){
		System.runAs(DummyRecordCreator.platformUser) {
		User_Preference__c tempuserperformance = preparedata();
		User_Preference__c Secondtempuserperformance = GoogleSearchValidate.checkUserPerformanceValidate();
		system.assertEquals(tempuserperformance, tempuserperformance);
		tempuserperformance.Google_Search_Column__c = '';
		update tempuserperformance;
		Secondtempuserperformance = GoogleSearchValidate.checkUserPerformanceValidate();
		system.assertEquals(tempuserperformance, tempuserperformance);
		}
	}
	
	private static User_Preference__c preparedata(){
		User_Preference__c currentGoogleSearchPerformance = new User_Preference__c();
		String Userid = Userinfo.getUserId();
		String RecordTypeId = DaoRecordType.googleSearchUserPerformacne.id;
		String GoogleSearchUserPreferenceName = 'GoogleSearchUserPreferenceName_'+Userid;
		try{
			currentGoogleSearchPerformance = [select Id, Name, Google_Search_Column__c, Google_Search_View__c, 
												Number_of_Records_Per_Page__c from User_Preference__c where RelatedTo__c =: Userid and Recordtypeid =:RecordTypeId and Unique_User_Preference_Name__c =:GoogleSearchUserPreferenceName limit 1];
		}catch(Exception e){
			String GoogleSearchColumn = 'abcdef';
			currentGoogleSearchPerformance.Name = GoogleSearchUserPreferenceName;
			currentGoogleSearchPerformance.Google_Search_Column__c = GoogleSearchColumn;
			currentGoogleSearchPerformance.RelatedTo__c = Userid;
			currentGoogleSearchPerformance.Unique_User_Preference_Name__c = GoogleSearchUserPreferenceName;
			currentGoogleSearchPerformance.RecordTypeId = RecordTypeId;
			insert currentGoogleSearchPerformance;
		}
		currentGoogleSearchPerformance.Google_Search_Column__c = 'abcdef';
		update currentGoogleSearchPerformance;
		return currentGoogleSearchPerformance;
	}*/
}