/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RecordTypeSelectorTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	List<String> rtNames=new List<String>();
    	rtNames.add('Independent_Candidate');
    	rtNames.add('Client_Contact');
    	RecordTypeSelector rTSelector=new RecordTypeSelector();
    	List<RecordType> recordTypes=rTSelector.fetchRecordTypesByName(rtNames);
    	System.assertNotEquals(recordTypes,null);
    	String rTName='Independent_Candidate';
    	List<RecordType> recordTypes2=rTSelector.fetchRecordTypesByName(rTName);
    	System.assertNotEquals(recordTypes2,null);
    	List<RecordType> recordTypes3 = rTSelector.fetchRecordTypeList();
    	System.assertNotEquals(recordTypes3,null);
        List<RecordType> recordTypes4 = rTSelector.fetchRecordTypesByObjectName('contact');
        System.assertNotEquals(recordTypes4,null);
    	}
    }
}