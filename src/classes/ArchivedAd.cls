public with sharing class ArchivedAd {
	public Advertisement__c ad{get; set;}
	private ApexPages.StandardController stdCon;
	public ArchivedAd(ApexPages.StandardController stdController){
		String id = stdController.getId();
		stdCon = stdController;
		AdvertisementSelector adSelector = new AdvertisementSelector();
		ad = adSelector.getVacIdByAdId(id);
		
	}


	public PageReference archive(){
		if(ad.Status__c== 'Active' ){
			if(ad.Job_Posting_Status__c == null || ad.Job_Posting_Status__c == 'active' || ad.Job_Posting_Status__c == 'expired' || ad.Job_Posting_Status__c=='Posted Successfully' || ad.Job_Posting_Status__c=='Updated Successfully'){
				ad.Status__c = 'Archived';
			}else{
				if(ad.Job_Posting_Status__c == 'In Queue'){
					ad.Status__c = 'Recalled';
				}
			}
		} else{
			if(ad.Status__c== 'Recalled'){
				ad.Status__c = 'Active';
			}
			
		}
		fflib_SecurityUtils.checkFieldIsUpdateable(Advertisement__c.SObjectType, Advertisement__c.Status__c);
		update ad;
		
		//Call web service to archive/recall or repost
		if(ad.RecordTypeId == DaoRecordType.JXTAdRT.Id){
			if(ad.Status__c== 'Active' ) {
				if(ad.Job_Posting_Status__c != 'In Queue') {
		        //Repost or update ad
		        JXTPostingCallWebService.callJxtFeedWebService(ad, 'Update');
		    }
		    } else if(ad.Status__c == 'Archived' || ad.Status__c == 'Recalled') {
		    	if(ad.Job_Posting_Status__c != 'In Queue') {
		        //Archive the ad
		        	JXTPostingCallWebService.callJxtFeedWebService(ad, 'Archive');
		    	}
			}
		}

		
		return new ApexPages.Standardcontroller(ad).view();
	}
	
	public PageReference cancelArchive(){
		return stdCon.view();
	}
	
	
}