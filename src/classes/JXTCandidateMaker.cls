/**
    * JXTCandidateMaker is a implementation of IEmailHandler interface, which can parse candidate,
	* candidate resume from incoming email, create candidate, candidate management, activity and attachemt.
	* 
	* Each JXTCandidateMaker MUST have a reference of ICVEmailParser to execute parsing email content.
    * 
    * @author: Lorena
    * @createdDate: 6/10/2016
    * @lastModify: 
    */
public with sharing class JXTCandidateMaker implements ICVEmailHandler{
	
	private static final String TASK_STATUS='Completed';
	private static final String TASK_PRIORITY = 'Normal';
	
	//parser for enquiry email
	private ICVEmailParser parser;
	
	private Messaging.InboundEmail email;
	
	public JXTCandidateMaker() { 
		
	}
	
	
	public JXTCandidateMaker(ICVEmailParser parser) {
		this();
		this.parser = parser;
	}
	
	public void setEmail(Messaging.InboundEmail srcEmail){
		this.email = srcEmail;	
	}
	
	public Messaging.InboundEmail getEmail(){
		return this.email;
	}
	
	//execute handle processing
	public void handle(){
		if(this.parser != null){
			
			//invoke make method of this object
			this.make();
		}
	}
	
	public void make() {
		
		//parse candidate email to JXTContent object for next processing
		JXTContent content = parser.parseJxtEmail();
		
		if(adOrVacNotExist(content) ) {
		    system.debug('Ad or Vacancy does not exist, do not process');
		} else {
		
        	//create candidate record
        	Contact candidate = createCandidate(content);
        	
        	Task t;
        	
        	List<Attachment> attachments;
        	
        	if(candidate!=null) {
        	
            	//create a activity/task related to incoming candidate
            	t = createTask(content, getEmail(), candidate.Id);
            	
            	if(t != null ){
            	    //create an attachment related to current activity/task;
        		    attachments = createAttachments(t, content);
        		    
        		    //create candidate management
            	    Placement_Candidate__c cm = createCm(content, candidate.Id);
            	}
        	}
        	
        	if(attachments != null && attachments.size() >0 ){
    			//call webservice to parse resume
    			try{
    				callWebService(t, attachments, content.skillGroupString);
    			}catch(Exception e){
    				ActivityDocOperator.updateActivityStatus('Error','Failed to call web service', t.id);
    			}
    		}else{
    			ActivityDocOperator.updateActivityStatus('Error','Failed to create attachment ['+content.files+']', t.id);
    		}	        	
		}
	}
	
	//If the adId or vacancyId is not valid in salesforce, send error email to CTC without processing
	public Boolean adOrVacNotExist(JXTContent content) {
	    if(content.adId==null ||  content.adId=='') {
	        String errorMsg = 'the advertisement is not valid:';
	        if(!Test.isRunningTest()) {
	            sendErrorEmail(content, errorMsg);
	        }
	        return true;
	    } else if(content.vacId==null ||  content.vacId=='') {
	        String errorMsg = 'the advertisement\' vacancy is not valid:';
	        if(!Test.isRunningTest()) {
	            sendErrorEmail(content, errorMsg);
	        }
	        return true;
	    } else {
	        return false;
	    }
	}
	
	private void sendErrorEmail(JXTContent content, String errorMsg) {
	    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] { 'log@clicktocloud.com'};
        message.subject = 'JXTEmailService Error Notification: Failed to import candidate by CTC Email Service';
        message.plainTextBody = 'Candidate failed to be imported because ' + errorMsg + '\n' + content ;
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results;
        if(!Test.isRunningTest()) {
            results = Messaging.sendEmail(messages);
        } 
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
	}
	
	private Contact createCandidate(JXTContent content) {
	    RecordTypeSelector rs = new RecordTypeSelector();
	    String rt = rs.fetchRecordTypesByName('Independent_Candidate').get(0).Id;
	    String condition;
	    
	    //Check if the incoming email contain firstName, lastName and email. If not, send email to log@clicktocloud.com
	    if(content.firstName!=null && content.lastName!=null && content.emailAddress!=null) {
	        condition = 'firstName=\'' + String.escapeSingleQuotes(content.firstName) + '\' AND lastName=\'' + String.escapeSingleQuotes(content.lastName) + '\' AND email=\'' + String.escapeSingleQuotes(content.emailAddress) + '\' ';
	    } else {
	        String errorMsg = 'the candidate info is not valid:';
	        sendErrorEmail(content, errorMsg);
	        return null;
	    }
	    
	    //Select candidate by firstName, lastName, email and recordType
	    CandidateSelector canSelector = new CandidateSelector();
	    List<Contact> cands = canSelector.selectCandidatesByCriteria( condition );
	    
	    //Create new candidate only when no existing candidate.
	    if(cands != null && cands.size()>0 ) {
	        return cands.get(0);
	    } else {
	        Contact candidate = new Contact(firstName=content.firstName, lastName=content.lastName, email=content.emailAddress);
	        //check FLS
	        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
	        	Contact.FirstName,
	        	Contact.LastName,
	        	Contact.Email
	        };
	        fflib_SecurityUtils.checkInsert(Contact.SObjectType, fieldList);
    	    insert candidate;
    	    return candidate;
	    }
	}
	
	private Placement_Candidate__c createCm(JXTContent content, String candId) {
	    
	    PlacementCandidateSelector ps = new PlacementCandidateSelector();
	    List<Placement_Candidate__c> cms;
	    //Field List for FLS checking
	    List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
	    	Placement_Candidate__c.Candidate__c,
	    	Placement_Candidate__c.Placement__c,
	    	Placement_Candidate__c.Status__c,
	    	Placement_Candidate__c.Online_Ad__c
	    };
	    //fetch existing candidate management dedup switch
	    Boolean isCmDedupEnabled = (PCAppSwitch__c.getInstance().Deduplicate_PC_Active__c && PCAppSwitch__c.getInstance().DeduplicationOnVCW_Active__c && TriggerHelper.Deduplication);
	    
	    if(isCmDedupEnabled) {
	        //if dedup enabled, fetch cm by firstName, lastName, email and vacancyId
	        cms = ps.getCmByCandidateAndVacId(content.firstName, content.lastName, content.emailAddress, content.vacId);  
	        if(cms != null && cms.size()>0) {
	            Placement_Candidate__c cmNeedUpdate = cms.get(0);
	            //if dedup enabled, update the existing cm with the latest lastModifiedDate candidate which is seen as the incoming candidate.
	            cmNeedUpdate.candidate__c = candId;
	            //checkFLS
	            fflib_SecurityUtils.checkInsert(Placement_Candidate__c.SObjectType, fieldList);
	            fflib_SecurityUtils.checkUpdate(Placement_Candidate__c.SObjectType, fieldList);
    	        upsert cmNeedUpdate;
    	        return cmNeedUpdate;
	        } else {
	            //if no duplicate cm, create a new cm
    	        Placement_Candidate__c cm = new Placement_Candidate__c(Candidate__c=candId, Placement__c=content.vacId, Status__c='New', Online_Ad__c=content.adId);
    	        fflib_SecurityUtils.checkInsert(Placement_Candidate__c.SObjectType, fieldList);
    	        insert cm;
    	        return cm; 
	        }   
	    } else {
	        //if dedup disabled, create cm no matter duplicate cm exist or not
	        Placement_Candidate__c cm = new Placement_Candidate__c(Candidate__c=candId, Placement__c=content.vacId, Status__c='New', Online_Ad__c=content.adId);
	        fflib_SecurityUtils.checkInsert(Placement_Candidate__c.SObjectType, fieldList);
	        insert cm;
	        return cm; 
	    }
	    
	}
	
    /**
	* Create a task according to email content
	*/
	private Task createTask(JXTContent content, Messaging.InboundEmail email, String candId){

		Task newTask = new Task();
		newTask.OwnerId = content.vacOwnerId;
		newTask.WhatId = content.vacId;
		newTask.whoId = candId;
		newTask.Subject = 'Email Service: ' + email.subject;
		newTask.ActivityDate = Date.today(); 
		newTask.Description = email.plainTextBody;
		newTask.status = TASK_STATUS;
		newTask.Priority = TASK_PRIORITY;
	    try{
	    	//check FLS
	    	List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
	    		Task.OwnerId,
	    		Task.WhatId,
	    		Task.WhoId,
	    		Task.Subject,
	    		Task.ActivityDate,
	    		Task.Description,
	    		Task.Status,
	    		Task.Priority
	    	};
	    	fflib_SecurityUtils.checkInsert(Task.SObjectType, fieldList);
	    	 insert newTask;
	    	 System.debug('Success to create task ['+newTask.whoId+'] to vacancy ['+newTask.WhatId+']');
	    }catch(Exception e){
	    	System.debug(e);
	    	System.debug('Failed to create task ['+newTask.whoId+'] to ['+newTask.WhatId+']');
	    	newTask = null;
	    }
	   return newTask;
	}
	
	public List<Attachment> createAttachments(Task t, JXTContent content ){
	    
	    List<CVContent> resumes = content.files;
	    List<Attachment> attNeedInsert = new List<Attachment>();
	    
	    //Get a list of salesforce attachments from email attachments
		if(resumes!=null && resumes.size()>0) {
		    
    		for(CVContent cv : resumes) {
    		    Attachment a = new Attachment();
        		a.Name = cv.getCvFilename();
        		a.Body = cv.getContent();
        		a.ParentId = t.Id;
        		a.OwnerId = t.OwnerId;
        		attNeedInsert.add(a);
    		}
		} else {
		    system.debug('Can not create attachment for activity because no fiels!');
		}
		//If the email contains attachment, import to salesforce attachment.
		if(attNeedInsert!=null && attNeedInsert.size()>0) {
			//check FLS
			List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
				Attachment.Name,
				Attachment.Body,
				Attachment.ParentId,
				Attachment.OwnerId
			};
			fflib_SecurityUtils.checkInsert(Attachment.SObjectType, fieldList);
	        insert attNeedInsert;
		}
	   return attNeedInsert;
	}
	
	public String callWebService(Task t, List<Attachment> attachments, String skillGroupString ){
		
		User currentUser = UserInformationCon.getUserDetails;
		//If the s3 folder is not set properly for current user, the avitivity status and description will be updated by error msg.
		if(! Test.isRunningTest()){
			if(currentUser.AmazonS3_Folder__c == null || currentUser.AmazonS3_Folder__c == ''){
				System.debug('Failed to find the AmazongS3 Folder of user ['+currentUser.Email+']');
				System.debug('Abort to call web service !');
				
				ActivityDocOperator.updateActivityStatus('Error','Failed to find the AmazongS3 Folder of user ['+currentUser.Email+']', t.id);
				return '';
			}
		}
		
	    //build ActivityDoc object list
		List<ActivityDoc> activityDocs = buildActivityDoc(t, attachments, currentUser, skillGroupString);

        //serialize activityDocs to json object
        String jsonString = JSON.serialize(activityDocs);
        
        //Added by Alvin for fixing the encoding issues when there are some special characters.
        //encode json string
        jsonString=EncodingUtil.urlEncode(jsonString,'UTF-8');
        
        //encode session id
        String sessionId = UserInfo.getSessionId();
        if(sessionId != null){
        	sessionId = EncodingUtil.urlEncode(UserInfo.getSessionId(), 'UTF-8');
        } else{
        	sessionId = '';
        }
        //build and encode url
        String surl = EncodingUtil.urlEncode(URL.getSalesforceBaseUrl().toExternalForm() +'/services/Soap/u/25.0/'+UserInfo.getOrganizationId(),'UTF-8');
       
        //invoke web service
        if(! Test.isRunningTest()){
        	ActivityDocServices.uploadDocumentByActivityAtt(jsonString, sessionId, surl);
        	ActivityDocOperator.updateActivityStatus('In Progress','Request has been sent out', t.id);
        }   
        
        return surl;
	}
	
	public List<ActivityDoc> buildActivityDoc(Task newTask, List<Attachment> attachments, User currentUser, String skillGroupString){
	    
	    //It aims to get who.type and what.type of Task to get lookup field API name of ResumeAndFiles
	    TaskSelector tSelector = new TaskSelector();
	    Task t = tSelector.getTaskById(newTask.Id);
	    
	    //Get documents with document type for all the attachments
	    Map<Attachment, String> attachmentsMap = getAttachmentNeedToParse(attachments);
		
		//Get custom setting for skill parsing
		Boolean skillParsingEnabled = SkillGroups.isSkillParsingEnabled();
		String enableSkillParsing = String.valueOf( skillParsingEnabled );
		
		String amazonS3Folder = currentUser.AmazonS3_Folder__c;
		String nameSpace = PeopleCloudHelper.getPackageNamespace();		
		String orgId = Userinfo.getorganizationid().subString(0,15);
		String activityId = t.Id;
		String whoId = t.WhoId;
        if(whoId == null){
        	whoId = '';
        }
        
        List<ActivityDoc> activityDocList = new List<ActivityDoc>();
		
		for(Attachment a : attachmentsMap.keySet() ) {
		    //build one ActivityDoc object
    		ActivityDoc activityDoc = new ActivityDoc();
    		//org id
            activityDoc.orgId = orgId;
            //activity id
            activityDoc.activityId = activityId;           
            //who map
            activityDoc.whoMap.put(whoId,getLookUpFieldNameViaObjectName(t.Who.type));
            //what map
            activityDoc.whatMap.put(t.WhatId,getLookUpFieldNameViaObjectName(t.What.type));
            //attachment name
            activityDoc.attachmentName = a.Name;
            //attachment 
            activityDoc.attachmentId = a.Id;
            //amazonS3 folder
            activityDoc.amazonS3Folder = amazonS3Folder;
            //name space
            activityDoc.nameSpace = nameSpace;
            //enable skill parsing
            activityDoc.enableSkillParsing = enableSkillParsing;
            //from email service
            activityDoc.isEmailService = false;           
            //is selected
            activityDoc.isSelected=true;
            
            if(attachmentsMap.get(a) == 'CoverLetter') {
                
                activityDoc.skillGroupIds = null;
                //make doc type as 'CoverLetter' if the attachemnt name contains 'CoverLetter'
                activityDoc.docType = 'CoverLetter';
            } else {
                //set default skill groups       
                if( skillParsingEnabled ){
                    
                	if( skillGroupString != null && !skillGroupString.equals('')){
                		try{
                			//split skill group string to List<String> and assign to activityDoc.skillGroupIds
                			activityDoc.skillGroupIds = skillGroupString.split(',', -1);             			
                		}catch(Exception e){
                			System.debug(e);
                		}	
                	} else {
                	    activityDoc.skillGroupIds = null;
                	}			
        		}
                //make doc type as 'Resume' if the attachemnt name doesn't contains 'CoverLetter'
                activityDoc.docType = 'Resume';
            } 
            activityDocList.add(activityDoc);
		}
		
		return activityDocList;
	}
	
	//Get document type and document which need to be imported to SF as ResumeAndFiles object
	public Map<Attachment, String> getAttachmentNeedToParse(List<Attachment> attachments) {
	    Map<Attachment, String> attachmentsMap = new Map<Attachment, String>();
	    Boolean markResume=false;
	    Boolean markCoverLetter=false;
	    for(Attachment att: attachments) {
	        if( (att.Name.containsIgnoreCase('cv') || att.Name.containsIgnoreCase('resume')) && !markResume) {
	            markResume=true;
	            attachmentsMap.put(att, 'Resume');
	        } else if( (att.Name.containsIgnoreCase('coverletter') || att.Name.containsIgnoreCase('cover letter')) && !markCoverLetter) {
	            markCoverLetter=true;
	            attachmentsMap.put(att, 'CoverLetter');
	        } else {
	            attachmentsMap.put(att, 'OtherDocument');
	        }
	    }
	    return attachmentsMap;
	}	

	public String getLookUpFieldNameViaObjectName(String objectApiName){
        //all fields of resume and files 
        //key is the field api name 
        Map<String,Schema.SObjectField> resumeObjectFields 
                    = Web_Document__c.SObjectType.getDescribe().fields.getMap();
                    
        //map contains resume lookup object api name and corresponding filed api name
        Map<String,String> resumeLookUpObjectFieldMap=new Map<String,String>();
        String fieldApiName='';
        for(String key:resumeObjectFields.KeySet())
        {
            Schema.SObjectField s=resumeObjectFields.get(key);
            String resumefieldApiName = s.getDescribe().getName();
            String resumeRelatedOjectName='';
            
            //Check if the field is lookup field
            if(s.getDescribe().getReferenceTo().size()>0)
            {
                resumeRelatedOjectName
                    =s.getDescribe().getReferenceTo().get(0).getDescribe().getName();
            }
            
            //put lookup object name and field name in the map
            if(resumeRelatedOjectName != '' && resumeRelatedOjectName != null)
            {
                resumeLookUpObjectFieldMap.put(resumeRelatedOjectName,resumefieldApiName);
            }
        }
        fieldApiName=resumeLookUpObjectFieldMap.get(objectApiName);
        
        return fieldApiName;
    }
	
	
	
}