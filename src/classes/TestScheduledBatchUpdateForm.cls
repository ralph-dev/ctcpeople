@isTest
	
public with sharing class TestScheduledBatchUpdateForm {
	public static testMethod void testschedule() {
		System.runAs(DummyRecordCreator.platformUser) {
		Test.StartTest();
		scheduledBatchUpdateForm sbuf = new scheduledBatchUpdateForm();
		String sch = '0 39 * * * ?';
		String jobId = system.schedule('Test Territory Check', sch, sbuf); 
		 
		// Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        // Verify the expressions are the same
       System.assertEquals(sch, ct.CronExpression);
       Test.stopTest();
		}
	}
}