public with sharing class InvoiceItemAstutePayrollImplementation {
	/****
		Methods generate the update info from Invoice Line Item.
	
	***/
	public static void syncInvoiceLineItems(list<String> invoiceLineItemIds,String session){
		InvoiceItemServices services=new InvoiceItemServices();
		list<Invoice_Line_Item__c> invoiceLineItems=services.retrieveInvoicesByIds(invoiceLineItemIds);
		InvoiceItemServices invoiceItemServ=new InvoiceItemServices();
		list<map<String,Object>> objSTR=services.convertInvoiceItemToInvocieItem(invoiceLineItems);
		map<String,Object> messagesJson=new map<String,Object>();
		messagesJson.put('invoiceItemSaves',objSTR);
		String messages=JSON.serialize(messagesJson);
		String endpointAstute=Endpoint.getAstuteEndpoint();
		endpointAstute=endpointAstute+'/AstutePayroll/invoiceItem';
		AstutePayrollFutureJobs.pushHandler(messages,endpointAstute,session);
	}
	
	/***
		Invoice line item
	
	
	********/
	public static void astutePayrollTrigger(list<Invoice_Line_Item__c> invoiceLineItems){
		CTCPeopleSettingHelperServices ctcPeopleSettingHelperServ=new CTCPeopleSettingHelperServices();
    	if(!ctcPeopleSettingHelperServ.hasApAccount()){
    		return;
    	}
    	list<String> invoiceLineItemIds=new list<String>();
    	list<String> invoiceLineItemIdsFirstSync=new list<String>();
    	for(Invoice_Line_Item__c perInvoiceItem : invoiceLineItems){
    		if(perInvoiceItem.Is_Ready_For_Astute_Payroll__c){
    			perInvoiceItem.Is_Ready_For_Astute_Payroll__c=false;
    			perInvoiceItem.Astute_Payroll_Upload_Status__c='On Hold';
    			invoiceLineItemIdsFirstSync.add(perInvoiceItem.Id);
    			continue;
    		}
    		if(perInvoiceItem.Is_Pushing_To_AP__c){
    			perInvoiceItem.Is_Pushing_To_AP__c=false;
    			perInvoiceItem.Astute_Payroll_Upload_Status__c='On Hold';
    			invoiceLineItemIds.add(perInvoiceItem.Id);
    		}
    	}
    	if(invoiceLineItemIds.size()>0){
    		AstutePayrollFutureJobs.syncInvoiceLineItemsFuture(invoiceLineItemIds, AstutePayrollUtils.GetloginSession());
    	}
    	if(invoiceLineItemIdsFirstSync.size()>0){
    		AstutePayrollFutureJobs.pushBrandNewInvoiceToAP(invoiceLineItemIdsFirstSync, AstutePayrollUtils.GetloginSession());
    	}
    	
	}
	
	
	
	
}