public with sharing class DaoContact extends CommonSelector{
	
	public DaoContact(){
		super(Contact.sObjectType);
	}

private static DaoContact dao = new DaoContact();
// maximum contacts that a candidate pool record can contain
static public final Integer MAXIMUM_CONTACT_LIMIT = 25000;

private static String defaultFields  = 'Title, Salutation,'
               +' RecordTypeId, Phone, Yrs_Exp_1__c, AccountId,'
               +' Status__c, Status_2__c, '
               +' Skills__c, Retain_My_Details_for_Other_Position__c, '
               +' Preferred_Location__c, Personal_Interests__c, People_Rating_c__c, '
               +' Industry_1__c, HR_Strategy__c,  '
               +' Gender__c, Education__c,'
               +' Direct_Reports__c, Desired_Salary__c, Current_Salary__c,  '
               +' Candidate_From_Web__c,  OwnerId,'
               +' Name, MobilePhone, MailingStreet, MailingState, '
               +' MailingPostalCode, MailingCountry, MailingCity, LeadSource, LastName, '
               +' Id, HomePhone, FirstName, Fax, EmailBouncedReason, '
               +' Email, Description, Department, '
               +' Birthdate, AssistantPhone, AssistantName';

static public List<Contact> getAllContacts(){
	dao.checkRead('id, Name,Phone,Email,RecordTypeId');
    return [select id, Name,Phone,Email,RecordTypeId
            from Contact where lastName != '' limit 50000];
}

static public List<Contact> getContactsContainName(String name,List<RecordType> recordTypes){
    name= String.escapeSingleQuotes(name); //To avoid SOQL injection
    name= name.endsWith('%') ? name: name+ '%';
    name= name.startsWith('%') ? name: '%' + name;
    List<Id> rtIds = new List<Id>();
    for(RecordType rt:recordTypes){
        rtIds.add(rt.Id);
    }
    List<Contact> result;
    try{
    	dao.checkRead(defaultFields);
    	CommonSelector.checkRead(Web_Document__c.sObjectType,'Id, Name,File_Size__c,S3_Folder__c,ObjectKey__c,Default_Doc__c, CreatedDate,Document_Type__c');
        result =  [select Title, Salutation,
                RecordTypeId, Phone, Yrs_Exp_1__c, AccountId,
                Status__c, Status_2__c, 
                Skills__c, Retain_My_Details_for_Other_Position__c, 
                Preferred_Location__c, Personal_Interests__c, People_Rating_c__c, 
                Industry_1__c, HR_Strategy__c,  
                Gender__c, Education__c,
                Direct_Reports__c, Desired_Salary__c, Current_Salary__c,  
                Candidate_From_Web__c,  OwnerId,
                Name, MobilePhone, MailingStreet, MailingState, 
                MailingPostalCode, MailingCountry, MailingCity, LeadSource, LastName, 
                Id, HomePhone, FirstName, Fax, EmailBouncedReason, 
                Email, Description, Department, 
                Birthdate, AssistantPhone, AssistantName
                ,(select Id, Name,File_Size__c,S3_Folder__c,ObjectKey__c,Default_Doc__c, CreatedDate,Document_Type__c
                from Contact.Web_Documents__r)
                from Contact where Name like :name and RecordTypeId in:rtIds  
                                          order by Name];
    }catch(Exception e){
        result = new List<Contact>();
    }
    return result;
}

static public List<Contact> getContactsByIds(List<String> idList,List<RecordType> recordTypes){
    List<Id> rtIds = new List<Id>();
    for(RecordType rt:recordTypes){
        rtIds.add(rt.Id);
    }
    List<Contact> result;
    try{    
    	dao.checkRead(defaultFields + ',LastModifiedDate');
    	CommonSelector.checkRead(Web_Document__c.sObjectType,'Id, Name,File_Size__c,S3_Folder__c,ObjectKey__c,Default_Doc__c, CreatedDate,Document_Type__c');
    	
        result = [select Title, Salutation, AccountId,
            RecordTypeId, Phone, Yrs_Exp_1__c, 
            Status__c, Status_2__c, 
            Skills__c, Retain_My_Details_for_Other_Position__c, 
            Preferred_Location__c, Personal_Interests__c, People_Rating_c__c, 
            Industry_1__c, HR_Strategy__c,  
            Gender__c, Education__c,
            Direct_Reports__c, Desired_Salary__c, Current_Salary__c,  
            Candidate_From_Web__c,  OwnerId,
            Name, MobilePhone, MailingStreet, MailingState, 
            MailingPostalCode, MailingCountry, MailingCity, LeadSource, LastName, 
            Id, HomePhone, FirstName, Fax, EmailBouncedReason, 
            Email, Description, Department, 
            Birthdate, AssistantPhone, AssistantName,LastModifiedDate,
            (select Id, Name,File_Size__c,S3_Folder__c,ObjectKey__c,Default_Doc__c, CreatedDate, Document_Type__c
            from Contact.Web_Documents__r)
            from Contact where Id in: idList and RecordTypeId in:rtIds  
                                          order by Name];
    }catch(Exception e){
        result = new List<Contact>();
    }
    return result;
}

static public List<Contact> getContactsByIds(List<String> idList){
    List<Contact> result;
    try{    
        //system.debug('test2 = '+ idlist);
        
        dao.checkRead(defaultFields + ',Account.id,Account.name,LastModifiedDate');
    	CommonSelector.checkRead(Web_Document__c.sObjectType,'Id, Name,File_Size__c,S3_Folder__c,ObjectKey__c,Default_Doc__c, CreatedDate,Document_Type__c');
    	
       result = [select Title, Salutation, AccountId,Account.id,Account.name,
            RecordTypeId, Phone, Yrs_Exp_1__c, 
            Status__c, Status_2__c, 
            Skills__c, Retain_My_Details_for_Other_Position__c, 
            Preferred_Location__c, Personal_Interests__c, People_Rating_c__c, 
            Industry_1__c, HR_Strategy__c,  
            Gender__c, Education__c,
            Direct_Reports__c, Desired_Salary__c, Current_Salary__c,  
            Candidate_From_Web__c,  OwnerId,
            Name, MobilePhone, MailingStreet, MailingState, 
            MailingPostalCode, MailingCountry, MailingCity, LeadSource, LastName, 
            Id, HomePhone, FirstName, Fax, EmailBouncedReason, 
            Email,Department, 
            Birthdate, AssistantPhone, AssistantName,
            (select Id, Name,File_Size__c,S3_Folder__c,ObjectKey__c,Default_Doc__c, CreatedDate, Document_Type__c
            from Contact.Web_Documents__r order by CreatedDate DESC)
            from Contact where Id in:idList order by Name];
    }catch(Exception e){
        result = new List<Contact>();
    }
    return result;
}

static public List<Contact> getContactsByEmails(List<String> emailList,List<RecordType> recordTypes){
    List<Id> rtIds = new List<Id>();
    for(RecordType rt:recordTypes){
        rtIds.add(rt.Id);
    }
    List<Contact> result;
    try{    
    	
    	dao.checkRead(defaultFields + ',LastModifiedDate');
    	CommonSelector.checkRead(Web_Document__c.sObjectType,'Id, Name,File_Size__c,S3_Folder__c,ObjectKey__c,Default_Doc__c, CreatedDate,Document_Type__c');
    	
        result = [select Title, Salutation, AccountId,
            RecordTypeId, Phone, Yrs_Exp_1__c, 
             Status__c, Status_2__c, 
            Skills__c, Retain_My_Details_for_Other_Position__c, 
            Preferred_Location__c, Personal_Interests__c, People_Rating_c__c, 
              Industry_1__c, HR_Strategy__c,  
            Gender__c, Education__c,
            Direct_Reports__c, Desired_Salary__c, Current_Salary__c,  
            Candidate_From_Web__c,  OwnerId,
            Name, MobilePhone, MailingStreet, MailingState, 
            MailingPostalCode, MailingCountry, MailingCity, LeadSource, LastName, 
            Id, HomePhone, FirstName, Fax, EmailBouncedReason, 
            Email, Description, Department, 
            Birthdate, AssistantPhone, AssistantName,LastModifiedDate,
            (select Id, Name,File_Size__c,S3_Folder__c,ObjectKey__c,Default_Doc__c, CreatedDate, Document_Type__c
            from Contact.Web_Documents__r)
            from Contact where Email in: emailList and RecordTypeId in:rtIds  
                                          order by Name];
    }catch(Exception e){
        result = new List<Contact>();
    }
    return result;
}
//Add by Jack
static public List<Contact> getContactsbyName(String name){
    name= String.escapeSingleQuotes(name); //To avoid SOQL injection
    name= name.endsWith('%') ? name: name+ '%';
    name= name.startsWith('%') ? name: '%' + name;
    List<Contact> result;
    try{
    	
    	dao.checkRead(defaultFields );
    	CommonSelector.checkRead(Web_Document__c.sObjectType,'Id, Name,File_Size__c,S3_Folder__c,ObjectKey__c,Default_Doc__c, CreatedDate,Document_Type__c');
    	
        result =  [select Title, Salutation,
                RecordTypeId, Phone, Yrs_Exp_1__c, AccountId,
                Status__c, Status_2__c, 
                Skills__c, Retain_My_Details_for_Other_Position__c, 
                Preferred_Location__c, Personal_Interests__c, People_Rating_c__c, 
                Industry_1__c,
                Gender__c, Education__c,
                Direct_Reports__c, Desired_Salary__c, Current_Salary__c,  
                Candidate_From_Web__c,  OwnerId,
                Name, MobilePhone, MailingStreet, MailingState, 
                MailingPostalCode, MailingCountry, MailingCity, LeadSource, LastName, 
                Id, HomePhone, FirstName, Fax, EmailBouncedReason, 
                Email, Department, 
                Birthdate, AssistantPhone, AssistantName,(select Id, Name,File_Size__c,S3_Folder__c,ObjectKey__c,Default_Doc__c, Document_Type__c, CreatedDate
                from Contact.Web_Documents__r order by Id)
                from Contact where Name like :name order by Name limit 300];
    }catch(Exception e){
        result = new List<Contact>();
    }
    return result;
}

//Add by Jack for Bulk Email WrapClass
static public List<Contact> getBulkEmailContactsByIds(List<String> idList, List<Component> tempComponent){
    List<Contact> result;
    String searchcriteria = '';
    if(tempComponent!= null && tempComponent.size()>0){
        for(Component com: tempComponent){
            if(com.apiname!='name' && com.apiname!='Email'){
                if(searchcriteria == ''){
                    searchcriteria = ','+ com.apiname;
                }
                else{
                    searchcriteria = searchcriteria + ',' + com.apiname;
                }
            }
            else{
                continue;
            }
        }
    }
    try{ 
    	dao.checkRead('Id, name, Email' + searchcriteria);
        String QueryString = 'Select Id, name, Email' + searchcriteria+' from Contact where Id in:idList order by Name';
        //system.debug('QueryString1 ='+ QueryString);
        result = Database.query(QueryString);       
    }catch(Exception e){
        result = new List<Contact>();
    }
    //system.debug('result1='+ result);
    return result;
}

//Add by Kevin for Bulk Email WrapClass
//Retrieve contact ids only
static public List<Contact> getBulkEmailBasicInfoOfContactsByIds(List<String> idList){
    List<Contact> result;
    String searchcriteria = '';

    try{ 
    	dao.checkRead('Id');
        String QueryString = 'Select Id from Contact where Id in:idList order by Name';
        //system.debug('QueryString2 ='+ QueryString);
        result = Database.query(QueryString);       
    }catch(Exception e){
        result = new List<Contact>();
    }
    //system.debug('result2='+ result);
    return result;
}

//Add by Kevin for Bulk Email WrapClass
static public Map<Id,Contact> getBulkEmailContactsByIdsReturnMap(List<String> idList, List<Component> tempComponent){
    List<Contact> tmpResult;
    Map<Id,Contact> result = new Map<Id,Contact>() ;
    String searchcriteria = '';
    if(tempComponent!= null && tempComponent.size()>0){
        for(Component com: tempComponent){
            if(com.apiname!='name' && com.apiname!='Email'){
                if(searchcriteria == ''){
                    searchcriteria = ','+com.apiname;
                }
                else{
                    searchcriteria = searchcriteria + ',' + com.apiname;
                }
            }
            else{
                continue;
            }
        }
    }
    try{ 
        String QueryString = 'Select Id, Name, Email' + searchcriteria+' from Contact where Id in:idList';
        dao.checkRead('Id, Name, Email' + searchcriteria);
        //system.debug('QueryString3 ='+ QueryString);
        tmpResult = Database.query(QueryString);       
    }catch(Exception e){
        tmpResult = new List<Contact>();
    }
    //system.debug('tmpResult3 ='+ tmpResult);
    for(Contact con:tmpResult){
        result.put(con.id,con);
    }
   // system.debug('result3 ='+ result);
    return result;
}



static public List<Contact> getClientContactsbyNameSR(String name){
    name= String.escapeSingleQuotes(name); //To avoid SOQL injection
    name= name.endsWith('%') ? name: name+ '%';
    name= name.startsWith('%') ? name: '%' + name;
    //modified by andy for searching contact in send resume function
    //List<RecordType> contactRecordType = DaoRecordType.nonCandidateRTs;
    List<RecordType> contactRecordType = DaoRecordType.contactRTs;
    
    List<Contact> result;
    try{
    	
    	dao.checkRead(defaultFields );
    	
    	
        result =  [select Title, Salutation,
                RecordTypeId, Phone, Yrs_Exp_1__c, AccountId,
                Status__c, Status_2__c, 
                Skills__c, Retain_My_Details_for_Other_Position__c, 
                Preferred_Location__c, Personal_Interests__c, People_Rating_c__c, 
                Industry_1__c, HR_Strategy__c,  
                Gender__c, Education__c,
                Direct_Reports__c, Desired_Salary__c, Current_Salary__c,  
                Candidate_From_Web__c,  OwnerId,
                Name, MobilePhone, MailingStreet, MailingState, 
                MailingPostalCode, MailingCountry, MailingCity, LeadSource, LastName, 
                Id, HomePhone, FirstName, Fax, EmailBouncedReason, 
                Email, Description, Department, 
                Birthdate, AssistantPhone, AssistantName
                from Contact where Name like :name and recordtypeid in:contactRecordType order by Name];
    }catch(Exception e){
        result = new List<Contact>();
    }
    return result;
}

static public List<Contact> getCandContactsByIds(List<String> idList){
    return getContactsByIds(idList,DaoRecordType.candidateRTs);
}
//add by jack for Send Resume function. As don't need to care the contact record type
static public List<Contact> getCandsContactsByIdsSR(List<String> idList){
    return getContactsByIds(idList);
}

public static List<Contact> getCandContactsContainName(String name){
    return getContactsContainName(name, DaoRecordType.candidateRTs);
}

public static List<Contact> getCorpContactsByEmails(List<String> emailList){
    return getContactsByEmails(emailList, DaoRecordType.corpContactRTs);
}

// Count total for a given account. Since the total number of records will be counted in SF return rows limit
// therefore the total will be no larger than "Limits.getLimitQueryRows()-10000"
public static Integer countContactForComp(Id compId){
    Integer rowLimit = Limits.getLimitQueryRows()-1000;
    if(rowLimit<0)
        rowLimit = 0;
        
    fflib_SecurityUtils.checkObjectIsReadable(dao.getSObjectType());
    return [select count() from Contact where AccountId=:compId limit :rowLimit];
}

}