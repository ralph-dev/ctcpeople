@isTest
private class UserPermissionTest {

	private static testMethod void testSkillParsingEnabled() {
		
		/**
		* added by andy for setting SQS credential
		*/
		SQSCredentialSelectorTest.createDummySQSCredential();

	    PCAppSwitch__c sw = new PCAppSwitch__c(Enable_Daxtra_Skill_Parsing__c=true );
	    insert sw;
	    DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
	    Skill_Group__c dsg = DummyRecordCreator.createDefaultSkillGroup();
		
		System.runAs(DummyRecordCreator.platformUser) {
		
			
	    
    	    UserPermission up = new UserPermission();
    	    system.assert(up.skillParsingEnabled);
	    }
	}
	
	private static testMethod void testIsValidCloudStorageAccount() {
		
		DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
	    Skill_Group__c dsg = DummyRecordCreator.createDefaultSkillGroup();
	    
	    System.runAs(DummyRecordCreator.platformUser) {
		    
		   
    	    UserPermission up = new UserPermission();
    	    system.assert(up.isValidCloudStorageAccount);
	    }
	}
	
	static testMethod void testIsBulkEmailEnabled() {
       
        System.runAs(DummyRecordCreator.platformUser) {
        	PCAppSwitch__c pcSwitch = PCAppSwitch__c.getOrgDefaults();
	        pcSwitch.Enable_Bulk_Email__c = true;
	        upsert pcSwitch;
	        Test.startTest();
            UserPermission up = new UserPermission();
            system.assert(up.isBulkEmailEnabled);
             Test.stopTest();
        }
       
    }
    
    static testMethod void testIsBulkEmailEnabledForUserLevel() {
    	System.runAs(DummyRecordCreator.platformUser) {
	        PCAppSwitch__c pcSwitch = PCAppSwitch__c.getOrgDefaults();
	        pcSwitch.Enable_Bulk_Email__c = false;
	        upsert pcSwitch;
	        Test.startTest();
       
            PCAppSwitch__c pcSwitchUser = new PCAppSwitch__c();
            pcSwitchUser.SetupOwnerId = UserInfo.getUserId();
            pcSwitchUser.Enable_Bulk_Email__c = true;
            insert pcSwitchUser;
            UserPermission up = new UserPermission();
            system.assert(up.isBulkEmailEnabled);
             Test.stopTest();
        }
       
    }
    
    static testMethod void testIsBulkSMSEnabled() {
        
        System.runAs(DummyRecordCreator.platformUser) {
        	PCAppSwitch__c pcSwitch = PCAppSwitch__c.getOrgDefaults();
	        pcSwitch.Enable_Send_Bulk_SMS__c = true;
	        upsert pcSwitch;
	        Test.startTest();
            UserPermission up = new UserPermission();
            system.assert(up.isBulkSMSEnabled);
        }
    }

    static testMethod void testIsScreenCandidateNoteEnabled() {
        PCAppSwitch__c pcSwitch = PCAppSwitch__c.getOrgDefaults();
        pcSwitch.Enable_Screen_Candidate_Note__c = true;
        upsert pcSwitch;
        Test.startTest();
        System.runAs(DummyRecordCreator.platformUser) {
            UserPermission up = new UserPermission();
            system.assert(up.isScreenCandidateNoteEnabled);
        }
    }


    static testMethod void testNumOfRecordsShowing() {
        System.runAs(DummyRecordCreator.platformUser) {
            User u = new UserSelector().getCurrentUser();
            u.Number_of_Records_Showing__c = String.valueOf(20);
            update u;
            UserPermission up = new UserPermission();
            system.assertEquals(20, up.numOfRecordsShowing);
        }
    }

}