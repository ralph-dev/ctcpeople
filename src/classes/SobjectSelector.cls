public with sharing virtual class SobjectSelector{
    private SObjectType targetSObjectType;
	private List<String> fieldsToQuery;
    
    /**
     * @param sobjectType
     *  :SObject for From clause
     * */
    public SobjectSelector(String sobjectType, Set<String> fns){

        List<Schema.DescribeSObjectResult> describeSObjectResult = Schema.describeSObjects(new String[]{sobjectType});
        if(describeSObjectResult.size() == 0) {
            throw new CouldNotInitializeSelectorException('');
        } else {
            Schema.DescribeSObjectResult describe = describeSObjectResult.get(0);
			targetSObjectType = describe.SObjectType;
			
			if(fns != null && fns.size() != 0) {
			    fieldsToQuery = new List<String>(fns);
			} else {
			    fieldsToQuery = new List<String>(describe.fields.getMap().keySet());
			}
			// print  fields want to query
// 			System.debug('fns: ' + fieldsToQuery);
// 			System.debug('fieldsToQuery: ' + fieldsToQuery);
        }
    }
    
    private String getQueryPartOne(){
        String ret = 'SELECT ';
        
        for(Integer i = 0; i < fieldsToQuery.size(); i++){
            if(i == 0){
                ret += fieldsToQuery.get(i);
            } else {
                ret += ', ' + fieldsToQuery.get(i);
            }
        }
        ret += ' FROM ' + targetSObjectType;
        
        return ret;
    }
    
    public Object selectSObjectByIds(Set<Id> idSet){
        String condition = ' WHERE id in :idSet';

        String query = getQueryPartOne() + condition;
        // print query
        // System.debug(query);
        fflib_SecurityUtils.checkRead(targetSObjectType,fieldsToQuery);
        return Database.query(query);
    }
    
    public Object selectSObjectByLimit(Integer limitCount, Integer offset){
        String condition = ' LIMIT :limitCount OFFSET :offset';
        String query = getQueryPartOne() + condition;
        // print query
        // System.debug(query);
        fflib_SecurityUtils.checkRead(targetSObjectType,fieldsToQuery);
        return Database.query(query);
    }
    
    public class CouldNotInitializeSelectorException extends Exception {
	}
}