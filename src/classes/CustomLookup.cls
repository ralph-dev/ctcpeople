public with sharing class CustomLookup {
	public String fieldTypeForSelectedField {get;set;}     
    public list<FieldsDetail> listOfLookupsElement {get;set;}
    public String objectName {get;set;}
    public String fieldName {get;set;}
    public String searchByName{get;set;}
    public String searchByPhone{get;set;}
    public boolean isChecked {get;set;}    
    public boolean isAllSelectCheckBox {get;set;}
    public String IdPassToTextBox{set;get;}
    public String NamesPassToHdn{set;get;} 
    public Boolean hasnext{set;get;}
    public Boolean haspre {set;get;}
    public Integer totalnumber{set;get;}
    public String query{set;get;}
    public Integer totalpage{set;get;}
    public Integer currentpage{set;get;}
    public String idrecorder{set;get;}
    
    
    public CustomLookup (){
        listOfLookupsElement = new list<FieldsDetail>();        
        objectName = String.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('oN'));
        fieldName = String.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('fN'));   
        idrecorder= String.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('idrecorder'));        
        hasnext=true;
        haspre=true;     
        searchByPhone = '';
        searchByName ='';
        getlistOfLookupsElement();
   	}
   	
   	public void getlistOfLookupsElement(){
        try{
        	currentpage=1; 
            listOfLookupsElement = new list<FieldsDetail>();
            String querystr= 'SELECT Name,'+fieldName+' FROM '+objectName;
            String query_totalnumber='SELECT count() FROM '+objectName+ ' limit 2000'; 
            //system.debug(' query_totalnumber is #######'+query_totalnumber);
			//system.debug(' querystr is #######'+querystr);
			
			CommonSelector.checkRead(objectName, 'Name,'+fieldName);
            totalnumber = Database.countQuery(query_totalnumber);          
			editquery(querystr);
			
            
            list<SObject> s = Database.query(query+' 0');                                    
            for(sObject sObj:s){                  
                    listOfLookupsElement.add(new FieldsDetail(String.valueOf(sObj.get('Name')),String.valueOf(sObj.get(fieldName)),false));                                     
            } 
            
        }catch(Exception ex){
             apexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Parameter is not defined, Please contact your Administrator!'));
        }       
    }
    /*
    	Deal with the query and total page.
    **/
    
    public void editquery(String querystr){
    	 	query =querystr;
            
            query=query+' order by id limit 20 offset ';
            totalpage=totalnumber/20;
			Integer mod_num=math.mod(totalnumber,20); 
			if(mod_num!=0){
				totalpage++;
			}
			judgeButton();
    }
    public void judgeButton(){
    	if(totalpage==1 || totalpage==currentpage){
            	hasnext=false;	
            }else{
            	hasnext=true;
            }
            
            if( currentpage>1 ){
            	haspre=true;
            }else{
            	haspre=false;
            }
    }
    
    /*Search event*/
    public void onSearch(){
        isAllSelectCheckBox = false;
        listOfLookupsElement = new list<FieldsDetail>();
        String byName = '\'%'+String.escapeSingleQuotes(searchByName) + '%\'';     
        try{
        	String query_totalnumber='SELECT count() FROM '+objectName+' WHERE  name Like '+byName +' limit 2000'; 
        	//System.debug('The search query is '+query_totalnumber);
        	
        	CommonSelector.checkRead(objectName, 'Name,'+fieldName);
        	
            totalnumber = Database.countQuery(query_totalnumber); 
            String querystr ='SELECT Name,'+fieldName+' FROM '+objectName +' WHERE  name Like '+byName ;
            editquery(querystr);  
            //System.debug('The search query is '+query);            
            list<SObject> s = Database.query(query+' 0');   
            for(sObject sObj:s){                                   
                if(sObj.get(fieldName) != null && sObj.get('Name') != null){
                      listOfLookupsElement.add(new FieldsDetail(String.valueOf(sObj.get('Name')),String.valueOf(sObj.get(fieldName)),false));                         
                }
            } 
        }catch(Exception ex){
            apexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Illegal Search parameter!'));
        }       
    }
    
    /*Next Page event*/
    public void onNext(){
    	 currentpage++;
    	 haspre=true;
    	 judgeButton();
    	 Integer offsetnumber=(currentpage-1)*20;
    	 try{    	 	
    	 	listOfLookupsElement.clear();
    	 	list<SObject> s =Database.query(query+ offsetnumber);
    	 	 for(sObject sObj:s){                                   
                if(sObj.get(fieldName) != null && sObj.get('Name') != null){
                      listOfLookupsElement.add(new FieldsDetail(String.valueOf(sObj.get('Name')),String.valueOf(sObj.get(fieldName)),false));                         
                }
            }
             
    	 }catch(Exception ex){
    	 	apexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please close the window'));
    	 }
    	
    }
    
    /*Previous Page event*/
    public void onPrevious(){
    	currentpage--;
    	Integer offsetnumber=(currentpage-1)*20;
    	judgeButton();
    	try{
    		
    		//System.debug('the query is '+ query+offsetnumber);
    		listOfLookupsElement.clear();
    	 	list<SObject> s =Database.query(query+ offsetnumber);
    	 	 for(sObject sObj:s){                                   
                if(sObj.get(fieldName) != null && sObj.get('Name') != null){
                      listOfLookupsElement.add(new FieldsDetail(String.valueOf(sObj.get('Name')),String.valueOf(sObj.get(fieldName)),false));                         
                }
            } 
    	 }catch(Exception ex){
    	 	apexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please close the window'));
    	 }
    }        
    
     /*Event for check box checked*/
    public void onCheck(){
        IdPassToTextBox = '';
        NamesPassToHdn  = '';
        String CountryCode  = '';
        for(FieldsDetail pn:listOfLookupsElement){
            if(pn.isChecked){
                      
                    IdPassToTextBox = pn.sObjectfield; 
                    NamesPassToHdn = pn.sObjectNameField;
                    searchByName=pn.sObjectNameField;                    
                    
            }
        }
    }

    
     public class FieldsDetail{
        public boolean isChecked {get;set;}
        public string sObjectNameField {get;set;}
        public string sObjectfield {get;set;}                
        public FieldsDetail(string sObjectNameField,string sObjectfield,boolean isChecked){
            this.sObjectNameField = sObjectNameField;
            this.sObjectfield = sObjectfield ;            
            this.isChecked = isChecked;            
        }        
    }
    
    
    
    

}