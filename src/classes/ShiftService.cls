/**
* Description: This is a service class for all services related to shift in advanced people search/roster
* Author: Emily Jiang
* Date: 18.06.2015
**/
public with sharing class ShiftService {
	/**
	* Method used to get display shifts
	* Split one shift into serveral shifts in order to display in the calendar
	**/
	public List<Shift__c> getDisplayShifts(List<Shift__c> inputShiftList){
		ShiftDomain sDomain = new ShiftDomain(inputShiftList);
		List<Shift__c> shifts = sDomain.splitShiftForDisplay();
		return shifts;
	}
	/**
	* Method used to create shifts
	* 
	**/
	public List<Shift__c> upsertShiftsFromWizard(List<ShiftDTO> shiftObjs){
		if(shiftObjs == null || shiftObjs.size() == 0){
			return null;
		}
		
		List<Shift__c> shifts = new List<Shift__c>();
		
		for(ShiftDTO obj : shiftObjs){
			Shift__c shift = new Shift__c();
			ShiftHelper.convertShiftDTOToShiftSObject(shift,obj);
			shifts.add(shift);		
		}
		
		ShiftSelector sSelector = new ShiftSelector();
		List<String> shiftIdList = new List<String>();
		List<Shift__c> shiftListToReturn = new List<Shift__c>();
		
		if(shifts != null && shifts.size()>0){
			//check FLS
			List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
				Shift__c.Location__c,
				Shift__c.Shift_Type__c,
				Shift__c.Recurrence_Weekdays__c,
				Shift__c.Start_Date__c,
				Shift__c.Start_Time__c,
				Shift__c.Recurrence_End_Date__c,
				Shift__c.End_Time__c,
				Shift__c.No_of_Position_Requried__c,
				Shift__c.Vacancy__c,
				Shift__c.Account__c,
				Shift__c.Shift_Status__c,
				Shift__c.Rate__c
			};
			fflib_SecurityUtils.checkInsert(Shift__c.SObjectType, fieldList);
			fflib_SecurityUtils.checkUpdate(Shift__c.SObjectType, fieldList);
			upsert shifts;
			
			for(Shift__c s : shifts){
				shiftIdList.add(s.Id);
			}
			
			//get open shift list by list of shift id, the shift start date should after today
			shiftListToReturn = sSelector.fetchOpenShiftListByShiftIdList(shiftIdList);
		}
		
		//System.debug('upsertShiftsFromWizard '+shiftListToReturn);
		
		return shiftListToReturn;
	}
	
	/**
	* update Shift that is stored in the SF
	*
	**/
	public List<Shift__c> updateShiftStoredInDB(List<ShiftDTO> shiftsObj){
		if(shiftsObj == null || shiftsObj.size() == 0){
			return null;
		}
		
		List<Shift__c> shiftsDB = new List<Shift__c>();
		List<String> shiftIdList = new List<String>();
		
		for(ShiftDTO obj : shiftsObj){
			Shift__c shift = new Shift__c();
			ShiftHelper.convertShiftDTOToShiftSObjectForShiftDB(shift,obj);
			shiftsDB.add(shift);
			shiftIdList.add(shift.Id);	
		}
		
		List<Shift__c> shiftListToUpdate = ShiftHelper.updateShiftEndDate(shiftsDB);
		//System.debug('shiftListToUpdate: '+shiftListToUpdate);
		List<Shift__c> shiftListToReturn = new List<Shift__c>();
		ShiftSelector sSelector = new ShiftSelector();
		if(shiftListToUpdate != null && shiftListToUpdate.size() > 0){
			//check FLS
			fflib_SecurityUtils.checkFieldIsUpdateable(Shift__c.SObjectType, Shift__c.End_Date__c);
			fflib_SecurityUtils.checkFieldIsInsertable(Shift__c.SObjectType, Shift__c.End_Date__c);
			upsert shiftListToUpdate; 
			//get open shift list by list of shift id
			shiftListToReturn = sSelector.fetchOpenShiftListByShiftIdList(shiftIdList);
			//System.debug('shiftListToReturn: '+shiftListToReturn);
		}
		
		return shiftListToReturn;
	}
	
	/**
	* delete Shift that is stored in the SF
	* 
	**/
	public Boolean deleteShiftById(String shiftId){
		System.debug('Shift Id : '+shiftId);
		boolean isSuccessDeleted = false;
		
		if(shiftId == null || shiftId == ''){
			return isSuccessDeleted;
		}
		
		ShiftSelector sSelector = new ShiftSelector();
		List<Shift__c> shiftList = sSelector.fetchShiftListByShiftId(shiftId);
		
		if(shiftList != null && shiftList.size()>0){
			isSuccessDeleted = true;
			delete shiftList;
		}
		
		return isSuccessDeleted;		
	} 
	
	/**
	* group open vacancies for a client to display on 'Open Vacancies' panel
	*
	**/
	public Map<String,List<ShiftDTO>> groupOpenShiftsForClient(String clientId){
		ShiftSelector sSelector = new ShiftSelector();
		List<Shift__c> shiftList = sSelector.fetchOpenShiftListByAccountId(clientId);
		if(shiftList == null || shiftList.size() == 0){
			return null;
		}
		
		Map<String,List<Shift__c>> vacancyShiftsMap = ShiftHelper.groupShiftsOfSameVacancy(shiftList);
		for(String key : vacancyShiftsMap.keySet()){
			List<Shift__c> values = vacancyShiftsMap.get(key);
			ShiftDomain sDomain = new ShiftDomain(values);
			List<Shift__c> valuseSplited = sDomain.splitShiftForDisplay();
			vacancyShiftsMap.put(key,valuseSplited);
		}
		
		Map<String,List<ShiftDTO>> vacancyShiftDTOsMap = new Map<String,List<ShiftDTO>>();
		for(String key : vacancyShiftsMap.keySet()){
			List<Shift__c> values = vacancyShiftsMap.get(key);
			List<ShiftDTO> shiftDTOList = new List<ShiftDTO>();
			for(Shift__c sglShift : values){
				ShiftDTO sglShiftDTO = ShiftHelper.convertShiftSObjectToShiftDTO(sglShift);
				shiftDTOList.add(sglShiftDTO);
			}
			vacancyShiftDTOsMap.put(key,shiftDTOList);
		}
		//System.debug('vacancyShiftDTOsMap: '+vacancyShiftDTOsMap);
		
		return vacancyShiftDTOsMap;
	}
	
	/**
	* get open shifts for a client to display Calendar
	*
	**/
	public List<ShiftDTO> getOpenShiftsForClient(String clientId){
		ShiftSelector sSelector = new ShiftSelector();
		List<Shift__c> shiftList = sSelector.fetchOpenShiftListByAccountId(clientId);
		if(shiftList == null || shiftList.size() == 0){
			return null;
		}
		ShiftDomain sDomain = new ShiftDomain(shiftList);
		List<Shift__c> shiftListSplited = sDomain.splitShiftForDisplay();
		//System.debug('shiftListSplited'+shiftListSplited);
		
		List<ShiftDTO> shiftDTOList = new List<ShiftDTO>();
		for(Shift__c sglShift : shiftListSplited){
			ShiftDTO sglShiftDTO = ShiftHelper.convertShiftSObjectToShiftDTO(sglShift);
			//System.debug('sglShiftDTO'+sglShiftDTO);
			shiftDTOList.add(sglShiftDTO);
		}
		return shiftDTOList;
	}
	/**
	* get open shiftDB for a client
	*
	**/
	public List<ShiftDTO> getOpenShiftsStoredInDatabase(String clientId){
		ShiftSelector sSelector = new ShiftSelector();
		List<Shift__c> shiftList = sSelector.fetchOpenShiftListByAccountId(clientId);
		if(shiftList == null || shiftList.size() == 0){
			return null;
		}
		
		List<ShiftDTO> shiftDTOList = new List<ShiftDTO>();
		for(Shift__c sglShift : shiftList){
			ShiftDTO sglShiftDTO = ShiftHelper.convertShiftSObjectToShiftDTO(sglShift);
			//System.debug('sglShiftDTO'+sglShiftDTO);
			shiftDTOList.add(sglShiftDTO);
		}
		return shiftDTOList;
	}
	
	/**
	* get shift by shiftId
	*
	**/
	public List<ShiftDTO> getShiftDBByShiftId(String shiftId){
		ShiftSelector sSelector = new ShiftSelector();
		List<Shift__c> shiftList = sSelector.fetchShiftListByShiftId(shiftId);
		if(shiftList == null || shiftList.size() == 0){
			return null;
		}
				
		List<ShiftDTO> shiftDTOList = new List<ShiftDTO>();
		for(Shift__c sglShift : shiftList){
			ShiftDTO sglShiftDTO = ShiftHelper.convertShiftSObjectToShiftDTO(sglShift);
			shiftDTOList.add(sglShiftDTO);
		}
		
		return shiftDTOList;
	}
}