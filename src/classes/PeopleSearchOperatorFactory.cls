public class PeopleSearchOperatorFactory{	
	private FieldType createType(String typeName, List<Operator> operators){
		FieldType output = new FieldType();
		output.type = typeName;
		output.operators = operators;
		return output;
	}
/*Methods Convinience----------------------------------------------------------------------------------------------------------*/
	/*
	 * Singleton map with operation string Id(Operator.value) as key and operation object as value
	*/
	private static Map<String, Operator> operatorsMap;
	public static Map<String, Operator> getOperatorsMap(){
		if(operatorsMap == null){
			operatorsMap = new Map<String, Operator>();
			for(Operator ft : new PeopleSearchOperatorFactory().allOperators()){
				operatorsMap.put(ft.value, ft);
			}
		}
		return operatorsMap;
	}
	
	public List<Operator> allOperators(){
		List<Operator> output = new List<Operator>();
		output.addAll(dateRangeOps());
		output.addAll(simpleEqualityOps());
		output.addAll(advanceEqualityOps());
		output.addAll(likeOps());
		output.addAll(includeOps());
		output.addAll(insideOps());
		output.addAll(emptyOps());
		return output;
	}
	
/*Supported FieldTypes----------------------------------------------------------------------------------------------------------*/
	public List<FieldType> allSupportedFieldTypes(){
		return new List<FieldType>{
			createType(PeopleSearchDTO.F_ID, 				textOps()),
			createType(PeopleSearchDTO.F_EMAIL, 			textOps()),
			createType(PeopleSearchDTO.F_PHONE, 			textOps()),
			createType(PeopleSearchDTO.F_STRING, 			textOps()),
			createType(PeopleSearchDTO.F_TEXTAREA, 			textOps()),
			createType(PeopleSearchDTO.F_URL, 				urlOps()),
			createType(PeopleSearchDTO.F_DATE, 				dateOps()),
			createType(PeopleSearchDTO.F_DATE_TIME, 		dateOps()),
			createType(PeopleSearchDTO.F_PICKLIST, 			picklistOps()),
			createType(PeopleSearchDTO.F_MILTI_PICKLIST, 	multiPicklistOps()),
			createType(PeopleSearchDTO.F_NUMBER, 			numberOps()),
			createType(PeopleSearchDTO.F_INTEGER, 			numberOps()),
			createType(PeopleSearchDTO.F_DOUBLE, 			numberOps()),
			createType(PeopleSearchDTO.F_DECIMAL, 			numberOps()),
			createType(PeopleSearchDTO.F_PERCENT, 			numberOps()),
			createType(PeopleSearchDTO.F_CURRENCY, 			numberOps()),
			createType(PeopleSearchDTO.F_BOOLEAN, 			booleanOps())
		};
	}
	
/*OPERATIONS PER FIELD TYPE METHODS----------------------------------------------------------------------------------------------------------*/
   public List<Operator> urlOps(){
		List<Operator> output = new List<Operator>();
		output.addAll(containLikeOps());
		output.addAll(emptyOps());
		return output;
	}

	public List<Operator> textOps(){
		List<Operator> output = new List<Operator>();
		output.addAll(simpleEqualityOps());
		output.addAll(likeOps());
		output.addAll(emptyOps());
		return output;
	}
	
	public List<Operator> dateOps(){
		List<Operator> output = new List<Operator>();
		output.addAll(dateRangeOps());
		output.addAll(emptyOps());
		return output;
	}
	
	public List<Operator> picklistOps(){
		List<Operator> output = new List<Operator>();
		output.addAll(insideOps());
		output.addAll(emptyOps());
		return output;
	}
	
	public List<Operator> multiPicklistOps(){
		List<Operator> output = new List<Operator>();
		output.addAll(includeOps());
		output.addAll(insideOps());
		output.addAll(emptyOps());
		return output;
	}
	
	public List<Operator> numberOps(){
		List<Operator> output = new List<Operator>();
		output.addAll(simpleEqualityOps());
		output.addAll(advanceEqualityOps());
		output.addAll(emptyOps());
		return output;
	}
	
	public List<Operator> booleanOps(){
		List<Operator> output = new List<Operator>();
		output.add(new EQ());
		output.addAll(emptyOps());
		return output;
	}
	
/*OPERATIONS PER CATEGORY METHODS----------------------------------------------------------------------------------------------------------*/
	public List<Operator> dateRangeOps(){
		return new List<Operator>{
			new Between()
		};
	}
	
	public List<Operator> simpleEqualityOps(){
		return new List<Operator>{
			new EQ(),
			new NE()
		};
	}
	
	public List<Operator> advanceEqualityOps(){
		return new List<Operator>{
			new LT(),
			new LE(),
			new GT(),
			new GE()
		};
	}
	
	public List<Operator> likeOps(){
		return new List<Operator>{
			new STLike(),
			new Contain(),
			new EndLike(),
			/*
			new NotSTLike(),
			new NotEndContain(),
			*/
			new NotContain()
		};
	}
	
	public List<Operator> containLikeOps(){
	    return new List<Operator>{
	        new Contain(),
	        new NotContain()
	    };
	}
	
	public List<Operator> stLikeOps(){
	    return new List<Operator>{
	        new STLike(),
	        new EndLike()
	    };
	}
	
	public List<Operator> includeOps(){
		return new List<Operator>{
			new Include(),
			new NotInclude()
		};
	}
	
	public List<Operator> insideOps(){
		return new List<Operator>{
			new Inside(),
			new NotInside()
		};
	}
	
	public List<Operator> emptyOps() {
		return new List<Operator> {
			new IsEmpty(),
			new IsNotEmpty()
		};
	}
	
/*DATE RANGE OPERATIONS----------------------------------------------------------------------------------------------------------*/    
    public class Between extends Operator{    	
    	public Between(){
    		init('Between', 'between', '('+FIELD+' >= '+VAL_1+' AND '+FIELD+' <= '+VAL_2+')');
    	}
		
		public override String generateCondition(String fieldName, String rawValue, String type){
			PeopleSearchDTO.DateValue value = (PeopleSearchDTO.DateValue)JSON.deserialize(rawValue,PeopleSearchDTO.DateValue.class);
			
			value.setAsLocalDate(type);
			
			return condition.replace(VAL_1, value.fromDate)
							.replace(VAL_2, value.toDate)
							.replace(FIELD, fieldName);
		}
    }
    
/*SIMPLE EQUALITY OPERATIONS----------------------------------------------------------------------------------------------------------*/
    //Equal to
    public class EQ extends Operator{
    	public EQ(){
    		super('Equals', 'eq', '=');
    	}
    }
    
    //Not equal to
    public class NE extends Operator{
    	public NE(){
    		super('Not equal to', 'ne', '!=');
    	}
    }
    
/*ADVANCE EQUALITY OPERATIONS----------------------------------------------------------------------------------------------------------*/
	//Less than
    public class LT extends Operator{
    	public LT(){
    		super('Less than', 'lt', '<');
    	}
    }
    
    //Less than or equal to
    public class LE extends Operator{
    	public LE(){
    		super('Less or equal to', 'le', '<=');
    	}
    }
    
    //Greater than
    public class GT extends Operator{
    	public GT(){
    		super('Greater than', 'gt', '>');
    	}
    }
    
    //Greater than or equal to
    public class GE extends Operator{
    	public GE(){
    		super('Greater or equal to', 'ge', '>=');
    	}
    }
    
/*LIKE OPERATIONS----------------------------------------------------------------------------------------------------------*/
    //Start with for string
    public virtual class STLike extends Operator{
    	public STLike(){
    		init('Starts with', 'stLike', '('+FIELD+' LIKE \''+VAL_1+'%\')');
    	}
		
		public override String generateCondition(String fieldName, String value, String type){			
			return condition.replace(VAL_1, getString(value))
							.replace(FIELD, fieldName);
		}
    }
    
    //Contain for string
    public class Contain extends STLike{
    	public Contain(){
    		init('Contains', 'like', '('+FIELD+' LIKE \'%'+VAL_1+'%\')');
    	}
    }
    
    //End with for string
    public class EndLike extends STLike{
    	public EndLike(){
    		init('Ends with', 'endLike', '('+FIELD+' LIKE \'%'+VAL_1+'\')');
    	}
    }
    
    //Does not start with for string
    public class NotSTLike extends STLike{
    	public NotSTLike(){
    		init('Not start with', 'notStLike', '(NOT '+FIELD+' LIKE \''+VAL_1+'%\')');
    	}
    }
    
    //Does not contain for string
    public class NotContain extends STLike{
    	public NotContain(){
    		init('Not contain', 'notLike', '(NOT '+FIELD+' LIKE \'%'+VAL_1+'%\')');
    	}
    }
    
    //Does not end with for String
    public class NotEndContain extends STLike{
    	public NotEndContain(){
    		init('Not end with', 'notEndLike', '(NOT '+FIELD+' LIKE \'%'+VAL_1+'\')');
    	}
    }
    
/*INCLUDE OPERATIONS----------------------------------------------------------------------------------------------------------*/
    //Include value in List
    public virtual class Include extends Operator{
    	public Include(){
    		init('Includes', 'inc', FIELD+' INCLUDES '+VAL_1);
    	}
		
		protected String getSearchValueForMultiPicklist(String rawValue){
			List<String> multiPicklistVals = getMultipicklistValue(rawValue);
			String value = '(';
			for(String str : multiPicklistVals){
				value += '\''+str+'\',';
			}
			
			value = value.removeEnd(',') + ')';
			return value;
		}
		
		public override String generateCondition(String fieldName, String rawValue, String type){
			String value = getSearchValueForMultiPicklist(rawValue);
			
			return condition.replace(VAL_1, value)
							.replace(FIELD, fieldName);
		}
    }
    
    //Does not include value in List
    public class NotInclude extends Include{
    	public NotInclude(){
    		init('Excludes', 'exc', FIELD+' EXCLUDES '+VAL_1);
    	}
    }
    
    //Inside value in List
    public virtual class Inside extends Operator{
    	public Inside(){
    		init('Equals', 'in', FIELD+' IN '+VAL_1);
    	}
		
		public override String generateCondition(String fieldName, String rawValue, String type){
			final String LIST_SEPARATOR = ';';
			String value; 
			List<String> multiPicklistVals = getMultipicklistValue(rawValue);
			if(type == PeopleSearchDTO.F_MILTI_PICKLIST){
				value = '(\'';
				for(String picklistVal : multiPicklistVals){
					value += picklistVal+LIST_SEPARATOR;
				}
				value = value.removeEnd(LIST_SEPARATOR) + '\')';
			}
			else if(type == PeopleSearchDTO.F_PICKLIST){
			    value = '(';
				for(String str : multiPicklistVals){
				    value += '\''+str+'\',';
			    }
			    value = value.removeEnd(',') + ')';
			}
			
			return condition.replace(VAL_1, value)
							.replace(FIELD, fieldName);
		}
    }
    
    //Does not include value in List
    public class NotInside extends Inside{
    	public NotInside(){
    		init('Not equal to', 'notIn', FIELD+' NOT IN '+VAL_1);
    	}
    }
    
    /*EMPTY OPERATIONS ADDED BY LORENA----------------------------------------------------------------------------------------------------------*/
    public class IsEmpty extends Operator {
    	public IsEmpty() {
    		super('Is Empty', 'ie');
    	}
    }
    
    public class IsNotEmpty extends Operator {
    	public IsNotEmpty() {
    		super('Is Not Empty', 'ine');
    	}
    }
    
/*BASE OPERATION CLASS----------------------------------------------------------------------------------------------------------*/
    public virtual class Operator{
    	public String label;					/*display name*/ 
		public String value;					/*operator value see operator ref table*/
		
		protected transient String condition;	/*used for actual query condition generation*/
		public transient final String FIELD = 'OBJ_FIELD';
    	public transient final String VAL_1 = 'VAL_1';
    	public transient final String VAL_2 = 'VAL_2';
    	public transient final String SEPARATOR = ' - ';
		
		/*
		 *Constructor for complex conditions
		 *Note use init() method after instanciation
		*/
		public Operator(){}
		
		public Operator(String l, String v) {
			String c;
			if(v == 'ie') {
				c = '(' + FIELD + ' = null )';
			} else if(v == 'ine') {
				c = '(' + FIELD + ' != null )';
			}			
			label = l;
			value = v;
			condition = c;
		}
		
		/*Constructor for simple conditions*/
		public Operator(String l, String v, String s){
			String c = '('+FIELD+' '+s+' '+VAL_1+')';
			init(l,v,c);
		}
		
		protected void init(String l, String v, String c){
			label = l;
			value = v;
			condition = c;
		}
		
		protected String getString(String str){
			String value = '';
			if(str == null) {
				str = '';
			}
			try{
				value = (String)JSON.deserialize(str, String.class);
			}catch(JSONException e) {
				value = str;
			}
			
			//system.debug(LoggingLevel.FINE, '\n\nString value: ' + value + '\n\n');
			return value;
		}
		
		protected List<String> getMultipicklistValue(String rawValue){
			List<String> multiPicklistVals = (List<String>)JSON.deserialize(rawValue,List<String>.class);
			//system.debug(LoggingLevel.FINE, '\n\nmultiPicklistVals: ' + multiPicklistVals + '\n\n');
			return multiPicklistVals;
		}
		
		//place holder method to be implemented on the child class
		public virtual String generateCondition(String fieldName, String rawValue, String type){
			String value = getString(rawValue);
			String output;

			if(PeopleSearchDTO.DATE_FIELDS.contains(type)) {
				value = PeopleSearchHelper.getLocalDateTime(rawValue);
			}else if(!PeopleSearchDTO.NUMBER_FIELDS.contains(type) && type != PeopleSearchDTO.F_BOOLEAN){
				value = '\''+value+'\'';
			}
			if(value == null || value == '') {
			    output = condition.replace(FIELD, fieldName);
			} else {
			    output = condition.replace(VAL_1, value).replace(FIELD, fieldName);
			}
			
			//system.debug(LoggingLevel.FINE, '\n\nfield: '+fieldName+'\nvalue: '+value+'\ncondition: '+output+'\n\n');
			return output;
		}
    }

/*PART OF PeopleSearchDTO----------------------------------------------------------------------------------------------------------*/
    public class FieldType{
    	public String type;						/* the type we supports e.g: pickList, Date, String, multiplePickList */
		public List<Operator> operators;
    }
}