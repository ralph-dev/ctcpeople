/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

@IsTest
private class SaveGSASearchTest {
	/*public testMethod static void constructorTest(){
		System.runAs(DummyRecordCreator.platformUser) {
		Cookie gsaUrlCookie = new Cookie('searchCre','a', null, -1, false); // put the gurl to cookie
		Cookie fromCookie = new Cookie('from','save', null, -1, false); // put the from to cookie
		Cookie keywordsCookie = new Cookie('encodedKeywords', 'b', null, -1, false);
		Cookie advancedCookie = new Cookie('advanced', 'true', null, -1, false);
		Cookie singleCriteriaJsonCookie = new Cookie('singleCriteriaJson', 'd', null, -1, false);
		Cookie multipleCriteriaJsonCookie= new Cookie('multipleCriteriaJson', 'e', null, -1, false);
		List<Cookie> cookieList = new List<Cookie>();
		cookieList.add(gsaUrlCookie);
		cookieList.add(fromCookie);
		cookieList.add(keywordsCookie);
		cookieList.add(advancedCookie);   
		cookieList.add(singleCriteriaJsonCookie);
		cookieList.add(multipleCriteriaJsonCookie);
		PageReference googleSearch = Page.googleSearch2;
		ApexPages.currentPage().setCookies(cookieList);
		SaveGSASearch saveSearch = new SaveGSASearch();
		Cookie gsaUrlCookie2 = new Cookie('searchCre',null, null, -1, false); // put the gurl to cookie
		Cookie fromCookie2 = new Cookie('from','save', null, -1, false); // put the from to cookie
		Cookie keywordsCookie2 = new Cookie('encodedKeywords', 'b', null, -1, false);
		Cookie advancedCookie2 = new Cookie('advanced', 'true', null, -1, false);
		Cookie singleCriteriaJsonCookie2 = new Cookie('singleCriteriaJson', 'd', null, -1, false);
		Cookie multipleCriteriaJsonCookie2= new Cookie('multipleCriteriaJson', 'e', null, -1, false);
		List<Cookie> cookieList2 = new List<Cookie>();
		cookieList2.add(gsaUrlCookie2);
		cookieList2.add(fromCookie2);
		cookieList2.add(keywordsCookie2);
		cookieList2.add(advancedCookie2);
		cookieList2.add(singleCriteriaJsonCookie2);
		cookieList2.add(multipleCriteriaJsonCookie2);
		ApexPages.currentPage().setCookies(cookieList2);
		SaveGSASearch saveSearch2 = new SaveGSASearch();
		system.assertEquals(saveSearch2.showSave, true);
		}
		
	}
	public testMethod static void saveGSATest(){
		System.runAs(DummyRecordCreator.platformUser) {
		Cookie gsaUrlCookie = new Cookie('searchCre','null', null, -1, false); // put the gurl to cookie
		Cookie fromCookie = new Cookie('from','save', null, -1, false); // put the from to cookie
		Cookie keywordsCookie = new Cookie('encodedKeywords', 'b', null, -1, false);
		Cookie advancedCookie = new Cookie('advanced', 'true', null, -1, false);
		Cookie singleCriteriaJsonCookie = new Cookie('singleCriteriaJson', 'd', null, -1, false);
		Cookie multipleCriteriaJsonCookie= new Cookie('multipleCriteriaJson', 'e', null, -1, false);
		List<Cookie> cookieList = new List<Cookie>();
		cookieList.add(gsaUrlCookie);
		cookieList.add(fromCookie);
		cookieList.add(keywordsCookie);
		cookieList.add(advancedCookie);
		cookieList.add(singleCriteriaJsonCookie);
		cookieList.add(multipleCriteriaJsonCookie);
		PageReference googleSearch = Page.googleSearch2;
		ApexPages.currentPage().setCookies(cookieList);
		SaveGSASearch saveSearch = new SaveGSASearch();
		try{
			PageReference saveGSApage = saveSearch.saveGSA();
			system.assert(saveGSApage!=null);
		}catch(Exception e){
			System.debug('saveGSATest exception');
		}
		}
	}
	
	public testMethod static void getGSARecordsTest(){
		System.runAs(DummyRecordCreator.platformUser) {
		Cookie gsaUrlCookie = new Cookie('searchCre','a', null, -1, false); // put the gurl to cookie
		Cookie fromCookie = new Cookie('from','save', null, -1, false); // put the from to cookie
		Cookie keywordsCookie = new Cookie('encodedKeywords', 'b', null, -1, false);
		Cookie advancedCookie = new Cookie('advanced', 'true', null, -1, false);
		Cookie singleCriteriaJsonCookie = new Cookie('singleCriteriaJson', 'd', null, -1, false);
		Cookie multipleCriteriaJsonCookie= new Cookie('multipleCriteriaJson', 'e', null, -1, false);
		List<Cookie> cookieList = new List<Cookie>();
		cookieList.add(gsaUrlCookie);
		cookieList.add(fromCookie);
		cookieList.add(keywordsCookie);
		cookieList.add(advancedCookie);
		cookieList.add(singleCriteriaJsonCookie);
		cookieList.add(multipleCriteriaJsonCookie);
		PageReference googleSearch = Page.googleSearch2;
		ApexPages.currentPage().setCookies(cookieList);
		SaveGSASearch saveSearch = new SaveGSASearch();
		GSA_Search__c gsaSearch = new GSA_Search__c();
		insert gsaSearch;
		
		List<GSA_Search__c> GSARecords = saveSearch.getGSARecords();
		system.assert(GSARecords.size()>0);
		}
	}
	public testMethod static void googleSearchTest(){
		System.runAs(DummyRecordCreator.platformUser) {
		GSA_Search__c search1 = new GSA_Search__c();
		search1.Search_Name__c = 'computer';
		search1.key_words__c  = 'Computer';
		search1.Advanced__c=false;
		insert search1;
		ApexPages.currentPage().getParameters().put('Search_Name__c', 'Computer');
		ApexPages.currentPage().getParameters().put('vid', 'a04900000012unE');
		Cookie gsaUrlCookie = new Cookie('searchCre','a', null, -1, false); // put the gurl to cookie
		Cookie fromCookie = new Cookie('from','save', null, -1, false); // put the from to cookie
		Cookie keywordsCookie = new Cookie('encodedKeywords', 'b', null, -1, false);
		Cookie advancedCookie = new Cookie('advanced', 'true', null, -1, false);
		Cookie singleCriteriaJsonCookie = new Cookie('singleCriteriaJson', 'd', null, -1, false);
		Cookie multipleCriteriaJsonCookie= new Cookie('multipleCriteriaJson', 'e', null, -1, false);
		List<Cookie> cookieList = new List<Cookie>();
		cookieList.add(gsaUrlCookie);
		cookieList.add(fromCookie);
		cookieList.add(keywordsCookie);
		cookieList.add(advancedCookie);
		cookieList.add(singleCriteriaJsonCookie);
		cookieList.add(multipleCriteriaJsonCookie);
		PageReference googleSearch = Page.googleSearch2;
		ApexPages.currentPage().setCookies(cookieList);
		SaveGSASearch saveSearch = new SaveGSASearch();
		try{
			PageReference googleSearchpage = saveSearch.googleSearch();
			system.assert(googleSearchpage!=null);
		}catch(Exception e){
			System.debug('googleSearchTest exception');
		}
		}
	}
	public testMethod static void deleteSearchTest(){
		System.runAs(DummyRecordCreator.platformUser) {
		Cookie gsaUrlCookie = new Cookie('searchCre','a', null, -1, false); // put the gurl to cookie
		Cookie fromCookie = new Cookie('from','save', null, -1, false); // put the from to cookie
		Cookie keywordsCookie = new Cookie('encodedKeywords', 'b', null, -1, false);
		Cookie advancedCookie = new Cookie('advanced', 'true', null, -1, false);
		Cookie singleCriteriaJsonCookie = new Cookie('singleCriteriaJson', 'd', null, -1, false);
		Cookie multipleCriteriaJsonCookie= new Cookie('multipleCriteriaJson', 'e', null, -1, false);
		List<Cookie> cookieList = new List<Cookie>();
		cookieList.add(gsaUrlCookie);
		cookieList.add(fromCookie);
		cookieList.add(keywordsCookie);
		cookieList.add(advancedCookie);
		cookieList.add(singleCriteriaJsonCookie);
		cookieList.add(multipleCriteriaJsonCookie);
		PageReference googleSearch = Page.googleSearch2;
		ApexPages.currentPage().setCookies(cookieList);
		SaveGSASearch saveSearch = new SaveGSASearch();
		try{
			PageReference deleteSearchpage = saveSearch.deleteSearch();
			system.assertEquals(deleteSearchpage, null);
		}catch(Exception e){
			System.debug('deleteSearchTest exception');
		}
		}
	}
	public testMethod static void editSearchTest(){
		System.runAs(DummyRecordCreator.platformUser) {
			Cookie gsaUrlCookie = new Cookie('searchCre','a', null, -1, false); // put the gurl to cookie
		Cookie fromCookie = new Cookie('from','save', null, -1, false); // put the from to cookie
		Cookie keywordsCookie = new Cookie('encodedKeywords', 'b', null, -1, false);
		Cookie advancedCookie = new Cookie('advanced', 'true', null, -1, false);
		Cookie singleCriteriaJsonCookie = new Cookie('singleCriteriaJson', 'd', null, -1, false);
		Cookie multipleCriteriaJsonCookie= new Cookie('multipleCriteriaJson', 'e', null, -1, false);
		List<Cookie> cookieList = new List<Cookie>();
		cookieList.add(gsaUrlCookie);
		cookieList.add(fromCookie);
		cookieList.add(keywordsCookie);
		cookieList.add(advancedCookie);
		cookieList.add(singleCriteriaJsonCookie);
		cookieList.add(multipleCriteriaJsonCookie);
		PageReference googleSearch = Page.googleSearch2;
		ApexPages.currentPage().setCookies(cookieList);
		SaveGSASearch saveSearch = new SaveGSASearch();
		try{
			PageReference editSearchpage =saveSearch.editSearch();
			system.assert(editSearchpage!=null);
		}catch(Exception e){
			System.debug('editSearchTest exception');
		}
		}
	}
	public testMethod static void back2googleSearchTest(){
		System.runAs(DummyRecordCreator.platformUser) {
			Cookie gsaUrlCookie = new Cookie('searchCre','a', null, -1, false); // put the gurl to cookie
		Cookie fromCookie = new Cookie('from','save', null, -1, false); // put the from to cookie
		Cookie keywordsCookie = new Cookie('encodedKeywords', 'b', null, -1, false);
		Cookie advancedCookie = new Cookie('advanced', 'true', null, -1, false);
		Cookie singleCriteriaJsonCookie = new Cookie('singleCriteriaJson', 'd', null, -1, false);
		Cookie multipleCriteriaJsonCookie= new Cookie('multipleCriteriaJson', 'e', null, -1, false);
		List<Cookie> cookieList = new List<Cookie>();
		cookieList.add(gsaUrlCookie);
		cookieList.add(fromCookie);
		cookieList.add(keywordsCookie);
		cookieList.add(advancedCookie);
		cookieList.add(singleCriteriaJsonCookie);
		cookieList.add(multipleCriteriaJsonCookie);
		PageReference googleSearch = Page.googleSearch2;
		ApexPages.currentPage().setCookies(cookieList);
		SaveGSASearch saveSearch = new SaveGSASearch();
		PageReference editback2googleSearch = saveSearch.back2googleSearch();
		system.assert(editback2googleSearch!= null);
		}
	}*/
}