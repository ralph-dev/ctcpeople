public class SearchResult {
	private String candidateId;
	private Double score;
	private String firstName;
	private String lastName;
	private String skills;
	private List<String> snippets;

	public String getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(String candidateId) {
		this.candidateId = candidateId;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}
	
	public List<String> getSnippets() {
		return snippets;
	}

	public void setSnippets(List<String> snippets) {
		this.snippets = snippets;
	}
}