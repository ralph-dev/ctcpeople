public with sharing class PlacementCandidateDomain extends SObjectDomain{
	public PlacementCandidateDomain(List<Placement_Candidate__c> cms) {
		super(cms);
	}

	public PlacementCandidateDomain(Placement_Candidate__c cm) {
		super(new List<Placement_Candidate__c>{cm});
	}

	public PlacementCandidateDomain() {
		super(new List<Placement_Candidate__c>{});
	}

	public Map<String, Placement_Candidate__c> createPlacementCandidates(List<candidateDTO> canDTOs, String vacancyId) {
		Map<String, Placement_Candidate__c> canCMMap = new Map<String, Placement_Candidate__c>();

		PlacementCandidateService pCandidateService = new PlacementCandidateService();
		canCMMap = pCandidateService.createCMByVancancyAndCandidates(canDTOs, vacancyId);

		return canCMMap;
	}
}