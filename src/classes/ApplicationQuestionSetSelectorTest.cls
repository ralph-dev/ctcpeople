/**
 * This is the test class for ApplicationQuestionSetSelector
 *
 * Created by: Lina Wei
 * Created on: 27/03/2017
 */

@isTest
public class ApplicationQuestionSetSelectorTest {

    static testMethod void testGetActiveSeekScreenByAccountIds() {
        // create data
        System.runAs(DummyRecordCreator.platformUser) {
            Test.startTest();
            Advertisement_Configuration__c seekAccount = AdConfigDummyRecordCreator.createDummySeekAccount();
            ApplicationQuestionSetDummyRecordCreator.createActiveSeekScreen(seekAccount);
            ApplicationQuestionSetDummyRecordCreator.createInactiveSeekScreen(seekAccount);
            List<Application_Question_Set__c> seekScreenList =
                    new ApplicationQuestionSetSelector().getActiveSeekScreenByAccountIds(new Set<Id>{
                            seekAccount.Id
                    });
            System.assertEquals(1, seekScreenList.size());
            Test.stopTest();
        }
    }
}