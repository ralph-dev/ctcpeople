/**
 * Created by zoechen on 21/7/17.
 */

public with sharing class OAuthServiceSelector extends CommonSelector {


	public OAuthServiceSelector( ){
		super(OAuth_Service__c.SObjectType);
	}
	
	
    
    /*
     *  Set default fields that needs to be retrieved by StyleCategorySelector
     */
   /** public override LIST<Schema.SObjectField> getSObjectFieldList(){
        List<Schema.SObjectField> fieldsList = new List<Schema.SObjectField>();
        fieldsList = getSObjectType().getDescribe().fields.getMap().values();
        return fieldsList;
    }*/


	private List<String> getDefaultFields(){
		return new List<String>{
			'Id',
			'Name',
			'OwnerId',
			'RecordTypeId',
			'Access_Token_URL__c',
			'Active__c',
			'Authorization_URL__c',
			'Request_Token_URL__c'};
	}

    /**
	* Get a record by given record type Id
	* @param recordtypeId
	* @return OAuth_Service__c
	*
	*/

    public OAuth_Service__c getOAuthServiceByRecordTypeId (Id recordTypeId){
        SimpleQueryFactory qf = simpleQuery()
                //.selectFields(getSObjectFieldList())
                .selectFields(getDefaultFields())
                .setCondition('RecordTypeId =: recordTypeId');
        List<OAuth_Service__c> tempList = Database.query(qf.toSOQL());
        if (tempList != null && !tempList.isEmpty()){
            return tempList.get(0);
        }
        return null;
    }

}