public class Paginator{
    public Integer currPage; //Current Page number.
    public Integer totalPages; //Total Page numbers.
    private List<Object> objs; //Total search result.
    private List<Object> pagedResult; //10 Recs for per page.
    public Integer recPerPage=10; // The number to show per page.
    
    public Paginator(List<Object> objs){
        this();
        paginateCollection(objs);
    }
    
    //change the number of record per page
    public void changerecPerpage(Integer tempRecPerpage){
        recPerPage = tempRecPerpage;
    }
    
    public Paginator(){
        currPage = 0;
        totalPages = 0;
        pagedResult = new List<Object>();
    }
    
    public List<Object> paginateCollection(List<Object> objs){
        this.objs = objs;
        Decimal totalNum=objs.size();
        Decimal decTotalPageNum =totalNum / recPerPage;
        totalPages = decTotalPageNum.round(System.RoundingMode.CEILING).intvalue();
        if(totalNum>0){
            currPage=1;
        }else
            currPage=0;
        return getNthPageCollection(currPage);
    }

    //Return Page numbers that will be shown on the page 
    public List<Integer> getPagesNums(){
        List<Integer> result = new List<Integer>();
        if(totalPages >=5 && currPage <=2){
            for(Integer i=1; i <=5; i++){
                result.add(i);
            }
        }else{
            for(Integer i = Math.max(1, currPage-2); i <= Math.min(currPage+2, totalPages); i++){
                result.add(i);
            }
        }
        return result;
    }
    
    public List<Object> getNthPageCollection(Integer page){
        pagedResult = new List<Object>();
        if(page>totalPages || page <1)
            return pagedResult;
        currPage = page;
        Integer startIndex = (page -1)* recPerPage;
        Integer endIndex = page * recPerPage -1;
        for(Integer i=startIndex;i<objs.size() && i<=endIndex;i++){
            pagedResult.add(objs.get(i));
        }
        return pagedResult;
    }
    
    // go to the next page
    public List<Object> getNextPageCollection(){
        if(currPage<totalPages){
            currPage++;
            pagedResult = getNthPageCollection(currPage);
        }
        //system.debug('*********** current page' + currPage);
        return pagedResult;
    }
    
    // go to the previous page
    public List<Object> getPrevPageCollection(){
        if(currPage>1){
            currPage--;
            pagedResult = getNthPageCollection(currPage);
        }
        //system.debug('^^^^^^^^^^^^^ current page' + currPage);
        return pagedResult;
    }    
    
    // go to the first page
    public List<Object> getFirstPageCollection(){
        currPage = 1;
        //system.debug('^^^^^^^^^^^^^ current page' + currPage);
        return paginateCollection(objs);
    }
    
    // go to the last page
    public List<Object> getLastPageCollection(){
        pagedResult = getNthPageCollection(totalPages);
        currPage = totalPages;
        //system.debug('^^^^^^^^^^^^^ current page' + currPage);
        return pagedResult;
    }      

    
    public Integer getCurrentPage(){
        return currPage;
    }
    
    public Boolean hasNextPage(){
        return currPage<totalPages;
    }

    public Boolean hasPrevPage(){
        return currPage>1;
    }   
    
    public Integer getTotalRecNumber(){
        return objs.size();
    }
    
    
    static testmethod void testPaginator(){
        System.runAs(DummyRecordCreator.platformUser) {
            Paginator p = new Paginator(new List<Object>());
            system.assertEquals(p.getCurrentPage(),0);
            system.assertNotEquals(p.getFirstPageCollection(),null);
            system.assertEquals(p.getFirstPageCollection().size(),0);
            DataTestFactory.createContactList(11);
            List<Contact> contactList = DaoContact.getContactsbyName('test');
            Paginator pp = new Paginator(contactList);

            pp.paginateCollection(contactList);
            system.assertEquals(pp.currPage, 1);
            pp.hasPrevPage();
            pp.hasNextPage();
            pp.getTotalRecNumber();
            pp.getPagesNums();
            pp.getNextPageCollection();
            system.assertEquals(pp.currPage, 2);
            pp.getPrevPageCollection();
            system.assertEquals(pp.currPage, 1);
            pp.getNthPageCollection(2);
            system.assertEquals(pp.currPage, 2);
        }
    }
}