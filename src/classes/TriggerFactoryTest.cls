@isTest
public class TriggerFactoryTest {
    public static testMethod void unitTest(){  
    	System.runAs(DummyRecordCreator.platformUser) {
        PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
       	pcSwitch.Delete_Roster_After_Delete_Shift_Trigger__c = true;
       	pcSwitch.Update_Placed_Positions_Under_Shift__c = true;
        upsert pcSwitch;
        Account acc = TestDataFactoryRoster.createAccount();
    	Contact con = TestDataFactoryRoster.createContact();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        Days_Unavailable__c roster = TestDataFactoryRoster.createRoster(con, shift, acc,'Filled');
        shift.Weekly_Recurrence__c = true;
        update shift;
        roster.Event_Status__c = 'Available';
        update roster;
        delete shift;
        system.assertEquals([select id from shift__c].size(),0);
        undelete roster;
        system.assertEquals([select id from Days_Unavailable__c].size(),1);
    	}
    }
}