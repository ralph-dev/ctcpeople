@RestResource(urlMapping='/credential/*')
global with sharing class CredentialSettingREST {
    
    @HttpGet
    global static ProtectedCredential getProtectedCrendential(){
        
        RestRequest req = RestContext.request;
        String name  = req.params.get('name');

        
        
        ProtectedCredential pc =  CredentialSettingService.getCredential( name);
        
        if(pc == null){
            RestResponse res = RestContext.response;

            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Failed to get credential - ' + name );
        }
        
        return pc;
        
    }
   
    
    @HttpPost
    global static ProtectedCredential saveProtectedCrendential(String orgId,String id,String name,String type,String provider,String username,String password,String token,String key,String secret,String relatedTo){
        
        ProtectedCredential pc = new ProtectedCredential();
    
        pc.name = name;
        pc.type = type;
        pc.provider = provider;
        pc.username = username;
        pc.password = password;
        pc.token = token;
        pc.key = key;
        pc.secret = secret;
        pc.relatedTo = relatedTo;
        
        if(pc.name == 'Daxtra Feed SQS'){
        	//daxtra sqs credential;
        	SQSCredential__c sqsNew = new SQSCredential__c();
            sqsNew.URL__c = username;
            sqsNew.Access_Key__c = key;
            sqsNew.Secret_Key__c = secret;
            sqsNew.Name = name;
            
            SQSCredentialSelector selector = new SQSCredentialSelector();
	    	SQSCredential__c sqsCred = selector.getSQSCredential('Daxtra Feed SQS');
	    	if (sqsCred == null) {
	    		insert sqsNew;
	    	}
               
            
        }else{
        	pc = CredentialSettingService.upsertCredential( pc);
        }
        
        
        
        if(pc == null){
            RestResponse res = RestContext.response;

            // set the status code and response body for test case
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Failed to save credentials - ' + name +' related to ' + relatedTo);
        }else{
            
        }
        
        return pc;
        
        
    }
    
   

}