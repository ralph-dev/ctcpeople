/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class InvoiceItemImplementationTest {

    static testMethod void myUnitTests() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	Integer recordNumber=10;
    	list<Invoice_Line_Item__c> invoiceLineItems=new list<Invoice_Line_Item__c>();
    	list<String> invoiceItemIds=new list<String>();
    	if(invoiceLineItems.size()==0){
			for(Integer i=0;i<recordNumber;i++){
	    		Invoice_Line_Item__c invoiceLineItem=new Invoice_Line_Item__c();
	    		invoiceLineItem.Vacancy__c=DataTestFactory.createVacancies().get(i).Id;
	    		if(math.mod(i,2)==0){
	    			invoiceLineItem.Is_Ready_For_Astute_Payroll__c=true;
	    		}else{
	    			invoiceLineItem.Is_Pushing_To_AP__c=true;
	    		}
	    		invoiceItemIds.add(invoiceLineItem.id);
	    		invoiceLineItems.add(invoiceLineItem);
	    	}
		}
    	InvoiceItemAstutePayrollImplementation.astutePayrollTrigger(invoiceLineItems);
    	InvoiceItemAstutePayrollImplementation.syncInvoiceLineItems(invoiceItemIds, UserInfo.getSessionId());
    	System.assertEquals(invoiceLineItems.get(0).Astute_Payroll_Upload_Status__c,'On Hold');
    	}
    }
}