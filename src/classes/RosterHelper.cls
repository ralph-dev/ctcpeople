public with sharing class RosterHelper {
	/**
	* treat each day as a single roster, if two days are adjacent, merge these two rosters
	* 
	**/
	public static void mergeRoster(List<Days_Unavailable__c> rosterListForDisplay, Days_Unavailable__c sglRoster){
		Days_Unavailable__c lastRoster = new Days_Unavailable__c();
		
		Date sglRosterStartDate = sglRoster.Start_Date__c;
		System.debug('sglRosterStartDate: '+sglRosterStartDate);
		Date sglRosterEndDate = sglRoster.End_Date__c;
		
		if(rosterListForDisplay.size() == 0) {
            rosterListForDisplay.add(sglRoster);
        }else{
            lastRoster = rosterListForDisplay[rosterListForDisplay.size() - 1 ];
          	Date lastRosterEndDate = lastRoster.End_Date__c;
          	System.debug('lastRosterEndDate: '+lastRosterEndDate);
            System.debug('days between: '+sglRosterStartDate.daysBetween(lastRosterEndDate));
            if(sglRosterStartDate.daysBetween(lastRosterEndDate) == -1) {
                lastRoster.End_Date__c = sglRoster.End_Date__c; 
            }else{
                /*
                 If lastEvent.end - sglDayEvent.start == 0, that means a duplicate
                 event is inserted, the duplicated event is same as previously inserted event.
                 Duplicate events exist when users choose dates
                 shown on both calendars. This is possible as left calendar may show
                 first several days of next month.
                 */
                rosterListForDisplay.add(sglRoster);
            }
        }
    }
    
   
	/**
	* deserialize json string to sObject or sObject list
	*
	**/
	public static List<RosterDTO> deserializeRosterJson(String rosterJson){
		List<RosterDTO> rosterDTOs = new List<RosterDTO>();
		try{
			rosterDTOs = (List<RosterDTO>)JSON.deserialize(rosterJson,List<RosterDTO>.class);
		}catch(Exception e){
			RosterDTO rosterObj = (RosterDTO)JSON.deserialize(rosterJson,RosterDTO.class);
			rosterDTOs.add(rosterObj);
		}
		return rosterDTOs;
	}
	
	/**
	* deserialize roster param json string to list of RosterParamDTO
	*
	**/
	public static List<RosterParamDTO> deserializeRosterParamJson(String rParamJson){
		List<RosterParamDTO> rParamDTOs = new List<RosterParamDTO>();
		try{
			rParamDTOs = (List<RosterParamDTO>)JSON.deserialize(rParamJson,List<RosterParamDTO>.class);
		}catch(Exception e){
			RosterParamDTO rParamObj = (RosterParamDTO)JSON.deserialize(rParamJson,RosterParamDTO.class);
			rParamDTOs.add(rParamObj);
		}
		return rParamDTOs;
	}
	
	public static void convertRosterDTOToRosterSObjectForRosterDB(Days_Unavailable__c r, RosterDTO rDBDTO){
		r.Location__c = rDBDTO.getLocation();
		r.Shift_Type__c = rDBDTO.getShiftType();
		r.Recurrence_Weekdays__c = rDBDTO.getWeekdaysDisplay();
		
		if(rDBDTO.getStartDate() != '' && rDBDTO.getStartDate() != null && rDBDTO.getStartDate() != 'Invalid date'){	
			r.Start_Date__c = Date.valueOf(rDBDTO.getStartDate());
		}else{
			r.Start_Date__c = null;	
		}
		if(rDBDTO.getStartTime() != '' && rDBDTO.getStartTime() != null && rDBDTO.getStartTime() != 'Invalid date'){
			r.Start_Time__c = rDBDTO.getStartTime();
		}else{
			r.Start_Time__c = '';
		}
		if(rDBDTO.getEndDate() != '' && rDBDTO.getEndDate() != null && rDBDTO.getEndDate() != 'Invalid date'){	
			r.End_Date__c = Date.valueOf(rDBDTO.getEndDate());
			r.Recurrence_End_Date__c = Date.valueOf(rDBDTO.getEndDate());
		}else{
			r.Recurrence_End_Date__c = null;	
		}
		if(rDBDTO.getEndTime() != '' && rDBDTO.getEndTime() != null && rDBDTO.getEndTime() != 'Invalid date'){	
			r.End_Time__c = rDBDTO.getEndTime();
		}else{
			r.End_Time__c = '';
		}
		if(rDBDTO.getShiftId() != '' && rDBDTO.getShiftId() != null){
			r.Shift__c = rDBDTO.getShiftId();
		}else{
			r.Shift__c = null;
		}
		if(rDBDTO.getClientId() != '' && rDBDTO.getClientId() != null){
			r.Accounts__c = rDBDTO.getClientId();
		}else{
			r.Accounts__c = null;
		}
		if(rDBDTO.getCandidateId() != '' && rDBDTO.getCandidateId() != null){
			r.Contact__c = rDBDTO.getCandidateId();
		}
		if(rDBDTO.getCmId() != null && rDBDTO.getCmId() != ''){
			r.Candidate_Management__c = rDBDTO.getCmId();
		}else{
			r.Candidate_Management__c = null;
		}
		if(rDBDTO.getShiftType() != null && rDBDTO.getShiftType() != ''){
			r.Shift_Type__c = rDBDTO.getShiftType();
		}else{
			r.Shift_Type__c = '';
		}
		if(rDBDTO.getStatus() != null && rDBDTO.getStatus() != ''){
			r.Event_Status__c = rDBDTO.getStatus();
		}else{
			r.Event_Status__c = '';
		}
		if(rDBDTO.getId() != null && rDBDTO.getId() != ''){
			r.Id = rDBDTO.getId();
		} 
		if(rDBDTO.getRate() != null && rDBDTO.getRate() != ''){
			 r.Rate__c = Decimal.valueOf(rDBDTO.getRate());
		}else{
			r.Rate__c = null;
		}
		
	}
	
	public static void assignValuesToRoster(Days_Unavailable__c sglRoster, Days_Unavailable__c roster){
		sglRoster.Id = roster.Id;
		sglRoster.Start_Time__c = roster.Start_Time__c;
		sglRoster.End_Time__c = roster.End_Time__c;
		sglRoster.Location__c = roster.Location__c;
		sglRoster.Contact__c = roster.Contact__c;
		sglRoster.Accounts__c = roster.Accounts__c;
		sglRoster.Candidate_Management__c = roster.Candidate_Management__c;
		sglRoster.Shift__c = roster.Shift__c;
		sglRoster.Shift_Type__c = roster.Shift_Type__c;
		sglRoster.Weekly_Recurrence__c = roster.Weekly_Recurrence__c;
		sglRoster.Recurrence_Weekdays__c = roster.Recurrence_Weekdays__c;
		sglRoster.Recurrence_End_Date__c = roster.Recurrence_End_Date__c;
		sglRoster.Event_Status__c = roster.Event_Status__c;
		//System.debug('roster.Shift__r.Vacancy__r.Name: '+roster.Shift__r.Vacancy__r.Name);
		sglRoster.Vacancy_Name_For_Display_On_Wizard__c = roster.Shift__r.Vacancy__r.Name;
		sglRoster.Contact_Name_For_Wizard_Display__c = roster.Contact__r.Name;
		sglRoster.Account_Name_For_Display_On_Wizard__c = roster.Accounts__r.Name;	
		sglRoster.recordTypeId = roster.recordTypeId;
		sglRoster.Rate__c = roster.Rate__c;
	}

	/**
	* In the Shift update wizard, End Date is Recurrence End Date, if Recurrence End Date is early
	* than End Date, the End Date need to be updated to Recurrence End Date. Otherwise, End Date
	* keep the same value.
	*
	**/
	public static Boolean checkEndDateNeedToBeUpdated(List<Days_Unavailable__c> rosterList){
		Boolean isEndDateNeedToBeUpdated = false;
			
			for(Days_Unavailable__c r : rosterList){
				if(r.Recurrence_End_Date__c != null && r.End_Date__c != null){
					Integer diff  = WizardUtils.getDiffBetweenTwoDates(r.End_Date__c, r.Recurrence_End_Date__c);
					System.debug('diff: '+diff);
					if(diff < 0){
						r.End_Date__c = r.Recurrence_End_Date__c;
						isEndDateNeedToBeUpdated = true;
						System.debug('isEndDateNeedToBeUpdated: '+isEndDateNeedToBeUpdated);
					}
				}			
			}
		return isEndDateNeedToBeUpdated;
	}
	
	/**
	* In the Shift update wizard, End Date is Recurrence End Date, if Recurrence End Date is early
	* than End Date, the End Date need to be updated to Recurrence End Date. Otherwise, End Date
	* keep the same value.
	*
	**/
	public static List<Days_Unavailable__c> updateRosterEndDate(List<Days_Unavailable__c> rosterList){	
			List<Days_Unavailable__c> rostersToReturn = new List<Days_Unavailable__c>();
			
			for(Days_Unavailable__c r : rosterList){
				if(r.Recurrence_End_Date__c == null && r.Start_Date__c == null){
					return rosterList;
				}
				
				Date endDate = WizardUtils.getEndDateByStartDateAndWeekdays(r.Start_Date__c);
				Integer endDateRecurEndDateDiff  = WizardUtils.getDiffBetweenTwoDates(endDate, r.Recurrence_End_Date__c);
			
				if(endDateRecurEndDateDiff < 0){
					r.End_Date__c = r.Recurrence_End_Date__c;
				}else{
					r.End_Date__c = endDate;
				}
				
				rostersToReturn.add(r);	
			}
			
			return rostersToReturn;
			
	}		
}