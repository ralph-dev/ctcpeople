public with sharing class RosterService {
	/**
	* get roster list for display by a candidate id
	*
	**/
	public List<Days_Unavailable__c> getTotalDisplayRosterList(List<Days_Unavailable__c> rosterList){
		// split single roster into roster list and saved in the collection of roster list
		RosterDomain rDomain = new RosterDomain(rosterList);
		List<Days_Unavailable__c> rosterListForDisplayTotal = rDomain.splitRosterForDisplay();
		
		return rosterListForDisplayTotal;
	}
	/**
	* update roster that is stored in the SF
	* the argument is still RosterDTO.class not Days_Unavailable__c.class
	*
	**/
	public List<Days_Unavailable__c> updateRosterStoredInDB(List<RosterDTO> rostersTBUpdated){		
		//System.debug('rostersTBUpdated: '+rostersTBUpdated);
		if(rostersTBUpdated == null || rostersTBUpdated.size() == 0){
			return null;
		}
		
		List<Days_Unavailable__c> rosters = new List<Days_Unavailable__c>();
		List<String> rosterIds = new List<String>();
		
		for(RosterDTO obj : rostersTBUpdated){
			Days_Unavailable__c roster = new Days_Unavailable__c();	
			RosterHelper.convertRosterDTOToRosterSObjectForRosterDB(roster,obj);
			rosters.add(roster);
			rosterIds.add(roster.Id);		
		}
		
		List<Days_Unavailable__c> rostersWithUpdatedEndDate = RosterHelper.updateRosterEndDate(rosters);
		//System.debug('rostersWithUpdatedEndDate: '+rostersWithUpdatedEndDate);
		
		RosterSelector rSelector = new RosterSelector();
		List<Days_Unavailable__c> rostersToReturn = new List<Days_Unavailable__c>();
		if(rostersWithUpdatedEndDate != null && rostersWithUpdatedEndDate.size()>0){
			Commonselector.quickUpsert(rostersWithUpdatedEndDate, 'End_Date__c');
			//upsert rostersWithUpdatedEndDate;
			
			//System.debug('rostersToReturn: '+rostersToReturn);
			rostersToReturn = rSelector.fetchRosterListByRosterIdList(rosterIds);
		}

		return rostersToReturn;
	}
	
	/**
	* delete Shift that is stored in the SF
	* 
	**/
	public Boolean deleteRosterById(String rosterId){
		System.debug('Roster Id : '+rosterId);
		boolean isSuccessDeleted = false;
		
		if(rosterId == null || rosterId == ''){
			return isSuccessDeleted;
		}
		
		RosterSelector rSelector = new RosterSelector();
		List<Days_Unavailable__c> rosterList = rSelector.fetchRosterListByRosterId(rosterId);
		
		if(rosterList != null && rosterList.size()>0){
			isSuccessDeleted = true;
			Commonselector.quickDelete(rosterList);
			//delete rosterList;
		}
		
		return isSuccessDeleted;		
	}
	
	/**
	* get single unavailable date for candidate
	*
	**/ 
	public List<String> getUnavailableDateStrListForCandidate(String canId){
		RosterSelector rSelector = new RosterSelector();
		List<Days_Unavailable__c> unavailCanList = rSelector.fetchUnavailableCandidateList(canId);
		
		//for a candidate, one unavailable record is a list of dateString
		//multi unavailable records is a list<list<String>>
		List<List<String>> dateStrListCollection = new List<List<String>>();		
		for(Days_Unavailable__c unavailCan : unavailCanList){
			List<String> dateStrList =  WizardUtils.getDateStrListForDateRange(unavailCan.Start_Date__c, unavailCan.End_Date__c);
			dateStrListCollection.add(dateStrList);
		}
		
		List<String> dateStrListToReturn = new List<String>();
		for(List<String> sglList : dateStrListCollection){
			for(String sglString : sglList){
				dateStrListToReturn.add(sglString);
			}
		}
		//System.debug('dateStrListToReturn'+dateStrListToReturn);
		
		return dateStrListToReturn;
	}
	
	/**
	* Under shift, when one of the fields (Start_Date__c, End_Date__c, Start_Time__c, End_Time__c)
	* (Recurrence_End_Date__c, Weekly_Recurrence__c, Recurrence_Weekdays__c, Shift_Type__c) change value	
	* need to update related roster to keep consistency.
	*
	**/
	public void updateRosterWhenShiftCertianFieldsChange(List<String> shiftIdList){
		ShiftSelector sSelector = new ShiftSelector();
		//select shifts based on shiftIdList
		List<Shift__c> shiftList = sSelector.fetchShiftListByShiftIdList(shiftIdList);
		
		//convert shiftList to shiftMap
		Map<String,Shift__c> shiftMap = new Map<String,Shift__c>();
		for(Shift__c s : shiftList){
			shiftMap.put(s.Id,s);
		}
		
		RosterSelector rSelector = new RosterSelector();
		
		//select all related rosters
		List<Days_Unavailable__c> rosterNeedToBeUpdated = rSelector.fetchRosterListByShiftIdList(shiftIdList);
		RosterDomain rDomain = new RosterDomain(rosterNeedToBeUpdated);
		//update all related rosters to make it consisitent with related rosters
		rDomain.updateRosterBasedOnShift(shiftMap);
	}
}