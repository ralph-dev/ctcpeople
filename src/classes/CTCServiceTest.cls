@isTest
public class CTCServiceTest {
   
    private static  Advertisement__c adv;
    private static  StyleCategory__c sc;
    public static testMethod void testCTCService(){ 
    	System.runAs(DummyRecordCreator.platformUser) {  
        sc = new StyleCategory__c();
        sc.Div_Html_EC2_File__c = 'test';
        insert sc;
        adv = DataTestFactory.createAdvertisement();
        
        System.debug('adv id = ' + adv.id);
        
        calloutInsert( );
        
        sc.Footer_EC2_File__c = 'test';
        sc.Footer_File_Type__c = 'test' ;
        sc.Header_EC2_File__c = 'test';
        sc.Header_File_Type__c = 'test';
        update sc;
        
       calloutUpdate();
        
        List<StyleCategory__c> scList1 = CTCService.getOptions(1, 'careerone');
        List<StyleCategory__c> scList2 = CTCService.getOptions(1, 'seek');
        List<StyleCategory__c> scList3 = CTCService.getOptions(1, 'jxt');
        List<StyleCategory__c> scList4 = CTCService.getOptions(1, 'templateStyle');
        List<StyleCategory__c> scList5 = CTCService.getOptions(1, 'jxt_nz');
        List<StyleCategory__c> scList6 = CTCService.getOptions(2, 'jxt');
        List<StyleCategory__c> scList7= CTCService.getOptions(3, 'jxt');
        sc.Name = 'test';
        sc.Enable_Enhanced_Listing_MyCareer__c = true;
        sc.Enable_Priority_Listing_MyCareer__c = true;
        sc.MyCareer_Priority_Listing_Quota__c = 10;
        sc.MyCareer_StandOut_Quota__c = 100;
        sc.Account_Active__c = true;
        SC.WebSite__c = 'MyCareer';
        sc.RecordTypeId =[select id from recordtype where developername ='WebSite_Admin_Record'].Id;
        update sc;
        adv.RecordTypeId = [select id from recordtype where developername ='MyCareer_com_Advertisement'].Id;
        adv.Status__c = 'Active';
        update adv;
        List<Boolean> checkAccess = CTCService.CheckMyCareerAccess();
    	}
    }
    
    @Future(callout=true)
    private static void calloutInsert( ){
       
        CTCService ctc = new CTCService();
        Test.setMock(WebServiceMock.class, new WebServiceMockImpls.CTCWSMockImpl4InsertJobPostingDetailSuccess());
        
        
        Boolean theCallout = ctc.theCallout(adv.Id, adv.Id, 'test', sc.Id, 'test', false, 'careerone');
        system.assert(theCallout);
        system.debug('theCallout ='+theCallout);
    }
    
    @Future(callout=true)
    private static void calloutUpdate( ){
        
        CTCService ctc = new CTCService();
        Test.setMock(WebServiceMock.class, new WebServiceMockImpls.CTCWSMockImpl4UpdateJobPostingDetailSuccess());
        
        Boolean theCallout2 = ctc.theCallout(adv.Id, adv.Id, 'test', sc.Id, 'test', true, 'careerone');
        system.assert(theCallout2);
        
    }

}