/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

public with sharing class SaveGSASearch {
	/*public String vid{get; set;}
	public GSA_Search__c gs{get; set;}
	public boolean showSave{get; set;}
	public String errorInfo{get; set;}
	public SaveGSASearch(){
		showSave = true;
		Cookie searchCreCookie = ApexPages.currentPage().getCookies().get('searchCre');
		Cookie keywordsCookie = ApexPages.currentPage().getCookies().get('encodedKeywords');
		Cookie advancedCookie = ApexPages.currentPage().getCookies().get('advanced');
		Cookie singleCriteriaJsonCookie = ApexPages.currentPage().getCookies().get('singleCriteriaJson');
		Cookie multipleCriteriaJsonCookie = ApexPages.currentPage().getCookies().get('multipleCriteriaJson');
		if(searchCreCookie==null || searchCreCookie.getValue()=='' || searchCreCookie.getValue()=='null'){
			showSave = false; // show save new search section.
		}else{
			gs = new GSA_Search__c();
			gs.search_URL__c = searchCreCookie.getValue(); // get url from cookie
			gs.key_words__c = keywordsCookie.getValue();  //get keywords from cookie
			gs.Advanced__c = Boolean.valueOf(advancedCookie.getValue()); //get advanced from cookie.
			gs.singleCriteriaJson__c=singleCriteriaJsonCookie.getValue(); // ... singleCriteria
			gs.multipleCriteriaJson__c = multipleCriteriaJsonCookie.getValue(); //...multipleCriteria
			//System.debug('multipleCriteriaJsonCookie.getValue() = '+multipleCriteriaJsonCookie.getValue());
			vid = ApexPages.currentPage().getParameters().get('vid');
		}
	}
	public PageReference saveGSA(){
		List<Cookie> cookieList = new List<Cookie>();
		Cookie fromCookie = new Cookie('from', 'save', null, -1, false);
		cookieList.add(fromCookie);
		ApexPages.currentPage().setCookies(cookieList);
		PageReference gsaSearch = Page.googleSearch2;
		gsaSearch.getParameters().put('from', 'save');
		gsaSearch.getParameters().put('id', vid);
		gsaSearch.setRedirect(true);
		try{
			//check FLS
			List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
				GSA_Search__c.Search_URL__c,
				GSA_Search__c.Key_words__c,
				GSA_Search__c.Advanced__c,
				GSA_Search__c.singleCriteriaJson__c,
				GSA_Search__c.multipleCriteriaJson__c
			};
			fflib_SecurityUtils.checkInsert(GSA_Search__c.SObjectType, fieldList);
			insert gs;
		}catch(System.DMLException e){
			errorInfo = 'the name exist already, please choose other name';
			return null;
		}
		
		return gsaSearch; 
	}
	
	public List<GSA_Search__c> getGSARecords(){
		CommonSelector.checkRead(GSA_Search__c.SObjectType,'Id, Search_Name__c');
		List<GSA_Search__c> records = [select Id, Search_Name__c from GSA_Search__c where CreatedById=:UserInfo.getUserId()];
		return records;
	}
	
	public PageReference googleSearch(){
		String searchName = ApexPages.currentPage().getParameters().get('Search_Name__c');
		CommonSelector.checkRead(GSA_Search__c.SObjectType,'Id, Search_Name__c, search_URL__c, key_words__c, Advanced__c, singleCriteriaJson__c, multipleCriteriaJson__c');
		GSA_Search__c gsaSearch = [select Id, Search_Name__c, search_URL__c, key_words__c, Advanced__c, singleCriteriaJson__c, multipleCriteriaJson__c from GSA_Search__c where Search_Name__c=:searchName limit 1];
		String vid = ApexPages.currentPage().getParameters().get('vid');	 // get vid from link in url
		System.debug('------------------------------------------------------------');
		//System.debug('singleCriteriaJson = '+gsaSearch.singleCriteriaJson__c);
		//System.debug('multipleCriteriaJson in googleSearch = '+gsaSearch.multipleCriteriaJson__c);
		List<Cookie> cookieList = new List<Cookie>();
		//System.debug('gurl = '+gsaSearch.search_URL__c); 
		Cookie gsaUrlCookie = new Cookie('searchCre',gsaSearch.search_URL__c, null, -1, false); // put the gurl to cookie
		Cookie fromCookie = new Cookie('from','save', null, -1, false); // put the from to cookie
		Cookie keywordsCookie = new Cookie('encodedKeywords', gsaSearch.key_words__c, null, -1, false);
		Cookie advancedCookie = new Cookie('advanced', String.valueOf(gsaSearch.Advanced__c), null, -1, false);
		Cookie singleCriteriaJsonCookie = new Cookie('singleCriteriaJson', gsaSearch.singleCriteriaJson__c, null, -1, false);
		Cookie multipleCriteriaJsonCookie= new Cookie('multipleCriteriaJson', gsaSearch.multipleCriteriaJson__c, null, -1, false);
		cookieList.add(gsaUrlCookie);
		cookieList.add(fromCookie);
		cookieList.add(keywordsCookie);
		cookieList.add(advancedCookie);
		cookieList.add(singleCriteriaJsonCookie);
		cookieList.add(multipleCriteriaJsonCookie);
		PageReference googleSearch = Page.googleSearch2;
		googleSearch.getParameters().put('from', 'save');  // put from=save to url
		googleSearch.getParameters().put('id', vid);  // put vid to url
		ApexPages.currentPage().setCookies(cookieList);
		googleSearch.setRedirect(true);
		return googleSearch;
	}
	
	public PageReference deleteSearch(){
		String sid = ApexPages.currentPage().getParameters().get('sid');
		CommonSelector.checkRead(GSA_Search__c.SObjectType,'Id');
		GSA_Search__c gsaSearch = [select Id from GSA_Search__c where Id=:sid limit 1];
		fflib_SecurityUtils.checkObjectIsDeletable(gsaSearch.getSObjectType());
		delete gsaSearch;
		return null;
	}
	public PageReference editSearch(){
		String searchName = ApexPages.currentPage().getParameters().get('Search_Name__c');
		CommonSelector.checkRead(GSA_Search__c.SObjectType,'Id, Search_Name__c');
		GSA_Search__c gsaSearch = [select Id, Search_Name__c from GSA_Search__c where Search_Name__c=:searchName limit 1];
		PageReference editSearch = Page.editGSASearch;
		editSearch.getParameters().put('id', gsaSearch.Id);
		editSearch.getParameters().put('vid', vid);
		editSearch.setRedirect(true);
		return  editSearch;
	}
	
	public PageReference back2googleSearch(){
		PageReference googleSearch = Page.googleSearch2;
		googleSearch.getParameters().put('id', vid);  // put vid to url
		googleSearch.setRedirect(true);
		return googleSearch;
	}*/
	
}