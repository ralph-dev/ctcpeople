@isTest
public with sharing class UpdateReminingTriggerTest {
	private static testmethod void runTestUpdateReminingTrigger(){
		System.runAs(DummyRecordCreator.platformUser) {
		List<Placement_Candidate__c> testdatas = preparedata();
		insert testdatas;
		system.debug('testdatas (test)= ' + testdatas);
		List<Placement_Candidate__c> testdatasOld = new List<Placement_Candidate__c>(testdatas);
		List<String> placedCandidates = new List<String>();
		List<Placement_Candidate__c> updatecms = new List<Placement_Candidate__c>();
		//testdatas = [select id, Candidate_Status__c, Show_Alert__c, Placement__c from Placement_Candidate__c where id in: testdatas];
		//system.debug('testdatas (test2)= ' + testdatas);
		for(Placement_Candidate__c testsignaldata : testdatas){
			if(testsignaldata.Candidate_Status__c == 'Placed'){
				placedCandidates.add(testsignaldata.id);
				system.assertEquals(testsignaldata.Show_Alert__c, false);
			}
			else{
				testsignaldata.Candidate_Status__c = 'Placed';
				updatecms.add(testsignaldata);
			}
		}
		system.debug('updatecms (test)=' +updatecms);
		if(placedCandidates!=null && placedCandidates.size()>0){
			List<Placement__c> testvacancylist = [select id, With_Candidate_Placed__c from Placement__c where id in: placedCandidates];
			for(Placement__c testvacancy : testvacancylist){
				system.assertEquals(testvacancy.With_Candidate_Placed__c, true);
			}
		}
		
		update updatecms;
		List<String> updateplacedCandidates = new List<String>();
		for(Placement_Candidate__c testsignaldata : updatecms){
			if(testsignaldata.Candidate_Status__c == 'Placed'){
				placedCandidates.add(testsignaldata.id);
				system.assertEquals(testsignaldata.Show_Alert__c, false);
			}
		}
		if(placedCandidates!=null && placedCandidates.size()>0){
			List<Placement__c> testvacancylist = [select id, With_Candidate_Placed__c from Placement__c where id in: placedCandidates];
			for(Placement__c testvacancy : testvacancylist){
				system.assertEquals(testvacancy.With_Candidate_Placed__c, true);
			}
		}
		system.debug('testdatas (test2)= ' + testdatas);
		system.debug('updatecms (test2)=' +updatecms);
		UpdateReminingTrigger.UpdateRemingTrigger( updatecms, testdatas, true);
		UpdateReminingTrigger.UpdateRemingTrigger(updatecms, testdatas,false);
		}
	}
	

	
	private static List<Placement_Candidate__c> preparedata(){
		// insert a new account	
        Account acct = new Account(Name = 'test');        
        insert acct ; 
        
        // insert new vacancies
        List<Placement__c> vanList = new List<Placement__c>();
        Placement__c v1 = new Placement__c(Company__c=acct.Id,Name='vacancy1');
        Placement__c v2 = new Placement__c(Company__c=acct.Id,Name='vacancy2');
        vanList.add(v1);
        vanList.add(v2);
        insert vanList;    
         
        // insert new candidate
        List<Contact> conList = new List<Contact>();
        Contact con1 = new Contact(LastName = 'con1', Email = 'test1@user2test.com');        
        Contact con2 = new Contact(LastName = 'con2', Email = 'test2@user2test.com'); 
        Contact con3 = new Contact(LastName = 'con3', Email = 'test3@user2test.com'); 
        Contact con4 = new Contact(LastName = 'con4', Email = 'test4@user2test.com');        
        Contact con5 = new Contact(LastName = 'con5', Email = 'test5@user2test.com'); 
        Contact con6 = new Contact(LastName = 'con6', Email = 'test6@user2test.com');
        Contact con7 = new Contact(LastName = 'con7', Email = 'test7@user2test.com');
        conList.add(con1); 
        conList.add(con2);
        conList.add(con3);
        conList.add(con4);
        conList.add(con5);
        conList.add(con6);
        conList.add(con7);
        insert conList;
        
        
        // insert new candidate managements
        List<Placement_Candidate__c> cmList = new List<Placement_Candidate__c>();
        Placement_Candidate__c cm1 = new Placement_Candidate__c(Placement__c = v1.Id ,Candidate__c = con1.Id, Status__c = 'First Interview'); 
        Placement_Candidate__c cm2 = new Placement_Candidate__c(Placement__c = v2.Id ,Candidate__c = con2.Id, Status__c = 'First Interview'); 
        Placement_Candidate__c cm3 = new Placement_Candidate__c(Placement__c = v1.Id ,Candidate__c = con3.Id, Candidate_Status__c ='First Interview'); 
        Placement_Candidate__c cm4 = new Placement_Candidate__c(Placement__c = v2.Id ,Candidate__c = con4.Id, Candidate_Status__c ='First Interview'); 
        cmList.add(cm1);
        cmList.add(cm2);
        cmList.add(cm3);
        cmList.add(cm4);
        
        return cmList;
	}
}