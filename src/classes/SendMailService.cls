public with sharing class SendMailService {
	public static final Integer TO_BE_SENT = 0;
	public static final Integer SEND_SUCC=100;
	public static final Integer SEND_FAIL=200;
	public static final Integer SEND_SUCC_PARTIAL=300;
	public static final Integer SEND_FAIL_ADD_EMPTY=201;
	public static final Integer SEND_FAIL_ATTACHMENT_ERR=202;
	
	public static final String TO_BE_SENT_MSG='To Be Sent';
	public static final String SEND_MAIL_SUCC_MSG='EMail has been sent';
	public static final String SEND_SUCC_PARTIAL_MSG='EMails has been sent';
	public static final String SEND_FAIL_ADD_EMPTY_MSG='Mail address is empty';
	public static final String SEND_FAIL_MSG='Unable to send your email';
	public static final String SEND_FAIL_ATTACHMENT_ERR_MSG='Errors in handling attachment';
	
	public static final String DOC_TYPE_PDF='application/pdf';
	public static final String DOC_TYPE_DOC='application/msword';
	public static final String DOC_TYPE_RTF='application/rtf';
	public static final String DOC_TYPE_DOCX='application/vnd.msword.document.12';
	public static final String DOC_TYPE_TXT='text/plain';
	public static final String DOC_TYPE_HTML='text/html';
	
	public static String getSendMailMsg(Integer code){
		Map<Integer,String> codeMap = new Map<Integer,String>();
		codeMap.put(TO_BE_SENT,TO_BE_SENT_MSG);
		codeMap.put(SEND_SUCC,SEND_MAIL_SUCC_MSG);
		codeMap.put(SEND_FAIL,SEND_FAIL_MSG);
		codeMap.put(SEND_FAIL_ADD_EMPTY,SEND_FAIL_ADD_EMPTY_MSG);
		return codeMap.get(code);
	}
	
	public static String getContentType(String fileExt){
		Map<String,String> contentTypeMap = new Map<String,String>();
		contentTypeMap.put('pdf',DOC_TYPE_PDF);
		contentTypeMap.put('doc',DOC_TYPE_DOC);
		contentTypeMap.put('rtf',DOC_TYPE_RTF);
		contentTypeMap.put('docx',DOC_TYPE_DOCX);
		contentTypeMap.put('txt',DOC_TYPE_TXT);
		contentTypeMap.put('html',DOC_TYPE_HTML);
		return contentTypeMap.get(fileExt);
	}
	
	public static List<Messaging.Emailfileattachment> makeEmailAttachments(List<DocRowRec> docs){
		if(docs == null || docs.size() == 0){
			return null;
		}
		
		List<Messaging.Emailfileattachment> attachments = new List<Messaging.Emailfileattachment>();
		for(DocRowRec doc : docs){
			Messaging.Emailfileattachment attachment= new Messaging.Emailfileattachment();
			Blob b = S3Documents.getS3Doc(doc.doc.S3_Folder__c, doc.doc.ObjectKey__c);
			if( b == null ){
				System.debug('Failed to get the document of '+doc.doc.ObjectKey__c+' from S3, ignored !');
				continue;
			}
		
			//System.debug(EncodingUtil.base64Encode(b));
			attachment.setFileName(doc.doc.Name);
			//attachment.setInline(false);
			attachment.setBody(b);
			attachments.add(attachment);
			
		}
		
		return attachments;
		
	}
	
	public static Integer sendMailWithTemplate(List<Contact> recipients, List<Messaging.Emailfileattachment> externalDocs, List<Id> sfDocs, Id templateId){
		return sendMailWithTemplate(recipients,externalDocs,sfDocs,templateId,false);
	}
	
	public static Integer sendMailWithTemplate(List<Contact> recipients, List<Messaging.Emailfileattachment> externalDocs, List<Id> sfDocs, Id templateId,Boolean enableActivityTracking){
		if(recipients==null||recipients.size()==0)
			return SEND_SUCC;
		List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
		for(Contact con:recipients){
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			mail.setTemplateId(templateId);
			mail.setTargetObjectId(con.Id);
			mail.setWhatId(con.AccountId);
			mail.setSaveAsActivity(enableActivityTracking);
			if(sfDocs!=null&&sfDocs.size()>0)
				mail.setDocumentAttachments(sfDocs);
			if(externalDocs != null && externalDocs.size()>0)
				mail.setFileAttachments(externalDocs);
			mailList.add(mail);
		}
		//if(Test.isRunningTest())
			//return SEND_SUCC;
		
		Messaging.SendEmailResult[] result=Messaging.sendEmail(mailList,false);
		List<Messaging.SendEmailResult> rl= new List<Messaging.SendEmailResult>(result);
		Integer noOfSuccessEmail = 0;				
		for(Messaging.SendEmailResult r:rl){
			List<Messaging.SendEmailError> a =new List<Messaging.SendEmailError>(r.getErrors());
			if(r.isSuccess())
				noOfSuccessEmail++;
			for(Messaging.SendEmailError e:a){
				system.debug('error : '+e.getMessage());
			}
		}
		if(noOfSuccessEmail==0)
			return SEND_FAIL;
		else if(noOfSuccessEmail>0 && noOfSuccessEmail<rl.size())
			return SEND_SUCC_PARTIAL;
		return SEND_SUCC;
	}
	
	//Author by Jack for the Send Resume function
	public static Integer sendMailWithTemplateFromSR(List<ConRowRec> recipients, Id templateId, boolean bccUser) {
		return sendMailWithTemplateFromSR(recipients, null, templateId, false, bccUser);
	}

	public static Integer sendMailWithTemplateFromSR(List<ConRowRec> recipients , List<Messaging.Emailfileattachment> externalDocs, 
	                                                 Id templateId, Boolean enableActivityTracking, boolean bccUser) {
		if(recipients==null||recipients.size()==0)
			return SEND_SUCC;
		String[] userEmail = null;
		List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
		if(bccuser == true){
			CommonSelector.checkRead(User.SObjectType,'Email');
			User currentUser = [select Email from User where id =: UserInfo.getUserid()];
			userEmail =new String[]{currentUser.Email};
		}
				
		for(ConRowRec con:recipients){
			//Get the contact id and account id
			String newContactAccountId = '';
			String newContactId = '';
			String newAccountId = '';
			String[] toaddress = new String[]{};
			newContactAccountId = con.ContactAccountId;
			newContactId = newContactAccountId.subString(0,newContactAccountId.indexof(':'));
			newAccountId = newContactAccountId.subString(newContactAccountId.indexof(':')+1, newContactAccountId.length());
			//get the address to sent
			String newsetToAddress = '';
			String setToaddress = '';
			newsetToAddress = con.ContactEmail;
			setToaddress = newsetToAddress.subString(newsetToAddress.indexof(':')+1, newsetToAddress.length());
			if(setToaddress != 'NoAdditionalEmail' && setToaddress != null && setToaddress != '' && setToaddress != 'NoEmail'){
				toaddress.add(setToaddress);  //selected email address from User
			}
			else{
				toaddress = null;
			}
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			mail.setBccAddresses(userEmail);
			mail.setToAddresses(toaddress);
			mail.setTemplateId(templateId);
			mail.setTargetObjectId(newContactId);
			mail.setUseSignature(false);
			if(!newAccountId.equalsignorecase('null')) //if the contact does not have related account, ignore the account id
				mail.setWhatId(newAccountId);
			mail.setSaveAsActivity(enableActivityTracking);
			
		    //Attach documents
			if(externalDocs != null && externalDocs.size()>0) 
				mail.setFileAttachments(externalDocs);
			mailList.add(mail);
		}
		system.debug('mailList'+ mailList.size());
		Messaging.SendEmailResult[] result = null;
		if(!Test.isRunningTest()){			
			result=Messaging.sendEmail(mailList,false);
		}		
		List<Messaging.SendEmailResult> rl= new List<Messaging.SendEmailResult>(result);
		Integer noOfSuccessEmail = 0;
		for(Messaging.SendEmailResult r:rl){
			List<Messaging.SendEmailError> a =new List<Messaging.SendEmailError>(r.getErrors());
			if(r.isSuccess()){
				noOfSuccessEmail++;
				//con.sendMailCode = SendMailService.SEND_SUCC;
				//update con;
			}
			for(Messaging.SendEmailError e:a){
				system.debug('error : '+e.getMessage());
			}
		}
		if(noOfSuccessEmail==0){
			return SEND_FAIL;
		}
		else if(noOfSuccessEmail>0 && noOfSuccessEmail<rl.size()){
			//String ErrorMessage = noOfSuccessEmail + SendMailService.SEND_SUCC_PARTIAL_MSG;
			//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,ErrorMessage));
			return SEND_SUCC_PARTIAL;
		}
		return SEND_SUCC;
	}
	
	public static Integer sendPreviewMailToUserWithTemplateFromSR( List<Messaging.Emailfileattachment> externalDocs, Id templateId) {
		
	
		//get current user's email address
        CommonSelector.checkRead(User.SObjectType,'Email');
	    User previewUser = [select Email from User where id =: UserInfo.getUserid()];
		String toEmail = previewUser.Email;
		if( toEmail  == null || toEmail == '' ){
		    return SEND_FAIL;
		}
		
		System.debug('Begin to send preview mail to user : '+ toEmail);
		
		String[] userEmail =new String[]{toEmail};
		
		List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
	
		
		
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		
		mail.setToAddresses(userEmail);
		mail.setTemplateId(templateId);
		mail.setTargetObjectId(UserInfo.getUserid());
		mail.setUseSignature(false);
		
		mail.setSaveAsActivity(false);
		
	    //Attach documents
		if(externalDocs != null && externalDocs.size()>0) 
			mail.setFileAttachments(externalDocs);
		mailList.add(mail);
		
		system.debug('mailList'+ mailList.size());
		Messaging.SendEmailResult[] result = null;
		if(!Test.isRunningTest()){			
			result=Messaging.sendEmail(mailList,false);
		}		
		List<Messaging.SendEmailResult> rl= new List<Messaging.SendEmailResult>(result);
		Integer noOfSuccessEmail = 0;
		for(Messaging.SendEmailResult r:rl){
			List<Messaging.SendEmailError> a =new List<Messaging.SendEmailError>(r.getErrors());
			if(r.isSuccess()){
				noOfSuccessEmail++;
				//con.sendMailCode = SendMailService.SEND_SUCC;
				//update con;
			}
			for(Messaging.SendEmailError e:a){
				system.debug('error : '+e.getMessage());
			}
		}
		if(noOfSuccessEmail==0){
			return SEND_FAIL;
		}
		else if(noOfSuccessEmail>0 && noOfSuccessEmail<rl.size()){
			//String ErrorMessage = noOfSuccessEmail + SendMailService.SEND_SUCC_PARTIAL_MSG;
			//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,ErrorMessage));
			return SEND_SUCC_PARTIAL;
		}
		return SEND_SUCC;
	}
	
	//Author by Jack for MassUpdate send Email function
	public static Integer sendMailWithTemplateFromMUD(List<Placement_Candidate__c> recipients, List<Messaging.Emailfileattachment> externalDocs,String cansWithEmail,  String replyto, String displayname,boolean use_sign, Id templateId){
		return sendMailWithTemplateFromMUD(recipients,externalDocs, cansWithEmail,replyto,displayname,use_sign,templateId,true);
	}
	
	public static Integer sendMailWithTemplateFromMUD(List<Placement_Candidate__c> recipients, List<Messaging.Emailfileattachment> externalDocs, String cansWithEmail,  String replyto, String displayname,boolean use_sign, Id templateId,Boolean enableActivityTracking){
		List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
		for(Placement_Candidate__c pc : recipients){
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(pc.Candidate__c);
            mail.setWhatId(pc.Id);
            mail.setTemplateID(templateId);
            mail.setSaveAsActivity(enableActivityTracking);	
            mail.setOrgWideEmailAddressId(cansWithEmail);
            mail.setReplyTo(replyto);
            mail.setSenderDisplayName(displayname);	
            mail.setUseSignature(use_sign);	
            if(externalDocs != null && externalDocs.size()>0) //Attached documents
				mail.setFileAttachments(externalDocs);
            mailList.add(mail);
		}
		Messaging.SendEmailResult[] result = null;
		//if(!Test.isRunningTest()){				
			result=Messaging.sendEmail(mailList,false);
		//}		
		List<Messaging.SendEmailResult> rl= new List<Messaging.SendEmailResult>(result);
		Integer noOfSuccessEmail = 0;
		for(Messaging.SendEmailResult r:rl){
			List<Messaging.SendEmailError> a =new List<Messaging.SendEmailError>(r.getErrors());
			if(r.isSuccess()){
				noOfSuccessEmail++;
				//con.sendMailCode = SendMailService.SEND_SUCC;
				//update con;
			}
			for(Messaging.SendEmailError e:a){
				system.debug('error : '+e.getMessage());
			}
		}
		if(noOfSuccessEmail==0){
			return SEND_FAIL;
		}
		else if(noOfSuccessEmail>0 && noOfSuccessEmail<rl.size()){
			//String ErrorMessage = noOfSuccessEmail + SendMailService.SEND_SUCC_PARTIAL_MSG;
			//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,ErrorMessage));
		return SEND_SUCC_PARTIAL;
		}
		return SEND_SUCC;
	}
}