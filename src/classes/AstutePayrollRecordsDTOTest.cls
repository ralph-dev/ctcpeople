/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AstutePayrollRecordsDTOTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		map<String,Object> biller=new map<String,Object>();
		   map<String,Object> billingContact=new map<String,Object>();
		   map<String,Object> firstApprover=new map<String,Object>();
		   map<String,Object> secondApprover=new map<String,Object>();
		   map<String,Object> employee=new map<String,Object>();
		   map<String,Object> workplace=new map<String,Object>();
    	   AstutePayrollRecordsDTO apRTDTO=new AstutePayrollRecordsDTO();
    	   biller.put('test1','tet1');
    	   billingContact.put('test1','tet1');
    	   firstApprover.put('test1','tet1');
    	   secondApprover.put('test1','tet1');
    	   workplace.put('test1','tet1');
    	   employee.put('test1','tet1');
    	   apRTDTO.setBiller(biller);
    	   apRTDTO.setBillingContact(billingContact);
    	   apRTDTO.setSecondApprover(secondApprover);
    	   apRTDTO.setFirstApprover(firstApprover);
    	   apRTDTO.setWorkplace(workplace);
    	  	System.assert(apRTDTO.getRequestMap().keyset().size()==0);
    	}
    	   
    }
}