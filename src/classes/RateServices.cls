public class RateServices {
    /***
		Convert rates Info to rate change.
					---Alvin
	
	***/
	public list<map<String,Object>> convertRatesToRateChange(list<Rate__c> rates){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> rateChangeMap=ctcPeopleHelperService.getRateChangeMap();
		list<map<String,Object>> rateChangeInfos=new list<map<String,Object>>();	
		for(Rate__c record : rates){
			rateChangeInfos.add(ctcPeopleHelperService.convertToAPFormat(record, rateChangeMap)); 
		}
		return 	rateChangeInfos;				
	}

}