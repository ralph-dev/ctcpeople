/**
 * This controller is used to update SQS Credential settings for SQSCredentialAdmin page input 
 * Created by: Lina
 * Created date: 13/12/16
 **/ 

public with sharing class SQSCredentialAdminController {
    private transient String url;
    private transient String accessKey;
    private transient String secretKey;
    public String name { get; set; }

    public String geturl(){
        return null;
    }

    public void seturl(String url){
        this.url = url;
    }

    public String getaccessKey(){
        return null;
    }
    
    public void setaccessKey(String accessKey){
        this.accessKey = accessKey;
    }

    public String getsecretKey(){
        return null;
    }

    public void setsecretKey(String secretKey){
        this.secretKey = secretKey;
    }
    
    
    public void saveCredential() {
        SQSCredentialSelector selector = new SQSCredentialSelector();
        if (isValidateInput()) {
            SQSCredential__c sqs = selector.getSQSCredential(name);
            if (sqs != null) {
                sqs.URL__c = url;
                sqs.Access_Key__c = accessKey;
                sqs.Secret_Key__c = secretKey;
                update sqs;
            } else {
                SQSCredential__c sqsNew = new SQSCredential__c();
                sqsNew.URL__c = url;
                sqsNew.Access_Key__c = accessKey;
                sqsNew.Secret_Key__c = secretKey;
                sqsNew.Name = name;
                insert sqsNew;
            }
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'SQS Credential Settings have been saved successfully!\n')); 
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter values for ALL FIELDS!')); 
        }
    }
    
    public boolean isValidateInput() {
        return (String.isNotBlank(url) && String.isNotBlank(accessKey) && String.isNotBlank(secretKey));
    }
    
    public static List<SelectOption> getListOfSQS() {
        List<SelectOption> sqsNames = new List<SelectOption>();
        sqsNames.add(new SelectOption('Daxtra Feed SQS', 'Daxtra Feed SQS'));
        return sqsNames;
    }


}