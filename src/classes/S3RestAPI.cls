public without sharing class S3RestAPI{
    private S3Credential credential;
    public static final String S3_DOMAIN = 's3.amazonaws.com';
    // Seconds that request signature will expire
    public static final Integer EXPIRY_SEC = 200;
    public static final String PROTOCOL = 'https';
    
    public S3RestAPI(S3Credential credential){
        this.credential = credential;
    }
    
    /*
        This method can only be used to generate string to sign without amz headers, as function of constructing 
        CanonicalizedAmzHeaders from given amz header list is not completed yet.
    */
    private String constructStrToSign(String method,String contentMD5,String contentType, DateTime requestTime,List<String> amzHeaders, String bucket, String objectKey){
        String stringToSign = method+'\n'
                + contentMD5+'\n' // Here we should put Content-MD5, but content for this request is empty
                + contentType+'\n' // Here we should put Content-Type, but content for this request is empty
                + requestTime.format('EEE, d MMM yyyy HH:mm:ss Z')+'\n' // Here we should put the request time
                + '/'+bucket+'/'+objectKey;
        return stringToSign;
    }
    
    /*
        TODO: this method is not completed. It can not be used
    */
    // private String constructStrToSign4QueryStr(String method,String contentMD5,String contentType, DateTime expiryTime,List<String> amzHeaders, String bucket, String objectKey){
    //     String stringToSign =method+'\n'
    //             + '\n' // Here we should put Content-MD5, but content for this request is empty
    //             + '\n' // Here we should put Content-Type, but content for this request is empty
    //             + expiryTime.getTime()+'\n' // Here we should put the request expiry time
    //             + '/'+bucket+'/'+objectKey;
    //     return stringToSign;
    // }
    
    /*
        Generate signature for a given string.
    */    
    private String generateSignature(String stringToSign){
        //system.debug('String To Sign:'+stringToSign);
        Blob encryptedStrToSign = Crypto.generateMac('hmacSHA1',Blob.valueOf(stringToSign),Blob.valueOf(credential.secretKey));
        String signature = EncodingUtil.base64Encode(encryptedStrToSign);
        //system.debug('Signature:'+signature);
        return signature;
    }
    
    /*
        TODO: this method is not completed. It can not be used
    */
    // private String generateSignature4QueryStr(String stringToSign){
    //     system.debug('String To Sign:'+stringToSign);
    //     Blob encryptedStrToSign = Crypto.generateMac('hmacSHA1',Blob.valueOf(stringToSign),Blob.valueOf(credential.secretKey));
    //     String signature = EncodingUtil.urlEncode(EncodingUtil.base64Encode(encryptedStrToSign),'UTF-8');
    //     system.debug('Signature:'+signature);
    //     return signature;    
    // }
    
    /*
        Retrieve the size of a given file which is stored in the given bucket.
        The return file size is calculated in bytes.
    */
    public Integer getFileSize(String bucket, String objectKey){
    
        // Check if crendential is empty or null
         if(credential.accessKey==null || credential.secretKey==null || credential.accessKey.length()<3 || credential.secretKey.length()<3)
            throw new S3RestAPIException(S3RestAPIException.S3RestAPIExceptionCode.NULL_CREDENTIAL,'');
            
        String urlEncodedObjKey =EncodingUtil.urlEncode(objectKey,'UTF-8');        
        //system.debug('URL encoded object key:'+urlEncodedObjKey);
        // Calculate request signature expiry time
        DateTime requestTime = DateTime.now();        
        String stringToSign = constructStrToSign('HEAD','','',requestTime,null,bucket,urlEncodedObjKey);
        String signature = generateSignature(stringToSign);
       
        // Prepare request
        String reqEndpoint= PROTOCOL+'://'+S3_DOMAIN+'/'+bucket+'/'+urlEncodedObjKey;
        HttpRequest getFileSizeReq = new HttpRequest();         
        getFileSizeReq.setMethod('HEAD');
        getFileSizeReq.setEndpoint(reqEndpoint);
        getFileSizeReq.setHeader('Date',requestTime.format('EEE, d MMM yyyy HH:mm:ss Z'));
        getFileSizeReq.setHeader('Authorization','AWS '+credential.accessKey+':'+signature);
        
        Http httpClient = new Http();
        Integer fileSize=-1;
        try{
            HttpResponse resp = httpClient.send(getFileSizeReq);
            fileSize = Integer.valueOf(resp.getHeader('Content-Length'));
            system.debug('size:'+resp.getHeader('Content-Length'));

        }catch(CalloutException e){
            if(e.getMessage().contains('403 for URL')){
                throw new S3RestAPIException(S3RestAPIException.S3RestAPIExceptionCode.AUTH_FAIL,e);
            }else{
                throw new S3RestAPIException(S3RestAPIException.S3RestAPIExceptionCode.UNKNOWN_CALLOUT_FAILURE,e);
            }          
        }catch(Exception e){
            throw new S3RestAPIException(S3RestAPIException.S3RestAPIExceptionCode.UNKNOWN,e);
        }               
        return fileSize;
    }    
}