/**
    * This is the advertisement selector
    *
    * @author: Lorena
    * @createdDate: 8/10/2016
    * @lastModify:
    */
public with sharing class AdvertisementSelector extends CommonSelector{

	public AdvertisementSelector() {
		super(Advertisement__c.SObjectType);
	}

	

	public override LIST<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField>{
				Advertisement__c.AdvertiserJobTemplateLogoCode__c,
				Advertisement__c.Advertisement_Account__c,
				Advertisement__c.Ad_Posting_Status_Description__c,
				Advertisement__c.AreaCode__c,
				Advertisement__c.Bullet_1__c,
				Advertisement__c.Bullet_2__c,
				Advertisement__c.Bullet_3__c,
				Advertisement__c.Classification2Code__c,
				Advertisement__c.ClassificationCode__c,
				Advertisement__c.Company_Name__c,
				Advertisement__c.CountryCode__c,
				Advertisement__c.CreatedById,
				Advertisement__c.Has_Referral_Fee__c,
				Advertisement__c.Id,
				Advertisement__c.IsQualificationsRecognised__c,
				Advertisement__c.Job_Contact_Name__c,
				Advertisement__c.Job_Contact_Phone__c,
				Advertisement__c.Job_Contact_Email__c,
				Advertisement__c.Job_Content__c,
				Advertisement__c.Job_Posting_Status__c,
				Advertisement__c.Job_Title__c,
				Advertisement__c.Job_Type__c,
				Advertisement__c.JobXML__c,
				Advertisement__c.JXT_Short_Description__c,
				Advertisement__c.JXT_Auto_Archive__c,
				Advertisement__c.Location_Hide__c,
				Advertisement__c.LocationCode__c,
				Advertisement__c.Name,
				Advertisement__c.Nearest_Transport__c,
				Advertisement__c.No_salary_information__c,
				Advertisement__c.Online_Ad__c,
				Advertisement__c.OwnerId,
				Advertisement__c.Placement_Date__c,
				Advertisement__c.Prefer_Email_Address__c,
				Advertisement__c.RecordTypeId,
				Advertisement__c.Referral_Fee__c,
				Advertisement__c.Residency_Required__c,
				Advertisement__c.Salary_description__c,
				Advertisement__c.Salary_Type__c,
				Advertisement__c.Search_Tags__c,
				Advertisement__c.SectorCode__c,
				Advertisement__c.SeekSalaryMax__c,
				Advertisement__c.SeekSalaryMin__c,
				Advertisement__c.Skill_Group_Criteria__c,
				Advertisement__c.Status__c,
				Advertisement__c.Street_Adress__c,
				Advertisement__c.SubClassification2Code__c,
				Advertisement__c.SubClassificationCode__c,
				Advertisement__c.TemplateCode__c,
				Advertisement__c.Terms_And_Conditions_Url__c,
				Advertisement__c.Vacancy__c,
				Advertisement__c.Website__c,
				Advertisement__c.WorkType__c,
				Advertisement__c.WorkTypeCode__c
		};
	}

	public List<String> getSObjectFieldListForSeekClone(){
		List<String> result = new List<String>();
		Set<String> excluedFields = new Set<String>{'CreatedById', 'CreatedDate', 'OwnerId', 'Placement_Date__c'};
		Map<String, SObjectField> allFieldsMap = GetfieldsMap.getFieldMap(PeopleCloudHelper.getPackageNamespace()+'Advertisement__c');
		if(allFieldsMap!=null && allFieldsMap.size()>0){
			for(Schema.SObjectField field: allFieldsMap.values()){
				Schema.DescribeFieldResult fd = field.getDescribe();
				if(!excluedFields.contains(fd.getName())){
					result.add(fd.getName());
				}
			}
		}
		return result;
	}

	//select advertisement and vacancy owner by ad Id, order by lastModifiedDate
	public Advertisement__c getVacIdByAdId(String adId) {
		SimpleQueryFactory qf = simpleQuery();
		String condition = '' ;
		qf.selectFields(getSObjectFieldList());
		qf.selectFields(new List<String>{'Vacancy__r.ownerId','CreatedById','OwnerId'});
		if(String.isNotBlank(adId)) {
			condition += 'Id =: adId';
		}
		qf.setCondition(condition);
		qf.getOrderings().clear();
		qf.addOrdering('LastModifiedDate', fflib_QueryFactory.SortOrder.DESCENDING,true);
		String soqlStr = qf.toSOQL();

		List<Advertisement__c> ads = Database.query(soqlStr);
		if(ads!=null && ads.size()>0) {
			return ads.get(0);
		} else {
			return null;
		}

	}

	public Advertisement__c getAdForSeekClone(String adId){
		SimpleQueryFactory qf = simpleQuery();
		String condition = '';
		qf.selectFields(getSObjectFieldList());
		qf.selectFields(getSObjectFieldListForSeekClone());
		condition += 'Id =: adId';
		qf.setCondition(condition);
		qf.clearOrderings();
		String soqlStr = qf.toSOQL();
		Advertisement__c result = Database.query(soqlStr);
		return result;
	}

	/**
	 * Select Advertisement by Id
	 * @param adId     advertisement Id
	 * @return Advertisement__c
	 */
	public Advertisement__c getAdvertisementById(String adId) {
		SimpleQueryFactory qf = simpleQuery()
									.selectFields(getSObjectFieldList())
									.setCondition('Id =: adId');
		try {
			Advertisement__c ad = Database.query(qf.toSOQL());
			return ad;
		} catch (Exception e) {
			System.debug('AdvertisementSelector_getAdvertisementById: ' + e);
			return null;
		}
	}

}