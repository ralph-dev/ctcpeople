/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

/**
 * This controller is used to update Daxtra Credential settings for DaxtraCredentialAdmin page input 
 * Created by: Lina
 * Created date: 19/05/16
 **/ 

public with sharing class DaxtraCredentialAdminController {
    //public String serviceEndpoint { get; set; }
    //public String username { get; set; }
    //public String password { get; set; }
    //public String databaseName { get; set; }
    
    //public void saveCredential() {
    //    if (isValidateInput()) {
    //        DaxtraCredential__c dc = DaxtraCredentialSelector.getDaxtraCredential();
    //        if (dc != null) {
    //            dc.Service_Endpoint__c = serviceEndpoint;
    //            dc.Username__c = username;
    //            dc.Password__c = password;
    //            dc.Database__c = databaseName;
    //            checkFLS();
    //            update dc;
    //        } else {
    //            DaxtraCredential__c dcNew = new DaxtraCredential__c();
    //            dcNew.Service_Endpoint__c = serviceEndpoint;
    //            dcNew.Username__c = username;
    //            dcNew.Password__c = password;
    //            dcNew.Database__c = databaseName;
    //            dcNew.Name = 'Daxtra Credential';
    //            checkFLS();
    //            insert dcNew;
    //        }
    //        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Daxtra Credential Settings have been saved successfully!\n')); 
    //    } else {
    //        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter values for ALL FIELD!')); 
    //    }
    //}
    
    //public boolean isValidateInput() {
    //    return (String.isNotBlank(serviceEndpoint) && String.isNotBlank(username) && String.isNotBlank(password) && String.isNotBlank(databaseName));
    //}

    //private void checkFLS(){
    //    List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
    //        DaxtraCredential__c.Service_Endpoint__c,
    //        DaxtraCredential__c.Username__c,
    //        DaxtraCredential__c.Password__c,
    //        DaxtraCredential__c.Database__c,
    //        DaxtraCredential__c.Name
    //    };
    //    fflib_SecurityUtils.checkInsert(DaxtraCredential__c.SObjectType, fieldList);
    //    fflib_SecurityUtils.checkUpdate(DaxtraCredential__c.SObjectType, fieldList);
    //}

}