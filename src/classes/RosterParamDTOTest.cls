@isTest
private class RosterParamDTOTest {
	final static String ROSTER_STATUS_PENDING = 'Pending'; 
	final static String ROSTER_TYPE_CLIENT = 'Client';
    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // TO DO: implement unit test
        Account acc = TestDataFactoryRoster.createAccount();
        RosterParamDTO rParamDTO = 
        	TestDataFactoryRoster.createRosterParamDTO(acc,ROSTER_STATUS_PENDING,ROSTER_TYPE_CLIENT);
        
        System.assertNotEquals(rParamDTO.getId(),'');
        System.assertNotEquals(rParamDTO.getStatus(),'');
        System.assertNotEquals(rParamDTO.getRosterType(),'');
    	}
    }
}