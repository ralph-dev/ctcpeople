/**
TriggerHelper is a helper class used to stop
firing chained triggers which will surpass SF
SOQL or other function limit.

Some Triggers will check this class's member before
they really work. 

**/

global with sharing class TriggerHelper {

// This member is global due to the fact that one of "Job Rocket(customer)" triggers require
// to access it 
global static boolean SyncTimeSheetApproverEnable=true;

public static boolean UpdateReminingEnable=true;
public static boolean CandidatPlaced = true;
public static boolean Deduplication = true;
public static boolean PlacementCandidateEventOnInterviewTrigger = true;
public static boolean AutoCreateUnavailableDays = true;
public static boolean UpdateAccountField = true;
public static boolean DisableTriggerOnWebDocument = false;

public static boolean DisableTriggerOnTask;
public static Boolean DisableTriggerOnContact;
public static Boolean DisableTriggerOnPlacementCandidate;
public static Boolean DisableTriggeronPlacement;
public static Boolean DisableTriggerOnAdvertisement;
public static Boolean DisableAstutePayrollImplementation;



static {
	enableAllTrigger();
}

public static void enableAllTrigger() {
	DisableTriggerOnContact = false;
	DisableTriggerOnPlacementCandidate = false;
	DisableTriggeronPlacement = false;
	DisableAstutePayrollImplementation = false;
	DisableTriggerOnAdvertisement = false;
	DisableTriggerOnTask = false;
}

public static void disableAllTrigger() {
    UpdateReminingEnable = false;
    CandidatPlaced = false;
    Deduplication = false;
    PlacementCandidateEventOnInterviewTrigger = false;
    AutoCreateUnavailableDays = false;
    UpdateAccountField = false;
    DisableTriggerOnWebDocument = true;    
    DisableTriggerOnContact = true;
	DisableTriggerOnPlacementCandidate = true;
	DisableTriggeronPlacement = true;
	DisableTriggerOnAdvertisement = true;
	DisableAstutePayrollImplementation = true;
	DisableTriggerOnTask = true;
}

public static Map<String,Map<String,Boolean>> getPlacedStatusConfig(){
	Map<String,Map<String,Boolean>> placedStatusConfig = new Map<String,Map<String,Boolean>>();
	PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
	if(cs.Placed_Candidate_Condition__c!=null){
		String[] placedStatusEntries = cs.Placed_Candidate_Condition__c.split(';');
		for(String e:placedStatusEntries){
			String[] statusProgressPair = e.split('\\|',-1);
			if(statusProgressPair.size()!=2)
				continue;
			Map<String,Boolean> candProgress = placedStatusConfig.get(statusProgressPair[0]);
			if(candProgress==null){
				candProgress = new Map<String,Boolean>();
				placedStatusConfig.put(statusProgressPair[0],candProgress);
			}
			candProgress.put(statusProgressPair[1],true);
		}
	}	
	return placedStatusConfig;
}

public static Boolean isPlacedStatus(String status,String candProgress, Map<String,Map<String,Boolean>> placedStatusConfig){
	if(placedStatusConfig==null){
		placedStatusConfig=getPlacedStatusConfig();
	}
	if(placedStatusConfig==null || placedStatusConfig.get(status)==null || placedStatusConfig.get(status).get(candProgress)==null)
		return false;
	return true;		
}

}