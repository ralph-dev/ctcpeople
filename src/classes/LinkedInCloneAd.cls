public with sharing class LinkedInCloneAd {
	private ApexPages.StandardController stdCon;
	public String adtemplateid; //Tempory ad id
	//Init Error Message for customer
    public PeopleCloudErrorInfo errormesg; 
    public Boolean msgSignalhasmessage{get ;set;} //show error message or not
    
	public LinkedInCloneAd(ApexPages.StandardController stdController){
		stdCon = stdController;
		Advertisement__c adTemplate = (Advertisement__c) stdController.getRecord();
		adtemplateid = adTemplate.id;
	}
	
	//Clone ad
	public PageReference cloneAd(){ 
		String whereClause = 'id =\''+adtemplateid +'\'';
        String ExcludeFieldsString = 'CreatedDate, CreatedById, OwnerId,'+ PeopleCloudHelper.getPackageNamespace()+'Application_URL__c'+','+PeopleCloudHelper.getPackageNamespace()+'Placement_Date__c';//Not Colon Field list string
        Set<String> ExcludeFieldsSet = new Set<String>();
        ExcludeFieldsSet = GetfieldsMap.getExcludeFieldsForJobPosting(ExcludeFieldsString);//fetch the Exclude Field set
        //system.debug('ExcludeFieldsSet ='+ ExcludeFieldsSet);
        
        /**
		* already did checkread in GetfieldsMap.getCreatableFieldsSOQL method
		*/
        String queryString = GetfieldsMap.getCreatableFieldsSOQL(PeopleCloudHelper.getPackageNamespace()+'Advertisement__c', ExcludeFieldsSet, whereClause);
        try{
        	Advertisement__c ad = database.query(queryString);
        	//system.debug('ad =' + ad);
	        Advertisement__c newAd = ad.clone(false, true);
	        newAd.Status__c = 'Active';
	        newAd.Job_Posting_Status__c = 'Clone to Post';
	        //checkFLS
	        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
	        	Advertisement__c.Status__c,
	        	Advertisement__c.Job_Posting_Status__c
	        };
	        fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList); 
	        insert newAd;
	        PageReference editPage = Page.LinkedInPost;
	        editPage.getParameters().put('id', newAd.id);
	        editpage.getParameters().put('action','clone');//The action is about clone ad not update
	        //system.debug('editpage = '+ editpage.getUrl());
	        return editPage;
        }catch(Exception e){
        	customerErrorMessage(PeopleCloudErrorInfo.CLONE_AD_ERROR); 
            return null;
        }
	}
	
	//Cancel clone ad
	public PageReference cancelClone(){
    	return stdCon.view();
    }
    
    public void customerErrorMessage(Integer msgcode){
   		errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        	msgSignalhasmessage = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
            	ApexPages.addMessage(newMessage);
            }               
    }
}