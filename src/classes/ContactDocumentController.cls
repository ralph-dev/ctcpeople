public with sharing class ContactDocumentController {
	public void createContactDocumentController(List<Contact> triggerContactList) {
		//system.debug('Start to create Contact Document ='+ triggerContactList);
		List<ActivityDoc> selectedActivityDocs = new List<ActivityDoc>();
        
        //get existing contact resume map.
        Map<String, Web_Document__c> ContactExistingDocumentMapping = new Map<String, Web_Document__c>();
        ContactExistingDocumentMapping = ContactDocumentSelector.getContactExistingDocument(triggerContactList);
        
        //generate ActivityDoc List
        ContactDocumentOperator cdo = new ContactDocumentOperator(); 
        selectedActivityDocs = cdo.getActivityDocList(triggerContactList, ContactExistingDocumentMapping);
        
        //system.debug('selectedActivityDocs ='+ selectedActivityDocs);
        //Post to ec2 Server
        if(selectedActivityDocs!=null && selectedActivityDocs.size()>0) {
            String jsonString = JSON.serialize(selectedActivityDocs);
            //String sessionId = EncodingUtil.urlEncode(UserInfo.getSessionId(), 'UTF-8');
            //String surl = EncodingUtil.urlEncode(URL.getSalesforceBaseUrl().toExternalForm() +'/services/Soap/u/25.0/'+UserInfo.getOrganizationId(),'UTF-8');
            ContactDocumentServices.UploaddocumentsByEc2(jsonString);
        }
        
	}
}