/** 
 * A selector class to return Daxtra Credential
 * Created by: Lina
 * Created: 19/05/2016
 */

public with sharing class DaxtraCredentialSelector {

    public static DaxtraCredential__c getDaxtraCredential() {
        List<DaxtraCredential__c> daxtraCreds = [SELECT Service_Endpoint__c, Username__c, Password__c, Database__c From DaxtraCredential__c LIMIT 1];
        if (daxtraCreds != null) {
            for (DaxtraCredential__c dc : daxtraCreds) {
                return dc;
            }
        } 
        return null;
    }
}