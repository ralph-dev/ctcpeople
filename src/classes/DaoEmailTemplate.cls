public with sharing class DaoEmailTemplate extends CommonSelector{
	
	
	public DaoEmailTemplate(){
		super(EmailTemplate.sObjectType);
	}
	
	private static DaoEmailTemplate dao = new DaoEmailTemplate();
	
	private static String defaultFields = 'TimesUsed, TemplateType, TemplateStyle, '
	            +'SystemModstamp, Subject, OwnerId, NamespacePrefix,  '
	            +'Name, Markup, LastUsedDate, LastModifiedDate,  '
	            +'LastModifiedById, IsActive, Id, HtmlValue,  '
	            +'FolderId, Encoding, DeveloperName, Description,  '
	            +'CreatedDate, CreatedById, BrandTemplateId, Body,  '
	            +'ApiVersion';


static public List<EmailTemplate> getAllSupportedTemplates(){
    List<EmailTemplate> result;
    try{ 
    	dao.checkRead(defaultFields);
        result =  [Select TimesUsed, TemplateType, TemplateStyle, 
            SystemModstamp, Subject, OwnerId, NamespacePrefix, 
            Name, Markup, LastUsedDate, LastModifiedDate, 
            LastModifiedById, IsActive, Id, HtmlValue, 
            FolderId, Encoding, DeveloperName, Description, 
            CreatedDate, CreatedById, BrandTemplateId, Body, 
            ApiVersion From EmailTemplate where ((TemplateStyle='none' and TemplateType='text')
            or( TemplateStyle='freeForm' and TemplateType='html')) and isactive=true order by Name];
    }catch(Exception e){
        result = new List<EmailTemplate>();
    }
    return result;
} 

static public EmailTemplate getTemplateById(String id){
    EmailTemplate result = null;
    try{ 
    	dao.checkRead(defaultFields);
        result = [Select TimesUsed, TemplateType, TemplateStyle, 
            SystemModstamp, Subject, OwnerId, NamespacePrefix, 
            Name, Markup, LastUsedDate, LastModifiedDate, 
            LastModifiedById, IsActive, Id, HtmlValue, 
            FolderId, Encoding, DeveloperName, Description, 
            CreatedDate, CreatedById, BrandTemplateId, Body, 
            ApiVersion From EmailTemplate where Id=:id];
    }catch(Exception e){
        
    }
    return result;
}


static public List<EmailTemplate> getTemplateLikeDevName(String devName){
    List<EmailTemplate> result = new List<EmailTemplate>();
    devName = '%'+devName+'%';
    try{ 
    	dao.checkRead(defaultFields);
        result = [Select TimesUsed, TemplateType, TemplateStyle, 
            SystemModstamp, Subject, OwnerId, NamespacePrefix, 
            Name, Markup, LastUsedDate, LastModifiedDate, 
            LastModifiedById, IsActive, Id, HtmlValue, 
            FolderId, Encoding, DeveloperName, Description, 
            CreatedDate, CreatedById, BrandTemplateId, Body, 
            ApiVersion From EmailTemplate where DeveloperName like:devName];
    }catch(Exception e){
        result = new List<EmailTemplate>();
    }
    return result;
}


//Add by Jack
static public List<EmailTemplate> getAllActiveTemplatesById(Id id){
    List<EmailTemplate> result; 
    String devName = 'QSRTEMP%';
    String bulk_email_dev_name = 'BE_CL_%';
    try{ 
    	dao.checkRead(defaultFields);
        result =  [Select TimesUsed, TemplateType, TemplateStyle, 
            SystemModstamp, Subject, OwnerId, NamespacePrefix, 
            Name, Markup, LastUsedDate, LastModifiedDate, 
            LastModifiedById, IsActive, Id, HtmlValue, 
            FolderId, Encoding, DeveloperName, Description, 
            CreatedDate, CreatedById, BrandTemplateId,// Body, 
            ApiVersion From EmailTemplate where ((TemplateStyle='none' and TemplateType='text')
            or( TemplateStyle='freeForm' and TemplateType='html')) and isactive=true and folderid=:id and (not DeveloperName like:devName) and (not DeveloperName like:bulk_email_dev_name) order by Name];
    }catch(Exception e){
        result = new List<EmailTemplate>();
    }
    return result;
}

//Add by Kevin
//Due to view state limit, this method aims to get email templates with fewer fields
static public List<EmailTemplate> getAllActiveTemplatesWithLimitedFieldsById(Id id){
    List<EmailTemplate> result; 
    String devName = 'QSRTEMP%';
    String bulk_email_dev_name = 'BE_CL_%';
    try{ 
    	dao.checkRead('Name,TemplateStyle, TemplateType,  IsActive, Id,FolderId, DeveloperName');
        result =  [Select Name,TemplateStyle, TemplateType, 
            IsActive, Id,FolderId, DeveloperName
            From EmailTemplate 
            Where ((TemplateStyle='none' and TemplateType='text')
            or( TemplateStyle='freeForm' and TemplateType='html') or TemplateType='custom') and isactive=true and folderid=:id and (not DeveloperName like:devName) and (not DeveloperName like:bulk_email_dev_name) order by Name];
    }catch(Exception e){
        result = new List<EmailTemplate>();
    }
    return result;
}


static public EmailTemplate getInitTemplatesByName(String tempName){
    EmailTemplate result;
    try{ 
    	dao.checkRead(defaultFields);
        result =  [Select TimesUsed, TemplateType, TemplateStyle, 
            SystemModstamp, Subject, OwnerId, NamespacePrefix, 
            Name, Markup, LastUsedDate, LastModifiedDate, 
            LastModifiedById, IsActive, Id, HtmlValue, 
            FolderId, Encoding, DeveloperName, Description, 
            CreatedDate, CreatedById, BrandTemplateId, Body, 
            ApiVersion From EmailTemplate where ((TemplateStyle='none' and TemplateType='text')
            or( TemplateStyle='freeForm' and TemplateType='html') or TemplateType='custom') and isactive=true and DeveloperName=: tempName order by Name];
    }catch(Exception e){
        result = new EmailTemplate();
    }
    return result;
}

static public List<EmailTemplate> getOldTemplateLikeDevName(String devName){
    List<EmailTemplate> result = new List<EmailTemplate>();
    devName = '%'+devName+'%';
    try{ 
    	dao.checkRead(defaultFields);
        result = [Select TimesUsed, TemplateType, TemplateStyle, 
            SystemModstamp, Subject, OwnerId, NamespacePrefix, 
            Name, Markup, LastUsedDate, LastModifiedDate, 
            LastModifiedById, IsActive, Id, HtmlValue, 
            FolderId, Encoding, DeveloperName, Description, 
            CreatedDate, CreatedById, BrandTemplateId, //Body, 
            ApiVersion From EmailTemplate where DeveloperName like:devName and CreatedDate < today];
    }catch(Exception e){
        result = new List<EmailTemplate>();
    }
    return result;
}

}