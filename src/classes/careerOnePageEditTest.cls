@isTest
private class careerOnePageEditTest {

	private static testMethod void test() {
	    TriggerHelper.DisableAstutePayrollImplementation = true;
	    
	    DummyRecordCreator.DefaultDummyRecordSet dms = DummyRecordCreator.createDefaultDummyRecordSet();
		
		System.runAs(DummyRecordCreator.platformUser) {
			
			AdvertisementDummyRecordCreator adrc = new AdvertisementDummyRecordCreator();
			Advertisement__c careerOneAd = adrc.generateCareerOneAdDummyRecord(dms);
			
			ApexPages.StandardController stdController = new ApexPages.StandardController(careerOneAd);
	        careerOnePageEdit c = new careerOnePageEdit(stdController);
	        
	        system.assertNotEquals(null, c.hasErrMsgs);
	        system.assertNotEquals(null, c.hasMsgs);
	        system.assertNotEquals(null, c.getSelectSkillGroupList());
	        system.assert(c.has_online_id);
	        system.assert(c.haserror);
	        
	        careerOneAd.Status__c = 'Active';
	        update careerOneAd;
	        ApexPages.StandardController stdControllerUpdated = new ApexPages.StandardController(careerOneAd);
	        careerOnePageEdit updateC = new careerOnePageEdit(stdControllerUpdated);
	        updateC.init();
	        updateC.deleteJob();
	        updateC.post();
		}
        
	}

}