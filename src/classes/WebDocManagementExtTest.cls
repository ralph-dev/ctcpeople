@isTest
private class WebDocManagementExtTest
{
	@isTest
	static void webDocManagementExtTest(){
		Contact thiscontact = new Contact(FirstName ='test', LastName ='yu');
	    insert thiscontact;
	    ApexPages.currentPage().getParameters().put('id', thiscontact.Id);
	    ApexPages.StandardController thestandardcontroller = new ApexPages.StandardController(thiscontact);

	    PCAppSwitch__c pcSwitch = PCAppSwitch__c.getInstance();
		pcSwitch.Enable_Daxtra_Skill_Parsing__c = true;
        insert pcSwitch;
        
        system.debug('pcSwitch ='+ pcSwitch.Enable_Daxtra_Skill_Parsing__c);
	    WebDocManagementExt theextension= new WebDocManagementExt(thestandardcontroller);
	    system.assert(theextension.activeSkillParsing == Userinfo.getOrganizationId().substring(0, 15));
	    theextension.pageinit();
	    UserInformationCon usercls = new UserInformationCon();
	    system.assert(theextension.userBucket == usercls.getUserDetail().AmazonS3_Folder__c);
	}
}