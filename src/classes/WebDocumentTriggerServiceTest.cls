/**
 * This is a Test class for WebDocumentTriggerService
 * 
 * Created by: Lina
 * Created on: 31/05/2016
 */

@isTest
private class WebDocumentTriggerServiceTest {
    
    static testmethod void testBulkInsert() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Disable Web Document Trigger
        TriggerHelper.DisableTriggerOnWebDocument = True;
        
        // Create data
        Contact con1 = new Contact(LastName='test1',email='test1@test.com');
        Contact con2 = new Contact(LastName='test2',email='test2@test.com');
        Contact con3 = new Contact(LastName='test3',email='test3@test.com');
        insert con1;
        insert con2;
        insert con3;
        //List<Id> conIdList = new List<Id>{con1.Id, con2.Id, con3.Id, con4.Id};
        
        // insert one resume for con1
        Web_Document__c doc11 = new Web_Document__c(name='a', Document_Related_To__c=con1.Id, Document_Type__c='Resume');
        insert doc11;
        // insert two resume for con2
        Web_Document__c doc21 = new Web_Document__c(name='c', Document_Related_To__c=con2.Id, Document_Type__c='Resume');
        insert doc21;
        Web_Document__c doc22 = new Web_Document__c(name='f', Document_Related_To__c=con2.Id, Document_Type__c='Resume');
        insert doc22;
        // insert three resume for con3
        Web_Document__c doc31 = new Web_Document__c(name='d', Document_Related_To__c=con3.Id, Document_Type__c='Resume');
        insert doc31;
        Web_Document__c doc32 = new Web_Document__c(name='e', Document_Related_To__c=con3.Id, Document_Type__c='Resume');
        insert doc32;
        sleep(1500);
        Web_Document__c doc33 = new Web_Document__c(name='b', Document_Related_To__c=con3.Id, Document_Type__c='Resume');
        insert doc33;
    
        Test.startTest();
        List<Id> docIdList = new List<Id>{doc11.Id, doc21.Id, doc22.Id, doc31.Id, doc32.Id, doc33.Id}; 
        List<Web_Document__c> docList = [SELECT Id, Name, Document_Related_To__c, LastModifiedDate FROM Web_Document__c WHERE id in: docIdList];
        docList.sort();
        Set<Id> singleResumeSet = WebDocumentTriggerService.getResumeDedupe(docList);
        Test.stopTest();
        
        // Verify result
        System.assertEquals(3, singleResumeSet.size());
        System.assertEquals(true, singleResumeSet.contains(doc11.Id));
        System.assertEquals(true, singleResumeSet.contains(doc22.Id));
        System.assertEquals(true, singleResumeSet.contains(doc33.Id));
    	}
        
    }

    static testmethod void testWithoutDocumentRelatedToId() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Disable Web Document Trigger
        TriggerHelper.DisableTriggerOnWebDocument = True;
        
        // Create data
        Contact con1 = new Contact(LastName='test1',email='test1@test.com');
        Contact con2 = new Contact(LastName='test2',email='test2@test.com');
        insert con1;
        insert con2;
        
        // insert one resume for con1
        Web_Document__c doc11 = new Web_Document__c(name='a', Document_Related_To__c=con1.Id, Document_Type__c='Resume');
        insert doc11;
        // insert two resume for con2
        Web_Document__c doc21 = new Web_Document__c(name='c', Document_Related_To__c=con2.Id, Document_Type__c='Resume');
        insert doc21;
        Web_Document__c doc22 = new Web_Document__c(name='f', Document_Related_To__c=con2.Id, Document_Type__c='Resume');
        insert doc22;
        delete con1;
        delete con2;

        Test.startTest();
        List<Id> docIdList = new List<Id>{doc11.Id, doc21.Id, doc22.Id}; 
        List<Web_Document__c> docList = [SELECT Id, Name, Document_Related_To__c, LastModifiedDate FROM Web_Document__c WHERE id in: docIdList];
        docList.sort();
        Set<Id> singleResumeSet = WebDocumentTriggerService.getResumeDedupe(docList);
        Test.stopTest();
        
        System.assertEquals(0, singleResumeSet.size());
    	}
        
    }     
    
    
    // Allow thread to sleep for 1s 
    // @param sleepTime = 1000
    public static void sleep(Integer sleepTime) {
        Long startTime = DateTime.now().getTime();
        Long finishTime = DateTime.now().getTime();
        while ((finishTime - startTime) < sleepTime) {
            //sleep for 9s
            finishTime = DateTime.now().getTime();
        }
    }     
}