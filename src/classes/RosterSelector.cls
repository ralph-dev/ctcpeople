public with sharing class RosterSelector extends CommonSelector{

	public RosterSelector(){
		super(Days_Unavailable__c.SObjectType);
	}

	final static String ROSTER_QUERY_BASE1 	= 'SELECT Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,' 
											+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
											+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
											+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
											+ 'Recurrence_End_Date__c, RecordTypeId,Rate__c FROM Days_Unavailable__c';	
											
	final static String ROSTER_QUERY_BASE2 	= 'SELECT Contact__c, Contact__r.Name, Start_Date__c, End_Date__c,'
											+ 'Event_Status__c FROM Days_Unavailable__c';	
	
	final static String ROSTER_QUERY_BASE3 = 'SELECT Id FROM Days_Unavailable__c';
	
	final static String ROSTER_QUERY_BASE4 = 'SELECT Id, Shift__c FROM Days_Unavailable__c';

	final static String ROSTER_QUERY_BASE5 = 'SELECT Id, Shift__c, Contact__c FROM Days_Unavailable__c';
	
	final static String ROSTER_STATUS_FILLED = 'Filled';
	
	public List<Days_Unavailable__c> fetchRosterListByCandidateId(String canId){
		//get record type id by developername
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');
		String query = ROSTER_QUERY_BASE1 +' WHERE RecordTypeId =: rtId and Contact__c =: canId';
		checkRead('Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,'
				+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
				+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
				+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
				+ 'Recurrence_End_Date__c, RecordTypeId,Rate__c');
		List<Days_Unavailable__c> canRosterList = Database.query(query);
		
		return canRosterList;		
	}
	
	public List<Days_Unavailable__c> fetchRosterListByCandidateIdAndStatus(String canId, String status){
		//get record type id by developername
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');


		String query = ROSTER_QUERY_BASE1 +' WHERE RecordTypeId =: rtId and Contact__c =: canId and Event_Status__c =:status';
		checkRead('Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,'
				+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
				+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
				+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
				+ 'Recurrence_End_Date__c, RecordTypeId,Rate__c');
		List<Days_Unavailable__c> canRosterList = Database.query(query);
		
		return canRosterList;		
	}
	
	public List<Days_Unavailable__c> fetchRosterListByClientId(String accId){
		//get record type id by developername
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');


		String query = ROSTER_QUERY_BASE1 +' WHERE RecordTypeId =: rtId and Accounts__c =: accId';
		checkRead('Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,'
				+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
				+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
				+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
				+ 'Recurrence_End_Date__c, RecordTypeId,Rate__c');
		List<Days_Unavailable__c> accRosterList = Database.query(query);
		
		return accRosterList;	
	}
	
	public List<Days_Unavailable__c> fetchRosterListByClientIdAndStatus(String accId, String status){
		//get record type id by developername
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');


		String query = ROSTER_QUERY_BASE1 +' WHERE RecordTypeId =: rtId and Accounts__c =: accId and Event_Status__c =: status';
		checkRead('Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,'
				+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
				+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
				+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
				+ 'Recurrence_End_Date__c, RecordTypeId,Rate__c');
		List<Days_Unavailable__c> accRosterList = Database.query(query);
		
		return accRosterList;	
	}
	
	public List<Days_Unavailable__c> fetchRosterListByRosterId(String rosterId){
		//get record type id by developername
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');


		String query = ROSTER_QUERY_BASE1 +' WHERE RecordTypeId =: rtId and Id =: rosterId';
		checkRead('Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,'
				+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
				+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
				+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
				+ 'Recurrence_End_Date__c, RecordTypeId,Rate__c');
		List<Days_Unavailable__c> rosterList = Database.query(query);
		
		return rosterList;	
	}
	
	public List<Days_Unavailable__c> fetchRosterListByRosterIdList(List<String> rosterIdList){
		//get record type id by developername
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');
		
		String query = ROSTER_QUERY_BASE1 +' WHERE RecordTypeId =: rtId and Id In: rosterIdList';
		
		checkRead('Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,'
				+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
				+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
				+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
				+ 'Recurrence_End_Date__c, RecordTypeId,Rate__c');
		List<Days_Unavailable__c> rosterList = Database.query(query);
		
		return rosterList;	
	}
	
	public List<Days_Unavailable__c> fetchUnavailableCandidateList(String canId){
		//get record type id by developername
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Availability');
		String unavailStatus = 'Unavailable';
		
		String query = ROSTER_QUERY_BASE2 +' WHERE RecordTypeId =: rtId AND Contact__c =:canId AND Event_Status__c =:unavailStatus';
		
		checkRead('Contact__c, Contact__r.Name, Start_Date__c, End_Date__c,'
				+ 'Event_Status__c');
		List<Days_Unavailable__c> unavailCanList = Database.query(query);
		
		//System.debug('unavailCanList'+unavailCanList);
		
		return unavailCanList;	
	}
	
	public List<Days_Unavailable__c> fetchRosterListByShiftId(String shiftId){
		//get record type id by developername
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');
		
		System.debug('shiftId'+shiftId);
		String query = ROSTER_QUERY_BASE3 +' WHERE RecordTypeId =: rtId and Shift__c =: shiftId';
		
		checkRead('Id');
		List<Days_Unavailable__c> rosterList = Database.query(query);
		//System.debug('rosterList'+rosterList);
		
		return rosterList;
	}
	
	public List<Days_Unavailable__c> fetchFilledRosterListByShiftIdList(List<String> shiftIdList){
		//get record type id by developername
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');
		
		String query = ROSTER_QUERY_BASE1 +' WHERE RecordTypeId =: rtId AND Shift__c in: shiftIdList AND Event_Status__c =:ROSTER_STATUS_FILLED';
		
		checkRead('Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,'
				+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
				+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
				+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
				+ 'Recurrence_End_Date__c, RecordTypeId,Rate__c');
		List<Days_Unavailable__c> rosterList = Database.query(query);
		//System.debug('rosterList'+rosterList);
		
		return rosterList;	
	}
	
	
	public List<Days_Unavailable__c> fetchRosterListByShiftIdList(List<String> shiftIdList){
		//get record type id by developername
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');
		
		String query = ROSTER_QUERY_BASE1 +' WHERE RecordTypeId =: rtId AND Shift__c in: shiftIdList';
		
		checkRead('Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,'
				+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
				+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
				+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
				+ 'Recurrence_End_Date__c, RecordTypeId,Rate__c');
		List<Days_Unavailable__c> rosterList = Database.query(query);
		//System.debug('rosterList'+rosterList);
		
		return rosterList;	
	}
	
	public List<Days_Unavailable__c> fetchRosterListByShiftIdSet(Set<ID> shiftIdSet){
		//get record type id by developername
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');
		
		String query = ROSTER_QUERY_BASE1 +' WHERE RecordTypeId =: rtId AND Shift__c in: shiftIdSet';
		checkRead('Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,'
				+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
				+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
				+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
				+ 'Recurrence_End_Date__c, RecordTypeId,Rate__c');
		List<Days_Unavailable__c> rosterList = Database.query(query);
		//System.debug('rosterList'+rosterList);
		
		return rosterList;	
	}

	public Map<String, Days_Unavailable__c> getDuplicateRoster(Set<String> IdSet, List<Shift__c> shifts) {
		Map<String, Days_Unavailable__c> rosterKeyMap = new Map<String, Days_Unavailable__c>();

		String query = ROSTER_QUERY_BASE5 + ' WHERE Contact__c in: IdSet AND Shift__c in: shifts';
		checkRead('Id, Shift__c, Contact__c');
		List<Days_Unavailable__c> rosterList = Database.query(query);
		//system.debug('rosterList ='+ rosterList);
		if(rosterList!= null && rosterList.size()>0){
			for(Days_Unavailable__c roster : rosterList) {
				rosterKeyMap.put(roster.Shift__c+':'+roster.Contact__c, roster);
			}
		}
		//system.debug('rosterKeyMap ='+ rosterKeyMap);
		return rosterKeyMap;
	}
}