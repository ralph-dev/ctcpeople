public with sharing class AstutePayrollRecordsDTO {
	public String accountId;
	public String billingContactId;
	public String firstApproverId;
	public String secondApproverId;
	public String candidateId;
	public String placementId;
	public String workplaceId;
	public String invoiceItemId;
	/***
		Used to transfer brand new invoice;
	
	**/
	public map<String,Object> getBrandNewInvoiceMap(){
		map<String,Object> finalMaps=new map<String,Object>();
		if(biller!=null){
			//System.debug('biller'+ biller);
			finalMaps.put('biller',biller);
		}
		if(billingContact!=null){
			//System.debug('billingContact'+ billingContact);
			finalMaps.put('billingContact',billingContact);
		}
		if(invoiceItem!=null){
			finalMaps.put('invoiceItem',invoiceItem);
		}
		return finalMaps;
		
	}
	public map<String,Object> getRequestMap(){
		map<String,Object> finalMaps=new map<String,Object>();
		if(biller!=null){
			//System.debug('biller'+ biller);
			finalMaps.put('biller',biller);
		}
		if(billingContact!=null){
			//System.debug('billingContact'+ billingContact);
			finalMaps.put('billingContact',billingContact);
		}
		if(firstApprover!=null){
			//System.debug('firstApprover'+ firstApprover);
			finalMaps.put('firstApprover',firstApprover);
		}
		if(secondApprover!=null){
			//System.debug('secondApprover'+ secondApprover);
			finalMaps.put('secondApprover',secondApprover);
		}
		if(workplace!=null){
			//System.debug('workplace'+ workplace);
			finalMaps.put('workplace',workplace);
		}
		if(employee!=null){
			//System.debug('employee'+ employee);
			finalMaps.put('employee',employee);
		}
		return finalMaps;
	}
	private map<String,Object> invoiceItem;
	private map<String,Object> biller;
	private map<String,Object> billingContact;
	private map<String,Object> firstApprover;
	private map<String,Object> secondApprover;
	private map<String,Object> employee;
	private map<String,Object> workplace;
	public void setInvoiceItem(map<String,Object> invoiceItems){
		if(invoiceItemId!=null && invoiceItems.containsKey(invoiceItemId)){
			invoiceItem=(map<String,Object>)invoiceItems.get(invoiceItemId);
		}
	}
	public void setBiller(map<String,Object> billers){
		System.debug('Account id is '+accountId);
		//System.debug('Billers is '+billers);
		if(accountId!=null && billers.containsKey(accountId)){
			biller=(map<String,Object>)billers.get(accountId); 
		}
	}
	public void setBillingContact(map<String,Object> accountbillingContacts){
		if(billingContactId!=null && accountbillingContacts.containsKey(billingContactId)){
			billingContact=(map<String,Object>)accountbillingContacts.get(billingContactId); 
		}
	}
	public void setFirstApprover(map<String,Object> firstApprovers){
		if(firstApproverId!=null && firstApprovers.containskey(firstApproverId)){
			firstApprover=(map<String,Object>)firstApprovers.get(firstApproverId);
		}
	}
	public void setSecondApprover(map<String,Object> secondApprovers){
		if(secondApproverId!=null && secondApprovers.containsKey(secondApproverId)){
			secondApprover=(map<String,Object>)secondApprovers.get(secondApproverId);
		}
	}
	public void setWorkplace(map<String,Object> workplaces){
		if(workplaceId!=null && workplaces.containsKey(workplaceId)){
			workplace=(map<String,Object>)workplaces.get(workplaceId);
		}
	}
	public void setEmployee(map<String,Object> placements,map<String,Object> candidates){
		if(candidateId!=null && placementId!=null
			&& placements.containsKey(placementId) && candidates.containsKey(candidateId)){
			employee=(map<String,Object>)candidates.get(candidateId);
			for(String key: ((map<String,Object>) placements.get(placementId)).keySet()){
				employee.put(key,((map<String,Object>) placements.get(placementId)).get(key));
			}
		}
	}
	

}