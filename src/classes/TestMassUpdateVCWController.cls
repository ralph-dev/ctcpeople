@isTest
private class TestMassUpdateVCWController {
	
	//static testMethod void testthiscontroller(){
	//    DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
	    
	//    System.runAs(DummyRecordCreator.platformUser) {
	    	
 //   		CandidateManagementDummyRecordCreator cmCreator = new CandidateManagementDummyRecordCreator();
    		
 //   	    List<Placement_Candidate__c> vacpc = cmCreator.generateCandidateManagementDummyRecord(rs);
 //   	    Test.startTest();
 //   		Apexpages.StandardsetController accConvac = new Apexpages.StandardsetController(vacpc);
 //   			//ExtractData4SRController ed7 = new ExtractData4SRController(accConvac);
 //   		MassUpdateVCWController thecontroller = new MassUpdateVCWController(accConvac);
 //   	   	thecontroller.thesamplepc.Status__c = 'test';
 //   	   	thecontroller.thesamplepc.Candidate_Status__c = 'test again';	   
 //   	    //thecontroller.setThesamplepc(pc);
 //   	    thecontroller.updatestatus();
 //   	    List<Placement_Candidate__c> tempcms1 = [select Id, Status__c, Candidate_Status__c from Placement_Candidate__c where id in: vacpc];
 //   	    system.assert(tempcms1[0].Status__c ==  'test');
 //   	    system.assert(tempcms1[0].Candidate_Status__c == 'test again');
 //   	    thecontroller.is_test = true;
 //   	    Document tempdoc = DummyRecordCreator.CreateDocument();
 //   	    thecontroller.docsId = tempdoc.id;
 //   	    thecontroller.addlocaldocs();
 //   	    system.assert(thecontroller.optInlocalDocList.size()>0);
 //   	    List<String> tempids = thecontroller.getAllDocumentsId();
 //   	    system.assert(tempids.size() == 1);
 //   	    EmailTemplate result =  [Select TimesUsed, TemplateType, TemplateStyle, 
 //   			SystemModstamp, Subject, OwnerId, NamespacePrefix, 
 //   			Name, Markup, LastUsedDate, LastModifiedDate, 
 //   			LastModifiedById, IsActive, Id, HtmlValue, 
 //   			FolderId, Encoding, DeveloperName, Description, 
 //   			CreatedDate, CreatedById, BrandTemplateId, Body, 
 //   			ApiVersion From EmailTemplate where (TemplateStyle='none' and TemplateType='text') limit 1];		
 //   		thecontroller.selectedTempId = result.Id;
 //   		thecontroller.resetEmailContent();
 //   		PageReference pr1 = thecontroller.previewTemp();
 //   		system.assert(pr1 == null);
 //   		thecontroller.createCustomTemplate();
 //   		system.assert(thecontroller.sendOutTemp != null);
 //   	    thecontroller.sendemails();
 //   	    thecontroller.delCustomTemplate();
 //   		system.assert(thecontroller.sendOutTemp == null);
 //   		thecontroller.sendemails();
 //   		system.assert((ApexPages.getMessages()).size()>0);
	//	    thecontroller.delCustomTemplate();
	//	    Test.stopTest();
	//    }
	    
	//}
	
	//static testmethod void testShowallfun(){
	//    System.runAs(DummyRecordCreator.platformUser) {
 //   		Placement_Candidate__c tempcm = DataTestFactory.createCandiateManangement();
 //   		Test.startTest();
 //   		Test.setCurrentPage(Page.MassUpdateVCW);
 //   		ApexPages.currentPage().getParameters().put('id', tempcm.id);
 //   		ApexPages.currentPage().getParameters().put('cmids', tempcm.Candidate__r.id);
 //   		ApexPages.currentPage().getParameters().put('cp','0');
 //   		ApexPages.currentPage().getParameters().put('sc','a');
 //   		ApexPages.currentPage().getParameters().put('asc','DSC');		
 //   		List<Placement_Candidate__c> tempcms = new List<Placement_Candidate__c>();
 //   	    ApexPages.StandardSetController controller = new ApexPages.StandardSetController(tempcms); 
 //   	    MassUpdateVCWController thecontroller = new MassUpdateVCWController(controller);
 //   	    thecontroller.commandlinkaction ='backToShowAll';
 //   	    PageReference p1 = thecontroller.backToShowAll();	    
 //   	    system.assert(p1 == null );
 //   	    PageReference p2 = thecontroller.updateAndReturn();
 //   	    system.assert(p2 == null );
 //   	    Test.stopTest();
	//    }
	//}	
	
	//static testMethod void test() {
	//    System.runAs(DummyRecordCreator.platformUser) {
	//        Test.startTest();
 //   	    MassUpdateVCWController c = new MassUpdateVCWController(new ApexPages.StandardSetController(new List<Placement_Candidate__c>()));
    	    
 //   	    system.assertNotEquals(null, c.isLocalDocListEmpty);
 //   	    system.assertNotEquals(null, c.isAttachmentsListEmpty);
 //   	    system.assertNotEquals(null, c.isAttachmentsFromEmailTemplateEmpty);
 //   	    system.assertNotEquals(null, c.isOversized());
 //   	    system.assertNotEquals(null, c.getTotalSize());
 //   	    system.assertNotEquals(null, c.getAllAttachmentsId());
 //   	    system.assertEquals(null, c.deleteDummyDoc(new List<Id>()));
 //   	    try {
 //   	        system.assertNotEquals(null, c.removeDummyDoc());
 //   	    } catch (Exception e) {
 //   	    }
 //   	    system.debug(c.selectedProgress);
 //   	    system.debug(c.selectedStatus);
 //   	    system.debug(c.recordsize);
    	    
 //   	    system.assertNotEquals(null, c.getOrgEmails());
 //   	    system.assertEquals(null, c.emailTempList);
 //   	    system.assertEquals(null, c.dellocalDocId);
 //   	    Test.stopTest();
	//    }
	//}
}