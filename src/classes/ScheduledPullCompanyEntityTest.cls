@isTest
private class ScheduledPullCompanyEntityTest {
	
	@isTest static void test_method_one() {
		ScheduledPullCompanyEntity scheduledCompanyEntity=new ScheduledPullCompanyEntity();
		String CRON_EXP = '0 0 0 3 9 ?';
      String jobId = System.schedule('TestApex',CRON_EXP,scheduledCompanyEntity);
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
        NextFireTime
        FROM CronTrigger WHERE id = :jobId];
         System.assertEquals(CRON_EXP, 
         ct.CronExpression);		
         
         system.assert(true);
	}
	
	
}