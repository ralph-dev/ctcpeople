public class DescribeHelper {

    /**
     * Get a serilizable map of describe field result of all fields of given list of field tokens.
     * @param fieldTokens 
     *      List of Schema.SObjectField for generating describe field result. 
     *      Schema.SObjectField can be access through Schema.SObjectType.SOBJECT_NAME.fields.getMap().values()
     * @return 
     *      A map, in which key is field api name value is the describe field result object.
     *      This map is serilizable using SF JSON utility class
     *          
     **/
    public static Map<String, Schema.DescribeFieldResult> getDescribeFieldResult(List<Schema.SObjectField> fieldTokens) {
        Map<String, Schema.DescribeFieldResult> describeResultMap = new Map<String, Schema.DescribeFieldResult>();
        for (Schema.SObjectField ft:fieldTokens) {
            Schema.DescribeFieldResult dr = ft.getDescribe();
            describeResultMap.put(dr.getName(), dr);
        }
        return describeResultMap;
    }
    
    
    public static Boolean hasValueInPicklist(String value, Schema.SObjectField f){
        Schema.DescribeFieldResult dr = f.getDescribe();
        try {
            List<Schema.PicklistEntry> entries = dr.getPicklistValues();    
            for (Schema.PicklistEntry e: entries) {
                if (e.getValue().equals(value)) {
                    return true;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public static Map<String, Schema.DescribeFieldResult> getDescribeSelectFieldResult(List<Schema.SObjectField> fieldTokens, List<String> selectFieldsAPIName) {
        Map<String, Schema.DescribeFieldResult> describeFieldResult = new Map<String, Schema.DescribeFieldResult>();
        Map<String, Schema.DescribeFieldResult> selectDescribeSelectFieldMap = new Map<String, Schema.DescribeFieldResult>();
        describeFieldResult = getDescribeFieldResult(fieldTokens);
        for(String fieldAPIName: selectFieldsAPIName) {
            if(describeFieldResult.keySet().contains(fieldAPIName)) {
                selectDescribeSelectFieldMap.put(fieldAPIName,describeFieldResult.get(fieldAPIName));
            }
        }

        return selectDescribeSelectFieldMap;
    }

    /*
    @Author: Tom Lee 01.09.2016
    @Description: Return List<Schema.PicklistEntry> of input pickList filed of Input SObject
    @Param: SObjectName
    @Param: PickList field Name
    @Return : Return List<Schema.PicklistEntry>
    */
    public static List<Schema.PicklistEntry> getPickListValues(String objectName, String fieldName){
        fflib_SObjectDescribe describe = fflib_SObjectDescribe.getDescribe(objectName);
        Schema.SObjectField field = describe.getField(fieldName);
        return field.getDescribe().getPicklistValues();
    }

    /*
    @Author: Tom Lee 01.09.2016
    @Description: Return pickList Map<Label,Value> 
    @Param: SObjectName
    @Param: PickList field Name
    @Return : Map <Label, Value> 
    */
    public static Map<String,String> getPickListValueMap(String objectName, String fieldName){
        Map<String,String> result=new Map<String,String>();
        List<Schema.PicklistEntry> pickList=getPickListValues(objectName, fieldName);
        if(!pickList.isEmpty()){
            for(Schema.PicklistEntry pickListObject :pickList){
                result.put(pickListObject.getLabel(),pickListObject.getValue());
                
            }
        }
        return result;
    }
}