@isTest
public with sharing class AdvertisementDummyRecordCreator {
	private List<Advertisement__c> ads {get; set;}
	private Set<Id> adIdSet {get; set;}
    private Map<Id, Advertisement__c> adMap {get; set;}

    public AdvertisementDummyRecordCreator() {
		ads = new List<Advertisement__c>();
		adIdSet = new Set<Id>();
		adMap = new Map<Id, Advertisement__c>();
	}

	public List<Advertisement__c> generateAdvertisementDummyRecord(DummyRecordCreator.DefaultDummyRecordSet rs) {
		for(Integer i=0;i<rs.vacancies.size();i++) {
			Advertisement__c ad = new Advertisement__c(RecordTypeId=DaoRecordType.seekAdRT.Id, Vacancy__c=rs.vacancies[i].Id);
			ads.add(ad);
		}
		//check FLS
		/**
		List<String> fieldList = new List<String>{
			'RecordTypeId', 'Vacancy__c'
		};
		fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
		*/
		CommonSelector.quickInsert( ads );
		return ads;
	}


	// Generate only one advertisement 
	public Advertisement__c generateOneAdvertisementDummyRecord(DummyRecordCreator.DefaultDummyRecordSet rs) {
		Advertisement__c ad = new Advertisement__c(RecordTypeId=DaoRecordType.seekAdRT.Id, Vacancy__c=rs.vacancies[0].Id);
		//check FLS
		/**
		List<String> fieldList = new List<String>{
			'RecordTypeId', 'Vacancy__c'
		};
		fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
		*/
		CommonSelector.quickInsert( ad);
		return ad;
	}

	public Advertisement__c generateCareerOneAdDummyRecord(DummyRecordCreator.DefaultDummyRecordSet rs) {
		//system.debug('rs ='+ rs);
		Advertisement__c ad = new Advertisement__c(RecordTypeId=DaoRecordType.careerOneAdRT.Id, Vacancy__c=rs.vacancies[0].Id, 
		                Skill_Group_Criteria__c='G000001,G000002,G000004,G000005', SearchArea_String__c='Australia - NSW - Sydney' ,Online_Job_Id__c='123456', Status__c='Deleted', City_C1__c = 'Sydney', State__c ='NSW', Cat_String__c = '1');
		//checkFLS();
		
		
		CommonSelector.quickInsert( ad );
		return ad;
	}

	private void checkFLS(){
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			Advertisement__c.RecordTypeId,
			Advertisement__c.Vacancy__c,
			Advertisement__c.Skill_Group_Criteria__c,
			Advertisement__c.SearchArea_String__c,
			Advertisement__c.Online_Job_Id__c,
			Advertisement__c.Status__c,
			Advertisement__c.City_C1__c,
			Advertisement__c.State__c
		};
		fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
	}
	
	public static Advertisement__c generateJXTAdDummyRecord() {
		Advertisement__c ad = new Advertisement__c();
		ad.RecordTypeId = DaoRecordType.JXTAdRT.Id;
		ad.WebSite__c='a';	 
		ad.Location__c='a'; 	
		ad.SearchArea_Label_String__c='a';
		ad.Classification__c='a'; 	
		ad.SubClassification2__c='a';
		ad.Classification2__c='a';	
		ad.SubClassification_Seek_Label__c='a';	 
		ad.WorkType__c='a';
		ad.Reference_No__c='a';		
		ad.Job_Title__c='a';		
		ad.Company_Name__c='a';
		ad.Template__c='a';
		ad.Bullet_1__c='a';		
		ad.Bullet_2__c='a';		
		ad.Bullet_3__c='a';			
		ad.Job_Type__c='a';
		ad.JXT_Short_Description__c='a';	
		ad.jxt_sectors__c='a';		
		ad.Hot_Job__c=true;
		ad.Residency_Required__c=true;		
		ad.Search_Tags__c='a';		
		ad.Street_Adress__c='a';
		ad.Nearest_Transport__c='a';		
		ad.Job_Contact_Name__c='a';		
		ad.Job_Contact_Phone__c='12345';
		ad.Location_Hide__c =false;			
		ad.Salary_Type__c='a';			
		ad.SeekSalaryMin__c='123';
		ad.SeekSalaryMax__c='2353'; 		
		ad.Salary_Description__c='a';	
		ad.No_salary_information__c=false;
		ad.Job_Content__c='a';			
		ad.Has_Referral_Fee__c='a';	
		ad.Referral_Fee__c=500; 
		ad.Status__c ='Active';
		ad.Terms_And_Conditions_Url__c=Endpoint.getPeoplecloudWebSite()+'';
		return ad;
	}

	public static Advertisement__c generateSeekAdDummyRecord() {
		Advertisement__c ad = new Advertisement__c();
		ad.RecordTypeId = DaoRecordType.seekAdRT.Id;
		ad.WebSite__c='Seek';

		ad.Job_Title__c = 'Job Title';
		ad.Online_Search_Title__c = 'Online Search Title';
        /********* Set location *********/
		ad.RealLocation_Seek__c = 'Sydney';
		ad.RealArea_Seek__c = 'CBD';
        /***********************************/
		ad.SeekSubclassification__c = 'SoftwareDeveloper';
		ad.WorkType__c = 'FullTime';
        /********* Set Salary *********/
		ad.SeekSalaryType__c = 'Annual';
		ad.SeekSalaryMin__c = '100000';
		ad.SeekSalaryMax__c = '200000';
		ad.SeekSalaryText__c = '$100000 - $200000';
        /***********************************/
		ad.Online_Summary__c = 'Online Summary';
		ad.Job_Content__c = 'Job Content';
		ad.Online_Footer__c = 'Online Footer';
        /********* Set Contact *********/
		ad.Job_Contact_Name__c = 'Test';
		ad.Job_Contact_Phone__c = '12345678';
		ad.Job_Contact_Email__c = 'test@test.com';
        /***********************************/
        /********* Set Video *********/
		ad.Video_ID__c = 'testVideo';
		ad.Video_Position__c = 'Above';
        /***********************************/
		ad.Prefer_Email_Address__c = 'test@test.com';
		ad.SeekScreen__c = '12345';
        /********* Set Template *********/
		ad.TemplateCode__c = '12345';
        /***********************************/
        /********* Set Standout *********/
		ad.AdvertiserJobTemplateLogoCode__c = '12345';
		ad.Search_Bullet_Point_1__c = 'Point1';
		ad.Search_Bullet_Point_2__c = 'Point2';
		ad.Search_Bullet_Point_3__c = 'Point3';
        /***********************************/
        ad.Residency_Required__c = true;
		ad.SeekMarketSegment__c = 'Graduate';
		ad.Online_Job_Id__c = 'SeekAdId';
		return ad;
	}
}