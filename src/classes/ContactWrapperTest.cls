@isTest
public class ContactWrapperTest {
    private testMethod static void testSort() {
    	System.runAs(DummyRecordCreator.platformUser) {
        List<ContactWrapper> cwList = new List<ContactWrapper>();
        
        Contact c1 = new Contact(lastName = 'test1', Birthdate = Date.newInstance(1990, 01, 01));
        Contact c2 = new Contact(lastName = 'test2', Birthdate = Date.newInstance(1990, 01, 02));
        Contact c3 = new Contact(lastName = 'test3', Birthdate = Date.newInstance(1990, 01, 03));
        
        cwList.add(new ContactWrapper(c3, 'Birthdate'));
        cwList.add(new ContactWrapper(c2, 'Birthdate'));
        cwList.add(new ContactWrapper(c1, 'Birthdate'));
        
        cwList.sort();
        
        System.assertEquals(c1.lastName, cwList.get(0).con.lastName);
    	}
    }
}