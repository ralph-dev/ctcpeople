/*****	
* 	Added by Alvin for the paginator of the Sobject
*								
*
*										15th Aug 13
*
*
*/
public with sharing class PaginatorForSObj {
	private Integer currPage;
    public Integer totalPages;
    private List<Sobject> objs;
    private List<Sobject> pagedResult;
    public Integer recPerPage=10;
    
    public PaginatorForSObj(List<Sobject> objs){
        this();        
        paginateCollection(objs);
    }
    
    //change the number of record per page
    public void changerecPerpage(Integer tempRecPerpage){
        recPerPage = tempRecPerpage;
        Decimal totalNum=objs.size();
        Decimal decTotalPageNum =totalNum / recPerPage;
        totalPages = decTotalPageNum.round(System.RoundingMode.CEILING).intvalue();
        currPage=1;
        
    }
    
    public PaginatorForSObj(){
        currPage = 0;
        totalPages = 0;
        pagedResult = new List<Sobject>();
    }
    
    public List<Sobject> paginateCollection(List<Sobject> objs){
        this.objs = objs;
        
        Decimal totalNum=objs.size();
        Decimal decTotalPageNum =totalNum / recPerPage;
        totalPages = decTotalPageNum.round(System.RoundingMode.CEILING).intvalue();
        if(totalNum>0){
            currPage=1;
        }else
            currPage=0;
        return getNthPageCollection(currPage);
    }
    
    public List<Sobject> getNthPageCollection(Integer page){
        pagedResult = new List<Sobject>();
        if(page>totalPages || page <1)
            return pagedResult;
        currPage = page;    
        Integer startIndex = (page -1)* recPerPage;
        Integer endIndex = page * recPerPage -1;
        for(Integer i=startIndex;i<objs.size() && i<=endIndex;i++){
            pagedResult.add(objs.get(i));
        }
        return pagedResult;
    }
    
    // go to the next page
    public List<Sobject> getNextPageCollection(){
        if(currPage<totalPages){
            currPage++;
            pagedResult = getNthPageCollection(currPage);
        }
        //system.debug('*********** current page' + currPage);
        return pagedResult;
    }
    
    // go to the previous page
    public List<Sobject> getPrevPageCollection(){
        if(currPage>1){
            currPage--;
            pagedResult = getNthPageCollection(currPage);
        }
        //system.debug('^^^^^^^^^^^^^ current page' + currPage);
        return pagedResult;
    }    
    
    // go to the first page
    public List<Sobject> getFirstPageCollection(){
        currPage = 1;
        //system.debug('^^^^^^^^^^^^^ current page' + currPage);
        return paginateCollection(objs);
    }
    
    // go to the last page
    public List<Sobject> getLastPageCollection(){
        pagedResult = getNthPageCollection(totalPages);
        currPage = totalPages;
        //system.debug('^^^^^^^^^^^^^ current page' + currPage);
        return pagedResult;
    }      

    
    public Integer getCurrentPage(){
        return currPage;
    }
    
    public Boolean hasNextPage(){
        return currPage<totalPages;
    }

    public Boolean hasPrevPage(){
        return currPage>1;
    }   
    
    public Integer getTotalRecNumber(){
        return objs.size();
    }
}