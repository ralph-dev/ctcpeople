/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

/*
	GoogleSearchCaculateAvailabiltyRate class to caculate the availabilty weight for per sutiable candidate
	Author by Jack, 20 Dec 2013
*/

 
public with sharing class GoogleSearchCaculateAvailabiltyRate {
	
	
	//public static List<GoogleSearchAvailablityWeight> GoogleSearchCaculateAvailabiltyRate(List<AvailabilityEntity> AvailabilityCompareRange, List<String> CandidateIdList, String searchcolumnString, String sortByColumnApiName, String sortingOrder, Boolean usingColumnView){
	//	list<String> returnresult = new List<String>();
	//	Map<String, Contact> QueryContactFieldMap = new Map<String, Contact>();
	//	List<GoogleSearchAvailablityWeight> tempGoogleSearchAvailablityWeight = new List<GoogleSearchAvailablityWeight>();
	//	if(CandidateIdList!= null && CandidateIdList.size()>0){
	//		List<Contact> contactList = new List<Contact>();
			
	//		// use different way to generate the list of contact according to traditional view or column view
	//		if(usingColumnView){
	//			system.debug('GoogleSearchAvailablityWeight --- usingColumnView='+usingColumnView);
	//			contactList =  AvailabilitySelector.getAllContactInfoList(CandidateIdList, searchcolumnString, sortByColumnApiName, sortingOrder);
	//		}else{
	//			system.debug('GoogleSearchAvailablityWeight --- usingColumnView='+usingColumnView);
	//			contactList = AvailabilitySelector.generateDummyContactList(CandidateIdList);
	//		}	
	//		system.debug('GoogleSearchAvailablityWeight --- contactList=' + contactList);
					
			
	//		QueryContactFieldMap = AvailabilitySelector.convertToAllContactInfoMap(contactList);
	//		List<Days_Unavailable__c> selectalavailabilityrecord = new List<Days_Unavailable__c>();
			
	//		if(AvailabilityCompareRange != null && AvailabilityCompareRange.size()>0){
	//			selectalavailabilityrecord = AvailabilitySelector.getAllRelatedAvlRecordsByStartDateEndDateGoogleResult(AvailabilityCompareRange, CandidateIdList);
	//			if(selectalavailabilityrecord!= null && selectalavailabilityrecord.size()>0){
	//				tempGoogleSearchAvailablityWeight = getAvailabledateFromDaysUnavailableRecords(AvailabilityCompareRange, selectalavailabilityrecord, QueryContactFieldMap);
	//			}else{
	//				tempGoogleSearchAvailablityWeight = getAvailabledateFromDaysUnavailableRecords(AvailabilityCompareRange, selectalavailabilityrecord, QueryContactFieldMap);
	//			}
	//		}
	//		else{//not availablity range search
	//			tempGoogleSearchAvailablityWeight  = generalGoogleSearchMapWithoutAvailablityWeight(QueryContactFieldMap,contactList);
	//		}
	//	}
	//	system.debug('tempGoogleSearchAvailablityWeight ='+ tempGoogleSearchAvailablityWeight);
	//	return tempGoogleSearchAvailablityWeight;
	//}
	
	//private static List<GoogleSearchAvailablityWeight> getAvailabledateFromDaysUnavailableRecords(List<AvailabilityEntity> AvailabilityCompareRange,List<Days_Unavailable__c> selectalavailabilityrecord, Map<String, Contact> QueryContactFieldMap){
	//	List<GoogleSearchAvailablityWeight> returnresult = new List<GoogleSearchAvailablityWeight>();
	//	Map<String , GoogleSearchAvailablityWeight> tempGoogleSearchAvailablityWeightMap = new Map<String , GoogleSearchAvailablityWeight>() ;
	//	Integer totalSearchRangeDates = 0;
	//	for(AvailabilityEntity availentity : AvailabilityCompareRange){
	//		Date availentityStartDate = date.valueOf(availentity.startDate); //Search Range start Date
	//		Date availentityEndDate = date.valueOf(availentity.endDate);     //Search Range end Date
	//		Integer eachSearchRangeDates = availentityStartDate.daysBetween(availentityEndDate)+1;
	//		totalSearchRangeDates += eachSearchRangeDates;
	//		system.debug('availentityStartDate ='+ availentityStartDate);
	//		system.debug('availentityEndDate ='+ totalSearchRangeDates);
	//		Boolean addtotalSearchRangeDates = true;  
	//		Days_Unavailable__c removeoverlaprecord = new Days_Unavailable__c();
	//		Map<String, Days_Unavailable__c> overlapMap = new Map<String, Days_Unavailable__c>();
	//		if(selectalavailabilityrecord!= null && selectalavailabilityrecord.size()>0){//Some time there is no availablity record in these ranges
	//			for(Days_Unavailable__c unavailday : selectalavailabilityrecord){
	//				if(unavailday.Event_Status__c!=null){
	//					removeoverlaprecord = checkOverLapRecord(unavailday, overlapMap);
	//					system.debug('removeoverlaprecord ='+ removeoverlaprecord);
	//					if(removeoverlaprecord == null){
	//						continue;
	//					}else{
	//						unavailday = removeoverlaprecord;
	//					}
	//					GoogleSearchAvailablityWeight tempGoogleSearchAvailablityWeight;
	//					if(tempGoogleSearchAvailablityWeightMap.get(unavailday.Contact__c) == null){//if this candidate doesn't include the map, init a new GoogleSearchAvailablityWeight object
	//						tempGoogleSearchAvailablityWeight = new GoogleSearchAvailablityWeight(unavailday.Contact__c,0,0,0);
	//						addtotalSearchRangeDates = false;
	//					}else{
	//						tempGoogleSearchAvailablityWeight = tempGoogleSearchAvailablityWeightMap.get(unavailday.Contact__c);
	//					}
	//					Integer datedifference = calculatecurrentdates(availentity, unavailday);
	//					if(datedifference >0){
	//						if(unavailday.Event_Status__c.equalsIgnoreCase('available')){
	//							tempGoogleSearchAvailablityWeight.numberofAvailabledays += datedifference;
	//						}else{
	//							tempGoogleSearchAvailablityWeight.numberofUnavailabledays += datedifference;
	//							overlapMap.put(unavailday.Contact__c, unavailday);
	//						}
	//					}
	//					tempGoogleSearchAvailablityWeightMap.put(unavailday.Contact__c, tempGoogleSearchAvailablityWeight);
	//					system.debug('currentContactId =' + tempGoogleSearchAvailablityWeight.currentContactId);
	//					system.debug('numberofUnavailabledays =' + tempGoogleSearchAvailablityWeight.numberofUnavailabledays);
	//					system.debug('numberofAvailabledays =' + tempGoogleSearchAvailablityWeight.numberofAvailabledays);
	//					system.debug('numberofTentativedays =' + tempGoogleSearchAvailablityWeight.numberofTentativedays);
	//				}
	//			}
	//		}
	//	}
	//	returnresult = sortGoogleSearchAvailablityWeightList(tempGoogleSearchAvailablityWeightMap, totalSearchRangeDates, QueryContactFieldMap);
	//	return returnresult;
	//}
	
	////Calculate the date difference
	//private static Integer calculatecurrentdates(AvailabilityEntity availentity ,Days_Unavailable__c unavailday){
	//	Integer calculatecurrentdatesresult = 0;
	//	Date availentityStartDate = date.valueOf(availentity.startDate); //Search Range start Date
	//	Date availentityEndDate = date.valueOf(availentity.endDate);     //Search Range end Date
	//	Date unavaildayStartDate = date.valueOf(unavailday.Start_Date__c);//Candidate Record start Date
	//	Date unavaildayendDate = date.valueOf(unavailday.End_Date__c);    //Candidate Record end Date
		
	//	/*
	//		if Days_Unavailable__c record start date is later than search range end date, don't need calculate
	//	*/
	//	if(unavaildayStartDate > availentityEndDate || unavaildayendDate < availentityStartDate){
	//		return calculatecurrentdatesresult;
	//	}else{
	//		if(unavaildayStartDate < availentityStartDate){ //Days_Unavailable__c record start date is eariler than search range start date
	//			if(unavaildayendDate < availentityEndDate){ //Days_Unavailable__c record end date is later than search range end date
	//				calculatecurrentdatesresult = availentityStartDate.daysBetween(unavaildayendDate);
	//			}
	//			else{
	//				calculatecurrentdatesresult = availentityStartDate.daysBetween(availentityEndDate);
	//			}
	//		}else{
	//			if(unavaildayendDate < availentityEndDate){ //Days_Unavailable__c record end date is later than search range end date
	//				calculatecurrentdatesresult = unavaildayStartDate.daysBetween(unavaildayendDate);
	//			}
	//			else{
	//				calculatecurrentdatesresult = unavaildayStartDate.daysBetween(availentityEndDate);
	//			}
	//		}
	//		return calculatecurrentdatesresult+1;
	//	}
	//}
	
	////Check the unavailable record is overlap record or not
	//private static Days_Unavailable__c checkOverLapRecord(Days_Unavailable__c unavailday, Map<String, Days_Unavailable__c> overlapMap){
	//	Days_Unavailable__c returnunavailday = null;
	//	system.debug('initunavailday ='+ unavailday);
	//	system.debug('overlapMap = '+ overlapMap);
	//	if(unavailday.Event_Status__c.equalsIgnoreCase('unavailable')){
	//		if(overlapMap.get(unavailday.Contact__c)==null){
	//			overlapMap.put(unavailday.Contact__c, unavailday);
	//			return unavailday;
	//		}else{
	//			Days_Unavailable__c tempunavailday = overlapMap.get(unavailday.Contact__c);
	//			system.debug('unavailday ='+ unavailday);
	//			if(tempunavailday.Contact__c == unavailday.Contact__c && tempunavailday.Start_Date__c<= unavailday.Start_Date__c
	//												&& unavailday.Start_Date__c <= tempunavailday.End_Date__c){
	//				if(tempunavailday.End_Date__c >= unavailday.End_Date__c){
	//					return returnunavailday;
	//				}else{
	//					unavailday.Start_Date__c = tempunavailday.End_Date__c + 1;
	//					overlapMap.put(unavailday.Contact__c, unavailday);
	//					return unavailday;
	//				}
					
	//			}else{
	//				overlapMap.put(unavailday.Contact__c, unavailday);
	//				return unavailday;
	//			}
	//		}
	//	}else{//if the record type is available don't need add to the map
	//		return unavailday;
	//	}
	//}
	
	//private static List<GoogleSearchAvailablityWeight> sortGoogleSearchAvailablityWeightList(Map<String , GoogleSearchAvailablityWeight> AvailabledateFromDaysUnavailableRecords, 
	//																						Integer totalsearchrangedays, Map<String, Contact> QueryContactFieldMap){
	//	Map<String , GoogleSearchAvailablityWeight> tempGoogleSearchAvailablityWeightMap = new Map<String , GoogleSearchAvailablityWeight>();
	//	List<GoogleSearchAvailablityWeight> tempGoogleSearchAvailablityWeight = new List<GoogleSearchAvailablityWeight>();
	//	for(String key: QueryContactFieldMap.keySet()){
	//		GoogleSearchAvailablityWeight temp = null;
	//		Contact tempcontact = new Contact();
	//		if(AvailabledateFromDaysUnavailableRecords.containsKey(key)){
	//			temp = AvailabledateFromDaysUnavailableRecords.get(key);
	//			tempcontact = QueryContactFieldMap.get(key);
	//			temp.numberofTentativedays = totalsearchrangedays - temp.numberofAvailabledays - temp.numberofUnavailabledays;
	//			if(tempcontact != null)
	//				temp.tempcontact = tempcontact;
	//		}else{//if the candidate does not have the availablity record in these range. Add the total dates to tentertative dates
	//			temp = new GoogleSearchAvailablityWeight(key,0,0,totalsearchrangedays);
	//			tempcontact = QueryContactFieldMap.get(key);
	//			if(tempcontact != null)
	//				temp.tempcontact = tempcontact;
	//		}
	//		tempGoogleSearchAvailablityWeight.add(temp);
	//	}		
	//	tempGoogleSearchAvailablityWeight.sort();
	//	system.debug('tempGoogleSearchAvailablityWeight ='+ tempGoogleSearchAvailablityWeight);			
	//	return tempGoogleSearchAvailablityWeight;																						
	//}
	
	//private static List<GoogleSearchAvailablityWeight> generalGoogleSearchMapWithoutAvailablityWeight(Map<String, Contact> QueryContactFieldMap, List<Contact> contactList){
	//	List<GoogleSearchAvailablityWeight> tempGoogleSearchAvailablityWeightMap = new List<GoogleSearchAvailablityWeight>();
	//	for(Contact contact : contactList){
	//		Contact tempcon = QueryContactFieldMap.get(contact.Id);
	//		GoogleSearchAvailablityWeight temp = new GoogleSearchAvailablityWeight(tempcon.id,tempcon);
	//		tempGoogleSearchAvailablityWeightMap.add(temp);
	//	}
	//	return tempGoogleSearchAvailablityWeightMap;
	//}
}