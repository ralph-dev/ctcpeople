public with sharing class JxtNZPost { 
    public String short_description_contentTemp {get; set;}
    public String jobContentTemp {get; set;}
    public Advertisement__c ad {get; set;}
    public Advertisement__c newAd {get; set;}
    public IEC2WebService iec2ws{get; set;}
    
    /*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}    
    /*************End Add skill parsing param********************************************************/
    
    public JxtNZPost(ApexPages.StandardController stdController){
    	//Init skill parsing param. Add by Jack on 23/05/2013
        enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
        enableSkillGroupList = new List<Skill_Group__c>();
        enableSkillGroupList = SkillGroups.getSkillGroups();
        defaultskillgroups = new List<String>();
        //end skill parsing param.
        
        ad = (Advertisement__c) stdController.getRecord();
        ad.Terms_And_Conditions_Url__c = 'http://';
        iec2ws = new EC2WebServiceImpl();
        if(ad.Skill_Group_Criteria__c!=null&&ad.Skill_Group_Criteria__c!=''){
        	String defaultskillgroupstring = ad.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
	        defaultskillgroups = defaultskillgroupstring.split(',');
        }
    }
    public List<SelectOption> getTemplateOptions(){
        return JobBoardUtils.getTemplateOptions('jxt_nz');
    }
    public List<SelectOption> getStyleOptions(){
        return JobBoardUtils.getStyleOptions('jxt_nz');
    }
    
    public List<SelectOption> getClassificationItems(){
        return JxtNZUtils.getClassifications();
    }
    public List<SelectOption> getClassificationItems2nd(){
        return JxtNZUtils.get2ndClassifications();
    }
    
    public PageReference feedXml(){
    	CommonSelector.checkRead(User.sObjectType,'AmazonS3_Folder__c');
    	User currentUser = [select AmazonS3_Folder__c from User where Id=:UserInfo.getUserId()];
    	String jobRefCode=UserInfo.getOrganizationId().subString(0, 15)+':'+newAd.id;
    	
    	CommonSelector.checkRead(StyleCategory__c.sObjectType,
    		' Id, Header_EC2_File__c, Header_File_Type__c,'
    	 	+'Div_Html_EC2_File__c, Div_Html_File_Type__c, '
    	 	 +'Footer_EC2_File__c, Footer_File_Type__c  ');
    	StyleCategory__c sc = [select Id, Header_EC2_File__c, Header_File_Type__c,
    	 	Div_Html_EC2_File__c, Div_Html_File_Type__c, 
    	 	 Footer_EC2_File__c, Footer_File_Type__c  
    	  		from StyleCategory__c where id = :newAd.Application_Form_Style__c limit 1];
    	
    	boolean result = iec2ws.insertDetailTable(jobRefCode, UserInfo.getOrganizationId().subString(0, 15),
    	     newAd.id, currentUser.AmazonS3_Folder__c, JobBoardUtils.blankValue(sc.Header_EC2_File__c), 
    	      JobBoardUtils.blankValue(sc.Header_File_Type__c), sc.Div_Html_EC2_File__c,
    	        sc.Div_Html_File_Type__c,  JobBoardUtils.blankValue(sc.Footer_EC2_File__c), 
    	         JobBoardUtils.blankValue(sc.Footer_File_Type__c), '', 'jxt_nz', 
    	             JobBoardUtils.blankValue(newAd.Job_Title__c));
    	if(result){
    		newAd.JobXML__c = JxtNZUtils.getFeed(newAd);
	        newAd.Status__c = 'Active';
	        newAd.Job_Posting_Status__c = 'In Queue';
            if(!Test.isRunningTest()){
                checkFLS();
            }
            update newAd;
    	}else{
    		//If insert DB failed, set status is Not Valid. Add by Jack On 10/09/2014
    		newAd.JobXML__c = JxtNZUtils.getFeed(newAd);
    		newAd.Status__c = 'Not Valid';
    		newAd.Job_Posting_Status__c = 'Insert/Update URL Failed';
            checkFLS();  
            update newAd;
    		//End to deal the insert DB failed issue
    		
    		CareerOneDetail.notificationsendout('orgid='+Userinfo.getOrganizationId()+' userid='+UserInfo.getUserId()+' jxt cannot post detail information to EC2 database', 'ec2 callout', 'jxtnzpost', 'feedXml()');
    		return Page.peoplecloudErrorPage;
    	} 
    	
        return null;
    }
    
    public PageReference saveAd(){
        newAd = ad.clone(false, true);
       
        String[] ss = newAd.JXTNZ_Location__c.split(',');
        /*
        newAd.Location__c = ss[3].trim();
        newAd.SearchArea_Label_String__c=ss[2].trim();
        */ 
        newAd.suburb__c = ss[0].trim();
        newAd.zipcode__c = ss[3].trim();
        newAd.Country__c = ss[2].trim();
        
        newAd.JXT_Short_Description__c = short_description_contentTemp;
        newAd.Job_Content__c = jobContentTemp;
       	newAd.RecordTypeId = JobBoardUtils.getRecordId('JXT_NZ');
        newAd.WebSite__c = 'JXT_NZ';
       
        //insert skill group ext id
	    if(enableskillparsing){
	    	String tempString  = String.valueOf(defaultskillgroups);
	        tempString = tempString.replaceAll('\\(', '');
	        tempString = tempString.replaceAll('\\)', '');
	        tempString = tempString.replace(' ','');
	        newAd.Skill_Group_Criteria__c = tempString;
	    }
	    //end insert skill group ext id
        if(!Test.isRunningTest()){
            checkFLS();
        }
        
        insert newAd;
        return null;
    }
    
    public List<SelectOption> getReferralItems() {
		return JxtNZUtils.getReferralItems();
	}
	public List<SelectOption> getWriteLabels() {
		return JxtNZUtils.getWriteLabels();		
	}
	
	//insert the skill group picklist value option
    public List<SelectOption> getSelectSkillGroupList(){
	  	List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
	  	List<SelectOption> displaySelectOption = new List<SelectOption>();
	  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
	  		displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
	  	}
	  	return displaySelectOption;
	}
	
	public String getCompanyName(){
		return UserInfo.getOrganizationName();
	}
    
    private void checkFLS(){
      List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Advertisement__c.Application_URL__c,
            Advertisement__c.ListingID__c,
            Advertisement__c.Ad_Closing_Date__c,
            Advertisement__c.Placement_Date__c,
            Advertisement__c.Advertisement_Account__c,
            Advertisement__c.Indeed_Question_URL__c,
            Advertisement__c.Vacancy__c,
            Advertisement__c.Skill_Group_Criteria__c,
            Advertisement__c.Job_Type__c,
            Advertisement__c.Job_Title__c,
            Advertisement__c.JXT_Short_Description__c,
            Advertisement__c.Bullet_1__c,
            Advertisement__c.Bullet_2__c,
            Advertisement__c.Bullet_3__c,
            Advertisement__c.Job_Content__c,
            Advertisement__c.Job_Contact_Name__c,
            Advertisement__c.Company_Name__c,
            Advertisement__c.Nearest_Transport__c,
            Advertisement__c.Residency_Required__c,
            Advertisement__c.IsQualificationsRecognised__c,
            Advertisement__c.Location_Hide__c,
            Advertisement__c.TemplateCode__c,
            Advertisement__c.AdvertiserJobTemplateLogoCode__c,
            Advertisement__c.ClassificationCode__c,
            Advertisement__c.SubClassificationCode__c,
            Advertisement__c.Classification2Code__c,
            Advertisement__c.SubClassification2Code__c,
            Advertisement__c.WorkTypeCode__c,
            Advertisement__c.SectorCode__c,
            Advertisement__c.Street_Adress__c,
            Advertisement__c.Search_Tags__c,
            Advertisement__c.CountryCode__c,
            Advertisement__c.LocationCode__c,
            Advertisement__c.AreaCode__c,
            Advertisement__c.Salary_Type__c,
            Advertisement__c.SeekSalaryMin__c,
            Advertisement__c.SeekSalaryMax__c,
            Advertisement__c.Salary_description__c,
            Advertisement__c.No_salary_information__c,
            Advertisement__c.Has_Referral_Fee__c,
            Advertisement__c.Referral_Fee__c,
            Advertisement__c.Terms_And_Conditions_Url__c,
            Advertisement__c.Status__c,
            Advertisement__c.Job_Posting_Status__c,
            Advertisement__c.JobXML__c,
            Advertisement__c.Prefer_Email_Address__c,
            Advertisement__c.RecordTypeId,
            Advertisement__c.Website__C
      };

      fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
      fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    } 

}