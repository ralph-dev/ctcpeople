/**
    Author: Raymond
    A helper class used to store all utility functions.
    This class should not be instancilized and all functions should be static.
*/

public with sharing class PeopleCloudHelper {
    
    public static final String PAGE_PARAM_RETURL = 'retURL';
    public static final String PAGE_PARAM_LIGHTNING_RETURL = 'vfRetURLInSFX';
    public static final String PAGE_PARAM_ID = 'id';
    public static final String PAGE_PARAM_LINK_ID_SUFFIX='_lkid';
	
	/**
     * The number of bytes in a kilobyte.
     */
    public static final Long ONE_KB = 1024;

    /**
     * The number of bytes in a megabyte.
     */
    public static final Long ONE_MB = ONE_KB * ONE_KB;


    /**
     * The number of bytes in a gigabyte.
     */
    public static final Long ONE_GB = ONE_KB * ONE_MB;

    /**
     * The number of bytes in a terabyte.
     */
    public static final Long ONE_TB = ONE_KB * ONE_GB;
    
	private static RecordType candRT=DaoRecordType.indCandidateRT;
	private static RecordType cmRT = DaoRecordType.getRecordTypesByObjectType(PeopleCloudHelper.getPackageNamespace()+'Placement_Candidate__c'); //[select Id from RecordType where SobjectType =: PeopleCloudHelper.getPackageNamespace()+'Placement_Candidate__c' limit 1];
	private static RecordType vacRT = DaoRecordType.getRecordTypesByObjectType(PeopleCloudHelper.getPackageNamespace()+'Placement__c' ); //[select Id from RecordType where SobjectType =: PeopleCloudHelper.getPackageNamespace()+'Placement__c' limit 1];
	private static Integer randNumber4CurrSession = (Integer)Math.ceil(Math.random()*1000000);
	private static RecordType candPoolRT{
		get{
			if(candPoolRT == null){
				candPoolRT = DaoRecordType.candidatePoolRT;
			}
			return candPoolRT;
		}
	}
	
	private static Profile standardPlatformProfile{
		get{
			if(standardPlatformProfile==null){
				//standardPlatformProfile = [select Id, Name from Profile where UserType='Standard' and Name='Standard Platform User' limit 1];
				CommonSelector.checkRead(Profile.sObjectType, 'Id, Name');
				standardPlatformProfile = [select Id, Name from Profile where UserType='Standard' and Name='ctcPeople Tes Code' limit 1];
			}
			return standardPlatformProfile;
		}
	}
	
	public static Folder ctcAdminFolder{
		get{
			if(ctcAdminFolder == null){
				ctcAdminFolder = DaoFolder.getFolderByDevName('CTC_Admin_Folder');
			}
			return ctcAdminFolder;
		}
	
	}
	
	public static Document triggerAdminConfigFile{
		get{
			if(triggerAdminConfigFile == null){
				triggerAdminConfigFile = DaoDocument.getCTCConfigFileByDevName('Trigger_Admin');
			}
			return triggerAdminConfigFile;
		}
	}
	
	public class PeopleCloudTestDataSet{
		public List<Account> companies;
		public List<Placement__c> vacancies;
		public List<Skill_Group__c> skillGroups;
		public List<Skill__c> skills;
		public List<Employment_History__c> employmentHistory;
		public List<Advertisement__c>  advertisements;
		public List<Contact> candidates;
		public List<Placement_Candidate__c> candidateManagements;
		public List<Candidate_Skill__c> candidateSkills;
		
		public PeopleCloudTestDataSet(){
			companies = new List<Account>();
			vacancies = new List<Placement__c>();
			skillGroups = new List<Skill_Group__c>();
			skills = new List<Skill__c>();
			employmentHistory = new List<Employment_History__c>();
			advertisements = new List<Advertisement__c>();
			candidates = new List<Contact>();
			candidateManagements = new List<Placement_Candidate__c>();
			candidateSkills = new List<Candidate_Skill__c>();
		}	
	}
	
	public class Application{
		public Contact candidate;
		public Placement_Candidate__c candidateManagement;
		public List<Web_Document__c> resumes;
		public List<Candidate_Skill__c> candidateSkills;
		public List<Placement_Candidate__c> candidateManagements;
		public List<Employment_History__c> employmentHistory;
		
		public Application(){
			resumes = new List<Web_Document__c>();
			candidateSkills = new List<Candidate_Skill__c>();
			candidateManagements = new List<Placement_Candidate__c>();
			employmentHistory = new List<Employment_History__c>();
		}
	
	}

	public class ApplicationList{
		public List<Application> applications;
		public List<Contact> candidates;
		public List<Placement_Candidate__c> candidateManagements;
		public List<Candidate_Skill__c> candidateSkills;
		public List<Employment_History__c> employmentHistory;
		
		public List<Web_Document__c> resumes;
		
		public ApplicationList(){
			applications = new List<Application>();
			candidates = new List<Contact>();
			candidateManagements = new List<Placement_Candidate__c>();
			resumes = new List<Web_Document__c>();
			candidateSkills = new List<Candidate_Skill__c>();
			employmentHistory=new List<Employment_History__c>();			
		}
		
		public Map<Id,Application> getApplicationMap(){
			Map<Id,Application> appMap = new Map<Id,Application>();
			for(Application app:applications){
				if(app.candidate.Id !=null)
					appMap.put(app.candidate.Id,app);
				else
					system.debug('Error: Id equals to null, won\'t add to Map');
			}
			return appMap;
		}
	
	}
	    
	  /*
	    This method will return the namespace of recent environment.
	  */
	  public static String getPackageNamespace(){
	    String placementName=Schema.SObjectType.Placement__c.getName();
	    String namespace ='';
	    //system.debug('Placement__c name:'+placementName);
	    if(placementName.indexOf('__') != placementName.lastIndexOf('__')){
              namespace = placementName.split('__')[0]+'__';
        }
	    //if(placementName.contains('PeopleCloud1__'))
	    //  namespace='PeopleCloud1__';
	    return namespace;
	  }
	
	//Depreca
	/*public static S3Credential getS3CredentialFromDocument(Document setting){
	      S3Credential credential = new S3Credential(null,null);
	      if(setting!=null && setting.Body!=null){
	          Dom.Document dom = new Dom.Document();
	          dom.load(setting.Body.toString());
	          Dom.XmlNode rootNode=dom.getRootElement();
	          if(rootNode!=null && rootNode.getName()=='fileroot' && rootNode.getChildElement('credential',null)!=null){
	              credential.accessKey = rootNode.getChildElement('credential',null).getChildElement('key',null).getText();
	              credential.secretKey = rootNode.getChildElement('credential',null).getChildElement('secret',null).getText();
	          }
	      }
	      return credential;
	  }*/

	  public static S3Credential getS3CredentialFromCustomSetting(){
	  	  CustomSettingSelector cSettingSelector = new CustomSettingSelector();
	  	  S3Credential__c S3Credential = cSettingSelector.getOrgS3Credential();

	      S3Credential credential = new S3Credential(null,null);
	      if(S3Credential!=null){
	      	credential.accessKey = S3Credential.Access_Key_ID__c;
	        credential.secretKey = s3Credential.Secret_Access_Key__c;
	      }
	      return credential;
	  }
	  
	  public static Document getStdS3CredentialSettingDocument(){
	      return DaoDocument.getCTCConfigFileByDevName('S3Account');
	  }
	    
	 public static testmethod void testGetPackageNamespace(){
	  	String placementName=Schema.SObjectType.Placement_Candidate__c.getName();
	  	String namespace = PeopleCloudHelper.getPackageNamespace();
	  	if(namespace == ''){
	  		system.assertEquals(true,placementName.indexOf('__') == placementName.lastIndexOf('__'));
	  	}else{
	  		system.assertEquals(true,placementName.startsWith(namespace));
	  	}
	  	
	   
	  }
  
	
	public static PeopleCloudTestDataSet prepareData(){
		Set<String> selectedskillgourplist = new Set<String>();
		Map<Id,Set<String>> selectedskillgourpmap = new Map<Id,Set<String>>();
		PeopleCloudTestDataSet dataSet = new PeopleCloudTestDataSet();
		
		//insert companies
        dataSet.companies.add(new Account(Name='comp1'));
        dataSet.companies.add(new Account(Name='comp2'));
        
        CommonSelector.quickInsert( dataSet.companies);
        
        //insert vacancies
        Placement__c p1=new Placement__c(RecordTypeId= vacRT.Id,Company__c=dataSet.companies.get(0).Id,Name='placement 1');
        Placement__c p2=new Placement__c(RecordTypeId= vacRT.Id,Company__c=dataSet.companies.get(1).Id,Name='placement 2');
        dataSet.vacancies.add(p1);
        dataSet.vacancies.add(p2);
        CommonSelector.quickInsert( dataSet.vacancies);
        
        // Insert Advertisements
        Advertisement__c ad1 = new Advertisement__c(Job_Title__c='ad 1',RecordTypeId=DaoRecordType.seekAdRT.Id,Vacancy__c=p1.Id,WebSite__c='Seek');
        Advertisement__c ad2 = new Advertisement__c(Job_Title__c='ad 2',RecordTypeId=DaoRecordType.seekAdRT.Id,Vacancy__c=p2.Id,WebSite__c='Seek');
        dataSet.advertisements.add(ad1);
        dataSet.advertisements.add(ad2);
        CommonSelector.quickInsert( dataSet.advertisements);
		
		// Create candidates
		for(Integer i=0;i<201;i++){
			Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',email='Cand'+i+'@test.ctc.test', Candidate_From_Web__c=true,jobApplication_Id__c=randNumber4CurrSession+1,Resume_Source__c='abc'+i);
			dataSet.candidates.add(cand1);
		}
		CommonSelector.quickInsert( dataSet.candidates);
		
		dataSet.candidateManagements = new List<Placement_Candidate__c>();
        for(Contact con:dataSet.candidates){
        	Placement_Candidate__c cm1=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=con.Id,Online_Ad__c=ad1.Id);
        	dataSet.candidateManagements.add(cm1);
        }
        
        for(Integer i=0;i<5;i++){
        	Skill_Group__c skillGroup = new Skill_Group__c(Name='SG'+i,Default__c=true,Enabled__c=true,Skill_Group_External_Id__c='CTCTestDeduplicationGroup'+i);
        	dataSet.skillGroups.add(skillGroup);
        }
        CommonSelector.quickInsert( dataSet.skillGroups);
        
        for(Skill_Group__c sg:dataSet.skillGroups){
        	for(Integer i=0;i<10;i++){
        		Skill__c skill = new Skill__c(Name='skill'+i,Skill_Group__c=sg.Id,Ext_Id__c=''+sg.Skill_Group_External_Id__c+i );
        		//system.debug('ext_id'+skill.Ext_Id__c);
        		dataSet.skills.add(skill);
        	}
        }
        CommonSelector.quickInsert( dataSet.skills);
        
        for(Contact con:dataSet.candidates){
        	selectedskillgourplist = new Set<String>();
        	for(Integer i=0;i<5;i++){
        		Integer randomNumber = Math.mod((Integer)Math.ceil(Math.random()*10),10);
	        	Candidate_Skill__c cs = new Candidate_Skill__c(Candidate__c=con.Id,Skill__c = dataSet.skills.get(randomNumber).Id);
	        	selectedskillgourplist.add(dataSet.skills.get(randomNumber).Ext_Id__c);
    	    	dataSet.candidateSkills.add(cs);
        	}
        	selectedskillgourpmap.put(con.Id,selectedskillgourplist);//add by jack
        }
        CommonSelector.quickInsert( dataSet.candidateSkills);
        
        for(Contact contact:dataSet.candidates){
        	Employment_History__c eh = new Employment_History__c(Contact__c = contact.Id,Title__c = 'History '+contact.Id);
        	dataSet.employmentHistory.add(eh);
        }
        CommonSelector.quickInsert( dataSet.employmentHistory);
        
        if(selectedskillgourpmap.size()>0 && selectedskillgourpmap!=null){
        	List<Contact> updateListContact = new List<Contact>();
        	List<Contact> completeupdateListContact = new List<Contact>();
        	Set<Id> idString = new Set<Id>();
        	idString = selectedskillgourpmap.keySet();
        	CommonSelector.checkRead(Contact.sObjectType,'id, Skill_Group__c');
        	updateListContact = [select id, Skill_Group__c from Contact where id in: idString];
        	for(Contact tempcontact: updateListContact){
        		Set<String> CandidateSkillgrouplist = selectedskillgourpmap.get(tempcontact.id);
        		String tempString = '';
        		tempString = String.valueOf(CandidateSkillgrouplist);
			    tempString = tempString.replaceAll('\\{', '');
			    tempString = tempString.replaceAll('\\}', '');
			    tempString = tempString.replace(' ','');
			    tempcontact.Skill_Group__c = tempString;
			    completeupdateListContact.add(tempcontact);
        	}
        	CommonSelector.quickUpdate( completeupdateListContact);
        }
        CommonSelector.checkRead(Contact.sObjectType,'RecordTypeId ,LastName, email , Candidate_From_Web__c ,jobApplication_Id__c ,Resume_Source__c, Skill_Group__c');
        dataSet.candidates = [select RecordTypeId ,LastName, email , Candidate_From_Web__c ,jobApplication_Id__c ,Resume_Source__c, Skill_Group__c from Contact where id in: dataSet.candidates];
        // Prepare Candidate Management data, no deduplication happen
        CommonSelector.quickInsert( dataSet.candidateManagements);
        return dataSet;
	}
	/**
		Generate a random candidate record and link it to given advertisement to simulate
		job application process. Web Document, Contact, Candidate Management records will be generated
		and inserted into database;
		
		By giving a map of field name and field value, programmer can control what values should be
		set for this candidate.
		
	**/
	public static Application candidateApplyJob(Advertisement__c ad,Map<String,Object> values, List<Skill__c> skills,Boolean enabledInsertEmploymentHistory){
		Map<Id,Skill__c> addedSkills = new Map<Id,Skill__c>();
		Double currMaxJobAppId = getMaxJobApplicationId();
		Application application = new Application();
		Integer randomNumber = (Integer)Math.ceil(Math.random()*1000000);
		application.candidate = new Contact(RecordTypeId=candRT.Id,LastName='candidate'+randomNumber,email='Cand'+randomNumber+'@test.ctc.test', Candidate_From_Web__c=true,jobApplication_Id__c=currMaxJobAppId+1,
		Resume_Source__c='abc'+randomNumber, Resume__c = 'abc'+randomNumber, Resume_Link__c = 'abc'+randomNumber);
		
		for(String key:values.keySet()){
			application.candidate.put(key, values.get(key));
		}
		CommonSelector.quickInsert( application.candidate);
		Web_Document__c wd1=new Web_Document__c(Name='wd1'+randomNumber,Document_Related_To__c=application.candidate.Id);
        Web_Document__c wd2=new Web_Document__c(Name='wd2'+randomNumber,Document_Related_To__c=application.candidate.Id);
        application.resumes.add(wd1);
        application.resumes.add(wd2);
        CommonSelector.quickInsert( application.resumes);
        
        if(skills!=null && skills.size()>0){
	        for(Integer i=0;i<5;i++){
        		Integer rdNumber;
        		Skill__c skill;	        		
        		// Find non-duplicate candidates
        		do{
        			rdNumber = Math.mod((Integer)Math.ceil(Math.random()*skills.size()),skills.size());
        			skill = skills.get(rdNumber);
        		}while(addedSkills.get(skill.Id)!=null);
        		addedSkills.put(skill.Id,skill);
	        	Candidate_Skill__c cs = new Candidate_Skill__c(Candidate__c=application.candidate.Id,Skill__c = skill.Id);
		    	application.candidateSkills.add(cs);
	    	}
	    	CommonSelector.quickInsert( application.candidateSkills);
        }
        
        if(enabledInsertEmploymentHistory){
	        for(Integer i=0;i<3;i++){
	        	Employment_History__c eh = new Employment_History__c(Contact__c = application.candidate.Id,Title__c = 'History '+application.candidate.Id);
	        	application.employmentHistory.add(eh);
	        }
	        CommonSelector.quickInsert( application.employmentHistory);
        }
        
		application.candidateManagement=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=ad.Vacancy__c,Candidate__c=application.candidate.Id,Online_Ad__c=ad.Id, Status__c='New');
		CommonSelector.quickInsert( application.candidateManagement);
		return application;
	}
	
	/*
		Simulate scenario where a candidate applys a job.	
	
	*/
	public static ApplicationList candidatesApplyJob(Integer noOfCandidates, Advertisement__c ad, Map<String,Object> values,  List<Skill__c> skills,Boolean enabledInsertEmploymentHistory){
		List<Contact> updateCandidatelist = new List<Contact>();//add by jack
		Double currMaxJobAppId = getMaxJobApplicationId();
		ApplicationList applicationList = new ApplicationList();
		CommonSelector.checkRead(Skill__c.sObjectType,'id, Name, Skill_Group__c, Skill_Group__r.Skill_Group_External_Id__c ' );
		skills = [select id, Name, Skill_Group__c, Skill_Group__r.Skill_Group_External_Id__c from Skill__c where id in: skills];
		for(Integer i=0;i<noOfCandidates;i++){
			Integer randomNumber = (Integer)Math.ceil(Math.random()*1000000);
			Application application = new Application();
			application.candidate = new Contact(RecordTypeId=candRT.Id,LastName='candidate'+randomNumber,email='Cand'+randomNumber+'@test.ctc.test', Candidate_From_Web__c=true,jobApplication_Id__c=currMaxJobAppId+i+1,
				Resume_Source__c='abc'+randomNumber, Resume__c = 'abc'+randomNumber, Resume_Link__c = 'abc'+randomNumber);
			for(String key:values.keySet()){
				application.candidate.put(key,values.get(key));
			}
			applicationList.applications.add(application);
			applicationList.candidates.add(application.candidate);
		}
		CommonSelector.quickInsert( applicationList.candidates);
		for(Application app: applicationList.applications){
			Map<Id,Skill__c> addedSkills = new Map<Id,Skill__c>();
			set<String> skillGroupSet = new set<String>();//add by Jack			
			Integer randomNumber = (Integer)Math.ceil(Math.random()*1000000);
			Web_Document__c wd1=new Web_Document__c(Name='wd1'+randomNumber,Document_Related_To__c=app.candidate.Id);
        	Web_Document__c wd2=new Web_Document__c(Name='wd2'+randomNumber,Document_Related_To__c=app.candidate.Id);
        	app.resumes.add(wd1);
        	app.resumes.add(wd2);
        	applicationList.resumes.add(wd1);
        	applicationList.resumes.add(wd2);
        	app.candidateManagement=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=ad.Vacancy__c,Candidate__c=app.candidate.Id,Online_Ad__c=ad.Id, Status__c='New');
        	applicationList.candidateManagements.add(app.candidateManagement);
        	
        	if(skills!=null && skills.size()>0){
	        	for(Integer i=0;i<5;i++){
	        		Integer rdNumber;
	        		Skill__c skill;
	        		// Find non-duplicate candidates
	        		do{
	        			rdNumber = Math.mod((Integer)Math.ceil(Math.random()*skills.size()),skills.size());
	        			skill = skills.get(rdNumber);
	        		}while(addedSkills.get(skill.Id)!=null);
	        		addedSkills.put(skill.Id,skill);
	        		skillGroupSet.add(skill.Skill_Group__r.Skill_Group_External_Id__c);
	    			
	        		Candidate_Skill__c cs = new Candidate_Skill__c(Candidate__c=app.candidate.Id,Skill__c = skill.Id);
		    		app.candidateSkills.add(cs);
		    		applicationList.candidateSkills.add(cs);
	    		}
        	}
        	
        	
    		if(skillGroupSet!=null && skillGroupSet.size()>0){
    			String skillgroupsetString = '';
    			skillgroupsetString = String.valueOf(skillGroupSet);
			    skillgroupsetString = skillgroupsetString.replaceAll('\\{', '');
			    skillgroupsetString = skillgroupsetString.replaceAll('\\}', '');
			    skillgroupsetString = skillgroupsetString.replace(' ','');
			    app.candidate.Skill_Group__c = skillgroupsetString;
			    updateCandidatelist.add(app.candidate);
    		}
    		
    		if(enabledInsertEmploymentHistory){
	        	for(Integer i=0;i<3;i++){
	        		Employment_History__c eh = new Employment_History__c(Contact__c = app.candidate.Id,Title__c = 'History '+app.candidate.Id);
	        		app.employmentHistory.add(eh);
	        		applicationList.employmentHistory.add(eh);
	        	}
    		}
		}
		CommonSelector.quickInsert( applicationList.resumes);
		if(skills!=null && skills.size()>0)
			CommonSelector.quickInsert( applicationList.candidateSkills);
		if(enabledInsertEmploymentHistory)
			CommonSelector.quickInsert( applicationList.employmentHistory);  
		if(updateCandidatelist!=null && updateCandidatelist.size()>0)
			CommonSelector.quickUpdate( updateCandidatelist);
		
		CommonSelector.checkRead(Contact.sObjectType,'RecordTypeId ,LastName, email , Candidate_From_Web__c ,jobApplication_Id__c ,Resume_Source__c, Skill_Group__c');
		applicationList.candidates = [select RecordTypeId ,LastName, email , Candidate_From_Web__c ,jobApplication_Id__c ,Resume_Source__c, Skill_Group__c from Contact where id in: applicationList.candidates];
        CommonSelector.quickInsert( applicationList.candidateManagements);
		return applicationList;
	}

	/*
		Simulate scenario where multiple duplicate candidates apply a job.	
	
	*/

	public static ApplicationList duplicateCandidatesApplyJob(Integer noOfCandidates, Application applicantToClone, Advertisement__c ad, Map<String,Object> values,  List<Skill__c> skills,Boolean enabledInsertEmploymentHistory, Boolean withDuplicateSkills){
		List<Contact> updateCandidatelist = new List<Contact>();//add by jack
		Double currMaxJobAppId = getMaxJobApplicationId();
		ApplicationList applicationList = new ApplicationList();
		CommonSelector.checkRead(Skill__c.sObjectType,'id, Name, Skill_Group__c, Skill_Group__r.Skill_Group_External_Id__c');
		skills = [select id, Name, Skill_Group__c, Skill_Group__r.Skill_Group_External_Id__c from Skill__c where id in: skills];
		Integer randomNumber = (Integer)Math.ceil(Math.random()*1000000);
		for(Integer i=0;i<noOfCandidates;i++){
			Application application = new Application();
			application.candidate = new Contact(RecordTypeId=candRT.Id,LastName=applicantToClone.candidate.LastName,email=applicantToClone.candidate.email, Candidate_From_Web__c=true,jobApplication_Id__c=currMaxJobAppId+i+1,Resume_Source__c='abc'+randomNumber);
			for(String key:values.keySet()){
				application.candidate.put(key,values.get(key));
			}
			applicationList.applications.add(application);
			applicationList.candidates.add(application.candidate);
		}
		CommonSelector.quickInsert( applicationList.candidates);
		
		for(Application app: applicationList.applications){
			Map<Id,Skill__c> addedSkills = new Map<Id,Skill__c>();
			set<String> skillGroupSet = new set<String>();//add by Jack
			Web_Document__c wd1=new Web_Document__c(Name='wd'+randomNumber,Document_Related_To__c=app.candidate.Id);
        	Web_Document__c wd2=new Web_Document__c(Name='wd'+randomNumber,Document_Related_To__c=app.candidate.Id);
        	app.resumes.add(wd1);
        	app.resumes.add(wd2);
        	applicationList.resumes.add(wd1);
        	applicationList.resumes.add(wd2);
        	app.candidateManagement=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=ad.Vacancy__c,Candidate__c=app.candidate.Id,Online_Ad__c=ad.Id, Status__c='New');
        	applicationList.candidateManagements.add(app.candidateManagement);
        	
    		// if enable creating duplicate skills, then skill up new candidate using same set of skill that applicantToClone has .
    		if(withDuplicateSkills){
    			for(Candidate_Skill__c skill: applicantToClone.candidateSkills){
	        		Candidate_Skill__c cs = new Candidate_Skill__c(Candidate__c=app.candidate.Id,Skill__c = skill.Skill__c);
		    		app.candidateSkills.add(cs);
		    		applicationList.candidateSkills.add(cs);
	    		}
    		}else if(skills!=null && skills.size()>0){
        		    			
    			// Otherwise, randomly generate skills for them.
	        	for(Integer i=0;i<5;i++){
	        		Integer rdNumber;
	        		Skill__c skill;	        		
	        		// Find non-duplicate candidates
	        		do{
	        			rdNumber = Math.mod((Integer)Math.ceil(Math.random()*skills.size()),skills.size());
	        			skill = skills.get(rdNumber);
	        		}while(addedSkills.get(skill.Id)!=null);
	        		addedSkills.put(skill.Id,skill);	   	        		
	        		skillGroupSet.add(skill.Skill_Group__r.Skill_Group_External_Id__c);  //add by Jack   		
	        		Candidate_Skill__c cs = new Candidate_Skill__c(Candidate__c=app.candidate.Id,Skill__c = skill.Id);
		    		app.candidateSkills.add(cs);
		    		applicationList.candidateSkills.add(cs);
	    		}
    		}
    		
    		if(skillGroupSet!=null && skillGroupSet.size()>0){
    			String skillgroupsetString = '';
    			skillgroupsetString = String.valueOf(skillGroupSet);
			    skillgroupsetString = skillgroupsetString.replaceAll('\\{', '');
			    skillgroupsetString = skillgroupsetString.replaceAll('\\}', '');
			    skillgroupsetString = skillgroupsetString.replace(' ','');
			    app.candidate.Skill_Group__c = skillgroupsetString;
			    updateCandidatelist.add(app.candidate);
    		}
    		
    		if(enabledInsertEmploymentHistory){
	        	for(Integer i=0;i<3;i++){
	        		Employment_History__c eh = new Employment_History__c(Contact__c = app.candidate.Id,Title__c = 'History '+app.candidate.Id);
	        		app.employmentHistory.add(eh);
	        		applicationList.employmentHistory.add(eh);
	        	}
    		}
		}
		CommonSelector.quickInsert( applicationList.resumes);				
		if(skills!=null && skills.size()>0)
			CommonSelector.quickInsert( applicationList.candidateSkills);
		if(enabledInsertEmploymentHistory)
			CommonSelector.quickInsert( applicationList.employmentHistory);  
		if(updateCandidatelist!=null && updateCandidatelist.size()>0)
			CommonSelector.quickUpdate( updateCandidatelist);  
			
		CommonSelector.checkRead(Contact.sObjectType, 'RecordTypeId ,LastName, email , Candidate_From_Web__c ,jobApplication_Id__c ,Resume_Source__c, Skill_Group__c');
		applicationList.candidates = [select RecordTypeId ,LastName, email , Candidate_From_Web__c ,jobApplication_Id__c ,Resume_Source__c, Skill_Group__c from Contact where id in: applicationList.candidates];
        CommonSelector.quickInsert( applicationList.candidateManagements);
		return applicationList;	
	}
	
	public static Double getMaxJobApplicationId(){
		List<Contact> candidates = null;
		try{
			CommonSelector.checkRead(Contact.sObjectType, 'Id, jobApplication_Id__c');
			candidates = [select Id, jobApplication_Id__c from Contact where jobApplication_Id__c >0 order by jobApplication_Id__c DESC limit 10];
			//system.debug('candidates ='+ candidates);
		}catch(Exception e){
		}
		if(candidates == null)
			return -1;
		else
			return candidates.get(0).jobApplication_Id__c;
		
	}
	
	public static Account createCandidatePool(){
		Integer randNum4CurrentInvokation = (Integer)Math.ceil(Math.random()*1000000);
		Account candidatePool = new Account(name='candidate pool'+randNum4CurrentInvokation,RecordTypeId = candPoolRT.Id);
		CommonSelector.quickInsert( candidatePool);
		return candidatePool;		
	}
	
	public static User createPlatformUser(){
		Integer randNum4CurrentInvokation = (Integer)Math.ceil(Math.random()*1000000);
		User ia1 = new User(Username='Peoplecloud_'+randNum4CurrentInvokation+'@clicktocloud.com',LastName='ln',Email='peoplecloud1_'+randNum4CurrentInvokation+'@test.com',Alias='pc1A1',CommunityNickname='peoplecloud1_'+randNum4CurrentInvokation,TimeZoneSidKey='Australia/Sydney',LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',ProfileId=standardPlatformProfile.id,LanguageLocaleKey='en_US');
		ia1.AmazonS3_Folder__c = 'Test';
		ia1.Job_Posting_Company_Name__c = 'CTC';
		ia1.JobBoards__c = 'mycareer.com;seek.com;careerone.com;trademe.com;jxt;jobx.co.nz';
		ia1.Monthly_Quota_CareerOne__c = '100';
		ia1.Monthly_Quota_MyCareer__c = '100';
		ia1.Monthly_Quota_Seek__c = '100';
		ia1.MyCareer_Usage__c = '0';
		ia1.Seek_Usage__c = '0';
		ia1.CareerOne_Usage__c = '0';
		ia1.Seek_Account__c = 'ididididid';
		CommonSelector.quickInsert( ia1);
		return ia1;
	}
	
	
	public static void configureSetting(){
		// Config PCAppSwitch custom setting
		PCAppSwitch__c pcSwitch= PCAppSwitch__c.getOrgDefaults();
    	pcSwitch.Deduplicate_PC_Active__c=true;
    	pcSwitch.DeduplicationOnVCW_Active__c=true;
    	pcSwitch.WebDocumentTrigger_Active__c = true;
		upsert pcSwitch;
		
		// Config triggerAdmin file which is setting for candidate pool bucketing function
		String triggerAdminDocContent = '<?xml version="1.0"?><!--file body starts here--><fileroot><trigger><apiname>UpdateAccountField</apiname><targetObject>Contact</targetObject><status>InActive</status><accountid>00190000002KiLz</accountid><recordtypeid>'+candRT.Id+'</recordtypeid><note><![CDATA[This trigger is used to enable automation of updating client/account of people.<br/> If a person has not a client related to such as independent candidate, this trigger will automatically assign a client to it. ]]></note></trigger><trigger><apiname>UpdateRemining</apiname><targetObject>Placement_Candidate__c</targetObject><status>Active</status><accountid></accountid><recordtypeid></recordtypeid><note><![CDATA[This trigger is used to enable a reminder which pops up an alert once a candidate placed. ]]></note></trigger></fileroot>';
		Blob triggerAdminDocContentBlob = Blob.valueof(triggerAdminDocContent);
		Document triggerAdminSetting = new Document();
		triggerAdminSetting.body= Blob.valueof(EncodingUtil.base64Encode(triggerAdminDocContentBlob));
		triggerAdminSetting.DeveloperName = 'Trigger_Admin';
		triggerAdminSetting.FolderId = UserInfo.getUserId();
		triggerAdminSetting.Name = 'TriggerAdminForTesting';
		triggerAdminSetting.type = 'text/xml';
		CommonSelector.quickUpsert( triggerAdminSetting);
	}
	
	//Check SMS Magic install this instance or not
	public static boolean smsfunctionavailable(){
		boolean result = false;
		boolean installsmsmageic = false;
		boolean checksms = false;
		
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		installsmsmageic = gd.keyset().contains(('smagicinteract__SenderId_Profile_Map__c').toLowerCase());
		
		PCAppSwitch__c pcSwitch= PCAppSwitch__c.getOrgDefaults();
		checksms = pcSwitch.Enable_Send_Bulk_SMS__c;
		if(Test.isRunningTest()) {
			result = true;
		}else {
			if(checksms & installsmsmageic){
				result = true;
			}
		}
		return result;
	}
	
	public static boolean bulkemilavailable(){
		boolean result = false;
		PCAppSwitch__c pcSwitch= PCAppSwitch__c.getOrgDefaults();
		result = pcSwitch.Enable_Bulk_Email__c;
		return result;
	}
	// Check if Add to Other vacancies button enabled ---Alvin
	public static boolean addToOtherVacanciesAvailable(){
		boolean result=true;
		PCAppSwitch__c pcSwitch= PCAppSwitch__c.getOrgDefaults();
		result=!pcSwitch.enableAddToOtherVacancies__c;
		return result;
	}
	//end
	
	//Check feature is available for this customer
	public static boolean featuresavailable(String feature){
		boolean result = false;
		String newfeatures = '';
		List<String> featurelist = new List<String>();
		Set<String> featureset = new Set<String>();
		PCAppSwitch__c pcSwitch= PCAppSwitch__c.getOrgDefaults();
		newfeatures = pcSwitch.Features__c;
		if(newfeatures != null && newfeatures != ''){
			featurelist = newfeatures.split(';');
			featureset.addAll(featurelist);
			if(featureset.contains(feature)){
				result = true;
			}
		}
		return result;
	}
	
	public static Long convertSizeStrToLong(String sizeStr){
        if(sizeStr == null ) {
            return 0;
        }
        
        
        Decimal size = 0;
        sizeStr = sizeStr.toLowerCase();
        String sizeNum = sizeStr.replaceAll('[^\\.\\d]','');
       
        if(sizeNum == ''){
            sizeNum = '0';
        }
        size = Decimal.valueOf(sizeNum);
        
        if(sizeStr.contains('byte')){
            
        }else if(sizeStr.contains('kb') || sizeStr.contains('k')){
            size = size * ONE_KB;
        }else if(sizeStr.contains('mb') || sizeStr.contains('m')){
            size = size * ONE_MB;
        }
        else if(sizeStr.contains('gb') || sizeStr.contains('g')){
            size = size * ONE_GB;
        }else if(sizeStr.contains('tb') || sizeStr.contains('t')){
            size = size * ONE_TB;
        }
        
        return size.longValue();
    }
 
	
    public static String byteCountToDisplaySize(Long size) {
        String displaySize = '';
        
       if (size / ONE_TB > 0) {
            displaySize = String.valueOf(divide(size, ONE_TB))  + ' TB';
        } else if (size / ONE_GB > 0) {
            displaySize = String.valueOf(divide(size, ONE_GB)) + ' GB';
        } else if (size / ONE_MB > 0) {
            displaySize = String.valueOf(divide(size, ONE_MB)) + ' MB';
        } else if (size / ONE_KB > 0) {
        	
            displaySize = String.valueOf(divide(size,  ONE_KB)) + ' KB';
        } else {
            displaySize = String.valueOf(size) + ' bytes';
        }
        return displaySize;
    }
	
	private static Decimal divide(Long size ,Long divisor){
		
		return Decimal.valueOf( size).divide(Decimal.valueOf(divisor), 2, System.RoundingMode.CEILING);
		
	}
	
	public static testMethod void testByteCountToDisplaySize(){
	    String sizeStr = PeopleCloudHelper.byteCountToDisplaySize(523);
		System.assertEquals('523 bytes',sizeStr );
		System.assertEquals(523,convertSizeStrToLong(sizeStr));
		
		sizeStr = PeopleCloudHelper.byteCountToDisplaySize(1024);
		System.assertEquals('1.00 KB', sizeStr);
		System.assertEquals(1024,convertSizeStrToLong(sizeStr));
		
		sizeStr = PeopleCloudHelper.byteCountToDisplaySize(1536);
		System.assertEquals('1.50 KB', sizeStr);
		System.assertEquals(1536,convertSizeStrToLong(sizeStr));
		
		sizeStr = PeopleCloudHelper.byteCountToDisplaySize(1024 * 1024 + 512 * 1024);
		System.assertEquals('1.50 MB', sizeStr);
		System.assertEquals(1024 * 1024 + 512 * 1024,convertSizeStrToLong(sizeStr));
		
		sizeStr = PeopleCloudHelper.byteCountToDisplaySize(ONE_GB);
		System.assertEquals('1.00 GB', sizeStr);
		System.assertEquals(ONE_GB,convertSizeStrToLong(sizeStr));
		
		sizeStr = PeopleCloudHelper.byteCountToDisplaySize(ONE_TB);
		System.assertEquals('1.00 TB', PeopleCloudHelper.byteCountToDisplaySize(ONE_TB));
		System.assertEquals(ONE_TB,convertSizeStrToLong(sizeStr));
	}
	
	public static String getRetURLParam(PageReference pr){
	    if(pr == null )
	        return null;
	        
	    String retURL = pr.getParameters().get(PAGE_PARAM_RETURL);
	    System.debug(PAGE_PARAM_LIGHTNING_RETURL + ' : ' + retURL);
		
		if(retURL == null ){
		    retURL = pr.getParameters().get(PAGE_PARAM_LIGHTNING_RETURL);
		    System.debug(PAGE_PARAM_LIGHTNING_RETURL + ' : ' + retURL);
		   
		}
	
		
		retURL = secureUrl(retURL);
		System.debug( 'secured url : ' + retURL);
		
		return retURL;
	    
	}
	public static String getIdParam(PageReference pr){
	    if(pr == null )
	        return null;
	        
	    return pr.getParameters().get(PAGE_PARAM_ID);
		
	    
	}
	
	public static String getLinkedIdParam(PageReference pr){
	    for(String p : pr.getParameters().keySet()){
	        if(p.endsWith(PAGE_PARAM_LINK_ID_SUFFIX)){
	            return  pr.getParameters().get(p);
	        }
	    }
	    
	    return null;
	    
	}
	
	public static String makeRetURL(PageReference pr){
	    String ret = makeRetURLByLinkedIdParam(pr);
	    
	    if(ret == null){
	        ret = makeRetURLByIdParam(pr);
	    }
	    
        if(ret == null){
            ret = getRetURLParam(pr);
        }
        
	    
	    return ret;
	    
	}
	
	public static String makeRetURLByIdParam(PageReference pr){
	    if(pr == null)
	        return null;
	    String sid =pr.getParameters().get(PAGE_PARAM_ID);		
	    if(sid == null){
	        return null;
	    }
		String retURL = secureUrl('/'+sid);
		
	    
	    return retURl;
	}
	
	public static String makeRetURLByLinkedIdParam(PageReference pr){
	    if(pr == null)
	        return null;
	    String sid = getLinkedIdParam(pr); 		
	    if(sid == null){
	        return null;
	    }
		String retURL = secureUrl('/'+sid);
		
	    
	    return retURl;
	}
	
	public static String secureUrl(String url){
	    if(url == null)
	        return null;
	    if(url.startsWith('//')){
           url = url.replaceFirst('/{1,}','/');
        }
        
        return url;
	}
	public static Boolean isLightningExperience( ){
	    
	    return 'Theme4d'.equals(UserInfo.getUiTheme());
	}
}