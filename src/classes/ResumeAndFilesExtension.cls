public with sharing class ResumeAndFilesExtension {

	private final sObject mysObject;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public ResumeAndFilesExtension() {
    }

    public ResumeAndFilesExtension(Object obj){}

    @RemoteAction
    public static String getFieldSets(String srcObj) {
        CTCSettingUploadDocumentServices ctcDocumentService = new CTCSettingUploadDocumentServices();
        return JSON.serialize(ctcDocumentService.setupOptionsForDocTypes(srcObj));
    }

    @RemoteAction
    public static String getOptionList(String srcObj) {
        CTCSettingUploadDocumentServices ctcDocumentService = new CTCSettingUploadDocumentServices();
        ctcDocumentService.setupOptionsForDocTypes(srcObj);
        return JSON.serialize(ctcDocumentService.getTypeFieldMap());
    }

    @RemoteAction
    public static Map<String, RemoteAPIDescriptor.RemoteAction> apiDescriptor(){
        RemoteAPIDescriptor descriptor = new RemoteAPIDescriptor();
        descriptor.action('getFieldSets').param(String.class, 'Resume and Files fields List');
        descriptor.action('getOptionList').param(String.class, 'Get Type Field Map');
        return descriptor.paramTypesMap;
    }
}