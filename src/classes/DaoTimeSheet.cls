public class DaoTimeSheet extends CommonSelector{
	
	public DaoTimeSheet(){
		super(TimeSheet__c.sObjectType);
	}
	
	private static DaoTimeSheet dao = new DaoTimeSheet();
	static public List<TimeSheet__c> getPendingTSByVIds(Set<String> ids){
		dao.checkRead('Id,Backup_Approver__c,Approver_Contact__c,Approver__c,Vacancy__c,TimeSheet_Status__c');
		return [select Id,Backup_Approver__c,Approver_Contact__c,Approver__c,Vacancy__c,TimeSheet_Status__c from TimeSheet__c where Vacancy__c in :ids and (TimeSheet_Status__c='In Progress')];
	}
	
	static testmethod void testDaoTimeSheet(){
		System.runAs(DummyRecordCreator.platformUser) {

		Account comp = new Account(Name='abc');
		insert comp;
		
		List<Placement__c> vInsertList = new List<Placement__c>(); 
		Placement__c v1=new Placement__c(Company__c=comp.Id,Name='asdf');
		Placement__c v2=new Placement__c(Company__c=comp.Id,Name='asdf');
		vInsertList.add(v1);
		vInsertList.add(v2);
		insert vInsertList;
		
		Set<String> insertIds = new Set<String>();
		insertIds.add(v1.Id);
		insertIds.add(v2.Id);
		
		List<TimeSheet__c> tsList = DaoTimeSheet.getPendingTSByVIds(insertIds);
		system.assertEquals(tsList.size(), 0);
		
		TimeSheet__c ts1 = new TimeSheet__c(Vacancy__c=v1.Id,TimeSheet_Status__c='In Progress');
		TimeSheet__c ts2 = new TimeSheet__c(Vacancy__c=v1.Id,TimeSheet_Status__c='In Progress');
		TimeSheet__c ts3 = new TimeSheet__c(Vacancy__c=v1.Id,TimeSheet_Status__c='In Progress');
		TimeSheet__c ts4 = new TimeSheet__c(Vacancy__c=v1.Id,TimeSheet_Status__c='Submitted');
		List<TimeSheet__c> tsInsertList = new List<TimeSheet__c>();
		tsInsertList.add(ts1);
		tsInsertList.add(ts2);
		tsInsertList.add(ts3);
		tsInsertList.add(ts4);
		insert tsInsertList;
		
		tsList = DaoTimeSheet.getPendingTSByVIds(insertIds);
		system.assertEquals(tsList.size(), 3);
		}
	}
	
	static testmethod void testSyncTimeSheetApproverTrigger(){
		System.runAs(DummyRecordCreator.platformUser) {
		
		TriggerHelper.CandidatPlaced=true;
		TriggerHelper.UpdateReminingEnable=true;
		TriggerHelper.SyncTimeSheetApproverEnable=true;
		
		Profile p = [select Id, Name from Profile where Name='System Administrator' limit 1];
		Account comp = new Account(Name='abc');
		insert comp;
		
		//insert internal approver 'iaX'
		User ia1 = new User(Username='Peoplecloud_ia1@clicktocloud.com',LastName='ln',Email='peoplecloud1_a1@test.com',Alias='pc1A1',CommunityNickname='peoplecloud1_as1',TimeZoneSidKey='Australia/Sydney',LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',ProfileId=p.id,LanguageLocaleKey='en_US');
		User ia2 = new User(Username='Peoplecloud_ia2@clicktocloud.com',LastName='ln',Email='peoplecloud2_a2@test.com',Alias='pc2A2',CommunityNickname='peoplecloud1_as2',TimeZoneSidKey='Australia/Sydney',LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',ProfileId=p.id,LanguageLocaleKey='en_US');
		insert ia1;
		insert ia2;
		
		List<Placement__c> vInsertList = new List<Placement__c>();
		Map<String,Placement__c> vMap = new Map<String, Placement__c>(); 
		Placement__c v1=new Placement__c(Company__c=comp.Id,Name='asdf',Internal_Approver__c=ia1.Id);
		Placement__c v2=new Placement__c(Company__c=comp.Id,Name='asdf',Internal_Approver__c=ia2.Id);
		vInsertList.add(v1);
		vInsertList.add(v2);
		insert vInsertList;
		vMap.put(v1.Id,v1);
		vMap.put(v2.Id,v2);
		
		
		Set<String> insertIds = new Set<String>();
		insertIds.add(v1.Id);
		insertIds.add(v2.Id);
		
		// check there are no rescent pending timesheets
		List<TimeSheet__c> tsList = DaoTimeSheet.getPendingTSByVIds(insertIds);
		system.assertEquals(tsList.size(), 0);
		
		// insert timesheet
		TimeSheet__c ts1 = new TimeSheet__c(Vacancy__c=v1.Id,TimeSheet_Status__c='In Progress');
		TimeSheet__c ts2 = new TimeSheet__c(Vacancy__c=v1.Id,TimeSheet_Status__c='In Progress');
		TimeSheet__c ts3 = new TimeSheet__c(Vacancy__c=v1.Id,TimeSheet_Status__c='In Progress');
		TimeSheet__c ts4 = new TimeSheet__c(Vacancy__c=v1.Id,TimeSheet_Status__c='Approved');
		TimeSheet__c ts5 = new TimeSheet__c(Vacancy__c=v2.Id,TimeSheet_Status__c='In Progress');
		TimeSheet__c ts6 = new TimeSheet__c(Vacancy__c=v2.Id,TimeSheet_Status__c='In Progress');
		TimeSheet__c ts7 = new TimeSheet__c(Vacancy__c=v2.Id,TimeSheet_Status__c='Submitted');
		List<TimeSheet__c> tsInsertList = new List<TimeSheet__c>();
		tsInsertList.add(ts1);
		tsInsertList.add(ts2);
		tsInsertList.add(ts3);
		tsInsertList.add(ts4);
		tsInsertList.add(ts5);
		tsInsertList.add(ts6);
		tsInsertList.add(ts7);
		insert tsInsertList;
		
		
		//change vacancy then fire update trigger
		v1.Name='ccc';
		v2.Name='cccbb';
		update vInsertList;
		Placement__c chk_v1 = [select Id, Name from Placement__c where Id=:v1.Id];
		Placement__c chk_v2 = [select Id, Name from Placement__c where Id=:v2.Id];
		// check if renaming is successful or not
		system.assertEquals(chk_v1.Name,'ccc');
		system.assertEquals(chk_v1.Name,v1.Name);
		system.assertEquals(chk_v2.Name,v2.Name);
		
		tsList = DaoTimeSheet.getPendingTSByVIds(insertIds);
		for(TimeSheet__c ts:tsList){
			Placement__c vTmp = vMap.get(ts.Vacancy__c);
			system.debug('timesheet status:'+ts.TimeSheet_Status__c);
			system.assertEquals(ts.Approver__c,vTmp.Internal_Approver__c);
		}
		system.assertEquals(tsList.size(), 5);
		}
	} 
}