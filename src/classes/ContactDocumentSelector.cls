/*
    ContactDocumentSelector fetch Contact related documents
    @param Contact List
    @return Contactid, webdocument map
 
*/
public class ContactDocumentSelector extends CommonSelector{
	public ContactDocumentSelector(){
		super(Web_Document__c.sObjectType);
	}
	
	private static ContactDocumentSelector selector = new ContactDocumentSelector();
	
    public static Map<String, Web_Document__c> getContactExistingDocument(List<Contact> conList) {
        Map<String, Web_Document__c> contactDocumentMapping = new Map<String, Web_Document__c>();
        
        selector.checkRead('Id, Document_Related_To__c');
        List<Web_Document__c> webDoclist = [Select Id, Document_Related_To__c from Web_Document__c where Document_Type__c = 'Resume' and Document_Related_To__c in: conList];
        if(webDoclist != null && webDoclist.size()>0) {
            for(Web_Document__c wd : webDoclist) {
                contactDocumentMapping.put(wd.Document_Related_To__c, wd);
            }
        }
        system.debug('contactDocumentMapping ='+ contactDocumentMapping.size());
        return contactDocumentMapping;
    }
}