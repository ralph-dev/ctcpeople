/**
 * This is the test class for Endpoint.cls
 * 
 * Created by: Lina
 * Created on: 06/09/2016
 *
 **/
@isTest
public class EndpointTest {
    
	static testmethod void testGetPeoplecloudWebSite(){
		System.runAs(DummyRecordCreator.platformUser) {
		GSA__c peopleCloudEndPoint = GSA__c.getValues('peoplecloudEndpoint');
		if(peopleCloudEndPoint == null || peopleCloudEndPoint.name__c == null || peopleCloudEndPoint.name__c==''){
            system.assertEquals('https://www.clicktocloud.com', Endpoint.getPeoplecloudWebSite());
        }else{
        	system.assertEquals(peopleCloudEndPoint.name__c, Endpoint.getPeoplecloudWebSite());
        }
		}
	}
	
	static testmethod void testEndpointCorrectness(){
		System.runAs(DummyRecordCreator.platformUser) {
		system.debug('Vacancy object name:'+Schema.SObjectType.Placement__c.getName());
		
		if(Schema.SObjectType.Placement__c.getName().contains('PeopleCloud1__')){
			system.assertEquals(Endpoint.PEOPLECLOUDWEBSITE,'https://www.clicktocloud.com');
			system.assertEquals(Endpoint.PEOPLECLOUDSEARCH,'https://search.clicktocloud.com');
		}
		}
	} 
	
	static testmethod void testGetcpewsEndpoint() {
		System.runAs(DummyRecordCreator.platformUser) {
	    Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('cpews');
	    if (customEndpoint == null || customEndpoint.Endpoint__c == null || customEndpoint.Endpoint__c == '') {
	        system.assertEquals('https://cpews.clicktocloud.com', Endpoint.getcpewsEndpoint());
	    } else {
	        system.assertEquals(customEndpoint.Endpoint__c, Endpoint.getcpewsEndpoint());
	    }
		}
	}
	
	static testmethod void testGetAstuteEndpoint() {
		System.runAs(DummyRecordCreator.platformUser) {
	    Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('astute');
	    if (customEndpoint == null || customEndpoint.Endpoint__c == null || customEndpoint.Endpoint__c == '') {
	        system.assertEquals('https://cpews.clicktocloud.com', Endpoint.getcpewsEndpoint());
	    } else {
	        system.assertEquals(customEndpoint.Endpoint__c, Endpoint.getAstuteEndpoint());
	    }
		}
	}
	
	static testmethod void testGetPeopleSearchEndpoint() {
		System.runAs(DummyRecordCreator.platformUser) {
	    Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('peoplesearch');
	    if (customEndpoint == null || customEndpoint.Endpoint__c == null || customEndpoint.Endpoint__c == '') {
	        system.assertEquals('https://peoplesearch.clicktocloud.com', Endpoint.getPeopleSearchEndpoint());
	    } else {
	        system.assertEquals(customEndpoint.Endpoint__c, Endpoint.getPeopleSearchEndpoint());
	    }
		}
	}	

	static testmethod void testGetBulkemailEndpoint() {
		System.runAs(DummyRecordCreator.platformUser) {
	    Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('bulkemail');
	    if (customEndpoint == null || customEndpoint.Endpoint__c == null || customEndpoint.Endpoint__c == '') {
	        system.assertEquals('https://ws.clicktocloud.com', Endpoint.getBulkemailEndpoint());
	    } else {
	        system.assertEquals(customEndpoint.Endpoint__c, Endpoint.getBulkemailEndpoint());
	    }
		}
	}

	static testmethod void testJXTDefaultEndpoint() {
		System.runAs(DummyRecordCreator.platformUser) {
		Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('jxtWebservice');
		if (customEndpoint == null || customEndpoint.Endpoint__c == null || customEndpoint.Endpoint__c == '') {
			system.assertEquals('https://cpews.clicktocloud.com', Endpoint.getJXTDefaultEndpoint());
		} else {
			system.assertEquals(customEndpoint.Endpoint__c, Endpoint.getJXTDefaultEndpoint());
		}
		}
	}

	static testmethod void testSeekDefaultEndpoint() {
		System.runAs(DummyRecordCreator.platformUser) {
		Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('seekWebservice');
		if (customEndpoint == null || customEndpoint.Endpoint__c == null || customEndpoint.Endpoint__c == '') {
			system.assertEquals('https://cpews.clicktocloud.com', Endpoint.getSeekDefaultEndpoint());
		} else {
			system.assertEquals(customEndpoint.Endpoint__c, Endpoint.getSeekDefaultEndpoint());
		}
		}
	}
}