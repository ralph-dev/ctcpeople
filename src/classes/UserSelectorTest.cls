@isTest
private class UserSelectorTest {

	private static testMethod void testSelector() {
		
	    User u = DataTestFactory.createUser();
		u.ProfileId = DataTestFactory.getProfileId('System Administrator');
		insert u;
		System.runAs(DummyRecordCreator.platformUser) {
		    List<Web_Document__c> resumeList = DataTestFactory.createResumeForUsr(u.Id);
		    List<User> uList = UserSelector.getUsrById(u.Id);
		    system.assertEquals(uList.size(), 1);
		    system.assertEquals(uList[0].Resume_and_Files__r.size(), 10);
		}

		
	}
	
	private static testMethod void testGetUser() {
	    
	    
	    DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
	    DummyRecordCreator.createUsers(rs);
	    System.runAs(DummyRecordCreator.platformUser) {
	    	
	        UserSelector userSelector = new UserSelector();
	        User u =userSelector.getUser(rs.users.get(0).Id);
	        system.assert(u!=null);
	    }
	}

	static testMethod void testGetCurrentUser() {
		System.runAs(DummyRecordCreator.platformUser) {
			User u = new UserSelector().getCurrentUser();
			System.assertEquals(DummyRecordCreator.platformUser.Id, u.Id);

		}
	}

	static testMethod void testGetUserList() {
		
		
		Set<String> userIds = new Set<String>();
		userIds.add(DummyRecordCreator.admin.Id);
		userIds.add(DummyRecordCreator.platformUser.Id);
		System.runAs(DummyRecordCreator.platformUser) {
			List<User> users = new UserSelector().getUserList(userIds);
			System.assertEquals(2, users.size());
		}
		
		
	}

}