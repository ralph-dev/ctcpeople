@isTest
private class JxtnzClaAdminTest
{
	@isTest
	static void testJxtNzClaAdmin(){
		JxtnzClaAdmin jxtAdmin = new JxtnzClaAdmin();
		jxtAdmin.init();
		system.assert(jxtAdmin.selectedClassification.size()>0);
		system.assert(jxtAdmin.getClassifications().size()>0);
		jxtAdmin.selectedClassification[0]='Accounting';
		jxtAdmin.setSelectedClassification(jxtAdmin.getSelectedClassification());
		PageReference testpagereference = jxtAdmin.save();
		system.assert(testpagereference == null);
	}
}