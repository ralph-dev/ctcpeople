@isTest
private class RosterDTOTest {
	final static String ROSTER_STATUS_PENDING = 'Pending';
	
    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        Days_Unavailable__c roster = 
        	TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_PENDING);
        RosterDTO rDTO = 
        	TestDataFactoryRoster.createRosterDTO(acc, con, shift, roster,ROSTER_STATUS_PENDING);
        
        System.assertNotEquals(rDTO.getStartDate(),'');
        System.assertNotEquals(rDTO.getStartTime(),'');
        System.assertNotEquals(rDTO.getEndDate(),'');
        System.assertNotEquals(rDTO.getEndTime(),'');
        System.assertNotEquals(rDTO.getLocation(),'');
        System.assertNotEquals(rDTO.getCandidateId(),'');
        System.assertNotEquals(rDTO.getClientId(),'');
        System.assertEquals(rDTO.getCmId(),'');
        System.assertNotEquals(rDTO.getShiftId(),'');
        System.assertNotEquals(rDTO.getShiftType(),'');
        System.assertNotEquals(rDTO.getStatus(),'');
        System.assertEquals(rDTO.getWeeklyRecurrence(),true);
        System.assertNotEquals(rDTO.getRecurEndDate(),'');
        System.assertNotEquals(rDTO.getWeekdaysDisplay(),'');
        System.assertNotEquals(rDTO.getId(),'');       
    	} 
    }
}