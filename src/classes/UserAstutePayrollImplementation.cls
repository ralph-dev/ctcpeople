public with sharing class UserAstutePayrollImplementation {
    public void pushToAstutePayroll(list<User> userRecords){
        UserServices userServ=new UserServices();
        list<map<String,Object>> objSTR=userServ.convertUserToOwner(userRecords);
        map<String,Object> messagesJson=new map<String,Object>();
        messagesJson.put('userSaves',objSTR);
        String messages=JSON.serialize(messagesJson);
        /** 
            call future job to send the message to server
        
        **/ 
        String endPointStr=Endpoint.getAstuteEndpoint();
        endpointStr=endPointStr+'/AstutePayroll/owner';
        if(!Test.isRunningTest()){
            AstutePayrollFutureJobs.pushToServer(messages,endpointStr,AstutePayrollUtils.GetloginSession());
        }
    }
    public static void astutePayrollSetCriterial(list<User> newRecords,map<Id,User> userOwnerMap){
         for(User singleUser: newRecords){
          if(!singleUser.Is_Api__c && singleUser.Astute_Payroll_Upload_Status__c !='On Hold'){
                   singleUser.Astute_Payroll_Upload_Status__c ='On Hold' ;                     
               }
           if(singleUser.Is_Api__c){
                 singleUser.Is_Api__c=false;
           }
         }        
        
    }
    /****
    	call the future job to Syncrhonize user info from ctc People
    	to Astute Payroll.
    										-Alvin  
    
    ********/
    public void astutePayrollTrigger(list<User> users){
	    CTCPeopleSettingHelperServices ctcPeopleService=new CTCPeopleSettingHelperServices();	    
	    if(!ctcPeopleService.hasApAccount()){
	        return;
	    }
	    list<User> usersForAstutePayroll=new list<User>();
        for(User singleUser: users){
        	if(singleUser.Is_Pushing_To_AP__c){
        		singleUser.Is_Pushing_To_AP__c=false;
        		singleUser.Astute_Payroll_Upload_Status__c ='On Hold';
        		usersForAstutePayroll.add(singleUser);
        	}
           
        }        
        if(usersForAstutePayroll.size()>0){
           pushToAstutePayroll(usersForAstutePayroll);
        }
	  }
}