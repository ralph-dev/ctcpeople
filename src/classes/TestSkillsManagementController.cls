@isTest
private class TestSkillsManagementController {
    
    static testMethod void testSkillsManagementCotroller(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	// insert a dummy candidate
    	List<Contact> candList = new List<Contact>();
    	RecordType candRT=[select Id from RecordType where SobjectType = 'Contact' limit 1];
        Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1');
        candList.add(cand1);
        insert candList;
    		
    	// insert dummy skill groups
    	List<Skill_Group__c> sgList = new List<Skill_Group__c>();
    	Skill_Group__c skillgroup1 = new Skill_Group__c(Name='skillgroup1');
        Skill_Group__c skillgroup2 = new Skill_Group__c(Name='skillgroup2');
        sgList.add(skillgroup1);
        sgList.add(skillgroup2);
        insert sgList;
    	
    	// insert dummy skills
    	List<Skill__c> skillList = new List<Skill__c>();
    	Skill__c skill1 = new Skill__c(Name='skill1', Skill_Group__c=skillgroup1.Id, Ext_Id__c='ext1');
        Skill__c skill2 = new Skill__c(Name='skill2', Skill_Group__c=skillgroup1.Id, Ext_Id__c='ext2');
    	Skill__c skill3 = new Skill__c(Name='skill3', Skill_Group__c=skillgroup2.Id, Ext_Id__c='ext3');
        Skill__c skill4 = new Skill__c(Name='skill4', Skill_Group__c=skillgroup2.Id, Ext_Id__c='ext4');
        skillList.add(skill1);
        skillList.add(skill2);
        skillList.add(skill3);
        skillList.add(skill4);
        insert skillList;
        
        // insert dummy candidate skill records
        List<Candidate_Skill__c> candidateSkillList = new List<Candidate_Skill__c>();
        Candidate_Skill__c cs1 = new Candidate_Skill__c(Candidate__c=cand1.id,Skill__c=skill1.Id,Verified__c=true );
        Candidate_Skill__c cs2 = new Candidate_Skill__c(Candidate__c=cand1.id,Skill__c=skill2.Id,Verified__c=true );
        Candidate_Skill__c cs3 = new Candidate_Skill__c(Candidate__c=cand1.id,Skill__c=skill3.Id);
        Candidate_Skill__c cs4 = new Candidate_Skill__c(Candidate__c=cand1.id,Skill__c=skill4.Id);
        candidateSkillList.add(cs1);
        candidateSkillList.add(cs2);
        candidateSkillList.add(cs3);
        candidateSkillList.add(cs4);
        insert candidateSkillList;
        
        // test controller constructor
        PageReference pageRef = Page.SkillsManagement;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', cand1.Id);
        ApexPages.StandardController con = new ApexPages.StandardController(cand1); 
        SkillsManagementController smc = new SkillsManagementController(con);
        String nameSpace = smc.namespace;
        String skillPageURL = smc.addSkillsPageUrl;
        PageReference pReference = smc.addSkills();
        system.assert(pReference!=null);
        
        // test retrieveSkillGroupsJSON()
        String skillGroupsJSON = SkillsManagementController.retrieveSkillGroupsJSON();
		List<Object> sgResultList = (List<Object>)JSON.deserializeUntyped(skillGroupsJSON);
		system.debug('=================== sgResultList:' + sgResultList);
		system.debug('=================== sgResultList size:' + sgResultList.size());
		system.assert(sgResultList.size()>=sgList.size());
		
	
		// test retrieveSkillsJSON()
		String skillsJSON = SkillsManagementController.retrieveSkillsJSON(cand1.Id);
		List<Object> skillResultList = (List<Object>)JSON.deserializeUntyped(skillsJSON);
		system.debug('=================== skillResultList:' + skillResultList);
		system.debug('=================== skillResultList size:' + skillResultList.size());
		system.assert(skillResultList.size()==skillList.size());
		
		// test updateVerificationStatus()
		Map<Id, Boolean> updatedSkillVerificationStatus = new Map<Id, Boolean>();
		updatedSkillVerificationStatus.put(cs1.Id,false);
		updatedSkillVerificationStatus.put(cs2.Id,false);
		updatedSkillVerificationStatus.put(cs3.Id,true);
		updatedSkillVerificationStatus.put(cs4.Id,true);
		SkillsManagementController.updateVerificationStatus(updatedSkillVerificationStatus);
		List<Candidate_Skill__c> csResultList = [select Id,Verified__c from Candidate_Skill__c where Candidate__c =: cand1.Id];
		system.assert(csResultList.size() == 4);
		for(Integer i =0 ; i < csResultList.size(); i++){
			if(csResultList.get(i).Id == cs1.Id || csResultList.get(i).Id == cs2.Id ){
				system.assert(csResultList.get(i).Verified__c == false);
			}else{
				system.assert(csResultList.get(i).Verified__c == true);
			}
		}
		
		// test deleteSkills()
		List<String> candidateSkillToDelete = new List<String>();
		candidateSkillToDelete.add(cs1.Id);
		candidateSkillToDelete.add(cs2.Id);
		SkillsManagementController.deleteSkills(candidateSkillToDelete);
		List<Candidate_Skill__c> csAfterDeleteResultList = [select Id from Candidate_Skill__c where Candidate__c =: cand1.Id];
		system.assert(csAfterDeleteResultList.size() == 2);
		
    	}
    }  
    
}