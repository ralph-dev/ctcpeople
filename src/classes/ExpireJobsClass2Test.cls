/**
 * Test class for ExpireJobClass2
 * Last modified date: 18/04/2016
**/ 

@isTest
private class ExpireJobsClass2Test
{
	private static void createDummyAd() {
		Account account  = new Account(Name = 'test');
		insert account;

		Placement__c vac = new Placement__c();
		vac.Company__c = account.id;
		insert vac;

		RecordType seekRecordType = DaoRecordType.seekAdRT;

		Advertisement__c ad = new Advertisement__c();
		ad.Vacancy__c = vac.id;
		ad.Status__c = 'Active';
		ad.Placement_Date__c = system.today() - 30;
		ad.RecordTypeId = seekRecordType.Id;
		insert ad;
	}
	
	static testmethod void expireJobsClass2Test() {
		System.runAs(DummyRecordCreator.platformUser) {
	    // create data
	    createDummyAd();
	    
        PCAppSwitch__c pcSwitch = PCAppSwitch__c.getOrgDefaults();
        pcSwitch.Disable_Notification_of_Expired_Ads__c = false;
        upsert pcSwitch;
		ExpireJobsClass2 expireJobs = new ExpireJobsClass2();
		system.assert(expireJobs.jobs_map_seek.size()>0);
		}
	}

}