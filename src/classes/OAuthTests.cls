@isTest
private class OAuthTests {
	public static OAuth_Service__c createTestData() {     
        OAuth_Service__c s = new OAuth_Service__c();
        s.name='test1234';
        s.Request_Token_URL__c = 'http://requesttokenurl';
        s.Access_Token_URL__c = 'http://requesttokenurl';
        //commented by andy for security review II
        //s.Consumer_Key__c = 'consumerkey';
        //s.Consumer_Secret__c = 'consumersecret';
        s.Authorization_URL__c = 'http://testauthorizationurl';
        
        CommonSelector.quickInsert(s);
        
        //added by andy for security review II
        CredentialSettingService.upsertLinkedInOAuthService(s.Id,'consumerkey','consumersecret');
        
        return s;
    }
    
    //Create dummy OAuth_Token__c data;
    public static OAuth_Token__c createOauthtoken(OAuth_Service__c dummyoauthservice){
    	OAuth_Token__c dummyOauthtoken =  new OAuth_Token__c();
    	dummyOauthtoken.OAuth_Service__c = dummyoauthservice.id;
    	dummyOauthtoken.Owner__c = userinfo.getUserId();
    	dummyOauthtoken.isAccess__c =  true;
    	//commented by andy for security review II
    	//dummyOauthtoken.secret__c = '123456';
    	//dummyOauthtoken.token__c = '123456';

		CommonSelector.quickInsert(dummyOauthtoken);
    	
    	    	
    	//added by andy for security review II
    	CredentialSettingService.upsertLinkedInOAuthToken(dummyOauthtoken.Id,'123456','123456');
    	
    	return dummyOauthtoken;
    }
    
    public static testMethod void testGetUsersOfService() {
    	System.runAs(DummyRecordCreator.platformUser) {
        OAuth_Service__c dummyosc = createTestData();
        OAuth_Token__c dymmyotc = createOauthtoken(dummyosc);
        OAuth o = new OAuth();
        List<User> getUsersOfService = o.getUsersOfService('test1234');
        User currentuser =(User) new CommonSelector(User.sObjectType).setParam('userId',dymmyotc.Owner__c).get('id','id=:userId')[0];
        system.assert(getUsersOfService.size()== 1);
        boolean testboolean = o.setService(dummyosc.name);
        system.assert(testboolean);
    	}
    }

    public static testMethod void testAuthorize() {
    	
       try {
       	System.runAs(DummyRecordCreator.platformUser) {
            //OAuthTests.createTestData();
            User a = new User();
            String ss = OAuthTests.createTestData().id;
            Test.setCurrentPage(Page.AuthPage);
            ApexPages.currentPage().getHeaders().put('Host','testhost');
            ApexPages.currentPage().getParameters().put('contactUrl','aaaaaaaaaaa');
            ApexPages.currentPage().getParameters().put('retURL','aaaaaaaaaaa');
            AuthController ac = new AuthController();
            ac.searchinfo = 'aaaaaaaaaaa';
            ac.authUrl = 'bogus';
            ac.message = 'bogus';
            //System.assert(ac.services.size()>0);
            PageReference authorizepage = ac.authorize();
            system.assert(authorizepage!=null);
            Test.setCurrentPage(Page.CompleteAuth);
            ApexPages.currentPage().getParameters().put('oauth_token','token');
            ApexPages.currentPage().getParameters().put('oauth_verifier','verifier');
            ApexPages.currentPage().getParameters().put('retURL','aaaaaaaaaaa');
            OAuth_Token__c ot = new OAuth_Token__c();
            
            ot.Owner__c = a.id;
            ot.OAuth_Service__c= ss;
            ot.isAccess__c = false;
            
            //commented by andy for security review II
            //ot.secret__c = 'secret';
            //ot.token__c  = 'token';
            CommonSelector.quickInsert( ot);
            
            //added by andy for security review II
            CredentialSettingService.upsertLinkedInOAuthToken(ot.Id,'secret','token');
            
            
            PageReference completeAuthorizationpage = ac.completeAuthorization();
            system.assert(completeAuthorizationpage!=null);
       	}
        }
        catch(Exception e) {
            throw e;
        }
    }
    
   	public static testMethod void testcontact(){
   		System.runAs(DummyRecordCreator.platformUser) {
        Test.setCurrentPage(Page.AuthPage);
        ApexPages.currentPage().getHeaders().put('Host','testhost');
        ApexPages.currentPage().getParameters().put('contactUrl','aaaaaaaaaaa');
        Contact con = new Contact(LastName ='test', FirstName='test',mailingcountry='Australia' , Othercountry='Australia', title='Developer');
        CommonSelector.quickInsert( con);
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(con);
        ApexPages.currentPage().getParameters().put('contactUrl', con.id);
        OauthService oauthser = new OauthService(stdController);
        system.assertEquals(oauthser.new_cont.LastName, 'test');
        PageReference searchpage = oauthser.search();
        system.assertEquals(searchpage, null);
        PageReference signoutpage = oauthser.signout();
        system.assertEquals(signoutpage, null);
        PageReference linkedprofile = oauthser.linkedprofile();
        system.assert(linkedprofile!=null);
        PageReference setnextPage = oauthser.setnextPage();
        system.assert(setnextPage!=null);
        PageReference setpreviousPage = oauthser.setpreviousPage();
        system.assert(setpreviousPage!=null);
   		}
    }
    
    public static testMethod void testcontacturl(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Test.setCurrentPage(Page.AuthPage);
        ApexPages.currentPage().getHeaders().put('Host','testhost');
        ApexPages.currentPage().getParameters().put('contactUrl','');
        Contact con = new Contact(LastName ='test', FirstName='test', Othercountry='Australia', title='Developer', public_Profile_Url__c='aaaaaaaaaaaaaaaaa');
        CommonSelector.quickInsert( con );
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(con);
        ApexPages.currentPage().getParameters().put('contactUrl', con.id);
        OauthService oauthser = new OauthService(stdController);
        oauthser.search();
        Contact con_1 = new Contact(LastName ='test_1', FirstName='test_1', Othercountry='Australia', title='Developer');
        CommonSelector.quickInsert( con_1 );        
        
        ApexPages.Standardcontroller stdController_1 = new ApexPages.Standardcontroller(con_1);
        ApexPages.currentPage().getParameters().put('contactUrl', con_1.id);
        OauthService oauthser_1 = new OauthService(stdController_1);
        oauthser_1.search();
        Contact con_2 = new Contact(LastName ='test_2');
        CommonSelector.quickInsert( con_2 );
        ApexPages.Standardcontroller stdController_2 = new ApexPages.Standardcontroller(con_2);       
        ApexPages.currentPage().getParameters().put('contactUrl', con_2.id);       
        OauthService oauthser_2 = new OauthService(stdController_2);       
        PageReference searchpage = oauthser_2.search();
        system.assertEquals(searchpage, null);
    	}
        }
    /** deprecated test method 02/05/2017 **/
     public static testMethod void testlinkedinprofile() {
     System.runAs(DummyRecordCreator.platformUser) {
       Contact con = new Contact(LastName ='test', FirstName='test',mailingcountry='Australia' , Othercountry='Australia', title='Developer');
       CommonSelector.quickInsert( con );
       Test.setCurrentPage(Page.AuthPage);
       ApexPages.currentPage().getHeaders().put('Host','testhost');
       ApexPages.currentPage().getParameters().put('contactUrl',con.id);
       ApexPages.currentPage().getParameters().put('ProfilePage','ProfilePage');
       LinkedinProfile pro = new LinkedinProfile();
       PageReference porfilepage = pro.porfile();
       system.assertEquals(porfilepage,null);
       PageReference Research = pro.Research();
       system.assertnotEquals(Research, null);
       pro.setDefault();
       system.assertnotEquals(pro.publicurl,null);
     	}
    }
    
}