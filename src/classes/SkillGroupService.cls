public with sharing class SkillGroupService extends SObjectCRUDService{
	public SkillGroupService() {
		super(Skill_Group__c.sObjectType);
	}

	//Update CTCPeopleSettingHelper__c
	public List<Skill_Group__c> updateDefaultSkillGroup(Set<Skill_Group__c> defaultSkillGroupSet, Set<String> selectedSkillGroupSet) {
	    List<Skill_Group__c> preSelectedSkillGroups =  SkillGroups.getSkillGroupsByExtIds(selectedSkillGroupSet );
        defaultSkillGroupSet.addAll(preSelectedSkillGroups);
	  	List<Skill_Group__c> defaultSkillGroupNeedUpdate = new List<Skill_Group__c>();
		for(Skill_Group__c sg : defaultSkillGroupSet) {
		    if(!selectedSkillGroupSet.contains(sg.Skill_Group_External_Id__c)) {
		        sg.Default__c = false;
		    } else {
		        sg.Default__c = true;
		    }
		    defaultSkillGroupNeedUpdate.add(sg);
		}
		Savepoint sp = Database.setSavepoint();
	

		try {
			    fflib_SecurityUtils.checkObjectIsInsertable(Skill_Group__c.SObjectType);
			    fflib_SecurityUtils.checkFieldIsUpdateable(Skill_Group__c.SObjectType, Skill_Group__c.Default__c);
				upsert defaultSkillGroupNeedUpdate;
				return defaultSkillGroupNeedUpdate;
			} catch(Exception e) {
				throw e;
			}
	}
}