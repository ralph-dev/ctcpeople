public with sharing class InitConfigFileController {
	
    // name of configuration files
    private static final String JOBX_CLASSIFICATION_CONF = 'jobx_classifications';
 	private static final String S3ACCOUNT_CONF = 'S3Account';
 	private static final String SEARCH_ACCOUNT_CONF = 'ClicktoCloud_Xml_Account';
 	private static final String SEARCH_CONTACT_CONF = 'ClicktoCloud_Xml_Contact';
 	private static final String SEARCH_VACANCY_CONF = 'ClicktoCloud_Xml_Placement';
 	private static final String Trigger_ADMIN_CONF = 'Trigger_Admin';
 	private static final String S2S_CONF = 'CTCS2SPeopleCloud';
 	
 	
	public PageReference init(){
		// pre-load configuration file
       //   
       Document tmp = DaoDocument.getCTCConfigFileByDevName(JOBX_CLASSIFICATION_CONF);
       tmp = DaoDocument.getCTCConfigFileByDevName(S3ACCOUNT_CONF);
       tmp = DaoDocument.getCTCConfigFileByDevName(SEARCH_ACCOUNT_CONF);
       tmp = DaoDocument.getCTCConfigFileByDevName(SEARCH_CONTACT_CONF);
       tmp = DaoDocument.getCTCConfigFileByDevName(SEARCH_VACANCY_CONF);
       tmp = DaoDocument.getCTCConfigFileByDevName(Trigger_ADMIN_CONF);
       tmp = DaoDocument.getCTCConfigFileByDevName(S2S_CONF);
       return Page.CTCAdmin;
	}
	
	public static testmethod void testInitConfigFileController(){
		System.runAs(DummyRecordCreator.platformUser) {
		InitConfigFileController initConfigCon = new InitConfigFileController();
		PageReference adminPage = initConfigCon.init();
		system.assert(true);
		}
	}
}