public with sharing class AstuteAccSettingController {
    public String styleCategoryId{get; set;}
    public String userName {get; set;}
    private transient String password;
    private transient String apiKey;

    public String getpassword(){
        return null;
    }

    public void setpassword(String password){
        this.password = password;
    }

    public String getapiKey(){
        return null;
    }

    public void setapiKey (String apiKey){
        this.apiKey = apiKey;
    }
    

    public AstuteAccSettingController(ApexPages.StandardController stdController){
        styleCategoryId = stdController.getId();
        initAstutePayroll();
    }
    
    public void initAstutePayroll(){
        Credential_Setting__c instance;
        if(styleCategoryId != null){
            instance = Credential_Setting__c.getValues(styleCategoryId);
        }
        userName = instance != null ? instance.Username__c : '';
    }
    
    public void save(){
        String userName = userName;
        String password = password;
        String APIKey = apiKey;
        ProtectedCredential pc = CredentialSettingService.upsertAstutePayrollCredential(styleCategoryId, userName, password, APIKey);
    }
    
    public PageReference cancel(){
        PageReference pageRef = new PageReference('/'+styleCategoryId);
        pageRef.setRedirect(true);
        return pageRef;
    }
}