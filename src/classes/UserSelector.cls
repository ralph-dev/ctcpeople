/**
* This is a selector class for User Object
* 
* Created by: Lorena
* Created on: 15/07/2016
* 
**/
public with sharing class UserSelector extends CommonSelector{

    public static String currentUserId = UserInfo.getUserId();

    public UserSelector(){
        super(User.SObjectType);
    }

    /*
     *  Set object as StyleCategory
     */
    /*private Schema.SObjectType getSObjectType(){
        return User.SObjectType;
    }*/
    
    /*
     *  Set default fields that needs to be retrieved by StyleCategorySelector
     */
    public override LIST<Schema.SObjectField> getSObjectFieldList(){
        return new List<Schema.SObjectField>{
            User.Id,
            User.Name,
            User.Email,
            User.UserType,
            User.UserRole.Name,
            User.UserRoleId,
            User.Profile.Name,
            User.ProfileId,
            User.IsActive,
            User.ConsultantCode__c,
            User.Job_Posting_Company_Name__c,
            User.Job_Posting_Email__c,
            User.AstutePayrollAccount__c,
            User.AmazonS3_Folder__c,
            User.WebJobFooter__c,
            User.View_CTC_Admin__c,
            User.VacancySearchFile__c,
            User.ClientSearchFile__c,
            User.PeopleSearchFile__c,
            User.Manage_TimeSheet__c,
            User.Company_Description__c,
            User.ContactId,
            User.AccountId,
            User.Manage_Quota__c,
            User.JobBoards__c,
            User.Seek_Account__c,
            User.Default_Seek_Account__c,
            User.LinkedIn_Account__c,
            User.Indeed_Account__c,
            User.Seek_Usage__c,
            User.MyCareer_Usage__c,
            User.CareerOne_Usage__c,
            User.Monthly_Quota_Seek__c,
            User.Monthly_Quota_MyCareer__c,
            User.Monthly_Quota_CareerOne__c,
            User.Number_of_Records_Showing__c
        };
    }
	
	static public List<User> getUsrById(String usrId){
        List<User> result;
        try{    
            CommonSelector.checkRead(User.SObjectType,'id, name');
            CommonSelector.checkRead(Web_Document__c.SObjectType,'Id, Name,File_Size__c,S3_Folder__c,ObjectKey__c,Default_Doc__c, CreatedDate, Document_Type__c, User__c');
            result = [select id, name, 
                (select Id, Name,File_Size__c,S3_Folder__c,ObjectKey__c,Default_Doc__c, CreatedDate, Document_Type__c, User__c
                from User.Resume_and_Files__r order by CreatedDate desc)
                from User where Id =: usrId order by Name];
        }catch(Exception e){
            result = new List<User>();
        }
        return result;
    }
    
    public User getUser(String userId) {
        SimpleQueryFactory qf = simpleQuery();
	    qf.setCondition('Id =: userId');
	    qf.selectFields(getSObjectFieldList());
	    String soqlStr = qf.toSOQL();
	    List<User> userList = Database.query(soqlStr);
        if(userList!=null &&userList.size()>0) { 
            return userList.get(0);
        } else {
            return null;
        }
    }  

    public List<User> getUserList(Set<String> userIds) {
        SimpleQueryFactory qf = simpleQuery();
        qf.setCondition('Id in: userIds');
        qf.selectFields(getSObjectFieldList());
        String soqlStr = qf.toSOQL();
        List<User> userList = Database.query(soqlStr);
        return userList;
    }

    /**
     * Get current user
     * Updated by Lina Wei on 28/03/2017
     */
    public User getCurrentUser() {
        try {
            SimpleQueryFactory qf = simpleQuery()
                    .selectFields(getSObjectFieldList())
                    .setCondition('Id =: currentUserId');
            List<User> userList = Database.query(qf.toSOQL());
            return userList.get(0);
        } catch(Exception e) {
            System.debug(e);
            return null;
        }
    }

}