@isTest
public class SeekScreenFieldExtTest {
    public static testMethod void testSeekScreenFieldExt(){  
        system.runAs(DummyRecordCreator.platformUser) {
            Application_Question_Set__c aQuestionSet = new Application_Question_Set__c(Name='test');
            insert aQuestionSet;
            Application_Question__c aQuestion = new Application_Question__c();
            aQuestion.name = 'Test'; 
            aQuestion.Application_Question_Set__c = aQuestionSet.Id;
            insert aQuestion;

            ApexPages.StandardController controller = new ApexPages.StandardController(aQuestion);
            SeekScreenFieldExt theController = new SeekScreenFieldExt(controller);
            List<FieldsClass.AvailableField> AvailableField = theController.getAllfields();
            system.assertNotEquals(AvailableField.size(),0);
            PageReference p1 = theController.save() ;
        }
    }
    public static testMethod void myUnitTest2(){  
        system.runAs(DummyRecordCreator.platformUser) {
            Application_Question_Set__c aQuestionSet = new Application_Question_Set__c(Name='test');
            insert aQuestionSet;
            Application_Question__c aQuestion = new Application_Question__c();
            aQuestion.name = 'Test'; 
            aQuestion.Application_Question_Set__c = aQuestionSet.Id;
            insert aQuestion;

            ApexPages.StandardController controller = new ApexPages.StandardController(aQuestion);
            SeekScreenFieldExt theController = new SeekScreenFieldExt(controller);
            List<FieldsClass.AvailableField> AvailableField = theController.getAllfields();
            system.assertNotEquals(AvailableField.size(),0);
            PageReference p1 = theController.save() ;
        }
    }

}