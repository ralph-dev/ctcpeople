@isTest
public class TestDataFactoryRoster {
	final static String SHIFT_STATUS_OPEN = 'Open';
	final static String ROSTER_STATUS_PENDING = 'Pending';
	
	//create account
	public static Account createAccount(){
		Account acc = new Account();
		acc.Name = 'Roster Test Company';
		CommonSelector.quickInsert( acc );
		return acc;
	}
	//create contact
	public static Contact createContact(){
		Contact con = new Contact();
		con.Lastname = 'Roster Test Candidate';
		con.MailingLatitude = -33.867930;
		con.MailingLongitude = 151.205628;
		CommonSelector.quickInsert( con);
		return con;
	}
	//create vacancy
	public static Placement__c createVacancy(Account acc){
		Placement__c vac = new Placement__c();
		vac.Name = 'Roster Test Vacancy';
		vac.Company__c = acc.Id;
		CommonSelector.quickInsert( vac);
		return vac;
	}
	
	//create vacancy search criteria
	public static VacancySearchCriteria createVacancySearchCriteria(Account acc){
		VacancySearchCriteria searchCriteria = new VacancySearchCriteria();
		searchCriteria.setVacancyEntered('');
		searchCriteria.setCompanyId(acc.Id);
		return searchCriteria;
	}
	
	//create vacancyDTO 
	public static VacancyDTO createVacancyDTO(Account acc,Placement__c vac){
		VacancyDTO vacDTO = new VacancyDTO();
		vacDTO.setVacancyCompanyName(acc.Name);
		vacDTO.setVacancyName(vac.Name);
		vacDTO.setVacancyStartDate('2015-12-01');
		vacDTO.setVacancyEndDate('2015-12-21');
		vacDTO.setVacancyId(vac.Id);
		vacDTO.setVacancyCompanyId(acc.Id);
		vacDTO.setVacancyCategory('Doctor');
		vacDTO.setVacancySpecialty('Ophthalmologist');
		vacDTO.setVacancySeniority('Senior Practioner');
		vacDTO.setVacancyRecordTypeId('');
		vacDTO.setVacancyCompanyShippingState('NSW');
		vacDTO.setVacancyCompanyShippingStreet('65 York Street');
		vacDTO.setVacancyCompanyShippingCity('Sydney');
		vacDTO.setVacancyCompanyShippingPostalCode('2000');
		vacDTO.setVacancyCompanyShippingCountry('Au');
		vacDTO.setVacancyCompanyShippingLatitude('-33.867930');
		vacDTO.setVacancyCompanyShippingLongitude('151.205628');
		return vacDTO;	
	}
	
	//create shift
	public static Shift__c createShift(Placement__c vac){
		Shift__c shift = new Shift__c();
		shift.Vacancy__c = vac.Id;
		shift.Shift_Status__c = SHIFT_STATUS_OPEN;
		shift.No_of_Position_Placed__c = 2;
		shift.No_of_Position_Requried__c = 4;
		CommonSelector.quickInsert( shift);
		return shift;
	}
	
	
	//create shiftList for merge shift function, groupShiftsOfSameVacancy 
	//and groupVacancyIdNameFromShifts function
	public static List<Shift__c> createShiftList(Placement__c vac){
		List<Shift__c> shiftList = new List<Shift__c>();
		Shift__c shift1 = new Shift__c();
		shift1.Vacancy__c = vac.Id;
		shift1.Shift_Status__c = SHIFT_STATUS_OPEN;
		shift1.Start_Date__c = Date.valueOf('2015-07-08');
		shift1.End_Date__c = Date.valueOf('2015-07-08');
		shiftList.add(shift1);
		
		Shift__c shift2 = new Shift__c();
		shift2.Vacancy__c = vac.Id;
		shift2.Shift_Status__c = SHIFT_STATUS_OPEN;
		shift2.Start_Date__c = Date.valueOf('2015-07-09');
		shift2.End_Date__c = Date.valueOf('2015-07-09');
		shiftList.add(shift2);
		
		CommonSelector.quickInsert( shiftList);
		return shiftList;	
	}
	
	//create shiftList for checkEndDateNeedToBeUpdated
	public static List<Shift__c> createShiftListToCheckEndDateNeedToBeUpdated(Placement__c vac){
		List<Shift__c> shiftList = new List<Shift__c>();
		Shift__c shift1 = new Shift__c();
		shift1.Vacancy__c = vac.Id;
		shift1.Shift_Status__c = SHIFT_STATUS_OPEN;
		shift1.Start_Date__c = Date.valueOf('2015-07-08');
		shift1.End_Date__c = Date.valueOf('2015-07-09');
		Shift1.Recurrence_End_Date__c = Date.valueOf('2015-07-08');
		shiftList.add(shift1);
			
		CommonSelector.quickInsert( shiftList);
		return shiftList;	
	}
	
	//create shiftList for splitShiftForDisplay function
	public static List<Shift__c> createShiftListForSplitShiftFunction(Account acc, Placement__c vac){
		List<Shift__c> shiftList = new List<Shift__c>();
		Shift__c shift1 = new Shift__c();
		shift1.Account__c = acc.Id;
		shift1.Vacancy__c = vac.Id;
		shift1.Shift_Status__c = SHIFT_STATUS_OPEN;
		shift1.Start_Date__c = Date.valueOf('2015-07-08');
		shift1.End_Date__c = Date.valueOf('2015-07-09');
		Shift1.Recurrence_End_Date__c = Date.valueOf('2015-07-15');
		Shift1.Recurrence_Weekdays__c = 'Wed;Thu';
		shiftList.add(shift1);
			
		CommonSelector.quickInsert( shiftList);
		return shiftList;	
	}
	
	//create shiftDTO
	public static ShiftDTO createShiftDTO(Account acc,Placement__c vac,Shift__c shift){
		ShiftDTO sDTO = new ShiftDTO();
		sDTO.setStartDate('2015-07-02');
		sDTO.setsTime('10:00');
		sDTO.setEndDate('2015-07-08');
		sDTO.seteTime('11:00');
		sDTO.setsRequiredPositionNumber('1');
		sDTO.setsPlacedPositionNumber('1');
		sDTO.setsLocation('Test Location');
		sDTO.setsVacId(vac.Id);
		sDTO.setsCompanyId(acc.Id);
		sDTO.setsType('Day');
		sDTO.setsRecurEndDate('2015-07-29');
		sDTO.setsWeekdaysDisplay('Wed;Thu');
		sDTO.setsId(shift.Id);
		sDTO.setsShiftStatus(SHIFT_STATUS_OPEN);
		sDTO.setsVacancyName(vac.Name);
		return sDTO;
	}
	
	public static RosterDTO createRosterDTO(Account acc, Contact con, Shift__c shift, Days_Unavailable__c roster,String status){
		RosterDTO rDTO = new RosterDTO();
		rDTO.setStartDate('2015-07-02');
		rDTO.setStartTime('10:00');
		rDTO.setEndDate('2015-07-08');
		rDTO.setEndTime('11:00');
		rDTO.setLocation('test test');
		rDTO.setCandidateId(con.Id);
		rDTO.setClientId(acc.Id);
		rDTO.setCmId('');
		rDTO.setShiftId(shift.Id);
		rDTO.setShiftType('Day');
		rDTO.setStatus(status);
		rDTO.setWeeklyRecurrence(true);
		rDTO.setRecurEndDate('2015-07-29');
		rDTO.setWeekdaysDisplay('Wed;Thu');
		rDTO.setId(roster.Id);
		
		return rDTO;
	}
	
	public static Days_Unavailable__c createRoster(Contact con, Shift__c shift, Account acc, String status){
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');
		
		Days_Unavailable__c roster = new Days_Unavailable__c();
		roster.Contact__c = con.Id;
		roster.Accounts__c = acc.Id;
		roster.Shift__c = shift.Id;
		roster.Event_Status__c = status;
		roster.recordTypeId = rtId;
		
		CommonSelector.quickInsert( roster);
		return roster;
	}
	
	public static RosterParamDTO createRosterParamDTO(Account acc,String rosterStatus, String rosterType){
		RosterParamDTO rParamDTO = new RosterParamDTO();
		rParamDTO.setId(acc.Id);
		rParamDTO.setStatus(rosterStatus);
		rParamDTO.setRosterType(rosterType);
		return rParamDTO;
	}
	
	//create rosterList for mergeRoster function
	public static List<Days_Unavailable__c> createRosters(Contact con, Shift__c shift, Account acc, String status){
		List<Days_Unavailable__c> rosterList = new List<Days_Unavailable__c>();
		Days_Unavailable__c roster1 = createRoster(con,shift,acc,status);
		roster1.Start_Date__c = Date.valueOf('2015-07-08');
		roster1.End_Date__c = Date.valueOf('2015-07-08');
		rosterList.add(roster1);
		
		Days_Unavailable__c roster2 = new Days_Unavailable__c();
		roster2.Start_Date__c = Date.valueOf('2015-07-09');
		roster2.End_Date__c = Date.valueOf('2015-07-09');
		rosterList.add(roster2);
		
		return rosterList;	
	}
	//create rosterList for checkEndDateNeedToBeUpdated function
	public static List<Days_Unavailable__c> createRostersToCheckEndDateNeedToBeUpdated(Contact con, Shift__c shift, Account acc, String status){
		List<Days_Unavailable__c> rosterList = new List<Days_Unavailable__c>();
		Days_Unavailable__c roster1 = createRoster(con,shift,acc,status);
		roster1.Start_Date__c = Date.valueOf('2015-07-08');
		roster1.End_Date__c = Date.valueOf('2015-07-09');
		roster1.Recurrence_End_Date__c = Date.valueOf('2015-07-08');
		rosterList.add(roster1);
		
		return rosterList;	
	}
	
	//create rosterList for splitRoster function
	public static List<Days_Unavailable__c> createRostersForSplitRosterFunction(Contact con, Shift__c shift, Account acc, String status){
		List<Days_Unavailable__c> rosterList = new List<Days_Unavailable__c>();
		Days_Unavailable__c roster1 = createRoster(con,shift,acc,status);
		roster1.Start_Date__c = Date.valueOf('2015-07-08');
		roster1.End_Date__c = Date.valueOf('2015-07-09');
		roster1.Recurrence_End_Date__c = Date.valueOf('2015-07-15');
		roster1.Recurrence_Weekdays__c = 'Wed;Thu';
		rosterList.add(roster1);
		
		CommonSelector.quickUpsert( rosterList );	
		return rosterList;	
	}

	public static Candidate_Skill__c createCandidateSkill(Contact con, Skill__c skill) {
		Candidate_Skill__c cSkill = new Candidate_Skill__c();
		cSkill.Candidate__c = con.Id;
		cSkill.Skill__c = skill.Id;
		CommonSelector.quickInsert( cSkill);
		return cSkill;
	}

	public static Skill_Group__c createSkillGroup() {
		Skill_Group__c sGroup = new Skill_Group__c();
		sGroup.Name = 'IT';
		sGroup.Skill_Group_External_Id__c = 'IT';
		CommonSelector.quickInsert( sGroup);
		return sGroup;
	}

	public static Skill__c createSkill() {
		Skill_Group__c sGroup = createSkillGroup();
		Skill__c skill = new Skill__c();
		skill.Skill_Group__c = sGroup.Id;
		skill.Name = 'Java';
		skill.Ext_Id__c = 'Java';
		CommonSelector.quickInsert( skill);
		return skill;
	}
}