/**************************************
 *                                    *
 * Deprecated Class, 13/06/2017 Ralph *
 *                                    *
 **************************************/
/**

Update multiple selected Candidate Management Record
to same given status.
Add the new Email Template function, Add the log activity function, add the replyto validation function
**/


public with sharing class MassUpdateVCWController{
 //   public String customEmailSubject{set;get;}
 //   Placement_Candidate__c[] pcs;
 //   public Placement_Candidate__c thesamplepc {get;set;}
 //   Public String msg{get;set;}
 //   public Boolean is_test{get;set;}
 //   Public Boolean displaytemplate {get;set;} 
 //   public Boolean hasorgemail{get;set;}
 //   public String commandlinkaction {get; set;}
 //   public String commandlinkvalue {get; set;}
 //   public String selectlink {get; set;} 
 //   public static final string massUpdateTemplate = 'Mass_Update_Template'; 
   
 //   //*********************
 //   public String SelectedOrgEmail {get;set;}
 //   List<SelectOption> orgEmails = new List<SelectOption>{};
 //   public List<SelectOption> getOrgEmails(){
 //       return orgEmails;
 //   }
 //   public List<Placement_Candidate__c> allvcw {get; set;}
 //   List<SelectOption> templates = new List<SelectOption>{};
 //   List<Placement_Candidate__c> records;
 //   List<Placement_Candidate__c> massupdaterecords; //for mass update status
 //   public string vid {get; set;}
 //   public string cmids;
 //   public List<Id> vids;
 //   public string currentpage;
 //   public String currentSelectCrieria;
 //   private String currentAscSelectCrietria;

 //   UserInformationCon usercls = new UserInformationCon();
 //   public User u; //get the display name and reply to email
    
 //   public string cmssource;
    
 //   //Add the Email Template function by Jack on 21 Sep 12 
 //   public SendResumeEmailTemplate newSendResumeEmailTemplate {get;set;} //Init the Template list
 //   public List<SendResumeEmailTem> newSendResTem{get; set;}
 //   public List<EmailTemplate> emailTempList{get;set;}    
 //   public String selectedTempId{get;set;}
 //   public String htmlTemplateContent{get;set;}
 //   public String customEmailContent{get;set;}
 //   private String customEmailFrameBottom{get;set;}
 //   private String customEmailFrameTop{get;set;}
 //   private EmailTemplate selectedTemp;
 //   public EmailTemplate sendOutTemp;
 //   public EmailTemplate sendResumeEmailTemplate;
 //   public PeopleCloudErrorInfo errormesg;
 //   public Boolean msgSignalhasmessage{get ;set;} //show error message or not
 //   public String selectedTempName{get;set;}
 //   //End the Add the Email Template function
    
 //   //Old SendEmail method params
 //   public String replyto {get;set;}
 //   public String displayname {get;set;}
 //   public boolean use_sign {get;set;}
    
 //   //Use Jquery to jump page
 //   public String returnUrl{get;set;}
	//public Boolean triggerParentPage {get; set;}
	
	//// attachments variables
 //   public List<DocRowRec> optInlocalDocList {get ; set;} //The document selected from local hard driver 
 //   public List<attachment> attachmentsFromEmailTemplate {get;set;} // Attachments from selected email template
 //   public List<Id> deleteDummyDocIds;
 //   public String dellocalDocId{get;set;}
 //   public String docsId {get; set;}  
 //   public Contact tempCand = new Contact();   //add for set the local doc to candidate on 20/09/12
 //   public static final Integer TotalDocsSize = 9437184; //Total size is 9 MB
 //   public String erroroptInlocalDocString {get ; set;}
 //   public Integer totalSize = 0;
 //   public Integer totalAttachmentSize = 0;
 //   public Integer totalDocumentSize = 0;
 //   public List<String> allAttachmentIds;
 //   public List<String> allDocumentIds;
   
 //   public MassUpdateVCWController(ApexPages.StandardSetController controller) {
 //   	records = new List<Placement_Candidate__c>();
 //   	u = usercls.getUserDetail();
 //       thesamplepc = new Placement_Candidate__c();
 //       vid = ApexPages.currentPage().getParameters().get('id');
 //       cmids = ApexPages.currentPage().getParameters().get('cmids');
 //       currentpage = ApexPages.currentPage().getParameters().get('cp');
 //       currentSelectCrieria = ApexPages.currentPage().getParameters().get('sc');
 //       currentAscSelectCrietria = ApexPages.currentPage().getParameters().get('asc');
 //       cmssource = ApexPages.currentPage().getParameters().get('type');
 //       displaytemplate = false;
 //       use_sign = false;
 //       is_test = false;
 //       selectlink = '';
 //       displayname = (UserInfo.getFirstName()== null ? '' : (UserInfo.getFirstName() + ' ')) +UserInfo.getLastName()+'('+UserInfo.getOrganizationName()+')';
 //       replyto = u.Email;
        
 //       //Init attachment file function
 //       optInlocalDocList = new List<DocRowRec>(); 
 //       attachmentsFromEmailTemplate = new List<attachment>();
 //       docsId = '';
 //       erroroptInlocalDocString = '';
 //       //End attachment files function
        
 //       //init add email template function by Jack on 21 Sep 2012
 //       newSendResumeEmailTemplate = new SendResumeEmailTemplate();
 //       newSendResumeEmailTemplate.initEmailTemplate();
 //       this.newSendResTem = newSendResumeEmailTemplate.newSendResTem;
 //       newSendResumeEmailTemplate = null;
 //       //init the Send_Resume_Blank_Template
 //       sendResumeEmailTemplate = DaoEmailTemplate.getInitTemplatesByName(massUpdateTemplate);
 //       selectedTempId = sendResumeEmailTemplate.id;
 //       previewTemp();
 //       //End add email template function        
        
 //       try{
 //           //Set return back URL and type
 //           if(cmids!=null){
 //              vids = new List<Id>();
 //              vids = cmids.split(',');
               
 //              commandlinkaction = cmssource;
 //              //commandlinkvalue = '<<< Back to Show All Page!';
 //              CommonSelector.checkRead(Placement_Candidate__c.sObjectType,'Id, Name, Status__c, Candidate__c');
 //              records = [select Id, Name, Status__c, Candidate__c from Placement_Candidate__c where id in:vids];
 //           } 
 //           else{  
 //           	if(test.isRunningTest()){
	//				records = (List<Placement_Candidate__c>)controller.getRecords();
	//			} 
	//			else{             
 //               	records = (List<Placement_Candidate__c>)controller.getSelected(); 
	//			}
 //               commandlinkaction = 'backToVacancy';
 //               commandlinkvalue = '<<< Back to Vacancy!';                               
 //           }
 //           if(records.size()<1){
 //           	customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CANDIDATE_SELECTED);
 //           }
 //           else{
 //           	CommonSelector.checkRead(Placement_Candidate__c.sObjectType,'Id,Name, Candidate__c,Placement__c,Candidate_Source1__c, Candidate__r.Email ,Candidate__r.Name, Candidate_Status__c,Status__c, Candidate__r.EmailBouncedReason');
	//            allvcw = [select Id,Name, Candidate__c,Placement__c,Candidate_Source1__c, Candidate__r.Email ,Candidate__r.Name, Candidate_Status__c,Status__c, Candidate__r.EmailBouncedReason
	//                    from Placement_Candidate__c where id in:records and Candidate__c != null];
	//			//system.debug('allvcw = ' + allvcw);
	//			if(allvcw.size()==0){
	//				allvcw = null;
	//				customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CANDIDATE_IN_SELECTED);
	//			}				
 //           }
 //           msg = '';          
 //           //** org emails if available            
 //           orgEmails.add(new SelectOption('', '-- Please Select Org Wide Email Address --'));
 //           CommonSelector.checkRead(OrgWideEmailAddress.sObjectType,'IsAllowAllProfiles, Id, DisplayName, Address');
 //           for(OrgWideEmailAddress oe : [Select o.IsAllowAllProfiles, o.Id, o.DisplayName, o.Address From OrgWideEmailAddress o 
 //                where o.IsAllowAllProfiles = true order by o.DisplayName limit 500])
 //           {
 //               orgEmails.add(new SelectOption(oe.Id, oe.DisplayName + '(' +oe.Address+')'));
 //           }
 //           hasorgemail = false;
 //           if(orgEmails.size()>1)
 //           {
 //               hasorgemail = true;
 //           }            
 //       }
 //       catch(system.Exception e)
 //       {
 //           msg = '<font color="red"><b>Page Loaded Unsuccessfully!</b></font>';    
 //       }
 //   }
    
 //   @future
 //   public static void delOldTemplate(){
 //       String delTempNam='QSRTEMP_'+UserInfo.getUserId();
 //       List<EmailTemplate> temp=DaoEmailTemplate.getOldTemplateLikeDevName(delTempNam);
 //       if(temp != null && temp.size()>0){
 //       try{
 //           fflib_SecurityUtils.checkObjectIsDeletable(EmailTemplate.SObjectType);
 //           System.debug('delete old Template successful');
 //           delete temp;
	//        }
	//        catch(Exception e){
	//            NotificationClass.notifyErr2Dev('Error key: Send Resume encounter error when deleting old the custom template', e);
	//        }
 //       } 
 //   }      

 //   public String selectedProgress{get;set;}
 //   public String selectedStatus{get;set;}
 //   public integer recordsize{get;set;}
    
 //   //Mass update CMs record
 //   public void updatestatus(){
 //       try{
 //           massupdaterecords = new Placement_Candidate__c[]{};
 //           system.debug('thesamplepc.Candidate_Status__c ='+thesamplepc);
 //           for(Placement_Candidate__c r :allvcw)
 //           {
 //               r.Candidate_Status__c = thesamplepc.Candidate_Status__c;
 //               r.Status__c = thesamplepc.Status__c;
 //               massupdaterecords.add(r);
 //           }
 //           if(massupdaterecords.size() >0)
 //           {
 //               TriggerHelper.SyncTimeSheetApproverEnable = false;
 //               TriggerHelper.UpdateReminingEnable = false;
 //               //TriggerHelper.CandidatPlaced = false;
 //               //check FLS
 //               List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
 //                       Placement_Candidate__c.Candidate_Status__c,
 //                       Placement_Candidate__c.Status__c
 //               };
 //               fflib_SecurityUtils.checkUpdate(Placement_Candidate__c.SObjectType, fieldList); 
 //               update massupdaterecords;
 //               customerErrorMessage(PeopleCloudErrorInfo.CONFIRM_CMS_UPDATE_SUCC);
 //           }
 //           else if(massupdaterecords.size() == 0){
 //           	customerErrorMessage(PeopleCloudErrorInfo.WARN_CMS_UPDATE_FAIL);
 //           }
 //       }
 //       catch(system.Exception e)
 //       {
 //       	customerErrorMessage(PeopleCloudErrorInfo.WARN_CMS_UPDATE_FAIL);
 //       }
 //   }
    
 //   public PageReference updateAndReturn(){
 //       updatestatus();
 //       return backToShowAll();
 //   }
    
 //   public PageReference backToShowAll(){
 //   	returnUrl = '';
 //   	triggerParentPage = false;
 //   	//system.debug('selectlink = ' + commandlinkaction );
 //       PageReference showallpage = new PageReference('/'+ vid);
 //       if(commandlinkaction == 'backToShowAll'){
 //           showallpage = page.IframeParentStandard;
 //           showallpage.setRedirect(true);
 //           showallpage.getParameters().put('sc',currentSelectCrieria);
 //           showallpage.getParameters().put('cp',currentpage);
 //           showallpage.getParameters().put('id',vid);
 //           showallpage.getParameters().put('asc',currentAscSelectCrietria);
 //           }
 //       returnUrl = showallpage.getURL();
 //       //system.debug('selectlink1 = ' + returnUrl );
 //       triggerParentPage = true;
 //       return null;
 //   }       
    
 //   public PageReference sendemails(){
 //   	String tempreplyto = '';
 //   	String tempdisplayname = '';    	
 //       //system.debug('template =' + sendOutTemp);
 //       if(sendOutTemp==null){
 //           customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_TEMPLATE);
 //           delCustomTemplate();
 //           return null;
 //       }
        
 //       List<Messaging.Emailfileattachment> attachments = new List<Messaging.Emailfileattachment>();
 //       if(attachmentsFromEmailTemplate!=null&&attachmentsFromEmailTemplate.size()>0){
 //       	List<attachment> attachmentsFromEmailTemplateWithBody = new List<attachment>();
 //       	attachmentsFromEmailTemplateWithBody = DaoAttachment.getAttachmentsDetailByAttachment(attachmentsFromEmailTemplate); 
 //       	for(attachment tempatta : attachmentsFromEmailTemplateWithBody){
 //       		Messaging.Emailfileattachment attachment= new Messaging.Emailfileattachment();
 //       		Blob b=tempatta.Body;
 //       		if(b==null){
	//                customerErrorMessage(PeopleCloudErrorInfo.ERR_NULL_EXT_DOC);
	//                if(!Test.isRunningTest())
	//                return null;
	//            }
	//            else{
	//	            attachment.setFileName(tempatta.Name);
	//	            attachment.setInline(false);
	//	            attachment.setBody(b);
	//	            attachments.add(attachment);
	//            }
 //       	}
 //       }
        
 //       if(optInlocalDocList!=null&&optInlocalDocList.size()>0){
	//        for(DocRowRec ld: optInlocalDocList){
	//            Messaging.Emailfileattachment attachment= new Messaging.Emailfileattachment();
	//            String fileExt= ld.locdoc.type;
	            
	//            Blob c=S3Documents.getlocalDoc(ld.locdoc.id);
	//            if(c==null){
	//                customerErrorMessage(PeopleCloudErrorInfo.ERR_NULL_EXT_LOCDOC);
	//                if(!Test.isRunningTest())
	//                return null;
	//            }
	//            else{
	//	            system.debug('get blob content size:'+c.size());
	//	            system.debug('file extension:'+fileExt);
	//	            attachment.setFileName(ld.locdoc.Name);
	//	            attachment.setInline(false);
	//	            attachment.setContentType(SendMailService.getContentType(fileExt));
	//	            attachment.setBody(c);
	//	            attachments.add(attachment);
	//	            //system.debug('ld.tempcandidate =' + ld.tempcandidate);
	//            }
	//        }
 //       }
 //       List<Placement_Candidate__c> peoplewithoutEmail = new List<Placement_Candidate__c>();
 //       List<id> tempVacancy = new List<id>();
 //       String peoplewithoutEmailString = null;
 //       List<Placement_Candidate__c> peoplewithEmail = new List<Placement_Candidate__c>();
 //       boolean has_org_mail = false;       
 //       try{
 //       	if(allvcw.size()>0&&allvcw != null){
	//            for(Placement_Candidate__c pc : allvcw)
	//            {
	//            	//system.debug('pc.Candidate__r.Email = '+ pc.Candidate__r.Email);
	//                if(pc.Candidate__r.Email != null && pc.Candidate__r.Email != '' && pc.Candidate__r.EmailBouncedReason == null )//If the Email has been bounced, the email address will not add to the list modify by Jack
	//                {
	//                	peoplewithEmail.add(pc);
	//                	if(pc.Placement__c!=null)
	//                	tempVacancy.add(pc.Placement__c);                	
	//                }
	//                else
	//                {
	//                    peoplewithoutEmail.add(pc);
	//                }
	//            }//for
 //       	}
 //       	else{//no candidate has been selected, error pop up, delete temp email template and return null
 //       		customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CANDIDATE_SELECTED);
 //       		delCustomTemplate();
	//	        return null;
 //       	}
 //           //system.debug('SelectedOrgEmail =' + SelectedOrgEmail);
 //           if(SelectedOrgEmail != null && SelectedOrgEmail != ''){
 //           	has_org_mail = true;
            	
 //           }   
 //          	else{
 //               SelectedOrgEmail = null;
 //               has_org_mail = false;
 //               if(replyto != null){ //replyto validation check
	//	        	String emailPattern ='([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
	//	        	Boolean validEmail = Pattern.matches(emailPattern, replyto);
	//	        	if(!validEmail){
	//	        		customerErrorMessage(PeopleCloudErrorInfo.ERR_WRONG_REPLYTO_ADDRESS);
	//	        		return null;
	//	        	}
	//	        }
 //           }
            
 //           //Set these two fields value for Send Email
 //           if(has_org_mail){
 //           	tempreplyto = null;
 //           	tempdisplayname = null;                
 //           }
 //           else{
 //           	tempreplyto = replyto;
 //           	tempdisplayname = displayname;            	
 //           }
            
 //           if(peoplewithEmail.size()>0){
 //           	String sendmsg='';
 //          		Integer sendMailStatus=SendMailService.sendMailWithTemplateFromMUD(peoplewithEmail,attachments, SelectedOrgEmail, tempreplyto, tempdisplayname, use_sign,sendOutTemp.Id);
 //          		if(sendMailStatus == SendMailService.SEND_SUCC && peoplewithoutEmail.size()<1){
 //          			customerErrorMessage(PeopleCloudErrorInfo.MASS_UPDATE_CONFIRM_SEND_MAIL_SUCC);
 //          		}
 //          		else if(sendMailStatus == SendMailService.SEND_SUCC_PARTIAL){
	//	        	customerErrorMessage(PeopleCloudErrorInfo.WARN_SEND_MAIL_SUCC);
	//	        }
	//	        else if(sendMailStatus == SendMailService.SEND_FAIL){
	//            	customerErrorMessage(PeopleCloudErrorInfo.ERR_SEND_MAIL_FAIL);
	//	        }
	//	        else if(peoplewithoutEmail.size()>0){
	//	        	for(Placement_Candidate__c c: peoplewithoutEmail){
	//                    sendmsg += c.Candidate__r.Name + ';' ;
	//                }
	//                PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(PeopleCloudErrorInfo.WARN_PART_CANDIDATE_NO_EMAIL,sendmsg,true);					
	//				msgSignalhasmessage = errormesg.returnresult;
	//				for(ApexPages.Message newMessage:errormesg.errorMessage){
	//					ApexPages.addMessage(newMessage);
	//				}
	//		    }			    
	//			//logActivity(peoplewithEmail, tempVacancy);
	//        }
 //           else{//if no selected candiates contains email return error 
 //           	customerErrorMessage(PeopleCloudErrorInfo.WARN_CMS_SEND_FAIL);
	//            delCustomTemplate();
	//            return null;
 //           } 
 //       }
 //       catch(system.Exception e)
 //       {
 //           String sendmsg = '<font color="red">Unsuccessfully send emails to ALL those candidates! System Exception: '+e.getMessage()+'</font>';
 //           String err_msg = 'System Exception for record creation! <br/>';
 //           err_msg += 'Detail: ' + e.getMessage() + '('+e.getTypeName() +')';
 //           err_msg += '<br/>';
 //           err_msg += 'User Information: '+ UserInfo.getName() +'<br/>';
 //           notificationsendout(err_msg,'systemexception','MassUpdateVCWController', 'sendemails'); 
 //       }
 //       return null;
 //   }
    
 //   //Change Email Template Content   
 //   public PageReference previewTemp(){
 //       selectedTemp = DaoEmailTemplate.getTemplateById(selectedTempId);
 //       //system.debug('selected temp:'+selectedTemp);
        
 //       // get all default attachments related to the selected email template
 //       if(selectedTempId != null){
 //           attachmentsFromEmailTemplate = DaoAttachment.getAttachmentsByEmailTemplateId(selectedTempId);
 //           //system.debug('inside previewTemp(), attachmentsFromEmailTemplate = ' + attachmentsFromEmailTemplate );
            
 //           if(isOversized()){
 //               attachmentsFromEmailTemplate.clear();
 //               msgSignalhasmessage = true;
 //               PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(PeopleCloudErrorInfo.WARN_UPLOAD_ATTACHMENT_FAIL,true);                   
 //               for(ApexPages.Message newMessage:errormesg.errorMessage){
 //                   ApexPages.addMessage(newMessage);
 //               }
 //           return null;
 //           }
 //       }
        
 //       if(selectedTempId==null){
 //           htmlTemplateContent='';
 //           return null;
 //       }else if(selectedTemp!=null&&selectedTemp.HtmlValue != null && selectedTemp.HtmlValue.length()>0){
 //           selectedTempName = selectedTemp.Name;  
 //           customEmailSubject=selectedTemp.Subject;
 //           htmlTemplateContent = selectedTemp.HtmlValue;
 //           htmlTemplateContent=htmlTemplateContent.replace('<![CDATA[', '<!-- PEOPLECLOUD_TMP_TEMPLATE_START -->');
 //           htmlTemplateContent=htmlTemplateContent.replace(']]>', '<!-- PEOPLECLOUD_TMP_TEMPLATE_END -->');
 //       }else{
 //       	selectedTempName = selectedTemp.Name;
 //       	customEmailSubject=selectedTemp.Subject;
 //           htmlTemplateContent = selectedTemp.Body;
 //           htmlTemplateContent = htmlTemplateContent.replace('\n', '<br>');
 //       }
        
 //       if(!isCustomTempRichText){
 //               //system.debug('isCustomTempRichText1 ='+ htmlTemplateContent);
 //               customEmailContent = htmlTemplateContent.replace('<br>','\n');
 //               sendResumeEmailTemplate = DaoEmailTemplate.getInitTemplatesByName(massUpdateTemplate);
 //               htmlTemplateContent=sendResumeEmailTemplate.HtmlValue;
 //               htmlTemplateContent=htmlTemplateContent.replace('<![CDATA[', '<!-- PEOPLECLOUD_TMP_TEMPLATE_START -->');
 //           htmlTemplateContent=htmlTemplateContent.replace(']]>', '<!-- PEOPLECLOUD_TMP_TEMPLATE_END -->');
 //               customEmailFrameBottom = getCustomEmailFrameBottom(htmlTemplateContent); 
 //               customEmailFrameTop = getCustomEmailFrameTop(htmlTemplateContent);
 //               }
 //       else{
 //               system.debug('isCustomTempRichText2 ='+ isCustomTempRichText);                  
 //               customEmailFrameBottom = getCustomEmailFrameBottom(htmlTemplateContent); 
 //               customEmailFrameTop = getCustomEmailFrameTop(htmlTemplateContent);
 //               customEmailContent = getCustomEmailBody(htmlTemplateContent);
 //               //system.debug('frame bottom:'+customEmailFrameBottom);
 //               //system.debug('frame top'+customEmailFrameTop);
 //               //system.debug('content:'+customEmailContent);
 //           } 
 //       return null;
 //   }
    
 //   public Boolean isCustomTempRichText{
 //       get{
 //           return selectedTemp != null && selectedTemp.HtmlValue != null && selectedTemp.HtmlValue.length()!=0;
 //       }
 //   }
    
 //   //set temp template body, header, footer for text email template
 //   private String getCustomEmailFrameBottom(String content){
 //       Integer endIndex = content.lastIndexOf('<!-- PEOPLECLOUD_TMP_TEMPLATE_END -->');
 //       return content.substring(endIndex+'<!-- PEOPLECLOUD_TMP_TEMPLATE_END -->'.length(),content.length());
 //   }
    
 //    private String getCustomEmailFrameTop(String content){
 //       Integer startIndex = content.lastIndexOf('<!-- PEOPLECLOUD_TMP_TEMPLATE_START -->');
 //       return content.substring(0,startIndex);
 //   }
    
 //   private String getCustomEmailBody(String content){
 //       // calculate frame start, end mark index
 //       Integer fStartIndex = content.lastIndexOf('<!-- PEOPLECLOUD_TMP_TEMPLATE_START -->');
 //       Integer fEndIndex = content.lastIndexOf('<!-- PEOPLECLOUD_TMP_TEMPLATE_END -->');
 //       // calculate body start,end index
 //       Integer startIndex = fStartIndex+'<!-- PEOPLECLOUD_TMP_TEMPLATE_START -->'.length();
 //       return content.substring(startIndex, fEndIndex);        
 //   }  
    
 //   //create temp email template
 //   public PageReference createCustomTemplate(){
 //       // create a custom email template
 //       sendOutTemp = null;
 //       String startTimeStamp = ''+Datetime.now().getTime();
 //       String runTimeStamp = ''+Datetime.now().getTime();
 //       EmailTemplate customTemplate = new EmailTemplate();
        
 //       String tmpContent = customEmailFrameTop +'<![CDATA['
 //               +customEmailContent
 //               +']]>'+customEmailFrameBottom;
 //       //system.debug('custom email content:'+tmpContent);
 //       customTemplate.HtmlValue = tmpContent;
 //       customTemplate.TemplateStyle = 'freeForm';
 //       customTemplate.TemplateType = 'html';
        
 //       if(isCustomTempRichText){
 //           customTemplate.BrandTemplateId = selectedTemp.BrandTemplateId;
 //           customTemplate.Subject = selectedTemp.Subject;
 //           customTemplate.Subject = customEmailSubject;
 //       }else{
 //           customTemplate.BrandTemplateId = sendResumeEmailTemplate.BrandTemplateId;
 //           customTemplate.Subject = selectedTemp.Subject;
 //           customTemplate.Subject = customEmailSubject;
 //       }
 //       customTemplate.DeveloperName = 'QSRTEMP_'+UserInfo.getUserId()+'_'+startTimeStamp+'_'+runTimeStamp;
 //       customTemplate.Name = 'QSRTEMP_'+UserInfo.getUserId()+'_'+startTimeStamp+'_'+runTimeStamp;
 //       customTemplate.Encoding = 'ISO-8859-1';
 //       customTemplate.IsActive = true;
 //       customTemplate.FolderId = Userinfo.getUserId();
 //       if(!Test.isRunningTest()){
 //           List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
 //               EmailTemplate.HtmlValue,
 //               EmailTemplate.TemplateStyle,
 //               EmailTemplate.TemplateType,
 //               EmailTemplate.BrandTemplateId,
 //               EmailTemplate.Subject,
 //               EmailTemplate.DeveloperName,
 //               EmailTemplate.Name,
 //               EmailTemplate.Encoding,
 //               EmailTemplate.IsActive,
 //               EmailTemplate.FolderId
 //           };
 //           fflib_SecurityUtils.checkInsert(EmailTemplate.SObjectType, fieldList);
 //       }
 //       insert  customTemplate;
 //       system.debug('custom temp id:'+customTemplate.Id);
 //       sendOutTemp = customTemplate;
 //       return null;
 //   }  
    
 //   //delete temp email template once error happen
 //   public PageReference delCustomTemplate(){
 //       String delTempNam='QSRTEMP_'+UserInfo.getUserId();
 //       List<EmailTemplate> temp=DaoEmailTemplate.getTemplateLikeDevName(delTempNam);
 //       try{
 //           fflib_SecurityUtils.checkObjectIsDeletable(EmailTemplate.SObjectType);
 //           delete temp;
 //           sendOutTemp = null;
 //       }catch(Exception e){
 //           NotificationClass.notifyErr2Dev('Error key: Send Resume encounter error when deleting the custom template', e);
 //       }
        
 //       return null;
 //   }
    
 //   //reset email content
 //   public PageReference resetEmailContent(){
 //   	system.debug('selectedTempId = '+ selectedTempId);
 //       previewTemp();
 //       return null;
 //   }
    
 //   //Show the error message in the page
 //   public void customerErrorMessage(Integer msgcode){
 //       errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
 //               msgSignalhasmessage = errormesg.returnresult;
 //               for(ApexPages.Message newMessage:errormesg.errorMessage){
 //                       ApexPages.addMessage(newMessage);
 //               }               
 //   }
    
 //   //Delete the Dummy docs according the user action    
 //   public PageReference removeDummyDoc(){
 //       deleteDummyDocIds = new List<Id>();
 //       Integer i=0;
 //       List<Integer> indexList = new List<Integer>();
 //       for(DocRowRec docTmp:optInlocalDocList){
 //           if(docTmp.locdoc.Id == dellocalDocId){
 //               deleteDummyDocIds.add(dellocalDocId);
 //               break;
 //           }
 //           i++;
 //       }  
 //       deleteDummyDoc(deleteDummyDocIds);      
 //       optInlocalDocList.remove(i);      
 //       system.debug('opt in doc size:'+optInlocalDocList.size());
 //       return null;
 //   }
    
    
 //   public PageReference deleteDummyDoc(List<Id> deldummyIds){
 //       Document deleteddoc = new Document();
 //       List<Document> docsToDel = new List<Document>();
 //       for(Id delid: deldummyIds){
 //               deleteddoc = DaoDocument.getSendResumeDummyDoc(delid);
 //               docsToDel.add(deleteddoc);
 //       }
 //       fflib_SecurityUtils.checkObjectIsDeletable(Document.SObjectType);
 //       delete docsToDel;
 //       return null;
 //   }      
    
 //   //Receive the Local dummy documents id and put to optInlocalDocList
 //   public PageReference addlocaldocs(){ 
 //       ApexPages.getMessages().clear();    
 //       system.debug('docsId =' + docsId);

 //       DocRowRec dummyDocRowRec = null;        
 //       if(docsId != null && docsId!=''){
 //           List<String> localdocs = docsId.split(';');
 //           String docid = '';
 //           Integer i=0;
 //           String argKey='arg'+i;
 //           for(String doc : localdocs){
 //               doc.contains(argKey);
 //               docid = doc.substring(doc.indexof('=')+1,doc.length());   
 //               dummyDocRowRec = new DocRowRec((DaoDocument.getSendResumeDummyDoc(docid)),Userinfo.getUserId()) ;               
 //               Integer temtotaldocssize = sizeOfDocs();
 //               temtotaldocssize += dummyDocRowRec.locdoc.bodylength;
 //               if(temtotaldocssize <TotalDocsSize){
 //                   optInlocalDocList.add(dummyDocRowRec);
 //               }
 //               else{
 //                   customerErrorMessage(PeopleCloudErrorInfo.ERR_ATTACHMENT_TO_LARGE);
 //                   return null;
 //               }
 //           }       
 //       }        
 //       if(erroroptInlocalDocString != null && erroroptInlocalDocString != ''){
 //           List<String> errorlocaldocs = erroroptInlocalDocString.split(';');
 //           for(String errorlocaldoc : errorlocaldocs){
 //               PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(PeopleCloudErrorInfo.WARN_UPLOAD_DOCUMENT_FAIL,errorlocaldoc,true);                   
 //               msgSignalhasmessage = errormesg.returnresult;
 //               for(ApexPages.Message newMessage:errormesg.errorMessage){
 //                   ApexPages.addMessage(newMessage);
 //               }
 //           }
 //       }
 //       //system.debug('optInlocalDocList =' + optInlocalDocList);
 //       return null;
 //   }
    
 //   private Integer sizeOfDocs(){
 //       Integer filesSizeSum=0;
        
 //       if(optInlocalDocList != null && optInlocalDocList.size()>0){
 //           for(DocRowRec ldsize: optInlocalDocList){
 //               filesSizeSum += ldsize.locdoc.BodyLength;
 //           }
 //       } 
 //       return filesSizeSum;
 //   }
    
 //   // created by Jack
 //   // this method aims to get all ids of attachments
 //   // also it aims to get the total size of attachments
 //   public List<String> getAllAttachmentsId(){
 //       List<String>    ids = new List<String>();
 //       Integer         size = 0;
        
 //       if(attachmentsFromEmailTemplate != null){
 //           // attachments from selected email template
 //           for(Attachment att:attachmentsFromEmailTemplate){
 //               ids.add(att.Id);
 //               size = size + att.BodyLength;
 //           }               
 //       }       
 //       totalAttachmentSize = size;
 //       return ids;   
 //   }
    
 //   // created by Jack
 //   // this method aims to get all ids of documents
 //   // also it aims to get the total size of documents
 //   public List<String> getAllDocumentsId(){
 //       List<String>    ids = new List<String>();
 //       Integer         size = 0;
        
 //       if(optInlocalDocList != null && optInlocalDocList.size() != 0){
 //           // local documents
 //           for(DocRowRec docTmp:optInlocalDocList){
 //               ids.add(docTmp.locdoc.Id);
 //               size = size + docTmp.locdoc.BodyLength;
 //           }
 //       }       
 //       totalDocumentSize = size;
 //       return ids;   
 //   }
    
    
 //   // created by Jack
 //   // this methods aims to get the total size of attachments and documents
 //   public Integer getTotalSize(){
 //       return totalAttachmentSize + totalDocumentSize;
 //   }
    
 //   //send error message
	//static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){    
 //       NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
 //       UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());
 //   }	
    
 //   // Add by Jack
 //   // this methods aims to check whether all attachments from selected email template are over sized.
 //   public Boolean isOversized(){
 //       if(attachmentsFromEmailTemplate == null || attachmentsFromEmailTemplate.size() == 0) {
 //           return false;
 //       }
        
 //       Integer tmpTotalSize = 0; 
 //       for(Attachment att: attachmentsFromEmailTemplate){
 //           tmpTotalSize = tmpTotalSize + att.BodyLength;
 //           if(tmpTotalSize > 3000000 || att.BodyLength > 2000000){
 //               return true;
 //           }   
 //       }
 //       return false;
 //   }  
    
 //   // created by Jack
 //   // check if there is any attachment from the selected email template
 //   public Boolean isAttachmentsFromEmailTemplateEmpty{
 //       get{
 //           return attachmentsFromEmailTemplate == null || attachmentsFromEmailTemplate.size()==0;
 //       }
 //   }
    
 //   // created by Jack
 //   // check if there is any attachment (either local document or default attachment from email template)
 //   public Boolean isAttachmentsListEmpty{
 //       get{
 //           return (attachmentsFromEmailTemplate == null || attachmentsFromEmailTemplate.size()==0) && (optInlocalDocList == null || optInlocalDocList.size()==0);
 //       }
 //   } 
    
 //   public Boolean isLocalDocListEmpty{
 //       get{
 //           return optInlocalDocList == null || optInlocalDocList.size()==0;
 //       }
 //   }   
}