public with sharing class XMLWriterShortCuts {
	//used to write XML format: <tagname elementName1=value1 elementName2=value2> value3</tagname>
	public void writeXML2ElementsAndValues(XMLStreamWriter writer,String tagName, String elementName1,String value1,
	String elementName2,String value2,String feedValue){
		if(feedValue!=null && feedValue!='' && feedValue!='null'){
			//The XMLStreamWriter will automatically encode the character, so there is no need for encoding.
			writer.writeStartElement(null,tagName,null);
			writer.writeAttribute(null,null,elementName1,value1);
			writer.writeAttribute(null,null,elementName2,value2);
			writer.writeCharacters(feedValue);
			writer.writeEndElement();
		}
	}
	//used for XML whose format is <tagname elementName1=value elementName2=value2 elementName3=value3 elementName4=value4 />
	public void writeXML4Elements(XMLStreamWriter writer,String tagName,
				String elementName1,String value1,
				String elementName2,String value2,
				String elementName3,String value3,
				String elementName4,String value4){
			if(value1==null || value1=='null'){
				value1='';
			}
			if(value2==null || value2=='null'){
				value2='';
			}
			if(value3==null || value3=='null'){
				value3='';
			}
			if(value4==null || value4=='null'){
				value4='';
			}						
			writer.writeEmptyElement(null,tagName,null);
			writer.writeAttribute(null,null,elementName1,value1);
			writer.writeAttribute(null,null,elementName2,value2);
			writer.writeAttribute(null,null,elementName3,value3);
			writer.writeAttribute(null,null,elementName4,value4);
	}
	
	
	//used to write XML format: <tagname elementName=value> value2</tagname>
	public void writeXMLChaAndElement(XMLStreamWriter writer,String tagName, String elementName,String value,String feedValue){
			if(value==null || value=='null'){
				value='';
			}
			if(feedValue==null || feedValue=='null'){
				feedValue='';
			}
			
			writer.writeStartElement(null,tagName,null);
			writer.writeAttribute(null,null,elementName,value);
			writer.writeCharacters(feedValue);
			writer.writeEndElement();
	}
	//Used to write String tag: format <tagname> value</tagname>
	public void writeXMLCharacters(XMLStreamWriter writer,String tagName, String value){
		if(value!=null && value!='' && value!='null'){
			writer.writeStartElement(null,tagName,null);
			//value=StringHandler.XMLEncoder(value);
			writer.writeCharacters(value);
			writer.writeEndElement();
		}
	}
	//Used to write String tag: format <tagname> value</tagname>
	public void writeXMLCDataCharacters(XMLStreamWriter writer,String tagName, Object value){
		if(value!=null && value!='' && value!='null'){
			writer.writeStartElement(null,tagName,null);
			//value=StringHandler.XMLEncoder(value);
			writer.writeCData(String.valueOf(value));
			writer.writeEndElement();
		}
	}
	//Used to write String tag: format <tagname> value</tagname>
	public void writeXMLCharacters(XMLStreamWriter writer,String tagName, Object value){
		if(value!=null && value!='' && value!='null'){
			writer.writeStartElement(null,tagName,null);
			//value=StringHandler.XMLEncoder(value);
			writer.writeCharacters(String.valueOf(value));
			writer.writeEndElement();
		}
	}
	//used for XML whose format is <tagname elementName1=value1 element2=value2/>
	public void writeXML2Elements(XMLStreamWriter writer,String tagName,
							 String elementName1,String value1,
							 String elementName2,String value2){
		if(value1==null || value1=='null'){
				value1='';
			}
			if(value2==null || value2=='null'){
				value2='';
			}
		writer.writeEmptyElement(null,tagName,null);
		writer.writeAttribute(null,null,elementName1,value1);
		writer.writeAttribute(null,null,elementName2,value2);
		
	}
	//used for XML whose format is <tagname elementName=value />
	public void writeXMLElement(XMLStreamWriter writer,String tagName, String elementName,String value){
		if(value!=null && value!='' && value!='null'){
			//value=StringHandler.XMLEncoder(value);
			writer.writeEmptyElement(null,tagName,null);
			writer.writeAttribute(null,null,elementName,value);
		}
	}
	
	//used for XML whose format is <tagname elementname1=value1 elementname2=value2 elementname3=value3> </tagname>
	public void writeXML3ElementsAndValue(XMLStreamWriter writer,String tagName,
				String elementName1,String value1,
				String elementName2,String value2,
				String elementName3,String value3,
				String feedValue){
		if(value1==null || value1=='null'){
				value1='';
			}
			if(value2==null || value2=='null'){
				value2='';
			}
			if(value3==null || value3=='null'){
				value3='';
			}
			if(feedValue==null || feedValue=='null'){
				feedValue='';
			}						
		writer.writeStartElement(null,tagName,null);
		writer.writeAttribute(null,null,elementName1,value1);
		writer.writeAttribute(null,null,elementName2,value2);
		writer.writeAttribute(null,null,elementName3,value3);			
		writer.writeCharacters(feedValue);			
		writer.writeEndElement();		
		
		
		}
	//Used to write XMl whose format is <tagname elementname1=value1 elementname2=value2 elementname3=value3 />
	public void writeXML3Elements(XMLStreamWriter writer,String tagName,
				String elementName1,String value1,
				String elementName2,String value2,
				String elementName3,String value3){
			if(value1==null || value1=='null'){
				value1='';
			}
			if(value2==null || value2=='null'){
				value2='';
			}
			if(value3==null || value3=='null'){
				value3='';
			}
		writer.writeEmptyElement(null,tagName,null);
		writer.writeAttribute(null,null,elementName1,value1);
		writer.writeAttribute(null,null,elementName2,value2);
		writer.writeAttribute(null,null,elementName3,value3);			
		}
	
	//used for XML whose format is 
	//<tagname elementName1=value elementName2=value2 elementName3=value3 elementName4=value4 elementName5=value5 elementName6=value6 />
	public void writeXML6Elements(XMLStreamWriter writer,String tagName,
				String elementName1,String value1,
				String elementName2,String value2,
				String elementName3,String value3,
				String elementName4,String value4,
				String elementName5,String value5,
				String elementName6,String value6){
			if(value1==null || value1=='null'){
				value1='';
			}
			if(value2==null || value2=='null'){
				value2='';
			}
			if(value3==null || value3=='null'){
				value3='';
			}
			if(value4==null || value4=='null'){
				value4='';
			}
			if(value5==null || value5=='null'){
				value5='';
			}
			if(value6==null || value6=='null'){
				value6='';
			}						
			writer.writeEmptyElement(null,tagName,null);
			writer.writeAttribute(null,null,elementName1,value1);
			writer.writeAttribute(null,null,elementName2,value2);
			writer.writeAttribute(null,null,elementName3,value3);
			writer.writeAttribute(null,null,elementName4,value4);
			writer.writeAttribute(null,null,elementName5,value5);
			writer.writeAttribute(null,null,elementName6,value6);
	}
	
		 
	

}