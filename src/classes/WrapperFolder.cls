/*
	Author by Jack
	Folder Wrapper Class for Email Template tree list function
*/

public with sharing class WrapperFolder {
		public Id folderId {get;set;}
		public Folder emailFolder;
		public String folderName {get; set;}
		public List<EmailTemplate> folderEmailTemplate {get; set;}		
		
		public WrapperFolder(Id fid){
			emailFolder = null;	
			folderName = '';
			system.debug('fid = '+ fid + '=' + UserInfo.getUserId());	
			
			if(fid == UserInfo.getUserId()){ //For customer personal folder
				folderId = fid;
				folderName = 'My Personal Email Templates';
			}
			
			else if(fid == UserInfo.getOrganizationId()){ //For company folder
				folderId = fid;
				folderName = 'Unfiled Public Email Templates';				
			}
			else{
				emailFolder = DaoFolder.getFolderByTypeAndId('Email', fid);//Customer Email folder
				if(emailFolder != null){
					folderId = fid;
					folderName = emailFolder.Name;
				}
				else{
					folderId = fid;
					folderName = 'other';
				}
			}
		}	
}