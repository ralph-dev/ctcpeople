/**
 * Created by zoechen on 25/7/17.
 */
@isTest
public with sharing class OAuthServiceDummyRecordCreator {

    public static OAuth_Service__c createOAuthServiceLinkedin() {
        OAuth_Service__c linkedinOAuthSerivce = new OAuth_Service__c();
        linkedinOAuthSerivce.Active__c = true;
        linkedinOAuthSerivce.Name = JobBoardUtils.LINKEDIN;
        linkedinOAuthSerivce.Access_Token_URL__c = 'TEST';
        linkedinOAuthSerivce.Authorization_URL__c = 'TEST';
        linkedinOAuthSerivce.Request_Token_URL__c = 'TEST';
        linkedinOAuthSerivce.recordTypeId = DaoRecordType.linkedinOauthRT.Id;
        checkUpdatableOrInsertableOAuthService();
        insert linkedinOAuthSerivce;
        return linkedinOAuthSerivce;

    }

    private static void checkUpdatableOrInsertableOAuthService(){
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                OAuth_Service__c.Active__c,
                OAuth_Service__c.Access_Token_URL__c,
                OAuth_Service__c.Authorization_URL__c,
                OAuth_Service__c.Name,
                OAuth_Service__c.Request_Token_URL__c
        };
        fflib_SecurityUtils.checkUpdate(OAuth_Service__c.SObjectType, fieldList);

    }
}