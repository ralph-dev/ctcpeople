public with sharing class AdvertisementConfigurationSelector extends CommonSelector{
	

	public AdvertisementConfigurationSelector(){
		super(Advertisement_Configuration__c.SObjectType);
	}
	


	public override List<Schema.SObjectField> getSObjectFieldList(){
		return new List<Schema.SObjectField>{
			Advertisement_Configuration__c.Id,
			Advertisement_Configuration__c.RecordTypeId,
			Advertisement_Configuration__c.OwnerId,
			Advertisement_Configuration__c.Name,
			Advertisement_Configuration__c.Active__c,
			Advertisement_Configuration__c.Advertisement_Account__c,
			
			//commented by andy for security review II
			//Advertisement_Configuration__c.Password__c,
			//Advertisement_Configuration__c.UserName__c,
			
			Advertisement_Configuration__c.Account_Type__c,
            Advertisement_Configuration__c.Indeed_Apply_Job_URL__c,
			Advertisement_Configuration__c.Website__c
		};
	}

	public LIST<String> getIndeedAccountList() {
		return new List<String> {
			    //commented by andy for security review II
				//'Indeed_API_Token__c',
				'Indeed_Apply_Job_URL__c'
		};
	}

	public LIST<String> getSeekAccountList() {
		return new List<String> {
				'Seek_Application_Export_Enabled__c'
		};
	}

	/*
		This class for fetch the indeed account from Advertisement Configuration Selector Object
		@return List<Advertisement_Configuration__c>
	*/
	public List<Advertisement_Configuration__c> getIndeedAccounts() {
		String indeedAccountRTId = DaoRecordType.IndeedAdRT.Id;
		SimpleQueryFactory qf = simpleQuery()
									.selectFields(getSObjectFieldList())
									.selectFields(getIndeedAccountList())
									.setCondition('RecordTypeId =: indeedAccountRTId');
		return Database.query(qf.toSOQL());
	}
	
	public List<ProtectedAccount> getIndeedProtectedAccounts(){
		List<ProtectedAccount> pas = new List<ProtectedAccount>();
		
		List<Advertisement_Configuration__c> acs = getIndeedAccounts(  );
		
		if(acs != null ){
			for(Advertisement_Configuration__c ac : acs){
				ProtectedCredential pc = CredentialSettingService.getCredential(ac.id);
				if(pc != null ){
					 pas.add(new ProtectedAccount(ac, pc));
				}
			}
			
		}
		
		return pas;
	}

	
	public Advertisement_Configuration__c getIndeedAccountById(String indeedAccountId) {
		String indeedAccountRTId = DaoRecordType.IndeedAdRT.Id;
		try {
			SimpleQueryFactory qf = simpleQuery()
										.selectFields(getSObjectFieldList())
										.selectFields(getIndeedAccountList())
										.setCondition('RecordTypeId =: IndeedAccountRTId and Id =: indeedAccountId');
			List<Advertisement_Configuration__c> indeedAccs = Database.query(qf.toSOQL());
			return indeedAccs.get(0);
		} catch (Exception e) {
			System.debug('AdvertisementConfigurationSelector_getIndeedAccountByIds: ' + e);
			return null;
		}
	}
	
	public ProtectedAccount getIndeedProtectedAccountById(String indeedAccountId){
		
		Advertisement_Configuration__c ac = getIndeedAccountById( indeedAccountId );
		
		if(ac != null ){
			ProtectedCredential pc = CredentialSettingService.getCredential(ac.id);
			if(pc != null ){
				return new ProtectedAccount(ac, pc);
			}
		}
		
		return null;
	}
	
	public Advertisement_Configuration__c getJXTAccount() {
		String jxtAccountRT = DaoRecordType.jxtAccountRT.Id;
		try {
			SimpleQueryFactory qf = simpleQuery()
                                        .selectFields(getSObjectFieldList())
										.setCondition('RecordTypeId =: jxtAccountRT');
            qf.getOrderings().clear();
			List<Advertisement_Configuration__c> jxtAccs = Database.query(qf.toSOQL());
			return jxtAccs.get(0);
		}catch(Exception e){
			System.debug('AdvertisementConfigurationSelector_getJXTAccount: ' + e);
			return null;
		}
	}
	
	public ProtectedAccount getJXTProtectedAccount(){
		
		Advertisement_Configuration__c ac = getJXTAccount();
		
		if(ac != null ){
			ProtectedCredential pc = CredentialSettingService.getCredential(ac.id);
			if(pc != null ){
				return new ProtectedAccount(ac, pc);
			}
		}
		
		return null;
	}

	/**
	* Get a list of account by given record type Id
	* @param recordtypeId
	* @return List<Advertisement_Configuration__c>
	* Added by Zoe on 18/07/2017
	*
	*/

	public List<Advertisement_Configuration__c> getAccountByRecordTypeId (Id recordTypeId){
		SimpleQueryFactory qf = simpleQuery()
				.selectFields(getSObjectFieldList())
				.setCondition('RecordTypeId =: recordTypeId');
		return Database.query(qf.toSOQL());
	}

	/**
     * Get a list of account by given record type Id
     * @param ids      list of account for the given record type
     * @return List<Advertisement_Configuration__c>
     * Added by Lina on 09/03/2017
	 */
	public List<Advertisement_Configuration__c> getActiveAccountByRecordTypeId(String recordTypeId) {
		SimpleQueryFactory qf = simpleQuery()
									.selectFields(getSObjectFieldList())
									.selectFields(getSeekAccountList())
									.setCondition('RecordTypeId =: recordTypeId AND Active__c = true');
		return Database.query(qf.toSOQL());
	}

	/**
     * Get a list of account by given ids
     * @param ids      list of account ids
     * @return List<Advertisement_Configuration__c>
     * Added by Lina on 03/03/2017
	 */
	public List<Advertisement_Configuration__c> getActiveAccountByIds(List<Id> ids) {
		SimpleQueryFactory qf = simpleQuery()
									.selectFields(getSObjectFieldList())
									.selectFields(getSeekAccountList())
									.setCondition('id IN: ids AND Active__c = true');
		return Database.query(qf.toSOQL());
	}
	
	public Boolean insertIndeedProtectedAccount(Advertisement_Configuration__c account, String token ){
		
		
		return doInsert(account) && CredentialSettingService.insertIndeedCredential(account.id, token) != null;
		
		
	}
	
	public Boolean insertJXTProtectedAccount(Advertisement_Configuration__c account, String username,String password ){
		
		
		return doInsert(account) && CredentialSettingService.insertJXTCredential(account.id, username,password) != null;
		
		
	}
}