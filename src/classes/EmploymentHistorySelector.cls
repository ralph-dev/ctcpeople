public with sharing class EmploymentHistorySelector extends CommonSelector{

	public EmploymentHistorySelector(){
		super(Employment_History__c.SObjectType);
	}

	/*private Schema.SObjectType getSObjectType(){
		return Employment_History__c.SObjectType;
	}*/

	public override List<Schema.SObjectField> getSObjectFieldList(){
		return new List<Schema.SObjectField>{
			Employment_History__c.Id,
			Employment_History__c.Contact__c
		};
	}

	public List<Employment_History__c> getContactEmploymentHistory(List<Id> conIds){
		SimpleQueryFactory qf = simpleQuery();
		List<Employment_History__c> result = new List<Employment_History__c>();
		String condition = 'Contact__c IN :conIds';
		qf.selectFields(getSObjectFieldList());
		qf.setCondition(condition);
		qf.getOrderings().clear();
		String soqlStr = qf.toSOQL();
		try{
			result = Database.query(soqlStr);
		}catch(Exception e){
			NotificationClass.notifyErr2Dev('SOQL on Employment History fail', e);
			return null;
		}
		return result;
	}
}