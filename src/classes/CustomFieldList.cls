/**
    This function is a middle process. 
    Firstly, you will need to put two paremeters into the Class 
    when initiating the CustomFieldList which are "Object name" and 
    "Field Set name". Please beware of the Namespace prefix which may
    lead to the incorrect result.
    
    In order to pass the Test codes please go to the Develop-> custom setting->Manage PCAppSwitch->Edit
    Fill in the Field Set Name in Advertisement with RevMarketCandiateFieldSet
    Fill in the Object Name for field Set with Contact
**/

public class CustomFieldList {
    private List<Schema.FieldSetMember> field_list=new List<Schema.FieldSetMember>();
    private String objects;
    private Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
    public String getobjects(){
        
        return objects; 
    }
    public String fieldsets;
    public String getfieldsets(){
        
        return fieldsets;
        
    }
    public void setobjects(String objects){
        this.objects=objects;
        
    }
    public void setfieldsets(String fieldsets){
        this.fieldsets=fieldsets;
        
    }
    public CustomFieldList(){
		}
    public CustomFieldList(String objectname, String fieldsetname){
        this.objects=objectname;
        this.fieldsets=fieldsetname;
        
    }
    public map<String,Schema.FieldSet> getCustomFieldSetMap(String objectName){
        	map<String,Schema.FieldSet> fsMap =new map<String,Schema.FieldSet>();
            try{                
                Schema.SObjectType sobjType = gd.get(objectName);  
                Schema.DescribeSObjectResult describesobject = sobjType.getDescribe();
                fsMap= describesobject.fieldSets.getMap();                               
            }catch(Exception e){
                System.debug('Issues in getting fieldset '+e);
            }
        return fsMap;
    }
    public List<Schema.FieldSetMember> getCustomFieldList(){
            
            if(objects==null){
                objects='Advertisement__c';
            }
            field_list=new List<Schema.FieldSetMember>();
            try{
                
                Schema.SObjectType sobjType = gd.get(objects);  
                Schema.DescribeSObjectResult describesobject = sobjType.getDescribe();
                Schema.FieldSet fs = describesobject.fieldSets.getMap().get(fieldsets);
                List<Schema.FieldSetMember> fielda = fs.getFields();
                
                
                field_list=fielda;
                return field_list;
            }catch(Exception e){
                System.debug(e);
                
                return field_list;
                
            }
    }
  

 


}