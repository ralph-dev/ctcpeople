global with sharing class SearchOperator {
	private Boolean active;
	private Boolean defaultValue;
	private String label;
	private String value;

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getDefaultValue() {
		return active;
	}

	public void setDefaultValue(Boolean active) {
		this.active = active;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}