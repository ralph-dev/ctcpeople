@isTest
private class TestQuickSendResume {
    
//    @testSetup static void setUpTestData() {
//        DummyRecordCreator.DefaultDummyRecordSet dms = DummyRecordCreator.createDefaultDummyRecordSet();
//        CandidateManagementDummyRecordCreator cmRecord = new CandidateManagementDummyRecordCreator();
//        system.assertNotEquals(null, cmRecord);
//    }

//    private static testmethod void testContactsQSRSence(){
//    	System.runAs(DummyRecordCreator.platformUser) {
//        // Get all contacts
//        Boolean isAllCandidates = true;
//        List<Contact> allContacts=DaoContact.getAllContacts();
//        Map<Id,RecordType> rtCheck = new Map<Id,RecordType>(DaoRecordType.candidateRTs);
//        // Check if candidate recordtype set is not empty
//        system.assertNotEquals(rtCheck, null);
//        system.assert(rtCheck.keySet().size()>0);
//        for(Contact con:allContacts){
//            if(rtCheck.get(con.RecordTypeId)==null)
//                isAllCandidates = false;
//        }
        
//        // Initialize data extracting page
//        ApexPages.Standardsetcontroller conSetCon= new ApexPages.Standardsetcontroller(allContacts);
//        conSetCon.setSelected(allContacts);
//        ExtractData4QSRController ea4QSRCon = new ExtractData4QSRController(conSetCon);
//        PageReference conPR = ea4QSRCon.initContacts();
//        if(allContacts.size()>0)
//            system.assert(conPR.getParameters().keySet().size()>0);
//        Test.setCurrentPageReference(conPR);
        
//        // Initialize the step 1 context
//        QuickSendResumeController QSRCon = new QuickSendResumeController();
//        QSRCon.step1Init();
//        QSRCon.changeCandDocHelpInfo();
//        system.assert(QSRCon.searchResult.size()>0);
//        system.assert(QSRCon.pagedSearchResult.size()>0);
//        system.assert(!QSRCon.isConSearchResultEmpty);
        
//        // check if initialization process is succeed
//        if(isAllCandidates){
//            system.assertEquals(QSRCon.searchResult.size(), allContacts.size());
//            system.assert(!QSRCon.hasMsg);
//        }else{
//            system.assert(QSRCon.searchResult.size()<allContacts.size());
//            system.assert(QSRCon.hasMsg);
//        }
        
//        // check if pagination function works       
//        if(QSRCon.searchResult.size()==QuickSendResumeController.RECORD_PER_PAGE){
//            system.assertEquals(QSRCon.pagedSearchResult.size(),QuickSendResumeController.RECORD_PER_PAGE);
//            QSRCon.nextPageOfCands();
//            system.assertEquals(QSRCon.pagedSearchResult.size(),QuickSendResumeController.RECORD_PER_PAGE);
//            QSRCon.prevPageOfCands();
//            system.assertEquals(QSRCon.pagedSearchResult.size(),QuickSendResumeController.RECORD_PER_PAGE);
//        }else if(QSRCon.searchResult.size()<QuickSendResumeController.RECORD_PER_PAGE){
//            system.assert(QSRCon.pagedSearchResult.size()<QuickSendResumeController.RECORD_PER_PAGE);
//            QSRCon.nextPageOfCands();
//            system.assertEquals(QSRCon.pagedSearchResult.size(),QSRCon.searchResult.size());
//            QSRCon.prevPageOfCands();
//            system.assertEquals(QSRCon.pagedSearchResult.size(),QSRCon.searchResult.size());
//        }else if(QSRCon.searchResult.size()>QuickSendResumeController.RECORD_PER_PAGE){
//            system.assertEquals(QSRCon.pagedSearchResult.size(),QuickSendResumeController.RECORD_PER_PAGE);
//            Decimal totalNum = QSRCon.searchResult.size();
//            Decimal decTotalPageNum =totalNum / QuickSendResumeController.RECORD_PER_PAGE;
//            Integer totalPages = decTotalPageNum.round(System.RoundingMode.CEILING).intvalue();
//            for(Integer i=1;i<totalPages;i++){
//                system.assertEquals(QSRCon.pagedSearchResult.size(),QuickSendResumeController.RECORD_PER_PAGE);
//                system.assert(QSRCon.hasNextPageOfCands);
//                QSRCon.nextPageOfCands();
//            }
//            system.assert(QSRCon.pagedSearchResult.size()<=QuickSendResumeController.RECORD_PER_PAGE);
            
//            for(Integer i=totalPages;i>1;i--){
//                system.assert(QSRCon.hasPrevPageOfCands);
//                QSRCon.prevPageOfCands();
//                system.assertEquals(QSRCon.pagedSearchResult.size(),QuickSendResumeController.RECORD_PER_PAGE);
//            }           
//        }
        
//        Boolean hasAddResume = false;
//        Integer noOfResumeTry2Add = 0;
//        for(QuickSendResumeController.ConRowRec con:QSRCon.searchResult){
//            // Check if show contact detail
//            QSRCon.selectedConId=con.contactRec.Id;
//            QSRCon.refreshConDetails();
//            // test properties which indidate if a contact record has
//            // been selected to display its detail
//            system.assert(QSRCon.isContactSelected);
            
//            // select resumes
//            system.assert(QSRCon.selectedCon.contactRec!=null);
//            system.assertEquals(QSRCon.selectedCon.contactRec.Id,con.contactRec.Id);
//            for(QuickSendResumeController.DocRowRec doc:con.docList){
//                doc.isSelected=true;
//                noOfResumeTry2Add++;
//                hasAddResume = true;
//            }
//        }
        
//        // Check if add and del resume function is working
//        system.assert(QSRCon.optInDocList.size()==0);
//        system.assert(QSRCon.isResumeListEmpty);
//        QSRCon.add2Attachment();
//        if(hasAddResume){
            
//            if(noOfResumeTry2Add>10){
//                system.assert(QSRCon.hasMsg);
//                system.assert(QSRCon.optInDocList.size()==10);
//            }else{
//                system.assert(!QSRCon.hasMsg);
//                system.assert(QSRCon.optInDocList.size()==noOfResumeTry2Add);
//            }
//            if(noOfResumeTry2Add>2){
//                QSRCon.delDocId = QSRCon.optInDocList.get(0).doc.Id;
//                QSRCon.removeDoc();
//                system.assertNotEquals(QSRCon.delConId,QSRCon.optInDocList.get(0).doc.Id);
//            }
//        }else{
//            system.assert(QSRCon.optInDocList.size()==0);
//        }
        
//        // check search contact function
//        QSRCon.contactName='testCanFirstName';
//        QSRCon.searchContact();
//        system.assert(QSRCon.searchResult.size()>0);
//    	}
//    }



//    private static testmethod void testAccountsQSRSence(){
//    	System.runAs(DummyRecordCreator.platformUser) {
//        List<Account> allAccount = DaoAccount.getAccsContainName('testchildqueriesacct');
//        List<Contact> allContacts=DaoContact.getCandContactsContainName('testCanFirstName');
//        system.assert(allAccount.size()>0);
        
//        // Simulate senece that user select certain accounts to do quick send resume.
//        ApexPages.Standardsetcontroller accSetCon= new ApexPages.Standardsetcontroller(allAccount);
//        accSetCon.setSelected(allAccount);
//        ExtractData4QSRController ea4QSRCon = new ExtractData4QSRController(accSetCon);
//        PageReference accPR=ea4QSRCon.initAccounts();
        
//        // Go to step one
//        Test.setCurrentPage(accPR);
//        QuickSendResumeController QSRCon = new QuickSendResumeController();
//        QSRCon.step1Init();
//        // add doc
//        Contact selectedContact;
//        for(Contact con: allContacts){
//            if(con.Web_Documents__r.size()>0){
//                selectedContact = con;
//                break;
//            }
//        }
//        QuickSendResumeController.ConRowRec tmp = new QuickSendResumeController.ConRowRec(selectedContact);
//        QSRCon.optInDocList.add(tmp.docList.get(0));
//        system.assert(QSRCon.searchResult.size() == 0);
//        system.assert(QSRCon.optInDocList.size() > 0);
//        // Go to step two
//        QSRCon.goNextStep();
//        QSRCon.step2Init();
////      for(QuickSendResumeController.AccRowRec acc:QSRCon.accSearchResult){
////          system.debug('acc name:'+acc.accRec.Name);
////      }
    
//        system.assertEquals(QSRCon.accSearchResult.size(),allAccount.size());
//        system.assert(!QSRCon.isAccSearchResultEmpty);
        
//        // check if pagination function works       
//        if(QSRCon.accSearchResult.size()==QuickSendResumeController.RECORD_PER_PAGE){
//            system.assertEquals(QSRCon.pagedAccSearchResult.size(),QuickSendResumeController.RECORD_PER_PAGE);
//            QSRCon.nextPageOfAccs();
//            system.assertEquals(QSRCon.pagedAccSearchResult.size(),QuickSendResumeController.RECORD_PER_PAGE);
//            QSRCon.prevPageOfAccs();
//            system.assertEquals(QSRCon.pagedAccSearchResult.size(),QuickSendResumeController.RECORD_PER_PAGE);
//        }else if(QSRCon.accSearchResult.size()<QuickSendResumeController.RECORD_PER_PAGE){
//            system.assert(QSRCon.pagedAccSearchResult.size()<QuickSendResumeController.RECORD_PER_PAGE);
//            QSRCon.nextPageOfAccs();
//            system.assertEquals(QSRCon.pagedAccSearchResult.size(),QSRCon.pagedAccSearchResult.size());
//            QSRCon.prevPageOfAccs();
//            system.assertEquals(QSRCon.pagedSearchResult.size(),QSRCon.searchResult.size());
//        }else if(QSRCon.accSearchResult.size()>QuickSendResumeController.RECORD_PER_PAGE){
//            system.assertEquals(QSRCon.pagedAccSearchResult.size(),QuickSendResumeController.RECORD_PER_PAGE);
//            Decimal totalNum = QSRCon.accSearchResult.size();
//            Decimal decTotalPageNum =totalNum / QuickSendResumeController.RECORD_PER_PAGE;
//            Integer totalPages = decTotalPageNum.round(System.RoundingMode.CEILING).intvalue();
//            for(Integer i=1;i<totalPages;i++){
//                system.assertEquals(QSRCon.pagedAccSearchResult.size(),QuickSendResumeController.RECORD_PER_PAGE);
//                system.assert(QSRCon.hasNextPageOfAccs);
//                QSRCon.nextPageOfAccs();
//            }
//            system.assert(QSRCon.pagedAccSearchResult.size()<=QuickSendResumeController.RECORD_PER_PAGE);
            
//            for(Integer i=totalPages;i>1;i--){
//                system.assert(QSRCon.hasPrevPageOfAccs);
//                QSRCon.prevPageOfAccs();
//                system.assertEquals(QSRCon.pagedAccSearchResult.size(),QuickSendResumeController.RECORD_PER_PAGE);
//            }           
//        }
//        // check search contact function
//        QSRCon.accountName='testchildqueriesacct';
//        QSRCon.searchAccs();
//        system.assert(QSRCon.accSearchResult.size()>0);
        
//        // Check if show account detail and contact function is working
//        // Check if add and del resume function is working
//        Boolean hasAddContact = false;
//        Integer noOfContactTry2Add = 0;
//        for(QuickSendResumeController.AccRowRec acc:QSRCon.accSearchResult){
//            QSRCon.selectedAccId=acc.accRec.Id;
//            // Check show account detail function
//            QSRCon.refreshAccDetails();
//            // test properties which indidate if a accound record has
//            // been selected to display its detail
//            system.assert(QSRCon.isAccSelected);
            
//            system.assert(QSRCon.selectedAcc.accRec!=null);
//            system.assertEquals(QSRCon.selectedAcc.accRec.Id,acc.accRec.Id);
//            for(QuickSendResumeController.ConRowRec con:acc.conList){
//                con.isSelected=true;
//                noOfContactTry2Add++;
//                hasAddContact = true;
//            }
//        }
//        system.assert(QSRCon.optInConList.size()==0);
//        system.assert(QSRCon.isRecipientListEmpty);
        
//        // Check add selected contact to recipients function
//        QSRCon.add2Recipients();
//        if(hasAddContact){
//            if(noOfContactTry2Add>0){
//                system.assert(!QSRCon.hasMsg);
//                system.assert(QSRCon.optInConList.size()==noOfContactTry2Add);
//            }else{
//                system.assert(!QSRCon.hasMsg);
//                system.assert(QSRCon.optInConList.size()==0);
//            }
            
//            // Check moving from different step won't
//            // remove recipients
//            QSRCon.goPreviousStep();
//            QSRCon.goNextStep();
//            system.assert(QSRCon.optInConList.size()==noOfContactTry2Add);
            
//            if(noOfContactTry2Add>2){
//                QSRCon.delConId = QSRCon.optInConList.get(0).contactRec.Id;
//                QSRCon.removeCon();
//                system.assertNotEquals(QSRCon.delConId,QSRCon.optInConList.get(0).contactRec.Id);
//            }
//        }else{
//            system.assert(QSRCon.optInConList.size()==0);
//        }
//    	}
                
//    }

//    private static testmethod void testQSRStep3(){
//    	System.runAs(DummyRecordCreator.platformUser) {
//        List<Account> allAccount = DaoAccount.getAccsContainName('testchildqueriesacct');
//        List<Contact> allContacts=DaoContact.getCandContactsContainName('testCanFirstName');
//        // Initialize conext
//        Test.setCurrentPage(Page.QuickSendResumeStepOne);
//        QuickSendResumeController QSRCon = new QuickSendResumeController();
//        QSRCon.TestingMode=true;
//        // Go to step one
//        QSRCon.step1Init();
//        // add doc
//        Contact selectedContact;
//        for(Contact con: allContacts){
//            if(con.Web_Documents__r.size()>0){
//                selectedContact = con;
//                break;
//            }
//        }
//        QuickSendResumeController.ConRowRec tmp = new QuickSendResumeController.ConRowRec(selectedContact);
//        QSRCon.optInDocList.add(tmp.docList.get(0));
//        system.assert(QSRCon.searchResult.size() == 0);
//        system.assert(QSRCon.optInDocList.size() > 0);
//        system.assert(QSRCon.searchResult.size() == 0);
        
//        // Go to step two
//        QSRCon.goNextStep();
//        QSRCon.step2Init();
//        Account selectedAcc;
//        for(Account acc:allAccount){
//            if(acc.Contacts.size()>0){
//                selectedAcc = acc;
//                break;
//            }
//        }
//        QuickSendResumeController.AccRowRec tmpAcc = new QuickSendResumeController.AccRowRec(selectedAcc);
//        QSRCon.optInConList.add(tmpAcc.conList.get(0)); 
//        system.assert(QSRCon.accSearchResult.size() == 0);
//        system.assert(QSRCon.optInConList.size() > 0);
        
//        // Go to step three
//        QSRCon.goNextStep();
//        QSRCon.step3Init();
//        system.assert(QSRCon.emailTempList.size()>0);

//        // Check preview function when no template selected
//        QSRCon.selectedTempId=null;
//        QSRCon.previewTemp();
//        system.assert(QSRCon.htmlTemplateContent!=null);
//        system.assert(QSRCon.htmlTemplateContent.length()==0);

//        // Check preview function when a template selected      
//        QSRCon.selectedTempId = QSRCon.emailTempList.get(0).Id;
//        system.debug('selected temp id:'+QSRCon.emailTempList.get(0).Id);
//        QSRCon.previewTemp();
//        system.assert(QSRCon.htmlTemplateContent!=null);
//        system.assert(QSRCon.htmlTemplateContent.length()>0);
        
//        // Check add additional doc function
//        system.assert(!QSRCon.enableAddDoc);
//        system.assert(QSRCon.folderOptions.size()==0);
//        system.assert(QSRCon.addDocList.size()==0);
//        QSRCon.enableAddDoc = false;
//        QSRCon.toggleAddDocList();
//        QSRCon.enableAddDoc = true;
//        QSRCon.toggleAddDocList();
//        system.assert(QSRCon.enableAddDoc);
//        system.assert(QSRCon.folderOptions.size()>0);
//        //system.assert(QSRCon.addDocList.size()>0);
        
//        QSRCon.changeFolder();
//        system.assertEquals(QSRCon.optInAddDocList.size(), 0);
//        Boolean hasAddResume = false;
//        Integer noOfResumeTry2Add = 0;      
//        for(QuickSendResumeController.SFDocRowRec doc:QSRCon.addDocList){
//            doc.isSelected=true;
//            hasAddResume=true;
//            noOfResumeTry2Add++;
//        }
//        QSRCon.add2AddDocs();
//        system.assertEquals(QSRCon.optInAddDocList.size(),noOfResumeTry2Add);
        
//        if(hasAddResume){
//            QuickSendResumeController.SFDocRowRec addDoc=QSRCon.optInAddDocList.get(0);
//            QSRCon.delAddDocId = addDoc.sfDocRec.Id;
//            QSRCon.removeAddDoc();
//            system.assertEquals(QSRCon.optInAddDocList.size(),noOfResumeTry2Add-1);
//        }
        
//        // try to send email without template selected
//        String templateId=QSRCon.selectedTempId;
//        QSRCon.selectedTempId=null;
//        QSRCon.previewTemp();
//        QSRCon.sendStdTempEmail();
//        QSRCon.sendMail();
//        for(ApexPages.Message ws:QSRCon.warningMsgs){
//            system.debug('warning msg:'+ws.getSummary());
//        }
//        for(ApexPages.Message im:QSRCon.infoMsgs){
//            system.debug('info msg:'+im.getSummary());
//        }
//        for(ApexPages.Message im:QSRCon.errorMsgs){
//            system.debug('err msg:'+im.getSummary());
//        }       
//        for(ApexPages.Message im:QSRCon.confirmMsgs){
//            system.debug('confirm msg:'+im.getSummary());
//        }
//        system.assert(QSRCon.errorMsgs.size()==1);
//        system.assert(QSRCon.hasError);

        
//        // try to send email without recipients
//        EmailTemplate etamplate = DaoEmailTemplate.getTemplateById(templateId); // for testing purpose, no specific aim
//        QSRCon.selectedTempId=etamplate.id;
//        QSRCon.previewTemp();
//        QSRCon.sendMail();
//        system.assert(QSRCon.hasMsg);
//        //system.assert(QSRCon.hasError);
//        //system.assert(QSRCon.errorMsgs.size()==1);
        
//        // send email with recipient list setup
//        List<String> emails = new List<String>();
//        emails.add('raymond@clicktocloud.com');
//        List<Contact> testRecipients = DaoContact.getCorpContactsByEmails(emails);
//        List<Account> accounts = DaoAccount.getAccsContainName('');
//        if(testRecipients.size()>0 && accounts.size()>0){
//            QuickSendResumeController.ConRowRec recipient = new QuickSendResumeController.ConRowRec(testRecipients.get(0));
//            recipient.company= new QuickSendResumeController.AccRowRec(accounts.get(0));
//            QSRCon.optInConList.add(recipient);
//            QSRCon.sendMail();
//            system.assert(QSRCon.hasError);
//            //system.assert(recipient.sendMailCode == SendMailService.SEND_SUCC);
//            //system.assertEquals(recipient.sendMailMsg,SendMailService.SEND_MAIL_SUCC_MSG);
//        }
        
//        // test help infos rolling function
//        QSRCon.changeStdHelpInfo();
//        QSRCon.changeStdHelpInfo();
        
//        // send custom email template
//        QSRCon.emailContentType=2;
//        system.debug('template type:'+QSRCon.isCustomTempRichText);
//        QSRCon.changeSendMailType();
//        system.debug('template type after change:'+QSRCon.isCustomTempRichText);
//        //system.assert(QSRCon.isCustomTempRichText);
//        QSRCon.genNameList();
//        system.debug('send mail name list:'+(QSRCon.nameList.length()>0));
//        system.assert(QSRCon.nameList.length()>0);
        
//        // switch between two types of email template
//        QSRCon.emailContentType=1;
//        QSRCon.changeSendMailType();
//        QSRCon.emailContentType=2;
//        QSRCon.changeSendMailType();
        
//        QSRCon.sendCustomTempEmail();
//        QSRCon.sendMail();
//    	}
//    }
}