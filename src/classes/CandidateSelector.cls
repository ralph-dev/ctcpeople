/**
 * This is the selector for Contact Object
 * Created by Lina
 * Created on 19/06/2017
 */

public with sharing class CandidateSelector extends CommonSelector {

    public CandidateSelector() {
    	super(Contact.SObjectType);
    }

    

    public override LIST<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField>{
                Contact.Id
        };
    }

    private List<String> getContactBasicFields() {
        return new List<String>{
                'Id',
                'FirstName',
                'LastName',
                'Email',
                'MobilePhone',
                'Phone',
                'MailingStreet',
                'MailingCity',
                'MailingState',
                'MailingPostalCode',
                'MailingCountry'
        };
    }

    private List<String> ContactResumeLinkField(){
        return new List<String>{
                'Id',
                'Name'
        };
    }

    //this will query all the fields excluding unsupported fields

    public List<Contact> selectContactByCriteria(List<PeopleSearchDTO.SearchCriteria> criteria) {
        return selectContactByCriteria(criteria, null);
    }

    //when second param is not null or a valid json column string, the only fields returned by the query are the selected fields and
    //some additional fields: Id, Name, RecordTypeId
    public List<Contact> selectContactByCriteria(List<PeopleSearchDTO.SearchCriteria> criteria, String selectedColumn) {
        return selectContactByCriteria(criteria, selectedColumn, null);
    }

    //when third param is not null or empty, the query will only select candidates who are in this set of Ids
    //the only fields returned by the query are the selected fields and
    //some additional fields: Id, Name, RecordTypeId
    public List<Contact> selectContactByCriteria(List<PeopleSearchDTO.SearchCriteria> criteria, String selectedColumn, Set<String> conIds) {
        String condition = '';
        fflib_QueryFactory.SortOrder listOrder = fflib_QueryFactory.SortOrder.DESCENDING;
        final String CONDITIO_SEPARATOR = ' AND ';
        for (PeopleSearchDTO.SearchCriteria criterion : criteria) {
            PeopleSearchOperatorFactory.Operator op = PeopleSearchOperatorFactory.getOperatorsMap().get(criterion.operator);
            //system.debug('\n\nCHECK ME: ' + criterion.field + ' = ' + criterion.values + '\nOperator:\n' + op + '\n\n');
            if (op != null) {
                condition += op.generateCondition(criterion.field, criterion.values, criterion.type) + CONDITIO_SEPARATOR;
            }
        }
        if (conIds != null && !conIds.isEmpty()) {
            condition += 'Id in: conIds';
            listOrder = fflib_QueryFactory.SortOrder.ASCENDING;
        } else {
            condition = condition.removeEnd(CONDITIO_SEPARATOR);
        }
 
        
        Set<String> allNeededFields = FieldsHelper.getFieldsForPeopleSearch(selectedColumn);
       // system.debug('queryString =' + newQueryFactory().selectFields(allNeededFields).setCondition(condition).addOrdering('LastModifiedDate', listOrder, true).setLimit(5000).toSOQL());

        SimpleQueryFactory qf = simpleQuery()
                .selectFields(allNeededFields)
                .setCondition(condition);
        qf.clearOrderings();
        qf.addOrdering('LastModifiedDate', listOrder, true);
        qf.setLimit(5000);
        return (List<Contact>) Database.query(qf.toSOQL());
    }

    public List<Contact> selectContactByIds(Set<String> contactIds, String selectedColumn) {
        Set<String> allNeededFields = FieldsHelper.getFieldsForPeopleSearch(selectedColumn);
        String condition = 'Id in: contactIds';
        SimpleQueryFactory qf = simpleQuery()
                .selectFields(allNeededFields)
                .setCondition(condition);
        qf.clearOrderings();
        qf.addOrdering('LastModifiedDate', fflib_QueryFactory.SortOrder.ASCENDING, true);
        qf.setLimit(5000);
        return (List<Contact>) Database.query(qf.toSOQL());
    }

    public Contact selectContactById(String contactId) {
        try{
            fflib_QueryFactory qf = newQueryFactory();
            qf.selectFields(FieldsHelper.getFieldsForPeopleSearch());
            qf.setCondition('Id =: contactId');

            fflib_QueryFactory qfSub = qf.subselectQuery(Web_Document__c.SObjectType, true);
            qfSub.selectFields(ContactResumeLinkField());
            qfSub.getOrderings().clear();
            qfSub.addOrdering('LastModifiedDate', fflib_QueryFactory.SortOrder.DESCENDING);
            qfSub.setLimit(1);

            return (Contact) Database.query(qf.toSOQL());
        }catch(Exception e){
            System.debug('Exception on candidate selector:'+e);
            return null;
        }
    }

    public Contact selectContactWithBasicFieldsById(String contactId) {
        try{
            fflib_QueryFactory qf = newQueryFactory();
            qf.selectFields(getContactBasicFields());
            qf.setCondition('Id =: contactId');

            fflib_QueryFactory qfSub = qf.subselectQuery(Web_Document__c.SObjectType, true);
            qfSub.selectFields(ContactResumeLinkField());
            qfSub.setCondition('Document_Type__c = \'Resume\'');
            qfSub.getOrderings().clear();
            qfSub.addOrdering('LastModifiedDate', fflib_QueryFactory.SortOrder.DESCENDING);
            qfSub.setLimit(1);

            return (Contact) Database.query(qf.toSOQL());
        }catch(Exception e){
            System.debug('Exception on candidate selector:'+e);
            return null;
        }
    }

    public List<Contact> selectContactByQuery(SkillSearchDTO.SkillQuery skillsSearch) {
        return selectContactByQuery(skillsSearch, null);
    }

    public List<Contact> selectContactByQuery(SkillSearchDTO.SkillQuery skillsSearch, String selectedColumnn) {
        return selectContactByQuery(skillsSearch, selectedColumnn, null);
    }

    public List<Contact> selectContactByQuery(SkillSearchDTO.SkillQuery skillsSearch, String selectedColumn, Set<String> conIds) {
        Set<String> allNeededFields = FieldsHelper.getFieldsForPeopleSearch(selectedColumn);

        List<Contact> cons = new List<Contact>();

        Map<String, Set<String>> skillsCandidateSetMap = new Map<String, Set<String>>();

        Map<String, String> skillsMap = getSkillsMap(skillsSearch.query);

        CandidateSkillSelector cSkillSelector = new CandidateSkillSelector();

        List<Candidate_Skill__c> cSkills = new List<Candidate_Skill__c>();

        cSkills = cSkillSelector.getCandidateSkillByCandidates(skillsMap, skillsSearch.verifiedSkills, conIds);

        if (cSkills != null && cSkills.size() > 0) {

            Set<String> allCandidateSet = new Set<String>();       //keep all candidate set in the skill search
            for (Candidate_Skill__c cSkill : cSkills) {
                allCandidateSet.add(cSkill.Candidate__c);

                Set<String> candidatesSet = new Set<String>();     //keep skill candidate Map value set

                if (skillsCandidateSetMap.containsKey(cSkill.Skill__c)) {
                    candidatesSet = skillsCandidateSetMap.get(cSkill.Skill__c);
                }

                candidatesSet.add(cSkill.Candidate__c);

                skillsCandidateSetMap.put(cSkill.Skill__c, candidatesSet);
            }
            if (conIds != null && conIds.size() > 0) {
                conIds.retainAll(allCandidateSet);   //only keep candidate from skill search
            } else {
                conIds = allCandidateSet;
            }

            Set<String> candidatesResult = new Set<String>();
            for (String skillId : skillsMap.keySet()) {
                Set<String> candidatesCompareResult = new Set<String>();
                if (skillsCandidateSetMap.containsKey(skillId)) {
                    candidatesCompareResult = skillsCandidateSetMap.get(skillId);
                }
                if (skillsMap.get(skillId).equalsignorecase('and')) {
                    if (candidatesResult != null && candidatesResult.size() > 0) {
                        candidatesResult.retainAll(candidatesCompareResult);
                    }
                } else if (skillsMap.get(skillId).equalsignorecase('not')) {
                    if (candidatesResult != null && candidatesResult.size() > 0) {
                        candidatesResult.removeAll(candidatesCompareResult);
                    }
                } else {
                    if (candidatesResult != null && candidatesResult.size() > 0) {
                        candidatesResult.addAll(candidatesCompareResult);
                    } else {
                        candidatesResult = candidatesCompareResult;
                    }
                }

            }
            conIds.retainAll(candidatesResult);
        } else {
            system.debug('no matching candidates with skills');
            return new List<Contact>();
        }
        if (conIds != null && conIds.size() > 0) {
            String condition = '';
            condition += 'Id in: conIds';

            SimpleQueryFactory qf = simpleQuery()
                    .selectFields(allNeededFields)
                    .setCondition(condition);
            qf.clearOrderings();
            qf.addOrdering('LastModifiedDate', fflib_QueryFactory.SortOrder.ASCENDING, true);
            qf.setLimit(5000);
            return (List<Contact>) Database.query(qf.toSOQL());
        } else {
            return new List<Contact>();
        }
    }

    private Map<String, String> getSkillsMap(String query) {
        Map<String, String> skillsMap = new Map<String, String>();
        List<String> skillsAndOperator = query.split(' ');
        skillsMap.put(skillsAndOperator.get(0), '');
        for (Integer i = 1; i < skillsAndOperator.size(); i = i + 2) {
            skillsMap.put(skillsAndOperator.get(i + 1), skillsAndOperator.get(i));
        }
        return skillsMap;
    }

    //Get candidates by condition string
    public List<Contact> selectCandidatesByCriteria(String condition) {
        SimpleQueryFactory qf = simpleQuery()
                .selectFields(getSObjectFieldList())
                .setCondition(condition);
        qf.clearOrderings();
        qf.addOrdering('LastModifiedDate', fflib_QueryFactory.SortOrder.DESCENDING, true);
        return (List<Contact>) Database.query(qf.toSOQL());
    }

}