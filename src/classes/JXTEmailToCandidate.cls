/**
    * Email service are automatically processing the contents, headers, and attachments of inbound email.
    * 
    * This class is designed to handle email with resume from different senders and automatically creates Candidate,
    * Attachment, activity, cm in SF and call webservice to parse resume and save details to candidate object in SF
    * 
    * @author: Lorena
    * @createdDate: 6/10/2016
    * @lastModify: 
    */
global class JXTEmailToCandidate implements Messaging.InboundEmailHandler {
	

	//Email Service entry method
    global Messaging.InboundEmailResult handleInboundEmail(
    	Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        
        if(email.binaryAttachments != null && email.binaryAttachments.size() > 0){
        	System.debug('#email.attachment number is #' + email.binaryAttachments.size());
        }else{
        	System.debug('NO ATTACHMENT !!!! #' );
        }
        
        
        //default return result
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        try {
        	
        	//parse email and do corresponding process
            invokeEmailHandler(email);  
            result.success = true;
        } catch(Exception ee) {
            System.debug('---exception in JXTEmailToCandidate----' + ee);
            System.debug(ee.getStackTraceString());
            
            result.success = false;
      		result.message = 'JXTEmailToCandidate Error: \n' + ee.getStackTraceString();   
           
        }
        
        return result;
    }  
    
    /*
    * Parse the content of email and decide what action to do according to the content of email
    * 
    */
    private void invokeEmailHandler(Messaging.InboundEmail email) {
    	
        //create a corresponding IEmailHandler object, such as VacancyMaker object
        ICVEmailHandler handler = CVEmailHandlerFactory.build( email );
        
        if(handler != null ){
        	//execute handle processing
        	handler.handle();
        }
    }
}