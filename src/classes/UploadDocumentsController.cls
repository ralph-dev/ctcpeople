public with sharing class UploadDocumentsController{
    
    public List<Schema.FieldSetMember> originalFieldsList{get;set;}     // keep all fields from fieldset
    public List<Schema.FieldSetMember> finalFieldsList{get;set;}        // keep all fields after filter out illegal fields
    public List<Schema.FieldSetMember> dateFieldList{get;set;}        // keep all fields after filter out illegal fields
    public List<Schema.FieldSetMember> textareaFieldList{get;set;}        // keep all fields after filter out illegal fields
    public List<Schema.FieldSetMember> otherList{get;set;}        // keep all fields after filter out illegal fields
    public String typeOptionToList{set;get;}
    public Web_Document__c webdocObject {get;set;}
    public String fieldSetName;     // field set where all fields are stored
    public String allFieldsString {get;set;}    // a string contains all fields separated with ";"
    public List<String> allFieldsList {get;set;}    // a list contains all fields type and name 
    public String srcId {get;set;}     // id of record to redirect to
    public String srcObj{get;set;}          // object that is using the upload documents function.
    public String srcPage {get;set;}      // page that is using the upload documents function
    public String returnLinkValue{get;set;}     // value/wording of the return link
    public String serviceEndPoint {get;set;}    // service end point
    public String orgId {get;set;}      // org id
    public String bucketName {get;set;}     // bucket name
    public String nsPrefix {get;set;}       // name space prefix
    public String sessionId {get;set;}      // session id
    public boolean showErrorMessage {get;set;}      // flag to display error message
    public String retURLNotEncoded {get;set;}       // return url not encoded 
    public String retURLEncoded {get;set;}  // return url encoded
    public String lookupField{get;set;} // name of the field which use to store relation
    public list<SelectOption> docTypeOptions{get;set;}
    public String docTypeSelected {get;set;}
    public Document document1 {set;get;}
	public list<Schema.FieldSetMember>  fieldSetSet=new list<Schema.FieldSetMember>();
	
    public UploadDocumentsController(){
        dateFieldList=new List<Schema.FieldSetMember>();
        textareaFieldList=new List<Schema.FieldSetMember>();
        otherList=new List<Schema.FieldSetMember>();
        serviceEndPoint = getServiceEndpoint();
        //System.debug('serviceEndPoint:' + serviceEndPoint);
        // retrive org id and bucketname
        getSfOrgInfo();
        // retrieve namespace prefix
        nsPrefix = getNSPrefix();
        // get name of the field which should be used to store lookup/master-detail relation
        lookupField = ApexPages.currentPage().getParameters().get('lookupField');
        // get session id
        sessionId = getSessionId();
        webdocObject = new Web_Document__c();
        srcId = ApexPages.currentPage().getParameters().get('srcId');
        srcObj = ApexPages.currentPage().getParameters().get('srcObj');
        srcPage = ApexPages.currentPage().getParameters().get('srcPage');
        
        String isdtpValue = ApexPages.currentPage().getParameters().get('isdtp');
        if(isdtpValue == null){
            String ref = ApexPages.currentPage().getHeaders().get('Referer');
            if(ref != null ){
                PageReference p = new PageReference(ref);
                isdtpValue = p.getParameters().get('isdtp');
            }
        }
    
        
        retURLNotEncoded = getReturnUrl(srcId,srcObj,srcPage,isdtpValue);
        
        
        returnLinkValue = getLinkValueBySObjType(srcObj);
        //System.debug('returnLinkValue:' + returnLinkValue);
      	//Customize picklist options
        CTCSettingUploadDocumentServices ctcDocumentService=new CTCSettingUploadDocumentServices();
		fieldSetSet=ctcDocumentService.setupOptionsForDocTypes(srcObj);
        docTypeOptions=ctcDocumentService.getDocTypeOptions();
        SelectOptionSorter.doSort(docTypeOptions, SelectOptionSorter.FieldToSort.Label);
        fieldSetName = getFieldSetNameBySObjType(srcObj);
        map<String,list<Schema.FieldSetMember>> typeFieldMap=new map<String,list<Schema.FieldSetMember>>();
        typeFieldMap=ctcDocumentService.getTypeFieldMap();
        
        // // display error message if no fileds are retrieved from custom setting.
        // if(fieldSetName == null){
        //     PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(PeopleCloudErrorInfo.ERR_FIELDSET_CONFIGURATION,true);                    
        //     showErrorMessage = errormesg.returnresult;
        //     for(ApexPages.Message newMessage:errormesg.errorMessage){
        //         ApexPages.addMessage(newMessage);
        //     }    
        // }
    
        allFieldsString = '';
        originalFieldsList=new List<Schema.FieldSetMember>();
        finalFieldsList=new List<Schema.FieldSetMember>();

        
        for(Schema.FieldSetMember singleFieldSetM: fieldSetSet){
            finalFieldsList.add(singleFieldSetM);           
        }
		typeOptionToList=System.JSON.serializePretty(typeFieldMap);
        System.debug('The finalFieldList size is '+fieldSetSet.size());
        allFieldsString = System.JSON.serializePretty(fieldSetSet); 
    }
    
    //Back to previous page 
    public PageReference returnToPreviousPage(){
        system.debug('return srcId = '+ srcId);
        PageReference previousPage = new PageReference('/'+ srcId);
        previousPage.setRedirect(true);
        return previousPage;
    }
     
    private String getFieldSetNameBySObjType(String sobjName){
        String fieldSetName = '';
        
        if(sobjName=='Candidate'){
            fieldSetName =  PCAppSwitch__c.getinstance().Field_Set_Name_Upload_Document_Candidate__c;    
        }else if (sobjName == 'User') {
            fieldSetName = 'default_upload_documents';    
        }else if(sobjName=='Contact'){
            // do nothing for now
        }else if(sobjName=='Client'){
            // do nothing for now
            System.debug('Client Field Set');
            fieldSetName = 'default_upload_documents'; 
        }else if(sobjName=='Vacancy'){  
            // do nothing for now
        }else{
            fieldSetName =  PCAppSwitch__c.getinstance().Field_Set_Name_Upload_Document_Candidate__c;        
        }
        
        if(fieldSetName == null || fieldSetName == ''){
            fieldSetName = 'default_upload_documents';  
            //fieldSetName = 'PeopleCloud1__default_upload_documents'; 
        }
        
        return fieldSetName;
    }
    
    
    private String getLinkValueBySObjType(String sobjName){
        return 'Back To '+sobjName+' Page';  
        
    }
    
    private String getServiceEndpoint(){
        //return Endpoint.getPeoplecloudWebSite();
         return Endpoint.getcpewsEndpoint();
    }
    
    private void getSfOrgInfo(){
        CommonSelector.checkRead(User.SObjectType,'Id, AmazonS3_Folder__c');
        User u = [select Id, AmazonS3_Folder__c from User where id = :Userinfo.getUserId() limit 1];        
        if(u.AmazonS3_Folder__c == NULL) {
            PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(PeopleCloudErrorInfo.S3_BUCKET_NAME_EMPTY_ERROR,true);                    
            showErrorMessage = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
                ApexPages.addMessage(newMessage);
            } 

        } else {
            bucketName = u.AmazonS3_Folder__c;
        }
        orgId = Userinfo.getOrganizationId().substring(0, 15);
    }
    
    private String getNSPrefix(){
        return PeopleCloudHelper.getPackageNamespace(); 
    }
    
    private String getSessionId(){
        return UserInfo.getSessionId();
    }
    
    private String getReturnUrl(String srcId, String srcObj, String srcPage,String isdtpValue){
        if(srcId !=null){
            srcId = EncodingUtil.urlEncode(srcId, 'UTF-8');
        }
        if(srcObj != null){
            srcObj = EncodingUtil.urlEncode(srcObj, 'UTF-8');
        }
        if(lookupField != null){
            lookupField = EncodingUtil.urlEncode(lookupField, 'UTF-8');
        }
        
        String queryString = 'srcId=' + srcId + '&srcObj=' + srcObj+'&lookupField='+lookupField;
        
        if(isdtpValue != null){
            isdtpValue = EncodingUtil.urlEncode(isdtpValue, 'UTF-8');
            queryString +='&isdtp='+isdtpValue;
        }
        
        if (srcPage != null && srcPage == 'SendResume') {
            return '/apex/SRUploadDocumentsResult?' + queryString;
        } else {
            return '/apex/UploadDocumentsResult?' + queryString;
        }
    }
    
    
}