@isTest
private class AccountSelectorTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		Account acc = TestDataFactoryRoster.createAccount();
    
	       AccountSelector accSelector = new AccountSelector();
	       
	       //test method fetchAccountListByKeyword(keyword)
	       String keyword = '';
	       List<Account> accListFetchedByKeyword = accSelector.fetchAccountListByKeyword(keyword);
	       System.assertNotEquals(accListFetchedByKeyword,null);
	       
	       //test method fetchAccountListByKeyword(String accountId)
	       List<Account> accListFetchedById = accSelector.fetchAccountInfoById(acc.Id);
	       System.assertNotEquals(accListFetchedById,null);
    	}
       
    }
}