/*
	Author: Kevin
	
	This class is to update candidate records when an availability record is created, updated or deleted.
      
*/

public with sharing class ContactUpdater {
	
	// update candidate records when availability are created, updated or deleted.
	public static void updateCandLastModifyDateByAvailability(List<Days_Unavailable__c> avlList){
		Set<String> candIdList = getCandIdListByAvlList(avlList);
		
		CommonSelector.checkRead(Contact.sObjectType, 'Id, LastModifiedDate');
        List<Contact> candList = [select Id, LastModifiedDate from Contact where id in: candIdList];
        try{
            //nothing changes in here 09/08/2017 Ralph
            //update candList;
        }
        catch(Exception e){
			// do nothing for now
        }
	}
	
	
	//get a list of candidate ids by Availability records
    private static Set<String> getCandIdListByAvlList(List<Days_Unavailable__c> avlList){
        try{
            Set<String> candIdSet = new Set<String>();
            for(Days_Unavailable__c avl : avlList){           
                candIdSet.add(avl.Contact__c);              
            }
            return candIdSet;
        }
        catch(Exception e){
			// do nothing for now
            return null;
        }
    }
}