public with sharing class TradeMeEdit {
    public Advertisement__c job{get; set;}
    public StyleCategory__c sc;
    public String[] photourls{get; set;}
    public IEC2WebService iec2ws{get; set;}
    public boolean show_page{get;set;}
    
    //Init Error Message for customer, Add by Jack on 10/09/2014
    public PeopleCloudErrorInfo trademe_errormesg; 
    public Boolean trademeMsgSignalhasmessage{get ;set;} //show error message or not
    
    /*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}
    /*************End Add skill parsing param********************************************************/
    
  
     
    public TradeMeEdit(ApexPages.StandardController stdController){
    	show_page = true; //If the status is not valid, show page is false
    	//Init skill parsing param. Add by Jack on 23/05/2013
        enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
        enableSkillGroupList = new List<Skill_Group__c>();
        enableSkillGroupList = SkillGroups.getSkillGroups();
        defaultskillgroups = new List<String>();
        //end skill parsing param.
        
        job = (Advertisement__c) stdController.getRecord();
        
        if(job.Status__c.equalsignorecase('Expired') || job.Status__c.equalsignorecase('Deleted') || job.Status__c.equalsignorecase('Archived')){
        	trademeMsgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.ADVERTISEMT_AD_STATUS_ERROR);
        	show_page = false;
        }
	    iec2ws = new EC2WebServiceImpl();
	    if(job.Skill_Group_Criteria__c!=null && job.Skill_Group_Criteria__c!=''){
	    	String defaultskillgroupstring = job.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
		    defaultskillgroups = defaultskillgroupstring.split(',');
	    }
	    
	    CommonSelector.checkRead(StyleCategory__c.sObjectType, 'ProviderCode__c, CompanyCode__c, OfficeCodes__c');
	    sc = [select ProviderCode__c, CompanyCode__c, OfficeCodes__c from StyleCategory__c where WebSite__c='Trademe']; 
	    job.Provider_Code__c = sc.ProviderCode__c;
	    job.Company_Name__c = sc.CompanyCode__c;
        
    }
    
    public List<SelectOption> getItems(){
        return TrademeUtil.getOfficeCodes(sc.OfficeCodes__c);
    }
    public List<SelectOption> getStyleOptions(){
        return JobBoardUtils.getStyleOptions('Trademe');
    }
    
    public PageReference post(){
    	Boolean insertDBSuccess = true; //Status of Insert DB 
    	Integer updateDBSuccess = 1;    //Status of Update DB
		
		CommonSelector.checkRead(User.sObjectType, 'AmazonS3_Folder__c');
		User currentUser = [select AmazonS3_Folder__c from User where Id=:UserInfo.getUserId()];
    	String jobRefCode=UserInfo.getOrganizationId().subString(0, 15)+':'+job.Id;
    	
    	CommonSelector.checkRead(StyleCategory__c.sObjectType, 
    		'Id, Header_EC2_File__c, Header_File_Type__c,Div_Html_EC2_File__c, Div_Html_File_Type__c, Footer_EC2_File__c, Footer_File_Type__c  ');
    	StyleCategory__c sc = [select Id, Header_EC2_File__c, Header_File_Type__c,
    	 	Div_Html_EC2_File__c, Div_Html_File_Type__c, 
    	 	 Footer_EC2_File__c, Footer_File_Type__c  
    	  		from StyleCategory__c where id = :job.Application_Form_Style__c limit 1];
    	
    	//If job status not valid, insert DB
    	if(job.Status__c.equalsignorecase('not valid')|| job.Status__c.equalsignorecase('clone to post')){                            
    		insertDBSuccess = iec2ws.insertDetailTable(jobRefCode, UserInfo.getOrganizationId().subString(0, 15),
            String.valueOf(job.Id), currentUser.AmazonS3_Folder__c, JobBoardUtils.blankValue(sc.Header_EC2_File__c), 
              JobBoardUtils.blankValue(sc.Header_File_Type__c), sc.Div_Html_EC2_File__c,
                sc.Div_Html_File_Type__c,  JobBoardUtils.blankValue(sc.Footer_EC2_File__c), 
                 JobBoardUtils.blankValue(sc.Footer_File_Type__c), '', 'trademe', 
                     JobBoardUtils.blankValue(job.Job_Title__c));
    	
    	}else{ 
    //		updateDBSuccess = iec2ws.updateDetailTable(String.valueOf(job.id).substring(0, 15), sc.Header_EC2_File__c, sc.Header_File_Type__c, 
			 //sc.Div_Html_EC2_File__c, sc.Div_Html_File_Type__c, 
			 //	sc.Footer_EC2_File__c, sc.Footer_File_Type__c, UserInfo.getOrganizationId().subString(0, 15));
            updateDBSuccess = iec2ws.updateDetailTable(String.valueOf(job.id), sc.Header_EC2_File__c, sc.Header_File_Type__c, 
             sc.Div_Html_EC2_File__c, sc.Div_Html_File_Type__c, 
                sc.Footer_EC2_File__c, sc.Footer_File_Type__c, UserInfo.getOrganizationId().subString(0, 15),  JobBoardUtils.blankValue(job.Job_Title__c));
            if(updateDBSuccess == 0){
                insertDBSuccess = iec2ws.insertDetailTable(jobRefCode, UserInfo.getOrganizationId().subString(0, 15),
                String.valueOf(job.Id), currentUser.AmazonS3_Folder__c, JobBoardUtils.blankValue(sc.Header_EC2_File__c), 
                  JobBoardUtils.blankValue(sc.Header_File_Type__c), sc.Div_Html_EC2_File__c,
                    sc.Div_Html_File_Type__c,  JobBoardUtils.blankValue(sc.Footer_EC2_File__c), 
                     JobBoardUtils.blankValue(sc.Footer_File_Type__c), '', 'trademe', 
                         JobBoardUtils.blankValue(job.Job_Title__c));
            }
    	}
    	
		job.photos__c=TrademeUtil.makeUrlString(photourls);
		job.Job_Content__c = TrademeUtil.filter(job.Job_Content__c);
		
		//insert skill group ext id
	    if(enableskillparsing){
	    	String tempString  = String.valueOf(defaultskillgroups);
	        tempString = tempString.replaceAll('\\(', '');
	        tempString = tempString.replaceAll('\\)', '');
	        tempString = tempString.replace(' ','');
	        Job.Skill_Group_Criteria__c = tempString;
	    }
	    //end insert skill group ext id
	    
	    //Check the Insert DB or Update DB success
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Advertisement__c.Status__c,
            Advertisement__c.Job_Posting_Status__c,
            Advertisement__c.Application_URL__c
        };
    	if(insertDBSuccess == false){
    		job.Status__c = 'Not Valid';
    		job.Job_Posting_Status__c = 'Insert/Update URL Failed';
    		job.Application_URL__c = '';
    		trademeMsgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.INSERT_OR_UPDATE_DYNAMIC_APPLICATION_FORM_FAILED);
            //check FLS
            fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    		update job;
			return null;  
    	}else if(updateDBSuccess > 1){//if update fail, update posting status to Insert/update URL failed. And show error message
    		job.Status__c = 'Active';
    		job.Job_Posting_Status__c = 'Insert/Update URL Failed';
    		job.Application_URL__c = '';
    		trademeMsgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.INSERT_OR_UPDATE_DYNAMIC_APPLICATION_FORM_FAILED);
    		//check FLS
            fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
            update job;
			return null;  
    	}else{
    		job.Status__c = 'Active';
    		job.Job_Posting_Status__c = '';
    		job.Application_URL__c = Endpoint.getcpewsEndpoint()+'/peoplecloud/GetApplicationForm?jobRefCode='+jobRefCode+'&website=trademe';
    		//check FLS
            fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
            update job;
			return new ApexPages.StandardController(job).view();
    	}
    }
    public List<SelectOption> getPhotoItems(){
    	return TrademeUtil.getPhotos();
    }
    
    //insert the skill group picklist value option
    public List<SelectOption> getSelectSkillGroupList(){
	  	List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
	  	List<SelectOption> displaySelectOption = new List<SelectOption>();
	  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
	  		displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
	  	}
	  	return displaySelectOption;
	}
	
	//Generate Error message, Add by Jack on 10/09/2014
    public static Boolean customerErrorMessage(Integer msgcode){
        PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        Boolean msgSignalhasmessage = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
                ApexPages.addMessage(newMessage);
            }
        return msgSignalhasmessage;               
    }
}