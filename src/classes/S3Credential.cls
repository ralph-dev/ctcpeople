public without sharing class S3Credential{
    public String accessKey{get;set;}
    public String secretKey{get;set;}
    public S3Credential(String accessKey, String secretKey){
        this.accessKey = accessKey;
        this.secretKey = secretKey;
    }
}