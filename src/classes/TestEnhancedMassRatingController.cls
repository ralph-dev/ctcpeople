/**************************************
 *                                    *
 * Deprecated Class, 13/06/2017 Ralph *
 *                                    *
 **************************************/

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestEnhancedMassRatingController {
    static testmethod void unitTest(){
        System.assertEquals(null, EnhancedMassRatingController.searchCandidates('',''));
    }
    /*static testMethod void testSearchCandidates(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	String result = EnhancedMassRatingController.searchCandidates('', '[]');
    	system.assertEquals('[]', result);
    	result = EnhancedMassRatingController.searchCandidates('', '["0018000000ibdMj","0018000000ibdMj"]');
    	system.assertEquals('[]', result);
    	}
    }
    
    static testMethod void testGoBack(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	PageReference recentPage=new PageReference('/apex/enhancedmassrating?asc=DESC&cmids=a08E0000007RlzJIAS%2Ca08E0000007Rm0EIAS%2Ca08E0000007Rm0FIAS%2Ca08E0000007RlzHIAS%2Ca08E0000007Rm01IAC%2Ca08E0000007RlzzIAC%2Ca08E0000007Rm0HIAS%2Ca08E0000007RlzFIAS%2Ca08E0000007RlzIIAS%2Ca08E0000007RlzVIAS%2Ca08E0000007Rm0GIAS%2Ca08E0000007Rm0IIAS%2Ca08E0000007Rm0JIAS%2Ca08E0000007Rm0KIAS%2Ca08E0000007Rm0LIAS%2Ca08E0000007Rm0MIAS%2Ca08E0000007RlzSIAS%2Ca08E0000007RlzRIAS%2Ca08E0000007RlzQIAS%2Ca08E0000007RlzPIAS%2Ca08E0000007RlzOIAS%2Ca08E0000007RlzNIAS%2Ca08E0000007RlzMIAS%2Ca08E0000007RlzLIAS%2Ca08E0000007RlzKIAS%2Ca08E0000007RlzkIAC%2Ca08E0000007RlzjIAC%2Ca08E0000007RlziIAC%2Ca08E0000007RlzhIAC%2Ca08E0000007RlzgIAC%2Ca08E0000007RlzfIAC%2Ca08E0000007RlzeIAC%2Ca08E0000007RlzdIAC%2Ca08E0000007RlzcIAC%2Ca08E0000007Rm03IAC%2Ca08E0000007Rm02IAC%2Ca08E0000007Rm00IAC%2Ca08E0000007RlzyIAC%2Ca08E0000007RlzxIAC%2Ca08E0000007RlzwIAC%2Ca08E0000007RlzvIAC%2Ca08E0000007RlzuIAC%2Ca08E0000007RlztIAC%2Ca08E0000007Rm0DIAS%2Ca08E0000007Rm0CIAS%2Ca08E0000007Rm0BIAS%2Ca08E0000007Rm0AIAS%2Ca08E0000007Rm09IAC%2Ca08E0000007Rm08IAC%2Ca08E0000007Rm07IAC%2Ca08E0000007Rm06IAC%2Ca08E0000007Rm05IAC%2Ca08E0000007Rm04IAC%2Ca08E0000007RlzsIAC%2Ca08E0000007RlzrIAC%2Ca08E0000007RlzqIAC%2Ca08E0000007RlzpIAC%2Ca08E0000007RlzoIAC%2Ca08E0000007RlznIAC%2Ca08E0000007RlzmIAC%2Ca08E0000007RlzlIAC%2Ca08E0000007RlzbIAC%2Ca08E0000007RlzaIAC%2Ca08E0000007RlzZIAS%2Ca08E0000007RlzYIAS%2Ca08E0000007RlzXIAS%2Ca08E0000007RlzWIAS%2Ca08E0000007RlzUIAS%2Ca08E0000007RlzTIAS%2Ca08E0000007Rm0NIAS%2Ca08E0000007Rm0OIAS%2Ca08E0000007Rm0PIAS%2Ca08E0000007Rm0QIAS%2Ca08E0000007Rm0RIAS%2Ca08E0000007Rm0SIAS%2Ca08E0000007Rm0TIAS&cp=1&id=a09E0000001xwfr&sc=Rating__c');
    	Test.setCurrentPage(recentPage);
    	EnhancedMassRatingController controller = new EnhancedMassRatingController(new ApexPages.StandardSetController(new List<Placement_Candidate__c>()));
    	controller.Init();
    	system.debug(controller.goBack().getUrl());
    	system.assert(controller.goBack().getUrl().contains('iframeparentstandard'));
    	system.assert(controller.cancel().getUrl().contains('iframeparentstandard'));
    	system.assert(controller.massUpdateAndReturn().getUrl().contains('iframeparentstandard'));
    	controller.first();
    	system.assert(controller.currentpage=='1');
    	controller.previous();
    	controller.next();
    	controller.massupdate();
    	controller.last();
//    	system.debug('current page'+controller.currentpage);
//    	system.debug('total page'+controller.totalPage);
//    	system.assert(controller.currentpage==''+controller.totalPage);
    	
     	recentPage=new PageReference('/apex/enhancedmassrating');
     	recentPage.getParameters().put('id','asdfasdfadsf');
     	recentPage.getParameters().put('retUrl','asdfasdfadsf');
    	Test.setCurrentPage(recentPage);
    	controller = new EnhancedMassRatingController(new ApexPages.StandardSetController(new List<Placement_Candidate__c>()));
    	controller.init();
		system.debug(controller.goBack().getUrl());
    	system.assert(controller.cancel().getUrl().contains('asdfasdfadsf'));
    	system.assert(controller.massUpdateAndReturn().getUrl().contains('asdfasdfadsf'));
    	}
 
    }
    
    /**
    	Tested codes added to cover the new status screen candidate.
  								
  								  								-- Alvin
    
    **/
    /*static testMethod void testInitiate(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	List<Placement_Candidate__c> cmps=new List<Placement_Candidate__c>();
    	Placement__c vacancy1 =  InitVacancyData();
        cmps = InitTestData(vacancy1);
    	PageReference recentPage=new PageReference('/apex/EnhancedMassRating?retURL=%2Fa0G900000071dDH&wrapMassAction=1&scontrolCaching=1&id='+vacancy1.id);
    	Test.setCurrentPage(recentPage);
    	EnhancedMassRatingController controller = new EnhancedMassRatingController(new ApexPages.StandardSetController(cmps));
    	controller.init();
    	system.debug(controller.goBack().getUrl());
        controller.keywords = 'test';
        PageReference newPageReference = controller.searchkeywords();
        system.assert(newPageReference == null);
        controller.sortingOrder = 'ASC';
        controller.sortBycolumnApiName = 'Name';
        PageReference sortColumn = controller.sortColumn();
        system.assert(sortColumn == null);
        controller.checkthisrecords = new Map<String, boolean>{cmps[0].id=>true};
        PageReference addToAnotherVacancy = controller.AddToAnotherVacancy();
        system.assert(addToAnotherVacancy.getUrl().contains('addcandidatestovacancy'));
    	}
    }
    
    private static Placement__c InitVacancyData(){
    	Account testaccount=new Account(Name='test');
        insert testaccount;
    	Placement__c vacancy1=new Placement__c(name='testvacancy',  Company__c=testaccount.id);
    	insert vacancy1;
    	return vacancy1;
    }
    
    private static List<Placement_Candidate__c> InitTestData(Placement__c vacancy1){
    	
    	List<Contact> candidates=new List<Contact>();
    	Contact can1=new Contact(FirstName='testFirst2', email='2test@can.com', LastName='testLast2',Resume__c='This is a test.');
        Contact can2=new Contact(FirstName='testFirst2', email='2test@can.com', LastName='testLast2');
        Contact can3=new Contact(FirstName='testFirst3', email='3test@can.com', LastName='testLast3');
        Contact can4=new Contact(FirstName='testFirst3', email='3test@can.com', LastName='testLast3');
        Contact can5=new Contact(FirstName='testFirst5', email='5test@can.com', LastName='testLast5');
        Contact can6=new Contact(FirstName='testFirst6', email='6test@can.com', LastName='testLast6');
        Contact can7=new Contact(FirstName='testFirst7', email='7test@can.com', LastName='testLast7');
        Contact can8=new Contact(FirstName='testFirst8', email='8test@can.com', LastName='testLast8');
        Contact can9=new Contact(FirstName='testFirst9', email='9test@can.com', LastName='testLast9');
        Contact can0=new Contact(FirstName='testFirst7', email='7test@can.com', LastName='testLast7');
        candidates.add(can1);
        candidates.add(can2);
        candidates.add(can3);
        candidates.add(can4);
        candidates.add(can5);
        candidates.add(can6);
        candidates.add(can7);
        candidates.add(can8);
        candidates.add(can9);
        candidates.add(can0);        
        insert candidates;
        
        //Insert a advertisement for test.To make up the candidate from Seek.
        Advertisement__c ad1=new Advertisement__c(WebSite__c='Seek');
        insert ad1;
                
         //Insert one Candidate Management.
        List<Placement_Candidate__c> cmps=new List<Placement_Candidate__c>();
        for(Contact cand: candidates){
	        Placement_Candidate__c cmp=new Placement_Candidate__c();
	        cmp.Candidate__c=cand.id;
	        cmp.Placement__c=vacancy1.id;
	        cmp.Online_Ad__c=ad1.id;
	        cmp.Status__c='New';
	        cmps.add(cmp);
        }
        insert cmps;
        return cmps;
    }*/
}