public with sharing class ContentDocumentLinkSelector{
	
	
	
	
	private static String defaultFields = 'id, LinkedEntityId, ContentDocumentId, shareType';
    
    public static List<ContentDocumentLink> getFileByLinkedEntityId(String LinkedEntityId) {
        List<ContentDocumentLink> files = new List<ContentDocumentLink>();
        String query = 'select '+defaultFields+',ContentDocument.title from ContentDocumentLink where LinkedEntityId =: LinkedEntityId';
        
        fflib_SecurityUtils.checkRead(ContentDocumentLink.sObjectType,CommonSelector.convertStringToList(defaultFields));
        fflib_SecurityUtils.checkRead(ContentDocument.sObjectType,new List<String>{'title'});
        files = database.query(query);
        return files;
    }
    
    public static List<ContentDocumentLink> getFileByLinkedEntityIdAndDocumentIds(String LinkedEntityId, Set<String> ContentDocumentIds) {
        List<ContentDocumentLink> files = new List<ContentDocumentLink>();
        String query = 'select '+defaultFields+',ContentDocument.title from ContentDocumentLink where LinkedEntityId =: LinkedEntityId and ContentDocumentId IN: ContentDocumentIds';
        fflib_SecurityUtils.checkRead(ContentDocumentLink.sObjectType,CommonSelector.convertStringToList(defaultFields));
        fflib_SecurityUtils.checkRead(ContentDocument.sObjectType,new List<String>{'title'});
        files = database.query(query);
        return files;
    }

}