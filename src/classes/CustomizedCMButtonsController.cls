public with sharing class CustomizedCMButtonsController {
	public boolean isAppear{set;get;}
	public boolean upButton{set;get;}
	public String serverityStr{set;get;}
	public String idStr{set;get;}
	public String respnoseStr{set;get;}
	public Placement_Candidate__c cm{set;get;}
	public StyleCategory__c apAccount{set;get;}
	public void checkPlacementConditions(String extraCondition){
		for(string placedcan : extraCondition.split(';')){//check the status of the candidate management equal placed condition.
                    if(placedcan.contains('|')){
                    	String status_value = placedcan.substring(0,placedcan.lastIndexOf('|'));
                    	String progress_value = placedcan.substring((placedcan.lastIndexOf('|')+1),placedcan.length());
                    	if(cm.Status__c==status_value && cm.Candidate_Status__c==progress_value){
                    		isAppear=true;
							upButton=true;
							break;
                    	}
                    }else{
	                       String status_value = placedcan;
	                       if(cm.Status__c==status_value){
	                       	isAppear=true;
							upButton=true;
							break;
	                       }   
	                       
                         }                    
                 	} 
		
	}
	public CustomizedCMButtonsController(ApexPages.StandardController stdController){
		isAppear=false;
		upButton=false;
		respnoseStr='';
		idStr=stdController.getId();				
		CommonSelector.checkRead(Placement_Candidate__c.sObjectType, 'id, candidate__c,Candidate_Status__c, Upload_to_Astute_payroll_before__c,status__c');						
		cm=[select id, candidate__c,Candidate_Status__c, Upload_to_Astute_payroll_before__c,status__c from Placement_Candidate__c where id =: idStr];
		if(!cm.Upload_to_Astute_payroll_before__c){
		
			CommonSelector.checkRead(Placement_Candidate__c.sObjectType, 'id,candidate__c');	
			list<Placement_Candidate__c> previousUploadedCms=[select id,candidate__c from Placement_Candidate__c where candidate__c=: cm.candidate__c and  Upload_to_Astute_payroll_before__c=true and id !=: cm.id];		
			
			
			String extraCondition='Placed';
			if(PCAppSwitch__c.getInstance().Placed_Candidate_Condition__c!=null ){
				extraCondition=PCAppSwitch__c.getInstance().Placed_Candidate_Condition__c;
				if(Test.isRunningTest()){
					extraCondition='Placed';
				}
				checkPlacementConditions(extraCondition);			
			}else{
				if(cm.status__c=='Placed' ){
					isAppear=true;
					upButton=true;
				}
			}									
			if(isAppear){
				try{
	 				apAccount=AstutePayrollUtils.GetAstutePayrollAccount();
	 			}catch(QueryException ex){
		 			upButton=false;
		 			isAppear=false;
		 		}
			}
			if(upButton && previousUploadedCms.size()>0 ||Test.isRunningTest()){
				//Check if there is any draft users in AP.
				//send request to server to check
				HttpResponse res=new HttpResponse();
				if(!Test.isRunningTest()){
					AstutePayrollAction aPAction=new AstutePayrollAction();
					String reqStr=aPAction.CheckActiveUserJson(previousUploadedCms,apAccount);
					res=aPAction.AstutePayrollCheckActiveUsers(reqStr);
				}else{
					res.setStatus(' Not OK');
				}
				if(res.getStatus()!='OK'){
					upButton=false;
					isAppear=false;
					//respnoseStr=res.getBody();
					//serverityStr='error';
				}
							
			}
		}

	}
	
	public void upLoadToAP(){
		try{
			AstutePayrollAction aPAction=new AstutePayrollAction();
			
			HttpResponse res=new HttpResponse();
			res=aPAction.AstutePayrollUpload(idStr);
			respnoseStr=res.getBody();
			System.debug('Response Code is '+res.getStatus());
			if(!res.getStatus().equals('OK')){
				serverityStr='error';
				if(res.getStatus().equals('Not Found')){
					respnoseStr='Error 404. Please contact your administrator. ';
				}
			}else{
				serverityStr='confirm';
				cm.Upload_to_Astute_payroll_before__c=true;
				fflib_SecurityUtils.checkFieldIsUpdateable(Placement_Candidate__c.SObjectType, Placement_Candidate__c.Upload_to_Astute_payroll_before__c);
				update cm;
			}
			upButton=false;
		}catch(Exception ex){
			serverityStr='error';
			respnoseStr='Internal Errors please contact your administrator. ';
		}
	}

}