global class AutoUpdateProcess {
    // ** called by scheduler 'scheduledBatchUpdateForm'
    //** this scheduler maybe run 1 - 5 times a day, all up to the org resource
    global void doUpdate(){

       BatchUpdateAllForm updateAll = new BatchUpdateAllForm();
       //**** description call to get dropdown(picklist) information.
        SObjectType objToken = Schema.getGlobalDescribe().get('Contact');     
        DescribeSObjectResult objDef = objToken.getDescribe();  
        Map<string, SObjectField> o_fields = objDef.fields.getMap();
       // system.debug('o_fields  = ' + o_fields );
       //****
        
       
       String querystr = 'select Id , Div_Html_File_ID__c, Div_Html_EC2_File__c from StyleCategory__c where RecordType.DeveloperName = \'ApplicationFormStyle\' '; 
       
       updateAll.allfields = o_fields;
       updateAll.query = querystr;
       updateAll.fieldapi = 'Div_Html_File_ID__c';
       
       CommonSelector.checkRead(StyleCategory__c.sObjectType, 'Id , Div_Html_File_ID__c, Div_Html_EC2_File__c');
       ID batchprocessid = Database.executeBatch(updateAll,1);
       system.debug('batch id =' + batchprocessid);
    }

  
}