@isTest
private class TestCustomizedCMButtonsController {
	private static  testmethod void uniTest(){
		System.runAs(DummyRecordCreator.platformUser) {
		RecordType APRecordType= DaoRecordType.AstutePayrollAccountRT;//[select id from RecordType where developername='AstutePayrollAccount'];
		
		//insert AstutePayroll Account
        StyleCategory__c apAccount=new StyleCategory__c();
        apAccount.RecordTypeId=APRecordType.id;
       // apAccount.AstutePayroll_api_key__c='key';
       // apAccount.AstutePayroll_api_password__c='password';
       // apAccount.AstutePayroll_api_username__c='name';
        CommonSelector.quickInsert( apAccount);
        CredentialSettingService.insertAstutePayrollCredential(apAccount.id,'name','password','key');
         
         User currentuser= (User)new CommonSelector(User.sObjectType)
         					.setParam('userId',Userinfo.getUserId())
         					.getOne('id, AstutePayrollAccount__c ',
         					'id =: userId');
        // User currentuser=[select id, AstutePayrollAccount__c from User where id =: Userinfo.getUserId()];
         
         currentuser.AstutePayrollAccount__c=apAccount.id;
         CommonSelector.quickUpdate( currentuser);
        //Insert Account and Contact
       
         
        Account accountTest=new Account();
        accountTest.Name='TestAcc';
        CommonSelector.quickInsert( accountTest);
        
        Contact testCon=new Contact();
        testCon.lastname='Goran';
        testCon.firstname='Steve';
        testCon.email='test@example.com';
        testCon.AccountId=accountTest.Id;
       CommonSelector.quickInsert(  testCon);
        
        
//insert candidate
		Contact cand1=new Contact();
		cand1.firstname='testfirstname';
		cand1.lastname='testLastName';
		CommonSelector.quickInsert(  cand1);
// insert vacancy
		Placement__c vacancy=new Placement__c();
		vacancy.name='Test';
		vacancy.Company__c=accountTest.Id;
		vacancy.biller_contact__c=testCon.id;
		CommonSelector.quickInsert(  vacancy);
//insert Candidate Management.
				
		Placement_Candidate__c cmTest=new Placement_Candidate__c();
		cmTest.candidate__c=cand1.id;
		cmTest.Placement__c=vacancy.id;
		CommonSelector.quickInsert(  cmTest);
		ApexPages.StandardController stcon=new ApexPages.StandardController(cmTest);
		CustomizedCMButtonsController cusCmbutton=new CustomizedCMButtonsController(stcon);
		System.assertEquals(cusCmbutton.isAppear,false);
		cusCmbutton.upLoadToAP();
		cusCmbutton.checkPlacementConditions('New;Test| testing;');
		
		System.assertEquals(cusCmbutton.serverityStr,'confirm');
		}
		
	}

}