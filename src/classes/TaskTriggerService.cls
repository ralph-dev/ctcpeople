public without sharing class TaskTriggerService {
	//Update Contact Field Last Activity Date
	public static void updateContactLastActivityDate(Map<Id, Task> conTaskMap){
		Set<Id> conIds = conTaskMap.keySet();
		
		CommonSelector.checkRead(Contact.sObjectType,'Id, LastName, Last_Activity_Date__c');
		List<Contact> conList= [Select Id, LastName, Last_Activity_Date__c From Contact Where Id in:conIds];
		List<Contact> conUpdateList = new List<Contact>();
		for(Contact con: conList){
			Task t = conTaskMap.get(con.Id);
			if(t != null ){
				Date dueDate = t.ActivityDate;
				Datetime upToDate;
				if(dueDate <= Date.today()){
					Datetime lastModDate = t.LastModifiedDate;
					Time lastModTime = lastModDate.time();
					upToDate = Datetime.newInstance(dueDate, lastModTime);
					con.Last_Activity_Date__c = upToDate;
				}else{
					Time newTime = Time.newInstance(9, 0, 0, 0);
					upToDate = Datetime.newInstance(dueDate, newTime);
					con.Last_Activity_Date__c = upToDate;
				}
			}else{
				con.Last_Activity_Date__c = null;
			}
			conUpdateList.add(con);
		}
		fflib_SecurityUtils.checkFieldIsUpdateable(Contact.SObjectType, Contact.Last_Activity_Date__c);
		update conUpdateList;
	}

	//Get Contact Last Modified Task
	public static Map<Id, Task> getConLastTaskMap(List<Id> conIds){
		Map<Id, Task> conLastTaskMap = new Map<Id, Task>();
		TaskSelector selector = new TaskSelector();
		List<Task> taskList = selector.getLastModifiedTaskForContact(conIds);
		for(Id conId: conIds){
			for(Task t: taskList){
				if(conId == t.WhoId){
					conLastTaskMap.put(conId, t);
					break;
				}
			}
		}
		return conLastTaskMap;	
	} 

}