@isTest
private class ContactSelectorTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		// TO DO: implement unit test
	        Contact con = TestDataFactoryRoster.createContact();
	        
	        ContactSelector conSelector = new ContactSelector();
	        //test method fetchContactInfoById(String Id)
	        List<Contact> conListRetrievedById = conSelector.fetchContactInfoById(con.Id);
	       	System.assertNotEquals(conListRetrievedById,null);
	       	
	       	//test method fetchContactListByNameKeyword(String nameKeyword)
	       	String nameKeyword = '';
	       	List<Contact> conListRetrievedByKeyword = conSelector.fetchContactListByNameKeyword(nameKeyword);
	       	System.assertNotEquals(conListRetrievedByKeyword,null);
	
	        Map<String, String> fieldsOperator = new Map<String, String>{'LastName'=>'=','CreatedDate'=>'<'};
	        Map<String, String> fields = new Map<String, String>{'LastName'=>'Roster Test Candidate','CreatedDate'=>(system.today()+1).format()};
	        Map<String, String> fieldsType = new Map<String, String>{'LastName'=>'String','CreatedDate'=>'Datetime'};
	
	        List<Contact> cons = conSelector.getCandidatesByCriteria(fieldsOperator, fields, fieldsType);
	        system.assert(cons.size()==1);
	
	        Map<String, candidateDTO> cDTOTestMap = new Map<String, candidateDTO>();
	        candidateDTO cDTO = new candidateDTO();
	        cDTO.setCandidate(con);
	        
	        cDTOTestMap.put(con.Id, cDTO);
	        cDTOTestMap = conSelector.candidateRadiusSelector(cDTOTestMap, 10, -33.8682860, 151.2037580);
	        system.assert(cDTOTestMap.get(con.id).getRadiusRanking()==1);
    	}
        
    }
    
    private static testMethod void testGetContactListByIds() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		ContactSelector csAll = new ContactSelector();
        
	        Set<String> fns = new Set<String>();
	        fns.add('AssistantName');
	        ContactSelector csSpe= new ContactSelector(fns);
	        List<Contact> init = DataTestFactory.createContacts();
	        List<Contact> res;
	        
	        //test if input is a list 
	        res = csAll.getContactsByIds(new List<Id>{init.get(0).id});
	        System.assertEquals(init.get(0).LastName, res.get(0).LastName);
	        
	        //test if input is a set
	        res = csAll.getContactsByIds(new Set<Id>{init.get(0).id});
	        System.assertEquals(init.get(0).LastName, res.get(0).LastName);
	        
	        //test if input is a list
	        res = csSpe.getContactsByIds(new List<Id>{init.get(0).id});
	        try{
	            System.debug(res.get(0).name);
	            System.assert(false, 'have selected fields should not be select');
	        } catch (Exception e) {
	        }
	        
	        //test if input is a Set
	        res = csSpe.getContactsByIds(new Set<Id>{init.get(0).id});
	        try{
	            System.debug(res.get(0).name);
	            System.assert(false, 'have selected fields should not be select');
	        } catch (Exception e) {
	            
	        }
    	}
        
    }
    
    private static testMethod void testGetContactsMapByIds() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		ContactSelector csAll = new ContactSelector();
	        List<Contact> init = DataTestFactory.createContacts();
	        Map<Id, Contact> res;
	        
	        //test if input is a list 
	        res = csAll.getContactsMapByIds(new List<Id>{init.get(0).id});
	        System.assertEquals(init.get(0).LastName, res.get(init.get(0).id).LastName);
	        
	        //test if input is a set
	        res = csAll.getContactsMapByIds(new Set<Id>{init.get(0).id});
	        System.assertEquals(init.get(0).LastName, res.get(init.get(0).id).LastName);
    	}
        
    }
    
    private static testMethod void testGetSObjectByLimit() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		ContactSelector cs = new ContactSelector();
        
	        List<Contact> init = DataTestFactory.createContacts();
	        List<Contact> res = cs.getSObjectByLimit(1, 1);
	    
	        System.assertEquals(init.get(1).LastName, res.get(0).LastName);
    	}
        
    }
    
    private static testMethod void testGetContactForLinkedIn() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		// Create Contact
	        Contact con = new Contact();
			con.Lastname = 'Test Candidate';
			con.MailingLatitude = -33.867930;
			con.MailingLongitude = 151.205628;
			con.LinkedIn_Member_Id__c = 'test';
			insert con;
	        ContactSelector conSelector = new ContactSelector();
	        Contact cand = conSelector.getContactForLinkedIn(con.Id);
	        System.assertEquals('test', cand.LinkedIn_Member_Id__c);
    	}
        
    }
    
    private static testMethod void testGetCandLastNameMap() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		Contact con1 = new Contact(LastName='test1',email='test1@test.com');
	        Contact con2 = new Contact(LastName='test2',email='test2@test.com');
	        Contact con3 = new Contact(LastName='test3',email='test3@test.com');
	        insert con1;
	        insert con2;
	        insert con3;
	        List<Id> conIdList = new List<Id>{con1.Id, con2.Id, con3.Id};
	        Map<Id, String> candLastNameMap = ContactSelector.getCandLastNameMap(conIdList);
	        System.assertEquals('test1', candLastNameMap.get(con1.Id));
	        System.assertEquals('test2', candLastNameMap.get(con2.Id));
	        System.assertEquals('test3', candLastNameMap.get(con3.Id));
    	}
        
    }
}