public with sharing class SeekEdit {
    public Advertisement__c job{get; set;}
    public boolean is_preview{get;set;}
    public String orgid{get; set;}
    public String aid;
    public String bucketname ;
    public String screenId;
    public String LogoId;
    public String templateId;
    public Boolean show_page{get;set;}
    public boolean is_Test = false;
    public boolean is_post {get;set;}
    public Integer reminingquota{get;set;}
    integer usage , quota;
    UserInformationCon usercls = new UserInformationCon();
    User u;
    String tempSalaryText = '';
    Set<String> internationalRegions = new Set<String>();
    Map<String,String> Countries = new Map<String,String>();
    public String jobContentSeekTemp{get; set;}
    
    public boolean org_level_seekAPI{get;set;}
    /*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}    
    /*************End Add skill parsing param********************************************************/
    
    //************Start Add Multi Seek Account Param, Author by Jack on 26/05/2013******************
    public String selectSeekAccount;
    public String displayCompanyName;
    public boolean iseditable;//check the action from update ad or clone ad
    //*************End Add Multi Seek Account Param*************************************************
    
    public boolean enablePostingAdWithEmail {get; set;}
    public String preferEmailAddress;
    
    //Init Error Message for customer
    public PeopleCloudErrorInfo errormesg; 
    public Boolean msgSignalhasmessage{get ;set;} //show error message or not
    
    public SeekEdit(ApexPages.StandardController stdController){
        
        show_page = true;
        is_post = false ; 
        u = usercls.getUserDetail();
        reminingquota = usercls.checkquota(1);
        usage = usercls.usage;
        quota = usercls.quota;
        Integer checkadstatusvalue = 0;
        
        //check the action from update ad or clone ad. Update ad isedit is true, else isedit is false; 
        iseditable = true;
        String isedit = '';
        isedit = ApexPages.currentPage().getParameters().get('isedit');
        if(isedit!=null && isedit.equals('false')){
            iseditable = false;
        }
        //end check the action from update ad or clone ad
        
        //Init skill parsing param. Add by Jack on 23/05/2013
        enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
        enableSkillGroupList = new List<Skill_Group__c>();
        enableSkillGroupList = SkillGroups.getSkillGroups();
        defaultskillgroups = new List<String>();
        //end skill parsing param.
        
        //Init the Job Posting with Email function
        JobPostingSettings__c userJobPostingSetting = SeekJobPostingSelector.getJobPostingCustomerSetting();
        enablePostingAdWithEmail = userJobPostingSetting.Posting_Seek_Ad_with_User_Email__c;
        
        
        job = (Advertisement__c) stdController.getRecord();
        
        //Get prefer email address according the admin setting
        if(job.Prefer_Email_Address__c == null || job.Prefer_Email_Address__c == '') {
            job.Prefer_Email_Address__c = SeekJobPostingSelector.getUserPreferEmailAddress();
        }
        
        if(job.Skill_Group_Criteria__c!=null&&job.Skill_Group_Criteria__c!=''){
            String defaultskillgroupstring = job.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
            defaultskillgroups = defaultskillgroupstring.split(',');
        }
        
        //Check ad status by CheckAdStatus class
        checkadstatusvalue = CheckAdStatus.CheckPostingAdStatus(job);
        if(checkadstatusvalue == PeopleCloudErrorInfo.ADVERTISEMT_AD_STATUS_CORRECT){
            is_post = true;
        }
        else{
            show_page = false;
            msgSignalhasmessage = customerErrorMessage(checkadstatusvalue); 
        }
    }
     public void init(){
        if(show_page){
            //******** Added 12 July 2010*********
            boolean has_error = false;
            org_level_seekAPI = false;
        
            if(u.AmazonS3_Folder__c != null ){
                bucketname = u.AmazonS3_Folder__c.trim();
                }
            else{
                msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.ERR_S3_ACCOUNT_FAILURE); 
                show_page = false;
            }
            if(show_page){
                if(!is_Test){//** for testmethod only
                    if(Test.isRunningTest()){
                        has_error = false;
                    }
                    else{
                        has_error = CTCAccessClass.CTCAccessChecking('seek');
                    }
                    if(has_error){
                        ApexPages.Message error = new ApexPages.Message(ApexPages.severity.WARNING,'Currently you have no privilege to update this Seek.com Ad.!');              
                        ApexPages.addMessage(error);
                        show_page = false; 
                    }
                    else{
                        //Start Add multi Seek posting account
                        selectSeekAccount = '';
                        org_level_seekAPI = false;
                        if(u.Seek_Account__c != null && u.Seek_Account__c != ''){//check user seek account 
                            system.debug('iseditable='+ iseditable);
                            if(!iseditable){
                                selectSeekAccount = u.Seek_Account__c;
                            }else{
                                selectSeekAccount = job.Seek_Account__c;
                            }
                            StyleCategory__c sc = DaoStyleCategory.getSeekAccountByStyleCategoryId(selectSeekAccount);
                            if(sc!=null){
                                org_level_seekAPI = sc.Seek_Default_Application_Form__c;
                            }else{
                                org_level_seekAPI = false;
                                msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.SEEK_JOBBOARD_ACCOUT);
                                return;
                            }
                        }
                        else{
                            msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.SEEK_JOBBOARD_ACCOUT);
                            show_page = false;
                            has_error = false;
                            return;
                        }
                        //End Add multi Seek posting account************************************
                        //system.debug('new test = ' + org_level_seekAPI);
                        orgid = UserInfo.getOrganizationId().substring(0, 15);   
                        show_page = getStyles(org_level_seekAPI);
                    }
                } // if has_err
            }// if(!is_Test)
        }
        //system.debug('showpage =' + show_page);        
    }
    
   
    //insert the skill group picklist value option
    public List<SelectOption> getSelectSkillGroupList(){
        List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
        List<SelectOption> displaySelectOption = new List<SelectOption>();
        for(Skill_Group__c tempskillgroup : tempskillgrouplist){
            displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
        }
        return displaySelectOption;
    }
    
    //****************** get seek.com account template information ************
    public boolean getTemplateDetail(){    
        try{
            boolean info_valid = true;
            if(job.Template__c != null){
                CommonSelector.checkRead(StyleCategory__c.SObjectType,'Id, Name,templateSeek__c, screenId__c, LogoID__c');
                StyleCategory__c  sc = [Select Id, Name,templateSeek__c, screenId__c, LogoID__c from StyleCategory__c where Id=:job.Template__c];
                system.debug('templateid =' + job.Template__c);
                system.debug('SeekScreen__c =' + job.SeekScreen__c);
                if(sc != null){
                    if(sc.templateSeek__c == '' || sc.templateSeek__c == null){
                        info_valid = false;
                    }
                    else{
                        //----- Changed by Gilberto for enabling Seek Export API
                        CommonSelector.checkRead(StyleCategory__c.SObjectType,'Id, screenId__c');
                        StyleCategory__c[]  scSeekScreens = [Select Id, screenId__c from StyleCategory__c where Id=:job.SeekScreen__c];
                        StyleCategory__c  scSeekScreen = null; 
                        system.debug('scSeekScreens ='+ job.SeekScreen__c);           
                        //get screen Id
                        if (scSeekScreens.size() > 0){
                            scSeekScreen = scSeekScreens[0];
                        }
                        screenId = '' ;
                        if(scSeekScreen != null){
                            if(scSeekScreen.screenId__c != null){
                                screenId = scSeekScreen.screenId__c;
                            }
                        }   
                        //----- End of Changed by Gilberto for enabling Seek Export API
                        if(sc.LogoId__c == null || sc.LogoId__c == ''){
                            LogoId = '';
                        }
                        else{
                            LogoId = sc.LogoId__c;
                        }
                    templateId = sc.templateSeek__c;
                    }
                }
                else{
                    info_valid = false;
                }
            }
            else{
                info_valid = false;
            }
            return info_valid;
        }
        catch(system.exception e){
            NotificationClass.notifyErr2Dev('getTemplateDetail() - Seek Export API code', e);
        }
        return false;    
    }
    
    public pageReference dopost(){
        usage = usercls.usage;
        quota = usercls.quota;
        String jobXML = '';
        String tempfooter = '';
        
        if(job.online_footer__c != null){
            tempfooter = job.online_footer__c.replaceAll('\\[\\%User Name\\%\\]',UserInfo.getName());
            tempfooter = tempfooter.replaceAll('\\[\\%Vacancy Referece No\\%\\]',job.Reference_No__c );
        }
        job.Online_Footer__c = tempfooter;
        job.Job_Content__c = jobContentSeekTemp;
        system.debug('job ='+job.Application_Form_Style_Name__c);
        //insert skill group ext id
        if(enableskillparsing){
            String tempString  = String.valueOf(defaultskillgroups);
            tempString = tempString.replaceAll('\\(', '');
            tempString = tempString.replaceAll('\\)', '');
            tempString = tempString.replace(' ','');
            Job.Skill_Group_Criteria__c = tempString;
        }
        //end insert skill group ext id
        
        if(!iseditable || String.valueOf(job.Seek_Account__c)==''){//if action from clone ad, set seek account to current ad account, or seek account is null
            job.Seek_Account__c = selectSeekAccount;
        }
        if(!org_level_seekAPI)
            job.SeekScreen__c = '';
            
        //update job;
        if(!getTemplateDetail()){
            job.Status__c = 'Not Valid'; //** if there is no template/screen information, job can not be posted online. Just saved in SF.com
            checkFLS();
            update job; 
            return new ApexPages.StandardController(job).view();
        }
        String footer='  ';
        if(job.online_footer__c !=null){
            footer = job.online_footer__c;
        }
        System.debug('job ID = '+job.id);
        System.debug('job Use_18_Characters_as_Job_Ref__c = '+job.Use_18_Characters_as_Job_Ref__c);
        if(job.Use_18_Characters_as_Job_Ref__c != true){
            aid = String.valueOf(job.id).substring(0, 15);
        }else{
            aid = String.valueOf(job.id); //Use 18 Characters as Job Reference Code if the checkbox is true
        }
        String JobRefCode = orgid + ':' +aid; 
        String urlbase = Endpoint.getcpewsEndpoint()+'/peoplecloud/GetApplicationForm?';
        
        jobXml = JobboardgeneralXML.generalSeekJobXML(job, templateId, screenId, LogoID, JobRefCode);
        
        //System.debug('jobXml = '+jobXml); 
        
        job.JobXML__c = jobXml;
        job.WebSite__c = 'Seek';
        
        //system.debug('account ='+job.Seek_Account__c + iseditable);
        
        
        //ID newRecordTypeId = [Select Name From RecordType where DeveloperName = 'Seek_com_Advertisement'].Id;
        ID newRecordTypeId = DaoRecordType.seekAdRT.Id;
        job.RecordTypeId = newRecordTypeId;
        StyleCategory__c selectedSc = style_map.get(job.Application_Form_Style__c);
        if(selectedSc != null){
            updateDetailTable(JobRefCode, orgid, aid, bucketname,
                 selectedSc.Header_EC2_File__c ,selectedSc.Header_File_Type__c , selectedSc.Div_Html_EC2_File__c, selectedSc.Div_Html_File_Type__c, 
                 selectedSc.Footer_EC2_File__c, selectedSc.Footer_File_Type__c, job.Template__c, 'seek', job.Job_Title__c);
        }
        try{
            if(reminingquota > 0){ //** quota enough 
                if(job.Status__c == 'Clone to Post' || job.Status__c == 'Ready to Post'){
                    if(is_post){
                        job.Job_Posting_Status__c = 'In Queue';
                        job.Status__c = 'Active';
                        u.Seek_Usage__c = String.valueOf(usage + 1);
                        fflib_SecurityUtils.checkFieldIsUpdateable(User.SObjectType, User.Seek_Usage__c);
                        update u;
                    }
                }
            }
            checkFLS();
            update job;
        }
        catch(System.Exception e){  
            String err_msg = 'Seek Job Update Unsuccessfully!<br/>';
            err_msg += 'Seek update and User Usage update: Exception <br/>';
            err_msg += 'Detail: ' + e.getMessage() + '('+e.getTypeName() +')';
            err_msg += '<br/> advertisement Id: ' + job.Id +'<br/>';
            notificationsendout(err_msg,'codefail','SeekEdit', 'dopost');
        }
        System.debug('after update job...');
        return new ApexPages.StandardController(job).view();
    }
    
    @future(callout=true)
    public static void updateDetailTable(String jobRefCode, String iid, String aid, String bucketname, String hname, String htype,
        String dname, String dtype, String fname, String ftype, String divisionName, String webSite, string jobTitle){
         CTCWS.PeopleCloudWSDaoPort port = new CTCWS.PeopleCloudWSDaoPort();
         port.timeout_x = 60000;
        CommonSelector.checkRead(Advertisement__c.SObjectType,'id,Job_Posting_Status__c, Status__c');
         Advertisement__c thejob = [select id,Job_Posting_Status__c, Status__c from Advertisement__c where id=:aid];
         boolean is_valid = false;
         try{
            Integer result =  port.updateJobPostingByVid(aid, hname, htype, dname, dtype, fname, ftype,iid);            
            if(result == 1){
                is_valid = true;
            }
            else{ //** details not in EC2 yet
                boolean result2 =  port.insertJobPostingDetail(jobRefCode, iid, aid, bucketname, hname, htype, dname, dtype, fname, ftype, divisionName, webSite, jobTitle);
                if(result2){
                    is_valid = true;
                }
            }
            if(is_valid)
            {
            
            }
         }catch(System.Exception e){
            thejob.Status__c='Not Valid';
            //check FLS
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField> {
                Advertisement__c.Job_Posting_Status__c,
                Advertisement__c.Status__c
            };
            fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
            update thejob;
         }
    }

    public List<SelectOption> styleOptions = new List<SelectOption>();
    public List<SelectOption> templateOptions = new List<SelectOption>();
    public List<SelectOption> seekScreenOptions = new List<SelectOption>();
    Map<String,String> template_map = new Map<String,String>();
    Map<Id,StyleCategory__c> style_map = new Map<Id,StyleCategory__c>();
    Map<Id,StyleCategory__c> seekScreen_map = new Map<Id,StyleCategory__c>();
    public Boolean seekScreenOn = false;

    public Boolean getSeekScreenOn(){
       return seekScreenOn;
    }
     
    public List<SelectOption> getStyleOptions(){
       return styleOptions;
    }
    
    public boolean getStyles(Boolean enableseekdefault){
        boolean getAllStyle = true;
        seekScreenOn = false; 
        //**** get Ad. Templates      
        StyleCategory__c[] templates = CTCService.getOptions(1,'seek'); 
        //templateOptions.add(new SelectOption('', ''));
        if(templates != null){
            for(StyleCategory__c ts : templates){
                templateOptions.add(new SelectOption(ts.Id, ts.Name));
                template_map.put(ts.Id,ts.Name);
            }
        }
        if(enableseekdefault){
            //**** populate Seek Screen selection
            List<StyleCategory__c> seekScreens = DaoStyleCategory.getSeekScreenId(selectSeekAccount);
            if(seekScreens != null && seekScreens.size()>0){   
                seekScreenOptions.add(new SelectOption('','---None---'));       
                for(StyleCategory__c ss : seekScreens){
                    seekScreenOptions.add(new SelectOption(ss.Id, ss.Name));
                    seekScreen_map.put(ss.Id, ss);
                }
                seekScreenOn = true;     
            }
            else{
                seekScreenOn = false;
                getAllStyle = false;
                msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.SEEK_GET_SEEK_SCREEN_ERROR); 
            } 
        }
        //else{
            //**** populate application form style selection
            StyleCategory__c[] styles = CTCService.getOptions(2,null); 
            if(styles != null){ 
                styleOptions.add(new SelectOption('','--None--'));         
                for(StyleCategory__c sc : styles){
                    styleOptions.add(new SelectOption(sc.Id, sc.Name));
                    style_map.put(sc.Id, sc);
                }     
            }
            else{
                getAllStyle = false;
                msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.SEEK_GET_TEMPLATE_ERROR); 
            }
            
        //}
        return getAllStyle;       
    }
    
    public List<SelectOption> getTemplateOptions(){
        return templateOptions;
    }
    
    public List<SelectOption> getSeekScreenOptions(){
        return seekScreenOptions ;
    }
    
    static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());
    }
    
    //Generate Error message, Add by Jack on27/06/2013
    public static Boolean customerErrorMessage(Integer msgcode){
        PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        Boolean msgSignalhasmessage = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
                ApexPages.addMessage(newMessage);
            }
        return msgSignalhasmessage;               
    }

    private void checkFLS(){
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Advertisement__c.Website__c,
            Advertisement__c.RecordTypeId,
            Advertisement__c.Job_Content__c,
            Advertisement__c.Online_Footer__c,
            Advertisement__c.Skill_Group_Criteria__c,
            Advertisement__c.Reference_No__c,
            Advertisement__c.Application_Form_Style__c,
            Advertisement__c.Job_Title__c,
            Advertisement__c.Application_URL__c,
            Advertisement__c.Status__c,
            Advertisement__c.Job_Posting_Status__c,
            Advertisement__c.ListingID__c,
            Advertisement__c.Preferred_Application_Mode__c,
            Advertisement__c.Placement_Date__c,
            Advertisement__c.Provider_Code__c,
            Advertisement__c.Company_Name__c,
            Advertisement__c.Photos__c,
            Advertisement__c.Seek_Account__c,
            Advertisement__c.SeekScreen__c,
            Advertisement__c.JobXML__c
        };
        fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    }
}