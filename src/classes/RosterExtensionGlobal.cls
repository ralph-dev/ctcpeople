public with sharing class RosterExtensionGlobal {
	public RosterExtensionGlobal(Object obj){}
	
	final static String ROSTER_TYPE_CANDIDATE = 'Candidate';
	final static String ROSTER_TYPE_CLIENT = 'Client';
	
	@RemoteAction
	public static List<Days_Unavailable__c> retrieveRosterListById(String rParamJson){
		//System.debug('RosterExtensionGlobal retrieveRosterListById rParamJson '+rParamJson);
		List<RosterParamDTO> rParamDTOs = RosterHelper.deserializeRosterParamJson(rParamJson);
		if(rParamDTOs == null || rParamDTOs.size() == 0){
			return null;
		}
		//System.debug('RosterExtensionGlobal retrieveRosterListById rParamDTOs '+rParamDTOs);
		String id = rParamDTOs[0].getId();
		String rosterType = rParamDTOs[0].getRosterType();
		
		//get roster list that is saved in SF
		RosterSelector rSelector = new RosterSelector();
		List<Days_Unavailable__c> rosterList = new List<Days_Unavailable__c>();
		if(rosterType == ROSTER_TYPE_CANDIDATE){
			rosterList = rSelector.fetchRosterListByCandidateId(id);
		}
		if(rosterType == ROSTER_TYPE_CLIENT){
			rosterList = rSelector.fetchRosterListByClientId(id);
		}
		if(rosterList == null || rosterList.size() == 0){
			return null;
		}
		
		//System.debug('rosterList'+rosterList);
		System.debug('roster id: '+rosterList[0].id);
		RosterService rService = new RosterService();
		List<Days_Unavailable__c> rosterListForDisplayTotal = rService.getTotalDisplayRosterList(rosterList); 
		
		//System.debug('RosterExtensionGlobal getCandidateRoster rosterListForDisplayTotal: '+JSON.serialize(rosterListForDisplayTotal));
		
		return rosterListForDisplayTotal;	
	}
	
	@RemoteAction
	public static List<Days_Unavailable__c> retrieveRosterDBListById(String rParamJson){
		List<RosterParamDTO> rParamDTOs = RosterHelper.deserializeRosterParamJson(rParamJson);
		if(rParamDTOs == null || rParamDTOs.size() == 0){
			return null;
		}
		String id = rParamDTOs[0].getId();
		String rosterType = rParamDTOs[0].getRosterType();
		
		//get roster list that is saved in SF
		RosterSelector rSelector = new RosterSelector();
		List<Days_Unavailable__c> rosterList = new List<Days_Unavailable__c>();
		if(rosterType == ROSTER_TYPE_CANDIDATE){
			rosterList = rSelector.fetchRosterListByCandidateId(id);
		}
		if(rosterType == ROSTER_TYPE_CLIENT){
			rosterList = rSelector.fetchRosterListByClientId(id);
		}
		if(rosterList == null || rosterList.size() == 0){
			return null;
		}
		
		//System.debug('RosterExtensionGlobal getCandidateRoster rosterListForDisplayTotal: '+JSON.serialize(rosterList));

		return rosterList;	
	}
	
	@RemoteAction
	public static List<Days_Unavailable__c> retrieveRosterListByIdAndStatus(String rParamJson){
		List<RosterParamDTO> rParamDTOs = RosterHelper.deserializeRosterParamJson(rParamJson);
		if(rParamDTOs == null || rParamDTOs.size() == 0){
			return null;
		}
		String id = rParamDTOs[0].getId();
		String status = rParamDTOs[0].getStatus();
		String rosterType = rParamDTOs[0].getRosterType();
		
		//get roster list that is saved in SF
		RosterSelector rSelector = new RosterSelector();
		List<Days_Unavailable__c> rosterList = new List<Days_Unavailable__c>();
		if(rosterType == ROSTER_TYPE_CANDIDATE){
			rosterList = rSelector.fetchRosterListByCandidateIdAndStatus(id,status);
		}
		if(rosterType == ROSTER_TYPE_CLIENT){
			rosterList = rSelector.fetchRosterListByClientIdAndStatus(id,status);
		}
		if(rosterList == null || rosterList.size() == 0){
			return null;
		}
		
		RosterService rService = new RosterService();
		List<Days_Unavailable__c> rosterListForDisplayTotal = rService.getTotalDisplayRosterList(rosterList); 
		
		//System.debug('RosterExtensionGlobal retrieveCandidateRosterListByCanIdAndStatus rosterListForDisplayTotal: '
		//			+JSON.serialize(rosterListForDisplayTotal));
					
		return rosterListForDisplayTotal;	
	}
	
	@RemoteAction
	public static List<Days_Unavailable__c> updateRosterDB(String rostersJson){
		//System.debug('RosterExtensionGlobal updateRosterDB rostersJson: '+rostersJson);
		List<RosterDTO> rostersToBeUpdated = RosterHelper.deserializeRosterJson(rostersJson);
		
		RosterService rService = new RosterService();
		List<Days_Unavailable__c> rostersDB = rService.updateRosterStoredInDB(rostersToBeUpdated); 
		if(rostersDB == null || rostersDB.size() == 0){
			return null;
		}
		
		//System.debug('RosterExtensionGlobal updateRosterDB rostersDB: '+JSON.serialize(rostersDB));
		
		return rostersDB;
	}
	
	@RemoteAction
	public static List<Days_Unavailable__c> retrieveRosterListByRosterId(String rosterId){
		//get roster list that is saved in SF
		RosterSelector rSelector = new RosterSelector();
		List<Days_Unavailable__c> rosterList = rSelector.fetchRosterListByRosterId(rosterId);
		if(rosterList == null || rosterList.size() == 0){
			return null;
		}
		
		// split single roster into roster list and saved in the collection of roster list
		RosterService rService = new RosterService();
		List<Days_Unavailable__c> rosterListForDisplay = rService.getTotalDisplayRosterList(rosterList);
				
		//System.debug('RosterExtensionGlobal retrieveCandidateRosterListByRosterId rosterListForDisplay: '+JSON.serialize(rosterListForDisplay));
		return rosterListForDisplay;
	}
	
	@RemoteAction
	public static List<String> retrieveUnavailDatesForCandidate(String canId){
		// split single roster into roster list and saved in the collection of roster list
		RosterService rService = new RosterService();
		List<String> unavailDateList = rService.getUnavailableDateStrListForCandidate(canId);
		
		//System.debug('RosterExtensionGlobal retrieveUnavailDatesForCandidate unavailDateList: '+unavailDateList);
		
		return unavailDateList;
	}
	
	@RemoteAction
	public static  Map<String,List<ShiftDTO>> retrieveOpenVacanciesMapForClient(String clientId){
		ShiftService sService = new ShiftService();
		Map<String,List<ShiftDTO>> vacancyShiftDTOsMap = sService.groupOpenShiftsForClient(clientId);
		return vacancyShiftDTOsMap;
	}
	
	@RemoteAction
	public static List<ShiftDTO> retrieveOpenVacanciesForClient(String clientId){
		ShiftService sService = new ShiftService();
		List<ShiftDTO> vacancyShiftDTOList = sService.getOpenShiftsForClient(clientId);
		return vacancyShiftDTOList;
	}
	
	@RemoteAction
	public static List<ShiftDTO> retrieveOpenVacanciesStoredInDB(String clientId){
		ShiftService sService = new ShiftService();
		List<ShiftDTO> vacancyShiftDTOList = sService.getOpenShiftsStoredInDatabase(clientId);
		return vacancyShiftDTOList;
	}
	
	
	@RemoteAction
	public static ShiftDTO retrieveShiftDBByShiftId(String shiftId){
		ShiftService sService = new ShiftService();
		List<ShiftDTO> shiftDTOList = sService.getShiftDBByShiftId(shiftId);
		if(shiftDTOList == null || shiftDTOList.size() == 0){
			return null;
		}
		
		return shiftDTOList[0];
	}
	
	@RemoteAction
	public static Map<String,ShiftDTO> retrieveVacancyNameIdMapFromShifts(String clientId){
		ShiftSelector sSelector = new ShiftSelector();
		List<Shift__c> shiftList = sSelector.fetchOpenShiftListByAccountId(clientId);
		Map<String,ShiftDTO> vacancyIdNameMap = ShiftHelper.groupVacancyIdNameFromShifts(shiftList);
		return vacancyIdNameMap;
	}
	
	@RemoteAction
	public static Boolean deleteRosterById(String rosterId){
		//get roster list that is saved in SF
		RosterService rService = new RosterService();
		Boolean isDeleted = rService.deleteRosterById(rosterId);
		
		System.debug('RosterExtensionGlobal deleteRosterById: '+isDeleted);
		
		return isDeleted;
	}
	
	@RemoteAction
    public static Map<String,List<Schema.PicklistEntry>> describeRoster(){
     	Map<String, Schema.DescribeFieldResult> resultMap = DescribeHelper.getDescribeFieldResult(Days_Unavailable__c.sObjectType.getDescribe().fields.getMap().values());
        Map<String,List<Schema.PicklistEntry>> describeResultMap = new Map<String,List<Schema.PicklistEntry>>();
        describeResultMap.put('ShiftType',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Shift_Type__c').picklistValues);
        describeResultMap.put('Status',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Event_Status__c').picklistValues);
        //describeResultMap.put('RateType',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Rate_Type__c').picklistValues);
       
        //System.debug('Describe Result'+JSON.Serialize(describeResultMap));
        
        return describeResultMap;        
    } 

    @RemoteAction
    public static List<Schema.PicklistEntry> EventStatus() {
    	List<Schema.PicklistEntry> statusPickListValue = new List<Schema.PicklistEntry>();
    	Map<String, Schema.DescribeFieldResult> resultMap = DescribeHelper.getDescribeFieldResult(Days_Unavailable__c.sObjectType.getDescribe().fields.getMap().values());
    	statusPickListValue = resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Event_Status__c').picklistValues;

    	return statusPickListValue;
    }
}