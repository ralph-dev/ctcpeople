/**
 * This the test class for DaxtraCredentialSelector
 * Created by: Lina
 * Created date: 19/05/2016
 * */

@isTest
public class DaxtraCredentialSelectorTest {
    
    static testmethod void unitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Create data
        DaxtraCredential__c dc = new DaxtraCredential__c();
        dc.Service_Endpoint__c = 'www.test.com';
        dc.Username__c = 'username';
        dc.Password__c = 'password';
        dc.Database__c = 'database';
        dc.Name = 'test';
        insert dc;
        
        DaxtraCredential__c dcSelected = DaxtraCredentialSelector.getDaxtraCredential();
        System.assertEquals('www.test.com', dcSelected.Service_Endpoint__c);
        System.assertEquals('username', dcSelected.Username__c);
        System.assertEquals('password', dcSelected.Password__c);
        System.assertEquals('database', dcSelected.Database__c);
    	}
    }
    
    static testmethod void testNull() {
    	System.runAs(DummyRecordCreator.platformUser) {
        DaxtraCredential__c dcSelected = DaxtraCredentialSelector.getDaxtraCredential();
        System.assertEquals(null, dcSelected);
    	}
    }
    
    static testmethod void testMultidc() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Create data
        DaxtraCredential__c dc = new DaxtraCredential__c();
        dc.Service_Endpoint__c = 'www.test.com';
        dc.Username__c = 'username';
        dc.Password__c = 'password';
        dc.Database__c = 'database';
        dc.Name = 'test';
        insert dc;

        DaxtraCredential__c dc2 = new DaxtraCredential__c();
        dc2.Service_Endpoint__c = 'www.test2.com';
        dc2.Username__c = 'username2';
        dc2.Password__c = 'password2';
        dc2.Database__c = 'database2';
        dc2.Name = 'test2';
        insert dc2;        
        
        DaxtraCredential__c dcSelected = DaxtraCredentialSelector.getDaxtraCredential();
        System.assertEquals('www.test.com', dcSelected.Service_Endpoint__c);
        System.assertEquals('username', dcSelected.Username__c);
        System.assertEquals('password', dcSelected.Password__c);
        System.assertEquals('database', dcSelected.Database__c);
    	}
    }
}