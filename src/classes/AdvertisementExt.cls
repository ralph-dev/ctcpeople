/**
* modified by andy for security review II
*/

public with sharing class AdvertisementExt{
//** Clone existing Ad template to a new one
    public Advertisement__c theTemplate {get;set;}
    public String errormsg {get;set;}
    public Boolean haserror {get;set;} //*** to control of displaying the vf page or not 
    
    //*** Loading page to check whether the record is validate or not 
    //*** The Record to clone must be a Template_Record Type.
    //*** Calling URL must have 'id' parameter which is template record id.
    public AdvertisementExt(){
        try{
            errormsg = '';
            haserror = false;
            
            theTemplate =(Advertisement__c) CommonSelector.simpleQuery(Advertisement__c.sObjectType)
            	.setParam('id',ApexPages.currentPage().getParameters().get('id'))
            	.getOne('Id,RecordType.developerName,Vacancy__c,Vacancy__r.Name,Reference_No__c,Residency_Required__c,'+
                            'Job_Content__c, Online_Footer__c,Job_Title__c,Salary_Min__c, Salary_Max__c,'+
                            'Currency_Type__c, Online_Search_Title__c,Online_Summary__c, Bullet_1__c,Bullet_2__c,'+
                            'Bullet_3__c, Job_Content_Rich__c,OnlineFooter__c',
                            
                            'Id=: id');
            /**             
            theTemplate = [select Id,RecordType.developerName,Vacancy__c,Vacancy__r.Name,Reference_No__c,Residency_Required__c,
                            Job_Content__c, Online_Footer__c,Job_Title__c,Salary_Min__c, Salary_Max__c,
                            Currency_Type__c, Online_Search_Title__c,Online_Summary__c, Bullet_1__c,Bullet_2__c,
                            Bullet_3__c, Job_Content_Rich__c,OnlineFooter__c
                            from Advertisement__c where Id=: ApexPages.currentPage().getParameters().get('id')];
            */
            if(theTemplate.RecordType.developerName != 'Template_Record'){
                errormsg = 'Sorry. Only template can be cloned!';
                haserror = true; //** hide the page and show error message
            } 
        }
        catch(system.Exception e){        
                errormsg = 'Some exception encountered. Please contact your admin.';
                haserror = true; 
        }      
    }

    //*** create new template record ***//
    public PageReference dosave(){
        try{
	        Advertisement__c newtemplate = theTemplate.clone(false,true);
	        newtemplate.Job_Content_Rich__c = newtemplate.Job_Content__c ;
	        newtemplate.OnlineFooter__c = newtemplate.Online_Footer__c ;
            //check FLS
            /**
            List<String> fieldList = new List<String>{
                'Vacancy__c','Residency_Required__c',
                'Job_Content__c', 'Online_Footer__c','Job_Title__c','Salary_Min__c', 'Salary_Max__c',
                'Currency_Type__c', 'Online_Search_Title__c','Online_Summary__c', 'Bullet_1__c','Bullet_2__c',
                'Bullet_3__c', 'Job_Content_Rich__c','OnlineFooter__c'
            };
            fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
            */
	        CommonSelector.quickInsert( newtemplate);
	        PageReference pageRef = new PageReference('/'+newtemplate.Id);
	        pageRef.setRedirect(true);
	        return pageRef;
        }
        catch(system.Exception e){
        }
        return null;
    }
    //*** cancel "clone" activity ***//
    public PageReference docancel(){
        PageReference pageRef = new PageReference('/'+theTemplate.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
}