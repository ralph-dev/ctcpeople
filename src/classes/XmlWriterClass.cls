/****
 this is used for xml writing in advanced search configuration page. 
 Source from apex class: SearchAdminController

 other apex class:  FieldsClass
 any hardcode: Yes
 log@clicktocloud.com
 
 GeneralSelectedFieldsListJsonDoc Method to Create Search fields Json Doc. Author by Jack on 17 Oct 2013
*/
public class XmlWriterClass{

	public Blob blobxml; // new XML doc body
	String xmlOutput;
	String SelectedObject = '';
	String RecordTypeId ='';

	List<FieldsClass.SelectedField> selectedfields = new List<FieldsClass.SelectedField>{};
	List<FieldsClass.AvailableField> selectedColumns = new List<FieldsClass.AvailableField>{};
	public List<FieldsClass.SelectedField> getSelectedfields() {
		return selectedfields ;
    }
	public void setSelectedfields(List<FieldsClass.SelectedField> a) {
		selectedfields = a;
    }

	public XmlWriterClass(List<FieldsClass.SelectedField> results, String objType, String RTId, List<FieldsClass.AvailableField> columns) {
	//** currently the objType can be Contact or Placement__c
	    SelectedObject = objType; 
	    selectedfields = results;
	    RecordTypeId = RTId;
	    selectedColumns = columns;
	    if(selectedfields != null && SelectedObject != null){
	        Configuration();
	        GeneralSelectedFieldsListJsonDoc(selectedfields , SelectedObject);//General JSON doc for search fields
	    }
	}

	public void constructXMLFile() {
	    String compStr='';	    
	    
	    if(selectedfields != null)
	    {
	        if(selectedfields.size()>0)
	        {
	            XmlStreamWriter w = new XmlStreamWriter();
	            w.writeStartDocument(null, '1.0');
	            w.writeComment('file body starts here');
	        	w.writeStartElement(null, 'fileroot', null);
	            w.writeStartElement(null, 'object' , null);
	            w.writeAttribute(null, null, 'objectname', SelectedObject);
	            for(FieldsClass.SelectedField s: selectedfields){//General Search Criteria to XML
	                w.writeStartElement(null, 'field', null);
	
	                compStr = generalComponentString(s, SelectedObject);
	                
	                if(s.getFieldApi().toLowerCase() == 'recordtypeid'){
	                    constructLeafElement(w,'type' , 'PICKLIST', null);
	                }
	                else{
	                    constructLeafElement(w,'type' , s.getFieldType(), null);
	                }
	                
	                constructLeafElement(w,'label' , s.getFieldLabel(), null);
	                constructLeafElement(w,'component' , null, compStr);
	                constructLeafElement(w,'apiname' , s.getFieldApi(), null);
	                constructLeafElement(w,'location' , s.getLocation(), null);
	                constructLeafElement(w,'sequence' , String.valueOf(s.getSequence()), null);
	                constructLeafElement(w,'filterable' , String.valueOf(s.getIsFilterable()), null);
	                w.writeEndElement(); // end field
	            }
	            w.writeEndElement(); // end object
	            
	            w.writeStartElement(null, 'columns' , null);
	            for(FieldsClass.AvailableField af :selectedColumns){//Genereal Selected Columns to XML
	                 w.writeStartElement(null, 'column', null);
	                 constructLeafElement(w,'label' , af.getFieldLabel(), null);
	                 constructLeafElement(w,'obj' , af.getObjectname(), null);
	                 constructLeafElement(w,'api' , af.getFieldApi(), null);
	                 constructLeafElement(w,'type' , af.getFieldType(), null);
	                 if(af.getFieldType().toLowerCase().trim() != 'multipicklist'){
	                    constructLeafElement(w,'sortable' , 'true', null);
	                 }
	                 else{
	                    constructLeafElement(w,'sortable' , 'false', null);
	                 }
	                 w.writeEndElement();
	            }
	            w.writeEndElement(); // end columns
	            w.writeEndElement(); //end fileroot
	            w.writeEndDocument();
	            xmlOutput = w.getXmlString();
	            blobxml = Blob.valueOf(xmlOutput);
	        }
	    }
	}
	public void constructLeafElement(XmlStreamWriter w, String NodeName, String Characters, String CDdata){
	    
	        if(NodeName.toLowerCase() != 'component')
	        {
	            w.writeStartElement(null,NodeName , null);
	            if (Characters==null)
	            	Characters='EMPTY_STRING';
	            w.writeCharacters(Characters);
	            w.writeEndElement(); // end label
	        }
	        else
	        {
	            w.writeStartElement(null,NodeName, null);
	            w.writeCData(CDdata);
	            w.writeEndElement(); // end component	        
	        }
	}
	
	public void Configuration(){
	    Document d;
	    constructXMLFile();
	    String namestr = '';
	    if(SelectedObject.endsWith('__c')) 
	    {
	        Integer l = SelectedObject.length() -3 ;
	        namestr = SelectedObject.substring(0,l);} 
	    else
	    {
	        namestr = SelectedObject;   
	    }
	    namestr = 'ClicktoCloud_Xml_'+namestr;
	    d= DaoDocument.getCTCConfigFileByDevName(namestr);
	    d.Body = blobxml;
	    fflib_SecurityUtils.checkFieldIsUpdateable(Document.getSObjectType(), Document.Body);
	    fflib_SecurityUtils.checkFieldIsInsertable(Document.getSObjectType(), Document.Body);
	    upsert d;
	}
	
	//get Json document
	public static Document GeneralSelectedFieldsListJsonDoc(List<FieldsClass.SelectedField> selectedfields, String SelectedObject){
		Document tempdoc = new Document();
		String namestr = '';
		String doctype = '';
		String bodyString = '';
		List<Component> tempcomponentlist = new List<Component>();
		
		tempcomponentlist = generalComponentList(selectedfields , SelectedObject);
		if(tempcomponentlist != null && tempcomponentlist.size()>0){
			bodyString = Json.serialize(tempcomponentlist);
			List<Component> tem =(List<Component>)JSON.deserialize(bodyString, List<Component>.class);
		}
	    if(SelectedObject.endsWith('__c')) 
	    {
	        Integer l = SelectedObject.length() -3 ;
	        namestr = SelectedObject.substring(0,l);} 
	    else
	    {
	        namestr = SelectedObject;   
	    }
	    namestr = 'ClicktoCloud_JSON_'+ namestr;
		
		tempdoc = DaoDocument.createdocunderCTCADMINfolder(bodyString, namestr, namestr);
		return tempdoc;
	}
	
	//General the html code for XML document
	public static String generalComponentString(FieldsClass.SelectedField s, String SelectedObject){
		String compStr = '';
		String googlesearchfieldoperator = '';
		List<String> picklistvalues = new List<String>();
		List<sObjectType> relatedobj = new List<sObjectType>();
		
		if(s.getFieldType().toLowerCase() == 'picklist'){// Get Picklist field XML
			picklistvalues = s.getSelectedPicklistValues();
			if(picklistvalues != null){
				googlesearchfieldoperator = '<div><SELECT  name="CTC_XML_OPERATOR" SIZE="1" id="CTC_OPERATOR_' + s.getFieldApi() + '" ><OPTION VALUE="Include">Includes</OPTION><OPTION VALUE="excludes">Excludes</OPTION>';
				compStr ='<div><SELECT  name="CTC_XML_COMPONENT" SIZE="1" id="CTC_' + s.getFieldApi() + '" >';
	           
	            for(String ss: picklistvalues ){
	            	compStr = compStr + '<OPTION VALUE="'+ ss +'" >' + ss ;
	                }
	                compStr = compStr + '</SELECT></div>';
	             }
	        }
	        else if(s.getFieldType().toLowerCase() == 'date' || s.getFieldType().toLowerCase() == 'datetime'){//General Date Field XML
	        	compStr ='<div><SELECT  name="CTC_XML_COMPONENT" id="CTC_' + s.getFieldApi() + '" >';
	            compStr = compStr + '<OPTION VALUE="1" >Today</OPTION>' ; 
	            compStr = compStr + '<OPTION VALUE="7">Last 7 Days</OPTION>' ;  
	            compStr = compStr + '<OPTION VALUE="14">Last 14 Days</OPTION>' ;  
	            compStr = compStr + '<OPTION VALUE="30">Last 30 Days</OPTION>' ; 
	            compStr = compStr + '<OPTION VALUE="60">Last 60 Days</OPTION>' ;  
	            compStr = compStr + '<OPTION VALUE="90">Last 90 Days</OPTION>' ;  
	            compStr = compStr + '</SELECT></div>';
	         }
	         else if(s.getFieldType().toLowerCase() == 'textarea'){//General textarea field
	         	compStr = '<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_' + s.getFieldApi() + '" value="" style="width:350px;" /></div>';
	         }
	         else if(s.getFieldType().toLowerCase() == 'multipicklist'){   
	         	picklistvalues = s.getSelectedPicklistValues();
	            if(picklistvalues != null){
	            	compStr ='<div><SELECT  name="CTC_XML_COMPONENT" multiple SIZE="5" id="CTC_' + s.getFieldApi() + '" >';
	           
	                for(String ss: picklistvalues ){
	                	compStr = compStr + '<OPTION VALUE="'+ ss +'" >' + ss ;
	                }
	            compStr = compStr + '</SELECT></div>';
	            }
	         }
	         else if(s.getFieldType().toLowerCase() == 'reference'){
	         	if(s.getFieldApi().toLowerCase() == 'recordtypeid'){
	         		//RecordType[] rts = [select id,name from RecordType where SobjectType=: SelectedObject and IsActive = true ];
					RecordType[] rts =DaoRecordType.getActiveRecordTypesByObjectType(SelectedObject);
	                if(rts != null){
	                	if(rts.size()>0){
	                		compStr ='<div><SELECT  name="CTC_XML_COMPONENT" id="CTC_' + s.getFieldApi() + '" >';
	                                
	                        for(RecordType rt: rts){
	                        	compStr = compStr + '<OPTION VALUE="'+ rt.Id +'" >' + rt.Name ;                                   
	                        }
	                        compStr = compStr + '</SELECT></div>';
	                    }
	                }
	           	}
	            else{
	            	List<FieldsClass.AvailableField> temp_all_fields = new List<FieldsClass.AvailableField>{};
	                        
	                relatedobj = s.getRelatedObject();
	                temp_all_fields = s.getRelatedObjectFields();
	                if(relatedobj!= null && relatedobj.size() ==1 ){
	                // ** if the relatedobj has more than one entry, the s.getRelationshipName()should be not correct. in study
	                	if(temp_all_fields.size()>0){
	                    	compStr = '<div><SELECT  name="CTC_XML_COMPONENT_SELECT"  SIZE="1" id="CTC_SELECT_' + s.getFieldApi() + '" >';
	                        for(FieldsClass.AvailableField af: temp_all_fields ){
	                        	compStr = compStr + '<OPTION VALUE="'+ s.getRelationshipName() + '.' + af.getFieldApi() +'" >' + af.getFieldLabel() ;
	                        }
	                        compStr = compStr + '</SELECT>';
	                        compStr = compStr + '&nbsp;&nbsp;<input  name="CTC_XML_COMPONENT_INPUT" type="text" id="CTC_INPUT_' + s.getFieldApi() + '" value=""/></div>';
	                    }
	                    else{
	                    	compStr = null;
	                    }
	                 }
	                 else{
	                 	compStr = '<div>This Reference field is related to more than one object. That can not be searchable so far. Please contact your system administrator. </div>';
	                 }
	            }
	       }else{
	       		compStr = '<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_' + s.getFieldApi() + '" value=""/></div>';
	       }
	    return compStr;
	}
	
	//Convert the selectedfields to List Component
	public Static List<Component> generalComponentList(List<FieldsClass.SelectedField> selectedfields, String SelectedObject){
		
	    List<Component> generalComponentlist = new List<Component>();
	    if(selectedfields != null && selectedfields.size()>0){
	    	for(FieldsClass.SelectedField s: selectedfields){
	    		String label;
			    String apiname; // key
			    String fieldtype;
			    String location;
			    String sequence;
	    		//String compStr = ''; 
	    		//compStr = generalComponentString(s, SelectedObject); not in the JSON string.
	    		
	    		if(s.getFieldApi().toLowerCase() == 'recordtypeid'){
	            	fieldtype = 'PICKLIST';
	            }
	            else{
	            	fieldtype = s.getFieldType();
	            }
	            
	            label = s.getFieldLabel();
	           	apiname = s.getFieldApi();
	            location = s.getLocation();
	            sequence = String.valueOf(s.getSequence());
	            Component tempcomponent = new Component(label, fieldtype, apiname, sequence, location );
	            generalComponentlist.add(tempcomponent);
	    	}
	    }
	    return generalComponentlist;
	}
	
	public static testMethod void test1(){
		System.runAs(DummyRecordCreator.platformUser) {
	    List<FieldsClass.SelectedField> results = new List<FieldsClass.SelectedField>();
	    List<FieldsClass.AvailableField> cols = new List<FieldsClass.AvailableField>();
	    FieldsClass.SelectedField temp = new FieldsClass.SelectedField('test','test__c', 'String',null,null,null,true);
	    FieldsClass.SelectedField temp3 = new FieldsClass.SelectedField('test','test__c', 'Picklist',null,null,null,true);
	    FieldsClass.AvailableField temp2 = new FieldsClass.AvailableField('label','obj', 'label', 'api', 'type', false);
	    temp.setLocation('left');
	    temp.setSequence(0);
	    results.add(temp);
	    results.add(temp3);
	    cols.add(temp2) ;
	    XmlWriterClass thecontroller = new XmlWriterClass( results, 'Contact','000000000000000',cols);
	    system.assert(thecontroller.blobxml!=null);
	    Document tempdoc = new Document();
	    tempdoc = GeneralSelectedFieldsListJsonDoc(results, 'Contact');
	    system.assert(tempdoc!= null);
		}
	}

}