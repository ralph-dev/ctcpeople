public with sharing class OperatorsHandle {
    private String fieldname;
    private String operatorname;
    private String typename;
    private String values;
    private Decimal intvalues;
 	private String jsonvalues;
    
    public OperatorsHandle(String fieldn, String operator,String typen,String val){
        fieldname=fieldn;
        operatorname=operator;
        typename=typen;
        values=val;
    }
	
	/*************************The method get the List of the string and it will get the operator*************************************/    
    public OperatorsHandle(String jsonstr){	
		jsonvalues=jsonstr;
		
    }
    
    public String getqueries(){
    	List<Object> jsonlist=new List<Object>();
		jsonlist=(List<Object>)JSON.deserializeUntyped(jsonvalues);
		String queryclause='';
		boolean firsttime=true;
		if(jsonlist.size()!=0){
			for(Integer i=0;i<jsonlist.size();i++){
				Map<String,Object> obj=(Map<String,Object>)jsonlist.get(i);             
                values=(String)obj.get('values');
                fieldname=(String)obj.get('fieldname');
                typename=(String)obj.get('fieldtype');
                operatorname=(String)obj.get('operator');
                if(firsttime){
                    queryclause=getQuery();
                    firsttime=false;
                }else{
                    queryclause=queryclause+' AND '+getQuery();
                }
				
			}
			
			CommonSelector.checkRead(Contact.sObjectType, 'id');
			queryclause='select id from Contact where '+queryclause;
		}
		queryclause=queryclause.trim();
		return queryclause;
    }
    
    
    
    /*
        According to the values, operators, field and type, generate the query strings.
    
    **/
    public String getQuery(){
        String querystr;
        if(values!=''){     
        if(typename.equals('DATE')){
                
                String[] strsquery=values.split('/');                
                 if(strsquery[0].length()==1){
	                values=strsquery[2]+'-'+strsquery[1]+'-0'+strsquery[0];
	            }else{
	                values=strsquery[2]+'-'+strsquery[1]+'-'+strsquery[0];
	            }            
            
        }
       if(typename.equals('DATETIME')){
            
            String[] strsquery=values.split('/');
            if(strsquery[0].length()==1){
                values=strsquery[2]+'-'+strsquery[1]+'-0'+strsquery[0];
            }else{
                values=strsquery[2]+'-'+strsquery[1]+'-'+strsquery[0];
            }
            values=values+'T00:00:00Z';
    
       }
       if(typename.equals('MULTIPICKLIST')){
             	       		
           if(operatorname.contains('equals')){
              	values=values.replace(',',';');
              	values='\''+values+'\'';
            	querystr=fieldname +' '+transferoperators(operatorname)+' '+values;
            }else{
            	values=values.replace('\",\"', '{;');
                String[] strsquery=values.split(',');
            	Boolean firsttime=true;
            	String valuestr='';
           	    for(String value_unit: strsquery){
            		if(value_unit.contains('{;')){
                    value_unit=value_unit.replace('{;',',');
                	}
                	 value_unit='\''+value_unit+'\'';
                	 if(firsttime){
                    	valuestr=value_unit;
                    	firsttime=false;
	                
	                }else{
	                    valuestr=valuestr+','+value_unit;
	                }
            		
            	}
            	values=valuestr;            	            	
            	if(operatorname.equals('contains')){
            		querystr=fieldname +' includes ('+values+')';
            	}else{
            		querystr=fieldname +' excludes ('+values+')';
            	}
            }           
       		querystr='('+querystr+')';
       }
       if(typename.equals('PICKLIST')){
      
            values=values.replace('\",\"', '{;');
            String[] strsquery=values.split(',');
            querystr='';
            Boolean firsttime=true;
            for(String value_unit: strsquery){
                //if the value contains ','  then it should be changed back here.
                if(value_unit.contains('{;')){
                    value_unit=value_unit.replace('{;',',');
                }
             
                value_unit='\''+value_unit+'\'';
             
                
                String queryunit=fieldname +' '+transferoperators(operatorname)+' '+value_unit;
                if(firsttime){
                    querystr=queryunit;
                    firsttime=false;
                
                }else{
                    querystr=querystr+' OR '+queryunit;
                }
            }
            querystr='('+querystr+')';           
       }
       
       
       //For the Date, it is special, the previous step is used to change the values of the date.
           if(!typename.contains('PICKLIST')){
                if(!typename.contains('BOOLEAN') && !typename.contains('DECIMAL') && !typename.contains('DOUBLE') && !typename.contains('DATE')&& !typename.contains('INTEGER')&&!typename.contains('CURRENCY')&&!typename.contains('PERCENT')){
                    //If the values contains "'", then we will use the \\\. In the first translation \\\' would be like '\'' 
                    if(values.contains('\'')){
                       values=values.replace('\'','\\\''); 
                   }
                    if(operatorname.equals('contains')){
                        values='\'%'+values+'%\'';
                    }else{
                        if(operatorname.equals('start with')){
                            values='\''+values+'%\'';
                        }else{
                            if(operatorname.equals('end with')){
                                values='\'%'+values+'\'';
                            }
                            else{
                                values='\''+values+'\'';
                            }   
                        }
                    }
                }
                
                querystr=fieldname +' ' + transferoperators(operatorname)+' '+values;
                /***********For records with not contains, use not query format.******************/
                if(operatorname.equals('not contains')){
                	querystr='( not '+querystr+')';
                }
           }    

       }else{       	
        querystr=fieldname +' ' + transferoperators(operatorname)+' '+'NULL';
        if(querystr.contains('not like')){
        	querystr=querystr.replace('like','!=');
        }
        if(querystr.contains('like')){
        	querystr=querystr.replace('like', '=');
        	
        }
       }
        
        
        return querystr;
        
    }
    /*
        Used to Translate the string to operators.
    
    **/
    public String transferoperators(String stroper){
        String operatranserred='';
        if(stroper.equals('equals')){
            operatranserred=' = ';
        }
        if(stroper.equals('not equals')){
            operatranserred=' !=';
        }
        if(stroper.equals('less than')){
            operatranserred=' < ';
        }
        if(stroper.equals('greater than')){
            operatranserred=' > ';
        }
        if(stroper.equals('greater or equal')){
            operatranserred=' >= ';
        }
        if(stroper.equals('less or equal')){
            operatranserred=' =< ';
        }
        if(stroper.equals('contains') || stroper.equals('start with') || stroper.equals('end with')){
            operatranserred=' like ';
        }
        if(stroper.equals('not contains')){
            operatranserred=' like ';
        }
        return operatranserred;
    }
    
    
    

}