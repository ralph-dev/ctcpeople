@isTest
public class SkillExtensionGlobalTest {
    public static testMethod void myunitTest(){
    	System.runAs(DummyRecordCreator.platformUser) {
        List<Skill_Group__c> skillGroupListData = DataTestFactory.createSkillGroup();
        List<Skill__c> skillList = DataTestFactory.createSkills(skillGroupListData[0]);
        SkillExtensionGlobal controller = new SkillExtensionGlobal(skillGroupListData);
        List<Skill_Group__c> skillGroupList = SkillExtensionGlobal.searchSkillGroups();
        system.assertEquals(skillGroupList.size(),10);
        List<String> queryString = new List<String>();
        for(Skill_Group__c sg : skillGroupListData){
            queryString.add(sg.Id);
        }
        String s = SkillExtensionGlobal.retrieveSkillsJSON(queryString);
    	}
    }
}