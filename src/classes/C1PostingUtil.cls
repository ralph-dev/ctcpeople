public class C1PostingUtil {
    List<JobPostingEntity> postings = new List<JobPostingEntity>();
    public Integer getPostingsCount(){
        return postings.size();
    }
    //public String orgid = UserInfo.getOrganizationId(); 
    public String orgid = UserInfo.getOrganizationId().subString(0, 15); 
    private String companyusername ;
    private String companycode ;
    private String companypwd ;
    
    User cur_u = null;// [select Id, CareerOne_Usage__c,Monthly_Quota_CareerOne__c from User where Id=:UserInfo.getUserId()];
    
    public C1PostingUtil(String accountname, String accountcode, String accountpwd){
        companyusername = accountname;
        companycode = accountcode;
        companypwd = accountpwd; 
        
        CommonSelector.checkRead(User.sObjectType, 'Id, CareerOne_Usage__c,Monthly_Quota_CareerOne__c');
        cur_u = [select Id, CareerOne_Usage__c,Monthly_Quota_CareerOne__c from User where Id=:UserInfo.getUserId()];
    }
    
    private String constructPosting(Advertisement__c job){
        //get templateid
        string templateid ;
        if(job.Template__c != null)
        {
        	CommonSelector.checkRead(StyleCategory__c.sObjectType,'Id,templateC1__c');
            StyleCategory__c sc = [select Id,templateC1__c from StyleCategory__c where id=: job.Template__c];
            if(sc != null)
            {
                if(sc.templateC1__c != null)
                {
                    templateid = sc.templateC1__c;
                }
                else
                {
                    //************** suzan *******************
                    String err_msg = 'CareerOne Job Posting Error<br/>';
                    err_msg += 'Error Location: C1PostingUtil/constructPosting/templateC1__c';
                    err_msg += '<br/> advertisement Id: ' + job.Id;
                    notificationsendout(err_msg,'codefail','C1PostingUtil', 'constructPosting');
                    //***************************************
                    return null;
                }
            }
            else{
            	String err_msg = 'CareerOne Job Posting Error<br/>';
                err_msg += 'Error Location: C1PostingUtil/constructPosting/sc';
                err_msg += '<br/> advertisement Id: ' + job.Id;
            	notificationsendout(err_msg,'codefail','C1PostingUtil', 'constructPosting');
            	return null;
            }
        }
        else
        {
            //************** suzan *******************
            String err_msg = 'CareerOne Job Posting Error<br/>';
            err_msg += 'Error Location: C1PostingUtil/constructPosting/Template__c';
            err_msg += '<br/> advertisement Id: ' + job.Id;
            notificationsendout(err_msg,'codefail','C1PostingUtil', 'constructPosting');
            //***************************************
            return null;
        }
        
        // get location, industry, category, occupation arrays 
        String[] locations;
        String[] industries; 
        String[] categories;
        String[] occupations;
        String jobpostingstr = '';
        
        try{
        	system.debug('locations =' +job.SearchArea_String__c);
            locations = job.SearchArea_String__c.split('\\|\\|\\|');
            industries = job.Industry_String__c.split('\\|\\|\\|');
            categories = job.Cat_String__c.split('\\|\\|\\|');
            occupations = job.Occ_String__c.split('\\|\\|\\|');
        }catch(Exception e){

            //************** suzan *******************
            String err_msg = 'CareerOne Job Posting Exception<br/>';
            err_msg += 'Error Location: 3';
            err_msg += '<br/> advertisement Id: ' + job.Id;
            err_msg += '<br/> Detail: ' + e + '<br/>';
            notificationsendout(err_msg,'codefail','C1PostingUtil', 'constructPosting');
            //***************************************
            return null;
        }
   
    if(locations.size() != occupations.size() || occupations.size() != categories.size()){
        //************** suzan *******************
        String err_msg = 'CareerOne Job Posting Exception<br/>';
        err_msg += 'Error Location: 4';
        err_msg += '<br/> advertisement Id: ' + job.Id;
        
        notificationsendout(err_msg,'codefail','C1PostingUtil', 'constructPosting');
        //***************************************
        return null;
    }
    Map<String,List<String>> map_posting = new Map<String,List<String>>(); //*** occupations
    Map<String,List<String>> map_posting2 = new Map<String,List<String>>(); //*** industries
    Set<String> templist = new Set<String>();
    Set<String> templist2 = new Set<String>();
    List<String> key_list = new List<String>();
    
    String tempKey;
    //system.debug('locations =' + locations);
    boolean flag = false;
    JobPostingEntity tempposting;
    integer whichone;
    //loop location, set tempkey  location__category 
    for(Integer l=0 ; l< locations.size(); l++){
        tempKey = locations[l] + '__' + categories[l];
        flag = false;
        tempposting = null;
        // to check if the tempKey exists in postings list.
        // if yes, set tempposting value, flage and index
        for(integer ii=0 ; ii<postings.size() ; ii++){
            if(postings[ii].thiskey == tempKey){
                flag = true;
                tempposting = postings[ii];
                whichone = ii;
                break;
            }
        }
        
        // if tempKey exists in posting list, add occupation and 
        //industry to current jobposting entity.
        if(flag){
            templist = tempposting.Occupations;
            templist.add(occupations[l]);
            templist2.add(industries[l]);
            tempposting.Occupations =templist;
            tempposting.Idustry =templist2;
            postings[whichone] = tempposting;
        }else{
            // if tempKey does not exist, add occupation and industry
            tempposting = new JobPostingEntity(locations[l], categories[l]);
            templist = tempposting.Occupations;
            templist.add(occupations[l]);
            templist2.add(industries[l]);
            tempposting.Occupations = templist;
            tempposting.Idustry = templist2;
            postings.add(tempposting);
        }
    }
    
    jobpostingstr += '<JobPostings>';
    List<String> key_split_result;
    integer count = 0;

    for(JobPostingEntity jp: postings){
        //Previous old version, if now further error could be deleted later. 
        //jobpostingstr += '<JobPosting bold="'+job.Job_Bolding__c+'" desiredDuration="30">';
        //jobpostingstr += '<InventoryPreference><Autorefresh desired="'+job.CareerOne_Autorefresh__c+'"><Frequency>7</Frequency></Autorefresh></InventoryPreference>';
        
        // Newly updated xml based on CareerOne Update, changed by Lina
        jobpostingstr += '<JobPosting bold="true" desiredDuration="30">';
        jobpostingstr += '<InventoryPreference><Autorefresh desired="true"><Frequency>7</Frequency></Autorefresh></InventoryPreference>';        
        jobpostingstr += '<Location locationId="'+jp.Searcharea+'" />';
        jobpostingstr += '<JobCategory monsterId="'+jp.Category+'" />';
        jobpostingstr += '<JobOccupations>';    	
        count = 0;
        for(String o : jp.Occupations){
            jobpostingstr +=  '<JobOccupation monsterId="'+o+'" parentCategoryId="'+jp.Category+'" />';
            count = count + 1 ;
            if(count == 3){
                break;
            }
        }
        jobpostingstr += '</JobOccupations>';
        jobpostingstr += '<BoardName monsterId="1" />';
        //jobpostingstr += '<DisplayTemplate monsterId="12438" />';
        jobpostingstr += '<DisplayTemplate monsterId="'+templateid+'" />';

        jobpostingstr += '<Industries>';
        count = 0;
        for(String m : jp.Idustry){
            jobpostingstr += '<Industry><IndustryName monsterId="'+m+'" /></Industry>';
            count = count + 1 ;
            if(count == 3){
                break;
            }
        }
        jobpostingstr += '</Industries>';
        jobpostingstr += '</JobPosting>';
    }

    jobpostingstr += '</JobPostings>';
    //system.debug('jobpostingstr ='+jobpostingstr);
    return jobpostingstr;
    }   
    
    
    private String constructXMl(Advertisement__c job){    
    
    String refcode = orgid +':'+ String.valueOf(job.id).substring(0,15);
    //String refcode = orgid +':'+ String.valueOf(job.id);
    String xmlbody ='<Job xmlns="http://schemas.monster.com/Monster" jobAction="addOrUpdate" jobRefCode="'+refcode+'">';
    xmlbody += '<RecruiterReference>';
    //xmlbody += '<UserName>c1b_austcorp</UserName>';
    xmlbody += '<UserName>'+companyusername+'</UserName>';
    xmlbody += '</RecruiterReference>';
    xmlbody += '<CompanyReference>';
    //xmlbody += '<CompanyXCode>xc1b_austcorpx</CompanyXCode>';
    xmlbody += '<CompanyXCode>'+companycode+'</CompanyXCode>';
    xmlbody += '<CompanyName><![CDATA['+UserInfo.getOrganizationName()+']]></CompanyName>';
    xmlbody += '</CompanyReference>';
    //**************channel************
    xmlbody += '<Channel monsterId="168" alias="AUEN" />';
    xmlbody += '<JobInformation>';
    xmlbody += '<JobTitle><![CDATA['+job.Job_Title__c+']]></JobTitle>';
    xmlbody += '<JobType monsterId="'+ job.WorkType__c +'" />';
    xmlbody += '<JobStatus monsterId="'+ job.Work_Status__c +'" />';
    
    xmlbody += '<Salary><Currency monsterId="4" />'; 
    if(job.Salary_Min__c != null)
    {
            xmlbody += '<SalaryMin>'+job.Salary_Min__c+'</SalaryMin>';
    }
    if(job.Salary_Max__c != null)
    {
            xmlbody += '<SalaryMax>'+job.Salary_Max__c+'</SalaryMax>';
    }
    if(job.CompensationType__c != null && job.CompensationType__c != 'none')
    {
        xmlbody += '<CompensationType monsterId="'+job.CompensationType__c+'"/>';
    }
    if(job.Salary_Description__c != null && job.Salary_Description__c != '')
    {
        xmlbody += '<SalaryDescription><![CDATA['+job.Salary_Description__c + ']]></SalaryDescription>';
    }
    xmlbody += '</Salary>';
    if(job.Company_Name__c != null && job.Company_Name__c != '')
    {
            xmlbody += '<Contact hideCompanyName="'+job.Hide_Company_Name__c+'">';            
            xmlbody += '<CompanyName><![CDATA['+job.Company_Name__c+']]></CompanyName>';
           // xmlbody += '<Phones>';
           // xmlbody += '<Phone phoneType="contact">12345678</Phone>';
           //  xmlbody += '<Phone phoneType="fax">87654321</Phone>';
           // xmlbody += '</Phones>';                        
          //  xmlbody += '<E-mail>log@clicktocloud.com</E-mail>';
            xmlbody += '</Contact>';
    
    }

    xmlbody += '<PhysicalAddress>';
    xmlbody += '<City>'+job.City_C1__c+'</City>';
    if(job.State__c != null && job.State__c != 'none')
    {
    xmlbody += '<State>'+job.State__c+'</State>';
    }
    String postcode = job.zipcode__c;
    
   /* if(job.Postcode__c != null && job.Postcode__c != 0)
    {
        String temp = String.valueOf(job.Postcode__c);
        if(temp.indexOf('.') != -1)
        {
            postcode = temp.substring(0,temp.indexOf('.'));
        
        }
    
    }
    */
    if(postcode != null && postcode != '')
        {
        xmlbody += '<PostalCode>'+postcode+'</PostalCode>';
        }
    xmlbody += '</PhysicalAddress>';
    xmlbody += '<DisableApplyOnline>false</DisableApplyOnline>';
    xmlbody += '<HideCompanyInfo><![CDATA['+job.Hide_Company_Name__c+']]></HideCompanyInfo>';
    //xmlbody += '<JobBody><![CDATA['+job.Job_Content__c+']]></JobBody>';
    String tempfooter = '';
    if(job.Online_Footer__c != null)
        {
        tempfooter = job.Online_Footer__c.replaceAll('\\[\\%User Name\\%\\]',UserInfo.getName());
        tempfooter = tempfooter.replaceAll('\\[\\%Vacancy Referece No\\%\\]',job.Reference_No__c );
        }
    String jobdes = job.Job_Content__c + '<br/><br/>' + tempfooter;
    xmlbody += '<JobBody><![CDATA['+ jobdes+ ']]></JobBody>';
    if(job.Education_Level__c != null && job.Education_Level__c != 'none')
    {
        xmlbody += '<EducationLevel monsterId="'+job.Education_Level__c+'" />';
    }
    if(job.years_of_work_experience__c != null && job.years_of_work_experience__c != 'none')
    {xmlbody += '<YearsOfExperience monsterId="'+job.years_of_work_experience__c+'" />';}

    xmlbody += '<LanguageOfJob monsterId="27" />';
                
    xmlbody += '</JobInformation>';

    //********************** Job Posting *****************************
    String postingstr = constructPosting(job);
    if(postingstr == null)
    {
        //************** suzan *******************
        String err_msg = 'CareerOne Job Posting Error<br/>';
        err_msg += 'Error Location: 5';
        err_msg += '<br/> advertisement Id: ' + job.Id;
    
        notificationsendout(err_msg,'codefail','C1PostingUtil', 'constructXMl');
        //***************************************
        return null;
    }
    else
    {
        xmlbody +=  postingstr;
    
    }
    //****************************************************************
    xmlbody += '</Job>';
	//system.debug('xmlbody =' + xmlbody);
    return xmlbody;
}

 public String constructSoap(Advertisement__c job, boolean is_Edit){
    
    String timeStamp = getTimeStamp(); 
    String soapstr ;
    try{
    string xmlbody = constructXMl(job); 
    //*** postings.size = new usage , this part must after constructXMl()
    integer career1_count = (cur_u.CareerOne_Usage__c == null || cur_u.CareerOne_Usage__c == '' )? 0 : Integer.valueOf(cur_u.CareerOne_Usage__c);
    integer career1_quota = (cur_u.Monthly_Quota_CareerOne__c == null || cur_u.Monthly_Quota_CareerOne__c == '' )? 0 : Integer.valueOf(cur_u.Monthly_Quota_CareerOne__c);
    integer remainingQuota = career1_quota - career1_count;
    if(postings.size()>remainingQuota && !is_Edit) //** the careerone new page, not edit page. 
    {
        return 'ERROR Total Ad:' + postings.size() + ':' + remainingQuota; //** not allowed post ads
    }
    //******************************
    
    if(xmlbody != null){
        soapstr = '<?xml version="1.0" encoding="ISO-8859-1"?>';
        soapstr += '<SOAP-ENV:Envelope xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:mn="http://schemas.monster.com/Monster" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">';
        soapstr += '<SOAP-ENV:Header><mh:MonsterHeader xmlns:mh="http://schemas.monster.com/MonsterHeader">';
        soapstr += '<mh:MessageData><mh:MessageId>Jobs for company '+UserInfo.getOrganizationName()+' created on '+timeStamp+'</mh:MessageId>';
        //soapstr += '<mh:Timestamp>2010-01-07T12:05:07.44+10:00</mh:Timestamp></mh:MessageData>';
        soapstr += '<mh:Timestamp>'+timeStamp+'</mh:Timestamp></mh:MessageData>';
        
        soapstr += '<mh:ProcessingReceiptRequest>';
        soapstr += '<mh:Address transportType="file">file</mh:Address>';
        soapstr += '</mh:ProcessingReceiptRequest></mh:MonsterHeader>';
        soapstr += '<wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/04/secext">';
        soapstr += '<wsse:UsernameToken><wsse:Username>'+companyusername+'</wsse:Username>';
        soapstr += '<wsse:Password>'+companypwd+'</wsse:Password>';
        soapstr += '</wsse:UsernameToken></wsse:Security></SOAP-ENV:Header>';
        soapstr += '<SOAP-ENV:Body><Jobs xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.monster.com/Monster">';
        soapstr += xmlbody;
        soapstr += '</Jobs></SOAP-ENV:Body></SOAP-ENV:Envelope>';
    
    }
    //system.debug('soapstr =' + soapstr);
    }
    catch(system.Exception e)
    {
    	NotificationClass.notifyErr2Dev('C1PostingUtil/constructSoap', e);
        return null;
    }
    return soapstr;
}

public static String getTimeStamp(){
//** get current timestamp : 2010-01-07T12:05:07Z 
    Datetime mydatetime = system.now();
    String thetime = mydatetime.format('yyyy-MM-dd\'T\'HH:mm:ss');
    
    String timestamp = thetime + 'Z';
    system.debug('timestamp =' + timestamp );
    return timestamp;


}
static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());
    
    
}

static testmethod void testC1PostingUtil(){
		System.runAs(DummyRecordCreator.platformUser) {
			Placement__c p = new Placement__c(Name='test');
        
	        RecordType appFormRT =DaoRecordType.ApplicationFormStyleRT ;// [select Id from RecordType where DeveloperName = 'ApplicationFormStyle' limit 1];
	        StyleCategory__c appForm = new StyleCategory__c(RecordTypeId=appFormRT.Id,Name='testAppForm');        
	        insert p;
		
			StyleCategory__c template = new StyleCategory__c(templateC1__c='12345'); 
			insert template;
		
	        Advertisement__c a = new Advertisement__c();
	        a.Vacancy__c = p.Id;
	        a.Reference_No__c ='test';        
	        a.Salary_Max__c = 50000;        
	        a.Salary_Min__c = 50000;
	        a.Job_Content__c= 'okdafadfadsfadftestetetetetetetsetestafsdaraertaetaetatw';        
	        a.Job_Title__c ='test';        
	        a.Currency_Type__c ='AUD';
	        a.Online_Footer__c = 'test';
	        RecordType[] rt = [Select Id from RecordType where DeveloperName like '%careerone%'];
	        if(rt !=null)         
	        { a.RecordTypeId = rt[0].Id;   }
	        a.State__c = 'NSW';
	        a.zipcode__c = '2000';
	        a.City__c ='Sydney';
	        a.Physical_Address__c = 'Sydney, NSW, 2000';
	        a.CompensationType__c = '1';
	        a.Education_Level__c = '12';
	        a.Company_Name__c ='ctc';
	        a.Hide_Company_Name__c = true;
	        a.Work_Status__c = '4';
	        a.WorkType__c = '1';
	        a.Job_Bolding__c = true;
	        a.Template__c = template.Id;//'a0190000000UCsp' ;//'12438';
	        a.Industry_String__c = '808|||908';
	        a.Application_Form_Style__c = appForm.Id;//'a0190000000UBnX';
	        a.SearchArea_String__c = '1234';
	        a.Occ_String__c = '123';
	        a.Cat_String__c = '606';
	        a.Status__c = 'Ready to Post';
	        a.Online_Job_Id__c = '1234568';
	        a.Status__c = 'Deleted';
			insert a;
			C1PostingUtil util = new C1PostingUtil('a','a','a');
			String str=util.constructSoap(a,true);
			system.assert(str != null);
		}
		

}



}