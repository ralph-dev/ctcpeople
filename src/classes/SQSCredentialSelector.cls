/** 
 * A selector class to return SQS Credential
 * Created by: Lina
 * Created: 12/12/2016
 */

public with sharing class SQSCredentialSelector extends CommonSelector {

    //private DynamicSelector dynamicSel;

	public SQSCredentialSelector() {	
		//dynamicSel = new DynamicSelector(SQSCredential__c.SObjectType.getDescribe().getName(), false, true, true);	
        super(SQSCredential__c.SObjectType);
	}

	/*
     *  Set object as SQSCredential__c
     */
    //private Schema.SObjectType getSObjectType(){
    //    return SQSCredential__c.SObjectType;
    //}

	public override LIST<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField>{
		    SQSCredential__c.Id,
		    SQSCredential__c.Name,
			SQSCredential__c.URL__c, 
			SQSCredential__c.Access_Key__c, 
			SQSCredential__c.Secret_Key__c
		};
	}
	
    /**
     * get the SQS Credential given the name of the queue
     * 
     * @param: name             The name of the SQS Queue
     * @return:  SQSCreds       SQS credential for that Queue
    **/
    public SQSCredential__c getSQSCredential(String name) {
        SimpleQueryFactory qf = simpleQuery();
        String condition = '';
        qf.selectFields(getSObjectFieldList());
        qf.setLimit(1);
        if (String.isNotBlank(name)) {
            condition += 'Name =: name';
        }
        qf.setCondition(condition);
        String soqlStr = qf.toSOQL();
        List<SQSCredential__c> SQSCreds = Database.query(soqlStr);
        if (SQSCreds != null) {
            for (SQSCredential__c sqs : SQSCreds) {
                return sqs;
            } 
        } 
        return null;
    }
}