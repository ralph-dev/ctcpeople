@isTest
private class DaoContactTest {

	private static testMethod void test() {
		System.runAs(DummyRecordCreator.platformUser) {
        DaoContact dc = new DaoContact();
        
        system.assertNotEquals(null, DaoContact.getAllContacts());
        system.assertNotEquals(null, DaoContact.getContactsContainName('test', new List<RecordType>{new RecordType()}));
        system.assertNotEquals(null, DaoContact.getContactsByIds(new List<Id>{'a049000000u5iOJ'}, new List<RecordType>{new RecordType()}));
        system.assertNotEquals(null, DaoContact.getContactsByIds(new List<Id>{'a049000000u5iOJ'}));
        system.assertNotEquals(null, DaoContact.getContactsByEmails(new List<String>{'a049000000u5iOJ@abc.com'}, new List<RecordType>{new RecordType()}));
        system.assertNotEquals(null, DaoContact.getContactsbyName('test'));
        // system.assertNotEquals(null, DaoContact.getBulkEmailContactsByIds(new List<String>{'test'}, new List<Component>{new Component()}));
        system.assertNotEquals(null, DaoContact.getBulkEmailBasicInfoOfContactsByIds(new List<Id>{'a049000000u5iOJ'}));
        system.assertNotEquals(null, DaoContact.getClientContactsbyNameSR('test'));
        system.assertNotEquals(null, DaoContact.getCandContactsByIds(new List<Id>{'a049000000u5iOJ'}));
        system.assertNotEquals(null, DaoContact.getCandsContactsByIdsSR(new List<Id>{'a049000000u5iOJ'}));
        system.assertNotEquals(null, DaoContact.getCandContactsContainName('test'));
        system.assertNotEquals(null, DaoContact.getCorpContactsByEmails(new List<String>{'a049000000u5iOJ@abc.com'}));
        system.assertNotEquals(null, DaoContact.countContactForComp('a049000000u5iOJ'));
		}
	}

}