/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class InvoiceItemServicesTest {

    static testMethod void myUnitTest() {
    	TriggerHelper.UpdateAccountField=false;
    	System.runAs(DummyRecordCreator.platformUser) {
    	list<Invoice_Line_Item__c> invoiceLineItems=DataTestFactory.createInvoiceLineItems();
    	list<String> invoiceItemIds=new list<String>();
    	for(Invoice_Line_Item__c invoiceItem: invoiceLineItems){
    		invoiceItemIds.add(invoiceItem.Id);
    	}
    	InvoiceItemServices invoiceItemServ=new InvoiceItemServices();
    	invoiceItemServ.convertInvoiceItemToInvocieItemByIds(invoiceItemIds);
    	list<map<String,Object>> invoiceItemInfos=invoiceItemServ.convertInvoiceItemToInvocieItem(invoiceLineItems);
    	System.assert(invoiceItemInfos.size()>0);
    	}
    	
    }
    
    static testMethod void myUnitTest1() {
    	TriggerHelper.UpdateAccountField=false;
    	System.runAs(DummyRecordCreator.platformUser) {
    	list<Invoice_Line_Item__c> invoiceLineItems=DataTestFactory.createInvoiceLineItems();
    	list<String> accountIds=new list<String>();
    	list<String> contactIds=new list<String>();
    	list<String> invoiceItemIds=new list<String>();
    	
    	for(Invoice_Line_Item__c invoiceItem: invoiceLineItems){
    		accountIds.add(invoiceItem.Account__c);
    		contactIds.add(invoiceItem.Billing_Contact__c);
    		invoiceItemIds.add(invoiceItem.Id);
    	}
    	InvoiceItemServices invoiceItemServ=new InvoiceItemServices();
    	invoiceItemServ.fixBiller(accountIds);
    	invoiceItemServ.fixBillerContact(contactIds);
    	list<Invoice_Line_Item__c> invoiceIterms=invoiceItemServ.retrieveInvoicesByIds(invoiceItemIds);
    	System.assert(invoiceIterms.size()>0);
    	}
    	
    }
}