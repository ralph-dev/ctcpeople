@isTest
private class RosterDomainTest {
	
	final static String ROSTER_STATUS_PENDING = 'Pending';
	
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        System.runAs(DummyRecordCreator.platformUser) {
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
    	List<Days_Unavailable__c> rosters = 
    		TestDataFactoryRoster.createRostersForSplitRosterFunction(con, shift, acc,ROSTER_STATUS_PENDING);
    	RosterDomain rDomain = new RosterDomain(rosters);
		List<Days_Unavailable__c> rosterListForDisplayTotal = rDomain.splitRosterForDisplay();
		System.assertNotEquals(rosterListForDisplayTotal.size(),0);

        candidateDTO conDTO = new candidateDTO();
        conDTO.setCandidate(con);
        conDTO.setRadiusRanking(1);
        
        List<Shift__c> shiftList = new List<Shift__c>{shift};

        Contact con1 = TestDataFactoryRoster.createContact();
        candidateDTO conDTO1 = new candidateDTO();
        conDTO1.setCandidate(con1);
        conDTO1.setRadiusRanking(1);
        List<candidateDTO> candidateDTOs = new List<candidateDTO>{conDTO, conDTO1};

        rDomain.createRosteringRecords(vac.Id, shiftList, candidateDTOs);

        List<Placement_Candidate__c> cms = [select id from Placement_Candidate__c];
        system.assertEquals(cms.size(),2);
        }
    }
}