public with sharing class PeopleAdMapperIndeed extends PeopleAdMapper {
    public PeopleAdMapperIndeed() {
        super();
    }

    public PeopleAdMapperIndeed(Advertisement__c ad) {
        super(ad);
    }
    private Advertisement_Configuration__c adAccount;

    public override void generateNewXMLTags() {
        //generate the indeed-apply-data string
        String applydata = setIndeedApplyData();
        
        //convert the Indeed_Sponsored_job__c from true or false to yes or no
        String isSponsordJob = 'no';
        if(ad.Indeed_Sponsored_job__c) {
            isSponsordJob = 'yes';
        }

        //system.debug('ad.Id ='+fieldMap4NewXML);
        fieldMap4NewXML.put('referencenumber',Userinfo.getorganizationid().subString(0,15)+':'+ad.Id);
        fieldMap4NewXML.put('title',ad.Job_Title__c);
        fieldMap4NewXML.put('date',system.now());
        fieldMap4NewXML.put('city',ad.City__c );
        fieldMap4NewXML.put('state',ad.State_Label__c );
        fieldMap4NewXML.put('country',ad.Country__c );
        fieldMap4NewXML.put('postalcode',ad.LinkedIn_Postal_Code__c );
        fieldMap4NewXML.put('description',ad.Job_Content__c );
        fieldMap4NewXML.put('salary',ad.Salary_Description__c );
        fieldMap4NewXML.put('education',ad.Education_Level__c );
        fieldMap4NewXML.put('jobtype',ad.Job_Type_Text__c );
        fieldMap4NewXML.put('category',ad.Industry__c );
        fieldMap4NewXML.put('experience',ad.Skills_Experience__c );
        fieldMap4NewXML.put('indeed-apply-data',applydata );
        fieldMap4NewXML.put('sponsored',isSponsordJob);
        fieldMap4NewXML.put('sponsored-amount',ad.Indeed_Sponsored_amount__c);
        fieldMap4NewXML.put('company', UserInformationCon.CompanyName()); 
        fieldMap4NewXML.put('url', adAccount.Indeed_Apply_Job_URL__c);
    }

    private String setIndeedApplyData () {
    	ProtectedAccount pa = new AdvertisementConfigurationSelector().getIndeedProtectedAccountById(ad.Advertisement_Account__c);
        //adAccount = new AdvertisementConfigurationSelector().getIndeedAccountById(ad.Advertisement_Account__c);
        adAccount = (Advertisement_Configuration__c) pa.account;
        
        String indeedapplydata = '';
        String indeedJobId = 'indeed-apply-jobId='+EncodingUtil.urlEncode(Userinfo.getorganizationid().subString(0,15)+':'+ad.Id, 'UTF-8');
        String indeedApplyJobTitle = 'indeed-apply-jobTitle='+EncodingUtil.urlEncode(ad.Job_Title__c, 'UTF-8');
        
        //String indeedApplyPostURL = 'indeed-apply-postUrl='+EncodingUtil.urlEncode(EndPoint.getPeoplecloudWebSite()+'/IndeedIntegration/application', 'UTF-8');
		String indeedApplyPostURL = 'indeed-apply-postUrl='+EncodingUtil.urlEncode(EndPoint.getcpewsEndpoint()+'/IndeedIntegration/application', 'UTF-8');
	
        indeedapplydata = indeedJobId + '&' + indeedApplyJobTitle + '&' + indeedApplyPostURL;
        
        String indeedapplyapitoken = '';
        
        //added by andy for security review II
        if(pa.getToken() != null && pa.getToken() != ''){
        	indeedapplyapitoken = 'indeed-apply-apiToken=' + EncodingUtil.urlEncode(pa.getToken(), 'UTF-8');
            indeedapplydata = indeedapplydata + '&' + indeedapplyapitoken;
        	
        }
        //commented by andy for security review II
        //if(adAccount.Indeed_API_Token__c!=null && adAccount.Indeed_API_Token__c!= ''){
        //    indeedapplyapitoken = 'indeed-apply-apiToken=' + EncodingUtil.urlEncode(adAccount.Indeed_API_Token__c, 'UTF-8');
        //    indeedapplydata = indeedapplydata + '&' + indeedapplyapitoken;
        //}
        
        String indeedapplyquestion = '';
        if(ad.Indeed_Question_URL__c!=null && ad.Indeed_Question_URL__c!=''){
            indeedapplyquestion = 'indeed-apply-questions=' + EncodingUtil.urlEncode(ad.Indeed_Question_URL__c, 'UTF-8');
            indeedapplydata = indeedapplydata + '&' + indeedapplyquestion;
        }
        String indeedApplyJobURL = '';
        if(adAccount.Indeed_Apply_Job_URL__c !=null && adAccount.Indeed_Apply_Job_URL__c != '') {
            indeedApplyJobURL = 'indeed-apply-joburl=' + EncodingUtil.urlEncode(adAccount.Indeed_Apply_Job_URL__c, 'UTF-8');
            indeedapplydata = indeedapplydata + '&' + indeedApplyJobURL;
        }

        if(UserInformationCon.CompanyName()!= null && UserInformationCon.CompanyName()!= ''){
            String indeedApplyJobCompanyName = 'indeed-apply-jobCompanyName='+EncodingUtil.urlEncode(UserInformationCon.CompanyName(), 'UTF-8');
            indeedapplydata = indeedapplydata + '&' + indeedApplyJobCompanyName;
        }

        String indeedapplyjoblocation = '';
        if(ad.City__c != null && ad.City__c !='') {
            indeedapplyjoblocation += ad.City__c + ' ';
        }
        if(ad.State_Label__c!= null && ad.State_Label__c!='') {
            indeedapplyjoblocation += ad.State_Label__c + ' ';
        } 
        if(ad.LinkedIn_Postal_Code__c!= null&&ad.LinkedIn_Postal_Code__c!= '' ) {
            indeedapplyjoblocation += ad.LinkedIn_Postal_Code__c + ' ';
        }
        indeedapplyjoblocation +=  ad.Country__c;
        //system.debug('indeedapplyjoblocation ='+ indeedapplyjoblocation);
        indeedapplyjoblocation = 'indeed-apply-jobLocation='+EncodingUtil.urlEncode(indeedapplyjoblocation, 'UTF-8');
        indeedapplydata = indeedapplydata + '&' + indeedapplyjoblocation;
        
        //String indeedapplydata = indeedapplyquestion + indeedapplyapitoken;
        //system.debug('indeedapplydata ='+ indeedapplydata);
        return indeedapplydata;
    }

}