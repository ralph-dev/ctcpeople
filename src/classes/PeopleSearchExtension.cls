/*
 * Author: Booz S. Espiridion
 */
public with sharing class PeopleSearchExtension {
	public PeopleSearchExtension(){
		
	}
	
    public PeopleSearchExtension(Object obj){
    	
    }
    
    @RemoteAction
    public static String getContactFieldsList(){
    	return new PeopleSearchService().getAllContactFields();
    }
    
    @RemoteAction
    public static List<PeopleSearchDTO.Field> getContactCriteriaList(){
    	return new PeopleSearchService().getContactCriteriaList();
    }
    
    @RemoteAction
    public static List<PeopleSearchOperatorFactory.FieldType> getFieldTypeList(){
    	return new PeopleSearchService().getFieldTypeList();
    }
    
    @RemoteAction
    public static List<PeopleSearchDTO.ContactSearchResult> searchCandidate(PeopleSearchDTO.SearchQuery search){
    	//system.debug(LoggingLevel.FINE, JSON.serializePretty(search));
    	return new PeopleSearchService().searchCandidate(search);
    }
    
    @RemoteAction
    public static String getContactDisplayColumns(){
    	return new PeopleSearchService().getContactDisplayColumns();
    }
    
    @RemoteAction
    public static String getContactDefaultDisplayColumn(){
    	return new PeopleSearchService().getContactDefaultDisplayColumn();
    }
    
    @RemoteAction
    public static String saveContactDisplayColumn(String jsonColumn){
    	return new PeopleSearchService().saveContactDisplayColumn(jsonColumn);
    }
    
    @RemoteAction
    public static String generateBulkEmailURI(List<String> contactIds) {
        String docBody = JSON.serialize(contactIds);
        Document doc = new DocumentService().createTempDocumentByBodyForBulkEmail(docBody);
        String URI = '/apex/' + PeopleCloudHelper.getPackageNamespace()+ 'bulkemailcandidatelistpage?cjid='+doc.Id
                        +'&sourcepage=peopleSearch';
        return URI;
    }
    
    @RemoteAction
    public static String generateBulkEmailURIWithTargetObjectsMap(List<String> contactIds, Map<String,String> targetObjectsMap) {
        String docBody = JSON.serialize(contactIds);
        Document doc = new DocumentService().createTempDocumentByBodyForBulkEmail(docBody);
        
        
        ///
        String URI = '/apex/' + PeopleCloudHelper.getPackageNamespace()+ 'bulkemailcandidatelistpage?cjid='+doc.Id
                +'&sourcepage=peopleSearch';
                
        String extraParams = generateTargetObjectsQueryString(targetObjectsMap);
        if(!extraParams.equals('')){
        	URI += '&' +extraParams;
        }
        return URI;
    }
    
    private static String generateTargetObjectsQueryString(Map<String,String> targetObjectsMap){
    	String extraParams = '';
        if(targetObjectsMap != null && targetObjectsMap.size() > 0){
        	String namespace = PeopleCloudHelper.getPackageNamespace();
        	for(String name : targetObjectsMap.keySet()){
        		String idvalue = targetObjectsMap.get(name);
        		if(idValue == null){
        			continue;
        		}
        		name = name.trim();
        		idvalue = idvalue.trim();
        		
        		if(name.toLowerCase().endsWith('__c') && namespace != null && !namespace.equals('')){
        			if(name.toLowerCase().startsWith(namespace.toLowerCase())){
        				
        			}else{
        				name = namespace + name;
        			}
        			
        		}
        		
        		String[] param = new String[]{name, idvalue};
        		if(param.size() == 2 ){
        			if(extraParams.equals('')){
        				extraParams = param[0] +'='+ param[1];
        			}else{
        				extraParams += '&' + param[0] +'='+ param[1];
        			}
        		}
        	}
        }
        
        return extraParams;
    }
    
    @RemoteAction
    public static String generateBulkEmailURIWithTargetObjectsList(List<String> contactIds, List<String> objectNameandIds) {
        String docBody = JSON.serialize(contactIds);
        Document doc = new DocumentService().createTempDocumentByBodyForBulkEmail(docBody);
        
        ///
        String URI = '/apex/' + PeopleCloudHelper.getPackageNamespace()+ 'bulkemailcandidatelistpage?cjid='+doc.Id
                +'&sourcepage=screenCandidate';
        
        if(objectNameandIds != null && objectNameandIds.size() > 0){
        	Map<String,String> targetObjectsMap = new Map<String,String>();
        	for(String nameAndId : objectNameandIds){
        		String[] param = nameAndId.split(':');
        		if(param.size() == 2 ){
        			targetObjectsMap.put(param[0],param[1]);
        		}
        	}
        	String extraParams = generateTargetObjectsQueryString(targetObjectsMap);
	        if(!extraParams.equals('')){
	        	URI += '&' +extraParams;
	        }
        }
 
        return URI;
    }
    
    @RemoteAction
    public static Contact getContactLongTextAreaFields(String Id) {
        return new ContactSelector().getContactLongTextFieldsById(Id);
    }
    
    @RemoteAction
    public static Contact getPeopleInfo(String Id) {
        return new CandidateSelector().selectContactById(Id);
    }
    
    /**
     * To determine whether to show skill search section
     * @return    true   -- if skill parsing enabled
     *           false  -- if skill parsing disabled
     */ 
    @RemoteAction
    public static Boolean isSkillSearchEnabled() {
        PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
        return cs.Enable_Skill_Search__c;
    }
    
    /**
     * get list of all the skills in the current org in JSON object format
     * @return  list of SkillSearchDTO.Skill 
     */
     @RemoteAction
     public static List<SkillSearchDTO.Skill> getAllSkillsList() {
         return new PeopleSearchService().getAllSkills();
     }     
    
    /**
     *  Provide additional information on the remote action API
     * */
    @RemoteAction
    public static Map<String, RemoteAPIDescriptor.RemoteAction> apiDescriptor(){
        RemoteAPIDescriptor descriptor = new RemoteAPIDescriptor();
        descriptor.description('CRUD & describe remote action for Search.');
        /*
        descriptor.action('createRecords').param(List<Auction__c>.class).active(false);
        descriptor.action('getRecords').param(List<Id>.class).active(false);
        descriptor.action('updateRecords').param(List<Auction__c>.class).active(false);
        descriptor.action('deleteRecords').param(List<Auction__c>.class).active(false);
        
        descriptor.action('createAuctions').param(List<Auction__c>.class);
        descriptor.action('getAuctions').param(List<Id>.class);
        descriptor.action('retrieveAuctions').param(List<Id>.class);
        descriptor.action('retrieveRecords').param(List<Id>.class);
        descriptor.action('updateAuctions').param(List<Auction__c>.class);
        descriptor.action('deleteAuctions').param(List<Auction__c>.class);
        */
        
        descriptor.action('getFieldsList');
        descriptor.action('getFieldTypeList');
        descriptor.action('searchCandidate').param(PeopleSearchDTO.SearchQuery.class);
        
        descriptor.action('getContactDisplayColumns');
        descriptor.action('getContactDefaultDisplayColumn');
        descriptor.action('saveContactDisplayColumn').param(String.class);
        
        descriptor.action('generateBulkEmailURI').param(List<String>.class);
        descriptor.action('generateBulkEmailURIWithTargetObjectsMap').param(List<String>.class, 'List of contact Ids').param(Map<String,String>.class,'Map of target object name and id value');
        descriptor.action('generateBulkEmailURIWithTargetObjectsList').param(List<String>.class, 'List of contact Ids').param(List<String>.class,'List of the string joined with target object name and id value by colon');
        
        descriptor.action('getContactLongTextAreaFields').param(String.class);
        descriptor.action('getPeopleInfo').param(String.class);      
        
        descriptor.action('getContactFieldsList');
        descriptor.action('saveContactDisplayColumn').param(String.class);
        descriptor.action('isSkillSearchEnabled');
        descriptor.action('getAllSkillsList');
        return descriptor.paramTypesMap;
    } 
}