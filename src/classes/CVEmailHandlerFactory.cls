/**
* This factory creates corresponding email handler object which implements IEmailHandler interface 
* depending on the content of email.
* 
*/

public with sharing class CVEmailHandlerFactory {
    public static ICVEmailHandler build( Messaging.InboundEmail email ){
		//firstly try to get Candidate maker
		ICVEmailHandler handler = getCandidateMaker( email );
		
		if(handler == null){
			//TODO try to create other type of email handler
			
			
			System.debug('Failed to create CandidateMaker according to inbound email');
		}
		
		return handler;
		
	}
	
	
	/**
	* create CandidateMaker/JXTCandidateMaker object
	* return:
	*	ICVEmailHandler/JXTCandidateMaker object
	*/
	public static ICVEmailHandler getCandidateMaker( Messaging.InboundEmail email ){
		
		ICVEmailParser parser = CandidateParserFactory.getParser( email ); 
		if ( parser != null ){
			if(parser instanceof JXTEmailParser) {
		        JXTCandidateMaker jxtMaker = new JXTCandidateMaker( parser );
		        jxtMaker.setEmail(email);
		        return jxtMaker;
		    } else {
    			CandidateMaker maker = new CandidateMaker ( parser );
    			maker.setEmail(email);
    			return maker;
		    }
		}
		
		return null;
		
	}
}