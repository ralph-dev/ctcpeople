public with sharing class IndeedAdXMLWriter {
	private XMLWriterShortCuts xmlShortCuts;
	private Advertisement__c ad;
	private XMLStreamWriter writer;

	public IndeedAdXMLWriter() {
		xmlShortCuts=new XMLWriterShortCuts();
		writer=new XMLStreamWriter();
	}

	public void setAd(Advertisement__c ad){
 		this.ad = ad;
	}

	public String writeXML(){
		PeopleAdMapper mapper = new PeopleAdMapperIndeed(ad);
		mapper.setAd(ad);
		
		Map<String,Object> fieldValMap = mapper.getFieldMap4NewXML();
		
		writer.writeStartElement(null,'job',null);
		writeAdContent(fieldValMap, writer);
		writer.writeEndElement();
		
		String finalXML=writer.getXmlString();
		//system.debug('finalXML ='+ finalXML);
		return finalXML;
	}

	// Write ad record values to xml
	private void writeAdContent(Map<String,Object> fieldValMap, XMLStreamWriter writer){
		for(String tagName:fieldValMap.keyset()){
			Object tagVal = fieldValMap.get(tagName);
			//system.debug('tagName ='+ tagName);
			//system.debug('tagVal ='+ tagVal);
			xmlShortCuts.writeXMLCDataCharacters(writer,tagName,tagVal);
		}
	}
}