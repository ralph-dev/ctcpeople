/*
    linkedincountry
    linkedinJobPostalCode
    linkedinJobFunction
    linkedinJobIndustry
    linkedinJobExperienceLevel
    linkedinJobType
        
*/

public with sharing class LinkedInPostUtil {
    public List<SelectOption> linkedinjobcountrySelectOption;
    //public List<SelectOption> linkedinJobPostalCodeSelectOption;
    public List<SelectOption> linkedinJobFunctionSelectOption;
    public List<SelectOption> linkedinJobIndustrySelectOption;
    public List<SelectOption> linkedinJobExperienceLevelSelectOption;
    public List<SelectOption> linkedinJobTypeSelectOption;
    public List<SelectOption> linkedinPostingRoleSelectOption;
    public boolean seletoptioncorrect{get;set;}
    public Map<String, LinkedinCountryCode> linkedincountryvalue {get; set;}
    public String linkedInAuthPageUrl;
    public Integer linkedInAuthPageResponsecode;
    
    //Generate the LinkedIn posting function select option
    public void getLinkedInPostUtil(String pickListFieldMapJsonString){
        linkedincountryvalue = new Map<String, LinkedinCountryCode>();
        Map<String, String> pickListFieldMap = (Map<String,String>)JSON.deserialize(pickListFieldMapJsonString, Map<String,String>.class);
        if(pickListFieldMap!=null && pickListFieldMap.size()>0){
            for(String linkedinfield : pickListFieldMap.keySet()){
                if(linkedinfield.equalsIgnoreCase('linkedincountry')){
                    linkedincountryvalue = (Map<String, LinkedinCountryCode>)JSON.deserialize(pickListFieldMap.get(linkedinfield), Map<String, LinkedinCountryCode>.class);
                    List<String> linkedincountryvaluelist = new List<String>();
                    linkedincountryvaluelist.addAll(linkedincountryvalue.keyset());
                    linkedincountryvaluelist.sort();
                    linkedinjobcountrySelectOption = GenerateSelectOption(linkedincountryvaluelist);
                }else if(linkedinfield.equalsIgnoreCase('linkedinJobIndustry')){
                    linkedincountryvalue = (Map<String, LinkedinCountryCode>)JSON.deserialize(pickListFieldMap.get(linkedinfield), Map<String, LinkedinCountryCode>.class);
                    List<String> linkedinJobIndustryvalue = new List<String>();
                    linkedinJobIndustryvalue.addAll(linkedincountryvalue.keyset());
                    linkedinJobIndustryvalue.sort();
                    linkedinJobIndustrySelectOption = GenerateSelectOption(linkedinJobIndustryvalue);
                }else if(linkedinfield.equalsIgnoreCase('linkedinJobFunction')){
                    linkedincountryvalue = (Map<String, LinkedinCountryCode>)JSON.deserialize(pickListFieldMap.get(linkedinfield), Map<String, LinkedinCountryCode>.class);
                    List<String> linkedinJobFunctionvalue = new List<String>();
                    linkedinJobFunctionvalue.addAll(linkedincountryvalue.keyset());
                    linkedinJobFunctionvalue.sort();
                    linkedinJobFunctionSelectOption = GenerateSelectOption(linkedinJobFunctionvalue);
                }else if(linkedinfield.equalsIgnoreCase('linkedinJobExperienceLevel')){
                    linkedincountryvalue = (Map<String, LinkedinCountryCode>)JSON.deserialize(pickListFieldMap.get(linkedinfield), Map<String, LinkedinCountryCode>.class);
                    List<String> linkedinJobExperienceLevelvalue = new List<String>();
                    linkedinJobExperienceLevelvalue.addAll(linkedincountryvalue.keyset());
                    linkedinJobExperienceLevelvalue.sort();
                    linkedinJobExperienceLevelSelectOption = GenerateSelectOption(linkedinJobExperienceLevelvalue);
                }else if(linkedinfield.equalsIgnoreCase('linkedinJobType')){
                    linkedincountryvalue = (Map<String, LinkedinCountryCode>)JSON.deserialize(pickListFieldMap.get(linkedinfield), Map<String, LinkedinCountryCode>.class);
                    List<String> linkedinJobTypevalue = new List<String>();
                    linkedinJobTypevalue.addAll(linkedincountryvalue.keyset());
                    linkedinJobTypevalue.sort();
                    linkedinJobTypeSelectOption = GenerateSelectOption(linkedinJobTypevalue);
                }else if(linkedinfield.equalsIgnoreCase('linkedinPostingRole')){
                    linkedincountryvalue = (Map<String, LinkedinCountryCode>)JSON.deserialize(pickListFieldMap.get(linkedinfield), Map<String, LinkedinCountryCode>.class);
                    List<String> linkedinPostingRolevalue = new List<String>();
                    linkedinPostingRolevalue.addAll(linkedincountryvalue.keyset());
                    linkedinPostingRolevalue.sort();
                    linkedinPostingRoleSelectOption = GenerateSelectOption(linkedinPostingRolevalue);
                }
            }
            if(linkedinjobcountrySelectOption.size()>0 //&& linkedinJobPostalCodeSelectOption.size()>0 
                && linkedinJobFunctionSelectOption.size()>0 && linkedinJobIndustrySelectOption.size()>0 
                    && linkedinJobExperienceLevelSelectOption.size()>0 && linkedinJobTypeSelectOption.size()>0){
                        seletoptioncorrect = true;
            }
            else{
                seletoptioncorrect = false;
            }
        }else{
            seletoptioncorrect = false;
        }       
    }
    
    //Get Linkedin Country code
    public static Map<String, LinkedinCountryCode> getlinkedinCountryCode(String pickListFieldMapJsonString, String mapkey){
        Map<String, LinkedinCountryCode> linkedincountryvalue = new Map<String, LinkedinCountryCode>();
        Map<String, String> pickListFieldMap = (Map<String,String>)JSON.deserialize(pickListFieldMapJsonString, Map<String,String>.class);
        if(pickListFieldMap!=null && pickListFieldMap.size()>0){
            //system.debug('mapkey ='+ mapkey);
            if(pickListFieldMap.containsKey(mapkey)){
                linkedincountryvalue = (Map<String, LinkedinCountryCode>)JSON.deserialize(pickListFieldMap.get(mapkey), Map<String, LinkedinCountryCode>.class);
            }
        }else{
            
        }
        return linkedincountryvalue;
    }

    //get the picklist field map JSON String
    public static string getPickListFieldMapJsonString(){
        String result = '';
        if(!Test.isRunningTest()){
	        Http h = new Http();
	        HttpRequest req = new HttpRequest();
	        req.setEndpoint(Endpoint.getcpewsEndpoint()+'/JobBoardMetadataService/picklist/linkedin/all');
	        //system.debug('endpoint ='+Endpoint.getcpewsEndpoint()+'/JobBoardMetadataService/picklist/linkedin/all' );
	        req.setMethod('GET');
	        
	        HttpResponse res = h.send(req);
	        result = res.getBody();
        }
        else{
        	result = TestLinkedInPostUtil.dummyString();
        }
        //system.debug('result =' + result);
        return result;//res.getbody();
    }
    
    //generate Select option 
    private static List<SelectOption> GenerateSelectOption(List<String> tempList){
        List<SelectOption> tempSelectOption = new List<SelectOption>();
        if(tempList!= null && templist.size()>0){
            for(String result : templist){
                tempSelectOption.add(new SelectOption(result,result));
            }
        }
        return tempSelectOption;
    }
    
    //Remove multi picklist square
    public static String removeMultipicklistSquare(String multipicklist){
        String tempString  = multipicklist;
        tempString = tempString.replaceAll('\\(', '');
        tempString = tempString.replaceAll('\\)', '');
        tempString = tempString.replaceAll('\\[', '');
        tempString = tempString.replaceAll('\\]', '');
        tempString = tempString.trim();
        return tempString;    
    }
    
    //Check the LinkedIn Validation
    public static Boolean checklinkedInAccountEnable(){
        Boolean linkedInAccountEnable = true;
        
        String linkedInAccount = UserInformationCon.getUserDetails.LinkedIn_Account__c;
        
        if(linkedInAccount == null || linkedInAccount == ''){
            linkedInAccountEnable = false;
        }else{
            StyleCategory__c currentlinkedinaccount = DaoStyleCategory.getLinkedInAccountById(linkedInAccount);
            //system.debug('currentlinkedinaccount ='+ currentlinkedinaccount);
            if(currentlinkedinaccount == null){
                linkedInAccountEnable = false;
            }
        }
        return linkedInAccountEnable;
    }
    
    //Sign in linkedIn Oauth Service
    public Boolean signinlinkedinoauth(){
        Boolean signinlinkedinoautheanble = true;
        linkedInAuthPageUrl = '';
        String retUrl = '';
        linkedInAuthPageResponsecode = 0;
        OAuth oa = new OAuth();        
        if(!oa.setService('linkedin')) {//If oa.setService is false, return to the AuthPage
            retUrl = EncodingUtil.urlEncode(ApexPages.currentPage().getURL(),'UTF-8');
            linkedInAuthPageUrl = Page.AuthPage.getURL()+'?retUrl='+ URL.getSalesforceBaseUrl().toExternalForm() + retUrl;
            signinlinkedinoautheanble = false;
            linkedInAuthPageResponsecode = PeopleCloudErrorInfo.OAUTH_SERVICE_FAILED;
           // system.debug('linkedInAuthPageUrl='+linkedInAuthPageUrl);
        }
        return signinlinkedinoautheanble;
    }
    
    //Init the LinkedIn Country if the Advertisement country is null
    public static String getUserLinkedInCounry(String pickListFieldsString){
    	String userLinkedInCounry = '';
    	userLinkedInCounry = UserInformationCon.getUserDetails.country;
    	Map<String, LinkedinCountryCode> tempmapstring  = getlinkedinCountryCode(pickListFieldsString, 'linkedincountry');
    	if(!tempmapstring.containsKey(userLinkedInCounry)){
    		userLinkedInCounry = 'Australia';
    	}
    	return userLinkedInCounry;
    }
}