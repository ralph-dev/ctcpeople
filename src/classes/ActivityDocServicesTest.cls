@isTest
private class ActivityDocServicesTest {
    static String jsonString = 'test';
    static String sessionid = 'sessionid';
    static String surl = 'surl';
        
	private static testMethod void testUploaddocumentsByEc2() {
		System.runAs(DummyRecordCreator.platformUser) {
			Integer responseCode = ActivityDocServices.UploaddocumentsByEc2(jsonString, sessionId, surl);
	        system.assertEquals(200, responseCode);
	        ActivityDocServices.ImportCandidateByEc2(jsonString, sessionid, surl);
	        ActivityDocServices.uploadDocumentByActivityAtt(jsonString, sessionid, surl);
		}
        
	}

}