/**
    Author: Raymond
    
    This calss is to dedup incoming contact records.
    
    In People Cloud dedup function is invoked after candidate management
    is created.
    
    Among the incoming new candidate managements, two different candidate managements can't
    share with same candidate record.
**/
public with sharing class Deduplication {
    
    /**
        This class 
    **/
    public class DuplicateSet{
        public Map<Id,List<Contact>> duplicateMap;
        public List<Contact> duplicateList;
        public List<Contact> noDupCand; // list of incoming candidate without duplication
        
        public DuplicateSet(){
            duplicateMap = new Map<Id,List<Contact>>();
            duplicateList = new List<Contact>();
        }
    }
    
    /**
        CandidateWorkingSet is a class used to find duplicates in the incoming candidates.
        The reason for creating this class is to isolate some utility collections which are
        created for easy access of data when doing duplicate check. These 
        utility collections are created in order to avoid hitting SF limit.
         
        
    **/
    public class CandidateWorkingSet{
        // Ids of incoming candidates
        // A map for quick searching incoming cm by cand id
        private Map<Id,Placement_Candidate__c> cand2CMs;
        private List<Contact> incomingCands{get;private set;}
        // Ids of all incoming candidate management records
        private List<Id> incomingAppIdList;
        // List of existing contact which may be duplicated with incoming candidates
        private List<Contact> existingCons{get;private set;}
        private List<Contact> possibleDupCons4NullEmailCon;
        DuplicateSet dupSet = new DuplicateSet();
        
        
        /**
            Initilizing this class will initilize those utility collections
        **/
        public CandidateWorkingSet(List<Placement_Candidate__c> incomingApplications,List<Id> excludedRecordTypeId){
            List<ID> incomingCandIDs = new List<ID>();
            for(Placement_Candidate__c cm:incomingApplications){
                if(cm.Candidate__c !=null){
                    incomingCandIDs.add(cm.Candidate__c);
                    // Id list that contain all incoming application
                    //incomingAppIdList.add(cm.id);
                }
            }
            
            // Get detail info of incoming candidates 
            CommonSelector.checkRead(Contact.sObjectType,'id, Candidate_From_Web__c,Email,FirstName,LastName');
            incomingCands = [select id, Candidate_From_Web__c,Email,FirstName,LastName from Contact 
                where id in: incomingCandIDs and RecordTypeId not in: excludedRecordTypeId order by LastModifiedDate ASC];
             
            // create two filters
            Set<String> emailFilter = new Set<String>();//email filter for selecting contacts using same email
            List<ID> idFilter = new List<ID>(); // id filter to avoid selecting the record itself
            
            // Following two filters used to filter contacts with empty email
            List<String> firstNameFilter = new List<String>(); // first name fileter used to avoid selecting record with same first name 
            List<String> lastNameFilter = new List<String>(); // first name fileter used to avoid selecting record with same first name      
            for(Contact con:incomingCands){
                idFilter.add(con.Id);
                // Skip null email as it may cause selecting all contacts
                if(con.email!=null && con.email.length()>0){
                    emailFilter.add(con.email);
                }else{
                    firstNameFilter.add(con.FirstName);
                    lastNameFilter.add(con.LastName);
                }
            }
            
            // Select potential duplicate records from existing contact
            CommonSelector.checkRead(Contact.sObjectType,'id, Candidate_From_Web__c,Email,FirstName,LastName');
            existingCons = [select id, Candidate_From_Web__c,Email,FirstName,LastName 
            from Contact where Id not in:idFilter and RecordTypeId not in:excludedRecordTypeId and (Email in: emailFilter 
                or (Email=null and Id not in:idFilter and FirstName in:firstNameFilter and LastName in:lastNameFilter)) 
            order by LastModifiedDate ASC];
        }
        
        /**
            The only method of this class which return the duplciates
        **/
        public DuplicateSet findDuplicates(){
            Map<String,Contact> dupCheckMap = new Map<String,Contact>();
            for(Contact con:existingCons){
                if(con.FirstName==null)
                    con.FirstName='';
                String key=con.FirstName.toLowercase()+con.LastName.toLowercase()+con.Email;
                dupCheckMap.put(key,con);
            }
            for(Contact newCand: incomingCands){
                if(newCand.FirstName==null)
                    newCand.FirstName='';
                String key = newCand.FirstName.toLowercase()+newCand.LastName.toLowercase()+newCand.Email;
                // de-dup function consider two contacts are same if they have same first name, last
                // name and email
                if(dupCheckMap.get(key)!=null){
                    candidateAIsDuplicateOfB(newCand, dupCheckMap.get(key));
                }else{
                    dupCheckMap.put(key,newCand);
            	}
            }
            return dupSet;
        }
        
        private void candidateAIsDuplicateOfB(Contact a, Contact b){
            dupSet.duplicateList.add(a);
            if(dupSet.duplicateMap.get(b.Id)==null){
                List<Contact> duplicates = new List<Contact>();
                duplicates.add(a);
                dupSet.duplicateMap.put(b.Id,duplicates);
            }else{
                dupSet.duplicateMap.get(b.Id).add(a);
            }
        }
    }
    
    private static void mergeDuplicates(DuplicateSet dupSet, List<Placement_Candidate__c> incomingApplications, Boolean enableCMDedup){
        List<Id> dupCandIds= new List<Id>();
        Map<Id,List<Web_Document__c>> resumeOfDupCand = new Map<Id,List<Web_Document__c>>();
        Map<Id,Contact> dupCandMap;
        Map<Id,Placement_Candidate__c> candId2cm = new Map<Id,Placement_Candidate__c>();
        Map<Id,List<Candidate_Skill__c>> candId2cs = new Map<Id,List<Candidate_Skill__c>>();
        Map<Id,List<Employment_History__c>> candId2eh = new Map<Id,List<Employment_History__c>>(); 
        Map<Id,List<Education_History__c>> candId2edu = new Map<Id,List<Education_History__c>>(); 
        List<Contact> contact2Update = new List<Contact>();
        List<Placement_Candidate__c> cm2Update = new List<Placement_Candidate__c>();
        List<Web_Document__c> resume2Update = new List<Web_Document__c>();
        List<Candidate_Skill__c> cs2Update = new List<Candidate_Skill__c>();
        List<Employment_History__c> eh2Update = new List<Employment_History__c>();
        List<Employment_History__c> eh2Del = new List<Employment_History__c>();
        List<Education_History__c> edu2Update = new List<Education_History__c>();
        List<Education_History__c> edu2Del = new List<Education_History__c>();        
        Map<Id, List<Placement_Candidate__c>> allDupCandiCMMap = new Map<Id, List<Placement_Candidate__c>>();
        // Added by Lina
        Set<Id> dupCandIdSet = new Set<Id>();
        
        for(Contact con:dupSet.duplicateList){
            dupCandIds.add(con.Id);
        }
        // TODO: can null goes into dupCandiIds?
        
        CommonSelector.checkRead(Web_Document__c.sObjectType,'Document_Related_To__c ');
        for(Web_Document__c resume:[select Document_Related_To__c from Web_Document__c where Document_Related_To__c in :dupCandIds]){
            if(resumeOfDupCand.get(resume.Document_Related_To__c)==null){
                resumeOfDupCand.put(resume.Document_Related_To__c,new List<Web_Document__c>());
            }
            resumeOfDupCand.get(resume.Document_Related_To__c).add(resume);
        }
        
        // Resume content, jobApplicationId and other information that need to be migrated from duplicate to exisint candidates
        // is only retrieved right before the merge process. This lazy loading approach will reduce the amount of data retrieved during
        // trigger operation, and avoid hitting sf limit. 
        
        //TODO: fix 1
        // dupCandMap = new Map<Id,Contact>([select Id, Resume__c,Resume_Source__c,Resume_Link__c,jobApplication_Id__c,Skill_Group__c from Contact where Id in :dupCandIds]);
        dupCandMap = new Map<Id,Contact>(ContactServices.getContactsForDedupUpdate(dupCandIds));
        
        // add by Lina
        CommonSelector.checkRead(Placement_Candidate__c.sObjectType, 'Id, Candidate__c, Placement__c, Job_Application_Id__c, Job_Application_Ext_Id__c');
        List<Placement_Candidate__c> allDupCandiCM = [select Id, Candidate__c, Placement__c, Job_Application_Id__c, Job_Application_Ext_Id__c from Placement_Candidate__c where Candidate__c in: dupCandIds];
        for (Placement_Candidate__c cm : allDupCandiCM){
            if (allDupCandiCMMap.get(cm.Candidate__c) == null) {
                List<Placement_Candidate__c> cmList= new List<Placement_Candidate__c>();
                cmList.add(cm);
                allDupCandiCMMap.put(cm.Candidate__c, cmList);
            } else {
                allDupCandiCMMap.get(cm.Candidate__c).add(cm);
            }
        }
        
        
        fflib_securityUtils.checkObjectIsReadable(Contact.SObjectType);
        CommonSelector.checkRead(Candidate_Skill__c.sObjectType, 'Id ');
        for(Candidate_Skill__c cs:[select Id, Candidate__c from Candidate_Skill__c where Candidate__c in: dupCandIds]){
            if(candId2cs.get(cs.Candidate__c) == null){
                candId2cs.put(cs.Candidate__c,new List<Candidate_Skill__c>());
            }
            candId2cs.get(cs.Candidate__c).add(cs);
        }
        
       
        fflib_securityUtils.checkObjectIsReadable(Contact.SObjectType);
        CommonSelector.checkRead(Employment_History__c.sObjectType, 'Id ');
        for(Employment_History__c eh:[select Id, Contact__c from Employment_History__c where Contact__c in: dupCandIds]){
            if(candId2eh.get(eh.Contact__c) == null){
                candId2eh.put(eh.Contact__c,new List<Employment_History__c>());
            }
            candId2eh.get(eh.Contact__c).add(eh);
        }
        fflib_securityUtils.checkObjectIsReadable(Contact.SObjectType);
       CommonSelector.checkRead(Education_History__c.sObjectType, 'Id');
        for(Education_History__c edu:[select Id, Contact__c from Education_History__c where Contact__c in: dupCandIds]) {
            if(candId2edu.get(edu.Contact__c) == null) {
                candId2edu.put(edu.Contact__c, new List<Education_History__c>());
            }
            candId2edu.get(edu.Contact__c).add(edu);
        }
        
        //CommonSelector.checkRead(Employment_History__c.sObjectType, 'Id ');
        eh2Del = [select Id from Employment_History__c where Contact__c in: dupSet.duplicateMap.keySet()];
        //CommonSelector.checkRead(Education_History__c.sObjectType, 'Id ');
        edu2Del = [select Id from Education_History__c where Contact__c in: dupSet.duplicateMap.keySet()];

		//Fetch the contact detail according the duplicateMap keyset. Author by Jack on 02 Aug 2013
		
		//TODO: fix
        // List<Contact> existingconlist = [select Id, Resume__c,Resume_Source__c,Resume_Link__c,jobApplication_Id__c,Skill_Group__c from Contact where Id in: dupSet.duplicateMap.keySet()];
        List<Contact> existingconlist = ContactServices.getContactsForDedupUpdate(new List<Id>(dupSet.duplicateMap.keySet()));
        
        for(Contact con: existingconlist){
        	String mergeToId = con.id;
            //Contact con = new Contact(Id=mergeToId);
            //TODO: can the list in duplicateMap be null?
            // dups is a list of incoming duplicate, which should be merged to existing candidate
            List<Contact> dups  = dupSet.duplicateMap.get(mergeToId);
            Integer lastDupIndex = dups.size()-1;
            // Only the resume content of last duplicate record will be used as it's the lates resume content
            Contact latestCon = dupCandMap.get(dups.get(lastDupIndex).Id);
            
            //Added by Tony
            List<Contact> duplicateContacts = new List<Contact>();
            for(Contact c : dups){
                duplicateContacts.add(dupCandMap.get(c.Id));
            }
            con = ContactServices.updatedContactForDedup(con, duplicateContacts);
            
            /* delete by Tony
            if(latestCon.Resume__c != null && latestCon.Resume__c.length()>0){
            	con.Resume__c = latestCon.Resume__c;
            }
            if(latestCon.Resume_Source__c != null && latestCon.Resume_Source__c.length()>0){
            	con.Resume_Source__c = latestCon.Resume_Source__c;
            }
            if(latestCon.Resume_Link__c != null && latestCon.Resume_Link__c.length()>0){
            	con.Resume_Link__c = latestCon.Resume_Link__c;
            }
            con.jobApplication_Id__c = latestCon.jobApplication_Id__c;
            
            //Add not exist skill group from duplicate contact to original contact. Author by Jack on 24 Jul 2013 
            String tempString  = '';
            tempString = ContactDomain.addNotExistSkillGrouptoOrignalContact(con.Skill_Group__c, latestCon.Skill_Group__c);
	        con.Skill_Group__c = tempString;
	        */
	        
            
            contact2Update.add(con);
            
            // Only employment history of the last duplicate candidate will be kept and merged into the existing candidate
            if(candId2eh.get(dups.get(lastDupIndex).Id) != null){
                for(Employment_History__c eh:candId2eh.get(dups.get(lastDupIndex).Id)){
                    eh.Contact__c = mergeToId;
                    eh2Update.add(eh);
                }
            }
            
            // Only education history of the last duplicate candidate will be kept and merged into the existing candidate
            if(candId2edu.get(dups.get(lastDupIndex).Id) != null){
                for(Education_History__c edu:candId2edu.get(dups.get(lastDupIndex).Id)){
                    edu.Contact__c = mergeToId;
                    edu2Update.add(edu);
                }
            }            
                        
            for(Contact dup:dups){
                
                // Changed by Lina
                if (allDupCandiCMMap.get(dup.Id) != null) {
                    // Merge candidate management from new duplicate record to existing candidate record
                    for (Placement_Candidate__c cm : allDupCandiCMMap.get(dup.Id)) {
                        cm.Candidate__c = mergeToId;
                        cm2Update.add(cm);
                    }
                }
                
                // Merge resumes from new duplicate record to existing candidate record
                if(resumeOfDupCand.get(dup.Id) !=null){
                    for(Web_Document__c webDoc:resumeOfDupCand.get(dup.Id)){
                        webDoc.Document_Related_To__c = mergeToId;
                    }
                    resume2Update.addAll(resumeOfDupCand.get(dup.Id));
                }
                // Merge skills to existing candidate records. 
                // All skills will be move to existing candidate record, 
                // and then skill dedup trigger(another trigger) will
                // remove duplicate skills.
                if(candId2cs.get(dup.Id) != null){
                    for(Candidate_Skill__c cs: candId2cs.get(dup.Id)){
                        cs.Candidate__c = mergeToId;
                        cs2Update.add(cs);
                    }
                }
            }
        }
        TriggerHelper.SyncTimeSheetApproverEnable=false;
        //TriggerHelper.Deduplication=false;
        TriggerHelper.CandidatPlaced=false;
        TriggerHelper.AutoCreateUnavailableDays = false;
        TriggerHelper.UpdateReminingEnable = false;
            
        if(dupSet.duplicateList.size()>0){
            //contact field List for FLS check
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Contact.Email,
                Contact.Approver_User_Name__c,
                Contact.Availability__c,
                Contact.Status__c 
            };
            fflib_SecurityUtils.checkUpdate(Contact.SObjectType, fieldList);
            update contact2Update;
            // If candidate management dedup is not enable, update candidate management
            // of duplicate records and link them to existing recrods.
            fflib_SecurityUtils.checkFieldIsUpdateable(Placement_Candidate__c.SObjectType, Placement_Candidate__c.Candidate__c);
            update cm2Update;
            update cs2Update;               
            update resume2Update;
            fflib_SecurityUtils.checkFieldIsUpdateable(Employment_History__c.SObjectType, Employment_History__c.Contact__c);
            update eh2Update;
            fflib_SecurityUtils.checkFieldIsUpdateable(Education_History__c.SObjectType, Education_History__c.Contact__c);
            update edu2Update;
            fflib_SecurityUtils.checkObjectIsDeletable(Contact.SObjectType);
            delete dupSet.duplicateList;
            fflib_SecurityUtils.checkObjectIsDeletable(Employment_History__c.SObjectType);
            delete eh2Del;
            fflib_SecurityUtils.checkObjectIsDeletable(Education_History__c.SObjectType);
            delete edu2Del;
            // If candidate management dedup enable, remove candidate management
            // of duplicate records.
            if(enableCMDedup) {
                // Added by Lina to fix candidate management does not dedup problem
                // 02/08/2016
                Set<Id> cmIdSet = new Set<Id>();
                for (Placement_Candidate__c cm : incomingApplications) {
                    cmIdSet.add(cm.Id);
                }                
                
                CommonSelector.checkRead(Placement_Candidate__c.sObjectType, 'Id, Candidate__c, Placement__c, Job_Application_Id__c, Job_Application_Ext_Id__c');
                List<Placement_Candidate__c> cmNeedDedup = [Select Id, Candidate__c, Placement__c, Job_Application_Id__c, Job_Application_Ext_Id__c from Placement_Candidate__c where Id in: cmIdSet];
                dedupCM(cmNeedDedup);
            }
        }
        else if(enableCMDedup){
        	dedupCM(incomingApplications);
        }
    }

    private static void dedupCM(List<Placement_Candidate__c> cm2Update){
        List<Id> vacIdFilter = new List<Id>(); // List of vacancy ids that incoming duplicates link to
        List<Id> cmIdFilter = new List<Id>(); // List of cm ids that incoming duplicates link to        
        List<Id> candIdFilter = new List<Id>();  // List of candidate ids of incoming duplicates
        Map<String,Placement_Candidate__c> cmDupCheckMap = new Map<String,Placement_Candidate__c>(); // A map used to check duplicate of a cm
        List<Placement_Candidate__c> duplicateCM = new List<Placement_Candidate__c>(); // Duplicate cms which will be deleted
        
        for(Placement_Candidate__c cm:cm2Update){
            vacIdFilter.add(cm.Placement__c);
            cmIdFilter.add(cm.Id);
            candIdFilter.add(cm.Candidate__c);
        }
        
        CommonSelector.checkRead(Placement_Candidate__c.sObjectType, 'Id, Candidate__c, Placement__c, Job_Application_Id__c, Job_Application_Ext_Id__c');
        List<Placement_Candidate__c> potentialDupCM = [select Id,Placement__c,Candidate__c,Job_Application_Id__c, Job_Application_Ext_Id__c from Placement_Candidate__c where Placement__c in:vacIdFilter and 
                                                      Candidate__c in:candIdFilter and Id not in:cmIdFilter];

        for(Placement_Candidate__c cm:potentialDupCM){
            String key = ''+cm.Placement__c+cm.Candidate__c;
            cmDupCheckMap.put(key,cm);        
        }

        // If a incoming candidate management is duplicate to existing candidate management,
        // it will be added to a list and deleted from the system in the later stage.
        //if(potentialDupCM.size()>0){
        	Map<Id, Placement_Candidate__c> updateJobApplicationidCMList = new Map<Id, Placement_Candidate__c>();
            for(Placement_Candidate__c cm:cm2Update){
                String key = ''+cm.Placement__c+cm.Candidate__c;
                if(cmDupCheckMap.get(key)!=null){
                	if(cmDupCheckMap.get(key).Job_Application_Ext_Id__c == null || 
                			cmDupCheckMap.get(key).Job_Application_Ext_Id__c <= cm.Job_Application_Ext_Id__c){//if duplicate application id of deplicate CM is greater than existing cm, assign the new Jobapplication id to existing cm. Add to update list
                		cmDupCheckMap.get(key).Job_Application_Ext_Id__c = cm.Job_Application_Ext_Id__c;      //Add by Jack. Start use new Job Application Id field to store the value. When duplicate, set the value to new field
                		cmDupCheckMap.get(key).Job_Application_Id__c = cm.Job_Application_Id__c;
                		updateJobApplicationidCMList.put(cmDupCheckMap.get(key).id, cmDupCheckMap.get(key));
                	}

                    duplicateCM.add(cm);
                }else{
                	cmDupCheckMap.put(key,cm); 
                }
            }
            if(duplicateCM.size()>0)
                delete duplicateCM;
            
            //Update jobapplication id in CM 
            if(updateJobApplicationidCMList.size()>0){
            	List<Placement_Candidate__c> updatelist = new List<Placement_Candidate__c>();
            	for(String key : updateJobApplicationidCMList.keySet()){
            		updatelist.add(updateJobApplicationidCMList.get(key));
            	}

            	if(updatelist.size()>0)
                update updatelist;//update job application id
            }               
        //}        
    }
    
    public static void enhancedDedupApplications(List<Placement_Candidate__c> incomingApplications, List<Id> excludedRecordTypeId, Boolean enableCMDedup){
        CandidateWorkingSet workingSet = new CandidateWorkingSet(incomingApplications,excludedRecordTypeId);
        mergeDuplicates(workingSet.findDuplicates(),incomingApplications,enableCMDedup);
    }   
    
    /*
        Retrieve a list of contact record type ids. Contacts assigned with these ids should not be
        dedup. 
    */
    public static List<Id> getExcludedRecordTypeIds(){
        List<Id> excludedIdList = new List<Id>();
        PCAppSwitch__c PCAppSwitchInstance = PCAppSwitch__c.getInstance();
        try{
            if( PCAppSwitchInstance !=null && PCAppSwitchInstance.Exclude_From_Dedup__c !=null
                && PCAppSwitchInstance.Exclude_From_Dedup__c.length()>1){
                String[] ids = PCAppSwitchInstance.Exclude_From_Dedup__c.split(';');
                for(Integer i=0;i<ids.size();i++){
                    
                        excludedIdList.add(ids[i]);
                
                }
            }
        }catch(Exception e){
            // if anything goes wrong in getting excluded ids, return a empty list
            system.debug(e);
        }
        return excludedIdList;
    }
    
    /**
        Test if recod id is excluded from dedup.
        
        WARN: This method should not be used in for loop!
    **/
    public static Boolean isRecordIdExcluded(Id id2Test){
        List<Id> excludedIdList = getExcludedRecordTypeIds();
        for(Id tmpId : excludedIdList){
            if(tmpId == id2Test)
                return true;
        }
        return false;
    }

        
    private static boolean isIdentical(Contact con1,Contact con2){
            if(con1.FirstName==null)
                con1.FirstName='';
            if(con2.FirstName==null)
                con2.FirstName='';
            return con1.email == con2.email && con1.FirstName.toLowerCase()==con2.FirstName.toLowerCase() 
                    && con1.LastName.toLowerCase() == con2.LastName.toLowerCase();
    }

}