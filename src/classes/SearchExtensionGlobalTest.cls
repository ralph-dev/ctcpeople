@isTest
public class SearchExtensionGlobalTest {
	public static testmethod void test_fetchRecordType() {
		System.runAs(DummyRecordCreator.platformUser) {
		List<RecordType> recordTypeList = new List<RecordType>();
		recordTypeList = SearchExtensionGlobal.fetchRecordType('Contact');
		system.assertNotEquals(recordTypeList,NULL);
		//system.debug('recordTypeList ='+ recordTypeList);
		}
	}

    @testSetup static void shiftsJSON() {
        //String JSONString = '[{"start":"2015-06-08","startDate":"2015-06-08","end":"2015-06-14","endDate":"2015-06-14","sType":"Day","sTime":"09:00","eTime":"11:00","sLocation":"Test Address","sVacantPositionNumber":1,"sVacId":"a0K28000000GHxbEAG","sCompanyId":"00128000002u8lLAAQ","sRecurEndDate":"2015-06-27","sWeeklyRecur":true,"sWeekdaysDisplay":"Mon;Tue;Wed;Thu"},{"start":"2015-06-15","startDate":"2015-06-15","end":"2015-06-18","endDate":"2015-06-18","sType":"Day","sTime":"09:00","eTime":"11:00","sLocation":"Test Address","sVacantPositionNumber":1,"sVacId":"a0K28000000GHxbEAG","sCompanyId":"00128000002u8lLAAQ","sRecurEndDate":"2015-06-27","sWeeklyRecur":true,"sWeekdaysDisplay":"Mon;Tue;Wed;Thu"},{"start":"2015-06-22","startDate":"2015-06-22","end":"2015-06-25","endDate":"2015-06-25","sType":"Day","sTime":"09:00","eTime":"11:00","sLocation":"Test Address","sVacantPositionNumber":1,"sVacId":"a0K28000000GHxbEAG","sCompanyId":"00128000002u8lLAAQ","sRecurEndDate":"2015-06-27","sWeeklyRecur":true,"sWeekdaysDisplay":"Mon;Tue;Wed;Thu"}]';
		String JSONString = '[{"start":"2015-07-08","startDate":"2015-07-08","end":"2015-07-11","endDate":"2015-07-11","sType":"Day","sTime":"09:00","eTime":"11:00","sLocation":"Test Address","sVacantPositionNumber":1,"sVacId":"a0K28000000GHxbEAG","sCompanyId":"00128000002u8lLAAQ","sRecurEndDate":"2015-07-27","sWeeklyRecur":true,"sWeekdaysDisplay":"Mon;Tue;Wed;Thu"},{"start":"2015-07-15","startDate":"2015-07-15","end":"2015-07-18","endDate":"2015-07-18","sType":"Day","sTime":"09:00","eTime":"11:00","sLocation":"Test Address","sVacantPositionNumber":1,"sVacId":"a0K28000000GHxbEAG","sCompanyId":"00128000002u8lLAAQ","sRecurEndDate":"2015-07-27","sWeeklyRecur":true,"sWeekdaysDisplay":"Mon;Tue;Wed;Thu"},{"start":"2015-07-22","startDate":"2015-07-22","end":"2015-07-25","endDate":"2015-07-25","sType":"Day","sTime":"09:00","eTime":"11:00","sLocation":"Test Address","sVacantPositionNumber":1,"sVacId":"a0K28000000GHxbEAG","sCompanyId":"00128000002u8lLAAQ","sRecurEndDate":"2015-07-27","sWeeklyRecur":true,"sWeekdaysDisplay":"Mon;Tue;Wed;Thu"}]';
		
		List<ShiftDTO> shiftObjs = (List<ShiftDTO>)JSON.deserialize(JSONString,List<ShiftDTO>.class);
		system.assert(shiftObjs.size()>0);
        
    }
	
	private static testMethod void testSelectField() {
		System.runAs(DummyRecordCreator.platformUser) {
		    System.assertNotEquals(null, SearchExtensionGlobal.selectField());
		}
	}
	
	private static testMethod void testOperator() {
		System.runAs(DummyRecordCreator.platformUser) {
	    System.assertNotEquals(null, SearchExtensionGlobal.operator());
		}
	}
	
	private static testMethod void testCandidatesJSON() {
		System.runAs(DummyRecordCreator.platformUser) {
	    System.assertNotEquals(null, SearchExtensionGlobal.candidatesJSON(new List<String> { '[{"sId" : "123"}]', '{ "field1" : "value1", "field2" : "value2"}', '{ "field1" : "value1", "field2" : "value2"}', '{ "field1" : "value1", "field2" : "value2"}', '{ "field1" : "value1", "field2" : "value2"}', '100', '-33.867487', '151.206990' }));
		}
	}
	
// 	private static testMethod void testCreateCMAndRostering() {
// 	    System.assertNotEquals(null, SearchExtensionGlobal.createCMAndRostering(new List<String> { 'one', 'two' }));
// 	}
	
	private static testMethod void testGetCTCAppSwitch() {
		System.runAs(DummyRecordCreator.platformUser) {
	    System.assertNotEquals(null, SearchExtensionGlobal.getCTCAppSwitch());
		}
	}
}