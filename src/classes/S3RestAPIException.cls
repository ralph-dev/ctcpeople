public without sharing class S3RestAPIException extends Exception{
    public enum S3RestAPIExceptionCode{UNKNOWN,NULL_CREDENTIAL,AUTH_FAIL,UNKNOWN_CALLOUT_FAILURE}
    public S3RestAPIExceptionCode errorCode{get;set;}
    public Map<Integer,String> pcErrorMsgMap = null;
    public String additionalMsg;
    //public Integer s3ServerErrorCode{get;set;}
    //public String s3ServerErrorMsg{get;set;}
    
    public S3RestAPIException(S3RestAPIExceptionCode errorCode,String additionalMsg){
        this.errorCode = errorCode;
        //this.s3ServerErrorCode = s3ServerErrorCode;
        //this.s3ServerErrorMsg = s3ServerErrorMsg;
        this.additionalMsg = additionalMsg;
        pcErrorMsgMap = new Map<Integer,String>();
        pcErrorMsgMap.put(S3RestAPIExceptionCode.UNKNOWN.ordinal(),'Encounter an unkown issue, please refer to return Salesforce interal exception message to track down the issue.');
        pcErrorMsgMap.put(S3RestAPIExceptionCode.NULL_CREDENTIAL.ordinal(),'Either Amazon S3 access key or Amazon S3 secret key is empty or invalid.');
        pcErrorMsgMap.put(S3RestAPIExceptionCode.AUTH_FAIL.ordinal(),'Amazon fail to authenticate your Amazon S3 credential. They may be wrong or deleted.');
        pcErrorMsgMap.put(S3RestAPIExceptionCode.UNKNOWN_CALLOUT_FAILURE.ordinal(),'Encounter an unknown callout issue, please refer to return Salesforce interal exception message to track down the issue.');
        setMessage( pcErrorMsgMap.get(errorCode.ordinal())+' '+additionalMsg);
    }
    
    public S3RestAPIException(S3RestAPIExceptionCode errorCode,Exception e){
        this(errorCode,'');    
        this.additionalMsg = 'SF internal Cause:'+e.getCause()+'\n'
                        +'SF internal exception message:'+e.getMessage()+'\n'
                        +'SF internal stack trace:'+e.getStackTraceString()+'\n'
                        +'SF internal exception type:'+e.getTypeName();
        setMessage( pcErrorMsgMap.get(errorCode.ordinal())+' '+additionalMsg);                
    }
    /*
        Return a univeral error code that used in People Cloud application.
        Please reference PeopleCloudErrorInfo for detail;
    */
    public Integer getPeopleCloudErrInfoCode(){
        return 400+errorCode.ordinal();
    }

}