/**
* The factory provide some ways to create the corresponding instance of IEmailParser for these emails 
* coming from :
* 
* This class use apex reflection to create instance of parser by getting parser name from Parser Registion map 
* instead of directory new instance of parser. 
* as the result, we can dynamically configure parser for different type  of email.
*/

public with sharing class CandidateParserFactory {
	
	
	
	public static Map<String, String> parserRegister = new Map<String, String>();
	
	//initialise the parser registion
	static{
		
		//by subject
		parserRegister.put('jxt', 'JXTEmailParser');
		parserRegister.put('cv' , 'CVEmailParser');
		parserRegister.put('resume' , 'CVEmailParser'); 
        parserRegister.put('application' , 'CVEmailParser'); 
	    
		
	}
	
	/*
	* Get an object of candidate parser according to the email
	* Param:
	* 	email - Messaging.InboundEmail
	* return:
	*	a parser object - IEmailParser
	*/
	public static ICVEmailParser getParser( Messaging.InboundEmail email ){
		ICVEmailParser parser = null;
		
		String parserName = getParserName( email );
		
		//new an instance of parser depending on parser name
		parser = createParser(parserName);
		if(parser != null ){
			parser.setEmail( email );
		}
		
		return parser;
		
	}
	
	/*
	* Get the class name of candidate parser according to the email
	* Param:
	* 	email - Messaging.InboundEmail
	* return:
	*	the class name of parser - String
	*/
	public static String getParserName( Messaging.InboundEmail email ){
	    
	    String parserName = '';
	    
	    if(email == null ) return parserName;
		
		String subject = email.subject;
		//get parser name by subject
		if(parserName == null || parserName == ''){

			for( String sub :  parserRegister.keySet() ){
				
				if( subject != null && subject.toLowerCase().contains( sub) ){
					parserName = parserRegister.get(sub);
					break;
				}	
			}
		}
		
		//continue to check the attachment number
		if(! parserName.equals('') ){
			Boolean valid = false;
			if(email.binaryAttachments != null){
				for(Integer i = 0; i < email.binaryAttachments.size() ; i++){
					String attachmentName = email.binaryAttachments[i].filename;
					
					if(isValidAttachment(attachmentName) ){
								
						valid = true;
						break;
					}		
				}
			}
			
			if( !valid ){
				if(parserName != 'JXTEmailParser') {
				    
				    parserName = '';
				}
				
				System.debug('Failed to find out a valid attachment');
			}
		}else{
			System.debug('Subject is not as expected !');
		}
		
		return parserName;
		
	}
	
	/*
	* Create an instance of parser according to the class name of parser
	* Param:
	* 	parsesr name - String
	* return:
	*	a parser object - IEmailParser
	*/
	public static ICVEmailParser createParser(String parserName){
		
		ICVEmailParser parser  = null;
		
		if(parserName != null && parserName != ''){
			try{
				Type parserType = Type.forName('', parserName);
				if(parserType != null){
				    parser =(ICVEmailParser) parserType.newInstance();
				}
				 
			}catch(TypeException e){
				System.debug('Failed to create an instance of parser ' + parserName);
				System.debug(e);
				//TODO more exception handling
				
			}
		}else{
			System.debug('Parser class name can not be empty !');
		}
		
		return parser;
	}
	
	public static Boolean isValidAttachment(String attachmentName){
		if(attachmentname == null ){
			return false;
		}
		attachmentName = attachmentName.toUpperCase().trim();
		return attachmentName.endsWith('.DOC')
								||attachmentName.endsWith('.DOCX')
								||attachmentName.endsWith('.PDF')
								||attachmentName.endsWith('.RTF')
								||attachmentName.endsWith('.PAGES') ;
	}
	
}