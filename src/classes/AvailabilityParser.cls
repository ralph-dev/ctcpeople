public with sharing class AvailabilityParser {
    final static String AVAIL_STATUS = 'eventStatus'; 
    final static String START_DATE = 'start'; 
    final static String END_DATE = 'end'; 
    
   
    public static List<AvailabilityEntity> convertJsonStringToObjects(String avJsonString){
		//System.debug('avJsonString:' + avJsonString);

		List<AvailabilityEntity> avlList = new List<AvailabilityEntity>();
		
		JSONParser parser = JSON.createParser(avJsonString);
		Days_Unavailable__c avl;
		
		while (parser.nextToken() != null) {
			if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
				AvailabilityEntity avlentity = (AvailabilityEntity)parser.readValueAs(AvailabilityEntity.class);
				//System.debug('=================== avlentity:' + avlentity);
				avlList.add(avlentity);	
			}
		}
		
		//System.debug('--- avlList:' + avlList);	
		return avlList;
    }
    
    
    
    public static List<AvailabilityEntity> convertObjectsToJsonString(List<Days_Unavailable__c> duvlList){
    	//System.debug('duvlList:' + duvlList);	
    	List<AvailabilityEntity> avlList = new List<AvailabilityEntity>();
    	
    	for(Days_Unavailable__c duvl : duvlList){
    		AvailabilityEntity avl = new AvailabilityEntity();
    		avl.startDate = duvl.Start_Date__c + '';
    		avl.endDate = duvl.End_Date__c + '';
    		avl.candidateId=duvl.Contact__c;
    		avl.candidateManagementId=duvl.Candidate_Management__c;
    		avl.startDate=avl.startDate.replace('00:00:00','');
    		avl.endDate=avl.endDate.replace('00:00:00','');
    		avl.candidateManagementName=duvl.Candidate_Management__r.name;
    		avl.vacancyName=duvl.Candidate_Management__r.Placement__r.name;
    		avl.clientName=duvl.Candidate_Management__r.Placement__r.Company__r.name;
    		avl.eventStatus = duvl.Event_Status__c;
    		avl.id = duvl.Id;
    		avl.eventReason=duvl.EventReason__c;
    		avlList.add(avl);	
    	}
    	
    	//System.debug('avlList:' + avlList);
    	return avlList;			
    }


}