public with sharing class ContactAstutePayrollImplementation {
	
	private static ContactServices contServ=new ContactServices();
	/****
		Methods to syncrhonize biller contacts
		This method is called from future jobs.
	
	*****/
	
	public static void syncBillerContacts(list<String> contactIds, String session){
		list<Contact> billerContacts=contServ.retrieveBillerContactsByIds(contactIds);
		list<map<String,Object>> objSTR=contServ.convertContactToBillerContact(billerContacts);
		map<String,Object> messagesJson=new map<String,Object>();
		messagesJson.put('billerContactsSaves',objSTR);
		String messages=JSON.serialize(messagesJson);
		String endPointStr=Endpoint.getAstuteEndpoint();
		endPointStr=endPointStr+'/AstutePayroll/billercontact';
		AstutePayrollFutureJobs.pushHandler(messages, endpointStr, session);
	}
	
	/***
		Methods to synchronize approver contacts
		This method is called from future jobs.
	
	******/
	public static void syncApproverContacts(list<String> contactIds, String session){
		list<Contact> approvers=contServ.retrieveApproverContactsByIds(contactIds);
		list<map<String,Object>> objSTR=contServ.convertContactToApprover(approvers);
		map<String,Object> messagesJson=new map<String,Object>();
		messagesJson.put('userSaves',objSTR);
		String messages=JSON.serialize(messagesJson);
		String endPointStr=Endpoint.getAstuteEndpoint();
		endPointStr=endPointStr+'/AstutePayroll/approver';
		AstutePayrollFutureJobs.pushHandler(messages, endpointStr, session);	
	}
	
	/***
		Methods to synchronize employee contacts
		This method is called from future jobs.
	***/
	public static void syncEmployeeContacts(list<String> contactIds, String session){
		list<Contact> employees=contServ.retrieveCandidateToEmployeesByIds(contactIds);
		list<String> candIds=new list<String>();
		for(Contact employee: employees){
			candIds.add(employee.Id);
		}
		PlacementServices pServices=new PlacementServices();
		map<String,String> candidatePlacementMap=pServices.fetchCanPlacementIDMap(candIds);
		list<map<String,Object>> objSTR=
			contServ.convertCandidateToEmployee(employees, candidatePlacementMap);
		map<String,Object> messagesJson=new map<String,Object>();
		messagesJson.put('userSaves',objSTR);
		String messages=JSON.serialize(messagesJson);
		String endPointStr=Endpoint.getAstuteEndpoint(); 
		endPointStr=endPointStr+'/AstutePayroll/employee';
		AstutePayrollFutureJobs.pushHandler(messages, endpointStr, session);
			
	}
    /***
		This will happend before the update of the contacts
		(The update is fired by a workflow)
		Fire a future job in AstutePayroll FutureJobs to update contacts
		@param list<contact>
		
		1. Is Approver Pushing To AP
		2. Is Biller Contact Pushing To AP
		3. Is Employee Pushing To AP
		
		
					--Alvin		
	****/
    public static void astutePayrollTrigger(list<Contact> conds){
    	CTCPeopleSettingHelperServices ctcPeopleSettingHelperServ=new CTCPeopleSettingHelperServices();
    	if(!ctcPeopleSettingHelperServ.hasApAccount()){
    		return;
    	}
    	list<String> approverIds=new list<String>();
    	list<String> billerContactIds=new list<String>();
    	list<String> employeeIds=new list<String>();
    	list<String> contactsForBillerContactFix=new list<String>();
        list<String> contactsForApproverFix=new list<String>();
    	
    	for(Contact perContact :  conds){
    		
    		
    		if(perContact.Is_Fixing_Data_for_Approver_in_AP__c){
    			perContact.Is_Fixing_Data_for_Approver_in_AP__c=false;
    			contactsForApproverFix.add(perContact.Id);
    			perContact.Astute_Payroll_Upload_Status__c='On Hold';
    		}else{
	    		if(perContact.Is_Approver_Pushing_To_AP__c){
	    			approverIds.add(perContact.Id);
	    			perContact.Is_Approver_Pushing_To_AP__c=false;
	    			perContact.Astute_Payroll_Upload_Status__c='On Hold';
	    		}
    		}
    		
    		if(perContact.Is_Fixing_Data_For_Biller_Contact_in_AP__c){
    			perContact.Is_Fixing_Data_For_Biller_Contact_in_AP__c=false;
    			contactsForBillerContactFix.add(perContact.Id);
    			perContact.Astute_Payroll_Upload_Status__c='On Hold';
    		}else{
	    		if(perContact.Is_Biller_Contact_Pushing_To_AP__c){
	    			billerContactIds.add(perContact.Id);
	    			perContact.Is_Biller_Contact_Pushing_To_AP__c=false;
	    			perContact.Astute_Payroll_Upload_Status__c='On Hold';
	    		}
    		}
    		if(perContact.Is_Employee_Pushing_To_AP__c){
    			employeeIds.add(perContact.Id);
    			perContact.Is_Employee_Pushing_To_AP__c=false;
    			perContact.Astute_Payroll_Upload_Status__c='On Hold';
    		}
    	}
    	String session=AstutePayrollUtils.GetloginSession();
    	if(approverIds.size()>0){
    		AstutePayrollFutureJobs.syncContactApproverFuture(approverIds, session);
    	}
    	if(billerContactIds.size()>0){
    		AstutePayrollFutureJobs.syncContactBillerContactFuture(billerContactIds, session);
    	}
    	if(employeeIds.size()>0){
    		AstutePayrollFutureJobs.syncContactEmployeeFuture(employeeIds, session);
    	}
    	 PlacementServices placementServ=new PlacementServices();
        if(contactsForBillerContactFix.size()>0){
            placementServ.fixBillerContact(contactsForBillerContactFix);
            InvoiceItemServices invoiceItemServ=new InvoiceItemServices();
            invoiceItemServ.fixBillerContact(contactsForBillerContactFix);
        }
        if(contactsForApproverFix.size()>0)
            placementServ.fixApprover(contactsForApproverFix);
    	
    	
    	
    	
    }
}