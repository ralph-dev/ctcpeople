/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

@IsTest(SeeAllData=true)
public class TestGoogleSearch2 {
   // public static testmethod void testSavedSearch(){
   // 	System.runAs(DummyRecordCreator.platformUser) {
   //     String tempNameString = 'validateName';
   //     String tempboolean = GoogleSearch2.validateName(tempNameString);
   //     system.assert(tempboolean.contains(String.valueof(PeopleCloudErrorInfo.GOOGLE_SEARCH_SAVE_SEARCH_CRITERIA_RECORDNAME_SUCC)));
   //     String tempReturnResultString = GoogleSearch2.saveSearchJsonString('',tempNameString);
   //     system.assert(tempReturnResultString.contains(String.valueof(PeopleCloudErrorInfo.GOOGLE_SEARCH_SAVE_SEARCH_CRITERIA_SUCC)));
   //     StyleCategory__c savedSearch = new StyleCategory__c(Name='test',RecordTypeId=DaoRecordType.GoogleSaveSearchRT.Id);
   //     insert savedSearch;
   //     String tempUpdateResultString = GoogleSearch2.updateSavedSearch('', savedSearch.id);
   //     system.assert(tempUpdateResultString.contains(String.valueof(PeopleCloudErrorInfo.GOOGLE_SEARCH_UPDATE_SEARCH_CRITERIA_SUCC)));
   // 	}
   // }
    
   // public static testmethod void testRetreiveSavedSearchesJSON(){
   // 	System.runAs(DummyRecordCreator.platformUser) {
   //     List<StyleCategory__c> toBeDeleted =  [select Id from StyleCategory__c where RecordTypeId=:DaoRecordType.GoogleSaveSearchRT.Id];
   //     delete toBeDeleted;
   //     String JSONStr = GoogleSearch2.retreiveSavedSearchesJSON();
   //     system.assertEquals('{"personalSavedSearches":[],"otherSavedSearches":[]}',JSONStr);
   // 	}
   // }
    
   // public static testmethod void testDelSavedSearch(){
   // 	System.runAs(DummyRecordCreator.platformUser) {
   //     StyleCategory__c savedSearch = new StyleCategory__c(Name='test',RecordTypeId=DaoRecordType.GoogleSaveSearchRT.Id);
   //     insert savedSearch;
   //     StyleCategory__c insertedSavedSearch = [select id from StyleCategory__c where id=:savedSearch.Id];
   //     system.assertNotEquals(insertedSavedSearch,null);
   //     GoogleSearch2.delSavedSearch(savedSearch.Id);
   //     Integer numberOfSavedSearch = [select count() from StyleCategory__c where id=:savedSearch.Id];
   //     system.assertEquals(0,numberOfSavedSearch);
   // 	}
   // }
    
   // public static testmethod void testAdd2VacancyRemoteAction(){
   // 	System.runAs(DummyRecordCreator.platformUser) {
   //     Account company = new Account(Name='client');
   //     insert company;
        
   //     RecordType rt = [select Id from RecordType where SobjectType =: PeopleCloudHelper.getPackageNamespace()+'Placement__c' limit 1];
   //     Placement__c p1=new Placement__c(RecordTypeId= rt.Id,Company__c=company.Id,Name='placement 1');
   //     insert p1;
        
   //     RecordType candRT=DaoRecordType.indCandidateRT;
   //     Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand1@test.ctc.test',Candidate_From_Web__c=true,jobApplication_Id__c=123456789,Resume_Source__c='abc1');
   //     Contact cand2 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 2',Email='Cand2@test.ctc.test',Candidate_From_Web__c=true,jobApplication_Id__c=123456779,Resume_Source__c='abc2');
   //     Contact cand3 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 3',Email='Cand3@test.ctc.test',Candidate_From_Web__c=true,jobApplication_Id__c=123456709,Resume_Source__c='abc3');
   //     List<Contact> conList = new List<Contact>();
   //     conList.add(cand1);
   //     conList.add(cand2);
   //     conList.add(cand3);
   //     insert conList;
         
   //     String selectedIds = cand1.Id+':'+cand2.Id+':'+cand3.Id;
   //     Map<Integer, String> allSelectedIdsMap = new Map<Integer, String>();
   //     allSelectedIdsMap.put(1,selectedIds);
        
   //     Integer result = GoogleSearch2.add2VacancyRemoteAction2(allSelectedIdsMap, p1.Id);
   //     Integer result2 = GoogleSearch2.add2VacancyRemoteAction(null, null);
        
   //     system.assertEquals(result,conList.size());
   //     system.assertEquals(result2,null);
   // 	}
   // }
    
   // public static testmethod void testcreateCandidatesIdJSON(){
   // 	System.runAs(DummyRecordCreator.platformUser) {
   //     Map<Integer, String> tempmap = new Map<Integer, String>();
   //     tempmap.put(1,'1:2:3');
   //     String result = GoogleSearch2.createCandidatesIdJSON(tempmap, '');
   //     system.assert(result!='');
   //     result = GoogleSearch2.sendbulksms('','','');
   //     system.assert(result.contains('advancedContactSearch'));
   //     result = GoogleSearch2.sendbulkemail('1', '', '');
   //     system.assert(result.contains('advancedContactSearch'));
   // 	}
   // }
    
   // // test getGSAContactResult4DefaultView()
   // // test getGSAContactResult4ColumnView()
   //public static testmethod void getGSAContactResult(){
   //	System.runAs(DummyRecordCreator.platformUser) {
   //     RecordType candRT=DaoRecordType.indCandidateRT;
   //     Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand1@test.ctc.test',Candidate_From_Web__c=true,jobApplication_Id__c=123456789,Resume_Source__c='abc1');
   //     Contact cand2 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 2',Email='Cand2@test.ctc.test',Candidate_From_Web__c=true,jobApplication_Id__c=123456779,Resume_Source__c='abc2');
   //     Contact cand3 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 3',Email='Cand3@test.ctc.test',Candidate_From_Web__c=true,jobApplication_Id__c=123456709,Resume_Source__c='abc3');
   //     List<Contact> conList = new List<Contact>();
   //     conList.add(cand1);
   //     conList.add(cand2);
   //     conList.add(cand3);
   //     insert conList;
        
   //     List<Id> ids2Display = new List<Id>();
   //     ids2Display.add(cand1.Id);
   //     ids2Display.add(cand2.Id);
   //     ids2Display.add(cand3.Id);
        
   //     GoogleSearch2 gs = new GoogleSearch2(true);
   //     gs.fetchGSAResult();
   //     gs.getGSAContactResult4DefaultView(ids2Display);
   //     gs.getGSAContactResult4ColumnView(ids2Display);
   //     system.assertNotEquals(null, gs.getGSAContactResult4ColumnView(ids2Display));
   //	}
   // }       
    
    
   // public static testmethod void testSaveGsaResult2Document(){
   // 	System.runAs(DummyRecordCreator.platformUser) {
   //     String dummyString = 'this is a test only...';
   //     GoogleSearch2 gs = new GoogleSearch2(true);
   //     String documentId = gs.saveGsaResult2Document(dummyString);
   //     System.assert(documentId != null && documentId != '');
   // 	}
   // }
    
   // /*
   //     Added by Alvin
    
   // *****/
   //  public static testmethod void testRetrunFromBulkEmailandSMSFunction(){
   //  	System.runAs(DummyRecordCreator.platformUser) {
   //  	Placement__c placement = new Placement__c(Name='ThisIsATest');
   //     String FolderId = UserInfo.getUserId();     
   //     Document doc1=new Document();
   //     doc1.Name='GS_'+Userinfo.getuserid()+'_1381378548899';
   //     doc1.Body=Blob.valueOf('{"3":"0039000000Id544AAB:0039000000GLIlHAAX:0039000000RSe5SAAT","2":"0039000000P7HzeAAF:0039000000NQswcAAD:0039000000NPvfBAAT","1":"0039000000Id5s3AAB:0039000000Id5rtAAB:0039000000P7H3zAAF"}');
   //     doc1.FolderId=FolderId;
   
   //     insert doc1;
   //     StyleCategory__c style1=new StyleCategory__c();
   //     style1.name='Bulk_SEND_TEMP_1381378585340';
   //     style1.ascomponents__c='{"keywords":"email","objsingle":{},"objmulti":{},"objselectedskills":"[]","operatorMap":"{}"}';
   //     insert style1;
        
   //     Apexpages.currentPage().getParameters().put('cjid',doc1.id);
   //     Apexpages.currentPage().getParameters().put('scjid',style1.id);
   //     ApexPages.StandardController stdController = new ApexPages.StandardController(placement);
   //     GoogleSearch2 gs = new GoogleSearch2();
   //     gs.googleSearchPreviousMethodInit();
   //     String name=gs.docname;
   //     String[] names=name.split('_');
   //     name=names[0]+'_'+names[1];
   //     String name1='GS_'+Userinfo.getuserid();
   //     System.assertEquals(name, name1);
   //  	}
   //  }
   //  /*End**/
    
   // public static testmethod void testSearchRequestGeneration(){
   // 	System.runAs(DummyRecordCreator.platformUser) {
   //     TriggerHelper.DisableTriggerOnContact = true;
   //     PeopleCloudHelper.PeopleCloudTestDataSet testData = PeopleCloudHelper.prepareData();
   //     ApexPages.StandardController stdController = new ApexPages.StandardController(testData.vacancies[0]);
   //     GoogleSearch2 gs = new GoogleSearch2(true);
   //     Integer start = 1;
   //     Integer offset = 1000;
        
   //     gs.init();
   //     gs.googleSearchPreviousMethodInit();
        
   //     /* Test multi-range availability search request generation*/
   //     gs.gsaAvailabilityCriteria = '["inmeta:Available:2014-01-08..2014-01-23","inmeta:Available:2014-02-11..2014-02-11","inmeta:Available:2014-02-25..2014-02-25"]';
   //     gs.gsaKeywords = 'email';
   //     gs.gsaMulCriteria = '(Industry_1__c:Adhesives|Industry_1__c:Aero%2520Space)';
   //     gs.gsaResultSortBy = '&sort=date:D:S:d1';
   //     gs.gsaSingleCriteria = 'Email:email';
   //     gs.gsaSkillCriteria = '';
   //     gs.collectionName = 'collection';
   //     gs.availabilityCriteriaJSON = '[{"startDate":"2014-01-08","endDate":"2014-01-23"},{"startDate":"2014-02-11","endDate":"2014-02-11"},{"startDate":"2014-02-25","endDate":"2014-02-25"}]';
   //     gs.endpointURL = 'https://www.clicktocloud.com';
        
   //     HttpRequest req = gs.generateReqForMultiRangeAvailabilitySearch(start, offset);
   //     String searchEndpoint = Endpoint.getPeoplecloudWebSite() + '/GSAQuerier/MultiRangeAvailabilitySearch';
   //     searchEndpoint += '?sid='+EncodingUtil.urlEncode(UserInfo.getSessionId(), 'UTF-8');
   //     searchEndpoint += '&sorg='+EncodingUtil.urlEncode(UserInfo.getOrganizationId(), 'UTF-8');
   //     searchEndpoint += '&site='+gs.collectionName;
   //     searchEndpoint += '&surl='+EncodingUtil.urlEncode(gs.endpointURL, 'UTF-8');
        
   //     String searchCriteria = '{';
   //     searchCriteria +='"queryTerm":"'+EncodingUtil.urlEncode(gs.gsaKeywords, 'UTF-8')+'",';
   //     searchCriteria +='"availabilityCriteria":'+gs.gsaAvailabilityCriteria+',';
   //     searchCriteria +='"otherSearchCriterias":"start='+start+'&num='+offset+'&site='+gs.collectionName+
   //         '&rc=1&client=rsTransformer&proxystylesheet=rsTransformer&output=xml_no_dtd&filter=0&getfields=Id.Name&requiredfields=providercompanyname:peoplecloud.'
   //         +gs.gsaSingleCriteria+'.'+gs.gsaMulCriteria+gs.gsaResultSortBy+'"';
            
   //     searchCriteria+='}';
   //     searchEndpoint += '&searchCriteria='+EncodingUtil.urlEncode(searchCriteria, 'UTF-8');
   //     system.assertEquals(searchEndpoint, req.getEndpoint());
    
    
   //     /* Test normal search request generation, which with either one or none */
   //     gs.availabilityCriteriaJSON = '[]';
   //     gs.gsaAvailabilityCriteria = '';
        
   //     String searchURL = GoogleSearchUtil2.getCustomSetting('peopleSearch')+'?'+'sid='+EncodingUtil.urlEncode(UserInfo.getSessionId(), 'UTF-8')
   //         +'&surl='+EncodingUtil.urlEncode(gs.endpointURL, 'UTF-8')+'&sorg='+EncodingUtil.urlEncode(UserInfo.getOrganizationId(), 'UTF-8')
   //         +'&q='+EncodingUtil.urlEncode(gs.gsaKeywords, 'UTF-8')+'&start='+start+'&num='+offset+'&site='+gs.collectionName+
   //         '&rc=1&client=rsTransformer&proxystylesheet=rsTransformer&output=xml_no_dtd&filter=0&getfields=Id.Name&requiredfields=providercompanyname:peoplecloud.'
   //         +gs.gsaSingleCriteria+'.'+gs.gsaMulCriteria+gs.gsaResultSortBy;
   //     req = gs.generateReqForNormalSearch(start, offset);
   //     system.assertEquals(searchURL, req.getEndpoint());
   // 	}
        
   // }
   public static testmethod void unitTest(){
      GoogleSearch2.SendBulkEmail();
      GoogleSearch2.saveSearchJsonString('','');
      GoogleSearch2.validateName('');
      GoogleSearch2.updateSavedSearch('','');
      GoogleSearch2.retreiveSavedSearchesJSON();
      GoogleSearch2.delSavedSearch('');
      GoogleSearch2.retrieveSkillGroupsJSON();
      GoogleSearch2.retrieveAllSkillsJSON();
      GoogleSearch2.add2VacancyRemoteAction2(null, '');
      GoogleSearch2.createCandidatesIdJSON(null, '');
      GoogleSearch2.addCandToVacancy('','','');
      GoogleSearch2.sendbulkemail('','','');
      Integer num = GoogleSearch2.add2VacancyRemoteAction('','');
      System.assertEquals(null, GoogleSearch2.sendbulksms('','',''));
      System.assertEquals(null, num);
   }
}