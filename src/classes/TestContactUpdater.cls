@isTest
public with sharing class TestContactUpdater {
	
	static testmethod void testUpdateCandLastModifyDateByAvailability(){
		System.runAs(DummyRecordCreator.platformUser) {
	   	Contact candidate = createTestCandidate(); 
	   	DateTime initialDt = [select Id, LastModifiedDate from Contact where id =: candidate.Id].LastModifiedDate;
	   			
    	Days_Unavailable__c avl1 = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-14');	
    	Days_Unavailable__c avl2 = createDaysUnavailableRec(candidate.Id,'2013-11-19', '2013-11-20');	
    	List<Days_Unavailable__c> avlList = new List<Days_Unavailable__c>();
    	avlList.add(avl1);
    	avlList.add(avl2);
    	
    	
    	ContactUpdater.updateCandLastModifyDateByAvailability(avlList);
    	Contact candidateAfterUpdate = [select Id, LastModifiedDate from Contact where id =: candidate.Id];
    	DateTime dtAfterUpdate = candidateAfterUpdate.LastModifiedDate;
    	
    	system.debug('initialDt = ' + initialDt);
    	system.debug('dtAfterUpdate = ' + dtAfterUpdate);
    	system.assert(dtAfterUpdate >= initialDt);
    	
    	deletecreateDaysUnavailableRec(avlList);
    	Contact candidateAfterDelete = [select Id, LastModifiedDate from Contact where id =: candidate.Id];
		DateTime dtAfterDelete = candidateAfterDelete.LastModifiedDate;
		system.assert(dtAfterDelete >= initialDt);
		}
	}

	
	// helper method
    // to create a dummy contact record
    private static Contact createTestCandidate(){
        Contact candidate = new Contact(LastName='candidate 1',email='Cand1@test.ctc.test');
    	insert candidate;
    	return candidate;
    }
    
    // create a days unavailable record
    private static Days_Unavailable__c createDaysUnavailableRec(String candidateId, String startDate, String endDate){
    	Days_Unavailable__c duvl = new Days_Unavailable__c();
    	duvl.Start_Date__c = Date.valueOf(startDate);
    	duvl.End_Date__c = Date.valueOf(endDate);
    	duvl.Contact__c = candidateId;
    	insert duvl;
    	return duvl;
    }
    
    private static void deletecreateDaysUnavailableRec(List<Days_Unavailable__c> avl2DelList){
    	delete avl2DelList;
    }
    
}