/**
* Description: This is a helper class for shift
* Author: Emily Jiang
* Date: 01.07.2015
**/
public with sharing class ShiftHelper {
    /**
    * deserialize json string to sObject or sObject list
    *
    **/
    public static List<ShiftDTO> deserializeShiftsJson(String shiftJson){
        List<ShiftDTO> shiftDTOs = new List<ShiftDTO>();
        //System.debug('shiftJson: '+shiftJson);
        try{
            shiftDTOs = (List<ShiftDTO>)JSON.deserialize(shiftJson,List<ShiftDTO>.class);
        }catch(Exception e){
            ShiftDTO shiftObj = (ShiftDTO)JSON.deserialize(shiftJson,ShiftDTO.class);
            shiftDTOs.add(shiftObj);
        }   
        //System.debug('shiftDTOs: '+shiftDTOs);
        return shiftDTOs;
    }
    
    /**
    * convert shift DTO to shift sObject
    * 
    **/
    public static void convertShiftDTOToShiftSObject(Shift__c s, ShiftDTO sDTO){
        s.Location__c = sDTO.getsLocation();
        s.Shift_Type__c = sDTO.getsType();
        s.Recurrence_Weekdays__c = sDTO.getsWeekdaysDisplay();
        if(sDTO.getsWeeklyRecur() != null){
            s.Weekly_Recurrence__c = sDTO.getsWeeklyRecur();
        }else{
            s.Weekly_Recurrence__c = false;
        }
        
        if(sDTO.getStartDate() != '' && sDTO.getStartDate() != null && sDTO.getStartDate() != 'Invalid date'){  
            s.Start_Date__c = Date.valueOf(sDTO.getStartDate());
        }else{
            s.Start_Date__c = null; 
        }
        
        if(sDTO.getsTime() != '' && sDTO.getsTime() != null && sDTO.getsTime() != 'Invalid date'){
            s.Start_Time__c = sDTO.getsTime();
        }else{
            s.Start_Time__c = '';
        }
        
        if(sDTO.getEndDate() != '' && sDTO.getEndDate() != null && sDTO.getEndDate() != 'Invalid date'){    
            s.End_Date__c = Date.valueOf(sDTO.getEndDate());
        }else{
            s.End_Date__c = null;   
        }
        
        if(sDTO.geteTime() != '' && sDTO.geteTime() != null && sDTO.geteTime() != 'Invalid date'){  
            s.End_Time__c = sDTO.geteTime();
        }else{
            s.End_Time__c = '';
        }
        
        if(sDTO.getsRequiredPositionNumber() != '' && sDTO.getsRequiredPositionNumber() != null){
            s.No_of_Position_Requried__c = Integer.valueOf(sDTO.getsRequiredPositionNumber());
        }else{
            s.No_of_Position_Requried__c = null;
        }
        
        if(sDTO.getsVacId() != '' && sDTO.getsVacId() != null){
            s.Vacancy__c = sDTO.getsVacId();
        }
        
        if(sDTO.getsCompanyId() != '' && sDTO.getsCompanyId() != null){
            s.Account__c = sDTO.getsCompanyId();
        }else{
            s.Account__c = null;
        }
        
        if(sDTO.getsRecurEndDate() != '' && sDTO.getsRecurEndDate() != null && sDTO.getsWeeklyRecur() == true){
            s.Recurrence_End_Date__c = Date.valueOf(sDTO.getsRecurEndDate());
        }else{  
            s.Recurrence_End_Date__c = null;
        }
        
        if(sDTO.getsId() != null && sDTO.getsId() != ''){
            s.Id = sDTO.getsId();
        }
        
        if(sDTO.getsShiftStatus() != null && sDTO.getsShiftStatus() != ''){
            s.Shift_Status__c = sDTO.getsShiftStatus();
        }else{
            s.Shift_Status__c = null;
        }
        
        if(sDTO.getsRate() != null && sDTO.getsRate() != ''){
            s.Rate__c = Decimal.valueOf(sDTO.getsRate());
        }else{
            s.Rate__c = null;
        }	
    }
    
    /**
    * convert shift DTO to shift sObject for Shift DB
    * In this method, endDate will be Recurrence_End_Date__c
    *
    **/
    public static void convertShiftDTOToShiftSObjectForShiftDB(Shift__c s, ShiftDTO sDBDTO){
        s.Location__c = sDBDTO.getsLocation();
        s.Shift_Type__c = sDBDTO.getsType();
        s.Recurrence_Weekdays__c = sDBDTO.getsWeekdaysDisplay();   
        if(sDBDTO.getStartDate() != '' && sDBDTO.getStartDate() != null && sDBDTO.getStartDate() != 'Invalid date'){    
            s.Start_Date__c = Date.valueOf(sDBDTO.getStartDate());
        }else{
            s.Start_Date__c = null; 
        }
        if(sDBDTO.getsTime() != '' && sDBDTO.getsTime() != null && sDBDTO.getsTime() != 'Invalid date'){
            s.Start_Time__c = sDBDTO.getsTime();
        }else{
            s.Start_Time__c = '';
        }
        if(sDBDTO.getEndDate() != '' && sDBDTO.getEndDate() != null && sDBDTO.getEndDate() != 'Invalid date'){
            s.Recurrence_End_Date__c = Date.valueOf(sDBDTO.getEndDate());
        }else{
            s.Recurrence_End_Date__c = null;    
        }
        if(sDBDTO.geteTime() != '' && sDBDTO.geteTime() != null && sDBDTO.geteTime() != 'Invalid date'){    
            s.End_Time__c = sDBDTO.geteTime();
        }else{
            s.End_Time__c = '';
        }
        if(sDBDTO.getsRequiredPositionNumber() != '' && sDBDTO.getsRequiredPositionNumber() != null){
            s.No_of_Position_Requried__c = Integer.valueOf(sDBDTO.getsRequiredPositionNumber());
        }else{
            s.No_of_Position_Requried__c = null;
        }
        if(sDBDTO.getsVacId() != '' && sDBDTO.getsVacId() != null){
            s.Vacancy__c = sDBDTO.getsVacId();
        }
        if(sDBDTO.getsCompanyId() != '' && sDBDTO.getsCompanyId() != null){
            s.Account__c = sDBDTO.getsCompanyId();
        }else{
            s.Account__c = null;
        }
        if(sDBDTO.getsId() != null && sDBDTO.getsId() != ''){
            s.Id = sDBDTO.getsId();
        }
        if(sDBDTO.getsShiftStatus() != null && sDBDTO.getsShiftStatus() != ''){
            s.Shift_Status__c = sDBDTO.getsShiftStatus();
        }else{
            s.Shift_Status__c = null;
        }
		if(sDBDTO.getsRate() != null && sDBDTO.getsRate() != ''){
            s.Rate__c = Decimal.valueOf(sDBDTO.getsRate());
        }else{
            s.Rate__c = null;
        }	
    }
    
    /**
    * assign value to shift 
    *
    **/
    public static void assignValuesToShift(Shift__c sglShift, Shift__c inputShift){
        sglShift.Start_Time__c = inputShift.Start_Time__c;
        sglShift.End_Time__c = inputShift.End_Time__c;
        sglShift.No_of_Position_Requried__c = inputShift.No_of_Position_Requried__c;
        sglShift.No_of_Position_Placed__c = inputShift.No_of_Position_Placed__c;
        sglShift.Location__c = inputShift.Location__c;
        sglShift.Vacancy__c = inputShift.Vacancy__c;
        sglShift.Account__c = inputShift.Account__c;
        sglShift.Shift_Type__c = inputShift.Shift_Type__c;
        sglShift.Weekly_Recurrence__c = inputShift.Weekly_Recurrence__c;
        sglShift.Recurrence_End_Date__c = inputShift.Recurrence_End_Date__c;
        sglShift.Recurrence_Weekdays__c = inputShift.Recurrence_Weekdays__c;
        sglShift.Id = inputShift.Id;
        sglShift.Shift_Status__c = inputShift.Shift_Status__c;
        sglShift.Vacancy_Name_For_Display_On_Wizard__c = inputShift.Vacancy__r.Name;
        sglShift.Rate__c = inputShift.Rate__c;
    }
    
    /**
    * treat each day as a single shift, if two days are adjacent, merge these two shifts
    * 
    **/
    public static void mergeShift(List<Shift__c> shiftList, Shift__c sglShift){
        
        Shift__c lastShift = new Shift__c();
        
        Date sglShiftStartDate = sglShift.Start_Date__c;
        System.debug('sglShiftStartDate: '+sglShiftStartDate);
        Date sglShiftEndDate = sglShift.End_Date__c;
        
        if(shiftList.size() == 0) {
            shiftList.add(sglShift);
        }else{
            lastShift = shiftList[shiftList.size() - 1 ];
            
            Date lastShiftEndDate = lastShift.End_Date__c;
            System.debug('lastShiftEndDate: '+lastShiftEndDate);
            System.debug('days between: '+sglShiftStartDate.daysBetween(lastShiftEndDate));
            
            if(sglShiftStartDate.daysBetween(lastShiftEndDate) == -1) {
                lastShift.End_Date__c = sglShift.End_Date__c; 
            }else{
                /*
                 If lastEvent.end - sglDayEvent.start == 0, that means a duplicate
                 event is inserted, the duplicated event is same as previously inserted event.
                 Duplicate events exist when users choose dates
                 shown on both calendars. This is possible as left calendar may show
                 first several days of next month.
                 */
                shiftList.add(sglShift);
            }
        }
    }
    
    /**
    * In client roster, open shifts need to be shown to users to notice which vacancy is open.
    * Open vacancies is actually Shifts With Open Status. For UI display, we need to group shifts
    * for same vacancy
    *
    **/
    public static Map<String,List<Shift__c>> groupShiftsOfSameVacancy(List<Shift__c> inputShiftList){
        Map<String,List<Shift__c>> vacancyShiftsMap = new Map<String,List<Shift__c>>();
        
        for(Shift__c sglShift : inputShiftList){
            String key = sglShift.Vacancy__c;
            if(vacancyShiftsMap.containsKey(key)){
                List<Shift__c> values = vacancyShiftsMap.get(key);
                values.add(sglShift);
                vacancyShiftsMap.put(key,values);
            }else{
                List<Shift__c> shiftList = new List<Shift__c>();
                shiftList.add(sglShift);
                vacancyShiftsMap.put(key,shiftList);
            }
        }
        
        return vacancyShiftsMap;
    }
    
     /**
    * groupShiftsOfSameVacancy function return a map of (vacanyId, List<Shift__c>)
    * in the wizard, we need to display vacancyName. 
    * groupVacancyIdNameFromShifts function return a map of (vacancyId, ShiftDTO)
    * ShiftDTO is used to contain the vacancyName
    **/
    public static Map<String,ShiftDTO> groupVacancyIdNameFromShifts(List<Shift__c> inputShiftList){
        Map<String,ShiftDTO> vacancyIdNameMap = new Map<String,ShiftDTO>();
        
        for(Shift__c sglShift : inputShiftList){
            String key = sglShift.Vacancy__c;
            ShiftDTO sDTO = new ShiftDTO();
            sDTO.setsVacancyName(sglShift.Vacancy__r.Name);
            if(!vacancyIdNameMap.containsKey(key)){
                vacancyIdNameMap.put(key,sDTO);
            }
        }
        return vacancyIdNameMap;
    }
    
    /**
    * convert sObject to ShiftDTO
    *
    **/
    public static ShiftDTO convertShiftSObjectToShiftDTO(Shift__c inputShift){
        ShiftDTO sDTO = new ShiftDTO();
        sDTO.setStartDate(String.valueOf(inputShift.Start_Date__c));
        sDTO.setEndDate(String.valueOf(inputShift.End_Date__c));
        sDTO.setsRequiredPositionNumber(String.valueOf(inputShift.No_of_Position_Requried__c));
        sDTO.setsPlacedPositionNumber(String.valueOf(inputShift.No_of_Position_Placed__c));
        Decimal noOfPositionOpen;
        if(inputShift.No_of_Position_Placed__c == null){
            noOfPositionOpen = inputShift.No_of_Position_Requried__c;
        }else{
            noOfPositionOpen = inputShift.No_of_Position_Requried__c - inputShift.No_of_Position_Placed__c;
        }
        sDTO.setsOpenPositionNumber(String.valueOf(noOfPositionOpen));
        if(inputShift.Vacancy_Name_For_Display_On_Wizard__c == ''){
            sDTO.setsVacancyName(inputShift.Vacancy__r.Name);
        }else{
            sDTO.setsVacancyName(inputShift.Vacancy_Name_For_Display_On_Wizard__c);
        }
        sDTO.setsId(inputShift.Id);
        sDTO.setsVacId(inputShift.Vacancy__c);
        sDTO.setsCompanyId(inputShift.Account__c);
        sDTO.setsShiftStatus(inputShift.Shift_Status__c);
        sDTO.setsRecurEndDate(String.valueOf(inputShift.Recurrence_End_Date__c));
        sDTO.setsWeekdaysDisplay(inputShift.Recurrence_Weekdays__c);
        sDTO.setsType(inputShift.Shift_Type__c);
        sDTO.setsTime(inputShift.Start_Time__c);
        sDTO.seteTime(inputShift.End_Time__c);
        sDTO.setsLocation(inputShift.Location__c);
        sDTO.setsRate(String.valueOf(inputShift.Rate__c));
        return sDTO;
    }
    
    /**
    * In the Shift update wizard, End Date is Recurrence End Date, if Recurrence End Date is early
    * than End Date, the End Date need to be updated to Recurrence End Date. Otherwise, End Date
    * keep the same value.
    *
    **/
    public static List<Shift__c> updateShiftEndDate(List<Shift__c> shiftList){  
            List<Shift__c> shiftsToReturn = new List<Shift__c>();
            
            for(Shift__c s : shiftList){
                if(s.Recurrence_End_Date__c == null && s.Start_Date__c == null){
                    return shiftList;
                }
                
                Date endDate = WizardUtils.getEndDateByStartDateAndWeekdays(s.Start_Date__c);
                Integer endDateRecurEndDateDiff  = WizardUtils.getDiffBetweenTwoDates(endDate, s.Recurrence_End_Date__c);
            
                if(endDateRecurEndDateDiff < 0){
                    s.End_Date__c = s.Recurrence_End_Date__c;
                }else{
                    s.End_Date__c = endDate;
                }
                
                shiftsToReturn.add(s);      
            }
            
            return shiftsToReturn;
            
    }
    
    /**
    *   When an roster change the status from any status to filled or from filled to any status,
    *   need to get a map using the shiftId as key and list of Roster as value
    *   
    *   convert previous map to a map using the shiftId as key and number of Roster as value to update
    *   No_of_Position_Placed__c value
    **/
    public static Map<String,Integer> getShiftIdAndRelatedPlacedRosterNumberMap(List<String> shiftIdList){
        RosterSelector rSelector = new RosterSelector();
        List<Days_Unavailable__c> rosterList = rSelector.fetchFilledRosterListByShiftIdList(shiftIdList);
        Map<String,List<Days_Unavailable__c>> shiftIdRosterMap = new Map<String,List<Days_Unavailable__c>>();
        
        for(Days_Unavailable__c roster : rosterList){
            if(shiftIdRosterMap.containsKey(roster.Shift__c)){
                List<Days_Unavailable__c> value = shiftIdRosterMap.get(roster.Shift__c);
                value.add(roster);
                shiftIdRosterMap.put(roster.Shift__c, value);
            }else{
                List<Days_Unavailable__c> value = new List<Days_Unavailable__c>();
                value.add(roster); 
                shiftIdRosterMap.put(roster.Shift__c,value);
            }
        }
        
        Map<String,Integer> shiftIdPlacedRosterNumberMap = new Map<String,Integer>();
        
        for(String shiftId : shiftIdRosterMap.keySet()){
            Integer value = shiftIdRosterMap.get(shiftId).size();
            shiftIdPlacedRosterNumberMap.put(shiftId,value);
        }
        
        return shiftIdPlacedRosterNumberMap;    
    }
    
    /**
    * Under shift, when one of the fields (Start_Date__c, End_Date__c, Start_Time__c, End_Time__c)
    * (Recurrence_End_Date__c, Weekly_Recurrence__c, Recurrence_Weekdays__c, Shift_Type__c) change value
    *
    * need to update related roster to keep consistency.
    **/
    public static Boolean checkIsFieldValueChange(Shift__c oldShift, Shift__c newShift){
        List<String> fieldNameList = new List<String>();
        fieldNameList.add('Start_Date__c');
        fieldNameList.add('End_Date__c');
        fieldNameList.add('Start_Time__c');
        fieldNameList.add('End_Time__c');
        fieldNameList.add('Recurrence_End_Date__c');
        fieldNameList.add('Weekly_Recurrence__c');
        fieldNameList.add('Recurrence_Weekdays__c');
        fieldNameList.add('Shift_Type__c');
        fieldNameList.add('Rate__c');
        
        Boolean isChanged = false;
        
        for(String fieldName : fieldNameList){
            if(oldShift.get(fieldName) != newShift.get(fieldName)){
                isChanged = true;
                break;
            }
        }
        
        return isChanged;
    }
    
    
}