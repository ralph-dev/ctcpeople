/**
* This is a service class that include services related to RecordType object
* Author : Emily Jiang
* Date : 22/06/2015
**/
public with sharing class RecordTypeServices {
	private String objectName;
	
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	/**
	* Method to get record type map, using developername as key
	**/
	public Map<String, String> getRecordTypeMap() {
		RecordTypeSelector rtSelector = new RecordTypeSelector();
		List<RecordType> rtList = rtSelector.fetchRecordTypeList();
		
		if(rtList == null || rtList.size() == 0){
			return null;
		}
		
		Map<String, String> rtMap = new Map<String,String>();
		
		for(RecordType rt : rtList){
			rtMap.put(rt.developerName, rt.id);
		}
		
		return rtMap;
	}

	public List<RecordType> getRecordTypeList() {
		List<RecordType> recordTypeList = new List<RecordType>();
        RecordTypeSelector rTypeSelector = new RecordTypeSelector();
        recordTypeList = rTypeSelector.fetchRecordTypesByObjectName(objectname);
        return recordTypeList;
	}

}