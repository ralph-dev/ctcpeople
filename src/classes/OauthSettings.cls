public with sharing class OauthSettings {
    // URLs used by the application - change to test.salesforce.com for
    private static String host='';
    // sandboxes
    private static String getHOST(){ 
      if(host==''){
        host= 'https://login.salesforce.com';
        if(isSandbox()){
          host= 'https://test.salesforce.com';
        }
      }
      return host;
    }
    public static boolean isSandbox(){
    	CommonSelector.checkRead(Organization.sObjectType, 'Id, IsSandbox');
      return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    // URLs used by the application
    public static String URL_CALLBACK = getHOST() + '/apex/OAuthRefreshToken';
    public static String URL_API_LOGIN = getHOST() + '';
    public static String URL_AUTHORIZATION = getHOST() + '/services/oauth2/authorize';
    public static String URL_REFRESH_TOKEN = getHOST() + '/services/oauth2/token';
    public static String URL_REQUEST_TOKEN = getHOST() + '/_nc_external/system/security/oauth/RequestTokenHandler';
    public static String URL_ACCESS_TOKEN = getHOST() + '/_nc_external/system/security/oauth/AccessTokenHandler';
}