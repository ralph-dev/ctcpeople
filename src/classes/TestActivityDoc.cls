@isTest
public with sharing class TestActivityDoc {
    
    public static testmethod void TestActivityDocOperator() {
    	DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
    	ActivityDummyRecordCreator taskCreator = new ActivityDummyRecordCreator();
        Task task =  taskCreator.generateOneActivityDummyRecord(rs);
    	system.runAs(DummyRecordCreator.platformUser) {
        
        
        Boolean result = ActivityDocOperator.updateActivityStatus('error','server error',task.id);
        system.assertEquals(result,true);
    	}
    }

    public static testmethod void TestActivityDocSelector() {
    	DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
	 	ActivityDummyRecordCreator taskCreator = new ActivityDummyRecordCreator();
        Task task =  taskCreator.generateOneActivityDummyRecord(rs);
        Attachment att = AttachmentDummyRecordCreator.createAttachmentForActivity('Test_attachment.txt', 'Test attachment', task );
    
    	System.runAs(DummyRecordCreator.platformUser) {
	        
	       
            Task result = ActivityDocSelector.GetTaskById(task.id);
            system.assert(result != null);
            List<SelectOption> selectOptionList = ActivityDocSelector.GetResumeFileSelectOption();
            system.assert(selectOptionList.size()>0);
            List<SelectOption> skillGroupSelectOption = ActivityDocSelector.GetSelectSkillGroupList();
            system.assert(skillGroupSelectOption != null);
        }
    }
    
    // public static testmethod void TestActivityDocController() {
    //     User u = DataTestFactory.createUser();
    //     u.profileId = Userinfo.getProfileId();
    //     u.AmazonS3_Folder__c= 'test';
    //     insert u;
    //     DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
    //     ActivityDummyRecordCreator taskCreator = new ActivityDummyRecordCreator();
    //     Task task =  taskCreator.generateOneActivityDummyRecord(rs);
    //     System.runAs(u) {
    //         ApexPages.StandardController std_noAttachment = new ApexPages.StandardController(task);
    //         ActivityDocController activityDocController_noAttachment = new ActivityDocController(std_noAttachment);
    //         system.assertEquals(activityDocController_noAttachment.activityDocs.size(), 0);
            
    //         Attachment att = AttachmentDummyRecordCreator.createAttachmentForActivity('Test_attachment.txt', 'Test attachment', task );
    //         ApexPages.StandardController std = new ApexPages.StandardController(task);
    //         ActivityDocController activityDocController = new ActivityDocController(std);
    //         system.assert(activityDocController.isvalid);
    //         PageReference page = activityDocController.uploadDocumentServices();
    //         system.assert(page == null);
    //         String fieldApiName=activityDocController.getLookUpFieldNameViaObjectName('Contact');
    //         system.assertEquals(fieldApiName,PeopleCloudHelper.getPackageNamespace()+'Document_Related_To__c');
            
    //         ContentDocumentLink cdl = FileDummyRecordCreator.createFileForActivity(task.Id, 'resume', 'resume.doc', 'test resume content');
    //         ApexPages.StandardController std2 = new ApexPages.StandardController(task);
    //         ActivityDocController activityDocController_withFileAndAtt = new ActivityDocController(std2);
    //         system.assert(activityDocController.isvalid);
            
    //     }
    // }
    
    public static testmethod void TestActivityDocControllerWithoutS3() {
        User u = DataTestFactory.createUser();
        u.profileId = Userinfo.getProfileId();
        u.AmazonS3_Folder__c= '';
        CommonSelector.quickInsert( u);
        DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
        ActivityDummyRecordCreator taskCreator = new ActivityDummyRecordCreator();
        Task task =  taskCreator.generateOneActivityDummyRecord(rs);
        
        System.runAs(u) {
        	
	        
            ApexPages.StandardController std_noAttachment = new ApexPages.StandardController(task);
            ActivityDocController activityDocController_noAttachment = new ActivityDocController(std_noAttachment);
            system.assertEquals(activityDocController_noAttachment.activityDocs.size(), 0);
            system.assert(!activityDocController_noAttachment.isvalid);
        }
    }
    
    public static testmethod void testCustomerErrorMessage() {
        
        System.runAs(DummyRecordCreator.platformUser) {
            PeopleCloudCustomErrorInfo error = ActivityDocControllerHelper.customerErrorMessage(215, true);
            system.assert(error.successStatus);
        }
    }
    
    public static testmethod void testIsValidCloudStorageAccount() {
        
        System.runAs(DummyRecordCreator.platformUser) {
            Boolean isValid = ActivityDocControllerHelper.isValidCloudStorageAccount();
            system.assert(isValid);
        }
    }
    
    public static testmethod void testGetLookUpFieldNameViaObjectName() {
        
        System.runAs(DummyRecordCreator.platformUser) {
            String objectName = ActivityDocControllerHelper.getLookUpFieldNameViaObjectName('Contact');
            system.assertEquals(objectName, PeopleCloudHelper.getPackageNamespace()+'Document_Related_To__c');
        }
    }
        
}