@isTest
private class IndeedPostingControllerTest {

	private static testMethod void test() {
		system.runAs(DummyRecordCreator.platformUser) {
		    Advertisement_Configuration__c tempIndeedAccount  = AdConfigDummyRecordCreator.createDummyIndeedAccount();
	        User userRec=[select id,Indeed_Account__c from User where id =: userinfo.getUserId()];
	        userRec.Indeed_Account__c=tempIndeedAccount.Id;
		    update userRec;
		    
		    Advertisement__c testad = PeopleAdMapperTest.createTestAd();
			Test.setCurrentPage(Page.IndeedPost); 
			//ApexPages.currentpage().getParameters().put('id','test');
			ApexPages.Standardcontroller testStdCon = new ApexPages.Standardcontroller(testad);
			IndeedPostingController testIndeedPost = new IndeedPostingController(testStdCon);
			
			PageReference testsavead = testIndeedPost.saveAd();
			system.assert(testsavead == null);
			PageReference testupdatead = testIndeedPost.updateAd();
			system.assert(testupdatead == null);
			testIndeedPost.initSelectOption();
			system.assert(testIndeedPost.selectSkillGroupList != null);
			PageReference testpostad = testIndeedPost.postad();
			system.assert(testpostad == null);
			PageReference testerrormsg = testIndeedPost.customerErrorMessage();
			system.assert(testerrormsg == null);
		}
	}

}