/**
 * Email service are automated process that use Apex class
 * to process the contents, headers, and attachments of inbound
 * email.
 * This class is designed to handle email with resume from different senders and automatically creates 
 * Attachment object in SF and call webservice to parse resume and save details to candidate object in SF
 * 
 * Author : Andy Yang
 */
global class CTCEmailToCandidate implements Messaging.InboundEmailHandler {
	

	//Email Service entry method
    global Messaging.InboundEmailResult handleInboundEmail(
    	Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        
        System.debug('#CTCEmailToCandidate#');
        System.debug('#email.fromAddress  #'+email.fromAddress );
        System.debug('#email.subject#'+email.subject);
        //System.debug('#email.plainTextBody #'+email.plainTextBody); 
        
        if(email.binaryAttachments != null && email.binaryAttachments.size() > 0){
        	System.debug('#email.attachment[0] #' + email.binaryAttachments[0].filename);
        }else{
        	System.debug('NO ATTACHMENT !!! IGNORE ! #' );
        }
        
        
        //default return result
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        result.success = false;
        try {
        	
        	//parse email and do corresponding process, such making attachment object and calling webservice
            invokeEmailHandler(email);  
            result.success = true;
        } catch(Exception ee) {
            System.debug('---exception in CTCEmailToCandidate----' + ee);
            System.debug(ee.getStackTraceString());
 
      		result.message = 'CTCEmailToCandidate Error: \n' + ee.getStackTraceString();   
           
        }
        
        return result;
    }  
    
    /*
    * Parse the content of email and decide what action to do according to the content of email
    * 
    */
    private void invokeEmailHandler(Messaging.InboundEmail email) {
    	
        //create a corresponding IEmailHandler object, such as VacancyMaker object
        ICVEmailHandler handler = CVEmailHandlerFactory.build( email );
        
        if(handler != null ){
        	//execute handle processing
        	handler.handle();
        }
    }
}