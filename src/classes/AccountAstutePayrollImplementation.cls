public with sharing class AccountAstutePayrollImplementation {
	public static void syncClientBiller(list<String> accountIds, String session){
		AccountServices accServices=new AccountServices();
		list<Account> accountRecords=accServices.retrieveAccountsById(accountIds);
		//If creation failed, just return without the following function
		if(accountRecords.size()==0){
			return;
		}
		String billerMessage=convertToBillerJson(accountRecords);
		String endpointAstute = Endpoint.getAstuteEndpoint();
        endpointAstute=endpointAstute+'/AstutePayroll/biller';
		AstutePayrollFutureJobs.pushHandler(billerMessage,endpointAstute,session);
	}
	
	public static String convertToBillerJson(list<Account> accounts){
		
		AccountServices accountServ=new AccountServices();
		list<map<String,Object>> objSTR=accountServ.convertAccountToBiller(accounts);
		map<String,Object> messagesJson=new map<String,Object>();
		messagesJson.put('billerSaves',objSTR);
		String messages=JSON.serialize(messagesJson);
		return messages;
	}
	
	/***
		This will happend before the update of the account
		(The update is fired by a workflow)
		Fire a future job in AstutePayroll FutureJobs to update Accounts
		Fixing data always have high priority  
		@param list<Account>
					--Alvin		
	****/
	public static void astutePayrollTrigger(list<Account> newRecords){
		CTCPeopleSettingHelperServices ctcPeopleSettingHelperServ=new CTCPeopleSettingHelperServices();
		if(!ctcPeopleSettingHelperServ.hasApAccount()){
    		return;
    	}
    	list<String> accountIds=new list<String>();
    	list<String> accountIdsNeedFix=new list<String>();
    	for(Account perAccount: newRecords){
    		if(perAccount.Is_Fixing_Data_For_AP__c){
    			perAccount.Is_Fixing_Data_For_AP__c=false;
    			perAccount.Astute_Payroll_Upload_Status__c ='On Hold' ;
    			accountIdsNeedFix.add(perAccount.Id);
    			continue;
    		}
    		
    		if(perAccount.Is_Pushing_To_Astute_Payroll__c){
    			perAccount.Is_Pushing_To_Astute_Payroll__c=false;
    			accountIds.add(perAccount.Id);
    			perAccount.Astute_Payroll_Upload_Status__c ='On Hold' ;
    		}
    	}
    	if(accountIds.size()>0){
    		AstutePayrollFutureJobs.syncClientBillerFuture(accountIds,AstutePayrollUtils.GetloginSession());
    	}
    	if(accountIdsNeedFix.size()>0){
    		PlacementServices placementServ=new PlacementServices();
            placementServ.fixBiller(accountIdsNeedFix);
            InvoiceItemServices invoiceItemServ=new InvoiceItemServices();
            invoiceItemServ.fixBiller(accountIdsNeedFix);
    	}  	
	}
	

}