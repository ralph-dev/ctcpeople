public with sharing class VacancySearchCriteria {
	private String vacancyEntered;
	private String companyId;
	public String getVacancyEntered(){
		return vacancyEntered;
	}
	public void setVacancyEntered(String vacancyEntered){
		this.vacancyEntered = vacancyEntered;
	} 
	public String getCompanyId(){
		return companyId;
	}
	public void setCompanyId(String companyId){
		this.companyId = companyId;
	} 	
}