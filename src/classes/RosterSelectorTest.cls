@isTest
private class RosterSelectorTest {
	final static String ROSTER_STATUS_PENDING = 'Pending';
	
    static testMethod void fetchRosterListByCandidateIdTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // TO DO: implement unit test
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        Days_Unavailable__c roster = TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_PENDING);
        
        RosterSelector rSelector = new RosterSelector();
        
        //test method rosterSelector fetchRosterListByCandidateId
        List<Days_Unavailable__c> rosterListByCanId = rSelector.fetchRosterListByCandidateId(con.Id);
        System.assertNotEquals(rosterListByCanId, null); 
        
		//test method rosterSelector fetchRosterListByCandidateIdAndStatus
        List<Days_Unavailable__c> rosterListPendingByCanId = rSelector.fetchRosterListByCandidateIdAndStatus(con.Id,ROSTER_STATUS_PENDING);
        System.assertNotEquals(rosterListPendingByCanId, null);
        
        //test method rosterSelector fetchRosterListByClientId
        List<Days_Unavailable__c> rosterListByAccId = rSelector.fetchRosterListByClientId(acc.Id);
        System.assertNotEquals(rosterListByAccId, null);
        
       	//test method rosterSelector fetchRosterListByClientIdAndStatus
        List<Days_Unavailable__c> rosterListPendingByAccId = rSelector.fetchRosterListByClientIdAndStatus(acc.Id,ROSTER_STATUS_PENDING);
        System.assertNotEquals(rosterListPendingByAccId, null);
        
        //test method rosterSelector fetchRosterListByRosterId
        List<Days_Unavailable__c> rosterListByRosterId = rSelector.fetchRosterListByRosterId(roster.Id);
        System.assertNotEquals(rosterListByRosterId, null);
        
        //test method rosterSelector fetchRosterListByRosterIdList
        List<String> rosterIdList = new List<String>();
        rosterIdList.add(roster.Id);
        List<Days_Unavailable__c> rosterListByRosterIdList = rSelector.fetchRosterListByRosterIdList(rosterIdList);
        System.assertNotEquals(rosterListByRosterIdList, null);
        
        //test method rosterSelector fetchUnavailableCandidateList
        List<Days_Unavailable__c> unavailCanListByCanId = rSelector.fetchUnavailableCandidateList(con.Id);
        System.assertEquals(unavailCanListByCanId.size(), 0);
        
        //test method rosterSelector fetchRosterListByShiftId
        List<Days_Unavailable__c> rosterListByShiftId = rSelector.fetchRosterListByShiftId(shift.Id); 
        System.assertNotEquals(rosterListByShiftId,null);
        
        //test method rosterSelector 
        List<String> shiftIdList = new List<String>();
        shiftIdList.add(shift.Id);
        List<Days_Unavailable__c> rosterListByShiftIdList = rSelector.fetchFilledRosterListByShiftIdList(shiftIdList); 
        System.assertNotEquals(rosterListByShiftIdList,null);
    	}
    }
}