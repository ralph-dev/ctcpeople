public with sharing class WorkplaceServices {
	/***
		Convert workplace Info to Biller workplace.
					---Alvin
	
	***/
	public list<map<String,Object>> convertWorkplaceToBillerWorkplace(list<Workplace__c> workplaces){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> workplaceMap
				=ctcPeopleHelperService.getWorkplaceMap();
		list<map<String,Object>> workplaceInfos=new list<map<String,Object>>();	
		for(Workplace__c record : workplaces){
			workplaceInfos.add(ctcPeopleHelperService.convertToAPFormat(record, workplaceMap)); 
		}
		return workplaceInfos;
	}
	/**
		Convert workplace info into Biller workplace map
	
	*****/
	public map<String,Object> convertWorkplaceToBillerWorkplaceMap(list<Workplace__c> workplaces){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> workplaceMap
				=ctcPeopleHelperService.getWorkplaceMap();
		map<String,Object> workplaceInfos=new map<String,Object>();	
		for(Workplace__c record : workplaces){
			workplaceInfos.put(record.Id,ctcPeopleHelperService.convertToAPFormat(record, workplaceMap)); 
		}
		return workplaceInfos;
	}
	/**
		Retrieve workplace info from workplace ids.
	****/
	public list<Workplace__c> retrieveWorkplaceByIds(list<String> workplaceIds){
		list<Workplace__c> workplaces=new list<Workplace__c>();
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		String queryStr=ctcPeopleHelperService.formQueryFromMap(ctcPeopleHelperService.getWorkplaceMap());
		CommonSelector.checkRead(Workplace__c.SObjectType,queryStr);
		queryStr='Select '+queryStr+' from Workplace__c where id in : workplaceIds';
		workplaces=database.query(queryStr);
		return workplaces;
	}

}