public with sharing class TaskSelector extends CommonSelector{
	
	//private DynamicSelector dynamicSel;

	public TaskSelector() {	
		super(Task.SObjectType);
		//dynamicSel = new DynamicSelector(Task.SObjectType.getDescribe().getName(), false, true, true);	
	}

	

	public override LIST<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField>{
			Task.Id,
			Task.OwnerId,
			Task.Subject,
			Task.Type,
			Task.ActivityDate,
			Task.Status,
			Task.Priority,
			Task.Description,
			Task.CreatedDate
		};
	}

	/**
	*@param whoId = 'whoId'
	*@return List<Task>
	**/
	public List<Task> getTop5TaksByContactId(String contactId) {
	    SimpleQueryFactory qf = simpleQuery();
	    String condition = '';
	    qf.selectFields(getSObjectFieldList());
	    qf.selectFields(new List<String>{'Owner.Name'});
		qf.setLimit(5);
		if(String.isNotBlank(contactId)) {
			condition += 'whoId =: contactId';
		}
		qf.setCondition(condition);
		qf.getOrderings().clear();
		qf.addOrdering('CreatedDate', fflib_QueryFactory.SortOrder.DESCENDING);
		String soqlStr = qf.toSOQL();
		//System.debug('soqlStr='+soqlStr);

		List<Task> tasks = Database.query(soqlStr);
		return tasks;
	}

	private List<Schema.SObjectField> getFieldListForLastTask(){
		return new List<Schema.SObjectField>{
			Task.Id,
			Task.Subject,
			Task.ActivityDate,
			Task.WhoId,
			Task.LastModifiedDate,
			Task.Status
		};
	}

	public List<Task> getLastModifiedTaskForContact(List<Id> conIds){
		SimpleQueryFactory qf = simpleQuery();
		String queryCondition = '%left message%';
		String statusCon = 'Completed';
		String condition = '';
		qf.selectFields(getFieldListForLastTask());
		if(!conIds.isEmpty()){
			condition+= ' Who.id in :conIds And Status =:statusCon And (Not Subject like :queryCondition)';
		}
		qf.setCondition(condition);
		qf.getOrderings().clear();
		qf.addOrdering('ActivityDate', fflib_QueryFactory.SortOrder.DESCENDING);
		String soqlStr = qf.toSOQL();

		List<Task> result = Database.query(soqlStr);
		return result;
	}
	
	
	/**
	*@param taskId
	*@return task
	**/
	public Task getTaskById(String tId) {
		
		String soqlStr = 'SELECT Id, WhoId, WhatId, Who.type, What.type FROM TASK WHERE Id =: tId ORDER BY LastModifiedDate DESC';
		
		checkRead('Id, WhoId, WhatId');
		List<Task> tasks = Database.query(soqlStr);
		return tasks.get(0);
	}
}