/***********************************************************************************
/************************************Author by Jack*********************************
/*Get the DateRange Query for DateRangeSearch at ReadXMLFileController.cls**********
/***********************************************************************************/

public with sharing class AdvancedPeopleSearchDateRangeQuery {
	
	public static String getSecondQuery(Date startDate, Date endDate){
		String secondQueryString = '';
		if(String.valueof(startDate)!= null&& String.valueof(endDate) == null){
			secondQueryString =  'End_Date__c >= '+String.valueof(startDate);
		}
		
		else if(String.valueof(startDate) == null && String.valueof(endDate) != null){
			secondQueryString =  'Start_Date__c <= '+ String.valueof(endDate) ;
		}
		else if(String.valueof(startDate)!= null &&String.valueof(endDate) != null){
			secondQueryString = 'Start_Date__c <= '+ String.valueof(endDate) +' And End_Date__c >= '+ String.valueof(startDate) ;
		}
		
		return secondQueryString;
	}
	
	public static testmethod void testAdvancedPeopleSearchDateRangeQuery(){
		System.runAs(DummyRecordCreator.platformUser) {
			String secondquerystring = '';
			String secondquerystring1 = '';
			String secondquerystring2 = '';
			Date StartDate = null;
			Date EndDate = null;
			secondquerystring = AdvancedPeopleSearchDateRangeQuery.getSecondQuery(Date.valueof('2012-07-05'),Date.valueof('2012-07-19'));
			system.assertEquals('Start_Date__c <= 2012-07-19 And End_Date__c >= 2012-07-05',secondQueryString);
			secondquerystring1 = AdvancedPeopleSearchDateRangeQuery.getSecondQuery(Date.valueof('2012-07-05'),EndDate);
			system.assertEquals('End_Date__c >= 2012-07-05',secondQueryString1);
			secondquerystring2 = AdvancedPeopleSearchDateRangeQuery.getSecondQuery(StartDate,Date.valueof('2012-07-19'));
			system.assertEquals('Start_Date__c <= 2012-07-19',secondQueryString2);
		}
		
	}
}