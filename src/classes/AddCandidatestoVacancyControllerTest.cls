@isTest
public class AddCandidatestoVacancyControllerTest {
    private static testMethod void testAddCandidatestoVacancyController(){
    	System.runAs(DummyRecordCreator.platformUser) {
    		/*****Create the contact list and vacancies for the test.*********/
	       RecordType candRT = DaoRecordType.indCandidateRT;
	       List<Contact> conList = new List<Contact>(); 
	       for(Integer i=0;i<200;i++){
	            Contact cand = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1'+i,Email='Cand1'+i+'@testcanddispatcher.ctc.test');
	            conList.add(cand);
	        }
	       insert conList;
	        List<Placement__c> vacancies=new List<Placement__c>();
	       for(Integer i=0;i<200;i++){
	             Placement__c vacan = new Placement__c(Name='test'+i);
	            vacancies.add(vacan);
	        }
	        insert vacancies;
	       PageReference pageRef = ApexPages.currentPage();     
	       pageRef.getParameters().put('retURL','%2Fsearch%2FTagSearchResults%3FtIdList%3D0t090000000TRKB%26scopeMode%3Dall%26sort%3Dalphabetical%26rId%3DbrowseTags&wrapMassAction=1&scontrolCaching=1');
	       ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(conList);              
	       AddCandidatestoVacancyController addcon=new AddCandidatestoVacancyController(ssc);
	       
	       /***Successfully insert candidate management.*****/
	       addcon.jsoncontactstr='{';
	       addcon.jsonvacancystr='{\"'+vacancies[0].id+'\":true,\"'+vacancies[0].id+'\":true}';
	       for(Integer i=0;i<100;i++){
	            addcon.jsoncontactstr=addcon.jsoncontactstr+'\"'+conList[i].id+'\":true,';
	            
	            if(i==99){
	                addcon.jsoncontactstr=addcon.jsoncontactstr+'\"'+conList[i].id+'\":true}';
	            }
	            
	       }
	       
	       addcon.insertdata();
	       addcon.errormessageforNoContact();
	       addcon.errormessageforNoVacancy();
	       /********Test search function*****/
	       addcon.clauseforvacancy=' test';
	       addcon.onsearch();
	       /**********Wrong candidate in the candidate management.*****/
	       addcon.jsoncontactstr='{\"0039000000RGT\":true}';
	       addcon.insertdata();
	       
	       /**********Too many vacanciesError;*****/
	       addcon.jsoncontactstr='{';
	       addcon.jsonvacancystr='{';
	       for(Integer i=0;i<100;i++){
	            addcon.jsoncontactstr=addcon.jsoncontactstr+'\"'+conList[i].id+'\":true,';
	            addcon.jsonvacancystr=addcon.jsoncontactstr+'\"'+vacancies[i].id+'\":true,';
	            if(i==99){
	                addcon.jsoncontactstr=addcon.jsoncontactstr+'\"'+conList[i].id+'\":true}';
	                addcon.jsonvacancystr=addcon.jsoncontactstr+'\"'+vacancies[i].id+'\":true}';
	            }
	            
	       }
	       addcon.insertdata();
	       System.assertEquals(addcon.conts, null);     
	         
	        Temporary_Data__c tempDatas=new Temporary_Data__c();
	        tempDatas.name='testTempData';
	        tempDatas.Data_Content__c='["0039000000RGT"]';
	        insert tempDatas;
	        addcon.getIdList(tempDatas.id);
	        
	        try {
	            system.assertNotEquals(null, addcon.generatefieldsetmember('Contact', 'phone'));
	        } catch (Exception e) {
	            
	        }
	        
	        try {
	            system.assertNotEquals(null, addcon.returnFieldsMap('phone', 'Contact'));
	        } catch (Exception e) {
	            
	        }
    	}
       
    }
    
}