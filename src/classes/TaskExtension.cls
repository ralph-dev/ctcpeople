/**
*@author Jack ZHOU
*@createDate 07 Jun 2016
*Provide the RemmoteAction on Task Object 
**/

public with sharing class TaskExtension {
	/*
	*Constructor 
	*/
	public TaskExtension() {} 

	public TaskExtension(Object obj) {}
	
	/**
	*Get the OpenTask List, Completed Task List and Action Plan List
	**/
	@RemoteAction
	public static List<Task> getTop5TasksByContactId(String contactId) {
		TaskSelector tService = new TaskSelector();
		return new TaskSelector().getTop5TaksByContactId(contactId);
	}
	
	//Get list of enabled skill group
    @RemoteAction
    public static List<SkillGroups.selectedSkillGroup> getSkillGroups() {
        return SkillGroups.getSelectedSkillGroup();
    }
    
    //Get resume and files document type select option
    @RemoteAction
    public static List<SelectOption> getDocTypePickList() {
        return ActivityDocSelector.GetResumeFileSelectOption();
    }
    
    //Get task information including whoId, whoName, whatId and whatName
    @RemoteAction
    public static Task getTaskInfo(String taskId) {
        return ActivityDocSelector.getTaskById(taskId);
    }
    
    //Init activity docs including attachments and files
    @RemoteAction
    public static List<ActivityDoc> getActivityDocs(String taskId) {
        return ActivityDocControllerHelper.getActivityDocs(taskId);
    }
    
    //Upload files/attachments to resume and files and S3
    @RemoteAction
    public static PeopleCloudCustomErrorInfo uploadDocuments(List<ActivityDoc> activityDocs) {
        return ActivityDocControllerHelper.uploadDocuments(activityDocs);
    }

    /**
    *Create Task
    **/
    @RemoteAction
    public static List<Task> createRecords(List<Map<String, Object>> tasks) {
        TaskService tService = new TaskService();
        return tService.createRecords(tasks);
    }

    /**
    *Delete Task
    **/
    @RemoteAction
    public static List<Id> deleteRecords(List<Map<String, Object>> tasks) {
        TaskService tService = new TaskService();
        return tService.deleteTaskRecords(tasks);
    }
	
	@RemoteAction
    public static Map<String, RemoteAPIDescriptor.RemoteAction> apiDescriptor(){
    	RemoteAPIDescriptor descriptor = new RemoteAPIDescriptor();
    	descriptor.action('getTop5TasksByContactId').param(String.class);
    	descriptor.action('getSkillGroups');
    	descriptor.action('getDocTypePickList');
    	descriptor.action('getTaskInfo').param(String.class, 'Get task information including whoId, whoName, whatId and whatName');
    	descriptor.action('getActivityDocs').param(String.class, 'Get activity docs including attachments and files');
    	descriptor.action('uploadDocuments').param(List<ActivityDoc>.class, 'Upload files/attachments to resume and files');
        descriptor.action('createRecords').param(List<Map<String, Object>>.class);
        descriptor.action('deleteRecords').param(List<Map<String, Object>>.class);
        return descriptor.paramTypesMap;
    }
}