/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class UserServicesTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	list<User> users=new list<User>();
    	User userRec=new User();
    	userRec.firstName='firstName';
    	userRec.lastName='Test name';
    	userRec.Email='Test@example.com';
    	users.add(userRec);
    	UserServices userServ=new UserServices();
    	list<map<String,Object>> userMapList=userServ.convertUserToOwner(users);
    	System.assertEquals(userMapList.size(),1);
    	}
    	
    }

	static testMethod void testUpdateNumberOfRecordsShowing() {
		System.runAs(DummyRecordCreator.platformUser) {
			Integer numOfRecords = new UserServices().updateNumberOfRecordsShowing(20);
			System.assertEquals(20, numOfRecords);
			User u = new UserSelector().getCurrentUser();
			System.assertEquals(20, Integer.valueOf(u.Number_of_Records_Showing__c));
		}
	}

}