@isTest
private class DefaultSkillGroupControllerTest
{
	@isTest
	static void testDefaultSkillGroupController() {
		DummyRecordCreator.DefaultDummyRecordSet dms = DummyRecordCreator.createDefaultDummyRecordSet();

		PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
		pcSwitch.Enable_Daxtra_Skill_Parsing__c = true;
		upsert pcSwitch;

		DefaultSkillGroupController dSkillGroupController = new DefaultSkillGroupController();
		system.assert(dSkillGroupController.isSkillSearchEnable);
		system.assert(dSkillGroupController.isSkillGroupValid);
		system.assert(dSkillGroupController.skillGroupList.size()>0);
		system.assertEquals(dSkillGroupController.selectedSkillGroupList.size(),0);

		List<String> selectedSkillGroupList = new List<String>{'G001'};
		dSkillGroupController.selectedSkillGroupList = selectedSkillGroupList;
		dSkillGroupController.updateDefaultSkillGroup();

		List <Skill_Group__c> defaultSkillGroup = SkillGroups.getDefaultSkillGroups();
		system.assertEquals(defaultSkillGroup.size(),1);
	}
}