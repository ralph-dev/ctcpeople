/**
 * This the test class for LinkedInCSASelector
 * Created by: Lina
 * Created date: 08/04/2016
 * */

@isTest
public class LinkedInCSASelectorTest {
    
    static testmethod void unitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        LinkedIn_CSA__c csa = new LinkedIn_CSA__c();
        csa.API_Key__c = 'test';
        csa.Customer_Id__c = 'test';
        csa.Name = UserInfo.getOrganizationName();
        insert csa;
        
        LinkedIn_CSA__c csaSelected = LinkedInCSASelector.getLinkedInCSA();
        System.assertEquals('test', csaSelected.API_Key__c);
        System.assertEquals('test', csaSelected.Customer_Id__c);
    	}
    }
    
    static testmethod void testNull() {
    	System.runAs(DummyRecordCreator.platformUser) {
        LinkedIn_CSA__c csaSelected = LinkedInCSASelector.getLinkedInCSA();
        System.assertEquals(null, csaSelected);
    	}
    }
    
    static testmethod void testMultiCSA() {
    	System.runAs(DummyRecordCreator.platformUser) {
        LinkedIn_CSA__c csa = new LinkedIn_CSA__c();
        csa.API_Key__c = 'test';
        csa.Customer_Id__c = 'test';
        csa.Name = UserInfo.getOrganizationName();
        insert csa;
        
        LinkedIn_CSA__c csa2 = new LinkedIn_CSA__c();
        csa2.API_Key__c = 'test2';
        csa2.Customer_Id__c = 'test2';
        csa2.Name = UserInfo.getOrganizationName();
        insert csa2;
        
        LinkedIn_CSA__c csaSelected = LinkedInCSASelector.getLinkedInCSA();
        System.assertEquals('test', csaSelected.API_Key__c);
        System.assertEquals('test', csaSelected.Customer_Id__c);
    	}
    }
}