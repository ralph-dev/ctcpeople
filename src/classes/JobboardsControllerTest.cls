/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class JobboardsControllerTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
        if(pcSwitch==null){
	        pcSwitch=new PCAppSwitch__c();
        }
        Advertisement__c adv = DataTestFactory.createAdvertisement();
        Advertisement_Configuration__c tempIndeedAccount  = AdConfigDummyRecordCreator.createDummyIndeedAccount();
        User userRec=[select id,Indeed_Account__c,Monthly_Quota_Seek__c, Monthly_Quota_CareerOne__c,Monthly_Quota_MyCareer__c,
    	Seek_Usage__c, CareerOne_Usage__c,MyCareer_Usage__c
    	from User where id =: userinfo.getUserId()];
    	userRec.Monthly_Quota_Seek__c=200+'';
    	userRec.Monthly_Quota_CareerOne__c=200+'';
    	userRec.Monthly_Quota_MyCareer__c=200+'';
    	userRec.Seek_Usage__c=1+'';
    	userRec.CareerOne_Usage__c=1+'';
    	userRec.MyCareer_Usage__c=1+'';
    	update userRec;
		pcSwitch.Web_Site__c=true;
        pcSwitch.Placed_Candidate_Status__c = true;
        pcSwitch.DeduplicationOnVCW_Active__c=false;
        pcSwitch.Deduplicate_PC_Active__c = false;
        // Exclude Corporate Candidate from dedup
        upsert pcSwitch;
    	JobboardsController thecontroller = new JobboardsController();
    	thecontroller.quotaChecking();
    	thecontroller.addwarningmsg('test');
    	system.assertEquals(null, thecontroller.client_has_mycareer);

		thecontroller.client_has_seek=true;
		thecontroller.client_has_c1=true;
		thecontroller.client_has_trademe=true;
		thecontroller.client_has_JXT=true;
		thecontroller.client_has_JXT_NZ=true;
		thecontroller.client_has_linkedIn=true;
		thecontroller.client_has_Indeed=true;
		thecontroller.client_has_website=true;
		thecontroller.seek_quota_exceed=true;
		thecontroller.career1_quota_exceed=true;
		thecontroller.seek_selected=true;
		thecontroller.career1_selected=true;
		thecontroller.trademe_selected=true;
		thecontroller.jxt_selected=true;
		thecontroller.jxt_nz_selected=true;
		thecontroller.website_selected=true;
		thecontroller.linkedin_selected=true;
		thecontroller.Indeed_selected=true;
		Test.setCurrentPage(Page.Jobboards);
		ApexPages.currentPage().getParameters().put('aid', adv.Id);
	   	thecontroller.quotaChecking();
	   	thecontroller.gotoJobBoards();
	   	thecontroller.JobBoardsInit();
	   	thecontroller.adderrormsg('Testing');
	   	StyleCategory__c sc = DataTestFactory.createStyleCategory();
	   	//sc.Div_Html_File_ID__c = 'test';
	   	sc.Footer_File_ID__c = 'test';
	   	sc.Header_File_ID__c = 'test';
	   	update sc;
	   	ApexPages.currentPage().getParameters().put('id', sc.Id);
	   	JobboardsController.AppFormStyleController appController=new JobboardsController.AppFormStyleController();
	   	system.assertNotEquals(null, appController.headerid);
	   	system.assertNotEquals(null, appController.footerid);
	   	system.assertEquals(null, appController.divid);
	   	system.assertEquals(null, appController.divhtmlstr);
    	}
    }
}