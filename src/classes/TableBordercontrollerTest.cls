@isTest
private class TableBordercontrollerTest {

	private static testMethod void testTableBordercontroller() {
		TriggerHelper.UpdateAccountField=false;
		System.runAs(DummyRecordCreator.platformUser) {
        RecordType candRT = DaoRecordType.indCandidateRT;
	   Account acc1=new Account();
	   acc1.Name='Testa';
	   insert acc1;
       List<Contact> conList = new List<Contact>(); 
       for(Integer i=0;i<3000;i++){
       		Contact cand = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1'+i,Email='Cand1'+i+'@testcanddispatcher.ctc.test',Accountid=acc1.id);
        	conList.add(cand);
        }
       insert conList;
       TableBordercontroller tablecontroller=new TableBordercontroller();
       tablecontroller.setallrecords(conList);
       System.assertEquals(tablecontroller.selectedlist.size(),10);
       tablecontroller.changerectohund();
       tablecontroller.changerectoten();
       tablecontroller.gethiderecords();
       tablecontroller.firstpage();
	   tablecontroller.lastpage();
	   tablecontroller.prepage();
	   tablecontroller.nextpage();
	   tablecontroller.getallrecords();
	   tablecontroller.gethasnext();
	   tablecontroller.gethaspre();	
		}
	}

}