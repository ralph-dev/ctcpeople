global class scheduledBatchUpdateForm implements Schedulable{    
    
    global void execute(SchedulableContext SC) {
        AutoUpdateProcess theprocess = new AutoUpdateProcess();
        theprocess.doUpdate();
    }
}