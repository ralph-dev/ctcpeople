@isTest
private class TestCareerOne {
	public static testmethod void testthiscontroller() {  
	    System.runAs(DummyRecordCreator.adminCareerOne) {
		    Test.setCurrentPage(Page.careerone); 
            Document div = DummyRecordCreator.createDocument();
            StyleCategory__c sc = new StyleCategory__c();        
            sc.Div_Html_File_ID__c = div.id;        
            sc.Div_Html_EC2_File__c='ahahah';        
            sc.Div_Html_File__c = 'afdaf';        
            sc.Div_Html_Doc__c = 'faf';        
            sc.Div_Html_File_Type__c = 'html';        
            sc.Name ='tst';        
            insert sc;        
            Placement__c p = new Placement__c(Name='test');        
            insert p;
            RecordType appFormRT = [select Id from RecordType where DeveloperName = 'ApplicationFormStyle' limit 1];
            StyleCategory__c appForm = new StyleCategory__c(RecordTypeId=appFormRT.Id,Name='testAppForm');  
        
            //insert template
            StyleCategory__c template = new StyleCategory__c(templateC1__c='12345'); 
		    insert template;
		
            Advertisement__c a = new Advertisement__c();
            a.Vacancy__c = p.Id;
            a.Reference_No__c ='test';        
            a.Salary_Max__c = 50000;        
            a.Salary_Min__c = 50000;
            a.Job_Content__c= 'okdafadfadsfadftestetetetetetetsetestafsdaraertaetaetatw';        
            a.Job_Title__c ='test';        
            a.Currency_Type__c ='AUD';
            a.Online_Footer__c = 'suzan';
            a.RecordTypeId = DaoRecordType.templateAdRT.Id;
            a.State__c = 'NSW';
            a.zipcode__c = '2000';
            a.City__c ='Sydney';
            a.Physical_Address__c = 'Sydney, NSW, 2000';
            a.CompensationType__c = '1';
            a.Education_Level__c = '12';
            a.Company_Name__c ='ctc';
            a.Hide_Company_Name__c = true;
            a.Work_Status__c = '4';
            a.WorkType__c = '1';
            a.Job_Bolding__c = true;
            a.Template__c = template.Id;//'a0190000000UCsp' ;//'12438';
            a.Industry_String__c = '808|||908';
            a.Application_Form_Style__c = appForm.Id;//'a0190000000UBnX';
            a.SearchArea_String__c = '1234';
            a.Occ_String__c = '123';
            a.Cat_String__c = '606';
            a.Status__c = 'Ready to Post';
            a.Online_Job_Id__c = '1234568';
            insert a;

            ApexPages.StandardController newcontroller = new ApexPages.StandardController(a);        
            CareerOneDetail thecontroller = new CareerOneDetail(newcontroller);
            thecontroller.init();
            thecontroller.getTemplateOptions();
            thecontroller.getStyleOptions();
            thecontroller.saveAndPost();
            system.assert(thecontroller.hasSaved);
            system.assert(thecontroller.newJob.Status__c=='Active');
            system.assert(thecontroller.newJob.JobXML__c!=null);
            String result = '<?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header><MonsterHeader xmlns="http://schemas.monster.com/MonsterHeader"><ChannelId xmlns="http://schemas.monster.com/MonsterHeader">168</ChannelId><MessageData><MessageId>ID1273137382611_3768_QA3BGWWEB202</MessageId><Timestamp>2010-05-06T04:16:25.908-05:00</Timestamp><RefToMessageId xmlns="http://schemas.monster.com/MonsterHeader">Jobs for company clicktocloud created on 2010-05-06T19:16:21Z</RefToMessageId></MessageData></MonsterHeader></SOAP-ENV:Header><SOAP-ENV:Body><JobsResponse xmlns="http://schemas.monster.com/Monster" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">  <ProcessingReceipt><RequestDocElement>{http://schemas.monster.com/Monster}Jobs</RequestDocElement><Status><ReturnCode returnCodeType="success">0</ReturnCode></Status></ProcessingReceipt><JobsPreProcessResponse xmlns="http://schemas.monster.com/Monster"><CompanyReference companyId="4055261"/><SoapEnvId>ID1273137382611_3768_QA3BGWWEB202</SoapEnvId><Status><ReturnCode returnCodeType="success">0</ReturnCode><Descriptions><Description descriptionType="info"><![CDATA[PreProcessing o.k.]]></Description></Descriptions></Status></JobsPreProcessResponse><JobResponse xmlns="http://schemas.monster.com/Monster"><JobReference jobRefCode="00D9000000012r8:a0090000001Eri8"><JobTitle>IT Java Programmer</JobTitle></JobReference><RecruiterReference userId="128071109"/><FolderReference><FolderName>IT Java Programmer</FolderName></FolderReference><CompanyReference companyId="4055261"><CompanyXCode>xc1b_austcorpx</CompanyXCode></CompanyReference><JobPostingResponse bold="false"><Location locationId="868"><City>sydney</City></Location><Locations><Location locationId="868"><City>sydney</City></Location></Locations><JobCategory monsterId="1"/><JobCategories><JobCategory monsterId="1"/></JobCategories><JobOccupations><JobOccupation categoryId="1" monsterId="11711"/></JobOccupations><JobCity>sydney</JobCity><BoardName monsterId="1"/><JobPostingDates><JobActiveDate>2010-05-06T04:16:24-05:00</JobActiveDate></JobPostingDates></JobPostingResponse><Status><ReturnCode returnCodeType="failure">1</ReturnCode><Descriptions><Description descriptionType="error"><![CDATA[Failure = Salary Type Id [1] is not found in lookupCurrency Type Id [4] is not found in lookupEducationLevel-monsterId does not contain an allowed valueLanguageOfJob Id [27] is not valid Source = ProcessJob_Phase1]]></Description></Descriptions></Status></JobResponse>  <ProcessingSummary xmlns="http://schemas.monster.com/Monster"><GroupStatus><TotalProcessed>1</TotalProcessed><NumGood>0</NumGood><NumFailed>1</NumFailed></GroupStatus></ProcessingSummary><JobsPostProcessResponse xmlns="http://schemas.monster.com/Monster"><CompanyReference companyId="4055261"/><SoapEnvId>ID1273137382611_3768_QA3BGWWEB202</SoapEnvId><Status><ReturnCode returnCodeType="success">0</ReturnCode></Status></JobsPostProcessResponse></JobsResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>';
            String jobid = thecontroller.newJob.Id;
            CareerOneDetail.ResultProcess( result,jobid);
	    }
    }
    
    static testmethod void testErrorHandling(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Placement__c p = new Placement__c(Name='test');
        
        RecordType appFormRT = [select Id from RecordType where DeveloperName = 'ApplicationFormStyle' limit 1];
        StyleCategory__c appForm = new StyleCategory__c(RecordTypeId=appFormRT.Id,Name='testAppForm');        
        insert p;
        
        //remove user amazon s3 buck to raise error
        User u = [select Id,AmazonS3_Folder__c from User where Id=: UserInfo.getUserId()];
        u.AmazonS3_Folder__c='';
        update u;
        
        StyleCategory__c[] websiteacc = [select id from StyleCategory__c where RecordType.DeveloperName like '%WebSite_Admin_Record%'];
        delete websiteacc;	
        
        Advertisement__c a = new Advertisement__c();
        a.Vacancy__c = p.Id;
        a.Reference_No__c ='test';        
        a.Salary_Max__c = 50000;        
        a.Salary_Min__c = 50000;
        a.Job_Content__c= 'okdafadfadsfadftestetetetetetetsetestafsdaraertaetaetatw';        
        a.Job_Title__c ='test';        
        a.Currency_Type__c ='AUD';
        a.Online_Footer__c = 'test';
        RecordType[] rt = [Select Id from RecordType where DeveloperName like '%careerone%'];
        if(rt !=null)         
        { a.RecordTypeId = rt[0].Id;   }
        a.State__c = 'NSW';
        a.zipcode__c = '2000';
        a.City__c ='Sydney';
        a.Physical_Address__c = 'Sydney, NSW, 2000';
        a.CompensationType__c = '1';
        a.Education_Level__c = '12';
        a.Company_Name__c ='ctc';
        a.Hide_Company_Name__c = true;
        a.Work_Status__c = '4';
        a.WorkType__c = '1';
        a.Job_Bolding__c = true;
        a.Template__c = '';//'a0190000000UCsp' ;//'12438';
        a.Industry_String__c = '808|||908';
        a.Application_Form_Style__c = appForm.Id;//'a0190000000UBnX';
        a.SearchArea_String__c = '1234|||345';
        a.Occ_String__c = '123|||980';
        a.Cat_String__c = '606|||45';
        a.Status__c = 'Ready to Post';
        a.Online_Job_Id__c = '1234568;1234567';
        a.Status__c = 'Deleted';
        ApexPages.StandardController newcontroller1 = new ApexPages.StandardController(a);
        careerOnePageEdit thecontroller1 = new careerOnePageEdit(newcontroller1);
        thecontroller1.is_test = true;
        thecontroller1.init();
    	system.assert(thecontroller1.haserror);
    	system.assert(ApexPages.getMessages().size()>0);
    	}
    }
    
    /*public static testmethod void testcareerOnePageEditcontroller() {   
        
        Placement__c p = new Placement__c(Name='test');
        
        RecordType appFormRT = [select Id from RecordType where DeveloperName = 'ApplicationFormStyle' limit 1];
        StyleCategory__c appForm = new StyleCategory__c(RecordTypeId=appFormRT.Id,Name='testAppForm');        
        insert p;
        
        Advertisement__c a = new Advertisement__c();
        a.Vacancy__c = p.Id;
        a.Reference_No__c ='test';        
        a.Salary_Max__c = 50000;        
        a.Salary_Min__c = 50000;
        a.Job_Content__c= 'okdafadfadsfadftestetetetetetetsetestafsdaraertaetaetatw';        
        a.Job_Title__c ='test';        
        a.Currency_Type__c ='AUD';
        a.Online_Footer__c = 'test';
        RecordType[] rt = [Select Id from RecordType where DeveloperName like '%careerone%'];
        if(rt !=null)         
        { a.RecordTypeId = rt[0].Id;   }
        a.State__c = 'NSW';
        a.zipcode__c = '2000';
        a.City__c ='Sydney';
        a.Physical_Address__c = 'Sydney, NSW, 2000';
        a.CompensationType__c = '1';
        a.Education_Level__c = '12';
        a.Company_Name__c ='ctc';
        a.Hide_Company_Name__c = true;
        a.Work_Status__c = '4';
        a.WorkType__c = '1';
        a.Job_Bolding__c = true;
        a.Template__c = '';//'a0190000000UCsp' ;//'12438';
        a.Industry_String__c = '808|||908';
        a.Application_Form_Style__c = appForm.Id;//'a0190000000UBnX';
        a.SearchArea_String__c = '1234|||345';
        a.Occ_String__c = '123|||980';
        a.Cat_String__c = '606|||45';
        a.Status__c = 'Ready to Post';
        a.Online_Job_Id__c = '1234568;1234567';
        insert a;
        system.debug(a);
        ApexPages.StandardController newcontroller2 = new ApexPages.StandardController(a);        
        careerOnePageEdit thecontroller2 = new careerOnePageEdit(newcontroller2);
        thecontroller2.is_test = true;
        thecontroller2.init();
        thecontroller2.getStyles();
        thecontroller2.getTemplateOptions();
        thecontroller2.getStyleOptions();
        thecontroller2.is_post = false;
        thecontroller2.post();
        String result = '<?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header><MonsterHeader xmlns="http://schemas.monster.com/MonsterHeader"><ChannelId xmlns="http://schemas.monster.com/MonsterHeader">168</ChannelId><MessageData><MessageId>ID1273137382611_3768_QA3BGWWEB202</MessageId><Timestamp>2010-05-06T04:16:25.908-05:00</Timestamp><RefToMessageId xmlns="http://schemas.monster.com/MonsterHeader">Jobs for company clicktocloud created on 2010-05-06T19:16:21Z</RefToMessageId></MessageData></MonsterHeader></SOAP-ENV:Header><SOAP-ENV:Body><JobsResponse xmlns="http://schemas.monster.com/Monster" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">  <ProcessingReceipt><RequestDocElement>{http://schemas.monster.com/Monster}Jobs</RequestDocElement><Status><ReturnCode returnCodeType="success">0</ReturnCode></Status></ProcessingReceipt><JobsPreProcessResponse xmlns="http://schemas.monster.com/Monster"><CompanyReference companyId="4055261"/><SoapEnvId>ID1273137382611_3768_QA3BGWWEB202</SoapEnvId><Status><ReturnCode returnCodeType="success">0</ReturnCode><Descriptions><Description descriptionType="info"><![CDATA[PreProcessing o.k.]]></Description></Descriptions></Status></JobsPreProcessResponse><JobResponse xmlns="http://schemas.monster.com/Monster"><JobReference jobRefCode="00D9000000012r8:a0090000001Eri8"><JobTitle>IT Java Programmer</JobTitle></JobReference><RecruiterReference userId="128071109"/><FolderReference><FolderName>IT Java Programmer</FolderName></FolderReference><CompanyReference companyId="4055261"><CompanyXCode>xc1b_austcorpx</CompanyXCode></CompanyReference><JobPostingResponse bold="false"><Location locationId="868"><City>sydney</City></Location><Locations><Location locationId="868"><City>sydney</City></Location></Locations><JobCategory monsterId="1"/><JobCategories><JobCategory monsterId="1"/></JobCategories><JobOccupations><JobOccupation categoryId="1" monsterId="11711"/></JobOccupations><JobCity>sydney</JobCity><BoardName monsterId="1"/><JobPostingDates><JobActiveDate>2010-05-06T04:16:24-05:00</JobActiveDate></JobPostingDates></JobPostingResponse><Status><ReturnCode returnCodeType="failure">1</ReturnCode><Descriptions><Description descriptionType="error"><![CDATA[Failure = Salary Type Id [1] is not found in lookupCurrency Type Id [4] is not found in lookupEducationLevel-monsterId does not contain an allowed valueLanguageOfJob Id [27] is not valid Source = ProcessJob_Phase1]]></Description></Descriptions></Status></JobResponse>  <ProcessingSummary xmlns="http://schemas.monster.com/Monster"><GroupStatus><TotalProcessed>1</TotalProcessed><NumGood>0</NumGood><NumFailed>1</NumFailed></GroupStatus></ProcessingSummary><JobsPostProcessResponse xmlns="http://schemas.monster.com/Monster"><CompanyReference companyId="4055261"/><SoapEnvId>ID1273137382611_3768_QA3BGWWEB202</SoapEnvId><Status><ReturnCode returnCodeType="success">0</ReturnCode></Status></JobsPostProcessResponse></JobsResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>';
        String aId = thecontroller2.job.Id;
        careerOnePageEdit.ResultProcessForEdit(result, aId);
        thecontroller2.deleteJob();
        result ='<?xml version="1.0" encoding="utf-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header><MonsterHeader xmlns="http://schemas.monster.com/MonsterHeader"><ChannelId xmlns="http://schemas.monster.com/MonsterHeader">168</ChannelId><MessageData><MessageId>ID1273551309363_3916_QA3BGWWEB202</MessageId><Timestamp>2010-05-10T23:15:31.675-05:00</Timestamp><RefToMessageId xmlns="http://schemas.monster.com/MonsterHeader">Company Jobs created on 2010-05-11T14:15:08Z</RefToMessageId></MessageData></MonsterHeader></SOAP-ENV:Header><SOAP-ENV:Body><DeletesResponse xmlns="http://schemas.monster.com/Monster" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><ProcessingReceipt><RequestDocElement>{http://schemas.monster.com/Monster}Deletes</RequestDocElement><Status><ReturnCode returnCodeType="success"/></Status></ProcessingReceipt><DeleteResponse><Target>JobPosting</Target><RecruiterReference><UserName>c1b_austcorp</UserName></RecruiterReference><Status><ReturnCode returnCodeType="success">0</ReturnCode><Descriptions><Description descriptionType="info">Expired [1] job postings o.k.</Description></Descriptions></Status><DeleteRequest><Delete><Target>JobPosting</Target><DeleteBy><Criteria>RecruiterName</Criteria><Value>c1b_austcorp</Value></DeleteBy><DeleteBy><Criteria>RefCode</Criteria><Value>00D9000000012r8:a0090000001Etlp</Value></DeleteBy></Delete></DeleteRequest></DeleteResponse></DeletesResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>';
        careerOnePageEdit.ResultProcessForDel(result,aId);
        thecontroller2.has_websiteacc = false;
        thecontroller2.valid_userinfo = false;
        thecontroller2.init();
        thecontroller2.remainingQuota = 100;
        thecontroller2.post();
    }*/
    
    public static testmethod void testcareerOnePageEditErrorHandling(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Placement__c p = new Placement__c(Name='test');
        
        RecordType appFormRT = [select Id from RecordType where DeveloperName = 'ApplicationFormStyle' limit 1];
        StyleCategory__c appForm = new StyleCategory__c(RecordTypeId=appFormRT.Id,Name='testAppForm');        
        insert p;
        
        Advertisement__c a = new Advertisement__c();
        a.Vacancy__c = p.Id;
        a.Reference_No__c ='test';        
        a.Salary_Max__c = 50000;        
        a.Salary_Min__c = 50000;
        a.Job_Content__c= 'okdafadfadsfadftestetetetetetetsetestafsdaraertaetaetatw';        
        a.Job_Title__c ='test';        
        a.Currency_Type__c ='AUD';
        a.Online_Footer__c = 'test';
        RecordType[] rt = [Select Id from RecordType where DeveloperName like '%careerone%'];
        if(rt !=null)         
        { a.RecordTypeId = rt[0].Id;   }
        a.State__c = 'NSW';
        a.zipcode__c = '2000';
        a.City__c ='Sydney';
        a.Physical_Address__c = 'Sydney, NSW, 2000';
        a.CompensationType__c = '1';
        a.Education_Level__c = '12';
        a.Company_Name__c ='ctc';
        a.Hide_Company_Name__c = true;
        a.Work_Status__c = '4';
        a.WorkType__c = '1';
        a.Job_Bolding__c = true;
        a.Template__c = '';//'a0190000000UCsp' ;//'12438';
        a.Industry_String__c = '808|||908';
        a.Application_Form_Style__c = appForm.Id;//'a0190000000UBnX';
        a.SearchArea_String__c = '1234|||345';
        a.Occ_String__c = '123|||980';
        a.Cat_String__c = '606|||45';
        a.Status__c = 'Ready to Post';
        a.Online_Job_Id__c = '1234568;1234567';
        a.Status__c = 'Deleted';
        ApexPages.StandardController newcontroller3 = new ApexPages.StandardController(a);
        careerOnePageEdit thecontroller3 = new careerOnePageEdit(newcontroller3);
        thecontroller3.is_test = true;
        thecontroller3.init();
        system.assert(ApexPages.getMessages().size()>0);
        system.assert(thecontroller3.haserror);
    	}
    } 
}