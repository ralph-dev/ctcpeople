public with sharing class InvoiceItemSelector extends CommonSelector {
	
	public InvoiceItemSelector(){
		super(Invoice_Line_Item__c.sObjectType);
	}
	private String queryStr='select id, Billing_Contact__c from Invoice_Line_Item__c ';
	public list<Invoice_Line_Item__c> getInvoiceItemsByBillingContactId(list<String> billingContactIds){
		list<Invoice_Line_Item__c> invoiceLineItems=new list<Invoice_Line_Item__c>();
		String invoiceItemQuery=queryStr+' where Billing_Contact__c in: billingContactIds and Invoice_Item_Id__c!=null ';
		
		checkRead('id, Billing_Contact__c');
		invoiceLineItems=database.query(invoiceItemQuery);
		return invoiceLineItems;
	}
	
	public list<Invoice_Line_Item__c> getInvoiceItemsByBillerId(list<String> accountBillerIds){
		list<Invoice_Line_Item__c> invoiceLineItems=new list<Invoice_Line_Item__c>();
		String invoiceItemQuery=queryStr+' where Account__c in: accountBillerIds and Invoice_Item_Id__c!=null ';
		
		checkRead('id, Billing_Contact__c');
		invoiceLineItems=database.query(invoiceItemQuery);
		return invoiceLineItems;
	}
}