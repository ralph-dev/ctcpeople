@isTest
private class CandidateExtensionGlobalTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		Contact con = TestDataFactoryRoster.createContact();
        
	        //test method getCandidate(String Id)
	        Contact conRetrieved = CandidateExtensionGlobal.getCandidate(con.Id);
	       	System.assertNotEquals(conRetrieved,null);
	       	
	       	//test method searchCandidate(String nameKeyword)
	       	String nameKeyword = '';
	       	List<Contact> conListRetrieved = CandidateExtensionGlobal.searchCandidate(nameKeyword);
	       	System.assertNotEquals(conListRetrieved,null);
    	}
        
    }
}