/**
 * This is the test class for FieldsHelper
 * Created by: Lina
 * Created on: 23/06/2017
 */

@isTest
private class FieldsHelperTest {

    static final String PEOPLESEARCHCOLUMN = '[{\"sortable\":null,\"referenceFieldName\":null,\"Order\":null,\"isCustomField\":null,\"fromasc\":null,\"fieldtype\":\"date\",\"Field_api_name\":\"CreatedDate\",\"defaultorder\":null,\"ColumnName\":\"Created Date\"},{\"sortable\":null,\"referenceFieldName\":null,\"Order\":null,\"isCustomField\":null,\"fromasc\":null,\"fieldtype\":\"PHONE\",\"Field_api_name\":\"Phone\",\"defaultorder\":null,\"ColumnName\":\"Business Phone\"}]';
    static String SCREENCANDIDATECOLUMN; //= '{\"Candidate Management\":[{\"sortable\":null,\"referenceFieldName\":\"LastModifiedBy\",\"Order\":0,\"isCustomField\":false,\"fromasc\":null,\"fieldtype\":\"REFERENCE\",\"Field_api_name\":\"LastModifiedById\",\"defaultorder\":null,\"ColumnName\":\"Last Modified By\"}],\"People\":[{\"sortable\":null,\"referenceFieldName\":\"Account\",\"Order\":1,\"isCustomField\":false,\"fromasc\":null,\"fieldtype\":\"REFERENCE\",\"Field_api_name\":\"AccountId\",\"defaultorder\":null,\"ColumnName\":\"Account\"},{\"sortable\":null,\"referenceFieldName\":\"CreatedBy\",\"Order\":2,\"isCustomField\":false,\"fromasc\":null,\"fieldtype\":\"REFERENCE\",\"Field_api_name\":\"CreatedById\",\"defaultorder\":null,\"ColumnName\":\"Created By\",\"columnObject\":{\"name\":\"People\"}}]}';

    static {
    	System.runAs(DummyRecordCreator.platformUser) { 
    		FieldsHelper.getLabelNames();
    	}
        
        SCREENCANDIDATECOLUMN = '{"'+FieldsHelper.cmLabel+'":[{\"sortable\":null,\"referenceFieldName\":\"LastModifiedBy\",\"Order\":0,\"isCustomField\":false,\"fromasc\":null,\"fieldtype\":\"REFERENCE\",\"Field_api_name\":\"LastModifiedById\",\"defaultorder\":null,\"ColumnName\":\"Last Modified By\"}],"'+FieldsHelper.contactLabel+'":[{\"sortable\":null,\"referenceFieldName\":\"Account\",\"Order\":1,\"isCustomField\":false,\"fromasc\":null,\"fieldtype\":\"REFERENCE\",\"Field_api_name\":\"AccountId\",\"defaultorder\":null,\"ColumnName\":\"Account\"},{\"sortable\":null,\"referenceFieldName\":\"CreatedBy\",\"Order\":2,\"isCustomField\":false,\"fromasc\":null,\"fieldtype\":\"REFERENCE\",\"Field_api_name\":\"CreatedById\",\"defaultorder\":null,\"ColumnName\":\"Created By\",\"columnObject\":{\"name\":\"'+FieldsHelper.contactLabel+'"}}]}';
    }
    
    static testMethod void testGetFieldsForPeopleSearch() {
    	System.runAs(DummyRecordCreator.platformUser) { 
    		 Set<String> fields = FieldsHelper.getFieldsForPeopleSearch(PEOPLESEARCHCOLUMN);
	        System.assert(fields.size() > 0);
	        System.assert(fields.contains('CreatedDate'));
	        System.assert(fields.contains('Phone'));
	        System.assert(fields.contains('RecordType.Name'));
    	}
       
    }

    static testMethod void testGetFieldsForPeopleSearchWithoutSelectedColumn() {
    	System.runAs(DummyRecordCreator.platformUser) { 
        Set<String> fields = FieldsHelper.getFieldsForPeopleSearch();
        System.assert(fields.size() > 0);
    	}
    }

    static testMethod void testGetFieldsForScreenCandidate() {
    	System.runAs(DummyRecordCreator.platformUser) { 
        Set<String> fields = FieldsHelper.getFieldsForScreenCandidate(SCREENCANDIDATECOLUMN);
        System.assert(fields.size() > 0);
        System.assert(fields.contains('Candidate__r.Account.Name'));
        System.assert(fields.contains('Candidate__r.AccountId'));
        System.assert(fields.contains('Candidate__r.CreatedBy.Name'));
        System.assert(fields.contains('Candidate__r.CreatedById'));
        System.assert(fields.contains('LastModifiedBy.Name'));
        System.assert(fields.contains('LastModifiedById'));
        testScreenCandidateFields(fields);
    	}
    }

    static testMethod void testGetFieldsForScreenCandidateWithoutSelectedColumn() {
    	System.runAs(DummyRecordCreator.platformUser) { 
        Set<String> fields = FieldsHelper.getFieldsForScreenCandidate(null);
        System.assertEquals(9, fields.size());
        testScreenCandidateFields(fields);
    	}
    }

    public static void testScreenCandidateFields(Set<String> fields) {
        System.assert(fields.contains('Candidate__r.Id'));
        System.assert(fields.contains('Candidate__r.Name'));
        System.assert(fields.contains('Candidate__r.Resume__c'));
        System.assert(fields.contains('Id'));
        System.assert(fields.contains('Name'));
        System.assert(fields.contains('Status__c'));
        System.assert(fields.contains('Recruiter_Notes__c'));
        System.assert(fields.contains('Shortlist__c'));
        System.assert(fields.contains('Shortlist_Time__c'));
    }
}