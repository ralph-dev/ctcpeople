@isTest
public class SearchOperatorTest {
    private static testMethod void testSearchOperator() {
    	System.runAs(DummyRecordCreator.platformUser) {
        SearchOperator so = new SearchOperator();
        
        so.setActive(true);
        System.assert(so.getActive());
        
        so.setDefaultValue(true);
        System.assert(so.getDefaultValue());
        
        so.setLabel('test');
        System.assertEquals('test', so.getLabel());
        
        so.setValue('test');
        System.assertEquals('test', so.getValue());
    	}
    }
}