@isTest
private class CTCEmailToCandidateTest {

    static testMethod void testHandleInboundEmail() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	
    	Messaging.InboundEmail email = new Messaging.InboundEmail();
       	
		email.plainTextBody = 'test content';
		email.subject = 'andys CV  - hello world';
		email.fromAddress = 'andy@anydomain.com';
		Messaging.InboundEmail.BinaryAttachment attach = new Messaging.InboundEmail.BinaryAttachment();
		attach.fileName = 'andy-latest-CV-resume.doc';
		attach.body = null;
		email.binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[]{attach};
		
		Messaging.InboundEmail email2 = new Messaging.InboundEmail();
       	
		email2.plainTextBody = 'test content';
		email2.subject = 'andys CV  - hello world';
		email2.fromAddress = 'andy@anydomain.com';
		
		
        CTCEmailToCandidate service = new CTCEmailToCandidate();
        
        Messaging.InboundEmailResult result = service.handleInboundEmail( email, new Messaging.InboundEnvelope());
        System.assertEquals(true, result.success);
        
        result = service.handleInboundEmail( email2, new Messaging.InboundEnvelope());
        System.assertEquals(true, result.success);
        System.assertEquals(true, result.success);
    	}
        
    }
}