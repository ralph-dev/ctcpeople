@isTest
private class CreateDummyDocumentTest {

	private static testMethod void test() {
		System.runAs(DummyRecordCreator.platformUser) {
		Document tempDoc = createDoc();
        CreateDummyDocument cdd = new CreateDummyDocument();
        cdd.CreateDummyDocument();
        cdd.myfiles.add(tempDoc);
        system.debug('myfiles ='+ cdd.myfiles);
        system.assertEquals(null, cdd.save());
        system.assert(cdd.optInlocalDocList.size()>0);
		}
	}

	private static Document createDoc() {
		Document doc = new Document();
		doc.Body = Blob.valueOf('Test');
		doc.Name = 'Test.txt';
		doc.Type = 'txt';
		doc.FolderId = DaoFolder.getFolderByDevName('CTC_Admin_Folder').Id;
		insert doc;
		return doc;
	}

}