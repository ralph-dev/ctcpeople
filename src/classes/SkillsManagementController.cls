global with sharing class SkillsManagementController{
    
    public String skillsJSON{get;private set;}
    public String skillGroupsJSON{get;private set;}
    public String skill2VerificationJSON{get;private set;}
    public String candidateSkillsJSON{get;private set;}
    public String addSkillsPageUrl{
    	get{
    		if(contact2ShowSkills != null && contact2ShowSkills.id != null){
 				PageReference skillupCandidatePage = Page.SkillUpCandidate;
        		skillupCandidatePage.getParameters().put('Id',contact2ShowSkills.Id);    			
    			return skillupCandidatePage.getUrl();
    		}else{
    			return Page.SkillUpCandidate.getUrl();
    		}
    	}
    }
    
    
    private ApexPages.StandardController con;
    public Contact contact2ShowSkills{get;private set;}
        
    public SkillsManagementController(ApexPages.StandardController con){
        // resolve the compaibility issue with IE10 by opening the page in IE8 mode.
        //Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');    
        this.con = con;
        contact2ShowSkills = (Contact)con.getRecord();
    }
    
    public String namespace{
        get{
            return PeopleCloudHelper.getPackageNamespace();
        }
    }  
    
    @RemoteAction
    global static String retrieveSkillGroupsJSON(){
        //List<Skill_Group__c> skillGroups = [select Id, Name from Skill_Group__c ];
        CommonSelector.checkRead(Skill_Group__c.SObjectType,'Id, Name, DisplayOrder__c');
        List<Skill_Group__c> skillGroups = [select Id, Name, DisplayOrder__c from Skill_Group__c order by DisplayOrder__c, Name Limit 50000];
        return System.JSON.serializePretty(skillGroups);
    }
    
    @RemoteAction
    //TODO: need to think about if there is any solution to optimize this function to avoid selecting all skills.
    global static String retrieveSkillsJSON(Id contactId){
        if(contactId == null)
            return '[]';
        CommonSelector.checkRead(Candidate_Skill__c.SObjectType,'Id,Skill__c,Skill__r.Name, Skill__r.Skill_Group__c, Skill__r.Skill_Group__r.name, Verified__c');
        List<Candidate_Skill__c> candidateSkills = [select Id,Skill__c,Skill__r.Name, Skill__r.Skill_Group__c, Skill__r.Skill_Group__r.name, Verified__c from Candidate_Skill__c where Candidate__c =: contactId];
        /*
        Map<Id,Boolean> skill2Verification = new Map <Id,Boolean>();
        for(Candidate_Skill__c candidateSkill : candidateSkills){
            skill2Verification.put(candidateSkill.Skill__c,candidateSkill.Verified__c);
        }*/
        return System.JSON.serializePretty(candidateSkills);
    }
    
    /*
        Update skill verification status.
        
        "updatedSkillVerificationStatus" is a map within which Id is the candidate skill id, boolean is its
        verification status. 
    */
    @RemoteAction
    global static void updateVerificationStatus(Map<Id,Boolean> updatedSkillVerificationStatus){
        List<Candidate_Skill__c> skillsToUpdate = new List<Candidate_Skill__c>();
        for(Id key : updatedSkillVerificationStatus.keySet()){
            Candidate_Skill__c candidateSkill = new Candidate_Skill__c(Id=key,Verified__c=updatedSkillVerificationStatus.get(key));
            skillsToUpdate.add(candidateSkill);
        }
        if(skillsToUpdate.size()>0) 
            CommonSelector.quickUpdate(skillsToUpdate, 'Verified__c');
            //update skillsToUpdate;
    }
    
    
    @RemoteAction
    global static void deleteSkills(List<String> candidateSkillToDelete){
       // system.debug('======== candidateSkillToDelete:' + candidateSkillToDelete);
        List<Candidate_Skill__C> csList = new List<Candidate_Skill__c>();
        for(Id csId:candidateSkillToDelete){
            Candidate_Skill__c cs = new Candidate_Skill__c(Id=csId);
            csList.add(cs);
        }
        CommonSelector.quickDelete(csList);
        //delete csList;
    }
    
    
    public PageReference addSkills(){
        PageReference skillupCandidatePage = Page.SkillUpCandidate;
        skillupCandidatePage.getParameters().put('Id',contact2ShowSkills.Id);
        return skillupCandidatePage;
    }

}