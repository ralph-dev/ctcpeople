/**
 * This is the data structure for Skill Search
 *
 * Created by: Lina
 * Created on: 09/08/2016
 * 
*/

public class SkillSearchDTO {
    
    public class Skill{
    	public String skillName; 			// Skill Name 
    	public String skillId; 			    // Skill Salesforce Id 
    	public SkillGroup skillGroup;
    	
    	public Skill(String Id, String name, String groupId, String groupName) {
    	    skillName = name;
    	    skillId = Id;
    	    skillGroup = new SkillGroup(groupId, groupName);
    	}
    }  
    
    public class SkillGroup {
        public String groupName;            // Skill Group Name
        public String groupId;              // Skill Group Salesforce Id
        
        public SkillGroup(String Id, String name) {
            groupName = name;
            groupId = Id;
        }
    }
    
    public class SkillQuery {
        public String query;
        public boolean verifiedSkills;
        
        public SkillQuery() {}
        
        public SkillQuery(String searchQuery, boolean flag) {
            query = searchQuery;
            verifiedSkills = flag;
        }
    }
    
}