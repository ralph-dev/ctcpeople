/**
 * This class is used to convert the old Google Saved Search to new People Saved Search
 * There are major changes to the operator and values for different field type
 * Please refer to the code for detailed changes.
 * 
 * Created by: Lina
 * 
**/


public with sharing class SaveSearchConvertor {
    
    private static Map<String, String> fieldsTypeMap;
    private static Map<String, String> operatorMap;
    
    static {
        if (fieldsTypeMap == null) {
            getAllContactFieldsTypeMap();
        }
        
        if (operatorMap == null) {
            getOperatorMap();
        }
    }
    
    public static List<StyleCategory__c> convertGoogleSearchToPeopleSearch(List<StyleCategory__c> StyleCategories) {
        List<StyleCategory__c> newStyleCategories = new List<StyleCategory__c>();
        for (StyleCategory__c sc : StyleCategories) {
            try {
                // if this is a saved search for Google search
                if (sc.ascomponents__c.contains('objsingle') || sc.ascomponents__c.contains('objmulti')) {
                    sc.ascomponents__c = convert(sc.ascomponents__c);
                } else {
                    // if this is a saved search for People search, but created before we changing the picklist type to select multiple values
                    if (sc.CreatedDate != null && sc.CreatedDate < Date.valueOf('2016-07-10')) {
                        sc.ascomponents__c = convertPicklist(sc.ascomponents__c);
                    }
                    
                    // if this is a saved search for People search, but created before we implement skill search
                    // need to convert the old skill search to new skill search
                    if (sc.ascomponents__c.contains('operatorMap')) {
                        sc.ascomponents__c = convertSkillSearch(sc.ascomponents__c);
                    }
                }
                newStyleCategories.add(sc);
            } catch (Exception e){
                system.debug('Convert the old saved search to new saved search failed.' + e);
            }
        }
        return newStyleCategories;
    }
    
    // Method to convert single picklist value to an array 
    // (This conversion only need to take action for saved search created before 10/07/2016)
    public static String convertPicklist(String oldJSON) {
        Map<String, Object> oldMap = (Map<String, Object>) JSON.deserializeUntyped(oldJSON);
        List<Object> searchCriterias = (List<Object>)oldMap.get('criteria');
        List<Object> newSearchCriterias = new List<Object>();
        for (Object o : searchCriterias) {
            try {
                Map<String, Object> searchCriteria = (Map<String, Object>) o;
                if ((String)searchCriteria.get('type') == 'PICKLIST') {
                    try {
                        List<String> values = (List<String>)searchCriteria.get('values');
                    } catch (Exception e){
                        List<String> values = new List<String>();
                        values.add((String)searchCriteria.get('values'));
                        searchCriteria.put('values', values);
                    }
                }
            } catch (Exception e) {
                // do nothing
            }
        }
        return JSON.serialize(oldMap);     
    }
    
    // Method to convert old skill search to new skill search
    // (This conversion only need to take action for saved search created before skill search implementation)
    public static String convertSkillSearch(String oldJSON) {
        Map<String, Object> oldMap = (Map<String, Object>) JSON.deserializeUntyped(oldJSON);
        Map<String, Object> oldSkillSearch = (Map<String, Object>)oldMap.get('skillsSearch');
        SkillSearchDTO.SkillQuery newSkillSearch = new SkillSearchDTO.SkillQuery();
        try {
            String oldSkillQuery = (String)oldSkillSearch.get('operatorMap');
            newSkillSearch = getSkillSearchFromOldJSON(oldSkillQuery);
        } catch (Exception e) {
            system.debug('fail to convert old skill search to new skill search');
        }
        oldMap.put('skillsSearch', newSkillSearch);
        return JSON.serialize(oldMap);     
    }    
    
    /**
     * Method to convert old Google Saved Search to new People Saved Search
     * 
     * @param: JSON String for Google Saved Search Criteria
     * @return: JSON String for new People Saved Search Criteria
     * 
    **/ 
    public static String convert(String oldJSON) {
        PeopleSearchDTO.SearchQuery query = new PeopleSearchDTO.SearchQuery();
        
        Map<String, Object> oldMap = (Map<String, Object>) JSON.deserializeUntyped(oldJSON);
            
        // keywords
        query.resumeKeywords = (String) oldMap.get('keywords');
        
        //skill search
        query.skillsSearch = getSkillSearchFromOldJSON((String)oldMap.get('operatorMap'));
        
        // availabilitySearch
        query.availability = getAvailabilitySearchFromOldJSON(oldMap);

        // Search Criteria
        List<PeopleSearchDTO.SearchCriteria> criteriaList = getSearchCriteriaFromOldJSON((Map<String, Object>)oldMap.get('objsingle'));
        criteriaList.addAll(getSearchCriteriaFromOldJSON((Map<String, Object>)oldMap.get('objmulti')));
        query.criteria = criteriaList;
        
        return JSON.serialize(query);
    } 
    
    /**
     * Convert old Google Search Metadata Criteria into new People Search Metadata Criteria
     * 
     * @param: Map<String, Object> oldCriteria
     * @return: List<PeopleSearchDTO.SearchCriteria>
     * 
    **/ 
    public static List<PeopleSearchDTO.SearchCriteria> getSearchCriteriaFromOldJSON(Map<String, Object> oldCriteria) {
        List<PeopleSearchDTO.SearchCriteria> criteriaList = new List<PeopleSearchDTO.SearchCriteria>();
        for (String fieldName : oldCriteria.keySet()) {
            try {
                Map<String, Object> fieldCriteria = (Map<String, Object>)oldCriteria.get(fieldName);
                PeopleSearchDTO.SearchCriteria newCriteria = new PeopleSearchDTO.SearchCriteria();
                newCriteria.field = fieldName;
                newCriteria.type = fieldsTypeMap.get(fieldName);
                // if the field type is date, need to change the operator to between 
                if (newCriteria.type == 'DATE' || newCriteria.type == 'DATETIME') {
                    newCriteria.operator = 'between';
                    DateTime oldDate = WizardUtils.convertDateToDateTime(Date.valueOf((String)fieldCriteria.get('value')));
                    Map<String, DateTime> dateValue = new Map<String, DateTime>();
                    // if old operator is "greaterorequal", change from date to input date and end date to 2050-12-31
                    if ((String)fieldCriteria.get('operator') == 'greaterorequal') {
                        dateValue.put('fromDate', oldDate);
                        dateValue.put('toDate', WizardUtils.convertDateToDateTime(Date.valueOf('2050-12-31'))); 
                    // if old operator is "lessorequal", change from data to 1900-1-1 and end date to input date
                    } else if ((String)fieldCriteria.get('operator') == 'lessorequal') {
                        dateValue.put('fromDate', WizardUtils.convertDateToDateTime(Date.valueOf('1900-1-1')));
                        dateValue.put('toDate', oldDate);
                    } else {
                        dateValue.put('fromDate', oldDate);  
                        dateValue.put('toDate', oldDate);
                    }
                    newCriteria.values = JSON.serialize(dateValue);
                } else if (newCriteria.type == 'PICKLIST' || newCriteria.type == 'REFERENCE') {
                    if (newCriteria.type == 'REFERENCE') {
                        newCriteria.type = 'PICKLIST';
                    }
                    if ((String)fieldCriteria.get('operator') == 'Include') {
                        newCriteria.operator = 'in';
                    } else if ((String)fieldCriteria.get('operator') == 'excludes'){
                        newCriteria.operator = 'notIn';
                    }
                    newCriteria.values = JSON.serialize(fieldCriteria.get('value')); 
                } else if (newCriteria.type == 'MULTIPICKLIST'){
                    newCriteria.operator = operatorMap.get((String)fieldCriteria.get('operator'));
                    newCriteria.values = JSON.serialize(fieldCriteria.get('value')); 
                } else if (newCriteria.type == 'BOOLEAN') {
                    newCriteria.operator = 'eq';
                    String oldBoolean = (String)fieldCriteria.get('value');
                    if (oldBoolean.containsIgnoreCase('true') || oldBoolean.containsIgnoreCase('yes')) {
                        if ((String)fieldCriteria.get('operator') == 'notequals') {
                            newCriteria.values = JSON.serialize(false);
                        } else {
                            newCriteria.values = JSON.serialize(true);
                        }
                    } else if (oldBoolean.containsIgnoreCase('false') || oldBoolean.containsIgnoreCase('no')) {
                        if ((String)fieldCriteria.get('operator') == 'notequals') {
                            newCriteria.values = JSON.serialize(true);
                        } else {
                            newCriteria.values = JSON.serialize(false);
                        }
                    }
                } else if (newCriteria.type == 'NUMBER' || newCriteria.type == 'DOUBLE' || 
                           newCriteria.type == 'INTEGER' || newCriteria.type == 'DECIMAL' ||
                           newCriteria.type == 'CURRENCY' || newCriteria.type == 'PERCENT') {
                    newCriteria.operator = operatorMap.get((String)fieldCriteria.get('operator'));
                    String oldNumber = (String)fieldCriteria.get('value');
                    if (oldNumber.isNumeric()) {
                        newCriteria.values = oldNumber;
                    } else {
                        newCriteria.values = JSON.serialize(Decimal.valueOf(oldNumber));
                    }
                } else if (newCriteria.type == 'URL') {
                    if ((String)fieldCriteria.get('operator') == 'equals') {
                        newCriteria.operator = 'like';
                    } else if ((String)fieldCriteria.get('operator') == 'notequals') {
                        newCriteria.operator = 'notLike';
                    }
                    newCriteria.values = (String)fieldCriteria.get('value');
                } else {
                    newCriteria.operator = operatorMap.get((String)fieldCriteria.get('operator'));
                    newCriteria.values = (String)fieldCriteria.get('value'); 
                }
                criteriaList.add(newCriteria);
            } catch (Exception e){
                System.debug('Convert field: ' + fieldName + ' failed.' + e);
            }
        }
        return criteriaList;
    }
    
    /**
     * Convert old skill search into new skill search
     * 
     * Old Data Type: "operatorMap":"{\"a0T28000005T7XMEA0\":\"and\",\"a0T28000005T7PiEAK\":\"or\",\"a0T28000005T7VsEAK\":\"and\",\"a0T28000005T7W7EAK\":\"and\",\"a0T28000005T7a9EAC\":\"or\"}"
     * 
     * @param: Map<String, String> oldSkillSearch
     * @return: SkillSearchDTO.SkillQuery
     * 
    **/     
    public static SkillSearchDTO.SkillQuery getSkillSearchFromOldJSON(String oldSkillSearch) {
        String query = '';
        Integer i = 0;
        Map<String, Object> oldQuery = (Map<String, Object>)JSON.deserializeUntyped(oldSkillSearch);
        
        for (String skillId : oldQuery.keySet()) {
            if (i == 0) {
                query += skillId;
                i++;
            } else {
                String logic = (String)oldQuery.get(skillId);
                query += ' ' + logic.toUpperCase() + ' ' + skillId;
            }
        }
        SkillSearchDTO.SkillQuery newSkillQuery = new SkillSearchDTO.SkillQuery(query, false);
        return newSkillQuery;
    }

    /**
     * Keep the availability search criteria the same, not yet implemented
    **/     
    public static SaveSearchConvertorDTO.OldAvailabilitySearch getAvailabilitySearchFromOldJSON(Map<String, Object> oldMap) {
        SaveSearchConvertorDTO.OldAvailabilitySearch availSearch = new SaveSearchConvertorDTO.OldAvailabilitySearch();
        availSearch.availabilitycriteria = oldMap.get('availabilitycriteria');
        availSearch.availabilitysearchmode = oldMap.get('availabilitysearchmode');
        return availSearch;
    }
    
    public static void getOperatorMap() {
        operatorMap = new Map<String, String>();
        operatorMap.put('equals', 'eq');
        operatorMap.put('notequals', 'ne');
        operatorMap.put('Include', 'inc');
        operatorMap.put('excludes', 'exc');
        operatorMap.put('greaterorequal', 'ge');
        operatorMap.put('lessorequal', 'le');
    }
    
    
    public static void getAllContactFieldsTypeMap(){
        fieldsTypeMap = new Map<String, String>();
        DescribeSObjectResult currentObject = Schema.getGlobalDescribe().get(Contact.SObjectType.getDescribe().getName()).getdescribe();
		Map<String, Schema.SObjectField> currentObjectFields = currentObject.fields.getmap();
        if (currentObjectFields != null) {
            for (Schema.SObjectField currentField : currentObjectFields.values()) {
                DescribeFieldResult fieldDescribe = currentField.getDescribe();
                fieldsTypeMap.put(fieldDescribe.getName(), fieldDescribe.getType().name().toUpperCase());
            }
        }
    }
    
}