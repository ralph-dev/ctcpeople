global with sharing class SkillUpCandidateController {
    public String candidateId {get;set;}
    
    public SkillUpCandidateController(){
        // resolve the compaibility issue with IE10 by opening the page in IE8 mode.
       // Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');           
        candidateId = ApexPages.currentPage().getParameters().get('id');
    }

    public String namespace{
        get{
            return PeopleCloudHelper.getPackageNamespace();
        }
    }
    
    @RemoteAction 
    global static String retrieveSkillGroupsJSON(){
        CommonSelector.checkRead(Skill_Group__c.SObjectType,'Id,Name, Default__c, Enabled__c,DisplayOrder__c');
        List<Skill_Group__c> skillGroupsList = [select Id,Name, Default__c, Enabled__c,DisplayOrder__c from Skill_Group__c where Enabled__c = true order by DisplayOrder__c, Name];
        //system.debug('++++++++++ skillGroupsList:' + skillGroupsList);
        return JSON.serializePretty(skillGroupsList);
    }
    
    @RemoteAction
    global static String retrieveSkillsJSON(String cid){
        
        CommonSelector.checkRead(Candidate_Skill__c.SObjectType,'Skill__c, Candidate__c');
        List<Candidate_Skill__c> candidateSkillsList = [select Skill__c, Candidate__c from Candidate_Skill__c where Candidate__c =: cid];   
        List<String> candidateExistingSkillIdList = new List<String>();
        for(Candidate_Skill__c ck:candidateSkillsList){
            candidateExistingSkillIdList.add(ck.Skill__c);
        }
        
        
        CommonSelector.checkRead(Skill__c.SObjectType,'Skill_Group__c, Skill_Group__r.Name, Name, Id, Ext_Id__c');
        List<Skill__c> skillsList = [select Skill_Group__c, Skill_Group__r.Name, Name, Id, Ext_Id__c 
                                    from Skill__c 
                                    where Id Not IN:  candidateExistingSkillIdList and Skill_Group__r.Enabled__c=true
                                    order by Name];
        
        
        return JSON.serialize(skillsList);
        
        
    }
    
    @RemoteAction
    global static Boolean insertNewSkillsForCandidate(String candiateId,List<String> newSkillsList){
        system.debug('=========== ccandiateIdid:'+candiateId);
        //system.debug('=========== newSkillsJSON:'+newSkillsList);

        List<Candidate_Skill__c> newSkillsToInsert = new List<Candidate_Skill__c>();
        for(Id newSkillId : newSkillsList){
            Candidate_Skill__c candidateSkill = new Candidate_Skill__c(Skill__c=newSkillId,Candidate__c=candiateId,Verified__c=true);
            newSkillsToInsert.add(candidateSkill);
        }
        if(newSkillsToInsert.size()>0){
            //check FLS
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Candidate_Skill__c.Skill__c,
                Candidate_Skill__c.Candidate__c,
                Candidate_Skill__c.Verified__c
            };
            fflib_SecurityUtils.checkInsert(Candidate_Skill__c.SObjectType, fieldList);
            insert newSkillsToInsert;
            return true;
        }else{
            return false;
        }
    }
    
    public PageReference goBackToPreviousPage(){
        system.debug('----------- going back to previous page!!!');
        //PageReference pageRef = new PageReference('/apex/SkillsManagement?id='+candidateId);
        PageReference pageRef = new PageReference('/'+candidateId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
}