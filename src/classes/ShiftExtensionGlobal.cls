public with sharing class ShiftExtensionGlobal {
	public ShiftExtensionGlobal(Object obj){
    }
    
    /**
    * 	retrieve open shifts for a vacancy, all the shifts' start date should be after today
    *
    **/
	@RemoteAction
	public static List<Shift__c> retrieveShiftsByVacancyId(String vacancyId){
		ShiftSelector sSelector = new ShiftSelector();
		// fetch open shifts whose start date is after today
		List<Shift__c> shifts = sSelector.fetchOpenShiftListByVacId(vacancyId);
		if(shifts.size() ==0 || shifts == null){
			return null;
		}
		
		ShiftService sService = new ShiftService();
		List<Shift__c> displayShifts = sService.getDisplayShifts(shifts);
		//System.debug('ShiftExtensionGlobal retrieveShiftsByVacancyId displayShifts: '+JSON.serialize(displayShifts));
		return displayShifts;
	}
	
	/**
    * 	retrieve open shifts for a vacancy, all the shifts' start date should be after today
    *
    **/
	@RemoteAction
	public static List<Shift__c> retrieveShiftsDBByVacancyId(String vacancyId){
		ShiftSelector sSelector = new ShiftSelector();
		// fetch open shifts whose start date is after today
		List<Shift__c> shiftsDB = sSelector.fetchOpenShiftListByVacId(vacancyId);
		if(shiftsDB.size() ==0 || shiftsDB == null){
			return null;
		}
		//System.debug('ShiftExtensionGlobal retrieveShiftsDBByVacancyId: '+JSON.serialize(shiftsDB));
		return shiftsDB;
	}
	
	/**
	* 	create shifts
	*
	**/
	@RemoteAction
	public static List<Shift__c> createShifts(String shiftsJson){
		//System.debug('shiftsJson: '+shiftsJson);
		List<ShiftDTO> shiftObjs = ShiftHelper.deserializeShiftsJson(shiftsJson);
		System.debug('shiftObjs size'+shiftObjs.size());
		ShiftService sService = new ShiftService();
		List<Shift__c> shifts = sService.upsertShiftsFromWizard(shiftObjs);
		//System.debug('shifts'+shifts);
		if(shifts.size() ==0 || shifts == null){
			return null;
		}
		List<Shift__c> displayShifts = sService.getDisplayShifts(shifts);
		//System.debug('ShiftExtensionGlobal createShifts shiftsForDisplay: '+JSON.serialize(displayShifts));
	
		return displayShifts;
	}
	
	/**
	* update shifts
	*
	**/
	@RemoteAction
	public static List<Shift__c> updateShiftDB(String shiftsJson){
		//System.debug('ShiftExtensionGlobal updateShiftDB shiftJson: '+shiftsJson);
		List<ShiftDTO> shiftsTBUpdated = ShiftHelper.deserializeShiftsJson(shiftsJson);
		
		ShiftService sService = new ShiftService();
		List<Shift__c> shiftsDB = sService.updateShiftStoredInDB(shiftsTBUpdated); 
		if(shiftsDB == null || shiftsDB.size() == 0){
			return null;
		}
		
		//System.debug('ShiftExtensionGlobal updateShiftDB shiftsDB: '+JSON.serialize(shiftsDB));
		return shiftsDB;
	}
	
	/**
	* get shift by shift id
	*
	**/
	@RemoteAction
	public static List<Shift__c> retrieveShiftsByShiftId(String shiftId){
		ShiftSelector sSelector = new ShiftSelector();
		List<Shift__c> shiftsDB = sSelector.fetchShiftListByShiftId(shiftId);
		if(shiftsDB == null || shiftsDB.size() == 0){
			return null;
		}
		
		ShiftService sService = new ShiftService();
		List<Shift__c> displayShifts = sService.getDisplayShifts(shiftsDB);
		//System.debug('ShiftExtensionGlobal retrieveShiftsByShiftId shiftsForDisplay: '+JSON.serialize(displayShifts));
		
		return displayShifts;
	}
	
	/**
	* delete shift
	*
	**/
	@RemoteAction
	public static Boolean deleteAllRecurShifts(String shiftId){
		ShiftService sService = new ShiftService();
		Boolean isDeleted = sService.deleteShiftById(shiftId);
		System.debug('ShiftExtensionGlobal deleteAllRecurShifts: '+isDeleted);
		return isDeleted;
	}
	
	/**
	* describe shift
	*
	**/
	@RemoteAction
    public static Map<String,List<Schema.PicklistEntry>> describeShift(){
     	Map<String, Schema.DescribeFieldResult> resultMap = DescribeHelper.getDescribeFieldResult(Shift__c.sObjectType.getDescribe().fields.getMap().values());
        
        Map<String,List<Schema.PicklistEntry>> describeResultMap = new Map<String,List<Schema.PicklistEntry>>();
        describeResultMap.put('ShiftType',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Shift_Type__c').picklistValues);
        describeResultMap.put('ShiftStatus',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Shift_Status__c').picklistValues);
        //describeResultMap.put('RateType',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Rate_Type__c').picklistValues);
       
        //System.debug('Describe Result'+JSON.Serialize(describeResultMap));
        
        return describeResultMap;        
    } 
}