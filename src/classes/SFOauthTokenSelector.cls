/*
  @Author Alvin
  Fetch the Customer Seeting value and Update the value
*/
public with sharing class SFOauthTokenSelector extends CommonSelector{

  public SFOauthTokenSelector(){
    super(SFOauthToken__c.SObjectType);
  }
  //Get SFOauth token record
  public static SFOauthToken__c querySFOauthToken(String name){
    SFOauthToken__c sfOauthToken = SFOauthToken__c.getInstance(name);
    if(sfOauthToken==null){
      sfOauthToken = newSFOauthToken(name);
    }
    return sfOauthToken;
  } 
  //Initiate sfOauthToken
  private static SFOauthToken__c newSFOauthToken(String name){
    SFOauthToken__c sfOauthToken = new SFOauthToken__c();
    sfOauthToken.Name = name;
    sfOauthToken.name__c = name;
    checkFLS();
    upsert sfOauthToken;
    
    return sfOauthToken;
  }
  //Update the SF Oauth Token at Customer Setting
  public static boolean upsertOauthToken(List<SFOauthToken__c> sfOauthtokenList){
    boolean isSuccess=true;
    try{
      List<SFOauthToken__c> sfotListToUpdate = new List<SFOauthToken__c>();
      for(SFOauthToken__c sfot: sfOauthtokenList){
        sfotListToUpdate.add(sfot);
      }
      checkFLS();
      upsert sfotListToUpdate Name__c;
      
    }catch(Exception ex){
      isSuccess=false;
      return isSuccess;
    }
    return isSuccess; 
  }

  private static void checkFLS() {
      List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
        SFOauthToken__c.Name,
        SFOauthToken__c.App_Id__c,
        SFOauthToken__c.ConsumerKey__c,
        SFOauthToken__c.InstanceURL__c,
        SFOauthToken__c.Name__c,
        SFOauthToken__c.OAuthToken__c,
        SFOauthToken__c.RefreshToken__c,
        SFOauthToken__c.SecretKey__c
      };      
      fflib_SecurityUtils.checkInsert(SFOauthToken__c.SobjectType, fieldList);
      fflib_SecurityUtils.checkUpdate(SFOauthToken__c.SobjectType, fieldList);
  }
}