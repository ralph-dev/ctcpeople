public with sharing class RatingController{
//** this class is used to rank vacancy candidate workflow

    Placement_Candidate__c vcwf;
    public RatingController(ApexPages.StandardController stdController) {
    	this.vcwf = (Placement_Candidate__c)stdController.getRecord();
    }
    public pageReference saverating(){
        fflib_SecurityUtils.checkFieldIsUpdateable(Placement_Candidate__c.SObjectType, Placement_Candidate__c.Rating__c);
    	update vcwf;
    	return null;
    }


    public static testMethod void testController(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Account a = new Account(Name = 'test');
        insert a ; 
        Placement__c p = new Placement__c(Company__c=a.Id,Name='test');
        insert p;
        Contact c = new Contact(LastName ='test', Email ='test@user2test.com');
        insert c;
        Placement_Candidate__c newpc = new Placement_Candidate__c(Placement__c = p.Id ,Candidate__c = c.Id );
        insert newpc;
        ApexPages.StandardController stdc = new ApexPages.StandardController(newpc);
        RatingController thecontroller = new RatingController(stdc);
        pageReference testpagereference = thecontroller.saverating();
        system.assert(testpagereference == null);
    	}
    }

}