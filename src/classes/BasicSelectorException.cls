public class BasicSelectorException extends fflib_SecurityUtils.SecurityException {
	
	private fflib_SecurityUtils.SecurityException  se ;
     
   // UserSelectorTest.testGetUserList -- fflib_SecurityUtils.FlsException: You do not have permission to read the field Default_Seek_Account__c on User
    public BasicSelectorException(fflib_SecurityUtils.SecurityException se){
    	
    	this.se = se;
    	setMessage(se.getMessage());
    }
    
    public fflib_SecurityUtils.SecurityException getSecurityException(){
        return se;
    }
    
    public String getComponentName(){
    	
		String compName = '';
		String[] names = getMessage().split(' -- fflib_SecurityUtils.FlsException:');
		if(names.size() > 0){
			String[] coms = names[0].split('\\.');
			if( coms.size() > 1){
				coms = coms[ coms.size() - 2 ].split('\\s');
				if(coms.size() > 0 ){
					compName = coms[coms.size() - 1 ].trim();
				}
			}
		}
		
		
    	
		if (ApexPages.currentPage() != null && ApexPages.currentPage().getUrl() != null) { 
        	compName = compName + ' - ' + 'Page : ' + ApexPages.currentPage().getUrl();
        } 
        
        else if(RestContext.request != null && RestContext.request.requestURI != null){
            compName = compName + ' - Rest : ' + RestContext.request.requestURI ;
        }
        else if(System.isFuture()){
            compName = compName + ' - Futrue';
        }  
        
        else if(Trigger.isExecuting){
            compName = compName + ' - Trigger';
        }  
        else if(System.isScheduled()){
            compName = compName + ' - Schedule' ;
        } else{
            try
            {
                throw new DmlException();
            }
            catch (DmlException e)
            {
                String stackTrace = e.getStackTraceString();
                
                Integer p = stackTrace.lastIndexOf('Class.');
                if(p >= 0){
                    String topClassName = stackTrace.substring(p + 6);
                    topClassName = topClassName.substring(0, topClassName.lastIndexOf(':'));
                
                	compName = compName + ' - Class : '+ topClassName;
                }
                
                
            }
        }
        
            
    	return compName;
    	
    }
    
    public String getMethodName(){
    	
    		String[] names = getMessage().split(' -- fflib_SecurityUtils.FlsException:');
    		if(names.size() > 0){
    			String[] coms = names[0].split('\\.');
    			if( coms.size() > 1){
    				return coms[coms.size() - 1].trim();
    			}
    		}
    	
    	
    	return '';
    	
    }
    public String getObjectName(){
    	
    	
    		String[] names = getMessage().split(' on ');
    		if(names.size() > 0){
    			
    			return names[names.size() - 1 ].trim();
    
    		}
    	
    	
    	return '';
    	
    }
    
    public String getFieldName(){
    	
    		String[] names = getMessage().split(' on ');
    		if(names.size() > 0){
    			String[] coms = names[0].split(' the field ');
    			if( coms.size() == 1){
    				coms = names[0].split(' field ');
    			}
    			if(coms.size() > 0)
    				return coms[coms.size() - 1 ].trim();
    
    		}
    	
    	
    	return '';
    }
    
     public String getOperation(){
    	
    		String[] names = getMessage().split('You do not have permission to ');
    		if(names.size() > 1){
    			String[] coms = names[1].split(' the field ');
    			if( coms.size() == 1){
    				coms = names[1].split(' field ');
    			}
    			
    			if(coms.size() > 0)
    				return coms[0 ].trim();
    
    		}
    	
    	
    	return '';
    }

}