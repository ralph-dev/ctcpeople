/*
 * Author: Booz S. Espiridion
 */
public class PeopleSearchDTO {
	
    public class Field{
    	public String apiName;							/*reference to SF Field name */
    	public String label;							/*field display name */
    	public String type;								/*field type (e.g; picklist)*/
    	public List<NamedValue> picklistValues;			/*optional if type require set values (e.g; {"label","value"})*/
    	public String sequence;							/*field sequence for display legacy may be remove later */
    	
    	public void removeIDFieldOnReferenceField(){
			label = PeopleSearchHelper.removeIDPrefixIfReferenceField(label, type);
		}
    }
    
    public class NamedValue{
    	public String label;
    	public String value;
    	public NamedValue(String l, Object v){
    		label = l;
    		value = String.valueOf(v);
    	}
    }
    
    public class DateValue{
    	public String fromDate;
    	public String toDate;
    	
    	public void setAsLocalDate(String type){
    		fromDate = PeopleSearchHelper.getLocalDateTime(fromDate);
			toDate = PeopleSearchHelper.getLocalDateTime(toDate);
			if(type == PeopleSearchDTO.F_DATE_TIME){
				fromDate = fromDate+'T00:00:00.000Z';
				toDate = toDate+'T23:59:59.000Z';
			}
    	}
    }
    
    public class SearchQuery{
    	public String resumeKeywords; 			/*string containe the value from search resume by keyword Daxtra*/ 
		public List<SearchCriteria> criteria; 	/*list of criteria objects see criteriaObject for ref */ 
		public SkillSearchDTO.SkillQuery skillsSearch; 		/*list of skills to be parsed object structure to be confirmed */ 
		public SaveSearchConvertorDTO.OldAvailabilitySearch availability; 			/*contain availability search object structure to be confirmed */ 
    }
    
    public class SearchCriteria{
    	public String field; 			/*filed name */ 
    	public String type; 			/*field type*/ 
    	public String operator; 		/*query operator*/ 
    	public String values; 			/*value to query against, value structure will be based on type and operator*/
    }
    
    public class ContactSearchResult{
    	public Contact Contact;
    	public SearchResult MatchResult;
    	
        public ContactSearchResult(){}

    	public ContactSearchResult(Contact c){
    		this.Contact = c;
    		this.MatchResult = new SearchResult();
    	}
    }
    
    public class DaxtraResult{
    	public Decimal Score;
    	public List<String> Snippets;
    	public String CandidateId;
    }
    
    public class FieldColumn implements Comparable{
    	public String sortable;
    	public String referenceFieldName;
    	public String Order;
    	public Boolean isCustomField;
    	public String fromasc;
    	public String fieldtype;
    	public String Field_api_name;
    	public String defaultorder;
    	public String ColumnName;
    	
    	public FieldColumn(Schema.SObjectField currentField){
    		if(currentField != null){
    			DescribeFieldResult fieldDescribe = currentField.getDescribe();
    			Field_api_name = fieldDescribe.getName();
    			ColumnName = PeopleSearchHelper.getFieldLabel(fieldDescribe);
    			fieldtype = fieldDescribe.getType().name().toUpperCase();
    			isCustomField = fieldDescribe.isCustom();
    			referenceFieldName = fieldDescribe.getRelationshipName();
    		}
    	}
        
        public Integer compareTo(Object compareTo){
            FieldColumn compareToFieldCol = (FieldColumn)compareTo;
            if(ColumnName.compareTo(compareToFieldCol.ColumnName) == 0){
                return 0;
            }else if(ColumnName.compareTo(compareToFieldCol.ColumnName) >= 1){
                return 1;
            }else{
                return -1;     
            }
           
        }
    }
    
    //constatnts
    public static final String DOC_BASE_NAME = 'ClicktoCloud_Xml_';
    public static final String RECORDTYPEID = 'recordtypeid';
    	//supported fields
	    public static final String F_ID 			= 'ID';
	    public static final String F_STRING 		= 'STRING';
	    public static final String F_TEXTAREA 		= 'TEXTAREA';
	    public static final String F_PICKLIST 		= 'PICKLIST';
	    public static final String F_CHECKBOX 		= 'CHECKBOX';
	    public static final String F_BOOLEAN 		= 'BOOLEAN';
	    public static final String F_EMAIL 			= 'EMAIL';
	    public static final String F_PHONE 			= 'PHONE';
	    public static final String F_DATE 			= 'DATE';
	    public static final String F_DATE_TIME 		= 'DATETIME';
	    public static final String F_MILTI_PICKLIST = 'MULTIPICKLIST';
	    public static final String F_NUMBER 		= 'NUMBER';
	    public static final String F_INTEGER 		= 'INTEGER';
	    public static final String F_DOUBLE 		= 'DOUBLE';
	    public static final String F_DECIMAL 		= 'DECIMAL';
	    public static final String F_PERCENT 		= 'PERCENT';
	    public static final String F_CURRENCY 		= 'CURRENCY';
	    public static final String F_URL 		= 'URL';
        public static final String F_RECORDTYPE = 'RECORDTYPE';

	public static final Set<String> NUMBER_FIELDS = new Set<String>{
		F_DECIMAL, F_DOUBLE, F_INTEGER, F_NUMBER, F_PERCENT, F_CURRENCY
	};

    public static final Set<String> DATE_FIELDS = new Set<String> {
        F_DATE, F_DATE_TIME
    };
}