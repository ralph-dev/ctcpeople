/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AccountAstutePayrollImplementationTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		Integer recordNumber=10;
			DataTestFactory.recordNumber=recordNumber;
	    	list<Account> accounts=DataTestFactory.createAccounts();
	    	list<String> accIds=new list<String>();
	    	for(Integer i=0;i< accounts.size();i++){
	    		Account acc=accounts.get(i);
	    		if(Math.mod(i, 2)==0){
	    			acc.Is_Fixing_Data_For_AP__c=true;
	    		}else{
	    			acc.Is_Pushing_To_Astute_Payroll__c=true;
	    		}
	    		accIds.add(acc.Id);
	    	}
	    	 
	    	AccountAstutePayrollImplementation.astutePayrollTrigger(accounts);
	    	AccountAstutePayrollImplementation.syncClientBiller(accIds, UserInfo.getSessionId());
	    	System.assertEquals(accounts.get(0).Astute_Payroll_Upload_Status__c, 'On Hold');
    	}
    	
    }
}