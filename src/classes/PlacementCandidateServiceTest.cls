@isTest
private class PlacementCandidateServiceTest {
	
	static testMethod void TestCreateCMByVancancyAndCandidates() {
		System.runAs(DummyRecordCreator.platformUser) {
		Contact con = TestDataFactoryRoster.createContact();
		List<Id> contactIds = new List<Id>{con.Id};
		candidateDTO cDTO = new candidateDTO();
        cDTO.setCandidate(con);
        List<candidateDTO> canDTOListTest = new List<candidateDTO>();
        canDTOListTest.add(cDTO);

        list<Placement__c> createVacancies = DataTestFactory.createVacancies();
        Placement__c vacancy = createVacancies[0];
        List<Id> vacancyIds = new List<Id>{createVacancies[0].Id};

        Placement_Candidate__c cm = new Placement_Candidate__c();
        cm.Placement__c = vacancy.Id;
        cm.Candidate__c = con.Id;
        insert cm;

        PlacementCandidateService pCandidateService = new PlacementCandidateService();
        Map<String, Placement_Candidate__c> canCMMap = new Map<String, Placement_Candidate__c>();
        canCMMap = pCandidateService.createCMByVancancyAndCandidates(canDTOListTest, vacancy.Id);
        system.debug('cms =' + canCMMap.values());
        system.assert(canCMMap.size()==1);
        
        List<Placement_Candidate__c> cms = pCandidateService.createRecords(contactIds, vacancyIds);
        system.assertEquals(1, cms.size());
        
        List<Map<String, Object>> sobjs = new List<Map<String, Object>>();
		Map<String, Object> sobj = new Map<String, Object>();
		sobj.put('Placement__c', createVacancies[0].Id);
		sobj.put('Candidate__c', con.Id);
		sobjs.add(sobj);
		cms = pCandidateService.createRecords(sobjs);
		system.assertEquals(1, cms.size());
		}
	}
	
	static testmethod void testCreateRecordsWithSObjectMap() {
		System.runAs(DummyRecordCreator.platformUser) {
	    // create data
	    Contact con = DataTestFactory.createSingleContact();
	    Placement__c vacancy = DataTestFactory.createSingleVacancy();
	    
	    Map<String, Object> cm = new Map<String, Object>();
	    cm.put('Name', 'Test');
	    cm.put('Candidate__c',con.Id);
		cm.put('Placement__c', vacancy.Id);
	    List<Map<String, Object>> sobjs = new List<Map<String, Object>>();
	    sobjs.add(cm);
	    
	    // call method 
	    PlacementCandidateService service = new PlacementCandidateService();
	    List<SObject> cms = service.createRecords(sobjs);
	    
	    // verify
	    System.assertEquals(1, cms.size());
		}
	}
	
	static testmethod void testCreateRecordsWithContactIdAndVacancyId() {
		System.runAs(DummyRecordCreator.platformUser) {
	    // create data
	    List<String> contactIds = DataTestFactory.createContactListIds(3);
	    List<String> vacancyIds = DataTestFactory.createVacancyListIds(3);
	    
	    // call method
	    PlacementCandidateService service = new PlacementCandidateService();
	    List<Placement_Candidate__c> cms = service.createRecords(contactIds, vacancyIds);
	    
	    // verify
	    System.assertEquals(9, cms.size());
		}
	}
	
}