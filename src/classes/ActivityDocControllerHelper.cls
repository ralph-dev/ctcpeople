/**
This is a helper for new page UploadActivityFiles
**/
public with sharing class ActivityDocControllerHelper {
    
    private static final String ATTACHMENT_RESOURCE_TYPE = 'Attachment';
    private static final String FILE_RESOURCE_TYPE = 'File';
    private static final String ORG_ID = Userinfo.getorganizationid().subString(0,15);
    
    public static List<ActivityDoc> getActivityDocs(String id) {
        
        List<ActivityDoc> activityDocs = new List<ActivityDoc>();
        //Check Skill parsing function is enable or not
        Boolean enableskillparsing = SkillGroups.isSkillParsingEnabled();
        String skillParsing = String.Valueof(enableskillparsing);
        //Get user id
        User currentUser = UserInformationCon.getUserDetails;
        
        Task task = ActivityDocSelector.getTaskById(id);
        List<ContentDocumentLink> files = ContentDocumentLinkSelector.getFileByLinkedEntityId(id);

        if(currentUser.AmazonS3_Folder__c != null && currentUser.AmazonS3_Folder__c != '') {
            
            if(task.Attachments != null &&  task.Attachments.size()>0) {
                for(Attachment att : task.Attachments) {
                    ActivityDoc activityDoc = new ActivityDoc();
                    //added by andy yang
                    activityDoc.userId = currentUser.Id;
                    //
                    
                    activityDoc.orgId = ORG_ID;
                    activityDoc.activityId = task.Id;
                    activityDoc.whoMap.put(task.WhoId,getLookUpFieldNameViaObjectName(task.Who.type));
                    activityDoc.whatMap.put(task.WhatId,getLookUpFieldNameViaObjectName(task.What.type));
                    activityDoc.resourceType = ATTACHMENT_RESOURCE_TYPE;
                    activityDoc.resourceName = att.Name;
                    activityDoc.resourceId = att.Id;
                    activityDoc.amazonS3Folder = currentUser.AmazonS3_Folder__c;
                    activityDoc.nameSpace = PeopleCloudHelper.getPackageNamespace();
                    activityDoc.enableSkillParsing = skillParsing;
                    activityDocs.add(activityDoc);
                }
            }
            
            if(files!=null && files.size()>0) {
                for(ContentDocumentLink f : files) {
                    ActivityDoc activityDoc = new ActivityDoc();
                    //added by andy yang
                    activityDoc.userId = currentUser.Id;
                    //
                    //
                    activityDoc.orgId = ORG_ID;
                    activityDoc.activityId = task.Id;
                    activityDoc.whoMap.put(task.WhoId,getLookUpFieldNameViaObjectName(task.Who.type));
                    activityDoc.whatMap.put(task.WhatId,getLookUpFieldNameViaObjectName(task.What.type));
                    activityDoc.resourceType = FILE_RESOURCE_TYPE;
                    activityDoc.resourceName = f.ContentDocument.title;
                    activityDoc.resourceId = f.ContentDocumentId;
                    activityDoc.amazonS3Folder = currentUser.AmazonS3_Folder__c;
                    activityDoc.nameSpace = PeopleCloudHelper.getPackageNamespace();
                    activityDoc.enableSkillParsing = skillParsing;
                    activityDocs.add(activityDoc);
                    
                }
            }
        }
        return activityDocs;
    }
    
    public static PeopleCloudCustomErrorInfo uploadDocuments(List<ActivityDoc> activityDocs) {
    	
    	User currentUser = UserInformationCon.getUserDetails;
        
        
        List<ActivityDoc> selectedActivityDocs = new List<ActivityDoc>();
        String taskId = '';
        Set<String> fileIdSet = new Set<String>();
        List<ContentDocumentLink> cdlsNeedInsert = new List<ContentDocumentLink>();
        
        for(ActivityDoc doc : activityDocs) {
        	
        	//added by andy yang
            doc.userId = currentUser.Id;
            //
         
            if(Test.isRunningTest()){
                doc.isSelected = true;
            }
            
            if(doc.isSelected){
                taskId = doc.activityId;
                selectedActivityDocs.add(doc);
                if(doc.resourceType == FILE_RESOURCE_TYPE) {
                    //Get file id set which need to share to organisation
                    fileIdSet.add(doc.resourceId);
                    
                }
            }
        }
        //Check existing ContentDocumentLink
        List<ContentDocumentLink> cdlsExisting = ContentDocumentLinkSelector.getFileByLinkedEntityIdAndDocumentIds(ORG_ID, fileIdSet);
        //Only share to organisation when the file didnt get shared before
        if(cdlsExisting!=null && cdlsExisting.size()>0) {
            for(ContentDocumentLink existingCdl : cdlsExisting) {
                if(!fileIdSet.contains(existingCdl.ContentDocumentId)) {
                    ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentId=existingCdl.ContentDocumentId, LinkedEntityId=ORG_ID, ShareType='C', Visibility='AllUsers' );
                    cdlsNeedInsert.add(cdl);
                }
            }
        } else {
            for(String contentDocumentId : fileIdSet ) {
                ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentId=contentDocumentId, LinkedEntityId=ORG_ID, ShareType='C', Visibility='AllUsers' );
                cdlsNeedInsert.add(cdl);
            }
        }
        
        try {
                //check FLS
                List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                    ContentDocumentLink.ContentDocumentId,
                    ContentDocumentLink.LinkedEntityId,
                    ContentDocumentLink.ShareType,
                    ContentDocumentLink.Visibility
                };
                fflib_SecurityUtils.checkInsert(ContentDocumentLink.SObjectType,fieldList);
        		insert cdlsNeedInsert;       		
	        } catch(Exception e) {
	        	return customerErrorMessage(PeopleCloudErrorInfo.ACTIVITY_FAIL_TO_SHARE_FILE_TO_ORGANISATION, false);
			}
        
        if(selectedActivityDocs != null && selectedActivityDocs.size()>0){
        	String jsonString = '';
        	String sessionId = '';
        	String surl = '';
        	try {
	            jsonString = JSON.serialize(selectedActivityDocs);
	            jsonString=EncodingUtil.urlEncode(jsonString,'UTF-8');
	            sessionId = EncodingUtil.urlEncode(UserInfo.getSessionId(), 'UTF-8');
	            surl = EncodingUtil.urlEncode(URL.getSalesforceBaseUrl().toExternalForm() +'/services/Soap/u/25.0/'+UserInfo.getOrganizationId(),'UTF-8');
        	} catch(Exception e) {
        		return customerErrorMessage(PeopleCloudErrorInfo.ACTIVITY_ENCODING_EXCEPTION, false);
        	}
        	
            ActivityDocServices.uploadDocumentByActivityAtt(jsonString, sessionId, surl);
            ActivityDocOperator.updateActivityStatus('Uploading','Request has been sent out', taskId);
            
        } 
        return customerErrorMessage(PeopleCloudErrorInfo.ACTIVITY_SEND_REQUEST_SUCCESS, true);
    }
    
    //Get custom error information
    public static PeopleCloudCustomErrorInfo customerErrorMessage(Integer msgcode, Boolean isSuccess){
        PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(msgcode,true);   
        PeopleCloudCustomErrorInfo errorInfo = new PeopleCloudCustomErrorInfo();
        errorInfo.successStatus = isSuccess;
        for(ApexPages.Message errors: errormesg.errorMessage) {
            errorInfo.msg = errors.getDetail() + '. ' + errorInfo.msg  ;
        }
        return errorInfo;
                       
    }
    
    //Check User S3 Amazon folder 
    public static Boolean isValidCloudStorageAccount() {
        
        User currentUser = UserInformationCon.getUserDetails;
        
        if(currentUser.AmazonS3_Folder__c != null && currentUser.AmazonS3_Folder__c != '') {
            return true;
        } else {
            return false;
        }
    }
    
    public static String getLookUpFieldNameViaObjectName(String objectApiName) {
        
        //all fields of resume and files 
        //key is the field api name 
        Map<String,Schema.SObjectField> resumeObjectFields 
                    = Web_Document__c.SObjectType.getDescribe().fields.getMap();
                    
        //map contains resume lookup object api name and corresponding filed api name
        Map<String,String> resumeLookUpObjectFieldMap=new Map<String,String>();
        
        String fieldApiName='';
        for(String key:resumeObjectFields.KeySet()) {
            
            Schema.SObjectField s=resumeObjectFields.get(key);
            String resumefieldApiName = s.getDescribe().getName();
            String resumeRelatedOjectName='';
            
            //Check if the field is lookup field
            if(s.getDescribe().getReferenceTo().size()>0) {
                resumeRelatedOjectName = s.getDescribe().getReferenceTo().get(0).getDescribe().getName();
            }
            
            //put lookup object name and field name in the map
            if(resumeRelatedOjectName != '' && resumeRelatedOjectName != null) {
                resumeLookUpObjectFieldMap.put(resumeRelatedOjectName,resumefieldApiName);
            }
        }
        
        fieldApiName=resumeLookUpObjectFieldMap.get(objectApiName);
        
        return fieldApiName;
    }


}