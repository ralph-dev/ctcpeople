/*
 *  Description: This is a service class for Document object
 *  Author: Jack ZHOU
 * 	Date: 27/05/2016
 *  
 *  For Create, Update And Delete Document 
 */
public with sharing class DocumentService extends SObjectCRUDService{
	public DocumentService() {
		super(Document.sObjectType);
	}

	public Document createTempDocumentByBodyForBulkEmail(String bodyString) {
		String startTimeStamp = ''+Datetime.now().getTime();    //Time Stamp
        String DeveloperName = 'GS_'+UserInfo.getUserId() + '_'+startTimeStamp;
        String FolderId = UserInfo.getUserId();

		Document doc  = new Document();
		doc.Name = DeveloperName;
		doc.Body = blob.valueOf(bodyString); 					
		doc.DeveloperName = DeveloperName;
		doc.FolderId = FolderId;
		Savepoint sp = Database.setSavepoint();
		try{
			//check FLS
			List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
				Document.Name,
				Document.Body,
				Document.DeveloperName,
				Document.FolderId
			};
			fflib_SecurityUtils.checkInsert(Document.SObjectType, fieldList);
			insert doc;
		}
		catch(Exception e){
			Database.rollback( sp );
		}
		return doc;
	}
}