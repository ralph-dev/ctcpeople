/**************************************
 *                                    *
 * Deprecated Class, 13/06/2017 Ralph *
 *                                    *
 **************************************/
public with sharing class IframeParent {
	//public String tempUrl {get; set;}
	//public PageReference pr;
	//public String jumpUrl {get; set;}
	//public Boolean displayIfrmae {get; set;} 
	//List<Placement_Candidate__c>  records;//get the selected records
	
	////Return back by URL
	//public String selectlink {get; set;} 
	//public String commandlinkaction {get; set;}
 //   public String commandlinkvalue {get; set;}
	//public Id sendresumeid {get; set;}  //capture the previous page id
	//public string cmids; //query string for cm id records
 //   public string currentpage;//the page number in ShowAll
 //   public String currentSelectCrieria;
 //   private String currentAscSelectCrietria;
 //   //public string cmssource;
	
	//public IframeParent(ApexPages.StandardsetController setCon){
	//	tempUrl =  '';
	//	selectlink = '';
	//	sendresumeid=ApexPages.currentPage().getParameters().get('Id');
		
	//	if(test.isRunningTest()){
	//		records = (List<Placement_Candidate__c>)setCon.getRecords();
	//	} 
	//	else{             
 //       	records = (List<Placement_Candidate__c>)setCon.getSelected(); 
	//	}
		
	//	tempUrl = getTempUrl(sendresumeid, records);
	//}
	
	//public IframeParent(){
	//	tempUrl =  '';
	//	selectlink = '';
	//	sendresumeid = ApexPages.currentPage().getParameters().get('id');
 //       cmids = ApexPages.currentPage().getParameters().get('cmids');
 //       currentpage = ApexPages.currentPage().getParameters().get('cp');
 //       currentSelectCrieria = ApexPages.currentPage().getParameters().get('sc');
 //       currentAscSelectCrietria = ApexPages.currentPage().getParameters().get('asc');
	//	PageReference showallpage = redirectPage(ApexPages.currentPage().getURL());
	//	//PageReference showallpage = page.ShowAllCandidateManagement_new;			
 //       showallpage.setRedirect(true);
 //       showallpage.getParameters().put('sc',currentSelectCrieria);
 //       showallpage.getParameters().put('cp',currentpage);
 //       showallpage.getParameters().put('id',sendresumeid);
 //       showallpage.getParameters().put('asc',currentAscSelectCrietria); 
 //       showallpage.getParameters().put('cmids',cmids); 
 //       showallpage.getParameters().put('type',commandlinkaction); 
 //       tempUrl = showallpage.getURL();
 //       //system.debug('tempUrl ='+ tempUrl);
	//}
	
	//public String getTempUrl(String vid, List<Placement_Candidate__c> tempcms){
	//	String cmsidquery = '';
	//	//system.debug('URL = '+ ApexPages.currentPage().getURL());
	//	pr=redirectPage(ApexPages.currentPage().getURL());
	//	Map<String,String> params=pr.getParameters();
	//	//Translate the list value to query string and put to params
	//	if(tempcms!=null&&tempcms.size()>0){
	//		for(Placement_Candidate__c loopcm : tempcms){
	//			cmsidquery += loopcm.id + ',';
	//		}
	//		params.put('cmids',cmsidquery);
	//	}
		
	//	params.put('id', vid);		
	//	return pr.getURL();
	//}
	
	//public PageReference generateURL(){
	//	if(jumpUrl != null){
 //           if(jumpUrl.startsWith('/')){
 //               jumpUrl.replaceFirst('/+', '');
 //           }
	//		pr = new ApexPages.PageReference('/'+jumpUrl);			
	//	}
	//	else{
	//		pr = null;
	//	}
	//	pr.setRedirect(true);
	//	//system.debug('pr = '+ pr);
	//	return pr;
	//}
	
	////Back to previous page from parent Iframe page
	// public PageReference backToVac(){
	// 	//system.debug('commandlinkaction = '+ commandlinkaction + '+ '+ commandlinkvalue);
 //       PageReference backvacpage = new PageReference('/'+ sendresumeid);
 //       if(selectlink == 'backToShowAll'){
 //           backvacpage = page.IframeParentStandard;
 //           backvacpage.setRedirect(true);
 //           backvacpage.getParameters().put('sc',currentSelectCrieria);
 //           backvacpage.getParameters().put('cp',currentpage);
 //           backvacpage.getParameters().put('id',sendresumeid);
 //           backvacpage.getParameters().put('asc',currentAscSelectCrietria);
 //           }
 //       backvacpage.setRedirect(true);
 //       return backvacpage;
 //   }
    
 //   //get the jump page reference, command link value
 //   public PageReference redirectPage(String curURL){
 //   	commandlinkaction = '';
 //   	commandlinkvalue = '';
 //   	//page for Mass update function
 //   	if((curURL.toLowerCase()).contains('iframeparentsetmassupdate')){  
 //   		commandlinkaction = 'backToVacancy';  		
 //   		commandlinkvalue = '<<< Back to Vacancy Page!';    		
 //   		return page.MassUpdateVCW;
 //   	}
 //   	else if((curURL.toLowerCase()).contains('iframeparentmassupdate')){
 //   		commandlinkaction = 'backToShowAll';
 //   		commandlinkvalue = '<<< Back to Show All Page!';
 //   		return page.MassUpdateVCW;
 //   	}
 //   	else if((curURL.toLowerCase()).contains('enhancedmassrating')){
 //   		return page.enhancedmassrating;
 //   	}
 //   	else if((curURL.toLowerCase()).contains('extract4sr')){
 //   		return page.extract4sr;
 //   	}
 //   	//page for Show All function
 //   	else if((curURL.toLowerCase()).contains('iframeparentset')){
 //   		commandlinkaction = 'backToVacancy';
 //   		commandlinkvalue = '<<< Back to Vacancy Page!';
 //   		return page.ShowAllCandidateManagement_new;
 //   	}
 //   	else if((curURL.toLowerCase()).contains('iframeparentstandard')){
 //   		commandlinkaction = 'backToVacancy';
 //   		commandlinkvalue = '<<< Back to Vacancy Page!';
 //   		return page.ShowAllCandidateManagement_new;
 //   	}
 //   	else{
 //   		return null;
 //   	}
    	
 //   }
}