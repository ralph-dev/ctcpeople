// ** in use page: JobPosting
// ** to build up an ad template

Public with sharing Class JobPosting{

    public Placement__c job {get;set;}
    public Advertisement__c advertisement;

    public boolean isNext{get;set;}
    public boolean is_preview{get;set;}
    public String OnlineFooter{get;set;}
    public boolean is_Edit{get;set;}
    
    public boolean hasSeek{get;set;}
    public boolean hasMycareer{get;set;}
    public string jobTitle {get;set;}
    public string jobRef {get;set;}
    
   	public boolean show_page{get;set;}
    User currentuser;
    UserInformationCon usercls = new UserInformationCon();
    
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}
	
    public JobPosting(ApexPages.StandardController stdController){
        is_Edit = false;
        this.advertisement = (Advertisement__c)stdController.getRecord();
        skillparsingstyleclass = '';
         
         //Init skill parsing param. Add by Jack on 23/05/2013
         enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
         enableSkillGroupList = new List<Skill_Group__c>();
         enableSkillGroupList = SkillGroups.getSkillGroups();
         //end skill parsing param.
        
        //Create Ad Template
        if(ApexPages.currentPage().getParameters().get('vid') != null 
        	&& ApexPages.currentPage().getParameters().get('vid') != ''){
        		defaultskillgroups = new List<String>();
        		
        		CommonSelector.checkRead(Placement__c.sObjectType,'id, name, Online_Job_Title__c');
        		this.job = [select id, name, Online_Job_Title__c from Placement__c where id =
       		 			:ApexPages.currentPage().getParameters().get('vid')];    
        		this.advertisement.Currency_Type__c = 'AUD'; //set the currency is AUD
				this.advertisement.Job_Title__c = this.job.Online_Job_Title__c; 
	
	        	currentuser = usercls.getUserDetail();
	        	OnlineFooter = currentuser.WebJobFooter__c;   
	        	defaultskillgroups = getDefaultSkillGroups(enableSkillGroupList); //if create ad template, the default skill group is from skill group record
       	}
        else{
        	//Update Ad Template
            if(ApexPages.currentPage().getParameters().get('id') != null 
           		 && ApexPages.currentPage().getParameters().get('id') != ''){
           		 defaultskillgroups = new List<String>();
           		 CommonSelector.checkRead(Advertisement__c.sObjectType,
           		 	'Id, Reference_No__c, Online_Footer__c,  OnlineFooter__c,Job_Content_Rich__c,Vacancy__c,Skill_Group_Criteria__c');
           		 	
           		 CommonSelector.checkRead(Placement__c.sObjectType,'id, name');
           		 
           		 Advertisement__c ad = [select Id, Reference_No__c, Online_Footer__c, 
					                OnlineFooter__c,Job_Content_Rich__c,Vacancy__c,Skill_Group_Criteria__c from Advertisement__c 
					                where Id=:ApexPages.currentPage().getParameters().get('id')];
					                this.job = [select id, name from Placement__c where id =: ad.Vacancy__c];
                jobRef = ad.Reference_No__c;
                is_Edit = true;
                OnlineFooter = ad.Online_Footer__c;
                if(ad.Skill_Group_Criteria__c!=null){
	                String defaultskillgroupstring = ad.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
	            	defaultskillgroups = defaultskillgroupstring.split(',');
                }
            	else{
            		defaultskillgroups = getDefaultSkillGroups(enableSkillGroupList); //if create ad template, the default skill group is from skill group record
            	}
            }
        }
        //system.debug('defaultskillgroups = '+defaultskillgroups);
   }
   
   public void init(){
         show_page = true; 
         if(!Test.isRunningTest()){        
         	checkPrivilege();
         }
   }
  
   public PageReference SaveJob(){
   		//update job;
        this.advertisement.Reference_No__c = jobRef;
        this.advertisement.Vacancy__c = job.Id;
        this.advertisement.Online_Footer__c = OnlineFooter ;
        this.advertisement.OnlineFooter__c = OnlineFooter ;
        this.advertisement.Job_Content_Rich__c = this.advertisement.Job_Content__c;
        if(enableskillparsing){
        	String tempString  = String.valueOf(defaultskillgroups);
        	tempString = tempString.replaceAll('\\(', '');
        	tempString = tempString.replaceAll('\\)', '');
        	tempString = tempString.replace(' ','');
        	this.advertisement.Skill_Group_Criteria__c = tempString;
        }
        if(!Test.isRunningTest()){
          checkFLS();
        }
        
        insert this.advertisement;
        String newPageUrl = '/' + this.advertisement.Id ;
        PageReference newPage = new PageReference(newPageUrl);
        newPage.setRedirect(true);
        return newPage;
   }
   
   public PageReference UpdateJob(){
        this.advertisement.Reference_No__c = jobRef;
        this.advertisement.Online_Footer__c = OnlineFooter ;
        this.advertisement.OnlineFooter__c = OnlineFooter ;
        this.advertisement.Job_Content_Rich__c = this.advertisement.Job_Content__c;
        if(enableskillparsing){
        	String tempString  = String.valueOf(defaultskillgroups);
        	tempString = tempString.replaceAll('\\(', '');
        	tempString = tempString.replaceAll('\\)', '');
        	tempString = tempString.replace(' ','');
        	this.advertisement.Skill_Group_Criteria__c = tempString;
        }
        checkFLS(); 
        update this.advertisement;
        String newPageUrl = '/' + advertisement.Id ;
        PageReference newPage = new PageReference(newPageUrl);
        newPage.setRedirect(true);
        return newPage;
   }
   
   public void checkPrivilege(){
         /*S2SVerificationClass verifycontroller = new S2SVerificationClass(); 
         String check_site_result = verifycontroller.CheckJobWebsite();
         if(check_site_result != null)
         {
             if(check_site_result == '0')
             {
                 ApexPages.Message error= new ApexPages.Message(ApexPages.severity.INFO,'You or your SF.com instance has no access to post job online. Please contact your System Administrator, email log@clicktocloud.com, or log your issue in the Dream Cloud!');
                 ApexPages.addMessage(error);
                 show_page = false;
             }
             else if(check_site_result == '-1')
             {
                 ApexPages.Message error= new ApexPages.Message(ApexPages.severity.INFO,'Your Salesforce instance has not been activated with Job Posting functionality. Please contact your System Administrator, email log@clicktocloud.com, or log your issue in the Dream Cloud!');
                 ApexPages.addMessage(error);
                 show_page = false;
             }
             else {
                if(currentuser!=null)
                 {
                    if(currentuser.JobBoards__c == null || currentuser.JobBoards__c == '')
                    {
                        ApexPages.Message error= new ApexPages.Message(ApexPages.severity.INFO,'You has no privilege to post job online. Please contact your System Administrator.');
                        ApexPages.addMessage(error);
                        show_page = false;
                    }               
                }
             }
         }
         else
        {
            ApexPages.Message error= new ApexPages.Message(ApexPages.severity.INFO,'You has no privilege to post job online. Please contact your System Administrator.');
            ApexPages.addMessage(error);
            show_page = false;
         
         }*/

   
   }
  //***********
  
  //Add Skill Parsing function for create or update ad template
  public List<String> getDefaultSkillGroups(List<Skill_Group__c> tempskillgrouplist){
  	List<String> defaultSkillGroups = new List<String>();
  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
  		if(tempskillgroup.Default__c){
  			defaultSkillGroups.add(tempskillgroup.Skill_Group_External_Id__c);
  		}
  	}
  	return defaultSkillGroups;
  }
  
 public List<SelectOption> getSelectSkillGroupList(){
  	List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
  	List<SelectOption> displaySelectOption = new List<SelectOption>();
  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
  		displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
  	}
  	return displaySelectOption;
  }

      private void checkFLS(){
      List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Advertisement__c.Application_URL__c,
            Advertisement__c.ListingID__c,
            Advertisement__c.Ad_Closing_Date__c,
            Advertisement__c.Placement_Date__c,
            Advertisement__c.Advertisement_Account__c,
            Advertisement__c.Indeed_Question_URL__c,
            Advertisement__c.Vacancy__c,
            Advertisement__c.Skill_Group_Criteria__c,
            Advertisement__c.Job_Type__c,
            Advertisement__c.Job_Title__c,
            Advertisement__c.JXT_Short_Description__c,
            Advertisement__c.Bullet_1__c,
            Advertisement__c.Bullet_2__c,
            Advertisement__c.Bullet_3__c,
            Advertisement__c.Job_Content__c,
            Advertisement__c.Job_Contact_Name__c,
            Advertisement__c.Company_Name__c,
            Advertisement__c.Nearest_Transport__c,
            Advertisement__c.Residency_Required__c,
            Advertisement__c.IsQualificationsRecognised__c,
            Advertisement__c.Location_Hide__c,
            Advertisement__c.TemplateCode__c,
            Advertisement__c.AdvertiserJobTemplateLogoCode__c,
            Advertisement__c.ClassificationCode__c,
            Advertisement__c.SubClassificationCode__c,
            Advertisement__c.Classification2Code__c,
            Advertisement__c.SubClassification2Code__c,
            Advertisement__c.WorkTypeCode__c,
            Advertisement__c.SectorCode__c,
            Advertisement__c.Street_Adress__c,
            Advertisement__c.Search_Tags__c,
            Advertisement__c.CountryCode__c,
            Advertisement__c.LocationCode__c,
            Advertisement__c.AreaCode__c,
            Advertisement__c.Salary_Type__c,
            Advertisement__c.SeekSalaryMin__c,
            Advertisement__c.SeekSalaryMax__c,
            Advertisement__c.Salary_description__c,
            Advertisement__c.No_salary_information__c,
            Advertisement__c.Has_Referral_Fee__c,
            Advertisement__c.Referral_Fee__c,
            Advertisement__c.Terms_And_Conditions_Url__c,
            Advertisement__c.Status__c,
            Advertisement__c.Job_Posting_Status__c,
            Advertisement__c.JobXML__c,
            Advertisement__c.Prefer_Email_Address__c,
            Advertisement__c.RecordTypeId,
            Advertisement__c.Website__C
      };

      fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
      fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    } 
}