@isTest
private class TestAvailabilitySelector{
    final static String ROSTER_STATUS_PENDING = 'Pending';
    final static String NAME_SPACE_PREFIX = PeopleCloudHelper.getPackageNamespace();

    static testMethod void testGenerateQueryString(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Contact candidate = createTestCandidate(); 
        List<AvailabilityEntity> avlList = new List<AvailabilityEntity>();
        
        AvailabilityEntity avl1 = declareAvailabilityRec(candidate.Id, '2013-11-12', '2013-11-14');         
        AvailabilityEntity avl2 = declareAvailabilityRec(candidate.Id, '2013-11-22', '2013-11-24');
        avlList.add(avl1);
        avlList.add(avl2);
        
        String query = AvailabilitySelector.generateQueryString(null, avlList);
        //System.debug('TestAvailabilitySelector ---- generated query:' + query );
        system.assert(!String.isblank(query));
    	}
    }
    
    static testMethod void testGetAllRelatedAvlRecsByCanIdNDateRange(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Contact candidate = createTestCandidate();      
        Days_Unavailable__c duvl1 = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
        Days_Unavailable__c duvl2 = createDaysUnavailableRec(candidate.Id,'2013-09-13', '2013-09-20');
        Days_Unavailable__c duvl3 = createDaysUnavailableRec(candidate.Id,'2013-08-13', '2013-08-20');
        
        List<AvailabilityEntity> avlList = new List<AvailabilityEntity>();
        
        AvailabilityEntity avl1 = declareAvailabilityRec(candidate.Id, '2013-09-13', '2013-09-14');         
        AvailabilityEntity avl2 = declareAvailabilityRec(candidate.Id, '2013-11-12', '2013-11-14');
        avlList.add(avl1);
        avlList.add(avl2);
        
        List<Days_Unavailable__c> relatedDURecList = AvailabilitySelector.getAllRelatedAvlRecsByCanIdNDateRange(avlList);   
        //System.debug('TestAvailabilitySelector ---- relatedDURecList:' + relatedDURecList );
        System.assert(relatedDURecList.size() == 2);
    	}
    }
    
    static testMethod void testGenerateDummyContactList(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Contact candidate = createTestCandidate();      
        List<String> dummyIdList = new List<String>();
        List<Contact> dummyContactList = new List<Contact>();
        dummyIdList.add(candidate.Id);
        dummyContactList = AvailabilitySelector.generateDummyContactList(dummyIdList);  
        system.assert(dummyContactList.size()==1);
    	}
        
    }
    
    
    static testMethod void testGetAllRelatedAvlRecsByCanIdAvlIdNDateRange(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Contact candidate = createTestCandidate();      
        Days_Unavailable__c duvl1 = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
        Days_Unavailable__c duvl2 = createDaysUnavailableRec(candidate.Id,'2013-09-13', '2013-09-20');
        Days_Unavailable__c duvl3 = createDaysUnavailableRec(candidate.Id,'2013-08-13', '2013-08-20');
        
        List<AvailabilityEntity> avlList = new List<AvailabilityEntity>();
        
        AvailabilityEntity avl1 = declareAvailabilityRec(candidate.Id, '2013-09-13', '2013-09-14');         
        AvailabilityEntity avl2 = declareAvailabilityRec(candidate.Id, '2013-11-12', '2013-11-14');
        avlList.add(avl1);
        avlList.add(avl2);
        
        List<Days_Unavailable__c> relatedDURecList = AvailabilitySelector.getAllRelatedAvlRecsByCanIdAvlIdNDateRange(duvl2.Id, avlList);    
        //System.debug('TestAvailabilitySelector ---- relatedDURecList:' + relatedDURecList );
        System.assert(relatedDURecList.size() == 1);
    	}
    }
    
    
    static testMethod void testGetAvlRecordById(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Contact candidate = createTestCandidate();      
        Days_Unavailable__c duvl1 = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
    
        Days_Unavailable__c avl = AvailabilitySelector.getAvlRecordById(duvl1.Id);  
        //System.debug('TestAvailabilitySelector ---- avl:' + avl );
        System.assert(avl != null);
        
        delete duvl1;
        Days_Unavailable__c avlNull = AvailabilitySelector.getAvlRecordById(duvl1.Id);  
        System.assert(avlNull == null);
    	}
    }
    
    
    // declare an avl record
    private static AvailabilityEntity declareAvailabilityRec(String candidateId, String startDate, String endDate){
        AvailabilityEntity avl = new AvailabilityEntity();
        avl.startDate = startDate;
        avl.endDate = endDate;
        avl.candidateId = candidateId;
        return avl;
    }
    
    
    // create a days unavailable record
    private static Days_Unavailable__c createDaysUnavailableRec(String candidateId, String startDate, String endDate){
        Days_Unavailable__c duvl = new Days_Unavailable__c();
        duvl.Start_Date__c = Date.valueOf(startDate);
        duvl.End_Date__c = Date.valueOf(endDate);
        duvl.Contact__c = candidateId;
        duvl.Event_Status__c='Unavailable';
        insert duvl;
        return duvl;
    }
    
    // helper method
    // to create a dummy contact record
    private static Contact createTestCandidate(){
        Contact candidate = new Contact(LastName='candidate 1',email='Cand1@test.ctc.test');
        insert candidate;
        return candidate;
    } 

    static testMethod void getAvailabilityPickListValue(){
    	System.runAs(DummyRecordCreator.platformUser) {
        String fieldAPIName = NAME_SPACE_PREFIX+'Shift_Type__c';
        List<Schema.PicklistEntry> avaPickListValue = new List<Schema.PicklistEntry>();
        avaPickListValue = AvailabilitySelector.getAvailabilityPickListValue(fieldAPIName);
        //system.debug('avaPickListValue ='+ avaPickListValue);
        system.assert(avaPickListValue.size()>0);
    	}
    }

    static testMethod void generateShiftQueryString() {

	System.runAs(DummyRecordCreator.platformUser) {
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);

        List<ShiftDTO> shiftDTOs = new List<ShiftDTO>();              //create a shiftDTO
        ShiftDTO shiftDTO = new ShiftDTO();
        shiftDTO = TestDataFactoryRoster.createShiftDTO(acc, vac, shift);
        shiftDTOs.add(shiftDTO);

        List<Days_Unavailable__c> rosters = 
            TestDataFactoryRoster.createRostersForSplitRosterFunction(con, shift, acc,ROSTER_STATUS_PENDING);

        Set<String> CandidateIds = new Set<String>{con.id};

        AvailabilitySelector aSelector = new AvailabilitySelector();
        List<Days_Unavailable__c> dUnaviableList = aSelector.getAllAvailabiltyRecordByShifts(shiftDTOs, CandidateIds);
        system.assertEquals(dUnaviableList.size(),2);
	}
    }
}