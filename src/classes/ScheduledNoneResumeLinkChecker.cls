/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

// This schedule job is no longer used
global class ScheduledNoneResumeLinkChecker  implements Schedulable{
	global void execute(SchedulableContext SC) {
    }
}