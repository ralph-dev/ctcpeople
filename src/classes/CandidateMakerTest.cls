@isTest
private class CandidateMakerTest {
	
	static CandidateMaker defaultMaker = null;
	
	static Account a;
	static Task  sampleTask;
	static Attachment att;
	
	static Messaging.InboundEmail sampleMail;
	
	static{
		System.runAs(DummyRecordCreator.platformUser) {
			//for Admin account
		
			a = new Account(name='ctcpeopleAdmin(Do not Delete)');
			insert a;
	       
	       
	      	defaultMaker = new CandidateMaker();
	      	
	      	sampleTask = new Task(
	      		subject='Email Service: Sample task',
	      		whatId = a.Id,
	      		status = 'Completed',
	      		Priority = 'Normal');
	      		
	      	insert sampleTask;
	      	
	      	att = new Attachment();
			
			att.Name = CandidateMaker.formatFilename( 'sample.doc');
			att.Body = Blob.valueof('test attachment content');
			att.ParentId = sampleTask.id;
			
		    insert att;
	       
	        sampleMail = new Messaging.InboundEmail();
			sampleMail.plainTextBody = 'test content';
			sampleMail.subject = 'andys resume - hello world';
			sampleMail.fromAddress = 'andy@anydomain.com';
			Messaging.InboundEmail.BinaryAttachment attach = new Messaging.InboundEmail.BinaryAttachment();
			attach.fileName = 'andy-resume.pdf';
			attach.body = null;
			sampleMail.binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[]{attach};
		}
		
	   
	}
	
	static testMethod void testBuildActivityDoc(){
		System.runAs(DummyRecordCreator.platformUser) {
			User cusr = UserInformationCon.getUserDetails;
		
			ActivityDoc doc = defaultMaker.buildActivityDoc(sampleTask, att , cusr);
			
			System.assert(doc != null);
			System.assert(doc.isEmailService);
			System.assert(doc.isSelected);
			System.assertEquals(sampleTask.id ,doc.activityId);
			System.assertEquals(att.Name ,doc.attachmentName);
			System.assertEquals(cusr.AmazonS3_Folder__c ,doc.amazonS3Folder);
		}
		
		
	}
	
	static testMethod void makeAttachment(  ){
		System.runAs(DummyRecordCreator.platformUser) {
			CVContent c = new CVContent() ;
			c.setCvFilename('xxxxx.doc');
			c.setContent(Blob.valueof('test attachment content'));
			Attachment testAtt = defaultMaker.makeAttachment(sampleTask,c);
			
			System.assert(testAtt.name.startsWith('xxxxx'));
			System.assert(testAtt.name.endsWith('.doc'));
			System.assert(testAtt.parentId == sampleTask.id);
		}
		
	}
	
	static testMethod void testGetDefaultAdminAccount(){
		System.runAs(DummyRecordCreator.platformUser) {
			System.assertEquals(a.Id, defaultMaker.getDefaultAdminAccount( ));
		}
		
	}
	
	static testMethod void testRecordTask(){
		System.runAs(DummyRecordCreator.platformUser) {
			Messaging.InboundEmail mail = new Messaging.InboundEmail();
			mail.plainTextBody = 'test content';
			mail.subject = 'andys resume';
			
			Task t = defaultMaker.recordTask(mail);
			
			System.assert(t != null);
			System.assert(t.WhatId == a.Id);
			System.assertEquals('Email Service: andys resume',t.Subject);
			System.assertEquals('test content',t.Description);
			System.assertEquals('Completed',t.Status);
			System.assertEquals('Normal',t.Priority);
		}
		
		
	}

    static testMethod void testFormatFilename() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		 System.assertEquals(null ,CandidateMaker.formatFilename(null));
	        System.assertEquals('' ,CandidateMaker.formatFilename(''));
	        
	        String formatted = CandidateMaker.formatFilename('xx.doc');
	        System.assert(!formatted.equals('xx.doc'));
	        System.assert(formatted.startsWith('xx'));
	        System.assert(formatted.endsWith('.doc'));
	        
	        formatted = CandidateMaker.formatFilename('yy.xx.doc');
	        System.assert(!formatted.equals('yy.xx.doc'));
	        System.assert(formatted.startsWith('yy.xx'));
	        System.assert(formatted.endsWith('.doc'));
	        
	        formatted = CandidateMaker.formatFilename('xxx');
	        System.assert(!formatted.equals('xxx'));
	        System.assert(formatted.startsWith('xxx'));
    	}
       
        
        
        
    }
    
    static testMethod void testGetDefautSkillGroups(){
    	System.runAs(DummyRecordCreator.platformUser) {
    		String groups = defaultMaker.getDefautSkillGroups();
    		System.assert(groups != null);
    	}
    	
    }
    
    static testMethod void testCallWebService(){
    	System.runAs(DummyRecordCreator.platformUser) {
    		try{
	    		String url = defaultMaker.callWebService(sampleTask,att );
	    		System.assert(url != null && !url.equals(''));
	    	}catch(Exception e){
	    		System.assert(false);
	    	}
    	}
    	
    }
    
    static testMethod void testHandle(){
    	System.runAs(DummyRecordCreator.platformUser) {
    		defaultMaker.setEmail(sampleMail);
	    	try{
	    		defaultMaker.handle();
	    		System.assert(true);
	    	}catch(Exception e){
	    		System.assert(false);
	    	}
    	}
    	
    }
}