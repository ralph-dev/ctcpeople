@isTest
public class SendMailServiceTest {
    static testMethod void testSendMail(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Account acc = new Account();
        List<Contact> recipients = DataTestFactory.createContacts();
        Integer divid = 1;
        String pEmail = 'test@clicktocloud.com';
        String conEmail = 'testtest@clicktocloud.com' ;
        String addemail = 'testtestest@clicktocloud.com' ;
        ConRowRec conR = new ConRowRec(recipients[0],acc,divid,pEmail,conEmail,addemail);
        List<ConRowRec> conRList = new List<ConRowRec>();
        conRList.add(conR);
        List<Id> sfDocs = new  List<Id>();
        for(Contact con : recipients){
            sfDocs.add(con.id);
        }
        List<Messaging.Emailfileattachment> externalDocs = new List<Messaging.Emailfileattachment>();
        Messaging.Emailfileattachment msg = new Messaging.Emailfileattachment();
        msg.setFileName('fileName');
        externalDocs.add(msg);
        String sentCode = SendMailService.getSendMailMsg(0);
        system.assertEquals(sentCode, 'To Be Sent');
        String sentMSG = SendMailService.getContentType('pdf');
        system.assertEquals(sentMSG, 'application/pdf');
        Integer i1 = SendMailService.sendMailWithTemplate(recipients, externalDocs, sfDocs, recipients[0].Id);
        system.assertEquals(i1,200);
        Integer i2 = SendMailService.sendMailWithTemplateFromSR(conRList, recipients[0].Id, true);
        system.assertEquals(i2, 200);
        List<Placement_Candidate__c> cmList = DataTestFactory.createCandidateManagements();
        Integer i3 = SendMailService.sendMailWithTemplateFromMUD(cmList, externalDocs, recipients[0].Id, conEmail, addemail, true, recipients[0].Id);
        system.assertEquals(i3, 200);
    	}
    }
}