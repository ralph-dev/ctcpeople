public with sharing class InvoiceItemServices {
	public list<map<String,Object>> convertInvoiceItemToInvocieItem(list<Invoice_Line_Item__c> invoiceItems){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> invoiceItemMap=ctcPeopleHelperService.getinvoiceItemMap('Invoice_Line_Item_Mapping');
		list<map<String,Object>> invoiceItemInfos=new list<map<String,Object>>();	
		for(Invoice_Line_Item__c record : invoiceItems){
			invoiceItemInfos.add(ctcPeopleHelperService.convertToAPFormat(record, invoiceItemMap)); 
		}
		return 	invoiceItemInfos;			
	}
	public map<String,Object> convertInvoiceItemToInvocieItemMap(list<Invoice_Line_Item__c> invoiceItems){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> invoiceItemMap=ctcPeopleHelperService.getinvoiceItemMap('Invoice_Line_Item_Mapping');
		map<String,Object> invoiceItemInfosMap=new map<String,Object>();	
		for(Invoice_Line_Item__c record : invoiceItems){
			
			invoiceItemInfosMap.put(record.Id,ctcPeopleHelperService.convertToAPFormat(record, invoiceItemMap)); 
		}
		return 	invoiceItemInfosMap;			
	}
	/**
		Retrieve Invoice Item info from invoice item ids.
	****/
	public list<Invoice_Line_Item__c> retrieveInvoicesByIds(list<String> invoiceItemIds){
		list<Invoice_Line_Item__c> invoiceIterms=new list<Invoice_Line_Item__c>();
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		
		
		String queryStr=ctcPeopleHelperService.formQueryFromMap(ctcPeopleHelperService.getinvoiceItemMap('Invoice_Line_Item_Mapping'));
		
		CommonSelector.checkRead(Invoice_Line_Item__c.sObjectType, queryStr);
		
		queryStr='Select '+queryStr+' from Invoice_Line_Item__c where id in : invoiceItemIds';
		
		
		invoiceIterms=database.query(queryStr);
		return invoiceIterms;
	}
	
	/****
		Retrieve invoice Item Save info from invoice item ids 
	**/
	 public list<map<String,Object>> convertInvoiceItemToInvocieItemByIds(list<String> invoiceItemIds){
	 	list<Invoice_Line_Item__c> invoiceItems=retrieveInvoicesByIds(invoiceItemIds);
	 	
	 	CommonSelector.checkRead(Invoice_Line_Item__c.sObjectType, 'id, Account__c, Billing_Contact__c' );
	 	list<Invoice_Line_Item__c> invoiceItemRecords=[select id, Account__c, Billing_Contact__c from Invoice_Line_Item__c where id in :invoiceItemIds ];
	 	list<String> accountIds=new list<String>();
	 	list<String> billingContactIds=new list<String>();
	 	list<AstutePayrollRecordsDTO> apRecordDTOs=new list<AstutePayrollRecordsDTO>();
	 	for(Invoice_Line_Item__c invoiceItem: invoiceItemRecords){
	 		String accountId=invoiceItem.Account__c;
	 		String billingContactId=invoiceItem.Billing_Contact__c;
	 		AstutePayrollRecordsDTO apRecordDTO=new AstutePayrollRecordsDTO();
	 		apRecordDTO.accountId=accountId;
			apRecordDTO.billingContactId=billingContactId;
			apRecordDTO.invoiceItemId=invoiceItem.Id;
			if(accountId!=null){
				accountIds.add(accountId);
			}
			if(billingContactId!=null){
				billingContactIds.add(billingContactId);
			}
			apRecordDTOs.add(apRecordDTO);
	 	}
	 	AccountServices accountServ=new AccountServices();
		ContactServices contactServ=new ContactServices();
		map<String,Object> billers=accountServ.convertAccountToBillerMap(accountServ.retrieveAccountsById(accountIds));
		map<String,Object> accountBillingContacts=contactServ.convertContactToBillerContactMap(contactServ.retrieveBillerContactsByIds(billingContactIds));
		map<String,Object> invoiceItemsInfo=convertInvoiceItemToInvocieItemMap(invoiceItems);
		list<map<String,Object>> requestData=new list<map<String,Object>>(); 
		for(AstutePayrollRecordsDTO apRecordsDTO: apRecordDTOs){
			apRecordsDTO.setBiller(billers);
			apRecordsDTO.setBillingContact(accountbillingContacts);
			apRecordsDTO.setInvoiceItem(invoiceItemsInfo);
			requestData.add(apRecordsDTO.getBrandNewInvoiceMap());
		}
		
	 	return requestData;
	 }
	 
	 /***
	 *	Fix biller contacts. 
	 *
	 ***/
	 
    public void fixBillerContact(list<String> contactIds){
        InvoiceItemSelector invoiceItemSel=new InvoiceItemSelector();
        list<Invoice_Line_Item__c> invoiceItems= invoiceItemSel.getInvoiceItemsByBillingContactId(contactIds);
        list<String> invoiceIds=new list<String>();
        for(Invoice_Line_Item__c invoiceItem: invoiceItems){
            invoiceIds.add(invoiceItem.Id);
        }
        if(invoiceIds.size()>0){
        	AstutePayrollFutureJobs.pushBrandNewInvoiceToAP(invoiceIds,userinfo.getSessionId());
        }
    }
    
      
    /***
		Fix for biller.

		**/
    public void fixBiller(list<String> accountIds){
       InvoiceItemSelector invoiceItemSel=new InvoiceItemSelector();
        list<Invoice_Line_Item__c> invoiceItems= invoiceItemSel.getInvoiceItemsByBillerId(accountIds);
        list<String> invoiceIds=new list<String>();
        for(Invoice_Line_Item__c invoiceItem: invoiceItems){
            invoiceIds.add(invoiceItem.Id);
        }
        if(invoiceIds.size()>0){
        	AstutePayrollFutureJobs.pushBrandNewInvoiceToAP(invoiceIds,userinfo.getSessionId());
        }
    }

}