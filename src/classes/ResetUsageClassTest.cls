@isTest
public class ResetUsageClassTest {
    static testmethod void testresetQuotaClass() {
    	System.runAs(DummyRecordCreator.platformUser) {
//   		PCAppSwitch__c runControl = PCAppSwitch__c.getInstance();
// 		runControl.Enable_Standard_Quota_Assignment__c = true;
// 		update runControl;
		
		StyleCategory__c tempseek = new StyleCategory__c();
		tempseek.Name = 'tempseekaccount';
		tempseek.website__c = 'seek';
		tempseek.Recordtypeid = DaoRecordType.jobBoardAccRT.id;
		tempseek.Standard_Quota_Per_User__c = 10.00;
		insert tempseek;
	  	
	  	ResetUsageClass thecontroller = new ResetUsageClass();
	  	// system.assert(thecontroller.runControl);
	  	system.assert(true);
    	}
	}
	
	static testMethod void testGetBody() {
		System.runAs(DummyRecordCreator.platformUser) {
	    system.assertNotEquals(null, ResetUsageClass.getBody());
		}
	}
	
	static testMethod void testGetUpdatedUser() {
		System.runAs(DummyRecordCreator.platformUser) {
	    ResetUsageClass c = new ResetUsageClass();
	    User u = new User();
	    u.JobBoards__c = 'seek mycareer careerone';
	    system.assertNotEquals(null, c.getUpdatedUser(u));
		}
	}
}