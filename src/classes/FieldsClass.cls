public class FieldsClass{
//***
// this class is used to get describe information of an object eg. Contact, Placement
// this is used in apex class XmlWriterClass
// log@clicktocloud.com

    String SelectedObject;
    public FieldsClass( ){
        SelectedObject = 'Contact'; // default = Contact
    }
    public FieldsClass( String Object_Name){
    
        SelectedObject = Object_Name;
        
    }
    //**** class to store data of object fields
    public class AvailableField{
        //** basic information
        Private String Objectname;
        Private String Objectlabel;
        Private Boolean isCustom;
        Private String FieldLabel;
        Private String FieldApi;
        Private String FieldType;
        Private Boolean isFilterable;
		private boolean isUpdateable;
        //** particular detail, depending on the field type
        Private List<String> PicklistValues;
        Private List<sObjectType> RelatedObject;
        Private String RelationshipName; 
        Private Boolean isNamePointing;
        //** additional infor
        Private boolean selected;
        public boolean getIsUpdateable(){
        	return isUpdateable;
        }
        public void setIsUpdateable(boolean isUpdateable){
        	this.isUpdateable=isUpdateable;
        }
        public String getObjectlabel(){
     
        return this.Objectlabel;    
        }
        public String getObjectname(){
     
        return this.Objectname;    
        }
        public String getFieldLabel(){
     
        return this.FieldLabel;    
        }
        public String getFieldApi(){
         
            return this.FieldApi;    
        }
        public String getFieldType(){
         
            return this.FieldType;    
        }
        public String getRelationshipName(){
     
        return this.RelationshipName;    
        }
        public Boolean getIsCustom(){
         
            return this.isCustom;    
        }
        public List<sObjectType> getRelatedObject(){
         
            return this.RelatedObject;    
        }
        public List<String> getPicklistValues(){
         
            return this.PicklistValues;    
        }
        public void setPicklistValues(List<String> s){
         
            this.PicklistValues = s;    
        }
        public Boolean getSelected(){
         
            return this.selected;    
        }
        public void setSelected(Boolean b){
         
            this.selected = b;    
        }
        public void setIsNamePointing (Boolean b){
         
            this.isNamePointing  = b;    
        }
        public Boolean getIsFilterable(){
         
            return this.isFilterable;    
        }
        public void setIsFilterable (Boolean b){
         
            this.isFilterable = b;    
        }
        public AvailableField(boolean custom, String label, String apiName, String ftype, List<String> pValues, List<sObjectType> rObject, String rName, boolean is_Filterable){
         //** this used in searchadmincontroller
            this.isCustom = custom;
            this.FieldLabel = label;
            this.FieldApi = apiName;
            this.FieldType = ftype;        
            this.PicklistValues = pValues;
            this.RelatedObject = rObject;
            this.RelationshipName = rName;
            this.selected = false;
            this.isFilterable = is_Filterable;
            if(RelatedObject != null && RelatedObject.size()>1)
            {
                this.isNamePointing = true;
            }
            else
            {   this.isNamePointing = false;}
        }
        public AvailableField(String objlabel, String objname, String label, String apiName, String ftype, boolean is_Filterable){
         //** only used in method getRelatedObjFields();
            this.Objectlabel = objlabel;
            this.Objectname = objname;
            this.FieldLabel = label;
            this.FieldApi = apiName;
            this.FieldType = ftype;        
            this.isFilterable = is_Filterable;
            this.selected =false;
        }
    
    
    }
   public List<AvailableField> availableFields = new List<AvailableField>{};
   
   /**
   		Retrieve all fields of an object. Return field metadata are store in a wrapper class
   		called AvailableField.
   **/
   	public List<AvailableField> getAvailableFields(){
   		if(getAllFields())
            {return availableFields;}
            return null;
    }
   
    public boolean getAllFields(){
        String typestr;
        String tempStr;
        List<sObjectType> relatedObj;
        String rName;
        List<String> picklist_values; 
        List <Schema.PicklistEntry> pickValList;
        SObjectType objToken = Schema.getGlobalDescribe().get(SelectedObject); //get Contact Object SobjectType
        //SObjectType objToken = Schema.getGlobalDescribe().get('Placement__c');         
        DescribeSObjectResult objDef = objToken.getDescribe();  //get contact object description,such as getLabel=People;getLabelPlural=People;          
        Map<string, SObjectField> fields = objDef.fields.getMap();  //fields.values(): get all the api name of the all contact fields   
        try{
            for(Schema.SObjectField f : fields.values()){//get the picklist value of the field
                DescribeFieldResult fdr = f.getDescribe();//get the detail information of all the field
                typestr = null;
                picklist_values = null;
                relatedObj = null;
                rName = null;
    
                //** to get type name string
                tempStr = fdr.getType()+'';
                tempStr = tempStr.replace('.',';');
                List<String> tokens = tempStr.split(';');
                if(tokens.size() == 3){
                        tempStr = tokens[2];  // ** get the name string , eg. Schema.DisplayType.String tokens[2] = String
                }
                if(fdr.getType() == Schema.DisplayType.String ){ // if the type of field is string, the typestr is tempStr + length of the field
                        typestr  = tempStr+'('+fdr.getLength()+')';
                }else{
                        typestr = tempStr;
                } 
                
                if(fdr.getType() == Schema.DisplayType.Reference) // if the type of the field is reference
                {
                    relatedObj = fdr.getReferenceTo();
                    rName = fdr.getRelationshipName();
                
                }
              
                //typestr = fdr.getType()+ ':';
              AvailableField avaField=  new AvailableField(fdr.isCustom(), fdr.getLabel(), fdr.getName(), typestr ,picklist_values , relatedObj,rName,fdr.isFilterable() );
              avaField.setIsUpdateable(fdr.isUpdateable());
              availableFields.add(avaField);
           }
           System.debug('The available fields size is '+availableFields.size());  
       }
       catch(Exception ex){
            //logMessages.add('['+name+']:ERROR: currFldName='+currFldName+',currRow='+currRow);
            //system.debug('['+name+']:ERROR:'+ex.getMessage());
            //throw ex;
            return false;   
       }
       return true;
    }  
    
    public class SelectedField{
        Private String FieldLabel;
        Private String FieldApi;
        Private String FieldType;
        Private Boolean isFilterable;
        //** Location indicates this field is on left hand or right hand in the search pagelayout.
        //** Sequence indicates this field locates at array number 1, 2 .... in pagelayout.  
        Private String Location;
        Private Integer Sequence;

        //** particular detail, depending on the field type
        Private List<String> SelectedPicklistValues; //fetch the picklist field value
        Private List<sObjectType> RelatedObject;
        Private List<AvailableField> RelatedObjectFields;
        Private String RelationshipName; 
        Private Boolean isNamePointing;
        
        public String getFieldLabel(){
     
        return this.FieldLabel;    
        }
        public String getFieldApi(){
         
            return this.FieldApi;    
        }
        public String getFieldType(){
         
            return this.FieldType;    
        }
        public String getLocation(){
         
            return this.Location;    
        }
        public Integer getSequence(){
         
            return this.Sequence;    
        }
        public List<sObjectType> getRelatedObject(){
         
            return this.RelatedObject;    
        }
        public List<AvailableField> getRelatedObjectFields(){
            return this.RelatedObjectFields;
        
        }
        public void setRelatedObjectFields(List<AvailableField> fs){
            this.RelatedObjectFields = fs;
        
        }
        public void setLocation(String s){
         
            this.Location = s;    
        }
        public void setSequence(Integer i){
         
            this.Sequence = i;    
        }
        public String getRelationshipName(){
     
        return this.RelationshipName;    
        }
        public List<String> getSelectedPicklistValues(){
         
            return this.SelectedPicklistValues;    
        }
        public void setSelectedPicklistValues(List<String> s){
         
            this.SelectedPicklistValues = s ;    
        }
        public Boolean getIsNamePointing (){
         
            return this.isNamePointing ;    
        }
        public Boolean getIsFilterable(){
         
            return this.isFilterable;    
        }
        public void setIsFilterable (Boolean b){
         
            this.isFilterable = b;    
        }
         public SelectedField( String label, String apiName, String ftype, List<String> pValues, List<sObjectType> rObject, String rName, Boolean isF){
            
            this.FieldLabel = label;
            this.FieldApi = apiName;
            this.FieldType = ftype;        
            this.SelectedPicklistValues = pValues;
            this.RelatedObject = rObject;
            this.RelationshipName = rName;
            this.isFilterable = isF;
            if(this.RelatedObject != null && this.RelatedObject.size()>1)
            {
                this.isNamePointing = true;
            }
            else
            {   this.isNamePointing = false;}
        }
    }
	//****************************************Static Methods Below**************************************************
	// that is a static method works for apex class SearchAdminController
    public static List<AvailableField> getRelatedObjFields(SObjectType objectToken,Map<String,String> tempMap){
    //** only get fiels without type: reference   
       String objectLabel ;
       String objectName ;
       String typestr;
       String tempStr;
       AvailableField theField ;
       List<AvailableField> all_fields = new List<AvailableField>{};
       DescribeSObjectResult objDef = objectToken.getDescribe(); 
       objectLabel = objDef.getLabel();
       objectName = objDef.getName();
       Map<string, SObjectField> fields = objDef.fields.getMap();
       try{
			for(Schema.SObjectField f : fields.values()){
            DescribeFieldResult fdr = f.getDescribe();
            typestr = null;
            
            if(fdr.getType() != Schema.DisplayType.Reference && fdr.isFilterable()){ //** except reference field and textarea
                
                //** to get type name string
                tempStr = fdr.getType()+''; //** converted to string, smart!
                tempStr = tempStr.replace('.',';');
                List<String> tokens = tempStr.split(';');
                if(tokens.size() == 3){
                	tempStr = tokens[2];  // ** get the name string , eg. Schema.DisplayType.String tokens[2] = String
                }
                typestr = tempStr;
                //** to prepopulate
                theField = new AvailableField(objectLabel,objectName, fdr.getLabel(), fdr.getName(), typestr ,fdr.isFilterable() );
                if(tempMap != null){
                    if(tempMap.containsKey(fdr.getName().toLowerCase()))
                    {
                        theField.setSelected(true);
                    }
                }
                all_fields.add(theField);
            }
       	  }
       }
       catch(Exception ex){
       		return null;   
       }
       return all_fields;
    }  
    
    //****************************************Static Methods Below**************************************************
    /**
    *Author by Jack
    *Retrieve a list of field label name according to input fieldName.
    * If fieldName is 'email', then this method return a list of field label name which may include 'Email', 'Primary Email'.
    */
    public static List<String> getContactEmailObjFields(SObjectType objectToken, String fieldName, String fieldType){
       String sfFieldType = '';   // field type string 
       integer lastdot = 0;   //get the last "." position from Field type
       List<String> allFieldName = new List<String>(); // get the all field API Name according fieldName
       DescribeSObjectResult objDef = objectToken.getDescribe(); 
       Map<String,SObjectField> emailObjectfields = objDef.fields.getMap();     
       try{
           for (Schema.SObjectField f : emailObjectfields.values()){
                DescribeFieldResult fdr = f.getDescribe();  
                //field type format is "Schema.DisplayType.PICKLIST". use the substring to get field type
                sfFieldtype = String.valueOf(fdr.getType());
                lastdot = sfFieldtype.lastIndexOf('.');
                sfFieldtype = sfFieldtype.substring(lastdot+1, sfFieldtype.length());   
                             
                if(((fdr.getLabel().toLowerCase()).contains(fieldName.toLowerCase())) && sfFieldtype.toLowerCase() == fieldType.toLowerCase()){
                    allFieldName.add(fdr.getName());
                    
                }                        
           }
       }
       catch(Exception ex){
            
            return null;   
       }
       return allFieldName;  
    }
    //End getContactEmailObjFields
    
    
    // to generate a map
    // field name as key and the DisplayColumn instance as value
    public static Map<String,DisplayColumn> getReferenceFieldRelationshipMap(String objectName){
    	Map<String,DisplayColumn> relationNameMap = new Map<String,DisplayColumn>();
    	Schema.SObjectType objectToken = Schema.getGlobalDescribe().get(objectName);
    	Schema.DescribeSObjectResult describeSObjectResult = objectToken.getDescribe(); 
    	if (describeSObjectResult != null) {
			Map<String,SObjectField> fieldMap = describeSObjectResult.fields.getMap(); 
			
			for (Schema.SObjectField field : fieldMap.values()){
				DescribeFieldResult fieldResult = field.getDescribe();
				//if(String.valueOf(fieldResult.getType()) == 'Schema.DisplayType.REFERENCE'){
				/**
					system.debug(String.valueOf('----------- fieldResult= ' + fieldResult));
					system.debug(String.valueOf('----------- fieldResult.getName()= ' + fieldResult.getName()));
					system.debug(String.valueOf('----------- fieldResult.getType()= ' + fieldResult.getType())); 
					system.debug(String.valueOf('----------- fieldResult.getReferenceTo()= ' + fieldResult.getReferenceTo()));  
					system.debug(String.valueOf('----------- fieldResult.getRelationshipName()= ' + fieldResult.getRelationshipName())); 
					system.debug(String.valueOf('----------- fieldResult.getLabel()= ' + fieldResult.getLabel())); 
					system.debug(String.valueOf('----------- fieldResult.isCustom()= ' + fieldResult.isCustom())); 
					*/
					DisplayColumn column = new DisplayColumn();
					column.fieldtype = String.valueOf(fieldResult.getType());
					column.ColumnName = String.valueOf(fieldResult.getLabel());
					column.Field_api_name = String.valueOf(fieldResult.getName());
					column.isCustomField = fieldResult.isCustom();
					column.referenceFieldName = String.valueOf(fieldResult.getRelationshipName());
					column.sortable = fieldResult.isSortable();
					relationNameMap.put(fieldResult.getName(),column);
			//	}
			}
    	}
    	return relationNameMap;
    }
    
    /*
    	To sort a list of selectOption by label name
        @Param:List of selectOption to be sort.
        @Return: Sorted list of selectOptions by Label
    */
    public static List<selectOption> selectOptionSortByLabel(List<selectOption>
                                                            selectOptionsToSort) {
        if(selectOptionsToSort == null || selectOptionsToSort.size() <= 1){
            return selectOptionsToSort;
        }
        List<SelectOption> lessSelectOption = new List<SelectOption>();
        List<SelectOption> greaterSelectOption = new List<SelectOption>();
        integer pivot = selectOptionsToSort.size() / 2;
        
        //save the pivot and remove it from the selectOption list
        SelectOption pivotValue = selectOptionsToSort[pivot];
        selectOptionsToSort.remove(pivot);
        for(selectOption SO : selectOptionsToSort){
            if(SO.getLabel() <= pivotValue.getLabel()){
                lessSelectOption.add(SO);
            }else if(SO.getLabel() > pivotValue.getLabel()){
                greaterSelectOption.add(SO);   
            }
        }
        List<selectOption> sortedSelectOptions = new List<selectOption>(); 
        sortedSelectOptions.addAll(selectOptionSortByLabel(lessSelectOption));
        sortedSelectOptions.add(pivotValue);
        sortedSelectOptions.addAll(selectOptionSortByLabel(greaterSelectOption));
        return SortedSelectOptions;
    }
    
    public static testMethod void testSelectOptionSortByLabel(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	List<SelectOption> options = new List<SelectOption>();
 	 	options.add(new SelectOption('US','US'));
 	 	options.add(new SelectOption('CANADA','Canada'));
 	 	options.add(new SelectOption('MEXICO','Mexico'));
 	 	
 	 	List<SelectOption> sortedOptions = new List<SelectOption>();
 	 	sortedOptions = selectOptionSortByLabel(options);
 	 	/**
 	 	System.debug('testSelectOptionSortByLabel --- sortedOptions[0]=' + sortedOptions[0]);
 	 	System.debug('testSelectOptionSortByLabel --- sortedOptions[1]=' + sortedOptions[1]);
 	 	System.debug('testSelectOptionSortByLabel --- sortedOptions[2]=' + sortedOptions[2]);
 	 	*/
 	 	System.assertEquals('Canada', sortedOptions[0].getLabel() );
 	 	System.assertEquals('Mexico', sortedOptions[1].getLabel() );
 	 	System.assertEquals('US', sortedOptions[2].getLabel() );
    	}
 	 	
    }
    
    public static testMethod void testGetReferenceFieldRelationshipMap(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	String obj = 'Contact';
    	Map<String,DisplayColumn> relationNameMap =FieldsClass.getReferenceFieldRelationshipMap(obj);
        system.assert(relationNameMap.keySet().contains('Name'));
    	}
    }
 
    
    
    public static testMethod void test1() {
        System.runAs(DummyRecordCreator.platformUser) {
        FieldsClass newClass = new FieldsClass();
        newClass = new FieldsClass('Contact');
        FieldsClass.AvailableField af = new FieldsClass.AvailableField(false,'Name','Name', 'String',null,null,null,true );
        af.getFieldLabel();
        af.getObjectlabel();
        af.getObjectname();
        af.getFieldLabel();
        af.getFieldApi();
        af.getFieldType();
        af.getRelationshipName();
        af.getIsCustom();
        af.getRelatedObject();
        af.getPicklistValues();
        String[] ls = new String[]{'a','b','c'};        
                
        af.setPicklistValues(ls);
        af.getSelected();
        af.setSelected(true);
        af.setIsNamePointing(true);
        af.getIsFilterable();
        af.setIsFilterable(true);
        List<FieldsClass.AvailableField> allAvailableFields = newClass.getAvailableFields();
        system.assert(allAvailableFields.size()>0);
        
        // Not sure what's the need of testing a data entity
        FieldsClass.SelectedField sf = new FieldsClass.SelectedField('Name', 'Name','String',null,null,null,true );
        sf.getFieldLabel();
        sf.getFieldApi();
        sf.getFieldType();
        sf.getLocation();
        sf.getSequence();
        sf.getRelatedObject();
        sf.getRelatedObjectFields();
        List<AvailableField> fss = new List<AvailableField>{af};
        sf.setRelatedObjectFields(fss);
        sf.setLocation('left');
        sf.setSequence(1);
        sf.getRelationshipName();
        sf.getSelectedPicklistValues();
        sf.setSelectedPicklistValues(ls);
        sf.getIsNamePointing();
        sf.getIsFilterable();
        sf.setIsFilterable(true);
                
        Map<String,String> temp_map = new Map<String,String>();
        temp_map.put('ab',null);
        FieldsClass.getRelatedObjFields(Account.sObjectType,temp_map);
        //Retrieve all fields with name containing keywork email
        List<String> fieldWithNameContainEmail = FieldsClass.getContactEmailObjFields(Contact.SObjectType,'Email','Email');
        for(String fieldName:fieldWithNameContainEmail){
        	system.assert(fieldName.toLowerCase().Contains('email'));
        }
        }
    }
    

}