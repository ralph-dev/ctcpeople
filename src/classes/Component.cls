public class Component {
    public String label;
    public String htmlbody;
    public String componentId;
    public String apiname; // key
    public String fieldtype;
    public String inputvalue;
    public String filter;
    public List<SelectOption> operator;
    public String filterable;
    public List<String> keywords; // ** this is only for long text field keyword search. 
    public String sequence {get; set;}
    public String location {get; set;}
    public String googlesearchoperator;
    
    public component(String s1, String s2, String s3, String s4, String cId, String f, String filter, String inputvalue){
        this.label = s1;
        this.htmlbody = s2;
        this.fieldtype= s3;
        this.apiname = s4;
        this.componentId = cId;
        this.filter = filter;
        this.operator = QueryClass.getOperatorType(this.fieldtype);
        this.filterable = f;
    	this.inputvalue = inputvalue;
    } 
    public String getFilterable(){
        return this.filterable;    
    }
    public List<SelectOption> getOperator(){
     
        return this.operator;    
    }
    public String getFilter(){
     
        return this.filter;    
    }
    public void setFilter(String f){
     
        this.filter = f;    
    }
    public String getLabel(){
     
        return this.label;    
    }
    public String getHtmlbody(){
     
        return this.htmlbody;    
    }
    public String getComponentId(){
     
        return this.componentId;    
    }
    public String getApiname (){
     
        return this.apiname ;    
    }
    public String getFieldtype(){
     
        return this.fieldtype;    
    }
    public String getInputvalue(){
     
        return this.inputvalue;    
    }
    public void setInputvalue(String s){
     
        this.inputvalue = s;    
    }
    public List<String> getKeywords(){
     
        return this.keywords;    
    }
    public void setKeywords(List<String> s){
     
        this.keywords = s;    
    }
    
    public Component(String templabel, String tempapiname){//Author by Jack for Dynamic column
    	label = templabel;
    	apiname = tempapiname; 
    }
    
    public Component(String tempapiname, String tempfilter ,String tempinputvalue){//Author by Jack for Component JSON code
    	apiname = tempapiname; 
    	filter = tempfilter;
    	inputvalue = tempinputvalue;
    }
    
    //Component Constructor for General JSON doc
    public component(String label, String fieldtype, String apiname, String sequence, String location){
    	this.label = label;
        this.fieldtype= fieldtype;
        this.apiname = apiname;
        this.sequence = sequence;
        this.filterable = filterable;
        this.location = location;
    }
    
    public static testmethod void testcomponent(){
    	System.runAs(DummyRecordCreator.platformUser) {
    		List<String> a = new List<String>{};
	        //component newcomponent = new component();
	        component nc = new component('Last Name','<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_LastName" value=""/></div>','STRING','LastName','component0','0','0','');
	        nc.getApiname();
	        nc.getComponentId();
	        nc.getFieldtype();
	        nc.getFilter();
	        nc.getFilterable();
	        nc.getHtmlbody();
	        nc.getInputvalue();
	        nc.getKeywords();
	        nc.getLabel();
	        nc.getOperator();
	        nc.setFilter('one');
	        nc.setInputvalue('one');
	        nc.setKeywords(a);
	        system.assertEquals(nc.getApiname(),'LastName');
    	}
        
    }
}