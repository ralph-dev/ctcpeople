public with sharing class CVEmailParser extends AbstractEmailParser{
	
	public CVEmailParser(){
		
	}
	public CVEmailParser(Messaging.InboundEmail inEmail){
		super(inEmail);
	}
	
	 public override JXTContent parseJxtEmail() {
	    return new JXTContent();
	}
	
	public override CVContent parse(){
		
		if(this.email == null) return null;
		
		CVContent content = new CVContent( );
		
		if( this.email.binaryAttachments != null && this.email.binaryAttachments.size() > 0){
			
			for(Integer i = 0; i < this.email.binaryAttachments.size() ; i++){
				if(CandidateParserFactory.isValidAttachment(this.email.binaryAttachments[i].filename)){
					content.setCvFilename( this.email.binaryAttachments[i].filename);
					content.setContent( this.email.binaryAttachments[i].body);
					break;
				}
			}	
		}
		
		return content;
	}
    
}