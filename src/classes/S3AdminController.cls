/**
Page Controller for "Amazon S3 Admin" sub-tab for "Admin" page

**/

public with sharing class S3AdminController{

	String XMLbody;
	Document d = new Document();
	xmldom theXMLDom;
	
	String server ='amazons3';
	String develperName = 'S3Account';//configuration xml file name.
	public string url{get;set;}
	
	//public String key { get;  set; }      //Allow user to input the key 
	//public String secret { get;  set; }   //Allow user to input the secret 
	
	private transient String key;
	private transient String secret;

	public String getkey(){
		return null;
	}

	public void setkey(String key){
		this.key = key;
	}

	public String getsecret(){
		return null;
	}

	public void setsecret(String secret){
		this.secret = secret;
	}

	private String s3Key ;      //Allow user to create bucket by the key has beent input
	private String s3Secret ;    //Allow user to create bucket by the secret has beent input

	public Boolean hasS3 {get; set;}  //This is to check s3 account setup
	
	public Boolean is_editmode { get;  set; }
	public Boolean displayBucketandUser {get;set;}
	public S3.AmazonS3 as3 { get; private set; }
	
	public User[] users2 ;
	
	public String filestr {get;set;}
	public User[] getUsers2() {
		return users2;
	}
	public void setUsers2(User[] s){
		users2= s;
	}

	private S3Credential__c s3Credential; //get s3 credential
	
	//***********
	public String bucketNameToCreate {get;set;}
	public String bucketNameToDelete {get;set;} 
	public String bucketSelected {get;set;}
    public String bucketToList {get;set;}
    public String bucketToUploadObject {get;set;}
    public S3.ListEntry[] bucketList {get;set;}    
    public String bucketNameForCreate {get;set;}
    public String bucketNameForDelete {get;set;}
    public S3.ListAllMyBucketsEntry[] allBucketList {get;set;}
    public String accessTypeSelected {get;set;}
    public String bucketNameToModifyAccess {get;set;}
    public String OwnerId {get;set;}
    public Boolean renderListBucketResults {get;set;}
    public String listBucketErrorMsg {get;set;}
    public String createBucketErrorMsg {get;set;}
    public String deleteBucketErrorMsg {get;set;}
    public String deleteObjectErrorMsg {get;set;}
    public String uploadObjectErrorMsg {get;set;}
    public String uploadObjectErrorMsg2 {get;set;}
    public String objectToDelete {get;set;}
    public String folderIdSelected {get;set;}
    public String bucketSelectToUploadForceDoc {get;set;}
    public String docToUploadName {get;set;}
	public String docToUploadId {get;set;}
	public String createdBucketName {get;set;}
	public List<SelectOption> BucketNames {get;set;} 
	public Boolean UpdateCredentialSuccess {get;set;}
	//***********
	
	public void initS3(){
	    displayBucketandUser = true;
	    hasS3 = false;
	    CustomSettingSelector cSettingSelector = new CustomSettingSelector();

	    s3Credential = cSettingSelector.getOrgS3Credential();
	    s3Key = s3Credential.Access_Key_ID__c;
	    s3Secret = s3Credential.Secret_Access_Key__c;
		
		if(s3Key != null && s3Secret != null 
							&& s3Key != '' && s3Secret != '' ) {
	    	try{
	    		as3 = new S3.AmazonS3(s3Key,s3Secret);
	        } 
	        catch(System.CalloutException callout){
	          System.debug('CALLOUT EXCEPTION: ' + callout);
	          ApexPages.addMessages(callout);
	       }
	       catch(Exception ex){
	           System.debug(ex);
	           ApexPages.addMessages(ex);
	       }
	    }else {
	    	displayBucketandUser = false;
	    	hasS3 = true;
	    }
	}
	
	public void pageinit(){
	    initS3();
	    renderListBucketResults = false;
	    listBucketErrorMsg =null;
	    createBucketErrorMsg=null;
	    deleteBucketErrorMsg=null;
	    UpdateCredentialSuccess = false;
	    getAllBucketNames();
	    getAllUsers();
	    //system.debug('displayBucketandUser ='+ displayBucketandUser);
	}
	
	public void getAllUsers(){
	    users2 = new List<User>();
	    users2 = DaoUsers.getActiveUsers();
	}
	
	public String[] allBuckets {
		get{
			try{
				if(as3 != null){
		            Datetime now = Datetime.now();
		            S3.ListAllMyBucketsResult allBuckets = new S3.ListAllMyBucketsResult();
		            //This performs the Web Service call to Amazon S3 and retrieves all the Buckets in your AWS Account. 
		            if(!Test.isRunningtest()){
		            	allBuckets = as3.ListAllMyBuckets(as3.key,now,as3.signature('ListAllMyBuckets',now));
		            }
		            else{
		            	allBuckets = createdummydata();
		            }
		            //Store the Canonical User Id for your account
		            OwnerId = allBuckets.Owner.Id;
		            
		            S3.ListAllMyBucketsList bucketList = allBuckets.Buckets;
		            S3.ListAllMyBucketsEntry[] buckets = bucketList.Bucket;
		            allBucketList = buckets;
		            
		            String[] bucketNames = new String[]{};
		            
		            
		            //Loop through each bucket entry to get the bucket name and store in string array. 
		            for(S3.ListAllMyBucketsEntry bucket: buckets){
		                 System.debug('Found bucket with name: ' + bucket.Name);
		                 
		                 bucketNames.add(bucket.name);
		            }
		            return bucketNames;
		        }
		        return null;
	        }catch (System.NullPointerException e) {
	        	displayBucketandUser = false;
	           	return null;
	        }catch(Exception ex){
	           //System.debug(ex);
	           displayBucketandUser = false;
	           System.debug('caught exception in listallmybuckets');
	           ApexPages.addMessages(ex);
	           return null; 
	        }
	        
        }//end getter
	    set;
    }
	
	public void getAllBucketNames() {
	    BucketNames = new List<SelectOption>();
	    BucketNames.add(new SelectOption('','--Select--'));
	    String[] bckts = allBuckets;
	    if(bckts!=null){
	        for(String bucket : allBuckets){
	            BucketNames.add(new SelectOption(bucket,bucket));    
	        }
	           
	    }
	}

	public PageReference refresh() {
		pageinit();
		return null;
	}

	public PageReference listBucket(){
	
		try{
	        if(as3 != null){
	            if(bucketToList != null && bucketToList != ''){
		        	listBucketErrorMsg =  null;
		            Datetime now = Datetime.now();
		            Integer maxNumberToList = 1000;    //Set the number of Objects to return for a specific Bucket
		            String Prefix = null;              //Limits the response to keys that begin with the indicated prefix. You can use prefixes to separate a bucket into different sets of keys in a way similar to how a file system uses folders. This is an optional argument.
		            String Marker = null;              //Indicates where in the bucket to begin listing. The list includes only keys that occur alphabetically after marker. This is convenient for pagination: To get the next page of results use the last key of the current page as the marker. The most keys you'd like to see in the response body. The server might return less than this number of keys, but will not return more. This is an optional argument.
		            String Delimiter = null;           //Causes keys that contain the same string between the prefix and the first occurrence of the delimiter to be rolled up into a single result element in the CommonPrefixes collection. These rolled-up keys are not returned elsewhere in the response. 
		            
		            //System.debug('Going to execute S3 ListBucket service for bucket: ' + bucketToList);
		            
		            //This performs the Web Service call to Amazon S3 and retrieves all the objects in the specified bucket
		            S3.ListBucketResult objectsForBucket = as3.ListBucket(bucketToList, Prefix, Marker,maxNumberToList, Delimiter,as3.key,now,as3.signature('ListBucket',now),as3.secret);
		            bucketList = objectsForBucket.Contents;
		        }
	        }
	        return null;
        }catch(System.CalloutException callout) {
	        System.debug('CALLOUT EXCEPTION: ' + callout);
	        ApexPages.addMessages(callout);
	        listBucketErrorMsg =  callout.getMessage();
	        return null;  
	    }catch(Exception ex) {
	        System.debug('EXCEPTION: ' + ex);
	        listBucketErrorMsg =    ex.getMessage();
	        ApexPages.addMessages(ex);
	        return null;    
	    }
	}
	
	public PageReference createBucket(){
        try{   
	        if(as3 != null){
	        if(bucketNameToCreate!= null && bucketNameToCreate != ''){
	        createBucketErrorMsg= null;
	        Datetime now = Datetime.now();        
	        System.debug('about to create S3 bucket called: ' + bucketNameToCreate);
	           
	        //This performs the Web Service call to Amazon S3 and create a new bucket.
	        
	        S3.CreateBucketResult createBucketReslt = as3.CreateBucket(bucketNameToCreate.escapeHtml4(),null,as3.key,now,as3.signature('CreateBucket',now));
	        createdBucketName = createBucketReslt.BucketName;
	        if(BucketNames!= null && createdBucketName != null) {
	            BucketNames.add(new SelectOption(createdBucketName,createdBucketName));}
	            System.debug('Successfully created a Bucket with Name: ' + createBucketReslt.BucketName);
	            createBucketErrorMsg='Success';}
	        }
	        return null;
        }catch(System.CalloutException callout){
	        System.debug('CALLOUT EXCEPTION: ' + callout);
	        ApexPages.addMessages(callout);
	        createBucketErrorMsg = callout.getMessage();
	        return null;  
	    }catch(Exception ex){
	        System.debug(ex);
	        ApexPages.addMessages(ex);
	        createBucketErrorMsg = ex.getMessage();
	        return null; 
	    }
	}
	/*
       Method to delete a bucket on AWS S3 
    */
    public PageReference deleteBucket(){
	    try{   
            if(as3 != null) {
	            if(bucketNameToDelete != null && bucketNameToDelete!= '') {
	                deleteBucketErrorMsg= null;
	                Datetime now = Datetime.now();        
	           
	                //This performs the Web Service call to Amazon S3 and create a new bucket.
	                S3.Status deleteBucketReslt = as3.DeleteBucket(bucketNameToDelete,as3.key,now,as3.signature('DeleteBucket',now), as3.secret);
	                deleteBucketErrorMsg = deleteBucketReslt.Description;
	                
	                Integer count=0;
	                Boolean flag = false;
	                for(SelectOption so: BucketNames) {
	                    if(so.getValue().trim() == bucketNameToDelete) { 
	                        flag = true;
	                        break;
	                    }
	                    count = count +1;
	                }
	                if(flag) {
	                    BucketNames.remove(count); 
	                }
                }
            }
	        return null;
        }catch(System.CalloutException callout) {
            System.debug('CALLOUT EXCEPTION: ' + callout);
	        ApexPages.addMessages(callout);
	        deleteBucketErrorMsg = callout.getMessage();
	        return null;  
	    }catch(Exception ex){
	        System.debug(ex);
	        ApexPages.addMessages(ex);
	        deleteBucketErrorMsg = ex.getMessage();
	        return null; 
	    }
    }
	    
	public PageReference updateusers(){
	    if(as3 != null){
	        User[] UserstoUpdate = new User[]{};
	        for(User u: users2){
	            if(u.AmazonS3_Folder__c != null && u.AmazonS3_Folder__c != '') {//  = ur.theFolderName.trim();
	                UserstoUpdate.add(u);
	            }
	        }
	        if(UserstoUpdate.size()>0)
	        {
	            //system.assertEquals(UserstoUpdate,null);
	            fflib_SecurityUtils.checkFieldIsUpdateable(User.SObjectType, User.AmazonS3_Folder__c);
	            update UserstoUpdate;
	        }
	    
	    }
	    return null;
	}

	public PageReference updateCredential(){
	    try{
	    	s3Credential.Access_Key_ID__c = key;
	    	s3Credential.Secret_Access_Key__c = secret;
	    	upsert s3Credential;
	    	displayBucketandUser = false;
	    	UpdateCredentialSuccess = true;
	    }catch(Exception ex){
	    	ApexPages.addMessages(ex);
	    }
	    return null;
	}
	
	public PageReference redirectToS3Key() {
	    if(as3 != null){
	        //get the filename in urlencoded format 
	        String filename = '';  
	        if(!Test.isRunningTest()){    
	        	filename = ApexPages.currentPage().getParameters().get('filename');
	        }
	        else{
	        	filename = 'test';
	        }
	        Datetime now = DateTime.now();
	        Datetime expireson = now.AddSeconds(1200000);
	        Long Lexpires = expireson.getTime()/1000;
	        String stringtosign = 'GET\n\n\n'+Lexpires+'\n/'+bucketToList+'/'+filename;
	        String signed = make_sig(stringtosign);
	        String codedsigned = EncodingUtil.urlEncode(signed,'UTF-8');
	        if(!bucketToList.isAlphanumeric()||filename.contains('/')){
	        	throw new SecurityException();
	        }else{
	        	url = 'https://'+bucketToList+'.s3.amazonaws.com/'+filename+'?AWSAccessKeyId='+as3.key+'&Expires='+Lexpires+'&Signature='+signed;
	          	PageReference newPage = new PageReference(url);
	        	return newPage;
	        }
	    }
	    return null;
    }
	   
    public PageReference getDirectory(String filestring){
	    try{
	        ApexPages.currentPage().getParameters().put('filename',filestring) ;
	        PageReference newPage = redirectToS3Key();
	        return newPage;
	    } catch(system.exception e) {
	        system.debug(e);
	    }
	    return null;
    }
    
    public String createS3URL(String bucket, String objKey, Integer expireTime) {
        String s3Link = '';
        if (as3 != null) {
	        String filename = '';  
	        if(!Test.isRunningTest()){    
	            filename = EncodingUtil.urlEncode(objKey, 'UTF-8');
	        } else {
	            filename = 'test';
	        }
	        Datetime now = DateTime.now();
	        Datetime expire = now.AddSeconds(expireTime);
	        Long Lexpire = expire.getTime()/1000;
	        String stringtosign = 'GET\n\n\n'+Lexpire+'\n/'+bucket+'/'+filename;
	        String signed = make_sig(stringtosign);
	        String codedsigned = EncodingUtil.urlEncode(signed,'UTF-8');
	        s3Link = 'https://'+bucket+'.s3.amazonaws.com/'+filename+'?AWSAccessKeyId='+as3.key+'&Expires='+Lexpire+'&Signature='+codedsigned;
        } 
	    return s3Link;
    }   	    
    
	private String make_sig(string canonicalBuffer) {        
	    if(as3 != null) {
	        String macUrl ;
	        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(as3.secret));
	        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(as3.secret)); 
	        macUrl = EncodingUtil.base64Encode(mac);                
	        return macUrl;
	    }
	    return null;
	}
	    
    //Create dummy data for testing
	private S3.ListAllMyBucketsResult createdummydata(){
	   	s3.CanonicalUser tempCanonicalUser= new s3.CanonicalUser();
	   	tempCanonicalUser.ID = '1234567';
	   	tempCanonicalUser.DisplayName = 'test';
	    	
	   	S3.ListAllMyBucketsEntry[] tempbucketsentrylist = new S3.ListAllMyBucketsEntry[]{};
		S3.ListAllMyBucketsEntry tempbucketsentry = new S3.ListAllMyBucketsEntry();
		tempbucketsentry.Name = 'test';
		tempbucketsentry.CreationDate = datetime.now();
		tempbucketsentrylist.add(tempbucketsentry);
			
		S3.ListAllMyBucketsList tempListAllMyBucketsList = new S3.ListAllMyBucketsList();
		tempListAllMyBucketsList.Bucket = tempbucketsentrylist;
			
		S3.ListAllMyBucketsResult tempListAllMyBucketsResult =  new S3.ListAllMyBucketsResult();
		tempListAllMyBucketsResult.Buckets = tempListAllMyBucketsList;
		tempListAllMyBucketsResult.Owner = tempCanonicalUser;
		return tempListAllMyBucketsResult;
   	}
   	
}