public with sharing class SFOauthTokenService{
  private static SFOauthToken__c oauthToken;
  static{
    oauthToken = SFOauthTokenSelector.QuerySFOauthToken('oAuthToken');
  }
    private String refreshToken;
    private String clientId;
    private String clientSecret;
    
    public SFOauthTokenService() {
        SFOauthToken__c sfOauthToken = SFOauthTokenSelector.querySFOauthToken('oAuthToken');
        if(sfOauthToken!=null){
          clientId = sfOauthToken.ConsumerKey__c;
          clientSecret = sfOauthToken.SecretKey__c;
          refreshToken = sfOauthToken.RefreshToken__c;
        }
    }
    
    //Save Token to Customer Setting    
    public boolean saveTokentoCustomSetting(String responseJsonString) {
        boolean updateCustomerSettingSuccessed = true;
        String refreshToken = '';
        String accessToken = '';
        String instanceURL = '';
        
        //Get the JSON map
        map<String,Object> httpJsonObj=(Map<String, Object>)JSON.deserializeUntyped(responseJsonString);
        refreshToken = (String)httpJsonObj.get('refresh_token');
        accessToken = (String)httpJsonObj.get('access_token');
        instanceURL = (String)httpJsonObj.get('instance_url');
        
        //system.debug('refreshToken ='+ refreshToken);
        //system.debug('accessToken ='+ accessToken);
        //system.debug('instanceURL ='+ instanceURL);
        
        updateCustomerSettingSuccessed = SaveToOauthToken(accessToken,instanceURL, refreshToken);
        return updateCustomerSettingSuccessed;
    }   
    
    /*
      @param access_Token, instanceURL, refresh_Token
      @return boolean
      @action save tokens to oAuthToken customer setting
    */    
    private boolean SaveToOauthToken(String access_Token,String instanceURL, String refresh_Token){
        boolean isSuccess=false;
        SFOauthToken__c sfOauthtoken=new SFOauthToken__c();
        sfOauthtoken.Name__c='oAuthToken';
        sfOauthtoken.Name='oAuthToken';
        if(refresh_Token!= null)
          sfOauthtoken.RefreshToken__c = refresh_Token;
        sfOauthtoken.OAuthToken__c=access_Token;
        sfOauthtoken.InstanceURL__c =instanceURL;
        List<SFOauthToken__c> sfOauthtokenList = new List<SFOauthToken__c>();
        isSuccess=SFOauthTokenSelector.upsertOauthToken(sfOauthtokenList);
        return isSuccess;
    }
    
    /*
        @param
        @return HttpResponse
        @action Send "Post" Request to SF, get access token
    */
    //Use RefreshToken to get Access Token
    public HttpResponse getAccessToken() {
        //system.debug('Start get Access Token');
        HttpResponse returnHttpResponse = null;
        String redirectUri = OauthSettings.URL_CALLBACK;
            
        PageReference sfCheckOauthValidPage = new PageReference(OauthSettings.URL_REFRESH_TOKEN);
        sfCheckOauthValidPage.setRedirect(true);
        sfCheckOauthValidPage.getParameters().put('grant_type','refresh_token');
        sfCheckOauthValidPage.getParameters().put('client_id',clientId);
        sfCheckOauthValidPage.getParameters().put('client_secret',clientSecret);
        sfCheckOauthValidPage.getParameters().put('refresh_token',refreshToken);
        String url = sfCheckOauthValidPage.getUrl();
        
        OAuthRefreshTokenService oauthRfTS = new OAuthRefreshTokenService();
        //system.debug('url ='+ url);
        returnHttpResponse = oauthRfTS.getRequestByAuthorization(url);
        return returnHttpResponse;
    }
}