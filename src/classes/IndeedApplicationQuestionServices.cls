public with sharing class IndeedApplicationQuestionServices {
    public list<Application_Question__c> convertToApplicationQuestion(list<IndeedScreenQuestionFeed> indeedQuestionFeeds,String questionSetId){
        
        
        list<Application_Question__c> applicationQuestions=new list<Application_Question__c>();
        for(IndeedScreenQuestionFeed indeedQuestionFeed: indeedQuestionFeeds){
            Application_Question__c applicationQuestion=new Application_Question__c();
            applicationQuestion.Application_Question_Set__c=questionSetId;
            applicationQuestion.Questions__c=indeedQuestionFeed.getQuestion();
            applicationQuestion.name=indeedQuestionFeed.getApiname();
            applicationQuestion.Field_Label_Name__c=indeedQuestionFeed.getName();
            applicationQuestion.Required__c=indeedQuestionFeed.getIsRequired();
            applicationQuestion.Sequence__c=indeedQuestionFeed.getSequence();
            applicationQuestion.Condition_Field__c=indeedQuestionFeed.getConditionField().get('value');
            applicationQuestion.Condition_Option__c=indeedQuestionFeed.getConditionOption().get('value');
            applicationQuestions.add(applicationQuestion);
        }
        
        return applicationQuestions;
    }
    
    public list<Application_Question__c> getPreApplicationQuestions(String questionSetId){
        //No update, delete and create list records
        IndeedApplicationQuestionSelector appQuestionSelector=new IndeedApplicationQuestionSelector();
        list<Application_Question__c> preApplicationQuestions=appQuestionSelector.retreiveApplicationQuestions(questionSetId);
        return preApplicationQuestions;
    }
    public list<IndeedScreenQuestionFeed> getPrePopulateApplicationQuestions(String questionSetId){
        IndeedApplicationQuestionSelector appQuestionSelector=new IndeedApplicationQuestionSelector();
        list<IndeedScreenQuestionFeed> indeedQuestionFeeds=new list<IndeedScreenQuestionFeed>();
        list<Application_Question__c> preApplicationQuestions=appQuestionSelector.retreiveApplicationQuestions(questionSetId);
        
        for(Application_Question__c applicationQuestion: preApplicationQuestions){
            IndeedScreenQuestionFeed indeedQuestionFeed=new IndeedScreenQuestionFeed();
            indeedQuestionFeed.setApiname(applicationQuestion.name);
            indeedQuestionFeed.setQuestion(applicationQuestion.Questions__c);
            indeedQuestionFeed.setName(applicationQuestion.Field_Label_Name__c);
            indeedQuestionFeed.setIsRequired(applicationQuestion.Required__c);
            indeedQuestionFeed.setSequence(applicationQuestion.Sequence__c.intValue()); 
            map<String,String> applicationQuestionConditionMap=new map<String,String>(); 
            applicationQuestionConditionMap.put('value',applicationQuestion.Condition_Field__c);
            applicationQuestionConditionMap.put('label',applicationQuestion.Condition_Field__c);
            indeedQuestionFeed.setConditionField(applicationQuestionConditionMap);
            map<String,String> applicationQuestionOptionMap=new map<String,String>();
            applicationQuestionOptionMap.put('value',applicationQuestion.Condition_Option__c);
            applicationQuestionOptionMap.put('label',applicationQuestion.Condition_Option__c);
            indeedQuestionFeed.setConditionOption(applicationQuestionOptionMap);
            indeedQuestionFeeds.add(indeedQuestionFeed);
                
        }
        
        return indeedQuestionFeeds;
        
    }
    
    public void saveToApplicationQuestions(list<Application_Question__c> applicationQuestions){
        //check FLS
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Application_Question__c.Application_Question_Set__c,
            Application_Question__c.Questions__c,
            Application_Question__c.Name,
            Application_Question__c.Field_Label_Name__c,
            Application_Question__c.Required__c,
            Application_Question__c.Sequence__c,
            Application_Question__c.Condition_Field__c,
            Application_Question__c.Condition_Option__c
        };
        fflib_SecurityUtils.checkInsert(Application_Question__c.SObjectType, fieldList);
        insert applicationQuestions;
    }
    
    public void deletePreApplicationQuestons(list<Application_Question__c> previousApplications){
        delete previousApplications;
    }
    
    public void doAction(list<IndeedScreenQuestionFeed> indeedQuestionFeeds,String questionSetId){
        list<Application_Question__c> preApplicationQuestions=getPreApplicationQuestions(questionSetId);
        list<Application_Question__c> applicationQuestions=convertToApplicationQuestion(indeedQuestionFeeds,questionSetId);
        try{
            if(applicationQuestions.size()>0){
                saveToApplicationQuestions(applicationQuestions);
            }
            if(preApplicationQuestions.size()>0){
                deletePreApplicationQuestons(preApplicationQuestions);
            }
        }catch(System.DmlException  ex){
            
            throw ex;
            
        }
        
    }
    

}