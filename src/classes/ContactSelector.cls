public with sharing class ContactSelector extends SobjectSelector{
	
	final static String CONTACT_BASE_QUERY 	= 'SELECT Id, FirstName, LastName,Title, MobilePhone, '
											+ 'Phone, Email, MailingCity, MailingCountry,'
											+ 'MailingStreet, MailingPostalCode, MailingState, CreatedDate FROM Contact';
	final static String CONTACT_BASE_FIELDS = ' Id, FirstName, LastName,Title, MobilePhone, '
											+ 'Phone, Email, MailingCity, MailingCountry,'
											+ 'MailingStreet, MailingPostalCode, MailingState, CreatedDate ';
	final static String CANDIDATE_SEARCH_GEOCODE_STRING = 'Select Id from Contact';
	final static String CONTACT_LONGTEXT_FIELDS = 'SELECT Id, Resume__c, Candidate_Summary__c from Contact';
	final static String CONDITION_SEPARATOR = ' AND ';
	
	//Added By Tony
	//Constructor for SObject selector
	public ContactSelector() {
		
	    //super('Contact', null);
	    super('Contact', new Set<String>(CommonSelector.convertStringToList(CONTACT_BASE_FIELDS)));
	}
	
	public ContactSelector(Set<String> fns) {
	    super('Contact', fns);
	}
	
	public List<Contact> getContacts(List<Id> conIds){
		CommonSelector.checkRead(Contact.sObjectType, 'Id, Resume__c, Resume_Source__c');
		List<Contact> conList = [Select Id, Resume__c, Resume_Source__c From Contact Where Id IN :conIds Limit 200];
		return conList;
	}

	public List<Contact> fetchContactInfoById(String Id){
		String query = CONTACT_BASE_QUERY + ' WHERE Id =: Id';
		List<Contact> conList =	new List<Contact>();
		CommonSelector.checkRead(Contact.sObjectType, CONTACT_BASE_FIELDS);
		conList = Database.query(query);
		return conList;
	}

	public List<Contact> fetchContactListByNameKeyword(String nameKeyword){
		String likeArg = '%'+nameKeyword+'%';
		String query = CONTACT_BASE_QUERY + ' WHERE FirstName LIKE: likeArg OR LastName LIKE: likeArg OR Name LIKE: likeArg';
		List<Contact> contList = new List<Contact>();

		CommonSelector.checkRead(Contact.sObjectType, CONTACT_BASE_FIELDS);
		contList = Database.query(query);
		return contList; 
	}
	
	//Search candidates by search criteria
	public List<Contact> getCandidatesByCriteria(Map<String, String> fieldsOperator, Map<String, String> fields, 
														Map<String, String> fieldsType) {
		List<Contact> candidatesList = new List<Contact>();
		String queryString = generateSearchFieldsQueryString(fieldsOperator, fields, fieldsType);
		//system.debug('queryString ='+ queryString);
		
		CommonSelector.checkRead(Contact.sObjectType, CONTACT_BASE_FIELDS);
		candidatesList = Database.query(queryString);
		return candidatesList;
	}

	//Generate search query
	private String generateSearchFieldsQueryString(Map<String, String> fieldsOperator, Map<String, String> fields, Map<String, String> fieldsType) {
		String fieldsQueryString = '';
		String queryString = '';

		for(String fieldAPIName : fieldsOperator.keySet()) {
			String fieldOperator = fieldsOperator.get(fieldAPIName);
			if(fieldOperator != null && fieldOperator != '') {
				String fieldValue = fields.get(fieldAPIName);
				if(fieldValue == null) 
					fieldValue = '';
				String fieldType = fieldsType.get(fieldAPIName);
				if(!(fieldType.equalsIgnoreCase('boolean')&&(fieldValue==null || fieldValue == '' ))){
					PeopleSearchOperatorFactory.Operator op = PeopleSearchOperatorFactory.getOperatorsMap().get(fieldOperator);
					if(op != null){
						fieldsQueryString += op.generateCondition(fieldAPIName, fieldValue, fieldType) + CONDITION_SEPARATOR;
					}
				}
				
				/*if(fieldsOperator.get(fieldAPIName).equalsignorecase('like')) {
					fieldValue = '%'+fieldValue+'%';
				}

				if(fieldsType.get(fieldAPIName).equalsignorecase('picklist')) {        //If field type is picklist, split the value string
					List<String> fieldValueList = fieldValue.split(',');
					String picklistString = '';
					for(String value : fieldValueList) {
						picklistString	= picklistString != '' ? picklistString + ' or ' + fieldAPIName + fieldsOperator.get(fieldAPIName) +' \''+ value +'\''
								: fieldAPIName + fieldsOperator.get(fieldAPIName) +' \''+ value +'\'';
					}
					if(picklistString!= '')
						fieldsQueryString = fieldsQueryString != '' ? fieldsQueryString+' and (' + picklistString +')': '('+picklistString+')';
				}
				else if(fieldsType.get(fieldAPIName).equalsignorecase('boolean') 
							|| fieldsType.get(fieldAPIName).equalsignorecase('date')){ //If field type is boolean or date, add the value directily
					fieldsQueryString = fieldsQueryString != '' ? fieldsQueryString+' and ' 
									+fieldAPIName+fieldsOperator.get(fieldAPIName)+fieldValue : fieldAPIName+fieldsOperator.get(fieldAPIName)+fieldValue ;
				}else if(fieldsType.get(fieldAPIName).equalsignorecase('datetime')) {  //If field type is datetime, convert date to datetime
					fieldValue = convertDateToDatetimeString(fieldValue);
					fieldsQueryString = fieldsQueryString != '' ? fieldsQueryString+' and ' 
									+fieldAPIName+' '+fieldsOperator.get(fieldAPIName)+' '+fieldValue : fieldAPIName+' '+fieldsOperator.get(fieldAPIName)+' '+fieldValue ;
				}else{																   //else, add value with quote
					fieldsQueryString = fieldsQueryString != '' ? fieldsQueryString+' and ' 
									+fieldAPIName+' '+fieldsOperator.get(fieldAPIName)+' \''+fieldValue+'\'' : fieldAPIName+' '+fieldsOperator.get(fieldAPIName)+' \''+fieldValue+'\'';					
				}*/
				
			}
		}
		if(fieldsQueryString!='') {
			fieldsQueryString = fieldsQueryString.removeEnd(CONDITION_SEPARATOR);
			queryString = CONTACT_BASE_QUERY + ' where ' + fieldsQueryString + ' Order By LastModifiedDate Limit 1000';
		}else {
			queryString = CONTACT_BASE_QUERY + ' Order By LastModifiedDate Limit 1000';
		}
		return queryString;
	}

	public Map<String, candidateDTO> candidateRadiusSelector(Map<String, candidateDTO> canDTOMap, Integer radiusRange, Decimal locationLatitude, Decimal locaitonLongitude) {
		Set<String> candidateSet = canDTOMap.keySet();
		Map<String, Contact> contactsINTheRange = new Map<String, Contact>();

		String contactINTheRangequeryString = CANDIDATE_SEARCH_GEOCODE_STRING + ' WHERE DISTANCE(MailingAddress, GEOLOCATION('+locationLatitude + ',' + locaitonLongitude + '), \'km\') < '+
																radiusRange + ' And Id in: candidateSet';
		CommonSelector.checkRead(Contact.sObjectType, 'Id');
		if(Test.isRunningTest()) {
			
		    contactsINTheRange = new Map<String, Contact> ([Select Id from Contact limit 1]);
		} else {
			
		    contactsINTheRange = new Map<String, Contact>((List<Contact>)database.query(contactINTheRangequeryString));
		}
		
		for(String candidateId: contactsINTheRange.keySet()) {
			if(canDTOMap.containsKey(candidateId)) {
				candidateDTO cDTO = canDTOMap.get(candidateId);
				cDTO.setRadiusRanking(1);
				canDTOMap.put(candidateId,cDTO);
			}
		}
		return canDTOMap;
	}

	private String convertDateToDatetimeString(String dateString) {
		Date inputDate = date.parse(datestring);
		Datetime inputDatetime = WizardUtils.convertDateToDateTime(inputDate);
		String datetimeString = inputDatetime.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
		return datetimeString;
	}
	
	//Added By Tony
	//Cast SObject List to Contact List
	public List<Contact> getContactsByIds(Set<Id> ids) {
	    return (List<Contact>) selectSObjectByIds(ids);
	}
	
	public List<Contact> getContactsByIds(List<Id> ids) {
	    return (List<Contact>) selectSObjectByIds(new Set<id>(ids));
	}
	
	public Map<Id, Contact> getContactsMapByIds(Set<Id> ids) {
	    return new Map<Id, Contact>(getContactsByIds(ids));
	}
	
	public Map<Id, Contact> getContactsMapByIds(List<Id> ids) {
	    return new Map<Id, Contact>(getContactsByIds(new Set<id>(ids)));
	}
	
	public List<Contact> getSObjectByLimit(Integer limitCount, Integer offset) {
	    return (List<Contact>) selectSObjectByLimit(offset, limitCount);
	}
	
	//Added By Lina
	// return contact to use in LinkedInProfileController class
	public Contact getContactForLinkedIn(String Id) {
		
		
	    String queryString = 'SELECT Id, FirstName, LastName, Title, Phone, MobilePhone, LinkedIn_Member_Id__c FROM Contact WHERE Id =: Id';
	    CommonSelector.checkRead(Contact.sObjectType, 'Id, FirstName, LastName, Title, Phone, MobilePhone, LinkedIn_Member_Id__c');
	    Contact con = Database.query(queryString);
		return con;
	}
	
	// Return a map of <contactId, Lastname>
	public static Map<Id, String> getCandLastNameMap(List<Id> conIdList) {
	    map<Id, String> candLastName = new Map<Id, String>();
	    String queryString = 'SELECT Id, LastName FROM Contact WHERE Id in: conIdList';
	    
	    CommonSelector.checkRead(Contact.sObjectType, 'Id, LastName');
	    List<Contact> conList = Database.query(queryString);
	    for (Id conId : conIdList) {
	        for (Contact con : conList) {
	            if (con.Id == conId) {
	                candLastName.put(conId, con.LastName);
	            }
	        }
	    }
		return candLastName;
	}

	public Contact getContactLongTextFieldsById(String Id) {
		String condition = ' WHERE Id=: Id';
		String queryString = CONTACT_LONGTEXT_FIELDS + condition;
		
		CommonSelector.checkRead(Contact.sObjectType, 'Id, Resume__c, Candidate_Summary__c');
		Contact con = Database.query(queryString);
		return con;
	}

}