@isTest
public with sharing class TestJobPostingAdminController {
	 public static testMethod void testController(){
	 	System.runAs(DummyRecordCreator.platformUser) {
	     Test.startTest();
        JobPostingAdminController thecontroller = new JobPostingAdminController();
        system.assert(thecontroller.is_Display);
        PageReference noaction = thecontroller.nonaction();
        system.assert(noaction == null);
        thecontroller.init();
        system.assert(thecontroller.getUseroptions().size()>0);
        thecontroller.getUseroptions();
        if(thecontroller.user_map != null)
        {
            if(thecontroller.user_map.size()>0)
            {
                thecontroller.selectedUser = thecontroller.user_map.values()[0].Id;
                thecontroller.selectedUser2 = thecontroller.selectedUser ;
            }
        }
        thecontroller.dosave();
        system.assert(thecontroller.is_Successful);
        PageReference changeruser = thecontroller.changeuser();
        system.assert(changeruser == null);
        thecontroller.dosubmit();
		system.assert(thecontroller.is_Successful2);
        thecontroller.selectedUser = 'all';
        thecontroller.dosave();
        system.assert(thecontroller.is_Successful);
        thecontroller.selectedUser2 = 'all';
        thecontroller.changeuser();
        thecontroller.dosubmit();
        system.assert(thecontroller.is_Successful2);
        thecontroller.selectedUser2 = '00';
        thecontroller.dosubmit();
        system.assert(!thecontroller.is_Successful2);
        Test.stopTest();
	 	}
    }
}