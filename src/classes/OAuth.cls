//***********************************************************************************************************
//***************************************OAuth Class get the authoration from Linkedin***********************/

public class OAuth {

    private OAuth_Service__c service;
    private String token;
    private String tokenSecret;
    private Boolean isAccess = false;
    private String verifier;

    private String nonce;
    private String timestamp;
    private String signature;
    private String consumerKey;
    private String consumerSecret;

    private Map<String,String> parameters = new Map<String,String>();
    
    public String message { get; set; }

    public String callbackUrl {get; set; }
    
    public String header {get;set;}
    
    public void setConsumerKey(String value) { consumerKey = value; }
    public void setConsumerSecret(String value) { consumerSecret = value; }

    /**
     * Looks up service name and starts a new authorization process
     * returns the authorization URL that the user should be redirected to
     * If null is returned, the request failed. The message property will contain
     * the reason.
     */ 
    public String newAuthorization(String search,String oauthConfigName) {//
    	
    	CommonSelector selector = new CommonSelector(OAuth_Service__c.sObjectType);
    	try{
    		if(Test.isRunningTest()){
	        	//commented by andy for security review II
	            //service = [SELECT Request_Token_URL__c, Access_Token_URL__c, Consumer_Key__c, 
	            //              Consumer_Secret__c, Authorization_URL__c,
	            //              (select token__c, secret__c, isAccess__c FROM tokens__r WHERE Owner__c=:UserInfo.getUserId() ) 
	            //              FROM OAuth_Service__c WHERE name ='test1234'];
	           
	            //added by andy fro security review II    
	             service = (OAuth_Service__c) selector
	            	.addSubselect(OAuth_Token__c.sObjectType, 'Id, isAccess__c', 'Owner__c=:userId')
	            	.setParam('userId',UserInfo.getUserId())  
	            	.getOne('Id,Request_Token_URL__c, Access_Token_URL__c, Authorization_URL__c', 
	            			'name =\'test1234\''
	            		);
	            		
	           
	            
	        }
	        else{//oauthservice name is linkedin
	        	//commented by andy for security review II
	            //service = [SELECT Request_Token_URL__c, Access_Token_URL__c, Consumer_Key__c, 
	            //              Consumer_Secret__c, Authorization_URL__c,
	            //              (select token__c, secret__c, isAccess__c FROM tokens__r WHERE Owner__c=:UserInfo.getUserId() ) 
	            //              FROM OAuth_Service__c WHERE name =:oauthConfigName];
	            
	            //added by andy fro security review II     
	            service = (OAuth_Service__c)  selector
	            	.addSubselect(OAuth_Token__c.sObjectType, 'Id, isAccess__c', 'Owner__c=:userId')
	            	.setParam('userId',UserInfo.getUserId())  
	            	.setParam('oauthConfigName',oauthConfigName)
	            	.getOne('Id,Request_Token_URL__c, Access_Token_URL__c, Authorization_URL__c', 
	            			'name = :oauthConfigName and Active__c = true'
	            		);         
	           
	                          
	        }
    	}catch(QueryException e){
    		System.debug('Couldn\'t find Oauth Service linkedin!' + e);
            message = 'Service linkedin was not found in the local configuration';
            return null;
    	}
        
        if(service==null) {
            System.debug('Couldn\'t find Oauth Service linkedin!');
            message = 'Service linkedin was not found in the local configuration';
            return null;
        }

        if(callbackUrl==null) { //callbackUrl is null
            if(ApexPages.currentPage()==null || ApexPages.currentPage().getHeaders().get('Host')==null) {
                message = 'No callback page was set and it couldn\'t be generated from Apex context';
                System.debug(message);
                return null;
            }
    
            //callbackUrl = PeopleCloudHelper.buildLightningUrl('https://'+ApexPages.currentPage().getHeaders().get('Host'), Page.CompleteAuth.getUrl() + '?returl='+ EncodingUtil.urlEncode(search,'UTF-8') );
            callbackUrl = 'https://'+ApexPages.currentPage().getHeaders().get('Host') + Page.CompleteAuth.getUrl() + '?returl='+ EncodingUtil.urlEncode(search,'UTF-8') ;
            
            //System.debug(callbackUrl);
           
            callbackUrl = EncodingUtil.urlEncode( callbackUrl,'UTF-8');//callback url with the contact ID
            //system.debug('full callbackurl:'+callbackUrl);                                                 
        }
                
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(service.Request_Token_URL__c);
        //System.debug('Request body set to: '+req.getBody());
        //commented by andy for security review II
        //consumerKey = service.Consumer_Key__c;
        //consumerSecret = service.Consumer_Secret__c;
        
        //added by andy fro security review II   
        ProtectedCredential pc = CredentialSettingService.getCredential(service.Id);
        if(pc != null){
        	consumerKey = pc.key;
        	consumerSecret = pc.secret;
        }else{
        	System.debug('Can not find the OAuthservice credential !');
        	return null;
        }
        
        try{
            sign(req);
        }
        catch(Exception e){
           NotificationClass.notifyErr2Dev('OAuth/newAuthorization', e);
           return null;
        }
        HttpResponse res = null;
        if(Test.isRunningTest()) {
            // testing
            res = new HttpResponse();
        } else {
            res = h.send(req);
        }
        //System.debug('Response from request token request: ('+res.getStatusCode()+')'+res.getBody());
        if(res.getStatusCode()>299) {
            message = 'Failed getting a request token. HTTP Code = '+res.getStatusCode()+
                      '. Message: '+res.getStatus()+'. Response Body: '+res.getBody();
            return null;
        }
        String resParams = res.getBody();
        //System.debug('aaaaaaaaaaaaaa'+res.getBody());   
        Map<String,String> rp = getUrlParams(resParams);//get the detail from the body
        OAuth_Token__c t = new OAuth_Token__c();
        t.Owner__c = UserInfo.getUserId();
        t.OAuth_Service__c = service.id;
        
        //commented by andy for security review II
        //t.token__c = rp.get('oauth_token');
        //t.secret__c = rp.get('oauth_token_secret');

        t.isAccess__c = false;
        
        
        try{
            OAuth_Token__c delT = service.tokens__r;
            //
            if(delT != null){
                CredentialSettingService.deleteProtectedAccount(delT);
                //CommonSelector.quickDelete(service.tokens__r);
                //delete service.tokens__r;
            }
        }catch(Exception e){
         	System.debug(e);   
        }
        
        
        
        //check FLS
        /**
        * commented by andy for security review II
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                OAuth_Token__c.Owner__c,
                OAuth_Token__c.OAuth_Service__c,
                //commented by andy for security review II
                //OAuth_Token__c.Token__c,
                //OAuth_Token__c.Secret__c,
                OAuth_Token__c.IsAccess__c
        };
        fflib_SecurityUtils.checkInsert(OAuth_Token__c.SObjectType, fieldList);
        insert t;
        */
        
        CommonSelector.quickInsert(t);
        
        //added by andy for security review II
        String token = rp.get('oauth_token');
        String secret = rp.get('oauth_token_secret');
        CredentialSettingService.upsertLinkedInOAuthToken(t.Id,secret,token);
        
        //System.debug('Got request token: '+t.token__c+'('+rp.get('oauth_token')+')');
        
        if(Test.isRunningTest()){
            return service.Authorization_URL__c;
        }
        else{
            if(service.Authorization_URL__c.contains('?')) {
            	//modified by andy for security review II
                //return service.Authorization_URL__c+'&oauth_token='+EncodingUtil.urlDecode(t.token__c,'UTF-8')+'&oauth_consumer_key='+service.Consumer_Key__c;
                return service.Authorization_URL__c+'&oauth_token='+EncodingUtil.urlDecode(token,'UTF-8')+'&oauth_consumer_key='+pc.key;
            } else {
            	//modified by andy for security review II
                //return service.Authorization_URL__c+'?oauth_token='+EncodingUtil.urlDecode(t.token__c,'UTF-8')+'&oauth_consumer_key='+service.Consumer_Key__c;
                return service.Authorization_URL__c+'?oauth_token='+EncodingUtil.urlDecode(token,'UTF-8')+'&oauth_consumer_key='+pc.key;
            }
        }
    }
    
    public boolean completeAuthorization(String token, String verifier) {
        //System.debug('Completing authorization for request token '+token+' with verifier '+verifier);
        OAuth_Token__c t = null;
        
        
        
        CommonSelector selector = new CommonSelector(OAuth_Token__c.sObjectType);
        
        try {
            if(Test.isRunningTest()){
            	//modified by andy for security review II
	            //t = [SELECT OAuth_Service__r.name, OAuth_Service__r.Access_Token_URL__c, OAuth_Service__r.Consumer_Key__c, 
	            // OAuth_Service__r.Consumer_Secret__c, token__c, secret__c, isAccess__c FROM OAuth_Token__c 
	            // WHERE Owner__c=:UserInfo.getUserId() AND isAccess__c = false];
	             t = (OAuth_Token__c)  selector
	             			.setParam('userId', UserInfo.getUserId())
	             			.getOne('OAuth_Service__r.Id, OAuth_Service__r.name, OAuth_Service__r.Access_Token_URL__c,Id, isAccess__c'
	             				,'Owner__c=:userId AND isAccess__c = false'
	             				);
	            
	             
             
            }
            else{
            	//modified by andy for security review II
	            //t = [SELECT OAuth_Service__r.name, OAuth_Service__r.Access_Token_URL__c, OAuth_Service__r.Consumer_Key__c, 
	            // OAuth_Service__r.Consumer_Secret__c, token__c, secret__c, isAccess__c FROM OAuth_Token__c 
	            // WHERE Owner__c=:UserInfo.getUserId() AND token__c = :EncodingUtil.urlEncode(token,'UTF-8') AND isAccess__c = false];
	             
	             String tokenId = '';
	             ProtectedCredential tokenpc = CredentialSettingService.getLinkedInOAuthTokenByToken( EncodingUtil.urlEncode(token,'UTF-8') );
	             if(tokenpc != null){
	             	tokenId = tokenpc.Name;
	             }
	             
	              t = (OAuth_Token__c) selector
	             			.setParam('userId', UserInfo.getUserId())
	             			.setParam('tokenId',tokenId)
	             			.getOne('OAuth_Service__r.Id, OAuth_Service__r.name, OAuth_Service__r.Access_Token_URL__c,Id, isAccess__c'
	             				,'Owner__c=:userId AND Id = :tokenId AND isAccess__c = false'
	             				);
	             
	             
	             
            }
        } catch(System.QueryException e) {

            message = 'Unknown request token: '+token+'. Restart authorization process' + e;
            System.debug(message);
            return false;
            
        }
        if(t == null){
        	message = 'Unknown request token: '+token+'. Restart authorization process';
            System.debug(message);
            return false;
        }

        service = t.OAuth_Service__r;
        
        //System.debug('service id = ' + service.Id);
        //modified by andy for security review II
        //consumerKey = service.Consumer_Key__c;
        //consumerSecret = service.Consumer_Secret__c;
        ProtectedCredential servicepc = CredentialSettingService.getCredential(service.Id);
        //System.debug(servicepc);
        if(servicepc != null){
        	consumerKey = servicepc.key;
        	consumerSecret = servicepc.secret;
        }else{
        	message = 'Can not find the OAuthService credential!';
        	System.debug(message);
        	return false;
        }

        ProtectedCredential tokenpc = CredentialSettingService.getCredential(t.Id);
        //System.debug(tokenpc);
        if( tokenpc != null ){
        	this.token = tokenpc.token;
        	tokenSecret = tokenpc.secret;
        }
        
        
        //
        if(verifier!=null) {
            this.verifier = EncodingUtil.urlEncode(verifier,'UTF-8');
            //System.debug(this.verifier);
        }
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(service.Access_Token_URL__c);
        req.setBody('');
        sign(req);
        HttpResponse res = null;
        if(service.name=='test1234') {
            res = new HttpResponse();
        } else {
            res = h.send(req);
            //System.debug('Response from request token request: ('+res.getStatusCode()+')'+res.getBody());
        }
        if(res.getStatusCode()>299) {
            message = 'Failed getting an access token. HTTP Code = '+res.getStatusCode()+'. Message: '+res.getStatus()+'. Response Body: '+res.getBody();
            return false;
        }

        String resParams = service.name == 'test1234' ? 
            'oauth_token=token&oauth_token_secret=token_secret' : res.getBody();

        Map<String,String> rp = new Map<String,String>();
        for(String s : resParams.split('&')) {
            List<String> kv = s.split('=');
            rp.put(kv[0],kv[1]);
           // System.debug('Access token response param: '+kv[0]+'='+kv[1]);
        }
        
        //commented by andy for security reivew II
        //t.token__c = rp.get('oauth_token');
        //t.secret__c = rp.get('oauth_token_secret');
        t.isAccess__c = true;
        
        system.debug('End oauth.completeAuth');
        //added by andy for security review II
        return selector.doUpdate(t,'Owner__c,IsAccess__c') && CredentialSettingService.upsertLinkedInOAuthToken(t.Id,rp.get('oauth_token_secret'),rp.get('oauth_token')) != null;
        
    }
    
    public List<User> getUsersOfService(String serviceName) {
    	
    	//modified by andy for security review II
        List<OAuth_Token__c> l = new CommonSelector(OAuth_Token__c.sObjectType)
        								.setParam('serviceName', serviceName)
        								.get('OAuth_Service__r.name, isAccess__c, Owner__r.name','OAuth_Service__r.name= :serviceName AND isAccess__c = true');
         
           // [SELECT OAuth_Service__r.name, isAccess__c, Owner__r.name FROM OAuth_Token__c 
           //  WHERE OAuth_Service__r.name= :serviceName AND isAccess__c = true];
             
             
        List<User> result = new List<User>();
        for(OAuth_Token__c t : l) {
            result.add(t.owner__r);
        }
        return result;
    }

    public boolean setService(String serviceName) {
        return setService(serviceName,UserInfo.getUserId());
    }

    public boolean setService(String serviceName, ID userId) {
        OAuth_Token__c t = null;
       
        	
    	//modified by andy for security review II
        //t =
        //[SELECT OAuth_Service__r.name, OAuth_Service__r.Consumer_Key__c, OAuth_Service__r.Consumer_Secret__c, 
        //token__c, secret__c, isAccess__c FROM OAuth_Token__c 
        //WHERE OAuth_Service__r.name= :serviceName AND Owner__c=:userId AND isAccess__c = true];
         try{
         	t = (OAuth_Token__c) new CommonSelector(OAuth_Token__c.sObjectType)
         				.setParam('serviceName', serviceName)
         				.setParam('userId', userId)
         				.getOne('OAuth_Service__r.Id,OAuth_Service__r.name, Id, isAccess__c',
         					' OAuth_Service__r.name= :serviceName AND Owner__c=:userId AND isAccess__c = true'
         					);
	         
         }catch(QueryException e){
         	System.debug(e);
         }
        
        if(t==null){
	         
         	message = 'User '+UserInfo.getUserName()+' did not authorize access to '+serviceName+'. Redirect user to authorization page. ';
        	return false;
         }
	           
        
        service = t.OAuth_Service__r;
        
        System.debug('Preparing OAuth request to service '+service.name);
        
        //modified by andy for security review II   
        //consumerKey = service.Consumer_Key__c;
        //consumerSecret = service.Consumer_Secret__c;
        ProtectedCredential pc = CredentialSettingService.getCredential(service.Id);
        if(pc != null ){
        	consumerKey = pc.key;
        	consumerSecret = pc.secret;
        }else{
        	message = 'Can not find the OAuthService credential !';
        	System.debug(message);
        	return false;
        }
        
        //modified by andy for security review II
        //this.token = t.token__c;
        //tokenSecret = t.secret__c;      
        ProtectedCredential tokenpc = CredentialSettingService.getCredential(t.Id);
        if(tokenpc != null){
        	this.token = tokenpc.token;
        	tokenSecret = tokenpc.secret;  
        }else{
        	message  = 'Can not find the OAuthToken credential!';
        	return false;
        }
        return true;
    }
        
    private void refreshParameters() {
        parameters.clear();
        //system.debug('callbackUrl ='+ callbackUrl);
        parameters.put('oauth_callback',callbackUrl);
        parameters.put('oauth_version','1.0');        
        if(token!=null) {
            parameters.put('oauth_token',token);
            //System.debug('kkkkkkkkkkkkkkk'+token);
        }
        if(verifier!=null) {
            parameters.put('oauth_verifier',verifier);
        }
        parameters.put('oauth_signature_method','HMAC-SHA1');
        parameters.put('oauth_timestamp',timestamp);
        parameters.put('oauth_nonce',nonce);
        parameters.put('oauth_consumer_key',consumerKey);
        
        
    }

    private Map<String,String> getUrlParams(String value) {
        
        Map<String,String> res = new Map<String,String>();
        if(value==null || value=='') {
            return res;
        }
        for(String s : value.split('&')) {
            //System.debug('getUrlParams: '+s);
            List<String> kv = s.split('=');
            if(kv.size()>1) {
                //System.debug('getUrlParams:  -> '+kv[0]+','+kv[1]);
                res.put(kv[0],kv[1]);
            }
        }
        return res;
    }

    private String createBaseString(Map<String,String> oauthParams, HttpRequest req) {
        
        Map<String,String> p = oauthParams.clone();
        if(req.getMethod().equalsIgnoreCase('post') && req.getBody()!=null && 
           req.getHeader('Content-Type')=='application/x-www-form-urlencoded') {
            p.putAll(getUrlParams(req.getBody()));
                    }
        String host = req.getEndpoint();
        Integer n = host.indexOf('?');
        //System.debug(host);

        if(n>-1) {
            p.putAll(getUrlParams(host.substring(n+1)));
            host = host.substring(0,n);
        }
        List<String> keys = new List<String>();
        keys.addAll(p.keySet());
        keys.sort();
        //system.debug('keys ='+ keys);
        String s = keys.get(0)+'='+p.get(keys.get(0));
        for(Integer i=1;i<keys.size();i++) {
            s = s + '&' + keys.get(i)+'='+p.get(keys.get(i));            
        }

        // According to OAuth spec, host string should be lowercased, but Google and LinkedIn
        // both expect that case is preserved.
        //system.debug('host ='+ host);
        //system.debug('s ='+ s);
        //system.debug('test ='+ req.getMethod().toUpperCase()+ '&' + 
        //    EncodingUtil.urlEncode(host, 'UTF-8') + '&' +            
        //    EncodingUtil.urlEncode(s, 'UTF-8'));
        return req.getMethod().toUpperCase()+ '&' + 
            EncodingUtil.urlEncode(host, 'UTF-8') + '&' +            
            EncodingUtil.urlEncode(s, 'UTF-8');
    }
    
    public void sign(HttpRequest req) {
        
        nonce = String.valueOf(Crypto.getRandomLong());
        timestamp = String.valueOf(DateTime.now().getTime()/1000);

        refreshParameters();
        
        String s = createBaseString(parameters, req); 
        //system.debug(('s ='+ s));       
        //System.debug('Signature base string: '+consumerSecret+'&'+
        //                            (tokenSecret!=null ? tokenSecret : ''));
        
        Blob sig = Crypto.generateMac('HmacSHA1', Blob.valueOf(s), 
                       Blob.valueOf(consumerSecret+'&'+
                                    (tokenSecret!=null ? tokenSecret : '')));
        signature = EncodingUtil.urlEncode(EncodingUtil.base64encode(sig), 'UTF-8');
        //signature = '9vIaLxTEOGdTy2ohYl%2FZ2gSYdBY%3D';
        header = 'OAuth ';
        for (String key : parameters.keySet()) {
            header = header + key + '="'+parameters.get(key)+'", ';
        }
        
        header = header + 'oauth_signature="'+signature+'"';
        //System.debug('Authorization: '+header);
        req.setHeader('Authorization',header);
        
    } 
    static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){//Send error message
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());    
    }  
}