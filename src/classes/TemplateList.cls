/*

	Get all template list.

*/

public with sharing class TemplateList {
    public String tid = null; 
    public List<StyleCategory__c> allTemplates = new List<StyleCategory__c>();
    
    //construtor:  get template style record type id
    public TemplateList(){
        show_new =false;
        tid = DaoRecordType.adTemplateRT.id;
    }
    
    // get all the record of record type which is 'templateStyle'
    public List<StyleCategory__c> allExistingRecords(){
    	if(tid !=null){
    		
    		CommonSelector.checkRead(StyleCategory__c.sObjectType, 'templateSeek__c, Name, templateMYC__c, templateC1__c,templateJXT__c, templateJXTNZ__c, templateActive__c,screenId__c, LogoId__c ');
           	List<StyleCategory__c> setCon = [Select p.templateSeek__c, 
            p.Name, p.templateMYC__c, p.templateC1__c, p.templateJXT__c, p.templateJXTNZ__c, 
            p.templateActive__c,p.screenId__c, p.LogoId__c 
            From StyleCategory__c p where p.RecordTypeId = :tid];
            return setCon ;
        }
        return null;
    }
       
    public List<StyleCategory__c> getAllTemplates() {
       allTemplates = allExistingRecords() ;
       return allTemplates;
      
    }
    
    public void setAllTemplates(List<StyleCategory__c> a){
        allTemplates = a;
    
    }
    
    public void updateTemplates(){
    	if(allTemplates != null){
        	if(allTemplates.size() >0){
                //check FLS
                List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                    StyleCategory__c.templateSeek__c,
                    StyleCategory__c.Name,
                    StyleCategory__c.templateMYC__c,
                    StyleCategory__c.templateC1__c,
                    StyleCategory__c.templateJXT__c,
                    StyleCategory__c.templateJXTNZ__c,
                    StyleCategory__c.templateActive__c,
                    StyleCategory__c.screenId__c,
                    StyleCategory__c.LogoId__c
                };
                fflib_SecurityUtils.checkUpdate(StyleCategory__c.SObjectType, fieldList);
                update allTemplates;
                err2 ='Updated Successfully!';
            }
        }
    
    }
    
    public PageReference newTemplate(){
        show_new = true;
        return null;
    }
    
    public boolean show_new{get;set;}
    public String err{get;set;}
    public String err2{get;set;}
    StyleCategory__c sc = new StyleCategory__c();
    public StyleCategory__c getSc(){
        return sc;
    
    }
    public void setSc(StyleCategory__c s){
        sc = s;    
    }
    
    public void save(){
       try{
            //system.debug('scname = ' + sc.Name);
            if(tid != null && sc.Name != null && sc.Name !='')
            {
                
                sc.RecordTypeId = tid;
                sc.templateActive__c = true;
                //Check FLS
                List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                    StyleCategory__c.Name,
                    StyleCategory__c.RecordTypeId,
                    StyleCategory__c.TemplateActive__c
                };
                fflib_SecurityUtils.checkInsert(StyleCategory__c.SObjectType, fieldList);
                insert sc;
                err ='Created Successfully!';
                allExistingRecords();
            }
            else
            {
                if(sc.Name == null || sc.Name == '')
                {
                    err ='Please fill in the required fields - marked in red!';
                
                }
            }
        }
        catch(system.Exception e)
        {
            err ='Created Unsuccessfully!';
            system.debug(e);
        }
    }
}