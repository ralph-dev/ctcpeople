global with sharing class AvailabilityRequestHandler {
    //static Contact candidate;
    private String candidateId;
	public static String candidateManagementId{get;set;}
	public static String calendarMode{get;set;}
	public Contact candidate{get;set;}
    
    final static String SUCCESS_STATUS = 'success';
    
    final static String DELETE_ERROR = 'error';
    final static String DELETE_ERROR_DESC = 'The record does not exist.';
    
    //public AvailabilityRequestHandler(ApexPages.StandardController stdController){
    //	candidate = (Contact)stdController.getRecord(); 	
    //}
    
    public AvailabilityRequestHandler(){
    	candidateId = ApexPages.currentPage().getParameters().get('canId');
    	candidateManagementId = ApexPages.currentPage().getParameters().get('cmId');
    	calendarMode = ApexPages.currentPage().getParameters().get('calendarMode');
    	try{
    		CommonSelector.checkRead(Contact.sObjectType, 'Id, FirstName, LastName');
    		candidate = [select Id, FirstName, LastName from Contact where Id =: candidateId];
    	}catch(Exception e){
    		// In case of no contact is found initialize with a empty contact record
    		candidate = new Contact(); 
    	}
    	
    	
    	System.debug('AvailabilityRequestHandler ------- candidateId:' + candidateId);
    	System.debug('AvailabilityRequestHandler ------- candidateManagementId:' + candidateManagementId);	 	
    }
    
    @RemoteAction 
    global static String createAvlRecords(String avlJsonString, String displayRangeJsonString, boolean isDupCheckEnabled){
        //System.debug('AvailabilityRequestHandler---avlJsonString:' + avlJsonString);
        System.debug('AvailabilityRequestHandler---isDupCheckEnabled:' + isDupCheckEnabled);
        
        // convert the json string to objects
        List<AvailabilityEntity> avlList = new List<AvailabilityEntity>();
        avlList = AvailabilityParser.convertJsonStringToObjects(avlJsonString);
        //System.debug('AvailabilityRequestHandler----- createAvlRecords ----avlList:' + avlList);
  
   
  		// get all related records 
  		List<Days_Unavailable__c> relatedSfAvlList = AvailabilitySelector.getAllRelatedAvlRecsByCanIdNDateRange(avlList);
  		//System.debug('AvailabilityRequestHandler----createAvlRecords ---relatedSfAvlList:' + relatedSfAvlList);
   	
  		// check duplicate
  		if(isDupCheckEnabled){
  			String checkDuplicateResult = AvailabilityService.checkAvlDupRecords(avlList, relatedSfAvlList);
  			//System.debug('AvailabilityRequestHandler--- createAvlRecords -----checkDuplicateResult:' + checkDuplicateResult);
  			if(checkDuplicateResult != null && checkDuplicateResult != ''){
  				return checkDuplicateResult;	
  			}	
  		}

        // resolve conflicts and create new avl records
        String createResult = AvailabilityService.createNewAvlRecords(avlList, relatedSfAvlList, isDupCheckEnabled);
        if(createResult != null && createResult!= ''){
        	return createResult;
        }
        
        //return records to display
        return getAvlRecords4Display(displayRangeJsonString);
    }
    
    @RemoteAction 
    global static String deleteAvlRecord(String avlId,  String displayRangeJsonString){
        Days_Unavailable__c avl2Delete = AvailabilitySelector.getAvlRecordById(avlId);
        
        if(avl2Delete != null ){
			// delete the record and check delete result
			String deleteResult = AvailabilityService.deleteAvlRecord(avl2Delete);
        	if(deleteResult != null && deleteResult != ''){
        		return deleteResult;	
        	}
        	
  			// return records to display
        	return getAvlRecords4Display(displayRangeJsonString);	
        }else{
        	// return error message (no such record)
        	return AvailabilityService.constructReturnMessage(DELETE_ERROR, DELETE_ERROR_DESC);	
        }    
    }
    
    @RemoteAction 
    global static String updateAvlRecords(String avlId, String avlJsonString,  String displayRangeJsonString, boolean isDupCheckEnabled){
        System.debug('AvailabilityRequestHandler---avlId:' + avlId);
       // System.debug('AvailabilityRequestHandler---avlJsonString:' + avlJsonString);
       // System.debug('AvailabilityRequestHandler---displayRangeJsonString:' + displayRangeJsonString);
                
        // convert the json string to objects
        List<AvailabilityEntity> avlList = new List<AvailabilityEntity>();
        avlList = AvailabilityParser.convertJsonStringToObjects(avlJsonString);
        //System.debug('AvailabilityRequestHandler---avlList:' + avlList);
                
        // get all related records 
  		List<Days_Unavailable__c> relatedSfAvlList = AvailabilitySelector.getAllRelatedAvlRecsByCanIdAvlIdNDateRange(avlId, avlList);
  		System.debug('AvailabilityRequestHandler---relatedSfAvlList:' + relatedSfAvlList);
            
        // check duplicate
        if(isDupCheckEnabled){
        	String checkDuplicateResult = AvailabilityService.checkAvlDupRecords(avlList, relatedSfAvlList);
  			System.debug('AvailabilityRequestHandler---checkDuplicateResult:' + checkDuplicateResult);
  			if(checkDuplicateResult != null && checkDuplicateResult != ''){
  				return checkDuplicateResult;	
  			}	
        }

        // resolve conflicts and create new avl records
        String updateResult = AvailabilityService.updateAvlRecord(avlId, avlList, relatedSfAvlList, isDupCheckEnabled);
        if(updateResult != null && updateResult!=''){
        	return updateResult;	
        }
        
        //return records to display
        return getAvlRecords4Display(displayRangeJsonString);
    }
    
    @RemoteAction 
    global static String getAvlRecords4Display(String avlJsonString){
        //System.debug('AvailabilityRequestHandler---avlJsonString:' + avlJsonString);
        
        // convert the json string to objects
        List<AvailabilityEntity> avlList = new List<AvailabilityEntity>();
        avlList = AvailabilityParser.convertJsonStringToObjects(avlJsonString);
       // System.debug('AvailabilityRequestHandler---avlList:' + avlList);
        
        // get all related records within this time frame
  		List<Days_Unavailable__c> relatedSfAvlList = AvailabilitySelector.getAllRelatedAvlRecsByCanIdNDateRange(avlList);
		//System.debug('AvailabilityRequestHandler---relatedSfAvlList:' + relatedSfAvlList);       
 
    	List<AvailabilityEntity> avlList4Display = AvailabilityParser.convertObjectsToJsonString(relatedSfAvlList);
    	//System.debug('AvailabilityRequestHandler---relatedSfAvlList:' + relatedSfAvlList);     
    	
    	// construct a map with process result
    	Map<String, List<AvailabilityEntity>> mapWithStatus = new Map<String, List<AvailabilityEntity>>(); 
    	mapWithStatus.put(SUCCESS_STATUS,avlList4Display);
  	
    	String avlListJSONString = JSON.serializePretty(mapWithStatus);
    	  
    	return avlListJSONString;	 
    }


}