public class LinkedinPersonProfileXml{
    XMLDOM theXmlDom;
    String xml {get; set;}
    String rootName {get; set;}
    String nodeName {get; set;}
    public String locName; 
    public String locCode; 
    public string firstname;
    public List<location_phaser> loc {get; set;}
    public Map<String, String> personalProfileOther {get; set;}
    public Map<String , peopleList> perProfile {get; set;}
    public List<List<peopleList>> result {get; set;}
    public List<jobposition> jobpos {get; set;}
    public String month;
    public String year;
    public String perdate {get; set;}
    public Map<String, String> companyInfo {get; set;}
    public Map<String, String> perStartdate {get; set;}
    public Map<String, String> perEnddate {get; set;}
    public Map<String, String> positionOtherInfo {get; set;}
    public List<positions> posis {get; set;}
    public Map<String, String> newgraducation {get; set;}
    public List<education> edu {get; set;}
    public List<educations> edus {get; set;}
    public Map<String, String> educationOtherInfo {get; set;}
    public String picture_url {get; set;}
    public String industry {get; set;}
    public String specialties {get; set;}
    public String publicurl {get; set;}
    public String headline {get; set;}
            
    public Map<String , peopleList> personalProfile(List<DOM.XMLNode> personalXml){
        personalProfileOther = new Map<String, String>();
        picture_url = 'https://www.linkedin.com/scds/common/u/img/icon/icon_no_photo_80x80.png';
        industry = '';
        specialties= '';
        publicurl = '';
        firstname = '';
        headline = '';
        personalProfileOther.put('first-name',firstname);
        personalProfileOther.put('picture-url',picture_url);
        personalProfileOther.put('industry',industry);
        personalProfileOther.put('specialties',specialties);
        personalProfileOther.put('public-profile-url',publicurl);
        personalProfileOther.put('headline',headline);
        perProfile = new Map<String , peopleList>();
        for(integer i = 0 ; i < personalXml.size(); i++){  
                nodeName = personalXml[i].getName();                 
                if(nodeName == 'location'){                
                    location(personalXml[i].getChildElements());
                }
                if(nodeName == 'positions'){
                    positions(personalXml[i].getChildElements());                    
                }
                if(nodeName == 'educations'){
                    educations(personalXml[i].getChildElements());
                }
                else{
                    personalProfileOther.put(nodeName, personalXml[i].getText());
                }               
            }
            peopleList newPeople = new peopleList(loc,posis,edus,personalProfileOther);
            perProfile.put('people',newPeople);
            return perProfile;            
    }
    
    public List<positions> positions(List<DOM.XMLNode> jobPositionsXml){
        posis = new List<positions>{};
        for(integer i = 0 ; i < jobPositionsXml.size (); i++){
            position(jobPositionsXml[i].getChildElements());
            positions newPosition = new positions(jobpos);
            posis.add(newPosition);
        }
        return posis;
    }
    
        public List<jobposition> position(List<DOM.XMLNode> jobPositionXml){
        jobpos = new List<jobposition>{};
        String posistiontitle = '';
        positionOtherInfo = new Map<String, String>();
        positionOtherInfo.put('title',posistiontitle);
        for(integer i = 0; i < jobPositionXml.size(); i++){
            nodeName = jobPositionXml[i].getName();
            if(nodeName == 'start-date'){
                perStartdate = new Map<String, String>();
                persondate(jobPositionXml[i].getChildElements());
                perStartdate.put('start_date',perdate);
            }
            if(nodeName == 'end-date'){
                perEnddate = new Map<String,String>();
                persondate(jobPositionXml[i].getChildElements());
                perEnddate.put('end_date',perdate);
            }
            if(nodeName == 'company'){                
                company_Info(jobPositionXml[i].getChildElements());                
            }
            else{
                positionOtherInfo.put(nodeName, jobPositionXml[i].getText());
            }
        }
        jobposition newjobpostion = new jobposition(perStartdate,perEnddate,companyInfo,positionOtherInfo);
        jobpos.add(newjobpostion);
        return jobpos;
    }
    
    public List<location_phaser> location(List<DOM.XMLNode> locationXml){
        loc = new List<location_phaser>{};
        for(integer i = 0 ; i < locationXml.size (); i++){
                nodeName = locationXml[i].getName();
                if(nodeName == 'name'){
                    locName = locationXml[i].getText();                    
                }
                if(nodeName == 'country'){                    
                    locCode = locationXml[i].getChildElements()[0].getText(); 
                    
                }                   
        }
        location_phaser locPhaser = new location_phaser(locName,locCode);
        loc.add(locPhaser); 
        return loc;
    }
        
    public String persondate (List<DOM.XMLNode> jobDateXml){//return the date by String        
        for(integer i = 0; i < jobDateXml.size(); i++){
            nodeName = jobDateXml[i].getName();
            if(nodeName == 'year'){
                if(jobDateXml[i].getText()!=null){
                    year = jobDateXml[i].getText();                    
                }
            }
            if(nodeName == 'month'){
                if(jobDateXml[i].getText()!=null){
                    month = jobDateXml[i].getText() + '/';
                }
            }            
        }
        perdate = month+year;
        return perdate;
    }
    
    public Map<String, String> company_Info (List<DOM.XMLNode> companyInfoXml){
        companyInfo = new Map<String, String>();
        for(integer i = 0; i < companyInfoXml.size(); i++){
            nodeName = companyInfoXml[i].getName();
            companyInfo.put(nodeName,companyInfoXml[i].getText());
        }
        return companyInfo;
    }
    
    public List<educations> educations(List<DOM.XMLNode> educationsXml){
        edus = new List<educations>{};
        for(integer i = 0 ; i < educationsXml.size (); i++){
            education(educationsXml[i].getChildElements());
            educations neweducations = new educations(edu);
            edus.add(neweducations);
        }
        
        return edus;
    }
    
    public List<education> education(List<DOM.Xmlnode> educationXml){
        educationOtherInfo = new Map<String, String>{};
        String Schoolename = '';
        String degree = '';
        String field = '';
        educationOtherInfo.put('school-name',Schoolename);
        educationOtherInfo.put('degree',degree);
        educationOtherInfo.put('field-of-study',field);
        edu = new List<education>{};
        //educationOtherInfo = new Map<String, String>();
         for(integer i = 0; i < educationXml.size(); i++){
            nodeName = educationXml[i].getName();
            if(nodeName == 'start-date'){
                perStartdate = new Map<String, String>();
                persondate(educationXml[i].getChildElements());
                perStartdate.put('start_date',perdate);
            }
            if(nodeName == 'end-date'){
                perEnddate = new Map<String,String>();
                persondate(educationXml[i].getChildElements());
                perEnddate.put('end_date',perdate);
            }
            else{
                educationOtherInfo.put(nodeName, educationXml[i].getText());
            }
        }
        education neweducation = new education(perStartdate,perEnddate,educationOtherInfo);
        edu.add(neweducation);
        return edu;
        
    }
    public class location_phaser{
        public String locName {get; set;} 
        public String locCode {get; set;} 
        public location_phaser(String s1, String s2){
            this.locName = s1;
            this.locCode = s2;
        }
    }
    
    public class peopleList{
        public List<location_phaser> loc{get ; set;}
        public Map<String, String> personalProfileOther {get ; set;}
        public List<positions> posis{get ; set;}
        public List<educations> edus {get; set;}
        public peopleList(List<location_phaser> s1 , List<positions> s3, List<educations> s4 , Map<String, String> s2){
            this.loc = s1;
            this.personalProfileOther = s2;
            this.posis = s3;
            this.edus = s4;
        }
    }
    
    public class positions{
        public List<jobposition> jobposs {get; set;} 
        public positions(List<jobposition> s1){
            this.jobposs = s1;
        }
    }
    
    public class jobposition{
        public Map<String, String> perStartdate {get; set;} 
        public Map<String, String> perEnddate {get; set;} 
        public Map<String, String> company_Info {get; set;} 
        public Map<String, String> positionOtherInfo {get; set;}
        
        public jobposition(Map<String, String> s1, Map<String, String> s2, Map<String, String> s3, Map<String, String> s4){
            this.perStartdate = s1;
            this.perEnddate = s2;
            this.company_Info = s3;
            this.positionOtherInfo = s4;            
        }
    }
    
    public class educations{
        public List<education> educs {get; set;}
        public educations(List<education> s1){
            this.educs = s1;
        }
    }
    public class education{
        public Map<String, String> perStartdate {get; set;} 
        public Map<String, String> perEnddate {get; set;} 
        public Map<String, String> educationOtherInfo {get; set;}
        public education(Map<String, String> s1, Map<String, String> s2, Map<String, String> s3){
            this.perStartdate = s1;
            this.perEnddate = s2;
            this.educationOtherInfo = s3;
        }
    }
}