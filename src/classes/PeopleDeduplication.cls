/*
	This class for PeopleDeduplication class trigger.
	Author by Jack on 13 Aug 2013
*/
public with sharing class PeopleDeduplication {
	public static void PeopleDeduplication(List<Contact> tempcontacts){
		String errmsg = '';   //display the error message
		Integer returnresult;
		List<Contact> duplicateexistingContact = new List<Contact>();
   		Contact tempcontact = new Contact();
		for(Contact tempcon : tempcontacts){
			tempcontact = tempcon;
		}
		
		if(tempcontact.Email != null && tempcontact.Email != ''){
			if(!tempcontact.Candidate_From_Web__c && !tempcontact.From_Own_WebSite__c 
                   			&& !Deduplication.isRecordIdExcluded(tempcontact.RecordTypeId))//** input by user internally
            	{
                	duplicateexistingContact = FetchExistingContact(tempcontact);
                	if(duplicateexistingContact!=null && duplicateexistingContact.size() >0) {                            
		            	errmsg += '<div style="color:green;">Duplication found. ';
		                errmsg += 'The email address already exists for the following person and <br/>cannot be linked to the new record. Please check the existing records below or change the email address of the new record. <br/></div>';
		                errmsg += '<div align="center" style="font-weight:normal;"><table border="0" style="border-collapse: collapse;" width="90%"> ';
		                errmsg += '<tr><td><b>People</b></td><td><b>Email</b></td><td><b>Client Name</b></td><td><b>RecordType</b></td><td><b>Owner</b></td></tr>';
		                for(Contact c: duplicateexistingContact){
                            errmsg += '<tr>';
                           
		                   	errmsg += '<td width="20%"><a href="/'+c.Id+'" target="_blank">';
                            if(c.FirstName!=null){
                                errmsg += c.FirstName.escapehtml4()+ ' ';
                            }
                            if(c.LastName!=null){
                                errmsg += c.LastName.escapehtml4();
                            }
                            errmsg += '</a> </td>';
                            if(c.Email!=null){
		                    	errmsg += '<td width="20%">'+c.Email.escapehtml4()+'</td>';
                            }
                            if(c.Account.Name!=null){
		                    	errmsg += '<td width="30%">'+c.Account.Name.escapehtml4()+'</td>';
                            }
                            if(c.RecordType.Name != null){
		                    	errmsg += '<td width="20%">'+c.RecordType.Name.escapehtml4()+'</td>';
                            }
                            if(c.Owner.Name != null){
		                    	errmsg += '<td width="10%">'+c.Owner.Name.escapehtml4()+'</td>';
                            }
		                    errmsg += '</tr>';
		             	}
		            errmsg += '</table></div>';
		            tempcontact.addError(errmsg,false);
		            }
            	}
		}
	}
	
	//Fetch existing same firstname, lastname, email contacts list
	private static List<Contact> FetchExistingContact(Contact tempcontact){
		List<Id> excludedIdList = Deduplication.getExcludedRecordTypeIds();//** input by user internally
		List<Contact> tempexistingContact = new List<Contact>();
		CommonSelector.checkRead(Contact.sObjectType,'id, Candidate_From_Web__c,Email,FirstName,LastName, Resume__c, Resume_Source__c, RecordType.DeveloperName, Account.Name, RecordType.Name, Owner.Name');
		tempexistingContact =  [Select id, Candidate_From_Web__c,Email,FirstName,LastName, 
                                Resume__c, Resume_Source__c, RecordType.DeveloperName, Account.Name,
                                RecordType.Name, Owner.Name 
                                from Contact where Email=:tempcontact.Email and FirstName=:tempcontact.FirstName and
                                LastName =: tempcontact.LastName and id !=: tempcontact.Id and RecordTypeId not in :excludedIdList                                            
                                order by LastModifiedDate DESC];
        return tempexistingContact;
	}
}