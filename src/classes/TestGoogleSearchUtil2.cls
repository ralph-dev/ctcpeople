@isTest
public with sharing class TestGoogleSearchUtil2 {

	public testmethod static void testsetString(){
		String testString;
		List<Contact> getContacts = fetchCandidate();
		Set<Id> getCandidatesID = fetchCandidatesID(getContacts);
		if(getCandidatesID!=null && getCandidatesID.size()>0){
			testString = GoogleSearchUtil2.setString(getCandidatesID);
			system.assert(testString!=null);
		}
		else{
			system.assertEquals(testString,null);
		}
	}
	
	private testmethod static void testgetListString(){
		String testString;
		List<Contact> getContacts = fetchCandidate();
		List<String> getCandidatesName = fetchCandidatesName(getContacts);
		if(getCandidatesName!=null && getCandidatesName.size()>0){
			testString = GoogleSearchUtil2.getListString(getCandidatesName);
			system.assert(testString!=null);
		}
		else{
			system.assertEquals(testString,null);
		}
	}
	
	private testmethod static void testgetJsonArrayString(){
		String testString;
		List<Contact> getContacts = fetchCandidate();
		List<JSONObject> getCandidatesObject = fetchCandidatesObject(getContacts);
		if(getCandidatesObject!=null && getCandidatesObject.size()>0){
			testString = GoogleSearchUtil2.getJsonArrayString(getCandidatesObject);
			system.assert(testString!=null);
		}
		else{
			system.assertEquals(testString,null);
		}
	}
	
	private testmethod static void testGenerateJsonStringFromMap(){
		Map<String,Map<String,String>> map2Convert = new Map<String,Map<String,String>>();
		Map<String,String> tempMap = new Map<String,String>();
		tempMap.put('temp','temp');
		map2Convert.put('temp',tempMap);
		
		String jsonString = GoogleSearchUtil2.generateJsonStringFromMap(map2Convert);
		System.debug('jsonString =' +jsonString);
		system.assertNotEquals(null, jsonString);
		
	}
	
	private static testmethod void testgetCustomSetting(){
		String testString;
		String testname = 'test';
		GSA__c tempgsa = fetchCollection();
		GSA__c collection = GSA__c.getValues(testname);
		system.debug('collection ='+collection);
		testString = GoogleSearchUtil2.getCustomSetting(testname);
		system.assertEquals(testString, '');
		tempgsa.name__c = '1223456';
		update tempgsa;		
		testString = GoogleSearchUtil2.getCustomSetting(testname);
		system.assert(testString!='');
	}
	
	private static testmethod void testgoogleSearchPageRequestJsonString(){
		String SelectedObject = 'Contact';
		String location = 'Left';
		
        FieldsClass.SelectedField tempselectedfield = new FieldsClass.SelectedField('Email', 'Email', 'EMAIL', null, null, 'true', false);
        tempselectedfield.setLocation('left');
        List<FieldsClass.SelectedField> selectedfields = new List<FieldsClass.SelectedField>();
        selectedfields.add(tempselectedfield);
        Document tempdoc = XmlWriterClass.GeneralSelectedFieldsListJsonDoc(selectedfields, SelectedObject);
        String JSONdocname = 'ClicktoCloud_JSON_'+SelectedObject;
        Folder ctcFolder = DaoFolder.getFolderByDevName('CTC_Admin_Folder');
        Document JSONSdoc = DaoDocument.fetchdocuments(ctcFolder.id, JSONdocname);
        system.assertEquals(tempdoc.id,JSONSdoc.id);
        List<JSONObject> tempjsonobject = GoogleSearchUtil2.googleSearchPageRequestJsonString(SelectedObject, location);
        system.assert(tempjsonobject.size()==1);
	}
	
	private static testmethod void testreturnUrl(){
		Placement__c tempvac = fetchvacancy();
		String result = GoogleSearchUtil2.returnUrl(tempvac.id,'','');
		system.assert(result.contains('advancedContactSearch'));
		tempvac = new Placement__c();
		result = GoogleSearchUtil2.returnUrl(tempvac.id,'','');
		system.assert(result.contains('advancedContactSearch'));
	}
	
	private static List<JSONObject> fetchCandidatesObject(List<Contact> contactlist){
		List<JSONObject>  CandidatesObject = new List<JSONObject>();
		JSONObject jsonobj = new JSONObject();
		for(Contact tempcontact : contactlist){
			jsonobj.putOpt(String.valueof(tempcontact.id), new JSONObject.value(tempcontact.LastName));
			CandidatesObject.add(jsonobj);
		}
		return CandidatesObject;
	}	
	
	private static Set<Id> fetchCandidatesID(List<Contact> contactlist){
		Set<Id> CandidatesIdList = new Set<Id>();
		for(Contact tempcontact : contactlist){
			CandidatesIdList.add(tempcontact.id);
		}
		return CandidatesIdList;
	}
	
	private static List<String> fetchCandidatesName(List<Contact> contactlist){
		List<String> CandidatesNameList = new List<String>();
		for(Contact tempcontact : contactlist){
			CandidatesNameList.add(tempcontact.Name);
		}
		return CandidatesNameList;
	}
	
	private static List<Contact> fetchCandidate(){
		List<Contact> testContactList = new List<Contact>();
		Account testaccount = new Account(Name='test');
		insert testaccount;
		for(Integer i=0; i<10; i++){
			Contact testcontact = new Contact(LastName = 'con'+i, Email = 'test'+i+'@user2test.com', AccountId = testaccount.id);
			testContactList.add(testcontact);
		}
		insert testContactList;
		return testContactList;
	}
	
	private static GSA__c fetchCollection(){
		GSA__c testGSA = new GSA__c(Name = 'test', name__c = '');	
		system.debug('test ='+ testGSA.SetupOwnerId);	
		insert testGSA;
		return testGSA;
	}
	
	private static Placement__c fetchvacancy(){
		Account testaccount = new Account(Name='test');
		insert testaccount;
		Placement__c tempvacancy = new Placement__c(Name='test', Company__c = testaccount.id);
		insert tempvacancy;
		return tempvacancy;
	}	
	
	static testMethod void test() {
	    GoogleSearchUtil2.getgooglesearchdoc('PeopleCloud1_Placement__c');
	    system.assertNotEquals(null, GoogleSearchUtil2.generateStringFromList(new List<Id>{'a049000000u5iOJ', 'a049000000u5iOJ'}));
	}
}