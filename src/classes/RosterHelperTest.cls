@isTest
private class RosterHelperTest {
	final static String ROSTER_STATUS_PENDING = 'Pending';
	final static String ROSTER_TYPE_CLIENT = 'Client';
	
	@isTest
    static void deserializeRosterJsonTest() {
        // TO DO: implement unit test
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        Days_Unavailable__c roster = 
        	TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_PENDING);
        RosterDTO rDTO = 
        	TestDataFactoryRoster.createRosterDTO(acc, con, shift, roster,ROSTER_STATUS_PENDING);
        
        String rDTOJson = JSON.serialize(rDTO);
        List<RosterDTO> rDTOList = RosterHelper.deserializeRosterJson(rDTOJson);
        System.assertNotEquals(rDTOList, null);
    }
    
    @isTest
    static void deserializeRosterParamJsonTest() {
        // TO DO: implement unit test
        Account acc = TestDataFactoryRoster.createAccount();
        RosterParamDTO rParamDTO = 
        	TestDataFactoryRoster.createRosterParamDTO(acc,ROSTER_STATUS_PENDING,ROSTER_TYPE_CLIENT);
        
        String rParamDTOJson = JSON.serialize(rParamDTO);
        List<RosterParamDTO> rParamDTOList = RosterHelper.deserializeRosterParamJson(rParamDTOJson);
        System.assertNotEquals(rParamDTOList, null);
    }
    
    @isTest
    static void convertRosterDTOToRosterSObjectForRosterDBTest() {
        // TO DO: implement unit test
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        Days_Unavailable__c roster = 
        	TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_PENDING);
        RosterDTO rDTO = 
        	TestDataFactoryRoster.createRosterDTO(acc, con, shift, roster,ROSTER_STATUS_PENDING);
        
       Days_Unavailable__c rosterConverted = new Days_Unavailable__c();
       RosterHelper.convertRosterDTOToRosterSObjectForRosterDB(rosterConverted,rDTO);
       System.assertEquals(rosterConverted.Id,rDTO.getId());
    }
    
    @isTest
    static void assignValuesToRosterTest(){
    	Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        Days_Unavailable__c roster = 
        	TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_PENDING);
        Days_Unavailable__c rosterAssigned = new Days_Unavailable__c();
        RosterHelper.assignValuesToRoster(rosterAssigned,roster);
        System.assertEquals(rosterAssigned.Id,roster.Id);
    }
    
    @isTest
    static void mergeRosterTest(){
    	Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
    	List<Days_Unavailable__c> rostersToMerge = TestDataFactoryRoster.createRosters(con, shift, acc,ROSTER_STATUS_PENDING);
    	
    	List<Days_Unavailable__c> rostersForDisplay = new List<Days_Unavailable__c>();
    	for(Days_Unavailable__c r : rostersToMerge){
    		RosterHelper.mergeRoster(rostersForDisplay,r);
    	}
    	System.assertEquals(rostersForDisplay.size(),1);
    }
    
    @isTest
    static void updateRosterEndDateTest(){
    	Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
    	List<Days_Unavailable__c> rosters = TestDataFactoryRoster.createRostersToCheckEndDateNeedToBeUpdated(con, shift, acc,ROSTER_STATUS_PENDING);
    	List<Days_Unavailable__c> rostersToReturn = RosterHelper.updateRosterEndDate(rosters);
    	System.assertNotEquals(rostersToReturn,null);
    }
    
    
}