/******
    Test codes for the testMassdeletecandidate
    
                                    Edited by Alvin
************/
@isTest
private class TestMassdeletecandidate {
   static testMethod void myUnitTest() {
   	System.runAs(DummyRecordCreator.platformUser) {
    Massdeletecandidate massdelete=new Massdeletecandidate();
    massdelete.jsonvalues ='[{"fieldname":"Direct_Reports__c","fieldtype":"DOUBLE","operator":"equals","values":""},{"fieldname":"AccountId","fieldtype":"REFERENCE","operator":"not equals","values":"0019000000PF5LGAA1"},{"fieldname":"Name","fieldtype":"STRING","operator":"equals","values":"Andy Young"}]';
    massdelete.scheduleflow=null;
    massdelete.jsondatavalue();
    String str=massdelete.clauses;
    System.debug('the original string is:'+String.escapeSingleQuotes(str)+'.');
    System.assertEquals(massdelete.clauses,'select id from Contact where Direct_Reports__c  =  NULL AND AccountId  != \'0019000000PF5LGAA1\' AND Name  =  \'Andy Young\'' );
   	}
    }
    
    static testMethod void test() {
    	System.runAs(DummyRecordCreator.platformUser) {
        Massdeletecandidate m = new Massdeletecandidate();
        system.assertNotEquals(null, m.getshowsteps());
    	}
    }
    
}