@isTest
public class TestJobPosting {
	public static testMethod void testInsertNewAdTemplate(){
		System.runAs(DummyRecordCreator.platformUser) {
		boolean enableskillparsing = SkillGroups.isSkillParsingEnabled();
		
        Placement__c p = new Placement__c(Name='ctc test');
        insert p;
        ApexPages.currentPage().getParameters().put('vid',p.Id);
        Advertisement__c b = new Advertisement__c();
        
        createtemplategroups();
        ApexPages.StandardController stdController = new ApexPages.StandardController(b);
        JobPosting thecontroller = new JobPosting(stdController);
        if(enableskillparsing){
        	system.assert(thecontroller.defaultskillgroups.size()>0);
        }
        else{
        	system.assert(thecontroller.defaultskillgroups.size()>0);
        }
        thecontroller.init();
        PageReference p1 = thecontroller.SaveJob();
        system.assert(p1 != null );
        thecontroller.checkPrivilege();
		}
    }
	
	public static testmethod void testupdateAdTemplate(){
		System.runAs(DummyRecordCreator.platformUser) {
		Placement__c p = new Placement__c(Name='ctc test');
        insert p;
		Advertisement__c a = new Advertisement__c(Vacancy__c = p.Id, Job_Title__c ='new job');
        insert a;
        ApexPages.currentPage().getParameters().put('vid',null);
        ApexPages.currentPage().getParameters().put('id',a.Id);
        createtemplategroups();
        ApexPages.StandardController stdController = new ApexPages.StandardController(a);
        JobPosting thecontroller2 = new JobPosting(stdController);
        thecontroller2.init();
        PageReference p2 = thecontroller2.UpdateJob();
        system.assert(p2 != null );
		}
	}
	public static List<Skill_Group__c> createtemplategroups(){
		List<Skill_Group__c> tempgrouplist = new List<Skill_Group__c>();
		for(integer i=0; i<10; i++){
			Skill_Group__c tempgroup = new Skill_Group__c();
			tempgroup.Name = 'ctc_test'+i;
			tempgroup.Skill_Group_External_Id__c = 'ctc_test'+i;
			tempgroup.Enabled__c = true;
			tempgroup.default__c = true;
			insert tempgroup;
			tempgrouplist.add(tempgroup);
		}
		return tempgrouplist;
	} 
	
	static testMethod void testGetSelectSkillGroupList() {
		System.runAs(DummyRecordCreator.platformUser) {
	    boolean enableskillparsing = SkillGroups.isSkillParsingEnabled();
		
        Placement__c p = new Placement__c(Name='ctc test');
        insert p;
        ApexPages.currentPage().getParameters().put('vid',p.Id);
        Advertisement__c b = new Advertisement__c();
        
        createtemplategroups();
        ApexPages.StandardController stdController = new ApexPages.StandardController(b);
        JobPosting c = new JobPosting(stdController);
        
        system.assertNotEquals(null, c.getSelectSkillGroupList());
        
        c.isNext = false;
        c.is_preview = true;
        c.hasSeek = false;
        c.hasMycareer = false;
        c.jobTitle = 'test';
        
        system.assertEquals(false, c.isNext);
        system.assertEquals(true, c.is_preview);
        system.assertEquals(false, c.hasSeek);
        system.assertEquals(false, c.hasMycareer);
        system.assertEquals('test', c.jobTitle);
		}
	}
}