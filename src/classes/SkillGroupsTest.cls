@isTest
private class SkillGroupsTest {

	private static testMethod void getSkillGroupsTest() {
	    
	    DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
	    System.runAs(DummyRecordCreator.platformUser) {
	    	
    	    List<Skill_Group__c> sgs = SkillGroups.getSkillGroups();
            system.assert(sgs.size()>0);
	    }
	}
	
	private static testMethod void isSkillParsingEnabledTest() {
	    
	   
	    System.runAs(DummyRecordCreator.platformUser) {
	    	 PCAppSwitch__c ps = new PCAppSwitch__c();
		    ps.Enable_Daxtra_Skill_Parsing__c = true;
		    insert ps;
            boolean ifEnable = SkillGroups.isSkillParsingEnabled();
            system.assert(ifEnable);
	    }

	}
	
	private static testMethod void getDefaultSkillGroupsTest() {
		
		DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
		Skill_Group__c dsg = DummyRecordCreator.createDefaultSkillGroup();
		
		System.runAs(DummyRecordCreator.platformUser) {
	    
		    
		    
		    
    	    List<Skill_Group__c> sgs = SkillGroups.getDefaultSkillGroups();
            system.assert(sgs.size()>0);
	    }
	}
	
	private static testMethod void getSkillGroupsByExtIdsTest() {
	    
	    DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
	    System.runAs(DummyRecordCreator.platformUser) {
	    	
    	    List<Skill_Group__c> sgs = SkillGroups.getSkillGroupsByExtIds(new Set<String>{rs.skillGroups.get(0).Skill_Group_External_Id__c});
            system.assert(sgs.size()>0);
	    }
	}
	
	private static testMethod void getSelectedSkillGroupTest() {
	    
	    DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
	    Skill_Group__c dsg = DummyRecordCreator.createDefaultSkillGroup();
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	    	
	    	
	        List<SkillGroups.selectedSkillGroup> selectedSkillGroupS = SkillGroups.getSelectedSkillGroup();
	        system.assert(selectedSkillGroupS.size()>0);
	    }
	}

}