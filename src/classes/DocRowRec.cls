/**
    Author by Jack
    
    Modified by Andy 20/09/2017
**/

public with sharing class DocRowRec implements Comparable{
    
        public enum SORT_BY {
            ByName,ByCreatedDate,BySize
        }
        
        public enum ORDER_AS {
            AsASC,AsDESC
        }
        
        public static Long convertSizeStrToLong(String sizeStr){
            return PeopleCloudHelper.convertSizeStrToLong(sizeStr);
        }
    
        public static SORT_BY sortBy = SORT_BY.ByCreatedDate;
        public static ORDER_AS orderAs = ORDER_AS.AsASC;
        public static Long LARGE_DOC_SIZE = Decimal.valueOf(2.2 * 1024 * 1024 + '').longValue();
        
        public Web_Document__c doc{get;set;}
        public Document locdoc {get; set;}
        public Id docofuserid {get;set;}
        public ConRowRec owner{get;set;}
        public UsrRowRec usr{get;set;}
        public boolean isSelected{get;set;}
        public Integer retrievingCode{get;set;}
        public String S3Folder {get; set;}
        public String ObjectKey {get; set;}
        public Long docsize {get; set;}
        
        public Contact tempcandidate {get; set;}
        public DocRowRec(Web_Document__c wd,ConRowRec crr, integer seldocsize){
            isSelected = wd.Default_Doc__c;
            //S3Folder = wd.S3_Folder__c;
            //ObjectKey = wd.ObjectKey__c;
            retrievingCode = QuickSendResumeController.INFO_NO_ERR;
            doc = wd;
            owner=crr;
            docsize = seldocsize;
        }
        
        public DocRowRec(Web_Document__c wd, UsrRowRec urr, integer seldocsize){
            isSelected = wd.Default_Doc__c;
            //S3Folder = wd.S3_Folder__c;
            //ObjectKey = wd.ObjectKey__c;
            retrievingCode = QuickSendResumeController.INFO_NO_ERR;
            doc = wd;
            usr=urr;
            docsize = seldocsize;
        }
        
    	public DocRowRec(Web_Document__c wd, ConRowRec crr, Boolean selected) {
            this(wd, crr);
            isSelected = selected;
        }
        public DocRowRec(Web_Document__c wd, ConRowRec crr) {
            isSelected = wd.Default_Doc__c;
            //isSelected = true;
            doc = wd;
            owner = crr;
            
            docsize = convertSizeStrToLong(doc.File_Size__c);
        }
        
    	public DocRowRec(Web_Document__c wd, UsrRowRec urr, Boolean selected) {
            this(wd, urr);
            isSelected = selected;
        }
        
        public DocRowRec(Web_Document__c wd, UsrRowRec urr) {
            isSelected = wd.Default_Doc__c;
            //isSelected = true;
            doc = wd;
            usr = urr;
            docsize = convertSizeStrToLong(doc.File_Size__c);
        }
        
        public DocRowRec(Document localdoc , ConRowRec crr){
            isSelected = true;
            locdoc = localdoc;
            owner=crr;
        }
        
        public DocRowRec(Document localdoc,Contact tempContact, Id userId){
            isSelected = true;
            locdoc = localdoc;
            docofuserid=userId;
            tempcandidate = tempContact;
        }
        
        public DocRowRec(Document localdoc, Id userId){
            isSelected = true;
            locdoc = localdoc;
            docofuserid=userId;
        }
        
        public String retrievingMsg{
            get{
                return QuickSendResumeController.getRetrievingMsg(retrievingCode);
            }
        }
    
 
        
        public String displaySize{
             get{
                
                 docsize = docsize == null ? 0 : docsize; 
                 return PeopleCloudHelper.byteCountToDisplaySize(docsize);
                
            }
        }           
    
        public Integer compareTo(Object compareTo) {

            DocRowRec compareToDoc = (DocRowRec)compareTo;
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = diffValue(compareToDoc);
            
           if(orderAs == ORDER_AS.AsDESC){
                returnValue = 0 - returnValue;
           }
            
            return returnValue;
        }
        
        private Integer diffValue(DocRowRec compareToDoc){
            Integer diff = 0;
            if(sortBy == SORT_BY.ByCreatedDate){
                DateTime thisDate = doc != null ? doc.CreatedDate : locdoc.CreatedDate;
                DateTime compareDate = compareToDoc.doc != null ? compareToDoc.doc.CreatedDate : compareToDoc.locdoc.CreatedDate;
                
                if(thisDate == null){
                    diff = -1;
                }else if(compareDate == null){
                    diff = 1;
                }else {
                    diff = compareDate.getTime() < thisDate.getTime() ? 1 : -1;
                }
            }else if(sortBy == SORT_BY.ByName){
                String name = doc != null ? doc.Name : locdoc.Name;
                String compareName = compareToDoc.doc != null ? compareToDoc.doc.Name : compareToDoc.locdoc.Name;
                
               
                if(name == null){
                    diff = -1;
                }else if(compareName == null){
                    diff = 1;
                }else{
                    diff = name.toLowerCase().compareTo(compareName.toLowerCase());
                }
                
            }else if(sortBy == SORT_BY.BySize){
                Long size = docsize;//convertSizeStrToLong( doc != null ? doc.File_Size__c : '0');
                Long compareSize = compareToDoc.docsize;//convertSizeStrToLong( compareToDoc.doc != null ? compareToDoc.doc.File_Size__c : '0');
                
               
                if(size == null){
                    diff = -1;
                }else if(compareSize == null){
                    diff = 1;
                }else{
                    diff = Integer.valueOf( size - compareSize );
                }
                
            }
             
             
             
            return diff;
        }
        
}