/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

@isTest
public class SeekEditTest {
    /*public static testMethod void myunitTest(){ 
        System.runAs(DummyRecordCreator.platformUser) {
            list<String> rTDevNameList = new list<String>();
	        rTDevNameList.add('ApplicationFormStyle');
	        rTDevNameList.add('templateStyle');
	        rTDevNameList.add('seekScreenStyle');
	        RecordTypeSelector recordSelector=new RecordTypeSelector();
	        list<RecordType> rTList = recordSelector.fetchRecordTypesByName(rTDevNameList);
	        User u = DataTestFactory.getCurrentUser();
	        StyleCategory__c sc = DataTestFactory.createStyleCategory();
	        Advertisement__c adv = DataTestFactory.createAdvertisement();
	        adv.Status__c = 'Active' ;
	        adv.Job_Posting_Status__c = 'In Queue';
	        adv.Skill_Group_Criteria__c = 'test';
	        adv.SeekScreen__c = sc.Id;
	        adv.online_footer__c = '\\[\\%User Name\\%\\]\\[\\%Vacancy Referece No\\%\\]';
	        adv.Reference_No__c = '123asdssa';
	        adv.Application_Form_Style__c = sc.Id;
	        update adv;
	        List<Skill_Group__c> skills = DataTestFactory.createSkillGroup();
	        Test.setCurrentPage(Page.SeekEdit); 
	        ApexPages.currentPage().getParameters().put('isedit','false');
	        ApexPages.currentPage().getParameters().put('Id',adv.id);
	        ApexPages.StandardController stdcontroller = new ApexPages.StandardController(adv); 
	        SeekEdit thecontroller = new SeekEdit(stdcontroller);
	        thecontroller.init();
	        system.assert(!thecontroller.show_page);
	        List<SelectOption> displaySelectOption = thecontroller.getSelectSkillGroupList();
	        system.assertNotEquals(displaySelectOption.size(),null);
	        boolean i = thecontroller.getTemplateDetail();
	        system.assert(!i);
	        adv.Template__c = sc.Id;
	        adv.Status__c = 'Ready to Post' ;
	        update adv;
	        u.Seek_Account__c = adv.Id;
	        u.Seek_Usage__c = '10';
	        u.Monthly_Quota_Seek__c = '100';
	        u.AmazonS3_Folder__c = 'test';
	        update u;
	        sc.Name = 'test';
	        sc.templateSeek__c = 'test'; 
	        sc.screenId__c = adv.SeekScreen__c;
	        sc.LogoID__c = 'test';
	        sc.recordtypeId = rTList[0].Id;
	        sc.Active__c = true ;
	        update sc;
	        ApexPages.currentPage().getParameters().put('isedit','true');
	        stdcontroller = new ApexPages.StandardController(adv);
	        thecontroller = new SeekEdit(stdcontroller);
	        thecontroller.init();
	        system.assert(thecontroller.show_page);
	        i = thecontroller.getTemplateDetail();
	        system.assert(i);
	        thecontroller.enableskillparsing = true;
	        pageReference p = thecontroller.dopost();
	        system.assertNotEquals(p, null);
	        Boolean b = thecontroller.getSeekScreenOn();
	        system.assert(!b);
	        List<SelectOption> slectionList = thecontroller.getStyleOptions();
	        system.assertNotEquals(slectionList.size(), null);
	        boolean  getStyles = thecontroller.getStyles(true);
	        system.assert(!getStyles);
	        getStyles = thecontroller.getStyles(false);
	        system.assert(getStyles);
	        List<SelectOption> TemplateOptions = thecontroller.getTemplateOptions();
	        system.assertEquals(TemplateOptions.size(), 0);
	        List<SelectOption> SeekScreenOptions = thecontroller.getSeekScreenOptions();
	        system.assertEquals(SeekScreenOptions.size(), 0);
        }
    }*/
}