public with sharing class StyleCategoryService extends SObjectCRUDService{
	//private DynamicSelector dynSel;

	public StyleCategoryService() {
		super(StyleCategory__c.sObjectType);
		//dynSel = new DynamicSelector(StyleCategory__c.sObjectType.getDescribe().getName(),false,true,true);
	
	}

	public override List<SObject> createRecords(List<Map<String, Object>> sobjs) {
		List<StyleCategory__c> styleCategories = (List<StyleCategory__c>)constructSObjectsForCRUD(sobjs, SObjectCRUDService.OperationType.CREATE);
        try{
			return createRecords(styleCategories);
		}catch(exception e){
			throw e;
		}
	}

	public override List<SObject> updateRecords(List<Map<String, Object>> sobjs) {
		List<StyleCategory__c> styleCategories = (List<StyleCategory__c>)constructSObjectsForCRUD(sobjs, SObjectCRUDService.OperationType.MODIFY);
        try{
			return updateRecords(styleCategories);
		}catch(exception e){
			throw e;
		}
	}
	
	public List<SObject> createSaveSearchRecords(List<Map<String, Object>> sobjs) {
	    List<StyleCategory__c> styleCategories = (List<StyleCategory__c>)constructSObjectsForCRUD(sobjs, SObjectCRUDService.OperationType.CREATE);
	    Id saveSearchRecordTypeId = DaoRecordType.GoogleSaveSearchRT.Id;
	    for (StyleCategory__c sc : styleCategories) {
	        sc.RecordTypeId = saveSearchRecordTypeId;
	        sc.Unique_Search_Record_Name__c = sc.Name + '_' + UserInfo.getUserId();
	        sc.Active__c = true;
	        sc.templateActive__c =true;
	    }
        try{
			return createRecords(styleCategories);
		}catch(exception e){
			throw e;
		}
	}
	
	public List<SObject> updateSaveSearchRecords(List<Map<String, Object>> sobjs) {
		List<StyleCategory__c> styleCategories = (List<StyleCategory__c>)constructSObjectsForCRUD(sobjs, SObjectCRUDService.OperationType.MODIFY);
	    for (StyleCategory__c sc : styleCategories) {
	        sc.Unique_Search_Record_Name__c = sc.Name + '_' + UserInfo.getUserId();
	    }
        try{
			return updateRecords(styleCategories);
		}catch(exception e){
			throw e;
		}
	}
	
}