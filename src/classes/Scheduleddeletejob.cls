/**
        This is a port for the scheduled job task. The function will be activated here. 
                                                                        
                                                                        Alvin

**/

global with sharing class  Scheduleddeletejob implements Schedulable{
	public static Integer maxcandidatesize=25000;
	public static Integer maxcandidatecmsize=2500;
    global void sendmail(List<String> email,String htmlbody){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(email);
        mail.setSubject('Mass Candidate Delete Report');
        mail.setHtmlBody(htmlbody);
        if(!Test.isRunningTest()){
        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }else{
        	system.debug(mail);
        }
        
    }
    global void inserttask(Task newtask, String description){
        Date today_date=Date.today();
        newtask.Description=description;
        newtask.Subject='Mass delete Candidates';
        newtask.Status='In Progress';
        newtask.ActivityDate=today_date; 
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Task.Description,
            Task.Subject,
            Task.Status,
            Task.ActivityDate
        };
        fflib_SecurityUtils.checkInsert(Task.SObjectType, fieldList); 
        insert newtask;    
    }
    
    global void execute(SchedulableContext SC) {
        String schedulejobid=SC.getTriggerId();
        schedulejobid=schedulejobid.substring(0,15);
        
        /*******************Task for the report of the scheduled jobs****************************/
        Task newtask=new Task();
        Date to_day=Date.today();
        
        
        /*****************Retrieve the report in the ****************************/
        StyleCategory__c sty=new StyleCategory__c();
        CommonSelector.checkRead(StyleCategory__c.SObjectType,'id, schedulejobId__c,End_Date__c,Start_Date__c,asquerystring__c');
        sty=[select id, schedulejobId__c,End_Date__c,Start_Date__c,asquerystring__c from StyleCategory__c where schedulejobId__c=: schedulejobid limit 1];
        String deletetaskerrormessage='Mass delete candidate failed. Here are the Details:';
        if(sty==null){
            system.abortJob(schedulejobid);
        }else{
             if(sty.End_Date__c < to_day    ){
                //If the schedular job is out of date, we delete it.
                try{
                    system.abortJob(schedulejobid);
                    fflib_SecurityUtils.checkObjectIsDeletable(sty.getSObjectType());
                    delete sty;
                }catch (Exception ex){                  
                    inserttask(newtask,deletetaskerrormessage+ex);
                    NotificationClass.notifyErr2Dev('Error key:delete scheduled job failed ', ex);
                }
                 
            }else{
                if(sty.Start_Date__c == to_day|| sty.Start_Date__c <to_day ){
                    //Delete the candidates according to the query string.
                    List<String> email= new List<String>();                                                 
                    if(PCAppSwitch__c.getinstance().massdelete_mail_address__c==null || PCAppSwitch__c.getinstance().massdelete_mail_address__c=='' ){
                        email.add('log@clicktocloud.com');
                    }else{
                        email.add(PCAppSwitch__c.getinstance().massdelete_mail_address__c);     
                    }
                    String errormessageissue='Mass delete candidate failed. Here are the details:';
                    try{
                        List<Contact> cands=new List<Contact>();                                
                        
                        String query= sty.asquerystring__c;
                        if(String.isNotBlank(query)) {
                            query = query + ' limit ' + maxcandidatesize;
                            String fieldsToCheck = getSelectedFields(query);
                            CommonSelector.checkRead(Contact.SObjectType, fieldsToCheck);
                            cands = Database.query(query);
                        }
                        if(cands.size()>0){
                            if(cands.size()==maxcandidatesize || (Test.isrunningTest() && cands.size() >3000)){
                                String messages='The candidates number is over '+maxcandidatesize+', the task has been canceled';
                                system.abortJob(schedulejobid);
                                fflib_SecurityUtils.checkObjectIsDeletable(sty.getSObjectType());
                                delete sty;                               
                                inserttask(newtask,messages);
                                sendmail(email,messages);
        
                            }else{
                                List<Placement_Candidate__c> cms=new List<Placement_Candidate__c>();
                                CommonSelector.checkRead(Placement_Candidate__c.SObjectType,'id');
                                cms=[select id from Placement_Candidate__c where Candidate__c in: cands limit :maxcandidatecmsize];
                                if(cms.size()==maxcandidatecmsize || (Test.isrunningTest() && cms.size() >50)){
                                    String cmmessages='The candidates management number is over '+maxcandidatecmsize+', the task has been canceled';                         
                                    system.abortJob(schedulejobid);
                                    fflib_SecurityUtils.checkObjectIsDeletable(sty.getSObjectType());
                                    delete sty;                                    
                                    inserttask(newtask,cmmessages);
                                    sendmail(email,cmmessages);
                                }else{
                                    //delete candidates and candidate managements
                                    if(cms.size()>0){
                                        fflib_SecurityUtils.checkObjectIsDeletable(Placement_Candidate__c.SObjectType);
                                        delete cms;
                                    }
                                    fflib_SecurityUtils.checkObjectIsDeletable(Contact.SObjectType);           
                                    delete cands;
                                    datetime currenttime = datetime.now();
                                    String currenttimestr=currenttime.format('MM/dd/yyyy HH:mm:ss', 'Australia/Sydney');
                                    String reportsmessage= '------------------ This email generated by Apex code, please do not reply ----------------<br/><br/>';
                                    reportsmessage+='Candidate managements deleted: '+cms.size()+'<br/>';
                                    reportsmessage+='Candidates deleted: '+cands.size()+'<br/>';
                                    reportsmessage+='Current time is : '+currenttimestr+'<br/>';
                                    sendmail(email,reportsmessage);
                                    reportsmessage=reportsmessage.replace('<br/>',' ');
                                    String taskdescription='Candidate managements deleted: '+cms.size();
                                    taskdescription+=' Candidates deleted: '+cands.size();
                                    taskdescription+=' Current time is : '+currenttimestr;
                                    
                                    /*******insert the task as an evidence*******/
                                    inserttask(newtask,taskdescription);                        
                                }
                            } 
                        }
    
                    }catch(Exception ex){                       
                        inserttask(newtask,errormessageissue+ex);
                        NotificationClass.notifyErr2Dev('Error key:Scheduledeletejob ', ex); 
                    }
                }
             }
           
            
        }

    }

    private String getSelectedFields(String str){
        if(str !=null){
            str.toLowerCase();
            String prefix = 'select';
            String sufix = 'from';
            return str.substringBetween(prefix,sufix);
        }
        return null;
    }
}