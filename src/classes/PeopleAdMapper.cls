public with sharing abstract class PeopleAdMapper {
	
	public Map<String, Object> fieldMap4NewXML;	// This map is passed back to the Ad XML writer to generate the final XML
												// The key will be tag name. The Value will be tag value
	public Set<String> fieldNames;		// All field names of object Advertisement__c
	public Advertisement__c ad;

	public PeopleAdMapper(Advertisement__c ad) {
		this.ad = ad;
		fieldNames = getAllAdFieldNames();
	}

	public PeopleAdMapper() {
		fieldNames = getAllAdFieldNames();
	}

	public void setAd(Advertisement__c ad){
		this.ad = ad;
	}

	// Get all field names 
	public Set<String> getAllAdFieldNames(){
		Set<String> apiNames = Schema.SObjectType.Advertisement__c.fields.getMap().keySet();
		return apiNames;
	}

	// Generate XML for new or updated ad
	public Map<String, Object> getFieldMap4NewXML(){
		fieldMap4NewXML = new Map<String,Object>();
		generateNewXMLTags();
		return fieldMap4NewXML;
	}

	// Generate values for New or Updated XML
	public abstract void generateNewXMLTags();
}