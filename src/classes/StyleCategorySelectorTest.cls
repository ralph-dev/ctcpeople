/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class StyleCategorySelectorTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	list<CTCPeopleSettingHelper__c> ctcSettinghelpers=DataTestFactory.createCTCPeopleSettings();
    	System.assert(ctcSettinghelpers.size()>0);
    	}
    	
    }
    
    static testMethod void getUserAstutePayrollAccountTest() {
       
       System.runAs(DummyRecordCreator.platformUser) {
        	StyleCategory__c sc = StyleCategoryDummyRecordCreator.setJobboardStyleCategory();
        
        
            StyleCategorySelector scSelector = new StyleCategorySelector();
            list<StyleCategory__c> scs = scSelector.getUserAstutePayrollAccount(sc.Id);
            system.assert(scs.size()>0);
        }
    }
    
    
    static testMethod void getUserSaveSearchRecordTest() {
        System.runAs(DummyRecordCreator.platformUser) {
        	StyleCategory__c sc = StyleCategoryDummyRecordCreator.createScWithOwnerAndRT( (String)DummyRecordCreator.platformUser.Id , (String)DaoRecordType.GoogleSaveSearchRT.Id);
        
       
            StyleCategorySelector scSelector = new StyleCategorySelector();
            list<StyleCategory__c> scs = scSelector.getUserSaveSearchRecord();
            system.assert(scs.size()>0);
        }
    }
    
    static testMethod void getStyleCatagoryByIdTest() {
        
        System.runAs(DummyRecordCreator.platformUser) {
        	StyleCategory__c sc = StyleCategoryDummyRecordCreator.setJobboardStyleCategory();
        
            StyleCategorySelector scSelector = new StyleCategorySelector();
            StyleCategory__c scs = scSelector.getStyleCatagoryById(sc.Id);
            system.assert(scs!=null);
        }
    }
    
    static testMethod void testInsertCareerOneProtectedAccount(){
    	System.runAs(DummyRecordCreator.platformUser) {
    		StyleCategorySelector scSelector = new StyleCategorySelector();
    		StyleCategory__c sc = StyleCategoryDummyRecordCreator.buildCareerOneAccount();
    		
    		Boolean s = scSelector.insertCareerOneProtectedAccount(sc, 'username 24802279', 'test password');
    		
    		System.assertEquals(true, s);
    		
    		ProtectedAccount pa = scSelector.getCareerOneProtectedAccount();
    		System.assertNotEquals(null, pa);
    		
    		System.assertEquals('username 24802279',pa.getUsername());
    		System.assertEquals('test password',pa.getPassword());
    		
    		
    		System.assertEquals('test',(String) pa.getAccountFieldValue('Advertiser_Uploadcode__c'));
    		
    		
    		//update
    		sc.Advertiser_Uploadcode__c = 'updated';
    		s = scSelector.updateCareerOneProtectedAccount(sc, 'u1','p1', 'Advertiser_Uploadcode__c');
    		System.assertEquals(true, s);
    		
    		pa = scSelector.getCareerOneProtectedAccount();
    		System.assertEquals('u1',pa.getUsername());
    		System.assertEquals('p1',pa.getPassword());
    		System.assertEquals('updated',(String) pa.getAccountFieldValue('Advertiser_Uploadcode__c'));
    		
    	}
    }

    static testMethod void getStyleCategoriesByRecordTypeIdTest() {
        System.runAs(DummyRecordCreator.platformUser) {
            StyleCategorySelector scSelector = new StyleCategorySelector();
            StyleCategory__c sc = StyleCategoryDummyRecordCreator.createLinkedInAccount();
            StyleCategory__c scs = scSelector.getStyleCategoriesByRecordTypeId(sc.RecordTypeId).get(0);
            system.assert(scs!=null);
        }
    }
}