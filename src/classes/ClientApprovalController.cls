/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

public with sharing class ClientApprovalController {
    /*//** client portal to display all timesheet with their status
    public String queryStatus{get;set;}
    public Integer queryDays{get;set;}
    public String contactId ; //** approver internal SF contact record Id
    public User theuser ;
    public List<TimeSheetClass.TimeSheetRow> listings {get;set;}
    public String msg{get;set;}
    public List<String> selectedStatus {get;set;} 
    public string test1 {get;set;}
    public TimeSheet__c theTS {get;set;}
    public String TSID{get;set;} 
    public List<TimeSheet_Entry__c> timesheets{get;set;}
    Integer diff =0 ; 
    
    public ClientApprovalController(Boolean isTest, String theid){
        queryDays = -1;
        contactId = theid ;
        timesheets = new List<TimeSheet_Entry__c>();
        theTS = new TimeSheet__c();
    }
    public ClientApprovalController(){
        queryDays = -1;
        timesheets = new List<TimeSheet_Entry__c>();
        theTS = new TimeSheet__c();
        initCurrentUser(); //** identify who login from Portal
        setupStatus(new String[]{'Submitted'});
        initTSList(); //** first layer to display all candidate + vacancy basic information
        gettimezonediff();
    }
    public void initCurrentUser(){
        UserInformationCon usercls = new UserInformationCon();
        theuser = usercls.getUserDetail();
        contactId = theuser.ContactId;
    }
    
    public void setupStatus(List<String> s){
        //** approvers can view timesheet with 'submitted', 'rejected','approved' timesheets. 
        selectedStatus = new List<String>();
        selectedStatus.addAll(s);
    
    }
    public set<Id> vIds = new set<Id>();
    public void initTSList(){
    	
    	CommonSelector.checkRead(TimeSheet__c.sObjectType, 'Id ,Name, Vacancy__c , Vacancy__r.Name,Candidate__c,Candidate__r.LastName, Candidate__r.FirstName, TimeSheet_Status__c,CreatedDate, End_Date__c, Start_Date__c, End_Time__c, Start_Time__c, Shift_Total_Mins__c,Approver_Notes__c,Comments__c,Break_Total_Mins__c');
    	List<TimeSheet__c> tss = [select Id ,Name, Vacancy__c , Vacancy__r.Name,Candidate__c,
                                      Candidate__r.LastName, Candidate__r.FirstName, TimeSheet_Status__c,
                                      CreatedDate, End_Date__c, Start_Date__c, End_Time__c, Start_Time__c,
                                      Shift_Total_Mins__c,Approver_Notes__c,Comments__c,Break_Total_Mins__c
                                      from TimeSheet__c 
                                      where (Approver_Contact__c =:contactId or Backup_Approver__c =:contactId)
                                      and TimeSheet_Status__c in: selectedStatus 
                                      order by ID DESC
                                      limit 1000
                                      ];
    	getAvailableTimeSheets(tss);
    }
    
    public void getAvailableTimeSheets(List<TimeSheet__c> tss){
    
        String row = 'odd';
        msg ='';
        try{
        	listings = new List<TimeSheetClass.TimeSheetRow>();
            for(TimeSheet__c ts : tss){
                listings.add(new TimeSheetClass.TimeSheetRow(ts,false,ts.Candidate__c,ts.Vacancy__c,row));
                row = row == 'even' ? 'odd': 'even';
			}   
        }
        catch(exception e)
        {
            listings = new List<TimeSheetClass.TimeSheetRow>();
        }      
        if(listings.size() ==0)
        {
        	msg ='~~ There no vailable timesheets ~~';
        }
                   

    }
    public void initSelection(){
        
    
    
    }
    public void doApproval(){
        SubmitAll('Approved');
    
    }
    public void doRejection(){
        SubmitAll('Rejected');
    
    }
    public void SubmitAll(String sts){
       try{     
            msg ='';
            List<TimeSheet__c> selectedTS = new List<TimeSheet__c>(); 
            TimeSheet__c temp = new TimeSheet__c();
            for(TimeSheetClass.TimeSheetRow tsr : listings)
            {
                if(tsr.Selected)
                {
                    temp = tsr.TheTimeSheet;
                    temp.TimeSheet_Status__c = sts;
                    selectedTS.add(temp);
                }
            }
            if(selectedTS.size() >0)
            {
                fflib_SecurityUtils.checkFieldIsUpdateable(TimeSheet__c.SObjectType, TimeSheet__c.TimeSheet_Status__c);
	            update selectedTS;
	            msg = '~~ '+selectedTS.size()+' timesheet(s) '+ sts.toLowerCase() +' successfully ~~';
	            for(TimeSheetClass.TimeSheetRow tsr : listings)
	            {
	            	tsr.Selected = false;
	            }
            }
            else
            {
            	string t = sts == 'Approved'? 'approval':'rejection';
            	msg = '~~ There is no timesheet selected for '+ t +' ~~';
            }
       }
       catch(system.exception e)
       {
       		msg = 'System Error : Update Unsuccessfully! '+ e.getMessage() ;
       }
    
    }
    public void populateTS(){
        
        for(TimeSheetClass.TimeSheetRow tsr : listings)
        {
            if(tsr.TheTimeSheet.Id == TSID)
            {
                theTS = tsr.TheTimeSheet;
                
                break;
            }
        }
        
        CommonSelector.checkRead(TimeSheet_Entry__c.sObjecttype, 'Id, Date__c,Shift_Start_Time__c,Shift_End_Time__c,Shift_Type__c,Break_Start_Time__c, Break_Start_Time_2__c,Break_Start_Time_3__c,Break_End_Time__c, Break_End_Time_2__c,Break_End_Time_3__c,Day_of_Week__c, Length__c, Break_1_Total_Mins__c,Break_2_Total_Mins__c, Break_3_Total_Mins__c, Shift_Total_Mins__c, Comments__c,Break_Type_1__c,Break_Type_2__c,Break_Type_3__c');
        timesheets = [Select Id, Date__c,Shift_Start_Time__c,Shift_End_Time__c,Shift_Type__c,
                      Break_Start_Time__c, Break_Start_Time_2__c,Break_Start_Time_3__c,
                      Break_End_Time__c, Break_End_Time_2__c,Break_End_Time_3__c,
                      Day_of_Week__c, Length__c, Break_1_Total_Mins__c,Break_2_Total_Mins__c,
                      Break_3_Total_Mins__c, Shift_Total_Mins__c, Comments__c, 
                      Break_Type_1__c,Break_Type_2__c,Break_Type_3__c
                      from TimeSheet_Entry__c 
                      where TimeSheet__c =:theTS.Id limit 1000];
    }
    public pageReference queryHistorty(){
        if(queryDays == -1)
        {
            setupStatus(new List<String>{queryStatus});
            initTSList();
        }
        else
        {
            Set<Id> ts_ids = getHistories(queryDays,queryStatus);
            doQuery(ts_ids);
        }
        return null;
    }
    
    public Set<Id> getHistories(Integer days, String statusvalue){
        //** track the timesheet status
        	//system.debug('days =' + days);
            Date cur_date = Date.today() - days + 1;
            system.debug('cur_date =' + cur_date);
            Time myTime = Time.newInstance(0, 0, 0, 0);
            Datetime firsttime = Datetime.newInstanceGMT(cur_date, myTime);

            
            Set<Id> parentids = new Set<Id>();
            
            for(TimeSheet__History h : TimeSheetClass.HistoryTracking(true,'PeopleCloud1__TimeSheet_Status__c', firsttime.addMinutes(diff),null))
            {
                
                if(h.NewValue == statusvalue)
                {
                    parentids.add(h.parentId);
                }
            }
            
            system.debug('parentids ='+ parentids);
            
            return parentids;
        
    }
    public void doQuery(Set<Id> ids){
        try{
        	CommonSelector.checkRead(TimeSheet__c.sObjectType, 'Id ,Name, Vacancy__c , Vacancy__r.Name,Candidate__c,Candidate__r.LastName, Candidate__r.FirstName, TimeSheet_Status__c,CreatedDate, End_Date__c, Start_Date__c, End_Time__c, Start_Time__c, Shift_Total_Mins__c,Approver_Notes__c, Comments__c ');
            List<TimeSheet__c> tss = [select Id ,Name, Vacancy__c , Vacancy__r.Name,Candidate__c,
                                      Candidate__r.LastName, Candidate__r.FirstName, TimeSheet_Status__c,
                                      CreatedDate, End_Date__c, Start_Date__c, End_Time__c, Start_Time__c,
                                      Shift_Total_Mins__c,Approver_Notes__c, Comments__c 
                                      from TimeSheet__c 
                                      where Id in: ids
                                      and (Approver_Contact__c =:contactId or Backup_Approver__c =:contactId)
                                      order by ID DESC
                                      ];
            getAvailableTimeSheets(tss);
           // msg ='There are '+tss.size()+ ' Timesheets';
        }
        catch(system.exception e)
        {
            if(e.getMessage().contains('Inline query has too many rows'))
            {
                msg = 'Information: Search limit exceeded - Please reduce the scope of your search.';
            }
            else
            {
                msg = 'Query Result: There is no result for this period.';
            }
        
        }

    }
    public void gettimezonediff(){
            
            Datetime Local_Datetime = Datetime.newInstance(2000,1,20,7,20,30);
            Datetime GMT_Datetime = Datetime.newInstanceGmt(2000,1,20,7,20,30);
            system.debug('GMT_Datetime='+ GMT_Datetime);
            
            system.debug('Local_Datetime ='+ Local_Datetime);
            system.debug('GMT_Datetime hour ='+ GMT_Datetime.hourGMT());
            system.debug('Local_Datetime hour ='+ Local_Datetime.hourGMT());
            
            
            diff = (Local_Datetime.dayGmt() - GMT_Datetime.dayGMT()) * 24 * 60 + (Local_Datetime.hourGmt() - 
            				GMT_Datetime.hourGmt())*60 + (Local_Datetime.minuteGmt() - GMT_Datetime.minuteGmt());
            				
            system.debug('difference =' + diff);

            
    }
    //--------------- test Method -------------------
 	static testmethod void testcontroller() {   
 		System.runAs(DummyRecordCreator.platformUser) {
 			Contact c = new Contact(LastName =' haha', Email='noduplicate@test.com');
	 		insert c ; 
	 		Placement__c v = new Placement__c(Name='testthiscon',Stage__c ='JobOn', TimeSheet_Approver__c = c.Id, Online_Job_Title__c ='Test manager' );
		 	insert v;
		 	Placement_Candidate__c pc = new Placement_Candidate__c(Candidate__c = c.Id, Placement__c = v.Id, Candidate_Status__c ='Placed');
		 	insert pc;
		 	TimeSheet__c ts = new TimeSheet__c(Vacancy__c = v.Id, Candidate__c = c.Id, Approver_Contact__c = c.Id, TimeSheet_Status__c ='Submitted');
			insert ts;
		 	List<TimeSheet__c> tss = new List<TimeSheet__c>{ts}; 
		 	Test.startTest();
		 		ClientApprovalController thecon = new ClientApprovalController(true, c.Id);
		 		thecon.setupStatus(new String[]{'Submitted'});
		        thecon.initTSList(); //** first layer to display all candidate + vacancy basic information
		        system.assert(thecon.listings.size()>0);
		        thecon.gettimezonediff();
		        system.assert(thecon.diff!= null);
		        thecon.populateTS();
		        system.assert(thecon.timesheets.size()== 0);
		        thecon.doApproval();
		        system.assert(thecon.msg != '');
		        thecon.doRejection();
		        system.assert(thecon.msg != '');
		        if(thecon.listings != null)
		        {
		        	for(TimeSheetClass.TimeSheetRow tsr: thecon.listings)
		        	{
		        		tsr.Selected = true;
		        	}
		        } 
		        thecon.doApproval();
		        system.assert(thecon.msg != '');
		        thecon.doRejection();
		        system.assert(thecon.msg != '');
		        thecon.queryHistorty();
		        thecon.queryDays = 2 ;
		        thecon.queryHistorty();
		        thecon.initSelection();
		        system.assert(thecon.listings.size()== 0);
	        Test.stopTest();
 		}
 		
 		
 	}*/
 	
}