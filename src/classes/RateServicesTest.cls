@isTest
private class RateServicesTest {
    static testMethod void unitTest(){
    	System.runAs(DummyRecordCreator.platformUser) {
        list<Rate__c> rates=DataTestFactory.createRates();
        RateServices  rateServ=new RateServices ();
        list<Map<String,Object>> convertedRates=rateServ.convertRatesToRateChange(rates);
        System.assert(convertedRates.size()>0);
    	}
        
    }

}