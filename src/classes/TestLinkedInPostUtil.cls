@isTest
public with sharing class TestLinkedInPostUtil {
	public static testmethod void testgetUserLinkedInCounry(){
		System.runAs(DummyRecordCreator.platformUser) {
		Advertisement__c testad = TestLinkedInCloneAd.dummyAd();
		String tempTestString = LinkedinPostUtil.getPickListFieldMapJsonString();
		String initcountry = LinkedInPostUtil.getUserLinkedInCounry(tempTestString);
		system.assertEquals(initcountry,'Australia');
		Boolean testchecklinkedin = LinkedInPostUtil.checklinkedInAccountEnable();
		String tempString = 'test';
		LinkedInPostUtil.removeMultipicklistSquare(tempString);
		}
	}
	
    public static string dummyString(){
        Map<String,String> dummyMapString = new Map<String,String>();
        Map<String, LinkedinCountryCode> newlinkedinmapcode = new Map<String, LinkedinCountryCode>();
        Map<String, LinkedinCountryCode> newJobFunctionCode = new Map<String, LinkedinCountryCode>();
        
        LinkedinCountryCode auscountrycode = new LinkedinCountrycode();
        auscountrycode.Code = 'au';
        auscountrycode.requirePostCode = true;
        LinkedinCountryCode uscountrycode = new LinkedinCountrycode();
        uscountrycode.Code = 'us';
        uscountrycode.requirePostCode = true;
        LinkedinCountryCode bfcountrycode = new LinkedinCountrycode();
        bfcountrycode.Code = 'bf';
        bfcountrycode.requirePostCode = false;
        
        newlinkedinmapcode.put('Australia', auscountrycode);
        newlinkedinmapcode.put('US', uscountrycode);
        newlinkedinmapcode.put('Burkina Faso',bfcountrycode);

        LinkedinCountryCode acctJobFunctioncode = new LinkedinCountrycode();
        acctJobFunctioncode.Code = 'acct';
        acctJobFunctioncode.requirePostCode = false;
        LinkedinCountryCode admJobFunctioncode = new LinkedinCountrycode();
        admJobFunctioncode.Code = 'adm';
        admJobFunctioncode.requirePostCode = false;
        
        newJobFunctionCode.put('Accounting/Auditing',acctJobFunctioncode);
        newJobFunctionCode.put('Administrative',admJobFunctioncode);
        
        Map<String, LinkedinCountryCode> newJobIndustryCode = new Map<String, LinkedinCountryCode>();
        LinkedinCountryCode acctJobIndustry = new LinkedinCountrycode();
        acctJobIndustry.Code = '47';
        acctJobIndustry.requirePostCode = false;
        LinkedinCountryCode itJobIndustry = new LinkedinCountrycode();
        itJobIndustry.Code = '96';
        itJobIndustry.requirePostCode = false;
        
        newJobIndustryCode.put('Accounting',acctJobIndustry);
        newJobIndustryCode.put('Information Technology and Services',itJobIndustry);
        
        Map<String, LinkedinCountryCode> newJobExperienceLevelCode = new Map<String, LinkedinCountryCode>();
        LinkedinCountryCode executiveJobExperienceLevel = new LinkedinCountrycode();
        executiveJobExperienceLevel.Code = '6';
        executiveJobExperienceLevel.requirePostCode = false;
        LinkedinCountryCode directorJobExperienceLevel = new LinkedinCountrycode();
        directorJobExperienceLevel.Code = '5';
        directorJobExperienceLevel.requirePostCode = false;
        
        newJobExperienceLevelCode.put('Executive',executiveJobExperienceLevel);
        newJobExperienceLevelCode.put('Director',directorJobExperienceLevel);
        
        Map<String, LinkedinCountryCode> newJobTypeCode = new Map<String, LinkedinCountryCode>();
        LinkedinCountryCode fullJobType = new LinkedinCountrycode();
        fullJobType.Code = 'F';
        fullJobType.requirePostCode = false;
        LinkedinCountryCode partJobType = new LinkedinCountrycode();
        partJobType.Code = 'P';
        partJobType.requirePostCode = false;
        
        newJobTypeCode.put('Full-time',fullJobType);
        newJobTypeCode.put('Part-time',partJobType);
        
        Map<String, LinkedinCountryCode> newPostingRoleCode = new Map<String, LinkedinCountryCode>();
        LinkedinCountryCode HpostingRoleType = new LinkedinCountrycode();
        HpostingRoleType.Code = 'H';
        HpostingRoleType.requirePostCode = false;
        LinkedinCountryCode RpostingRoleType = new LinkedinCountrycode();
        RpostingRoleType.Code = 'R';
        RpostingRoleType.requirePostCode = false;
        LinkedinCountryCode SpostingRoleType = new LinkedinCountrycode();
        SpostingRoleType.Code = 'S';
        SpostingRoleType.requirePostCode = false;
        LinkedinCountryCode WpostingRoleType = new LinkedinCountrycode();
        WpostingRoleType.Code = 'W';
        WpostingRoleType.requirePostCode = false;
        
        newPostingRoleCode.put('Hiring Manager',HpostingRoleType);
        newPostingRoleCode.put('Company Recruiter',RpostingRoleType);
        newPostingRoleCode.put('Staffing Firm',SpostingRoleType);
        newPostingRoleCode.put('Works at Company',WpostingRoleType);
        
        //linkedincountry.addAll(newlinkedinmapcode.keyset());
        /*List<String> linkedinJobFunction = new List<String>{'Accounting / Auditing','Administrative'};
        List<String> linkedinJobIndustry = new List<String>{'Accounting','Information Technology and Services'};
        List<String> linkedinJobExperienceLevel = new List<String>{'Executive','Director'};
        List<String> linkedinJobType = new List<String>{'Full-time','Part-time'};*/
        
        String jsonlinkedincountry = JSON.serialize(newlinkedinmapcode);
        String jsonlinkedinJobFunction = JSON.serialize(newJobFunctionCode);
        String jsonlinkedinJobIndustry = JSON.serialize(newJobIndustryCode);
        String jsonlinkedinJobExperienceLevel = JSON.serialize(newJobExperienceLevelCode);
        String jsonlinkedinJobType = JSON.serialize(newJobTypeCode);
        String jsonlinkedinPostingRole = JSON.serialize(newPostingRoleCode);
        
        dummyMapString.put('linkedincountry',jsonlinkedincountry);
        dummyMapString.put('linkedinJobFunction',jsonlinkedinJobFunction);
        dummyMapString.put('linkedinJobIndustry',jsonlinkedinJobIndustry);
        dummyMapString.put('linkedinJobExperienceLevel',jsonlinkedinJobExperienceLevel);
        dummyMapString.put('linkedinJobType',jsonlinkedinJobType);
        dummyMapString.put('linkedinPostingRole',jsonlinkedinPostingRole);
        String result = JSON.serialize(dummyMapString);
        system.assertNotEquals(null, result);
        return result;
    }
}