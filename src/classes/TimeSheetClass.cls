/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

public with sharing class TimeSheetClass{
    /*public static String TimeSheetClassMsg {get;set;}
    public class TimeSheet //** time sheet entry
    {
    	private Timesheet_Entry__c TimeSheetEntry ;
        private Integer theSequence ;
        private String class_name; //** css style
        public String Shift_StartTime {get;set;} //** for display purpose
        public String Shift_EndTime {get;set;}
        public String Break_StartTime_1 {get;set;} //** for display purpose
        public String Break_StartTime_2 {get;set;}
        public String Break_StartTime_3 {get;set;}
        public String Break_EndTime_1 {get;set;}
        public String Break_EndTime_2 {get;set;}
        public String Break_EndTime_3 {get;set;}
        
        public TimeSheet(Timesheet_Entry__c te, Integer i){
            this.TimeSheetEntry = te;
            this.theSequence = i;
            Shift_StartTime = DecimaltoTimeStr(te.Shift_Start_Time__c);
            Shift_EndTime = DecimaltoTimeStr(te.Shift_End_Time__c);
            Break_StartTime_1 = DecimaltoTimeStr(te.Break_Start_Time__c);
			Break_StartTime_2 = DecimaltoTimeStr(te.Break_Start_Time_2__c);
			Break_StartTime_3 = DecimaltoTimeStr(te.Break_Start_Time_3__c);
			Break_EndTime_1 = DecimaltoTimeStr(te.Break_End_Time__c);
			Break_EndTime_2 = DecimaltoTimeStr(te.Break_End_Time_2__c);
			Break_EndTime_3 = DecimaltoTimeStr(te.Break_End_Time_3__c);
        }
        public Timesheet_Entry__c getTimeSheetEntry(){
            return TimeSheetEntry;
        }
        public void setTimeSheetEntry(Timesheet_Entry__c te){
            this.TimeSheetEntry = te;
        }
        public Integer getTheSequence(){
            return theSequence;
        }
        public void setTheSequence(Integer i){
            this.theSequence = i;
        }
        public String getClass_name(){        	
        	return class_name; 
        }
        public void setClass_name(String b){        	
        	class_name = b; 
        }
    }
    
    public static List<TimeSheet> pushNewEntry(Timesheet_Entry__c te, Integer theIndex,  List<TimeSheet> timesheets){
        timesheets.add(new TimeSheetClass.TimeSheet(te,theIndex));
        return timesheets;
    }
    
    public static List<TimeSheet> deleteEntry(String theseq, List<TimeSheet> timesheets){
        try{            
            String seq = theseq;
            Integer todelete ;
            if(seq != null)
            {
                Integer thes = Integer.valueOf(seq);
                for(Integer i=0; i< timesheets.size(); i++)
                {
                    if(timesheets[i].getTheSequence() == thes)
                    {
                        todelete = i;
                        break;
                    }
                }
                if(todelete != null)
                {
                    TimeSheet tse = timesheets.remove(todelete);
                    if(tse != null)
                    {
                        Timesheet_Entry__c te = tse.getTimeSheetEntry();
                        fflib_SecurityUtils.checkObjectIsDeletable(te.getSObjectType());
                        delete te;
                        TimeSheetClassMsg = 'Delete Successfully!';
                    }
                    //system.debug('timesheets =' + timesheets);
                }

            }

        }
        catch(system.Exception e)
        {
        	
            TimeSheetClassMsg = 'Delete Successfully!';
            
        }
        return timesheets;
        
    }
    
    public static Id CreateNewTimeSheet(String Vid, String Cid, String TSstatus){
    	try{
	    	if(Vid == null || Cid == null)
	    	{ return null;}
	    	Placement__c v= DAOPlacement.getVById(Vid);
	    	TimeSheet__c newTS = new TimeSheet__c(Vacancy__c = Vid, Candidate__c = Cid , TimeSheet_Status__c = TSstatus);
	    	if(v !=null && v.Internal_Approver__c != null)
	    		newTS.Approver__c =  v.Internal_Approver__c;
            //check FLS
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                TimeSheet__c.Vacancy__c,
                TimeSheet__c.Candidate__c,
                TimeSheet__c.TimeSheet_Status__c
            };
            fflib_SecurityUtils.checkInsert(TimeSheet__c.SObjectType, fieldList);
	    	insert newTS;
	    	return newTS.Id;
    	}
    	catch(exception e)
    	{
    		System.debug(e);
    	}
    	return null;
    	
    }
    public static String DecimaltoTimeStr(Decimal timed){
    	
    	
    	 //** decimal to string to display on page
		 if(timed == null)
         {
         	return null;
         }
         String temp = String.valueOf(timed);
         temp = temp.replace('.',':');
		 return temp;
         
    
    }
    public static Decimal TimeStrtoDecimal(String times){
    	//** string to decimal to store in system
         if(times == null || times == '')
         {
         	return null;
         }
         times = times.replace(':','.');

         Decimal temp = Decimal.valueOf(times);
         return temp;
    	
    }
    public Static List<SelectOption> initApprovers(String companyid, Placement__c v){
        List<SelectOption> temps = new List<SelectOption>();
        List<String> rtIds = new List<String>();
        List<String> approverids = new List<String>();
        String idstring ;
        try{
        	
            //** eg.get coporate contact recordtype, coporate candidate recordtype ...
            if(Test.isRunningTest()){
            	idstring = DaoRecordType.indCandidateRT.id;
            }
            else{
            	idstring = TimeSheetInfo__c.getInstance().People_RecordType_IDs__c;
            }
	        if(idstring != null && idstring != '')
	        {
	        	rtIds = idstring.split(';');
	        }
	        
	        
            //**  get default approvers (setup on vacancy record)
            if(v.Backup_Approver__c != null){
            	approverids.add(v.Backup_Approver__c);
            	
            }
            
            if(v.TimeSheet_Approver__c != null){
            	
            	approverids.add(v.TimeSheet_Approver__c);
            }
            
            
            Contact[] cons; 
            if(companyid != null)
            {
            	
            	CommonSelector.checkRead(Contact.sObjectType,'Id, LastName, FirstName,Email');
            	if(approverids.size() > 0){
            		cons = [select Id, LastName, FirstName,Email from Contact 
                		where AccountId =:companyid 
                		and (RecordType.Id in: rtIds or Id in:approverids)];
            	}else{
            		cons = [select Id, LastName, FirstName,Email from Contact 
                		where AccountId =:companyid 
                		and RecordType.Id in: rtIds ];
            	}
                
            }
            //system.debug('cons = '+ cons);
            if(cons != null)
            {
                for(Contact c : cons)
                {
                    temps.add(new SelectOption(c.Id , c.FirstName +' '+ c.LastName) );                
                }            
            }
            //system.debug('temps =' + temps);
            return temps;            
        }
        catch(system.exception e)
        {
        	System.debug(e);
        
        }
    	return null;
    }
    
    public Static List<SelectOption> populateCandidate(Placement__c v){
    	List<SelectOption> result = new List<SelectOption>();
    	try{
    		CommonSelector.checkRead(Placement_Candidate__c.sObjectType,'Id,Candidate__r.LastName,Candidate__r.FirstName, Candidate__c');
    		List<Placement_Candidate__c> candidates = [select  Id,Candidate__r.LastName,
    													Candidate__r.FirstName, Candidate__c
											    		from Placement_Candidate__c 
											    		where Placement__c =:v.Id 
											    		and Candidate_Status__c ='Placed'];
			//system.debug('candidates ='+ candidates);
    		for(Placement_Candidate__c c : candidates)
    		{
    			result.add(new SelectOption(c.Candidate__c , c.Candidate__r.FirstName +' '+ c.Candidate__r.LastName));
    		    
    		}    		
    	}
    	catch(system.exception e)
    	{
    		system.debug(e);
    	
    	}
    	return result;
    	
    }
    
    public Static TimeSheet__c getTheTimeSheet(String tid){
    	CommonSelector.checkRead(TimeSheet__c.sObjectType, 'Id, Name,Vacancy__c, Candidate__c, Start_Date__c, End_Date__c, Start_Time__c, End_Time__c,TimeSheet_Status__c ,Candidate__r.Name,Approver__c, Approver_Contact__c, CreatedDate, Comments__c,Backup_Approver__c,Approver_Notes__c');
		return [select Id, Name,Vacancy__c, Candidate__c, Start_Date__c, End_Date__c, 
               Start_Time__c, End_Time__c,TimeSheet_Status__c ,Candidate__r.Name,Approver__c,
               Approver_Contact__c, CreatedDate, Comments__c,Backup_Approver__c,Approver_Notes__c
               from TimeSheet__c where id=:tid];
	
	}
    //*************************** class for portal timesheet list
   
    //** for client portal to display timesheets in a list. apporver can mass aprove all and view one. 
    //** used in class ClientApprovalController
    public class TimeSheetRow
    {
        public TimeSheet__c theTimeSheet{get; set;}
        public Id cId{get; set;}
        public Id vId{get; set;}
        public Boolean selected {get; set;}

        public String rownum{get; set;}
        public TimeSheetRow(Timesheet__c ts, Boolean s, Id cid, Id vid, String r){
            this.theTimeSheet = ts;
            this.selected = s;
            this.cId = cid;
            this.vId = vid;
            this.rownum = r;
        }
        public void doSave(){
        	upsert this.theTimeSheet ; 
        }        
    }    
    
    //*********************** timesheet history tracking************************************
	public Static List<TimeSheet__History> HistoryTracking(Boolean isClient,String fieldname, Datetime firstdate , String parentId){
		try{
			CommonSelector.checkRead(TimeSheet__History.sObjectType,'ParentId, OldValue, NewValue, IsDeleted, Id, Field, CreatedDate, CreatedById');
			if(isClient){
				
				return [Select ParentId, OldValue, NewValue, IsDeleted, 
						Id, Field, CreatedDate, CreatedById
						From TimeSheet__History
						where Field =: fieldname
						and CreatedDate >=: firstdate	
						order by ParentId
						limit 1000];
			}else{
				return [Select ParentId, OldValue, NewValue, IsDeleted, 
						Id, Field, CreatedDate, CreatedById
						From TimeSheet__History
						where Field =: fieldname  
						and ParentId =: parentId	
						order by CreatedDate DESC
						limit 1000];
				}
		}
		catch(system.exception e)
		{
			System.debug(e);
		}
		return new List<TimeSheet__History>();
	}
	
	public static boolean validateTimeSheet(TimeSheet__c ts){
    	try{
	    	system.debug('ts = '+ts.Start_Date__c+';'+ts.End_Date__c);
	    	if(ts.Start_Date__c != null && ts.End_Date__c != null )
	    	{
	    		if(ts.Start_Date__c > ts.End_Date__c)
	    		{
	   				//errTS ='Validation Error: Update Failed. Please make sure Start Date not greater than End Date.';
	   				TimeSheetClassMsg = 'Validation Error: Failed. Please make sure Start Date not greater than End Date.';
	   				
	   				return false;
	    		}
	    	}
	    	else
	    	{
	    		
	    		TimeSheetClassMsg ='Validation Error: Failed. Both Start Date and End Date are required fields.';
	    		return false;
	    	}
	    	if(ts.Start_Time__c == null || ts.End_Time__c == null)
	    	{
	    		TimeSheetClassMsg ='Validation Error: Failed. Both Start Time and End Time are required fields.';
	    		return false;
	    	}
	    	if(ts.Candidate__c == null)
	    	{
	    		TimeSheetClassMsg = 'Validation Error: Failed. Candidate can not be blank.';
	    		return false;
	    	}
	    	//msgTS ='Updated Successfully!';
	    	return true;
    	}
    	catch(system.exception e)
    	{
    		system.debug('error=' + e);
    		
    	}
    	return false;
    }
	//*********************test method ***************************
	static testmethod void testcontroller() {  
		
		Integer seq = 1; 
		Account dummyaccount = new Account(Name='test');
		insert dummyaccount;
		Contact c = new Contact(LastName='suiza', Email='dfdffd@test.com', AccountId = dummyaccount.id, RecordTypeId = DaoRecordType.indCandidateRT.id);
		insert c;
		Placement__c p = new Placement__c(Name='Test ok ', Online_Job_Title__c ='Test Manager', Company__c=dummyaccount.id);
		insert p;
		TimeSheet__c ts = new TimeSheet__c(Vacancy__c = p.Id, Candidate__c = c.Id, Start_Date__c= date.newinstance(1960, 2, 17), End_Date__c = Date.today(), Start_Time__c = 11.00, End_Time__c= 11.00); 
		insert ts;
		Placement_Candidate__c pc1 = new Placement_Candidate__c(Placement__c = p.Id ,Candidate__c = c.Id, Candidate_Status__c = 'Placed');
		insert pc1;
		
		System.runAs(DummyRecordCreator.platformUser) {
		TimeSheetRow tsr = new TimeSheetRow(ts,true,c.id,p.id,'12346');
		//system.debug('c='+ c);
		system.debug(ts.Start_Time__c);
		//static method test code
		String timestring = TimeSheetClass.DecimaltoTimeStr(ts.Start_Time__c);
		system.assert(timestring.contains(':'));
		Decimal TimeStrtoDecimal = TimeSheetClass.TimeStrtoDecimal(timestring);
		system.assertnotEquals(TimeStrtoDecimal, null);
		
		//system.debug('p='+ p);
		List<SelectOption> initApprovers = TimeSheetClass.initApprovers(dummyaccount.id, p);
		system.assert(initApprovers.size()>0);
		
		List<SelectOption> populateCandidate = TimeSheetClass.populateCandidate(p);
		system.assert(populateCandidate.size()>0);
		
		TimeSheet__c getTheTimeSheet = TimeSheetClass.getTheTimeSheet(ts.id);
		system.assert(getTheTimeSheet!= null);
		
		Timesheet_Entry__c te = new Timesheet_Entry__c(TimeSheet__c = ts.Id );
		Test.StartTest();
		TimeSheetClass.TimeSheet tsc = new TimeSheetClass.TimeSheet(te, seq);
		tsc.getClass_name();
		tsc.getTheSequence();
		tsc.getTimeSheetEntry();
		tsc.setClass_name('showerror');
		tsc.setTheSequence(2);
		tsc.setTimeSheetEntry(te);
		List<TimeSheetClass.TimeSheet> timesheets = new List<TimeSheetClass.TimeSheet>{tsc};
		List<TimeSheet> pushNewEntry = TimeSheetClass.pushNewEntry(te, 3, timesheets );
		system.assert(pushNewEntry.size()>0);
		List<TimeSheet> deleteEntry = TimeSheetClass.deleteEntry('3', timesheets);
		system.assert(deleteEntry.size()>0);
       	Id CreateNewTimeSheet = TimeSheetClass.CreateNewTimeSheet(p.Id, c.Id , 'In Progress');
       	system.assertnotEquals(CreateNewTimeSheet, null);
       	system.assert(TimeSheetClass.validateTimeSheet(ts));
		Test.StopTest();
		}
		
	} */
}