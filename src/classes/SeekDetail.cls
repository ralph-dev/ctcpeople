/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

public with sharing class SeekDetail{
    public Advertisement__c job{get; set;}
    public Advertisement__c newJob{get; set;}
    public boolean show_page{get;set;}
    public boolean remaining_quota_enough{get;set;}
    public String orgid{get; set;}
    public String seekSaveInfo{get; set;} 
    public String bucketname ;
    public String screenId;
    public String LogoId{get;set;}
    public String templateId;
    public String jobContentSeekTemp{get; set;}
    public String jobOnlineFooterTemp{get;set;}
    public String err_msg{get;set;}
    public Integer reminingquota{get;set;}
    public boolean org_level_seekAPI{get;set;}
    
    integer usage , quota;
    String tempSalaryText='';

    User u ;
    UserInformationCon usercls = new UserInformationCon();
    Set<String> internationalRegions = new Set<String>();
    Map<String,String> Countries = new Map<String,String>();
    public boolean isNext{get;set;}
    
    /*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}    
    /*************End Add skill parsing param********************************************************/

	//************Start Add Multi Seek Account Param, Author by Jack on 26/05/2013******************
    public String selectSeekAccount;
    public String displayCompanyName;
    //*************End Add Multi Seek Account Param*************************************************
    
    public boolean enablePostingAdWithEmail {get; set;}
    public String preferEmailAddress;
    
    //Init Error Message for customer
    public PeopleCloudErrorInfo errormesg; 
    public Boolean msgSignalhasmessage{get ;set;} //show error message or not
    
    public SeekDetail(ApexPages.StandardController stdController){
    	//Init skill parsing param. Add by Jack on 23/05/2013
        enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
        enableSkillGroupList = new List<Skill_Group__c>();
        enableSkillGroupList = SkillGroups.getSkillGroups();
        defaultskillgroups = new List<String>();
        //end skill parsing param.
        
        //Init the Job Posting with Email function
        JobPostingSettings__c userJobPostingSetting = SeekJobPostingSelector.getJobPostingCustomerSetting();
        enablePostingAdWithEmail = userJobPostingSetting.Posting_Seek_Ad_with_User_Email__c;
        
        this.job = (Advertisement__c) stdController.getRecord();
        
        //Get prefer email address according the admin setting
        if(job.Prefer_Email_Address__c == null || job.Prefer_Email_Address__c == '') {
        	job.Prefer_Email_Address__c = SeekJobPostingSelector.getUserPreferEmailAddress();
        }
        
        if(this.job.Skill_Group_Criteria__c!=null&&this.job.Skill_Group_Criteria__c!=''){
        	String defaultskillgroupstring = this.job.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
	        defaultskillgroups = defaultskillgroupstring.split(',');
        }
     }
     
	//insert the skill group picklist value option
    public List<SelectOption> getSelectSkillGroupList(){
	  	List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
	  	List<SelectOption> displaySelectOption = new List<SelectOption>();
	  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
	  		displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
	  	}
	  	return displaySelectOption;
	}
	
    public void init(){
        u = usercls.getUserDetail();
        reminingquota = usercls.checkquota(1);
        system.debug('remaining quota =' + reminingquota);
        show_page = true;
        org_level_seekAPI = false;
        
        //******** Added 12 July 2010*********
        if(u.AmazonS3_Folder__c != null){
        	bucketname = u.AmazonS3_Folder__c.trim();
        }
        else{
        	msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.ERR_S3_ACCOUNT_FAILURE); 
            show_page = false;
        }
        
        //Start Add multi Seek posting account
        selectSeekAccount = '';
        if(u.Seek_Account__c != null && u.Seek_Account__c != ''){//check user seek account 
        	selectSeekAccount = u.Seek_Account__c;
        	StyleCategory__c sc = DaoStyleCategory.getSeekAccountByStyleCategoryId(selectSeekAccount);
        	if(sc!=null){
            	org_level_seekAPI = sc.Seek_Default_Application_Form__c;
	        }else{
	            org_level_seekAPI = false;
	            msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.SEEK_JOBBOARD_ACCOUT);
	            return;
	        }
        }
        else{
        	msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.SEEK_JOBBOARD_ACCOUT);
            show_page = false;
            return;
        }
        //End Add multi Seek posting account************************************
        
        orgid = UserInfo.getOrganizationId().substring(0, 15);
        show_page = getStyles(org_level_seekAPI);//get the styles successful
    }
   
    public String appformname {get;set;}
    public String tempname {get;set;}
    
    //****************** get seek.com account template information ************
    public boolean getTemplateDetail(){    
        try{
        	boolean info_valid = true;
        	if(newjob.Template__c != null){
                CommonSelector.checkRead(StyleCategory__c.SObjectType,'Id, Name,templateSeek__c, screenId__c, LogoID__c');
            	StyleCategory__c  sc = [Select Id, Name,templateSeek__c, screenId__c, LogoID__c from StyleCategory__c where Id=:newjob.Template__c];
            	system.debug('templateid =' + newjob.Template__c);
	            if(sc != null){
	                if(sc.templateSeek__c == '' || sc.templateSeek__c == null){
	                    info_valid = false;
	                }
	                else{
	                    //----- Changed by Gilberto for enabling Seek Export API
                        CommonSelector.checkRead(StyleCategory__c.SObjectType,'Id, screenId__c');
	                    StyleCategory__c[]  scSeekScreens = [Select Id, screenId__c from StyleCategory__c where Id=:newjob.SeekScreen__c];
	                    StyleCategory__c  scSeekScreen = null;            
	                    //get screen Id
	                    if (scSeekScreens.size() > 0){
	                        scSeekScreen = scSeekScreens[0];
	                    }
	                    screenId = '' ;
	                    if(scSeekScreen != null){
	                        if(scSeekScreen.screenId__c != null){
	                            screenId = scSeekScreen.screenId__c;
	                        }
	                    }   
	                    //----- End of Changed by Gilberto for enabling Seek Export API
	                    if(sc.LogoId__c == null || sc.LogoId__c == ''){
	                        LogoId = '';
	                    }
	                    else{
	                        LogoId = sc.LogoId__c;
	                    }
	                templateId = sc.templateSeek__c;
	                }
	            }
	            else{
	                info_valid = false;
	            }
	        }
	        else{
	            info_valid = false;
	        }
        	return info_valid;
        }
        catch(system.exception e){
        	NotificationClass.notifyErr2Dev('getTemplateDetail() - Seek Export API code', e);
        }
        return false;    
    }
    
    public pageReference dopost(){
    	system.debug('--------------------------1');
        remaining_quota_enough = true;
        reminingquota = usercls.checkquota(1);
        if(reminingquota <= 0){
            seekSaveInfo = 'Sorry. You have no more quota to post this Ad. to Seek.com now.';
            remaining_quota_enough = false;
            return null;
        }
        
        String tempfooter = '';
        job.online_footer__c = jobOnlineFooterTemp;
        if(job.online_footer__c != null)
        {
            tempfooter = job.online_footer__c.replaceAll('\\[\\%User Name\\%\\]',UserInfo.getName());
            tempfooter = tempfooter.replaceAll('\\[\\%Vacancy Referece No\\%\\]',job.Reference_No__c );
        }
        
        job.Online_Footer__c = tempfooter;
        newJob = job.clone(false, true);
        newJob.WebSite__c = 'Seek';
        newjob.Job_Content__c = jobContentSeekTemp;
        newJob.Seek_Account__c = selectSeekAccount;//assign seek account
        newJob.RecordTypeId = DaoRecordType.seekAdRT.Id;// assign record type
        if(!getTemplateDetail()){
            msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.JOBBOARD_TEMPLATE_ERROR);
            return null;
        }
        try{
            checkFLS();
            insert newJob;
        }catch(System.Exception e){
            System.debug(e.getMessage());
        }
        System.debug('newJob ID = '+newJob.id);
      	//String aid = String.valueOf(newJob.id).substring(0, 15);									//general ad id
        String aid = String.valueOf(newJob.id); //Use 18 characters id as the Job Reference ID
        String orgid = UserInfo.getOrganizationId().substring(0, 15);
        String JobRefCode = orgid + ':' +aid;														//general JobReferenceId
      	
      	//general job xml
      	newJob.JobXML__c = JobboardgeneralXML.generalSeekJobXML(newJob, templateId, screenId, LogoID, JobRefCode);
       	
       	//insert skill group ext id
	    if(enableskillparsing){
	    	String tempString  = String.valueOf(defaultskillgroups);
	        tempString = tempString.replaceAll('\\(', '');
	        tempString = tempString.replaceAll('\\)', '');
	        tempString = tempString.replace(' ','');
	        newJob.Skill_Group_Criteria__c = tempString;
	    }
	    //end insert skill group ext id
        
        StyleCategory__c selectedSc = style_map.get(newJob.Application_Form_Style__c);
        
        //** callout future insert to ec2
        if(selectedSc != null)//if application form existing and SAE disable, insert data to ec2
        {
            if(!Test.isRunningTest())
            insertDetailTable(JobRefCode, orgid, aid, bucketname,
             selectedSc.Header_EC2_File__c ,selectedSc.Header_File_Type__c , selectedSc.Div_Html_EC2_File__c, selectedSc.Div_Html_File_Type__c, 
             selectedSc.Footer_EC2_File__c, selectedSc.Footer_File_Type__c, newJob.Template__c, 'seek', newJob.Job_Title__c);
        }
        newJob.Status__c = 'Active';
        newJob.Job_Posting_Status__c = 'In Queue';
        try{
            checkFLS();
            update newJob;
            CommonSelector.checkRead(User.SObjectType,'Id,Seek_Usage__c');
            User thisuser = [select Id,Seek_Usage__c from User where Id=:UserInfo.getUserId()];
            usage = (thisuser.Seek_Usage__c == null || thisuser.Seek_Usage__c == '')? 0 : Integer.valueOf(thisuser.Seek_Usage__c);
            thisuser.Seek_Usage__c = String.valueOf(usage + 1);
            fflib_SecurityUtils.checkFieldIsUpdateable(User.SObjectType, User.Seek_Usage__c); 
            update thisuser;
            
            reminingquota = reminingquota -1;
            if(reminingquota <=0){
                remaining_quota_enough = false;
            }
            seekSaveInfo = 'The new Ad. has been put in queue and waiting to post online.';
        }
        catch(system.Exception e)
        {
         	seekSaveInfo = 'The new Ad. has not been put in queue, Contact your system admin.';   
        }
        return null;
     
    }
    
    @future(callout=true)
    public static void insertDetailTable(String jobRefCode, String iid, String aid, String bucketname, String hname, String htype,
        String dname, String dtype, String fname, String ftype, String divisionName, String webSite, string jobTitle){
        
         CTCWS.PeopleCloudWSDaoPort port = new CTCWS.PeopleCloudWSDaoPort();
         port.timeout_x = 60000;
         CommonSelector.checkRead(Advertisement__c.SObjectType,'id');
         Advertisement__c newJob = [select id from Advertisement__c where id=:aid];
         try{
            boolean result =  port.insertJobPostingDetail(jobRefCode, iid, aid, bucketname, hname, htype, dname, dtype, fname, ftype, divisionName, webSite, jobTitle);
            
         }catch(System.Exception e){       
            newJob.Status__c=('Not Valid');
            fflib_SecurityUtils.checkFieldIsUpdateable(Advertisement__c.SObjectType, Advertisement__c.Status__c);
            update newJob;
         }
    }

    public List<SelectOption> styleOptions = new List<SelectOption>();
    public List<SelectOption> templateOptions = new List<SelectOption>();
    public List<SelectOption> seekScreenOptions = new List<SelectOption>();
    Map<String,String> template_map = new Map<String,String>();
    Map<Id,StyleCategory__c> style_map = new Map<Id,StyleCategory__c>();
    Map<Id,StyleCategory__c> seekScreen_map = new Map<Id,StyleCategory__c>();
    public Boolean seekScreenOn = false;

     public Boolean getSeekScreenOn(){
        return seekScreenOn;
     }
     
     public List<SelectOption> getSeekScreenOptions(){
        return seekScreenOptions ;
     }
     
     public List<SelectOption> getStyleOptions(){
        return styleOptions;
    }
    
    public boolean getStyles(Boolean enableseekdefault){
    	boolean getAllStyle = true;
	    seekScreenOn = false; 
        //**** get Ad. Templates      
        StyleCategory__c[] templates = CTCService.getOptions(1,'seek'); 
        //templateOptions.add(new SelectOption('', ''));
        if(templates != null){
            for(StyleCategory__c ts : templates){
                templateOptions.add(new SelectOption(ts.Id, ts.Name));
                template_map.put(ts.Id,ts.Name);
            }
        }
        if(enableseekdefault){
        	//**** populate Seek Screen selection
        	//system.debug('selectSeekAccount ='+selectSeekAccount);
	        List<StyleCategory__c> seekScreens = DaoStyleCategory.getSeekScreenId(selectSeekAccount);
	        if(seekScreens != null && seekScreens.size()>0){          
	        	seekScreenOptions.add(new SelectOption('','---None---'));
	            for(StyleCategory__c ss : seekScreens){
	                seekScreenOptions.add(new SelectOption(ss.Id, ss.Name));
	                seekScreen_map.put(ss.Id, ss);
	            }
	            seekScreenOn = true;     
	        }
	        else{
	        	seekScreenOn = false;
	        	getAllStyle = false;
	        	msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.SEEK_GET_SEEK_SCREEN_ERROR); 
	        } 
        }
        //else{
        	//**** populate application form style selection
	        StyleCategory__c[] styles = CTCService.getOptions(2,null); 
	        if(styles != null){   
	        	styleOptions.add(new SelectOption('','--None--'));        
	            for(StyleCategory__c sc : styles){
	                styleOptions.add(new SelectOption(sc.Id, sc.Name));
	                style_map.put(sc.Id, sc);
	            }     
	        }
	        else{
	        	getAllStyle = false;
	        	msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.SEEK_GET_TEMPLATE_ERROR); 
	        }
        //}
        return getAllStyle;       
    }
    
    public List<SelectOption> getTemplateOptions(){
        return templateOptions;
    }
    
    //Generate Error message, Add by Jack on27/06/2013
    public static Boolean customerErrorMessage(Integer msgcode){
        PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        Boolean msgSignalhasmessage = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
                ApexPages.addMessage(newMessage);
            }
        return msgSignalhasmessage;               
    }

    private void checkFLS(){
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Advertisement__c.Website__c,
            Advertisement__c.RecordTypeId,
            Advertisement__c.Job_Content__c,
            Advertisement__c.Online_Footer__c,
            Advertisement__c.Skill_Group_Criteria__c,
            Advertisement__c.Reference_No__c,
            Advertisement__c.Application_Form_Style__c,
            Advertisement__c.Job_Title__c,
            Advertisement__c.Application_URL__c,
            Advertisement__c.Status__c,
            Advertisement__c.Job_Posting_Status__c,
            Advertisement__c.ListingID__c,
            Advertisement__c.Preferred_Application_Mode__c,
            Advertisement__c.Placement_Date__c,
            Advertisement__c.Provider_Code__c,
            Advertisement__c.Company_Name__c,
            Advertisement__c.Photos__c,
            Advertisement__c.Seek_Account__c,
            Advertisement__c.SeekScreen__c,
            Advertisement__c.JobXML__c,
            Advertisement__c.Prefer_Email_Address__c
        };
        fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
        fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    }
}