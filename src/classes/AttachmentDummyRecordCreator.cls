public with sharing class AttachmentDummyRecordCreator {

	public static List<Attachment> createAttachmentsForActivity(Task t) {
	    List<Attachment> atts = new List<Attachment>();
	    atts.add(createAttachmentForActivity('test resume', 'body content resume', t ));
        atts.add(createAttachmentForActivity('test coverletter', 'body content coverletter', t ));
        atts.add(createAttachmentForActivity('test att', 'body content attachment', t ));
        //check FLS
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Attachment.Name,
            Attachment.ParentId,
            Attachment.Body,
            Attachment.OwnerId
        };
        fflib_SecurityUtils.checkInsert(Attachment.SObjectType, fieldList);
        insert atts;
        return atts;
    }
    
    public static Attachment createAttachmentForActivity(String attName, String body, Task t) {
        Attachment att = new Attachment(Name=attName, ParentId=t.Id, Body=Blob.valueOf(body), OwnerId=t.OwnerId);
        return att;
    }

}