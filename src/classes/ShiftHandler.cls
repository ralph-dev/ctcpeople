public with sharing class ShiftHandler implements ITrigger {
	static List<String> shiftIdListNeedToBeDeleted = new List<String>();
	static List<Days_Unavailable__c> rosterList = new List<Days_Unavailable__c>();
	static List<String> shiftIdListNeedToBeUpdated = new List<String>();
	
	// Constructor
    public ShiftHandler() {
      
    }
    public void bulkBefore() {
    	if(trigger.isBefore != null && trigger.isBefore && trigger.isDelete){
    		Set<ID> shiftIds = Trigger.oldMap.keySet();
    		RosterSelector rSelector = new RosterSelector();
    		rosterList = rSelector.fetchRosterListByShiftIdSet(shiftIds);
    	}
    }  

    public void bulkAfter() {}

    public void beforeInsert(SObject so) {}

    public void beforeUpdate(SObject oldSo, SObject so) {}
    
    public void beforeDelete(SObject so) {}

    public void afterInsert(SObject so) {}

    public void afterUpdate(SObject oldSo, SObject so) {
    	Shift__c shiftOld = (Shift__c) oldSo;
    	Shift__c shiftNew = (Shift__c) so;
    	
		//Under shift, when one of the fields (Start_Date__c, End_Date__c, Start_Time__c, End_Time__c)
		//(Recurrence_End_Date__c, Weekly_Recurrence__c, Recurrence_Weekdays__c, Shift_Type__c) change value	
		//need to update related roster to keep consistency.
    	if(ShiftHelper.checkIsFieldValueChange(shiftOld, shiftNew)){
    		shiftIdListNeedToBeUpdated.add(shiftNew.Id);
    	}
    	
    	//System.debug('shiftIdList: '+shiftIdListNeedToBeUpdated);
    }

    public void afterDelete(SObject so) {}
    
    public void afterUndelete(SObject so){} 

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {	
    	if(trigger.isAfter != null && trigger.isDelete != null && trigger.isAfter && trigger.isDelete){
    		if(rosterList != null && rosterList.size() > 0){
    			
                CommonSelector.quickDelete(rosterList);
                //delete rosterList;
    		}
    	}
    	
    	if(trigger.isAfter != null && trigger.isAfter){		
    		//update roster when shift (start date, end date, start time, end time, 
    		//(shift type, recurrence end date, recurrence weekdays) change
    		RosterService rService = new RosterService();
    		rService.updateRosterWhenShiftCertianFieldsChange(shiftIdListNeedToBeUpdated);
    	}
    }
}