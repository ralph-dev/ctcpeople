/**
 * This is the test class for DaoStyleCategory
 *
 * Create by: Lina Wei
 * Created on: 27/03/2017
 */

@isTest
private class DaoStyleCategoryTest {

    static testMethod void testOldMethod() {
    	System.runAs(DummyRecordCreator.platformUser) {
        System.assertNotEquals(null, DaoStyleCategory.previousSaveSearchCriteriaRecord('test', 'test'));
        System.assertNotEquals(null, DaoStyleCategory.getOldStyleCategoryLikeDevName('test'));
    	}

    }

    static testMethod void testGetLinkedInAccount() {
        System.runAs(DummyRecordCreator.platformUser) {
            Test.startTest();
            StyleCategory__c account = StyleCategoryDummyRecordCreator.createLinkedInAccount();
            List<StyleCategory__c> linkedInAccountList = DaoStyleCategory.getLinkedInAccount();
            System.assertEquals(1, linkedInAccountList.size());
            StyleCategory__c result = DaoStyleCategory.getLinkedInAccountById(account.Id);
            System.assertEquals(account.Id, result.Id);
            Test.stopTest();
        }
    }

    static testMethod void testGetApplicationFormStyle() {
        System.runAs(DummyRecordCreator.platformUser) {
            Test.startTest();
            StyleCategory__c account = StyleCategoryDummyRecordCreator.setJobboardStyleCategory();
            List<StyleCategory__c> applicationForms = DaoStyleCategory.getApplicationFormStyle();
            System.assertEquals(1, applicationForms.size());
            Test.stopTest();
        }
    }

    static testMethod void testCheckJobBoardAccountActive() {
        System.runAs(DummyRecordCreator.platformUser) {
            Test.startTest();
            StyleCategory__c account = StyleCategoryDummyRecordCreator.createLinkedInAccount();
            Boolean active = DaoStyleCategory.checkJobBoardAccountActive(account.Id);
            System.assertEquals(true, active);
            Test.stopTest();
        }
    }

    /**
     * Depreciated
     * Can be deleted after those method deleted
     */
    static testMethod void testDepreciatedMethod() {
        System.runAs(DummyRecordCreator.platformUser) {
            DaoStyleCategory.getSeekAccountByStyleCategoryId('test');
            DaoStyleCategory.getSeekScreenId('test');
            DaoStyleCategory.getGSRStyleCategoryLikeUniqueName('test');
            DaoStyleCategory.getGoogleSavedSearchesNotOwner(DummyRecordCreator.platformUser.Id);
            DaoStyleCategory.getGoogleSavedSearchesByOwner(DummyRecordCreator.platformUser.Id);
            DaoStyleCategory.getSeekAccount();
            System.assert(true);
        }
    }

}