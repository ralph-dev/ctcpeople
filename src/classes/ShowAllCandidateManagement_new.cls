/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

/**
ShowAll function working under the vacancy.
Before this update, ShowAll function used relatedlist to show the CandidateMangement records. 
If Vacancy has more than 100 records, the relatedList can't show the records over 100 records
This update use the pagination to solve this issue, rebuild the screencandidate and MassupdateStatus buttons
rebuild the Edit and Delete function for each CandidateManagement record
build the link for the screencandidate and Massupdatestatus page, these links can help custom return back to showall page 
**/

public with sharing class ShowAllCandidateManagement_new {
    /*public final Placement__c vacancy{get;set;}
    public List<Placement_Candidate__c> candman {get; set;}
    public ApexPages.StandardSetController thecontrollers {get;set;}
    public List<Placement_Candidate__c> selectedCandman {get; set;}
    public List<canManWrapper> isselectedCanManWrapper {get; set;}    
    public String vacId {get; set;}
    public Boolean flag{get;set;}
    public Integer size{get;set;}  
    public String canManIds {get;set;} 
    public Integer currentpagenumber{get; set;}
    public string pagenumber{get; set;} 
    public String currentSelectCrieria{get; set;}
    public static Integer RECORD_PER_PAGE = 100; //set the records of per page
    public Integer totalPage {get;set;}
    public string err_info{get; set;}
    public string currentAscSelectCrietria {get; set;}
    
    public Boolean msgSignalhasmessage{get ;set;} //display error message
    public PeopleCloudErrorInfo errormesg; //
    
    //Init Return JS param
    public String returnUrl{get;set;}
	public Boolean triggerParentPage {get; set;}
	public Boolean enableBulkSendSMS{get; set;}
    
    public ShowAllCandidateManagement_new(ApexPages.StandardSetController controller) { 
    		errormesg = null;   
    		msgSignalhasmessage =false;		
            String currentpagenumberstring = ApexPages.currentPage().getParameters().get('cp');//get the currentpage number from URL
            String currentSelectCrieria = ApexPages.currentPage().getParameters().get('sc');// get the sort criteria from URL
            String currentAscSelectCrietria = ApexPages.currentPage().getParameters().get('asc');// get the asc sort criteria from URLcurrent
            //Check bulk sms is available 
       	    PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
            enableBulkSendSMS = pcSwitch.Enable_Send_Bulk_SMS__c ;
            if(currentpagenumberstring!=null){//if current page number is null, set current page number is 1
                currentpagenumber = Integer.valueOf(currentpagenumberstring);
            }
            else{
                currentpagenumber = 1;
            }            
            vacId=ApexPages.currentPage().getParameters().get('Id');//get the vacancy Id from URL
            //System.debug('==============='+vacId);
			CommonSelector.checkRead(Placement__c.SObjectType,'Id,Name');
			CommonSelector.checkRead(Contact.SObjectType,'Id,Name');
            vacancy = [select Id,Name,(select Id,Name from Candidates__r) from Placement__c where Id =: vacId LIMIT 1];        
            
            if(currentSelectCrieria==null){
                currentSelectCrieria = 'Rating__c';// If the URL doesn't include the sort criteria, set the CreateDate as the sort criteria
            }
            if(currentAscSelectCrietria==null){
            	currentAscSelectCrietria='DESC';
            }
            setSelectCrieria(currentSelectCrieria);//set the sort criteria in the VF page
            setAscSelectCrieria(currentAscSelectCrietria);
            size = vacancy.Candidates__r !=null ?vacancy.Candidates__r.size():0;
            try{
				CommonSelector.checkRead(Placement_Candidate__c.SObjectType,'name, Status__c, Recruiter_Notes__c,Rating__c , Candidate_Source1__c, '
						+ ' Candidate_Status__c ,Candidate__r.Name, Star_Rating__c');
	            thecontrollers = new ApexPages.StandardSetController(DataBase.getQueryLocator('select name, Status__c, Recruiter_Notes__c,Rating__c , Candidate_Source1__c, '
	            					+ ' Candidate_Status__c ,Candidate__r.Name, Star_Rating__c from Placement_Candidate__c'
	            					+ ' where Placement__c =: vacId order by '+ currentSelectCrieria+ ' ' + currentAscSelectCrietria )); 
	            					//get the result from DB and user StandardSetController inner method to do pagination.

	            thecontrollers.setPageSize(RECORD_PER_PAGE);
	            thecontrollers.setpageNumber(currentpagenumber);
	            if(size>0)
	            {
	                if( math.mod(size, RECORD_PER_PAGE) > 0)
	                    {
	                        totalPage = size /RECORD_PER_PAGE + 1;
	                    }
	                else
	                    {
	                        totalPage = size /RECORD_PER_PAGE ;
	                    }
	            }
	           else{
	                totalPage =0;
	                }
	            getCurrVCWF();
	            getPageNumber();
            }
            catch(system.exception e){
                 NotificationClass.notifyErr2Dev('ShowAllCandidateManagement_new/ShowAllCandidateManagement_new/thecontrollers', e);
                 }           
      }
      
    public void getCurrVCWF(){
        selectedCandman = new List<Placement_Candidate__c>();
        isselectedCanManWrapper = new List<canManWrapper>();
        candman = (List<Placement_Candidate__c>) thecontrollers.getRecords();
        for(Placement_Candidate__c a:candman){
                isselectedCanManWrapper.add(new canManWrapper(a));//push the whole page records into the isselectedCanManWrapper object
        }        
    }
    
    public void previous(){      
     	thecontrollers.previous();
     	getCurrVCWF();
    	getPageNumber();
    }
    
    public void first(){       
       thecontrollers.first();
       getCurrVCWF();
       getPageNumber();
    }
    
    public void last(){       
       thecontrollers.last();
       getCurrVCWF();
       getPageNumber();
    }
    
    public void next(){       
       thecontrollers.next();
       getCurrVCWF();
       getPageNumber();
    }
    
    
    public boolean hasNext{
        get{
           return thecontrollers.getHasNext();
        }
        set;
    }
    public boolean hasPrevious{
        get{
           return thecontrollers.getHasPrevious();
        }
        set;
    }
        
    public void getPageNumber(){
        pagenumber = string.valueOf(thecontrollers.getPageNumber());
    }
    public PageReference screenCandidate(){
    	returnUrl = '';
    	triggerParentPage = false;
    	try{
	        canManIds = '';
	        getPageNumber();
	        getSelected();
	        PageReference scpage = Page.EnhancedMassRating;
	        
		        if(Test.isRunningTest()){
		        	for(Placement_Candidate__c selCanMan:candman){
			                canManIds = canManIds + selCanMan.id + ',';
			        }
		        }
		        else{
		        	if(selectedCandman.size()>0){
				        for(Placement_Candidate__c selCanMan:selectedCandman){
				                canManIds = canManIds + selCanMan.id + ',';
				        }
			         }
			        else{
			        	customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CMS_SELECTED);
			        	return null;
			        }
		        } 
		        //System.debug('=========================='+canManIds);
		        canManIds = canManIds.substring(0,canManIds.length()-1);
		        scpage.setRedirect(true);        
		        scpage.getParameters().put('cmids',canManIds);
		        scpage.getParameters().put('id',vacId);
		        scpage.getParameters().put('cp',pagenumber);
		        scpage.getParameters().put('sc',currentSelectCrieria);
		        scpage.getParameters().put('asc',currentAscSelectCrietria);
		        triggerParentPage = true;
		        returnUrl = scpage.getURL();
		        //system.debug('SC = '+ triggerParentPage + returnUrl);
		        return null;	        
    	}
    	catch(system.exception e){
    		 NotificationClass.notifyErr2Dev('ShowAllCandidateManagement_new/ShowAllCandidateManagement_new/screenCandidate', e);
    		 return null;
    	}
    }
    
    //Send Resume function
    public PageReference sendemail(){
    	returnUrl = '';
    	triggerParentPage = false;
    	try{
	        canManIds = '';
	        getPageNumber();
	        getSelected();
	        PageReference scpage = Page.Extract4SR;
	        
		        if(Test.isRunningTest()){
		        	for(Placement_Candidate__c selCanMan:candman){
			                canManIds = canManIds + selCanMan.id + ',';
			        }
		        }
		        else{
		        	if(selectedCandman.size()>0){
				        for(Placement_Candidate__c selCanMan:selectedCandman){
				                canManIds = canManIds + selCanMan.id + ',';
				        }
			         }
			        else{
			        	customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CMS_SELECTED);
			        	return null;
			        }
		        } 
		        //System.debug('=========================='+canManIds);
		        canManIds = canManIds.substring(0,canManIds.length()-1);
		        scpage.setRedirect(true);        
		        scpage.getParameters().put('cmids',canManIds);
		        scpage.getParameters().put('id',vacId);
		        scpage.getParameters().put('cp',pagenumber);
		        scpage.getParameters().put('sc',currentSelectCrieria);
		        scpage.getParameters().put('asc',currentAscSelectCrietria);
		        scpage.getParameters().put('sourcepage','showallpage');
		        triggerParentPage = true;
		        returnUrl = scpage.getURL();
		        return null;	        
    	}
    	catch(system.exception e){
    		 NotificationClass.notifyErr2Dev('ShowAllCandidateManagement_new/ShowAllCandidateManagement_new/screenCandidate', e);
    		 return null;
    	}
    }
    
    //Send SMS function
    public PageReference sendsms(){
    	returnUrl = '';
    	triggerParentPage = false;
    	try{
	        canManIds = '';
	        getPageNumber();
	        getSelected();
	        PageReference scpage = Page.Extract4SMS;
	        
		        if(Test.isRunningTest()){
		        	for(Placement_Candidate__c selCanMan:candman){
			                canManIds = canManIds + selCanMan.id + ',';
			        }
		        }
		        else{
		        	if(selectedCandman.size()>0){
				        for(Placement_Candidate__c selCanMan:selectedCandman){
				                canManIds = canManIds + selCanMan.id + ',';
				        }
			         }
			        else{
			        	customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CMS_SELECTED);
			        	return null;
			        }
		        } 
		        //System.debug('=========================='+canManIds);
		        canManIds = canManIds.substring(0,canManIds.length()-1);
		        scpage.setRedirect(true);        
		        scpage.getParameters().put('cmids',canManIds);
		        scpage.getParameters().put('id',vacId);
		        scpage.getParameters().put('cp',pagenumber);
		        scpage.getParameters().put('sc',currentSelectCrieria);
		        scpage.getParameters().put('asc',currentAscSelectCrietria);
		        scpage.getParameters().put('sourcepage','showallpage');
		        triggerParentPage = true;
		        returnUrl = scpage.getURL();
		        return null;	        
    	}
    	catch(system.exception e){
    		 NotificationClass.notifyErr2Dev('ShowAllCandidateManagement_new/ShowAllCandidateManagement_new/screenCandidate', e);
    		 return null;
    	}
    }
    public PageReference massUpdateStatus(){
    	returnUrl = '';
    	triggerParentPage = false;
    	try{
	        canManIds = '';
	        getPageNumber();
	        getSelected();
	        PageReference massupdatestatuspage = Page.iframeparentmassupdate;
	        
		        if(Test.isRunningTest()){
		        	for(Placement_Candidate__c selCanMan:candman){
			                canManIds = canManIds + selCanMan.id + ',';
			        }
		        }
		        else{
		        	if(selectedCandman.size()>0){
				        for(Placement_Candidate__c selCanMan:selectedCandman){
				                canManIds = canManIds + selCanMan.id + ',';
				        }
			        }
			        else{
			        	customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CMS_SELECTED);			        	
			        	return null;
			        }
		        } 
		        //System.debug('=========================='+canManIds);
		        canManIds = canManIds.substring(0,canManIds.length()-1);
		        massupdatestatuspage.setRedirect(true);        
		        massupdatestatuspage.getParameters().put('cmids',canManIds);
		        massupdatestatuspage.getParameters().put('id',vacId);
		        massupdatestatuspage.getParameters().put('cp',pagenumber);
		        massupdatestatuspage.getParameters().put('sc',currentSelectCrieria);
		        massupdatestatuspage.getParameters().put('asc',currentAscSelectCrietria);
		        triggerParentPage = true;
		        returnUrl = massupdatestatuspage.getURL();
		        return null;
	        
    	}
    	catch(system.exception e){
    		 NotificationClass.notifyErr2Dev('ShowAllCandidateManagement_new/ShowAllCandidateManagement_new/massUpdateStatus', e);
    		 return null;
    	}
    }
    
    public List<Placement_Candidate__c> getSelectCanMans(){
        if(selectedCandman.size()>0){
            return selectedCandman;
        }else{
        	
            return null;
        }
    }
    public PageReference backToVac(){
        ApexPages.Standardcontroller vacStdController = new ApexPages.Standardcontroller(vacancy);
        return vacStdController.view();
    }
    
    public class canManWrapper{//CanManWrapper contains the CandidateManagement record and boolean to judge select
        public Placement_Candidate__c canMan_all {get; set;}
        public Boolean selected {get; set;}
        public canManWrapper(Placement_Candidate__c canMan_detail){
            canMan_all = canMan_detail;
            selected = false;
        }
    }
    
    public PageReference getSelected(){        
        selectedCandman.clear();//clean the select record before get the select records 
        for(canManWrapper cmwwrapper:isselectedCanManWrapper){
                if(cmwwrapper.selected == true){
                selectedCandman.add(cmwwrapper.canMan_all);
                //canManIds = canManIds + cmwwrapper.canMan_all.id + ',';
                }
        }
        
        return null;
    }
    
    public List<SelectOption> getSortCriteria(){       
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('Rating__c ','Candidate Rating'));
        options.add(new SelectOption('Candidate__r.name','Candidate Name'));
        options.add(new SelectOption('Status__c','Status'));        
        options.add(new SelectOption('CreatedDate','Create Date'));
        options.add(new SelectOption('Candidate_Source1__c','Candidate Source'));
        return options;       
    }
    
    public List<SelectOption> getSortAsc(){       
        List<SelectOption> Ascoptions = new List<SelectOption>();
        Ascoptions.add(new SelectOption('ASC','Ascending'));
        Ascoptions.add(new SelectOption('DESC','Descending'));
        //Ascoptions.add(new SelectOption('Status__c','Status'));
        //Ascoptions.add(new SelectOption('Rating__c ','Candidate Rating'));
        return Ascoptions;       
    }
    
    public PageReference getSelectCrieria(){//if custom change the sort criteria, refresh ShowAllCandidateManagement_new page with the new sort criteria
        try{
        pageReference selectCriteria = Page.ShowAllCandidateManagement_new;
        selectCriteria.setRedirect(true);
        selectCriteria.getParameters().put('cmids',canManIds);
        selectCriteria.getParameters().put('id',vacId);
        selectCriteria.getParameters().put('cp','1');
        selectCriteria.getParameters().put('sc',currentSelectCrieria);
        selectCriteria.getParameters().put('asc',currentAscSelectCrietria);
        return selectCriteria;  
        }
        catch(system.exception e){
    		 NotificationClass.notifyErr2Dev('ShowAllCandidateManagement_new/ShowAllCandidateManagement_new/massUpdateStatus', e);
    		 return null;
    	}            
    }
    public void setSelectCrieria(String selectCrieria){        
        this.currentSelectCrieria = selectCrieria;               
    }
    
    public void setAscSelectCrieria(String ascselectCrieria){        
        this.currentAscSelectCrietria = ascselectCrieria;               
    }
    
    //Generate Error Message
    public void customerErrorMessage(Integer msgcode){
        errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        msgSignalhasmessage = errormesg.returnresult;
        for(ApexPages.Message newMessage:errormesg.errorMessage){
        	ApexPages.addMessage(newMessage);
        }               
    }
     
    static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){//Send error message
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());    
    } */ 
}