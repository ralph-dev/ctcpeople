public class CTCService{
	private string err_msg;
    public boolean theCallout(String AdId, String instanceid, String jobtitle, 
                            String selectedStyle, String bucketname, boolean is_edit, String website){
        String dname, dtype, fname, ftype, hname,htype, divisionid;
        String[] temp_file_array;
        err_msg = null;
        Boolean detailres = false;
        try{
           CTCWS.PeopleCloudWSDaoPort EC2ws = new CTCWS.PeopleCloudWSDaoPort();
           //CTCWS4Test.PeopleCloudWSDaoPort EC2ws = new CTCWS4Test.PeopleCloudWSDaoPort();
           
           //Create Application form in EC2 Server
           String jobcode = instanceid +':'+ AdId; // ** make sure that is unique
           err_msg += 'advertisement jobrefcode: ' + jobcode;
           if(selectedStyle != null){
               if(selectedStyle != ''){
               	
               		CommonSelector.checkRead(StyleCategory__c.sObjecttype, 'Id, Name,Div_Html_EC2_File__c, Footer_File_Type__c,Header_EC2_File__c,Header_File_Type__c,Footer_EC2_File__c ');
                    StyleCategory__c theSC = [select Id, Name,Div_Html_EC2_File__c,
                                             Footer_File_Type__c,Header_EC2_File__c,Header_File_Type__c,
                                             Footer_EC2_File__c from StyleCategory__c where Id =:selectedStyle];
                    if(theSC != null){
                        dname = theSC.Div_Html_EC2_File__c;
                        dtype = 'html';
                        fname = theSC.Footer_EC2_File__c;
                        ftype = theSC.Footer_File_Type__c;
                        hname = theSC.Header_EC2_File__c;
                        htype = theSC.Header_File_Type__c;
                        divisionid = theSC.Id;
                    }
               }
           }
           
           //set header and footer
		   if(dname != null && dtype != null && dname != '' && dtype != ''){
		   		if(fname == null || fname == '' || ftype == null || ftype == ''){
               		fname = null;
                	ftype = null;
               	}
                if(hname == null || hname == '' || htype == null || htype == ''){
	                hname = null;
	                htype = null;
	            }
               	
               	//update Ad or post Ad
               	if(is_edit){
                  // Integer updateRes = EC2ws.updateJobPostingByVid(AdId, 
                  //              hname, htype, dname, dtype, fname, ftype,instanceid);  
                   
                   //extended for updating job title             
                   Integer updateRes = EC2ws.updateJobPostingWithTitleByVid(AdId, 
                                hname, htype, dname, dtype, fname, ftype,instanceid,jobtitle);  

                    //system.debug('updateRes ='+ updateRes );
                    if(updateRes == 1){
                        detailres = true;
                    }
                    else{
                        detailres = false; 
                    }
               }else{
                	detailres = EC2ws.insertJobPostingDetail(jobcode, instanceid, 
                            AdId, bucketname, hname, htype, dname, 
                            dtype, fname, ftype,selectedStyle,website,jobtitle); // divisionName = StyleCategory SF record id
               }
           }
           else{           	    
                notificationsendout(err_msg,'styleCategoryfail','jobboardPost/selectedStyle', 'CTCService/theCallout');
           }           
        }catch(Exception e){
         	system.debug(e);
         	notificationsendout(err_msg + '</br>' + e.getMessage(),'insertec2','jobboardPost', 'CTCService/theCallout');
        }
        return detailres ;
    }

    //Send the error message to support mail box
    public static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){
    
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());
    
    
    }
    
	public static List<StyleCategory__c> getOptions(Integer flag, String website){
	    // style for job description of job boards
	    if(flag == 1){   
	        StyleCategory__c[] templates;
	        if(website!=null && website.toLowerCase() == 'careerone'){
	        	
	        	CommonSelector.checkRead(StyleCategory__c.sObjecttype, 'Id, Name,templateC1__c ');
	            templates = [Select Id, Name,templateC1__c from StyleCategory__c 
	                        where (templateC1__c <>null and  templateC1__c <> '')  
	                        and RecordType.DeveloperName ='templateStyle' 
	                        and templateActive__c = true limit 100];
	        }
	        if(website!=null  && website.toLowerCase() == 'seek'){
	            /*
	            templates = [Select Id, Name,templateSeek__c,screenId__c,LogoId__c from StyleCategory__c 
	                        where (templateSeek__c <>null and  templateSeek__c <> '') 
	                        and (LogoId__c <>null and  LogoId__c <> '') 
	                        and (screenId__c <>null and  screenId__c <> '')
	                        and RecordType.DeveloperName ='templateStyle' 
	                        and templateActive__c = true limit 100];*/
	                        
	            CommonSelector.checkRead(StyleCategory__c.sObjecttype, 'Id, Name,templateSeek__c,screenId__c,LogoId__c');
	            templates = [Select Id, Name,templateSeek__c,screenId__c,LogoId__c from StyleCategory__c 
	                        where (templateSeek__c <>null and  templateSeek__c <> '') 
	                        and RecordType.DeveloperName ='templateStyle' 
	                        and templateActive__c = true limit 100];
	        }
	        if(website!=null  && website.toLowerCase() == 'jxt') {
	        	CommonSelector.checkRead(StyleCategory__c.sObjecttype, 'Id, Name,templateJXT__c');
	            templates = [Select Id, Name,templateJXT__c from StyleCategory__c 
	                        where (templateJXT__c <> null and  templateJXT__c <> '')  
	                        and RecordType.DeveloperName ='templateStyle' 
	                        and templateActive__c = true ORDER BY Name limit 100];
	        }
	         if(website!=null  && website.toLowerCase() == 'jxt_nz') {
	         	
	         	CommonSelector.checkRead(StyleCategory__c.sObjecttype, 'Id, Name,templateJXTNZ__c');
	            templates = [Select Id, Name,templateJXTNZ__c from StyleCategory__c 
	                        where (templateJXTNZ__c <> null and  templateJXTNZ__c <> '')  
	                        and RecordType.DeveloperName ='templateStyle' 
	                        and templateActive__c = true ORDER BY Name limit 100];
	        } 
	        
	        return templates;
	    }
	    if(flag == 2){
	        //**** populate application form style selection
	        CommonSelector.checkRead(StyleCategory__c.sObjecttype, 'Id, Name, Div_Html_File_ID__c,Footer_File_ID__c,Header_File_ID__c, Div_Html_EC2_File__c, Footer_EC2_File__c, Header_EC2_File__c,Footer_File_Type__c, Header_File_Type__c, Div_Html_File_Type__c ');
	        StyleCategory__c[] styles = [Select Id, Name, Div_Html_File_ID__c,Footer_File_ID__c,Header_File_ID__c,
	                                    Div_Html_EC2_File__c, Footer_EC2_File__c, Header_EC2_File__c,
	                                    Footer_File_Type__c, Header_File_Type__c, Div_Html_File_Type__c 
	                                    from StyleCategory__c where Active__c = true 
	                                    and RecordType.DeveloperName ='ApplicationFormStyle' ORDER BY Name limit 500];
	        return styles;
	    }
	    //not in used
	    if(flag == 3){
	        //**** populate Seek Screen selection
	        CommonSelector.checkRead(StyleCategory__c.sObjecttype, 'Id, Name, screenId__c');
	        StyleCategory__c[] styles = [Select Id, Name, screenId__c from StyleCategory__c where Active__c = true 
	                                    and RecordType.DeveloperName ='seekScreenStyle' ORDER BY Name limit 500];
	        return styles;
	    }
	    return null;
	
	}
	
	//**************** for control mycareer listing *************
	public static Integer standoutremaining ; 
	public static Integer priorityremaining ; 
	public static List<Boolean> CheckMyCareerAccess()
	{   
	    Date firstdate = system.Today().toStartOfMonth(); 
	    Integer standoutnum =0;
	    Integer prioritynum =0;
	    Boolean s = false;
	    Boolean p = false;
	    List<Boolean> isEnabled = new List<Boolean>(); 
	    try{
	    	
	    	CommonSelector.checkRead(StyleCategory__c.sObjecttype,'Id, Name, Enable_Enhanced_Listing_MyCareer__c, Enable_Priority_Listing_MyCareer__c, MyCareer_Priority_Listing_Quota__c,MyCareer_StandOut_Quota__c');
	        StyleCategory__c mca = [Select Id, Name, Enable_Enhanced_Listing_MyCareer__c, Enable_Priority_Listing_MyCareer__c,
	                                MyCareer_Priority_Listing_Quota__c,MyCareer_StandOut_Quota__c
	                                     from StyleCategory__c 
	                                     where Account_Active__c = true and WebSite__c = 'MyCareer'
	                                     and RecordType.DeveloperName ='WebSite_Admin_Record' limit 1];
	        
	        
	        CommonSelector.checkRead(Advertisement__c.sObjecttype,'Id, Enhanced_Listing__c,Priority_Listing__c');
	        Advertisement__c[] ads = [select Id, Enhanced_Listing__c,Priority_Listing__c
	                                  from Advertisement__c 
	                                  where RecordType.DeveloperName ='MyCareer_com_Advertisement' and 
	                                  Status__c in ('Active','Archived','Archived','Recalled') and
	                                  CreatedDate >=: firstdate 
	                                  limit 1000];
	        if(mca != null)
	        {
	            
	            if(ads != null)
	            {
	                for(Advertisement__c a : ads)
	                {
	                    if(a.Enhanced_Listing__c)
	                    {
	                        standoutnum= standoutnum+1;
	                    }
	                    if(a.Priority_Listing__c)
	                    {
	                        prioritynum = prioritynum +1;
	                    }
	                }
	            
	            }
	            system.debug('standoutnum='+standoutnum);
	            Integer sdq = mca.MyCareer_StandOut_Quota__c != null ? (mca.MyCareer_StandOut_Quota__c).intValue(): 0; 
	            Integer pq = mca.MyCareer_Priority_Listing_Quota__c != null ? (mca.MyCareer_Priority_Listing_Quota__c).intValue(): 0; 
	            if(sdq > standoutnum && mca.Enable_Enhanced_Listing_MyCareer__c)
	            {
	                s = true;
	            }
	            if(pq > prioritynum && mca.Enable_Priority_Listing_MyCareer__c)
	            {
	                p = true;
	            }
	            isEnabled.add(s);
	            isEnabled.add(p);
	            standoutremaining = sdq - standoutnum;
	            priorityremaining = pq - prioritynum;
	            
	        }
	        else
	        {
	            standoutremaining = 0;
	            priorityremaining = 0;
	            return null;
	        }
	    }
	    catch( system.exception e)
	    {
	        standoutremaining = 0;
	        priorityremaining = 0;
	        return null;
	    }
	
	    return isEnabled;               
	}
	
	
}