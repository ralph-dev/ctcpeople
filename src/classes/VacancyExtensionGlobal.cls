public with sharing class VacancyExtensionGlobal {
	public VacancyExtensionGlobal(Object obj){}
	public VacancyExtensionGlobal(){}
    
	@RemoteAction
    public static Placement__c getVacancy(String vacancyId){
    	VacancySelector vacSelector = new VacancySelector();
		list<Placement__c> vacList = vacSelector.fetchVacancyListById(vacancyId);
		if(vacList.size() == 0 || vacList == null){
			return null;
		}
		//System.debug('VacancyExtensionGlobal getVacancy vacList: '+JSON.serialize(vacList.get(0)));
		return vacList.get(0);
    } 
    
    @RemoteAction
    public static String searchVacancy(String searchCriteria){ 
    	VacancySearchCriteria vSearchCriteria = (VacancySearchCriteria)JSON.deserialize(searchCriteria,VacancySearchCriteria.class);
    	//System.debug('vSearchCriteria: '+vSearchCriteria);
    	VacancySelector vacSelector = new VacancySelector();
		List<Placement__c> vacList = vacSelector.fetchVacancyListBySearchCriteria(vSearchCriteria);
		List<VacancyDTO> vacDTOList = VacancyHelper.convertVacancyListToVacancyDTO(vacList);
		//System.debug('VacancyExtensionGlobal searchVacancy vacDTOList : '+JSON.serialize(vacDTOList));
		return JSON.serialize(vacDTOList);
    }
    
    /*
    *	upsert vacancy
    */
    @RemoteAction
    public static String upsertVacancy(String vacDTOJson){
    	List<VacancyDTO>  vacDTOList = VacancyHelper.deserializeVacancyDTOJson(vacDTOJson);
    	//System.debug('VacancyExtensionGlobal upsertVacancy vacDTOList : '+vacDTOList);
    	
    	VacancyService vService = new VacancyService();
    	List<Placement__c> vacList = vService.upsertVacancyFromWizard(vacDTOList);
    	//System.debug('VacancyExtensionGlobal upsertVacancy vacList: '+JSON.serialize(vacList));
    	
    	if(vacList.size() == 0 || vacList == null){
			return null;
		}
		
		List<VacancyDTO> vacDTOListDisplay = VacancyHelper.convertVacancyListToVacancyDTO(vacList);
		//System.debug('VacancyExtensionGlobal upsertVacancy vacDTOListDisplay : '+ JSON.serialize(vacDTOListDisplay));
		
    	return JSON.serialize(vacDTOListDisplay.get(0));
    }
    
    /**
    *	Get the picklist values of Specialty__c, Seniority__c, Category__c
    *	Create a new Map by using  Specialty as key to avoid namespace problem in the front end
    *
    **/
    @RemoteAction
     public static Map<String,List<Schema.PicklistEntry>> describeVacancy(){
     	//System.debug('VacancyExtensionGlobal describeVacancy : ' + DescribeHelper.getDescribeFieldResult(Placement__c.sObjectType.getDescribe().fields.getMap().values()));
        Map<String, Schema.DescribeFieldResult> resultMap = DescribeHelper.getDescribeFieldResult(Placement__c.sObjectType.getDescribe().fields.getMap().values());
    	Map<String,List<Schema.PicklistEntry>> describeResultMap = new Map<String,List<Schema.PicklistEntry>>();
        describeResultMap.put('Specialty',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Specialty__c').picklistValues);
        describeResultMap.put('Seniority',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Seniority__c').picklistValues);
        describeResultMap.put('Category',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Category__c').picklistValues);
        describeResultMap.put('RateType',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Rate_Type__c').picklistValues);
        //System.debug('Describe Result'+JSON.Serialize(describeResultMap));
        return describeResultMap;
    }
    
    /**
    *	Get the record type of Placement__c
    *
    **/
    @RemoteAction
    public static String fetchRecordTypeInfo(){
  		Schema.DescribeSObjectResult R = Placement__c.SObjectType.getDescribe();
  		List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
  		//System.debug('VacancyExtensionGlobal fetchRecordTypeInfo  RT: '+ JSON.serialize(RT));
        return JSON.serialize(RT);
    }  
    
    /**
     *  Provide additional information on the remote action API
     * */
    @RemoteAction
    public static Map<String, RemoteAPIDescriptor.RemoteAction> apiDescriptor(){
        RemoteAPIDescriptor descriptor = new RemoteAPIDescriptor();
        descriptor.description('CRUD & describe remote action for Search.');
        
		descriptor.action('getVacancy').param(String.class);
        descriptor.action('searchVacancy').param(String.class);
        descriptor.action('upsertVacancy').param(String.class);
        
        descriptor.action('describeVacancy');
        descriptor.action('fetchRecordTypeInfo');
        return descriptor.paramTypesMap;
    }  
}