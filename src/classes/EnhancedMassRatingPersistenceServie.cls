public with sharing class EnhancedMassRatingPersistenceServie {
    // Keys for storing workbench params
    public static final String NAME = 'NAME';
    public static final String SEARCH_KEYWORD = 'SEARCH_KEYWORD';
    public static final String CURRENT_PAGE = 'CURRENT_PAGE';
    public static final String DETAIL_SECTION_ID = 'DETAIL_SECTION_ID';
    public static final String VISIBLE_TAB_ID = 'VISIBLE_TAB_ID';
    public static final String COLUMN_TO_SORT = 'COLUMN_TO_SORT';
    public static final String CANDIDATE_MANAGEMENT_IDS = 'CANDIDATE_MANAGEMENT_IDS';


    public Id persistCurrentWorkbenchStatus(String statusName, Map<String,Object> workbenchStatuses){
        String paramJSON = JSON.serialize(workbenchStatuses);
        DaoTemporaryData daoTempData = new DaoTemporaryData();
        String secondaryParamJSON = '';
        Temporary_Data__c savedTempData = daoTempData.saveScreenCandidateTemporaryData(statusName, paramJSON, secondaryParamJSON);
        //system.debug('Serialized JSON:'+paramJSON);
        return savedTempData.Id;
    }
    
    public void updateCurrentWorkbenchStatus(Id statudId, Map<String,Object> workbenchStatuses){
        String statusJSON = JSON.serialize(workbenchStatuses);
        DaoTemporaryData daoTempData = new DaoTemporaryData();
        Temporary_Data__c savedTempData = new Temporary_Data__c();
        savedTempData.Id = statudId;
        savedTempData.Data_Content__c = statusJSON;
        fflib_SecurityUtils.checkFieldIsUpdateable(Temporary_Data__c.SObjectType, Temporary_Data__c.Data_Content__c);
        update savedTempData;
        //system.debug('Serialized JSON:'+statusJSON);
    }
        
    public void cleanOldWorkbenchStatuses(){
    	DaoTemporaryData daoTempData = new DaoTemporaryData();
    	daoTempData.deleteOldScreenCandidateTemporaryData(UserInfo.getUserId());    	
    }
    
    public Map<String, Object> retrieveWorkbenchStatus(Id statusId){
    	DaoTemporaryData daoTempData = new DaoTemporaryData();
    	String statusJSON = null; 
    	try{
    		Temporary_Data__c workbenchStatus = daoTempData.retrieveScreenCandidateTemporaryDataById(statusId);
    		statusJSON = workbenchStatus.Data_Content__c;
    	}catch(Exception e){
    		system.debug('Encounter exception while retrieving screen candidate workbench status:'+e);
    	}
    	Map<String, Object> workbenchStatusMap = new Map<String, Object>();
    	if(statusJSON != null && statusJSON.length() > 0){
    		workbenchStatusMap = (Map<String, Object>)JSON.deserializeUntyped(statusJSON);
    	}
    	//system.debug('Deserialized object:'+workbenchStatusMap);
    	return workbenchStatusMap;
    }
    
    //Fetch Suitable cm according keywords. Author by Jack 05 May 2014
	public static List<Placement_Candidate__c> searchCandidatesByKeywords(String keywords, List<Placement_Candidate__c> allvcws){
		Map<String, List<Placement_Candidate__c>> tempContactCMMap = new Map<String, List<Placement_Candidate__c>>();
		List<Placement_Candidate__c> returnCMlist = new List<Placement_Candidate__c>();
		for(Placement_Candidate__c cm : allvcws){  //Convert CM list to Map<ContactID, List<CM>>
			List<Placement_Candidate__c> cmList;
			if(tempContactCMMap.get(cm.id) == null){
				cmList = new List<Placement_Candidate__c>();
				cmList.add(cm);
				tempContactCMMap.put(cm.Candidate__c, cmList);
			}else{
				cmList = tempContactCMMap.get(cm.id);
				cmList.add(cm);
				tempContactCMMap.put(cm.Candidate__c, cmList);
			}
		}
		//system.debug('tempContactCMMap ='+ tempContactCMMap.keyset());
		if(keywords!= null && keywords.trim()!= ''){//if keywords not exist return allvcws
			List<List<Contact>> findResult = (List<List<Contact>>)[find :keywords returning Contact(Id where id in :tempContactCMMap.keyset())];
			List<Contact> conList=findResult[0];
			//system.debug('conList ='+ conList);
			if(conList!= null && conList.size()>0)
				for(Contact fitablecontact : conList){
					if(tempContactCMMap.containsKey(fitablecontact.id)){
						returnCMlist.addAll(tempContactCMMap.get(fitablecontact.id));
					}
				}
		}else{
			returnCMlist = allvcws;
		}
		return returnCMlist;
	}
	
	//General sort column map
	public static Map<String, boolean> GeneralSortableColumnMap(List<DisplayColumn> displayColumnList){
		Map<String, boolean> generalSortableColumnMap = new Map<String, boolean>();
		if(displayColumnList != null && displayColumnList.size()>0){
			for(displayColumn dc: displayColumnList){
				generalSortableColumnMap.put(dc.Field_api_name , dc.sortable);
			}
		}else{
			return null;
		}
		return generalSortableColumnMap;
	}
}