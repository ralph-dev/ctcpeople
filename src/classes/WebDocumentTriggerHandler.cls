public with sharing class WebDocumentTriggerHandler implements ITrigger{
	
	public static map<Id, Web_Document__c> candResumeMap = new Map<Id, Web_Document__c>(); 
	public static map<Id, String> candLastNameMap = new Map<Id, String>(); 
	public static Set<Id> resumeDedupe = new Set<Id>();
    // This List is to store Contact Ids that has no resume left
    private static List<Id> idList = new List<Id>();
    // This Map is to store "resume" field on Contact to be deleted
    public static Map<Id, Boolean> delResumeField = new Map<Id, Boolean>();
    private static Boolean isSkillParsingEnable;
    

	public WebDocumentTriggerHandler(){ 
		isSkillParsingEnable = SkillGroups.isSkillParsingEnabled();
	}

    public void bulkBefore() {}

    public void bulkAfter() {
        // Get a map of <CandidateID, LastModfiedResume>
        // The last modified resume is a candidate's last modified resume excluding the one in trigger.new or trigger.old
        List<Web_Document__c> docList = new List<Web_Document__c>();
        List<Web_Document__c> resumeList = new List<Web_Document__c>();
        List<Id> conIdList = new List<Id>();
        List<Id> docIdList = new List<Id>();
        if (Trigger.isDelete) {
            docList = (List<Web_Document__c>)trigger.old;
        } else {
            docList = (List<Web_Document__c>)trigger.new;
        }
        for (Web_Document__c doc : docList) {
            if ((doc.Document_Type__c != null && doc.Document_Type__c == 'Resume') ||
                (doc.Type__c != null && doc.Type__c == 'Resume')) {
                resumeList.add(doc);
                docIdList.add(doc.Id);
                conIdList.add(doc.Document_Related_To__c);
            }
        }
        candResumeMap = WebDocumentTriggerSelector.getCandResumeMap(conIdList, docIdList);
        candLastNameMap = ContactSelector.getCandLastNameMap(conIdList);

        // Get List of resume without duplication
        // multiple resume insert or update on one candidate in a bulk should only send one ADD/Update request to SQS
        if (Trigger.isUpdate) {
            resumeDedupe = WebDocumentTriggerService.getResumeDedupe(resumeList, (List<Web_Document__c>)trigger.old);
        } else {
            resumeDedupe = WebDocumentTriggerService.getResumeDedupe(resumeList);

        }
        
    }
    
    public void beforeInsert(SObject so) {
    }
    
    public void beforeUpdate(SObject oldSo, SObject so) {
        
    } 
   
    public void afterInsert(SObject so) {
    	Web_Document__c webDoc = (Web_Document__c)so;
    	if (resumeDedupe.contains(webDoc.Id)) {
    	    PersonFeedListCreater.addPersonFeedToList(webDoc, candResumeMap, candLastNameMap);
    	}
    }
    
    public void afterUpdate(SObject oldSo, SObject so) {
        Web_Document__c oldDoc = (Web_Document__c) oldso;
        Web_Document__c newDoc = (Web_Document__c) so;
        if (newDoc.Document_Related_To__c != oldDoc.Document_Related_To__c && resumeDedupe.contains(newDoc.Id)) {
            PersonFeedListCreater.addPersonFeedToList(newDoc, candResumeMap, candLastNameMap);
        }
    }
    
    public void beforeDelete(SObject so) {
    }
    
    public void afterDelete(SObject so) {
        Web_Document__c webDoc = (Web_Document__c)so;
        // is the webDoc still have document related to?
        if (resumeDedupe.contains(webDoc.Id)) {
            PersonFeedListCreater.addDeletePersonFeedToList(webDoc, candResumeMap, candLastNameMap);
        }
        Web_Document__c lastWebDoc = candResumeMap.get(webdoc.Document_Related_To__c);
        if(lastWebDoc != null){
            if(lastWebDoc.CreatedDate < webDoc.CreatedDate){
                if(!isSkillParsingEnable){
                    idList.add(webDoc.Document_Related_To__c);
                    delResumeField.put(webDoc.Document_Related_To__c, false);
                }
            }            
        }
        else{
            idList.add(webDoc.Document_Related_To__c);
            delResumeField.put(webDoc.Document_Related_To__c, true);
        }
    }

    public void afterUndelete(SObject so) {
    }
    
    public void andFinally() {  
        if(!PCAppSwitch__c.getInstance().Disable_Daxtra_SQS_Request__c){
            SQSHelper.sendSQSRequests(PersonFeedListCreater.PERSONFEEDLIST);
        }
        if(PersonFeedListCreater.webDocList.size()>0){
        	WebDocumentTriggerService.sendReparseRequest(PersonFeedListCreater.webDocList);
        }
        if(idList.size()>0){
            WebDocumentTriggerService.cleanRecords(idList, delResumeField);
        }
    }

	public static void afterInsertWebDoc(List<Web_Document__c> webdocs){
		List<String> conIdList = new List<String>();
		List<Attachment> attList = new List<Attachment>();
		
		for(Web_Document__c webdoc:webdocs){
			if(webdoc.Type__c=='Resume' && webdoc.Document_Related_To__c!=null ){
			    conIdList.add(webdoc.Document_Related_To__c);
			}			
		}
		
		attList = WebDocumentTriggerSelector.getContactAttachment(conIdList);
        fflib_SecurityUtils.checkObjectIsDeletable(Attachment.getSObjectType());
		delete attList;
	}
	
	public static void afterDelWebDoc(List<Web_Document__c> webdocs){
		Map<String, String> wdmap = new Map<String, String>();
		if (webdocs==null) return; 
		for(Web_Document__c webdoc : webdocs){
			if(wdmap.containsKey(webdoc.S3_Folder__c)){
				String temp = wdmap.get(webdoc.S3_Folder__c)+S3Utils.SEPERATOR+webdoc.ObjectKey__c;
				wdmap.put(webdoc.S3_Folder__c, temp);
			}else{
				wdmap.put(webdoc.S3_Folder__c, webdoc.ObjectKey__c);
			}
		}
		String iid = UserInfo.getOrganizationId().substring(0,15);
		S3Utils t = new S3Utils(iid, wdmap);
		S3Utils.deleteS3files(t.jobj.valueToString());
	}
}