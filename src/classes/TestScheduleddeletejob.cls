/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * Used to test Scheduleddeletejob
 */
@isTest
private class TestScheduleddeletejob {
	public static String accids='';
	public static String insertdata(boolean withcms){
		
		PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
        pcSwitch.Placed_Candidate_Status__c = true;
        pcSwitch.DeduplicationOnVCW_Active__c=false;
        pcSwitch.Deduplicate_PC_Active__c = false;
        // Exclude Corporate Candidate from dedup
        upsert pcSwitch;		
        
		/******insert Account first*********/
    	Account acc=new Account();
    	acc.name='TestAccount for scheduled test';
    	insert acc;
    	
    	List<Contact> cands=new List<Contact>();
    	for(Integer i=0;i<3000;i++){
    		Contact cand1=new Contact();
	    	cand1.AccountId=acc.Id;
	    	cand1.Email='test'+i+'@cand1.com';
	    	cand1.FirstName='Test';
	    	cand1.lastname='cand'+i;
	    	cands.add(cand1);
    	}
    	insert cands;
    	system.debug('insert cands - Current SOQL Queries:' + Limits.getQueries() + '/' + Limits.getLimitQueries());
    	if(withcms){
    		Placement__c vac=new Placement__c();
    		vac.Name='Test vacancies for scheduled test';
    		insert vac;
    		
    		List<Placement_Candidate__c> cms=new List<Placement_Candidate__c>();
    		for(Integer i=0;i<30;i++){
    			Placement_Candidate__c cm=new Placement_Candidate__c();
    			cm.Candidate__c=cands[i].id;
    			cm.Placement__c=vac.id;
    			cms.add(cm);
    		}
    		insert cms;
    	}
    	system.debug('insert cms - Current SOQL Queries:' + Limits.getQueries() + '/' + Limits.getLimitQueries());
    	
    	return acc.id;
	}
	 
	/**Test the delete cands conditions**/
    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	Date startdate=Date.today();
    	Integer year=startdate.year();
    	Integer month=startdate.month();
    	Integer dat=startdate.day();
    	year--;
    	Date endDate =date.newInstance(year,month,dat);    	
    	String accid='Test id';    	
    	String querystring='select id from Contact where AccountId = \''+accid+'\'';
    	Scheduleddeletejob sj=new Scheduleddeletejob();
    	String CRON_EXP = '0 0 0 3 9 ?';
    	String jobId = System.schedule('TestApex',CRON_EXP,sj);
    	
    	/****Insert into stylecategory a scheduledjob reocrd******/
    	StyleCategory__c sty=new StyleCategory__c();
    	sty.Start_Date__c=startdate;
    	sty.End_Date__c=enddate;
    	sty.asquerystring__c=querystring;
    	sty.schedulejobId__c=jobId;
    	insert sty;
    	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
         System.assertEquals(CRON_EXP, 
         ct.CronExpression);
    		
    	}
        
    }
    /******Test the delete cands and cms***************/
    static testMethod void myUnitTestfordeletecms() {
    	TriggerHelper.UpdateAccountField=false;
    	System.runAs(DummyRecordCreator.platformUser) {
    	Date startdate=Date.today();
    	Integer year=startdate.year();
    	Integer month=startdate.month();
    	Integer dat=startdate.day();
    	year++;
    	Date endDate =date.newInstance(year,month,dat);
    	accids=insertdata(true);    	
    	String querystring='select id from Contact where AccountId = \''+accids+'\'';
    	Scheduleddeletejob sj=new Scheduleddeletejob();
    	String CRON_EXP = '0 0 0 3 9 ?';
    	String jobId = System.schedule('TestApex',CRON_EXP,sj);
    	
    	/****Insert into stylecategory a scheduledjob reocrd******/
    	StyleCategory__c sty=new StyleCategory__c();
    	sty.Start_Date__c=startdate;
    	sty.End_Date__c=enddate;
    	sty.asquerystring__c=querystring;
    	sty.schedulejobId__c=jobId;
    	insert sty;
    	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
         System.assertEquals(CRON_EXP, 
         ct.CronExpression);
    	}
    	
    }
     /******Test the delete cands and cms over numbers ***************/
    static testMethod void myUnitTestforexceedCandidatesnumber() {
    	TriggerHelper.UpdateAccountField=false;
    	System.runAs(DummyRecordCreator.platformUser) {
    	Date startdate=Date.today();
    	Integer year=startdate.year();
    	Integer month=startdate.month();
    	Integer dat=startdate.day();
    	year++;
    	Date endDate =date.newInstance(year,month,dat);
    	Account acc=new Account();
    	acc.name='TestAccount for scheduled test';
    	insert acc;
    	
    	List<Contact> cands=new List<Contact>();
    	for(Integer i=0;i<3300;i++){
    		Contact cand1=new Contact();
	    	cand1.AccountId=acc.Id;
	    	cand1.Email='test'+i+'@cand1.com';
	    	cand1.FirstName='Test';
	    	cand1.lastname='cand'+i;
	    	cands.add(cand1);
    	}
    	insert cands;
    	
    	    	
    	String querystring='select id from Contact where AccountId = \''+acc.id+'\'';
    	Scheduleddeletejob sj=new Scheduleddeletejob();
    	String CRON_EXP = '0 0 0 3 9 ?';
    	String jobId = System.schedule('TestApex',CRON_EXP,sj);
    	
    	/****Insert into stylecategory a scheduledjob reocrd******/
    	StyleCategory__c sty=new StyleCategory__c();
    	sty.Start_Date__c=startdate;
    	sty.End_Date__c=enddate;
    	sty.asquerystring__c=querystring;
    	sty.schedulejobId__c=jobId;
    	insert sty;
    	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
         System.assertEquals(CRON_EXP, 
         ct.CronExpression);
    	}
    	
    }
     /******Test the delete cands and cms over numbers ***************/
    static testMethod void myUnitTestforexceedCandidateManagementsnumber() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	Date startdate=Date.today();
    	Integer year=startdate.year();
    	Integer month=startdate.month();
    	Integer dat=startdate.day();
    	year++;
    	Date endDate =date.newInstance(year,month,dat);
    	Account acc=new Account();
    	acc.name='TestAccount for scheduled test';
    	insert acc;
    	
    	List<Contact> cands=new List<Contact>();
    	for(Integer i=0;i<2200;i++){
    		Contact cand1=new Contact();
	    	cand1.AccountId=acc.Id;
	    	cand1.Email='test'+i+'@cand1.com';
	    	cand1.FirstName='Test';
	    	cand1.lastname='cand'+i;
	    	cands.add(cand1);
    	}
    	insert cands;
    	Placement__c vac=new Placement__c();
    	vac.Name='Test vacancies for scheduled test';   
    	insert vac;	
    	List<Placement_Candidate__c> cms=new List<Placement_Candidate__c>();
    	for(Integer i=0;i<60;i++){    		
	    		Placement_Candidate__c cm=new Placement_Candidate__c();
	    		cm.Candidate__c=cands[i].id;
	    		cm.Placement__c=vac.id;
	    		cms.add(cm);
    	}
    	insert cms;
    	    	    		    	
    	String querystring='select id from Contact where AccountId = \''+acc.id+'\'';
    	Scheduleddeletejob sj=new Scheduleddeletejob();
    	String CRON_EXP = '0 0 0 3 9 ?';
    	String jobId = System.schedule('TestApex',CRON_EXP,sj);
    	
    	/****Insert into stylecategory a scheduledjob reocrd******/
    	StyleCategory__c sty=new StyleCategory__c();
    	sty.Start_Date__c=startdate;
    	sty.End_Date__c=enddate;
    	sty.asquerystring__c=querystring;
    	sty.schedulejobId__c=jobId;
    	insert sty;
    	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
         System.assertEquals(CRON_EXP, 
         ct.CronExpression);
    	}
    	
    }
    
    
}