/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class IndeedApplicationQuestionSelectorTest {
    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
		  String questionSetId=createData();
		  IndeedApplicationQuestionSelector indeedApplicationSec=new IndeedApplicationQuestionSelector();
		  list<Application_Question__c> applicationQuestions= indeedApplicationSec.retreiveApplicationQuestions(questionSetId);
		  System.assertEquals(applicationQuestions.size(),2);
    	}
		  
		  
    }
    public static  String createData() {
    	 Application_Question_Set__c applicationQuestionSet=new Application_Question_Set__c();
		  applicationQuestionSet.Name='IndeedQuestionName';
		  insert applicationQuestionSet;
		  Application_Question__c applicationQuestion=new Application_Question__c();
		  applicationQuestion.Application_Question_Set__c=applicationQuestionSet.Id;
		  applicationQuestion.name='FirstName';
		  applicationQuestion.Field_Label_Name__c='First Name';
		  applicationQuestion.Sequence__c=1;
		  applicationQuestion.Required__c=true;
		  insert applicationQuestion;
		  Application_Question__c applicationQuestion2=new Application_Question__c();
		  applicationQuestion2.Application_Question_Set__c=applicationQuestionSet.Id;
		  applicationQuestion2.name='Email';
		  applicationQuestion2.Field_Label_Name__c='Email';
		  applicationQuestion2.Sequence__c=2;
		  applicationQuestion2.Required__c=true;
		  insert applicationQuestion2;
    	return applicationQuestionSet.id;
    }
}