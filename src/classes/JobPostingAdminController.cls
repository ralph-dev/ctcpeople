public with sharing class JobPostingAdminController{
    public String verification_reslut {get;set;}
    public String check_site_result {get;set;}
    Public String[] websites {get;set;}
    public boolean is_Display {get;set;}
    public string thesites ;
    
    //Check the privilege about the job board posting
    public JobPostingAdminController(){
        is_Display = true;
	}
    
    //Jobsite class
    public class JobSite{
    	public boolean selected{get; set;}
        public String SiteName {get; set;}
      
        public JobSite(boolean b , String s){
            selected = b;
			SiteName = s;
        }
    }
    
    Public JobSite[] jobsites {get;set;}
    User[] users;
    public Map<Id,User> user_map ;//= new Map<Id,User>();
    List<SelectOption> useroptions = new List<SelectOption>();
    public List<SelectOption> getUseroptions(){
        return useroptions;
    }
    public String selectedUser {get;set;}
    public String selectedUser2 {get;set;}
    public void init(){
        jobsites = new JobSite[]{}; 
        if(websites != null){
            for(String s: websites){
            	jobsites.add(new JobSite(true, s));
            }       
        }
        CommonSelector.checkRead(User.sObjectType,'Id,UserName,Name, JobBoards__c, WebJobFooter__c');
        user_map = new Map<Id,User>([Select Id,UserName,Name, JobBoards__c, WebJobFooter__c from User where isActive = true and UserType = 'Standard']);
        if(user_map != null){
            users = user_map.values();
        }
        useroptions.add(new SelectOption('all','--All--'));
        if(users!=null) {
            for(User u: users){
                useroptions.add(new SelectOption(u.Id,u.UserName + '('+u.Name+')'));
            }
        }
        is_Successful = false;
        is_Successful2 = false;

    }
    
    Public string msg1{get;set;}
    Public string msg2{get;set;}
    public boolean is_Successful{get;set;}
    public boolean is_Successful2{get;set;}

    public void dosave(){
        //** save selected websites for each user
        try{
            User[] toupdates = new User[]{};
            thesites =null;
            if(users != null){
                for(JobSite js: jobsites){
                    if(js.selected){
                        if(thesites == null){
                       		thesites = js.SiteName;
                       	}
                        else{
                            thesites += ';' + js.SiteName;
                        }
                    }
                }
                if(selectedUser != null){
                    if(selectedUser == 'all'){
                    	for(User u: users){ //** for all users
                        	u.JobBoards__c = thesites ;
                            toupdates.add(u);                        
                        }                    
                    }
                    else{
                        User uu = user_map.get(selectedUser);
                        uu.JobBoards__c = thesites ;
                        toupdates.add(uu);
                    }
                }
                
                if(toupdates.size() >0)
                {
                    fflib_SecurityUtils.checkFieldIsUpdateable(User.SObjectType, User.JobBoards__c);
                	update toupdates;
                    msg1 = 'Save Successfully!!';
                    is_Successful = true;
                }
            }            
        }
        catch(system.Exception e){
            system.debug(e);
            msg1 = 'Save Unsuccessfully!!';
            is_Successful = false;
        }        
    }
    public PageReference changeuser(){
        if(selectedUser2 != null){
            if(selectedUser2 == 'all'){
                onlinefooter = '';            
            }
            else{
                onlinefooter = user_map.get(selectedUser2).WebJobFooter__c;            
            }        
        }
        return null;
    
    }
    public string onlinefooter{get;set;}
    public void dosubmit(){
    	is_Successful2 = false;
        try{
            if(selectedUser2 != null)
            {   User[] toupdates = new User[]{};
                if(selectedUser2 == 'all')
                {
                    for(User u: users)
                    {
                        u.WebJobFooter__c = onlinefooter;
                        toupdates.add(u);   
                    }
                
                }
                else
                {
                    User uu = user_map.get(selectedUser2);
                    uu.WebJobFooter__c = onlinefooter;
                    toupdates.add(uu);  
                }
                if(toupdates.size()>0)
                {
                    fflib_SecurityUtils.checkFieldIsUpdateable(User.SObjectType, User.WebJobFooter__c);
                    update toupdates;
                    is_Successful2 = true;
                    msg2 = 'Save Successfully!!';
                }
            
            
            }
        }catch(system.Exception e)
        {
            system.debug(e);
            msg1 = 'Save Unsuccessfully!!';
            is_Successful2  = false;
        
        }
    
    
    }
    public PageReference nonaction(){
    	return null;
    } 
}