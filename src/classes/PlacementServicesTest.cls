/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PlacementServicesTest {
    
    static testMethod void testConvertPlacementToEmployee() {
    	System.runAs(DummyRecordCreator.platformUser) {
        PlacementServices ps = new PlacementServices();
        system.assertNotEquals(null, ps.convertPlacementToEmployee(DataTestFactory.createCandidateManagements()));
        PlacementServices plaServ=new PlacementServices();
    	DataTestFactory.createWorkplaces();
    	DataTestFactory.createCandidateManagements();
    	DataTestFactory.createCTCPeopleSettings();
    	list<String> approverIds=new list<String>();
    	for(Contact cand: DataTestFactory.createContacts()){
    		approverIds.add(cand.Id);
    	}
    	list<String> accountIds=new list<String>();
    	for(Account acc: DataTestFactory.createAccounts()){
    		accountIds.add(acc.Id);
    	}
    	list<String> workplaceIds=new list<String>();
    	for(Workplace__c workplace: DataTestFactory.createWorkplaces()){
    		workplaceIds.add(workplace.Id);
    	}
    	plaServ.fixApprover(approverIds);
    	plaServ.fixWorkplaces(workplaceIds);
    	plaServ.fixBillerContact(approverIds);
    	plaServ.fixBiller(accountIds);
    	}
    }
    
    static testMethod void testRetrievePlacementsByIds() {
    	System.runAs(DummyRecordCreator.platformUser) {
        PlacementServices ps = new PlacementServices();
        system.assertNotEquals(null, ps.retrievePlacementsByIds(new List<String>{'1234'}));
    	}
    }
    
    static testMethod void testFetchCanPlacementIDMap() {
    	System.runAs(DummyRecordCreator.platformUser) {
        PlacementServices ps = new PlacementServices();
        system.assertNotEquals(null, ps.fetchCanPlacementIDMap(new List<String>{'1234'}));
    	}
    }
    
    static testMethod void testfetchAllRecords() {
    	System.runAs(DummyRecordCreator.platformUser) {
        PlacementServices ps = new PlacementServices();
        list<Placement_Candidate__c> cms = DataTestFactory.createCandidateManagements();
        List<String> ids = new List<String>();
        
        for(Placement_Candidate__c cm : cms) {
            ids.add(cm.id);
        }
        
        system.assertNotEquals(null, ps.fetchAllRecords(ids));
    	}
    }
}