@isTest
private class AutoUpdateAppFormTest
{
	static testMethod void autoUpdateAppFormTest(){
		System.runAs(DummyRecordCreator.platformUser) {
			Folder folder = DaoFolder.getFolderByDevName('CTC_Admin_Folder');
			String docBody = '<table id="detailInfo"><tr><th>Birthdate</th><td><input name="Birthdate_table" type="text" class="dateISO" /></td></tr><tr><th>Business Phone</th><td><input name="Phone_table" type="text" /></td></tr><tr><th>Contact Description</th><td><textarea name="Description_table" rows="4" cols="30"  maxlength="255" ></textarea>Maximum Length of 255 Characters</td></tr><tr><th>Department</th><td><input name="Department_table" type="text"  maxlength="80" /></td></tr><tr><th>Email</th><td><input name="Email_table" type="text" class="required email" /></td></tr><tr><th>Industry</th><td><select name="PeopleCloud1__Industry_1__c_table" ><option value="">--Please Select--</option><option value="Any">Any</option><option value="Adhesives">Adhesives</option><option value="Advertising">Advertising</option><option value="Aero Space">Aero Space</option><option value="Agricultural">Agricultural</option><option value="Architectural &amp; Design">Architectural &amp; Design</option><option value="Audio Visual">Audio Visual</option><option value="Automotive">Automotive</option><option value="Beauty/Haircare">Beauty/Haircare</option><option value="Business Services">Business Services</option><option value="Chemicals">Chemicals</option><option value="Coatings">Coatings</option><option value="Commercial Services">Commercial Services</option><option value="Electrical">Electrical</option><option value="Electronic">Electronic</option><option value="Environment">Environment</option><option value="Equipment Hire">Equipment Hire</option><option value="Exhibition Displays">Exhibition Displays</option><option value="Fashion">Fashion</option><option value="Film Distributers">Film Distributers</option><option value="Finance">Finance</option><option value="Goods to Retail">Goods to Retail</option><option value="Hospitality">Hospitality</option><option value="Hotels">Hotels</option><option value="Industrial">Industrial</option><option value="IT">IT</option><option value="Liquor">Liquor</option><option value="Magnetic">Magnetic</option><option value="Marine">Marine</option><option value="Marketing">Marketing</option><option value="Materials Handling">Materials Handling</option><option value="Media">Media</option><option value="Medical /Healthcare">Medical /Healthcare</option><option value="Music">Music</option><option value="Packaging">Packaging</option><option value="Photographic">Photographic</option><option value="Plastics">Plastics</option><option value="Point of Sale">Point of Sale</option><option value="Printing">Printing</option><option value="Publishing">Publishing</option><option value="Real Estate">Real Estate</option><option value="Recruitment">Recruitment</option><option value="Refrigeration">Refrigeration</option><option value="Retail">Retail</option><option value="Scientific">Scientific</option><option value="Security">Security</option><option value="Signage">Signage</option><option value="Sport &amp; Recreation">Sport &amp; Recreation</option><option value="Storage">Storage</option><option value="Telecommunications">Telecommunications</option><option value="Transport">Transport</option><option value="Travel">Travel</option><option value="Veterinary">Veterinary</option><option value="Waste">Waste</option></select></td></tr><tr><th>Last Name</th><td><input name="LastName_table" type="text"  class="required"  maxlength="80" /></td></tr><tr><th>Salutation</th><td><select name="Salutation_table" ><option value="">--Please Select--</option><option value="Mr.">Mr.</option><option value="Ms.">Ms.</option><option value="Mrs.">Mrs.</option><option value="Dr.">Dr.</option><option value="Prof.">Prof.</option></select></td></tr></table>';
		    Document d = new Document(name = 'test', folderId = folder.Id, body = Blob.valueOf(docBody));
		    insert d;
		    
			AutoUpdateAppForm af = new AutoUpdateAppForm(contactSObjectMap(), d.id);
			
			String dId = d.Id;
			List<Document> docList = new List<Document>();
			docList.add(d);
			system.assertNotEquals(null, af.CheckFileFields(docList, contactSObjectMap()));
			Document d2 = DaoDocument.getDocById(dId);
			System.assertNotEquals(d2, null);
			
			try {
			    system.assertNotEquals(null, af.describePickList(contactSObjectMap(), 'test'));
			} catch (Exception e) {
			    
			}
		}
		
	}

	static testMethod void describePickListTest() {
		System.runAs(DummyRecordCreator.platformUser) {
			AutoUpdateAppForm autoUpdateAppForm = new AutoUpdateAppForm();
			Map<String, SObjectField> allFields = new Map<String, SObjectField>();
			allFields = contactSObjectMap();
			List<String> picklistFieldValues = new List<String>();
			picklistFieldValues = autoUpdateAppForm.describePickList(allFields, 'Industry_1__c');
			system.assert(picklistFieldValues.size()>0);
		}
		
	}

	private static Map<String, SObjectField> contactSObjectMap() {
		
		Map<string, SObjectField> contactSObjectMap  = new Map<string, SObjectField>();
		SObjectType objToken = Schema.getGlobalDescribe().get('Contact');     
        DescribeSObjectResult objDef = objToken.getDescribe();  
        contactSObjectMap = objDef.fields.getMap();
        return contactSObjectMap;
	}

}