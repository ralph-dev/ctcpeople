/**
 * This is the selector class for Advertisement Configuration Detail Object
 *
 * Created by: Lina
 * Created on: 03/03/2017
 */

public with sharing class AdvertisementConfigurationDetailSelector extends CommonSelector{

    public AdvertisementConfigurationDetailSelector(){
    	super(Advertisement_Configuration_Detail__c.sObjectType);
    }

    public override List<Schema.SObjectField> getSObjectFieldList(){
        return new List<Schema.SObjectField>{

                Advertisement_Configuration_Detail__c.Id,
                Advertisement_Configuration_Detail__c.Name,
                Advertisement_Configuration_Detail__c.Active__c,
                Advertisement_Configuration_Detail__c.Advertisement_Configuration__c,
                Advertisement_Configuration_Detail__c.Default__c,
                Advertisement_Configuration_Detail__c.ID_API_Name__c,
                Advertisement_Configuration_Detail__c.Template_Items__c,
                Advertisement_Configuration_Detail__c.RecordTypeId
        };
    }

    /**
     * This method return all related advertisement config details for given job board accounts
     * @param accountIds       list of job board account ids
     * @return List<Advertisement_Configuration_Detail__c>
     */
    public List<Advertisement_Configuration_Detail__c> getActiveAdConfigDetailsByAccountIds(Set<String> accountIds) {
        try {
            SimpleQueryFactory qf = simpleQuery()
                    .selectFields(getSObjectFieldList())
                    .setCondition('Advertisement_Configuration__c IN: accountIds AND Active__c = true');
            return Database.query(qf.toSOQL());
        } catch (Exception e) {
            system.debug('AdvertisementConfigurationDetailSelector_getActiveAdConfigDetailsByAccountIds: ' + e);
            return null;
        }
    }

    /**
     * This method return advertisement config details for given value of id field
     * @param id        SFID of the record
     * @return Advertisement_Configuration_Detail__c
     */
    public Advertisement_Configuration_Detail__c getTemplateById(String id) {
        try {
            SimpleQueryFactory qf = simpleQuery()
                    .selectFields(getSObjectFieldList())
                    .setCondition('Id =: id');
            List<Advertisement_Configuration_Detail__c> adConfigs = Database.query(qf.toSOQL());
            return adConfigs.get(0);
        } catch (Exception e) {
            system.debug('AdvertisementConfigurationDetailSelector_getTemplateByIdApiName' + e);
            return null;
        }
    }

}