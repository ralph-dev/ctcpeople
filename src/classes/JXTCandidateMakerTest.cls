@isTest
private with sharing class JXTCandidateMakerTest {
	
	static JXTCandidateMaker defaultMaker = null;
	
	static Advertisement__c adv;
	static List<CVContent> files;
	static JXTContent content;
	static Task sampleTask;
	static List<Attachment> atts;	

	static DummyRecordCreator.DefaultDummyRecordSet rs;
	
	static{
	    
	    rs = DummyRecordCreator.createDefaultDummyRecordSet();
	    AdvertisementDummyRecordCreator adCreator = new AdvertisementDummyRecordCreator();
	    adv = adCreator.generateOneAdvertisementDummyRecord(rs);
	    
	    ActivityDummyRecordCreator actCreator = new ActivityDummyRecordCreator();
	    sampleTask = actCreator.generateOneActivityDummyRecord(rs);
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	    
	    
	   
		
	      	defaultMaker = new JXTCandidateMaker();
	      	
	      	files = new List<CVContent>();
	      	
	      	atts = AttachmentDummyRecordCreator.createAttachmentsForActivity(sampleTask);
	    }

	   
	}
	
	static testMethod void testAdOrVacNotExist() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        
	        content = new JXTContent('test@test.com', 'testCanFirstName', 'testCanLastName', '1test@test.com', '', '', 'test vacOwnerId', '', files);
	        Boolean ifNotExist = defaultMaker.adOrVacNotExist(content);
	        system.assert(ifNotExist);
	        
	        content = new JXTContent('test@test.com', 'testCanFirstName', 'testCanLastName', '1test@test.com', 'test adId', '', 'test vacOwnerId', '', files);
	        ifNotExist = defaultMaker.adOrVacNotExist(content);
	        system.assert(ifNotExist);
	        
	        content = new JXTContent('test@test.com', 'testCanFirstName', 'testCanLastName', '1test@test.com', 'test adId', 'test vacId', 'test vacOwnerId', '', files);
	        ifNotExist = defaultMaker.adOrVacNotExist(content);
	        system.assert(!ifNotExist);
	    }
	    
	}
	
	static testMethod void testCreateAttachments(){
	    
	    CVContent cv = new CVContent();
	    cv.setCvFilename('test att');
	    cv.setContent(Blob.valueof('test att content'));
	    files.add(cv);
	    
		content = new JXTContent('test@test.com', 'testCanFirstName', 'testCanLastName', '1test@test.com', adv.Id, rs.vacancies[0].Id, 'test vacOwnerId', '', files);
		
        System.runAs(DummyRecordCreator.platformUser) {
    		List<Attachment> testAtt = defaultMaker.createAttachments(sampleTask,content);
    		System.assert(testAtt[0].name.startsWith('test att'));
    		System.assertEquals(testAtt.size(), 1);
    		System.assertEquals(testAtt[0].parentId, sampleTask.Id);
        }
	}
	
	
	static testMethod void testBuildActivityDoc(){
	    
	    
    
    	CTCPeopleSettingHelper__c ctc = new CTCPeopleSettingHelper__c(Name='test', Default_Skill_Groups__c='["G001"]', recordTypeId=DaoRecordType.DefaultSkillGroupRT.Id );
        insert ctc;
        PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
        cs.Enable_Daxtra_Skill_Parsing__c=true;
        upsert cs; 
	    System.runAs(DummyRecordCreator.platformUser) {
    		List<ActivityDoc> docs = defaultMaker.buildActivityDoc(sampleTask, atts , DummyRecordCreator.platformUser, '');
    		System.assertEquals(docs.size(),3);
        }
	}
	
	static testMethod void testGetAttachmentNeedToParse() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
    	    Map<Attachment, String> attMap = defaultMaker.getAttachmentNeedToParse(atts);
    	    system.assertEquals(attMap.size(), 3);
	    }
	}
	
	static testMethod void testCallWebService(){
		System.runAs(DummyRecordCreator.platformUser) {
    	try{
    		String url = defaultMaker.callWebService(sampleTask,atts,'');
    		System.assert(url != null && !url.equals(''));
    	}catch(Exception e){
    		System.assert(false);
    	}
		}
    }

}