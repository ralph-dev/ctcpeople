public with sharing class AutoCreateUnavailableDays {
	final static String UNAVAILABLE_TYPE = 'Unavailable';
	
	public static void AutoCreateUnavailableDays(Set<Id> inputidlist, List<Placement_Candidate__c> triggeroldlist){
		PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
		// Prepare ids of all updated or inserted candidate management records	
		List<Id> updatedCMIds = new List<Id>();
		updatedCMIds.addAll(inputidlist);
		List<Placement_Candidate__c> cms=new list<Placement_Candidate__c>();
		
		// Get contract vacancy record type
		if(!Test.isRunningTest() && cs.VacancyRecordType_for_Unavailable_Days__c!=null){
		//Get Vacancy record type for specific name
		 	String[] recordTypeStrs=cs.VacancyRecordType_for_Unavailable_Days__c.split(',');
		 	list<String> recordTypeDeveloperNames=new list<String>();
		 	
		 	for(String recordTypeName: recordTypeStrs)	{
		 		recordTypeDeveloperNames.add(recordTypeName);	
		 	}
		//Get Record Type Id of the selected Record Type if it is noted
			list<RecordType> recordTypes=new list<RecordType>();
			recordTypes=DaoRecordType.getRecordTypesIn(recordTypeDeveloperNames);//[select id from RecordType where developername in : recordTypeDeveloperNames];		
		
			if(recordTypes.size()>0){
				CommonSelector.checkRead(Placement_Candidate__c.sObjectType,
						'Id,Placement__c,Placement__r.End_Date__c,placement__r.Start_Date__c,Candidate__c, Status__c,Candidate_Status__c,Start_Date__c, End_Date__c');
				cms=[select Id,Placement__c,Placement__r.End_Date__c,placement__r.Start_Date__c,Candidate__c, 
					Status__c,Candidate_Status__c,Start_Date__c, End_Date__c
					from Placement_Candidate__c where Id in:updatedCMIds and placement__r.recordtypeid in: recordTypes];
			}
		}else{		
			CommonSelector.checkRead(Placement_Candidate__c.sObjectType,
				'Id,Placement__c,Placement__r.End_Date__c,placement__r.Start_Date__c,Candidate__c, Status__c,Candidate_Status__c,Start_Date__c, End_Date__c');
			cms=[select Id,Placement__c,Placement__r.End_Date__c,placement__r.Start_Date__c,Candidate__c, 
				Status__c,Candidate_Status__c,Start_Date__c, End_Date__c
				from Placement_Candidate__c where Id in:updatedCMIds];
		}		
		// Retrieve setting for placed status			
		Map<String,Map<String,Boolean>> placedStatusConfig = TriggerHelper.getPlacedStatusConfig();
		List<Days_Unavailable__c> unAvaDayList = new List<Days_Unavailable__c>();
		
	
		Schema.DescribeFieldResult reasonDescResult = Days_Unavailable__c.Reason__c.getDescribe();
		String unavaReason = 'Temp Contract';
		// Get default value of unavailable reason from custom setting.
		// If default value is not setup in custom setting, then use the default value of picklist.
		// If default value of reason picklist is not set up, then pick the first picklist value as unavailabe reason.
		// If no value in reason picklist, then 'Temp Contract' is used.
		if(cs.Default_Reason_For_Unavailable_Days__c!=null){
			unavaReason = cs.Default_Reason_For_Unavailable_Days__c;
		}else if(reasonDescResult.getPicklistValues().size()>0){
			unavaReason = reasonDescResult.getPicklistValues().get(0).getValue();
			for(Schema.PicklistEntry e:reasonDescResult.getPicklistValues()){
				if(e.isDefaultValue())
					unavaReason = e.getValue();
			}
		}
		
		//system.debug('Default value of reason:'+reasonDescResult.getDefaultValue());
		//system.debug('Picklist values of reason:'+reasonDescResult.getPicklistValues());
		
		Map<Id,Placement_Candidate__c> oldVersionCM = new Map<Id,Placement_Candidate__c>();
		if(Trigger.isUpdate){
			for(Placement_Candidate__c cm:triggeroldlist){
				oldVersionCM.put(cm.Id,cm);
			}
		}
		Schema.DescribeFieldResult eventReasonDescResult = Days_Unavailable__c.EventReason__c.getDescribe();
		for(Placement_Candidate__c cm:cms){
			Date newStartDate = null;
			Date newEndDate = null;
			
			//
			//system.debug('Candidate Management start date:'+cm.Start_Date__c);
			//system.debug('Candidate Management end date:'+cm.End_Date__c);
			if(cm.End_Date__c!=null && cm.Start_Date__c!=null){
				newEndDate = cm.End_Date__c;
				newStartDate = cm.Start_Date__c;
			}else if(cm.Placement__r.End_Date__c!=null && cm.placement__r.Start_Date__c!=null){
				newEndDate = cm.Placement__r.End_Date__c;
				newStartDate = cm.placement__r.Start_Date__c;
			}
			
			Boolean autoCreateUnavaDayFlag = true;
			if(Trigger.isUpdate && oldVersionCM.get(cm.Id)!=null && oldVersionCM.get(cm.Id).Status__c == cm.Status__c)
				autoCreateUnavaDayFlag = false;
			
			if(autoCreateUnavaDayFlag && (cm.Status__c == 'Placed' || cm.Candidate_Status__c == 'Placed' 
				|| TriggerHelper.isPlacedStatus(cm.Status__c,cm.Candidate_Status__c,placedStatusConfig))
				&& newStartDate!=null &&  newEndDate !=null
				&& newEndDate >= newStartDate){
				Days_Unavailable__c newUnAvaDay = new Days_Unavailable__c(Contact__c=cm.Candidate__c, Start_Date__c=newStartDate,
												End_Date__c=newEndDate,Reason__c=unavaReason, Event_Status__c=UNAVAILABLE_TYPE);
				unAvaDayList.add(newUnAvaDay);
			}
		}
		//insert unAvaDayList;
		if(unAvaDayList.size()>0){
			//check FLS 
			List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
				Days_Unavailable__c.Contact__c,
				Days_Unavailable__c.Start_Date__c,
				Days_Unavailable__c.End_Date__c,
				Days_Unavailable__c.Reason__c,
				Days_Unavailable__c.Event_Status__c
			};
			fflib_SecurityUtils.checkInsert(Days_Unavailable__c.SObjectType, fieldList);
			insert unAvaDayList;
			List<AvailabilityEntity> avlList = new List<AvailabilityEntity>();
        	avlList = AvailabilityParser.convertObjectsToJsonString(unAvaDayList);
			List<Days_Unavailable__c> relatedSfAvlList = AvailabilitySelector.getAllRelatedAvlRecsByCanIdNDateRange(avlList);
			list<Days_Unavailable__c> relatedSfAvlFilterList=new list<Days_Unavailable__c>();
			for(Days_Unavailable__c dayUnavailable: relatedSfAvlList){
				if(dayUnavailable.Event_Status__c!='Unavailable'){
					relatedSfAvlFilterList.add(dayUnavailable);
				}
			}
			if(avlList.size()>0){
				AvailabilityService.resolveAllConflicts4SingleAvl(avlList.get(0), relatedSfAvlFilterList, true);
			}
		}
	
	}
}