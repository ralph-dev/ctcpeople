/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

public with sharing class LinkedinProfile {
    public String body {get; set;}
    public String profileurl {get; set;}
    public String message {get; set;}
    public String contactUrl {get; set;}
    public String name {get; set;}
    public Map<String,LinkedinPersonProfileXml.peopleList> perProfile {get; set;}
    public String publicurl {get; set;}
    public String starter {get; set;}
    public String noprofilebutton {get; set;}
    public String searchcriteria;
    /*Add by Jack, As the IFrame can not be load, use flag and URL to jump page*/
    public boolean jumpauthpage {get; set;}
    public String linkedInAuthPageUrl {get; set;}
            
    public PageReference porfile(){
    	jumpauthpage = true;
        name = ApexPages.currentPage().getParameters().get('ProfilePage');//Contact public URL
        contactUrl = ApexPages.currentPage().getParameters().get('contactUrl');//Contact ID
        searchcriteria = ApexPages.currentPage().getParameters().get('searchcriteria');//keep the searchcriteria result
        
        if(name.equals('')){
        	message = 'There is no Personal Profile for this Contact!';
        	noprofilebutton = 'true';
        	return null;
        }
        else{
        	name = EncodingUtil.urlEncode(name,'UTF-8');//Encode Public Url
	        profileurl = 'https://api.linkedin.com/v1/people/url='+name+':(id,first-name,last-name,headline,location:(name),industry,picture-url,positions,phone-numbers,educations,summary,specialties,public-profile-url)';
	        Http h = new Http();
	        HttpRequest req = new HttpRequest();
	        req.setMethod('GET');
	        req.setEndpoint(profileurl);
	        OAuth oa = new OAuth();
	        if(!oa.setService('linkedin')) {//If oa.setService is false, return to the AuthPage
	            //System.debug(oa.message);
	            message=oa.message;
	            //authorationUrl = 'https://'+ApexPages.currentPage().getHeaders().get('Host')+Page.AuthPage.getUrl();
	            //return new PageReference(authorationUrl).getParameters().put('contactUrl',contactUrl);
	            jumpauthpage = false;
	            PageReference secondPage = Page.AuthPage;
	            secondPage.setRedirect(true);
	            secondPage.getParameters().put('contactUrl',contactUrl); 
	            linkedInAuthPageUrl = secondPage.getURL();
	            return null; 
	        }
	        try{
	        	oa.sign(req);
	        }
	        catch (system.exception e){
	            NotificationClass.notifyErr2Dev('LinkedinProfile/porfile', e);	            
	            return null;
	        }
	        HttpResponse res = null;
	        if(test.isRunningTest()){
	            res = new HttpResponse();
	        }
	        else{
	            res = h.send(req);
	        }
	        if(test.isRunningTest()){
	            body = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><person>  <id>spoTl9kpeC</id>  <first-name>Wei</first-name>  <last-name>Gao</last-name>  <headline>Senior Java &amp; Salesforce Developer at Click to Cloud</headline>  <location>    <name>Sydney Area, Australia</name>    <country>      <code>au</code>    </country>  </location>  <industry>Information Technology and Services</industry>  <positions total="3">    <position>      <id>123091280</id>      <title>Senior Java &amp; Salesforce Developer</title>      <summary>Developer focus on integrate with other systems.Strong interested in Cloud Computing, such as, Salesforce.com, Amazon, GAE.</summary>      <start-date>        <year>2009</year>        <month>7</month>      </start-date>      <is-current>true</is-current>      <company>        <id>1211869</id>        <name>Click to Cloud</name>        <size>11-50 employees</size>        <type>Privately Held</type>        <industry>Information Technology and Services</industry>      </company>    </position>    <position>      <id>139487032</id>      <title>Web Developer and Helpdesk</title>      <summary>The JPA Manchester is an importer of Manchester products, supplying various kinds of towels such as bed sheets, quilts covers, mattress protectors, and blankets.</summary>      <start-date>        <year>2008</year>        <month>1</month>      </start-date>      <end-date>        <year>2008</year>        <month>7</month>      </end-date>      <is-current>false</is-current>      <company>        <name>JPA Manchester</name>        <industry>Information Technology and Services</industry>      </company>    </position>    <position>      <id>139487242</id>      <title>Webmaster and Web Developer</title>      <summary>Xianliang Towel is a professional towel factory in China with 420 employees, an annual turnover of approximate $US 400,000, supplying various kinds of towels</summary>      <start-date>        <year>2003</year>        <month>7</month>      </start-date>      <end-date>        <year>2005</year>        <month>2</month>      </end-date>      <is-current>false</is-current>      <company>        <name>Xianliang Towel Co Ltd</name>        <industry>Textiles</industry>      </company>    </position>  </positions>  <educations total="3">    <education>      <id>55040890</id>      <school-name>University of Wollongong</school-name>      <notes></notes>      <activities></activities>      <degree>Master</degree>      <field-of-study>Software Engineer,  Computer and Network Security</field-of-study>      <start-date>        <year>2005</year>      </start-date>      <end-date>        <year>2007</year>      </end-date>    </education>    <education>      <id>55205871</id>      <school-name>Shandong University of Science and Technology</school-name>      <activities></activities>      <degree>bachelor</degree>      <field-of-study>Compute Science</field-of-study>      <start-date>        <year>2001</year>      </start-date>      <end-date>        <year>2003</year>      </end-date>    </education>    <education>      <id>55205930</id>      <school-name>University of Shandong Radio and TV</school-name>      <notes></notes>      <activities></activities>      <degree>Diploma of Computer Applications</degree>      <field-of-study>Computer Applications</field-of-study>      <start-date>        <year>1998</year>      </start-date>      <end-date>        <year>2001</year>      </end-date>    </education>  </educations></person>';        
	        }else{
	            body = res.getBody();        
	        } 	        
	        LinkedinXml rx = new LinkedinXml();
	        rx.readXml(body);//get the XML body from Linkedin and parse XML by LinkedinXml Class
	        perProfile = rx.perPro;
	        Contact con = [select public_Profile_Url__c from Contact where id = : contactUrl];
	        publicurl = con.public_Profile_Url__c;
	        noprofilebutton = 'false';//The profile button can bu clicked
	        return null;
        }        
    }
    public PageReference Research(){
        contactUrl = ApexPages.currentPage().getParameters().get('contactUrl'); //Customer click the not the right person and update the null value to Public profile url field    
        Contact con = [select public_Profile_Url__c from Contact where id = : contactUrl];
        con.public_Profile_Url__c = '';
        fflib_SecurityUtils.checkFieldIsUpdateable(Contact.SObjectType, Contact.Public_Profile_Url__c);
        update con;
        starter = ApexPages.currentPage().getParameters().get('start'); 
        PageReference secondPage = Page.Oauthservice;
        secondPage.setRedirect(true);
        secondPage.getParameters().put('contactUrl',contactUrl);
        secondPage.getParameters().put('start',starter);
        secondPage.getParameters().put('searchcriteria',searchcriteria); 
        return secondPage;
    }
    
    public void setDefault(){
        publicurl = ApexPages.currentPage().getParameters().get('ProfilePage');//get the Public URL from URL and update to Contact field
        contactUrl = ApexPages.currentPage().getParameters().get('contactUrl');
        Contact con = [select public_Profile_Url__c from Contact where id = : contactUrl];
        con.public_Profile_Url__c = publicurl;
        fflib_SecurityUtils.checkFieldIsUpdateable(Contact.SObjectType, Contact.Public_Profile_Url__c);
        update con;     
    }
    static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){//Send error message
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());    
    }
}