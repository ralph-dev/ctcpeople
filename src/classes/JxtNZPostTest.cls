@isTest
public class JxtNZPostTest {
    
    public testMethod static void jxtNZPostTest(){
    	System.runAs(DummyRecordCreator.platformUser) {
		StyleCategory__c sc = new StyleCategory__c();
		sc.Name='a';
		insert sc;
		Placement__c v = new Placement__c();
		insert v; 
		Advertisement__c ad = new Advertisement__c();
		ad.WebSite__c='a';	 ad.Location__c='a'; 	ad.SearchArea_Label_String__c='a';
		ad.Classification__c='a'; 	ad.Vacancy__c=v.id; 	ad.SubClassification2__c='a';
		ad.Classification2__c='a';	ad.SubClassification_Seek_Label__c='a';	 ad.WorkType__c='a';
		ad.Reference_No__c='a';		ad.Job_Title__c='a';		ad.Company_Name__c='a';
		ad.Template__c='a';		ad.Application_Form_Style__c=sc.id;	ad.Bullet_1__c='a';		
		ad.Bullet_2__c='a';		ad.Bullet_3__c='a';			ad.Job_Type__c='a';
		ad.JXT_Short_Description__c='a';	ad.jxt_sectors__c='a';		ad.Hot_Job__c=true;
		ad.Residency_Required__c=true;		ad.Search_Tags__c='a';		ad.Street_Adress__c='a';
		ad.Nearest_Transport__c='a';		ad.Job_Contact_Name__c='a';		ad.Job_Contact_Phone__c='12345';
		ad.Location_Hide__c =false;			ad.Salary_Type__c='a';			ad.SeekSalaryMin__c='123';
		ad.SeekSalaryMax__c='2353'; 		ad.Salary_Description__c='a';	ad.No_salary_information__c=false;
		ad.Job_Content__c='a';			ad.Has_Referral_Fee__c='a';	ad.Referral_Fee__c=500;
		ad.Terms_And_Conditions_Url__c=Endpoint.getPeoplecloudWebSite()+'';
		ad.JXTNZ_Location__c='a,b,c,d,e,f';
		ad.Industry__c='abc';
		JXTNZPost jp = new JXTNZPost(new ApexPages.StandardController(ad));
		jp.iec2ws = new EC2WebServiceTestImpl();
		jp.getReferralItems();  
		jp.getStyleOptions();
		jp.getTemplateOptions();
		jp.saveAd();
		jp.feedXml();  
		system.assertNotEquals(null, jp.getSelectSkillGroupList());
		system.assertNotEquals(null, jp.getCompanyName());
		system.assertEquals(null, jp.skillparsingstyleclass);
		try {
		    system.assertNotEquals(null, jp.getClassificationItems());
		} catch (Exception e) {
		    
		}
		
		try {
		    system.assertNotEquals(null, jp.getClassificationItems2nd());
		} catch (Exception e) {
		    
		}
    	}
	}
	
}