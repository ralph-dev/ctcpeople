/***********************************************
*
* DisplayColumnHandler Class handles all actions related to columns configuration (Google search, Screen candidate)
* Author by Kevin
* Date May 2014
*
/***********************************************/

global with sharing class DisplayColumnHandler{
	public Boolean removeIDPrefix = false;
	
    String userId;
    String recordTypeId;
    String userPreferenceName;
    final static String XML_FILE_NAME =  'ClicktoCloud_Xml_Contact';
    final static String CONTACT_OBJECT_NAME =  'Contact';
    final static String CM_OBJECT_NAME ='Candidate Management';
    String cmLabelName;
    String contactLabelName;
    //final static String SCREEN_CANDIDATE_COLUMN_CM_PREFIX = '{"Candidate Management":';
    //final static String SCREEN_CANDIDATE_COLUMN_CM_SUFFIX = ',"columnObject":{"name":"Candidate Management"}}]}';
    final static String SCREEN_CANDIDATE_COLUMN_REMOVESTRING = '}]';
    //final static String SCREEN_CANDIDATE_COLUMN_PEOPLE_PREFIX = '{"People":';
    
    private Boolean useUniquePackageXMLColumn = false;
    private String uniqueXMLFileName;
    
    // constructor
    // set userId, recordTypeId, user preference record name
    public DisplayColumnHandler(String uId, String rtId, String upName){
        this.userId = uId;
        this.recordTypeId = rtId;
        this.userPreferenceName = upName;
        
        // Get label name of Candidate Management and Contact
        FieldsHelper.getLabelNames();
        cmLabelName = FieldsHelper.cmLabel;
        contactLabelName = FieldsHelper.contactLabel;
    }
    
    //Sets the unique xml file per subscriber org
    public void setUniqueDefaultXML(String xmlFileName){
    	useUniquePackageXMLColumn = true;
    	uniqueXMLFileName = xmlFileName;
    }
    
    // retrieve search column json from xml file
    public String getSearchColumnJSONFromXml(){
        return parsingXMLColumn();
    }
    
    // retrieve search column from user preference
    public String getSearchColumnJSONFromUserPreference(){
        return getUserPreference().Google_Search_Column__c;
    }
    // retrieve object label names
    public Map<String,String> getObjectsLabelName(){
        Map<String,String> labelNames = new Map<String,String>();
        labelNames.put(CM_OBJECT_NAME,cmLabelName);
        labelNames.put(CONTACT_OBJECT_NAME,contactLabelName);
        return labelNames;
    }
    
    // retrieve screen candidate column json from user preference
    public String getScreenCandidateColumnJSONFromUserPreference(){
        String newSelectedColumn;
        String selectedColumn;
        List<PeopleSearchDTO.FieldColumn> ratings = new List<PeopleSearchDTO.FieldColumn>();
        try {
            selectedColumn = getUserPreference().Screen_Candidate_Column__c;
            if(selectedColumn != null){
               	//check if this Screen_Candidate_Column__c is saved using the old screen candidate function
                // If it is, updated the string so that it can be read by the new screen candidate function
                // and only the first four columns will be returned
                //if (!selectedColumn.startsWith('{"'+cmLabelName+'":') && !selectedColumn.startsWith('{"'+contactLabelName+'":')) {
                if(selectedColumn.startsWith('[')){
                	Set<PeopleSearchDTO.FieldColumn> fieldsObj = (Set<PeopleSearchDTO.FieldColumn>) JSON.deserialize(selectedColumn, Set<PeopleSearchDTO.FieldColumn>.class);
            		if(fieldsObj != null){
                        for (PeopleSearchDTO.FieldColumn fObj : fieldsObj) {
	                        String fname = fObj.Field_api_name;
	                        if (!String.isEmpty(fname)) {
	                        	// Check if ating__c field is included, if it is, change it column name to Star Rating
	                            if(fname.equalsIgnoreCase('Rating__c')){
	                                fObj.ColumnName = 'Star Rating';
	                                ratings.add(fObj);
	                            }
	                            // Check if star_rating__c field is included, if it is, replace it with rating__c
	                            if(fname.equalsIgnoreCase('Star_Rating__c')){
	                                fObj.Field_api_name = 'Rating__c';
	                                ratings.add(fObj);
	                            } 
	                        }
                    	}
		                // if both star_rating__c and rating__c are in the customised column setting
		                // only return the rating__c 
		                if(ratings.size()>=2){
		                    fieldsObj.remove(ratings.get(0));
		                }
                        selectedColumn = JSON.serialize(fieldsObj);
	                    Integer i = fieldsObj.size();
	                    if (i > 4) {
	                        List<PeopleSearchDTO.FieldColumn> newfieldsObj = new List<PeopleSearchDTO.FieldColumn>(fieldsObj);
	                        while(i > 4){
	                            newfieldsObj.remove(i-1);
	                            i = i - 1;
	                        }
	                        newSelectedColumn = JSON.serialize(newfieldsObj);
	                    }else{
	                        newSelectedColumn = selectedColumn;
	                    }
	                    newSelectedColumn = newSelectedColumn.removeEnd(SCREEN_CANDIDATE_COLUMN_REMOVESTRING);
	                    newSelectedColumn = '{"'+cmLabelName+'":' + newSelectedColumn + ',"columnObject":{"name":"'+cmLabelName+'"}}]}';
	                    updateScreenCandidateColumnJSON(newSelectedColumn);
	                    return newSelectedColumn;
	                }
	            }else{
	                // Check if the label name of objects changed, if changed, recreate the column setting string
	                Map<String, List<PeopleSearchDTO.FieldColumn>> objSelectedColumnMap = (Map<String, List<PeopleSearchDTO.FieldColumn>>) JSON.deserialize(selectedColumn, Map<String, List<PeopleSearchDTO.FieldColumn>>.class);
	                if(objSelectedColumnMap != null){
	                    Set <String> objectNames = new Set<String>();
                        objectNames = objSelectedColumnMap.keySet();
                        if(objectNames != null){
                            if(objectNames.size() == 1){
                                if(!objectNames.contains(cmLabelName) && !objectNames.contains(contactLabelName)){
                                    return null;
                                }
                            }else if(objectNames.size() == 2){
                                if(objectNames.contains(cmLabelName) && !objectNames.contains(contactLabelName)){
                                    for(String key:objectNames){
                                        if(key != cmLabelName){
                                            List<PeopleSearchDTO.FieldColumn> tempCMColumns = objSelectedColumnMap.get(key);
                                            objSelectedColumnMap.put(contactLabelName,tempCMColumns);
                                        }
                                    }
                                    return regenerateColumnString(objSelectedColumnMap,selectedColumn);
                                }else if(!objectNames.contains(cmLabelName) && objectNames.contains(contactLabelName)){
                                    for(String key:objectNames){
                                        if(key != contactLabelName){
                                            List<PeopleSearchDTO.FieldColumn> tempCMColumns = objSelectedColumnMap.get(key);
                                            objSelectedColumnMap.put(cmLabelName,tempCMColumns);
                                        }
                                    }
                                    return regenerateColumnString(objSelectedColumnMap,selectedColumn);
                                }else if(!objectNames.contains(cmLabelName) && !objectNames.contains(contactLabelName)){
                                    return null;
                                }
                            }
                        }
	                }
	            }
            }
        }catch (Exception generalError) {
            system.debug(LoggingLevel.ERROR, '\n\nFailed to get Screen Candidate Customized Column \n\n');
        }
        return selectedColumn;
    }

    // given a google search column json, update the user preference record
    public void updateGoogleSearchColumnJSON(String columnJSON){
        User_Preference__c up = new User_Preference__c();
        up = getUserPreference();
        up.Google_Search_Column__c = columnJSON;
        up.Sort_Column_Api_Name__c = '';
        up.Sort_Order__c = '';
        //checkFLS
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            User_Preference__c.Google_Search_Column__c,
            User_Preference__c.Sort_Column_Api_Name__c,
            User_Preference__c.Sort_Order__c
        };
        fflib_SecurityUtils.checkUpdate(User_Preference__c.SObjectType,fieldList);
        update up;
    }
    
    
    // given a screen candidate column json, update the user preference record
    public void updateScreenCandidateColumnJSON(String columnJSON){
        User_Preference__c up = new User_Preference__c();
        up = getUserPreference();
        up.Screen_Candidate_Column__c = columnJSON;
        fflib_SecurityUtils.checkFieldIsUpdateable(User_Preference__c.SObjectType, User_Preference__c.Screen_Candidate_Column__c);
        update up;
    }
    
    // create new user preference record
    public User_Preference__c createNewUserPreference(){
        User_Preference__c up = new User_Preference__c();           
        up.Name = userPreferenceName;
        up.Google_Search_Column__c = getSearchColumnJSONFromXml();
        up.RelatedTo__c = userId;
        up.Unique_User_Preference_Name__c = userPreferenceName;
        up.RecordTypeId = recordTypeId;
        //system.debug('createNewUserPreference up = ' + up);
        try{
            //checkFLS
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                    User_Preference__c.Name,
                    User_Preference__c.Google_Search_Column__c,
                    User_Preference__c.RelatedTo__c,
                    User_Preference__c.Unique_User_Preference_Name__c,
                    User_Preference__c.RecordTypeId
            };
            fflib_SecurityUtils.checkInsert(User_Preference__c.SObjectType, fieldList);
            insert up;
        }catch(Exception ex){
            up = null;
            system.debug('createNewUserPreference error = ' + ex);
            NotificationClass.notifyErr2Dev('DisplayColumnHandler.createNewUserPreference encounter problems.', ex);
        }
        return up;
    }   
    
    // get user preference record by userid, recordtype id, user preference name
    public User_Preference__c getUserPreference(){
        User_Preference__c up = new User_Preference__c();
        try{
        	CommonSelector.checkRead(User_Preference__c.sObjectType, ' Id, Name, Google_Search_Column__c, Google_Search_View__c,Number_of_Records_Per_Page__c,Sort_Column_Api_Name__c, Sort_Order__c,Screen_Candidate_Column__c,Screen_Candidate_Sort_Column_API_Name__c, Screen_Candidate_Sort_Order__c');
            up = [select    Id, Name, Google_Search_Column__c, Google_Search_View__c,Number_of_Records_Per_Page__c,Sort_Column_Api_Name__c, Sort_Order__c,  
            				Screen_Candidate_Column__c,Screen_Candidate_Sort_Column_API_Name__c, Screen_Candidate_Sort_Order__c
                    from    User_Preference__c 
                    where   RelatedTo__c =: userId 
                    and     Recordtypeid =:recordTypeId 
                    and     Unique_User_Preference_Name__c =:userPreferenceName 
                    limit 1];
            //system.debug('getUserPreference up = ' + up);        
        }catch(Exception ex){
        	system.debug('getUserPreference error = ' + ex);
            // if the user preference record does not exit, create a new one
            up = createNewUserPreference();
        }     
        return up;
    }

    //generate google search column JSON String
    private String parsingXMLColumn(){
        String returnstring = '';
        Document tempdoc;
        
        //get the unique xml file for each subscriber org
        if(useUniquePackageXMLColumn){
        	tempdoc = DaoDocument.getCTCConfigFileByDevName(uniqueXMLFileName);
        }
        
        //if no unique xml file or useUniquePackageXMLColumn is not set use the default package xml
        if(tempdoc == null){
        	tempdoc = DaoDocument.getCTCConfigFileByDevName(XML_FILE_NAME);
        }
        
        system.debug('tempdoc id ='+ tempdoc.id);
        if(tempdoc != null && tempdoc.Body.toString()!= null){
            xmldom theXMLDom = new xmldom(tempdoc.Body.toString()); //read xml from document
            returnstring = parsingXMLtoColumn(theXMLDom, CONTACT_OBJECT_NAME);
            
        }return returnstring;
    }
    
    //General Display column List from XML
    private String parsingXMLtoColumn(xmldom theColumnXMLDOM, String objectName){
        String DisplaycolumnJsonString = '';
        List<DisplayColumn> Displaycolumns = new List<DisplayColumn>();
        if(theColumnXMLDOM.getElementsByTagName('columns') != null){
            List<xmldom.Element> columns = theColumnXMLDOM.getElementsByTagName('columns');
            if(columns.size()>0 && columns[0].hasChildNodes()){
                Displaycolumns = generalColumns(columns[0].childNodes, objectName);
                if(Displaycolumns.size()>0){
                    DisplaycolumnJsonString = JSON.serialize(Displaycolumns);
                }else{
                    DisplaycolumnJsonString = null;
                }
            }
        }
        //system.debug('DisplaycolumnJsonString='+DisplaycolumnJsonString);
        return DisplaycolumnJsonString;
    }
    
    //Genearl Display column
    private List<DisplayColumn> generalColumns(List<xmldom.Element> columns, String objectName){
        String NameSpacePrefix = (PeopleCloudHelper.getPackageNamespace()).toLowerCase();
        Map<string, SObjectField> tempfields = GetfieldsMap.getFieldMap(objectName);
        //system.debug('keyset ='+tempfields.keySet() );
        List<DisplayColumn> displaycolumnlist = new List<DisplayColumn>();
        for(xmldom.Element column : columns){
            DisplayColumn tempdisplaycolumn = new DisplayColumn();
            if(column.hasChildNodes()){
                List<xmldom.Element> columnattributes = column.childNodes;
                for(xmldom.Element columnattribute: columnattributes){                  
                    if(columnattribute.nodeName.toLowerCase() == 'label'){   
                        tempdisplaycolumn.ColumnName = columnattribute.nodeValue;
                    }else if(columnattribute.nodeName.toLowerCase() == 'api' ){ 
                        String fieldapiname ='';    
                        if(columnattribute.nodeValue.toLowerCase().contains(NameSpacePrefix)){
                            Integer index = NameSpacePrefix.length();
                            fieldapiname = columnattribute.nodeValue.toLowerCase().substring(index);
                        }else{
                            fieldapiname = columnattribute.nodeValue.toLowerCase();
                        } 
                        //system.debug('fieldapiname =' + fieldapiname);
                        if(tempfields.keySet().contains(fieldapiname)
                                && columnattribute.nodeValue.toLowerCase() !='Name'){//check this field is existing && node value is not Name
                            
                            tempdisplaycolumn.Field_api_name = columnattribute.nodeValue;
                        }else{
                            tempdisplaycolumn = null;
                        }
                    }else if(tempdisplaycolumn != null && columnattribute.nodeName.toLowerCase() == 'type'){
                        if(columnattribute.nodeValue.toLowerCase().contains('string')){
                            tempdisplaycolumn.fieldtype = 'String';
                        }else if(columnattribute.nodeValue.toLowerCase().contains('date')){
                            tempdisplaycolumn.fieldtype = 'date';
                        }else{
                            tempdisplaycolumn.fieldtype = columnattribute.nodeValue;
                        }
                    }
                }
            }
            
            if(tempdisplaycolumn != null){//filter incorrect field from XML
            	//Remove "ID" prefix on reference fields
            	if(removeIDPrefix){
	            	tempdisplaycolumn.removeIDFieldOnReferenceField();
            	}
	            
                displaycolumnlist.add(tempdisplaycolumn);
            }
            //system.debug('tempdisplaycolumn ='+tempdisplaycolumn);          
        }       
        return displaycolumnlist;           
    }

    
    // Make the ScreenCandidate JSON string
    private String regenerateColumnString(Map<String, List<PeopleSearchDTO.FieldColumn>> objMap, String columnString){
        String newSelectedColumn;
        newSelectedColumn = '{"'+cmLabelName+'":' + JSON.serialize(objMap.get(cmLabelName)).removeEnd(SCREEN_CANDIDATE_COLUMN_REMOVESTRING) + ',"columnObject":{"name":"'+cmLabelName+'"}}],'
        +'"'+ contactLabelName+'":'+JSON.serialize(objMap.get(contactLabelName)).removeEnd(SCREEN_CANDIDATE_COLUMN_REMOVESTRING) +',"columnObject":{"name":"'+contactLabelName+'"}}]}';
        updateScreenCandidateColumnJSON(newSelectedColumn);
        return newSelectedColumn;
    }
}