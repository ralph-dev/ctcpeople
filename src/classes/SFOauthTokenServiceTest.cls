@isTest
private class SFOauthTokenServiceTest {

    static {
        SFOauthToken__c sfOauthToken = new SFOauthToken__c();
        sfOauthToken.Name='TestOAuthToken';
        sfOauthToken.ConsumerKey__c = '123456789';
        sfOauthToken.SecretKey__c = '123456789';
        sfOauthToken.Name__c= 'testoAuthToken';
        sfOauthToken.RefreshToken__c = 'testOauthtoken';
        sfOauthToken.OAuthToken__c='test Oauth Token';
        sfOauthToken.InstanceURL__c='TestUrl';
        insert sfOauthToken;
    }
    
    @isTest
    static void testSaveTokentoCustomSetting(){
        String jsonString = '{}';
        SFOauthTokenService service = new SFOauthTokenService();
        System.assert(service.saveTokentoCustomSetting(jsonString));
    }
    
    @isTest
    static void testGetAccessToken(){
        SFOauthTokenService service = new SFOauthTokenService();
        System.assertEquals(service.getAccessToken().getStatusCode(), 200);
    }
    
}