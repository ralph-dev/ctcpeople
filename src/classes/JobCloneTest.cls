@IsTest
private class JobCloneTest {
	public testmethod static void cloneTest(){
		System.runAs(DummyRecordCreator.platformUser) {
		Advertisement__c ad1 = new Advertisement__c();
		ad1.WebSite__c='JXT';
		insert ad1;
		ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(ad1);
		ApexPages.currentPage().getParameters().put('id', ad1.id);
		JobClone jc1 = new JobClone(stdController);
		system.assertNotEquals(jc1.cloneAd(), null);
		ad1.WebSite__c='Seek';
		update ad1;
		jc1 = new JobClone(new ApexPages.Standardcontroller(ad1));
		system.assertNotEquals(jc1.cloneAd(), null);
		ad1.WebSite__c='CareerOne';
		update ad1;
		jc1 = new JobClone(new ApexPages.Standardcontroller(ad1));
		system.assertNotEquals(jc1.cloneAd(), null);
		ad1.WebSite__c='Trademe';
		update ad1;
		jc1 = new JobClone(new ApexPages.Standardcontroller(ad1));
		system.assertNotEquals(jc1.cloneAd(), null);
		ad1.WebSite__c='Other';
		update ad1;
		jc1 = new JobClone(new ApexPages.Standardcontroller(ad1));
		try{
			jc1.cloneAd();
		}catch(System.Exception e){
		}
		}
	}
	
}