public with sharing class ContactServices extends SObjectCRUDService{
	private DynamicSelector dynSel;

	public ContactServices() {
		super(Contact.sObjectType);
		dynSel = new DynamicSelector(Contact.sObjectType.getDescribe().getName(),false,true,true);
	}

	/**
	*Update Contact by Contact JSON String
	*@param String
	*@return Contact
	**/
	public override List<SObject> updateRecords(List<Map<String, Object>> sobjs) {
		List<Contact> contacts = (List<Contact>)constructSObjectsForCRUD(sobjs, SObjectCRUDService.OperationType.MODIFY);
		try {
			return updateRecords(contacts);
		}catch(exception e) {
			throw e;
		}
	}

	/***
		Convert Contact Info to Biller Contact.
					---Alvin
	
	***/
	public list<map<String,Object>> convertContactToBillerContact(list<Contact> billerContacts){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> contactBillerContactMap
				=ctcPeopleHelperService.getContactBillerContactMap('Biller_Contacts_Mapping');
		list<map<String,Object>> billerContactInfos=new list<map<String,Object>>();	
		for(Contact record : billerContacts){
			map<String, Object> billerContactMap=
                ctcPeopleHelperService.convertToAPFormat(record, contactBillerContactMap);
            billerContactMap.put('name',record.get('firstName')+' '+record.get('lastName'));
			billerContactInfos.add(billerContactMap); 
		}
		return billerContactInfos;
	}
	/**
		Convert Contact info to Biller Contact Map
	
	*****/
	public map<String,Object> convertContactToBillerContactMap(list<Contact> billerContacts){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> contactBillerContactMap
				=ctcPeopleHelperService.getContactBillerContactMap('Biller_Contacts_Mapping');
		map<String,Object> billerContactInfos=new map<String,Object>();	
		for(Contact record : billerContacts){
			map<String, Object> billerContactMap=
                ctcPeopleHelperService.convertToAPFormat(record, contactBillerContactMap);
           	billerContactMap.put('name',record.get('firstName')+' '+record.get('lastName'));
			billerContactInfos.put(record.Id,billerContactMap); 
		}
		return billerContactInfos; 
	}
	
	/**
		Convert contact ids to Contacts
	
	**/
	public list<Contact> retrieveBillerContactsByIds(list<String> billerContactIds){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		String queryStr=ctcPeopleHelperService.formQueryFromMap(ctcPeopleHelperService.getContactBillerContactMap('Biller_Contacts_Mapping'));
		if(!queryStr.toLowerCase().contains('firstname')){
			queryStr='firstName, '+queryStr;
		}
		if(!queryStr.toLowerCase().contains('lastname')){
			queryStr='lastName, '+queryStr;
		}
		
		CommonSelector.checkRead(Contact.sObjectType, queryStr);
		queryStr='Select '+queryStr+' from Contact where id in : billerContactIds';
		list<Contact> billingContacts=new list<Contact>();
		
		billingContacts=database.query(queryStr);
		return billingContacts;
	}
	/**
	
		Convert Contact to Approver.
						--Alvin
	
	******/
	public list<map<String,Object>> convertContactToApprover(list<Contact> approvers){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> approverContactMap
				=ctcPeopleHelperService.getContactApproverMap('Approver_Mapping');
		
		list<map<String,Object>> approverInfos=new list<map<String,Object>>();
		for(Contact record : approvers){
			map<String,Object> singleApproverInfo=ctcPeopleHelperService.convertToAPFormat(record, approverContactMap);
			singleApproverInfo.put('USERTYPE','approver');
			approverInfos.add(singleApproverInfo); 
		}
		return approverInfos; 
			
	}
	/***
		Convert Contact to Approver Info
		
	
	***/
	public map<String,Object> convertContactToApproverMap(list<Contact> approvers){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> approverContactMap
				=ctcPeopleHelperService.getContactApproverMap('Approver_Mapping');
		map<String,Object> approverInfos=new map<String,Object>();
		for(Contact record : approvers){
			map<String,Object> singleApproverInfo=ctcPeopleHelperService.convertToAPFormat(record, approverContactMap);
			singleApproverInfo.put('USERTYPE','approver');
			approverInfos.put(record.Id,singleApproverInfo);  
		}
		return approverInfos; 
			
	}
	/**
		Convert contact ids to approver contacts
	
	**/
	public list<Contact> retrieveApproverContactsByIds(list<String> approverContactIds){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		String queryStr=ctcPeopleHelperService.formQueryFromMap(ctcPeopleHelperService.getContactApproverMap('Approver_Mapping'));
		
		CommonSelector.checkRead(Contact.sObjectType, queryStr);
		
		queryStr='Select '+queryStr+' from Contact where id in : approverContactIds';
		list<Contact> approverContacts=new list<Contact>();
		approverContacts=database.query(queryStr);
		return approverContacts;
	}
	
	/***
		Convert Candidate Info to Employee.
					---Alvin
	
	***/
	public list<map<String,Object>> convertCandidateToEmployee(list<Contact> candidates,map<String,String> candidatePlacementMap){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> employMap=ctcPeopleHelperService.getContactEmployeeMap('Employee_Mapping');
		list<String> placementIds=new list<String>();
        placementIds=candidatePlacementMap.values();
        PlacementServices placementServ=new PlacementServices();
        list<Placement_Candidate__c> candidateManagements=placementServ.retrievePlacementsByIds(placementIds);
        map<String,Object> placements=placementServ.convertPlacementToEmployeeMap(candidateManagements);        
		list<map<String,Object>> employeeInfos=new list<map<String,Object>>();        
		for(Contact record : candidates){
			map<String,Object> singleCandidateInfo=ctcPeopleHelperService.convertToAPFormat(record, employMap);
			String placementId=candidatePlacementMap.get(record.Id);
			if(placements.get(placementId)!=null){
	             for(String key: ((map<String,Object>) placements.get(placementId)).keySet()){
					singleCandidateInfo.put(key,((map<String,Object>) placements.get(placementId)).get(key));
				}
			}
			singleCandidateInfo.put('USERTYPE','employee');
			employeeInfos.add(singleCandidateInfo);
		}
		return 	employeeInfos;				
	}
	/**
	
		Convert Contact to Candidate.
						--Alvin
	
	******/
	public map<String,Object> convertCandidateToEmployeeMap(list<Contact> candidates){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> employMap=ctcPeopleHelperService.getContactEmployeeMap('Employee_Mapping');
		map<String,Object> employeeInfos=new map<String,Object>();	
		for(Contact record : candidates){
			map<String,Object> singleApproverInfo=ctcPeopleHelperService.convertToAPFormat(record, employMap);
			singleApproverInfo.put('USERTYPE','employee');
			employeeInfos.put(record.Id,singleApproverInfo);
		}
		return 	employeeInfos;				
	}
	/***
		Convert Contact Id to Candidates
		
	
	***/
	public list<Contact> retrieveCandidateToEmployeesByIds(list<String> candIds){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		String queryStr=ctcPeopleHelperService.formQueryFromMap(ctcPeopleHelperService.getContactEmployeeMap('Employee_Mapping'));
		
		CommonSelector.checkRead(Contact.sObjectType, queryStr);
		
		queryStr='Select '+queryStr+' from Contact where id in : candIds';
		list<Contact> employeeContacts=new list<Contact>();
		employeeContacts=database.query(queryStr);
		return employeeContacts;
	}

	/*
		Get suitable candidates by criteria for AdvancedPeopleSeach function. 
		@Author Jack ZHOU
	
	*/
	public List<candidateDTO> getCandidateByCriteria(Map<String, String> fieldsOperator, Map<String, String> fields, Map<String, String> fieldsType, 
															Map<String, String> skillsMap, List<ShiftDTO> shiftDTOs , 
															Integer radiusRange, double locationLatitude, double locaitonLongitude) {
		List<Contact> suitableCandidates = new List<Contact>();
        List<candidateDTO> displayCandidates = new List<candidateDTO>();
        //search candidates based on the query string
        ContactSelector cSelector = new ContactSelector();
        suitableCandidates = cSelector.getCandidatesByCriteria(fieldsOperator,fields,fieldsType);

        if(suitableCandidates!=null&&suitableCandidates.size()>0) {
	        Map<String, candidateDTO> candidateDTOMap = convertContactToCandidateDTOMap(suitableCandidates);

	        //system.debug('candidateDTOMap = '+ candidateDTOMap);
	        //filter candidates based on skillsMap.
	        if(skillsMap!=null && skillsMap.size()>0) {			
		        candidateSkillServices cSkillServices = new candidateSkillServices();
		        candidateDTOMap = cSkillServices.searchCandidatesBySkills(skillsMap, candidateDTOMap);
	        }
	        //system.debug('candidateDTOMapAfterskills ='+ candidateDTOMap);

	        if(shiftDTOs!=null&&shiftDTOs.size()>0&&candidateDTOMap.size()>0){
		        AvailabilityService aService = new AvailabilityService();
		        candidateDTOMap = aService.rankingCandidatesByAvailability(shiftDTOs, candidateDTOMap);
		    }
		    //system.debug('candidateDTOMapAftershifts ='+ candidateDTOMap);

	        if(radiusRange>0&&locationLatitude!=null&&locaitonLongitude!=null&&candidateDTOMap.size()>0) {
		        candidateDTOMap = cSelector.candidateRadiusSelector(candidateDTOMap, radiusRange, locationLatitude, locaitonLongitude);
		    }
		    //system.debug('candidateDTOMapAfterRadius ='+ candidateDTOMap);
		    //system.debug('candidateDTOMapValues ='+ candidateDTOMap.values());
	        displayCandidates = candidateDTOMap.values();
	    }
        return displayCandidates;
	}
	
	private Map<String, candidateDTO> convertContactToCandidateDTOMap(List<Contact> conList) {
		Map<String, candidateDTO> candidateSearchResultMap = new Map<String, candidateDTO>();   
		
		for(Contact con : conList) {
                candidateDTO cDTO = new candidateDTO();
                cDTO.setCandidateId(con.Id);
                cDTO.setCandidate(con);
                cDTO.setAvailabilityPercentage(0);
                cDTO.setRadiusRanking(0);
                cDTO.setNumberOfRoster(0);
                cDTO.setNumberOfShifts(0);
                candidateSearchResultMap.put(con.Id, cDTO);
        }
        return candidateSearchResultMap;
	}
	
	
	/**
	 * Author: Tony
	 */
    private static final List<String> BASE_FIELDS = new List<String>{'Resume__c', 'Resume_Source__c', 'Resume_Link__c', 'jobApplication_Id__c', 'Skill_Group__c'};
    private static final List<String> AUDIT_FIELDS = new List<String>{'CreatedDate', 'LastModifiedDate'};
    private static final String fsName = PeopleCloudHelper.getPackageNamespace() + 'Deduplication_Update_Set';
	public static Set<String> getRequiredFieldSet() {
        Set<String> ret = new Set<String>();
        List<String> filedsFromFieldSet = ContactDomain.getFieldSetKeyByName(fsName);
        ret.addAll(filedsFromFieldSet);
        
        return ret;
	} 
	
	// Return a list of Contacts that with specific fields by ids used in deduplication
    public static List<Contact> getContactsForDedupUpdate(List<Id> ids) {
        Set<String> requiredFields = getRequiredFieldSet();
        requiredFields.addAll(AUDIT_FIELDS);
        requiredFields.addAll(BASE_FIELDS);
        requiredFields.addAll(new Set<String>{'Id'});
        ContactSelector cs = new ContactSelector(requiredFields);
        
        return cs.getContactsByIds(ids);
    }
    
    // return a contact that been updated by dup contacts
    public static Contact updatedContactForDedup(Contact base, List<Contact> dupContacts) {
        ContactDomain cd = new ContactDomain(dupContacts);
        Set<String> requiredFields = getRequiredFieldSet();
        requiredFields.addAll(BASE_FIELDS);
        
        return cd.mergeContacts(base, requiredFields, 'CreatedDate');
    }
    
}