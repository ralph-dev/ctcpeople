/**
* modified by andy for security review II
*/

public with sharing class AppFormStyleController{
    public String headerid{get;private set;}
    public String footerid{get;private set;}
    public String divid{get;private set;}   
    public String divhtmlstr{get; set;}     
    public AppFormStyleController(){
        String styleId = ApexPages.currentPage().getParameters().get('id');
        if(styleId  != null && styleId != ''){
        	
        	StyleCategory__c sc = (StyleCategory__c) new StyleCategorySelector().setParam('styleId',styleId)
        								.getOne(
        								'Id, Div_Html_File_ID__c, Footer_File_ID__c, Header_File_ID__c ',
        								'Id=:styleId');
        	/**						
	        StyleCategory__c sc = [Select Id, Div_Html_File_ID__c,
	                            Footer_File_ID__c, Header_File_ID__c 
	                            from StyleCategory__c where Id=:styleId ];
	                            */
	        if(sc != null){
	            if(sc.Div_Html_File_ID__c != null){
	                divid = sc.Div_Html_File_ID__c;    
	                
	                Document d = (Document) CommonSelector.simpleQuery(Document.sObjectType).setParam('divid',divid)
	                		.getOne('Id, Body', 'id=: divid');   
	                		
	               // Document d = [select Id, Body from Document where id=: divid];
	                divhtmlstr = d.body.toString();
	            }
	            if(sc.Footer_File_ID__c != null){
	                footerid = sc.Footer_File_ID__c;           
	            }
	            if(sc.Header_File_ID__c != null){
	                headerid = sc.Header_File_ID__c ;           
	            }
			}                    
      	}
    }

	public static testMethod void testController(){
		System.runAs(DummyRecordCreator.platformUser) {
			Document d= new Document();
		    d.folderid = UserInfo.getUserId();
		    String body = '<div></div>';
		    d.name = 'test';
		    d.body =Blob.valueOf(body);
		    d.type ='html';
		    CommonSelector.quickInsert( d);
			
			Document dpic= new Document();
			dpic.folderid = UserInfo.getUserId();
		    String dbody = '';
		    dpic.name = 'test';
		    dpic.body =Blob.valueOf(dbody);
		    dpic.type ='jpg';
		     CommonSelector.quickInsert( dpic);
			
		    StyleCategory__c sc = new StyleCategory__c();
		    sc.Div_Html_File_ID__c = d.Id;
		    sc.Name ='test';
		    sc.Footer_File_ID__c = dpic.Id;
		    sc.Header_File_ID__c = dpic.Id;
		     CommonSelector.quickInsert( sc);
		    
		    ApexPages.currentPage().getParameters().put('id',sc.id);
		    AppFormStyleController thecontroller = new AppFormStyleController();
		    system.assert(thecontroller.divhtmlstr!='');
		}
	    
	
	}
}