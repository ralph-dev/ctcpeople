@isTest
private class ApplicationQuestionSetControllerTest
{
	@testSetup static void setup() {
		Application_Question_Set__c aQuestionSet = new Application_Question_Set__c(Name='Test');
		insert aQuestionSet;
	}

	@isTest
	static void testApplicationQuestionSetController() {
		system.runAs(DummyRecordCreator.platformUser) {
			Application_Question_Set__c aQuestionSet = [select Id from Application_Question_Set__c where Name='test'];
            Application_Question__c aQuestion = new Application_Question__c(Name='test', Application_Question_Set__c = aQuestionSet.Id);
            
			Test.setCurrentPage(Page.ApplicationQuestionSet);
			ApexPages.currentPage().getParameters().put('RecordType', DaoRecordType.SeekScreenFieldRT.Id);

			ApexPages.StandardController sc = new ApexPages.StandardController(aQuestion);
			ApplicationQuestionSetController aQuestionSetController = new ApplicationQuestionSetController(sc);

			PageReference redURL = aQuestionSetController.redirectQuestionPage();
			
			system.assert(redURL.getUrl().contains('seekscreenfieldnewedit'));
		}
	}
}