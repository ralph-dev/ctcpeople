/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AccountServicesTest {
	static testMethod void myUnitTest3() {
		System.runAs(DummyRecordCreator.platformUser) {
			Integer recordNumber=10;
			DataTestFactory.recordNumber=recordNumber;
	    	list<Account> accounts=DataTestFactory.createAccounts();
	    	AccountServices accServices=new AccountServices();
	    	list<map<String,Object>> accountServList=accServices.convertAccountToBiller(accounts);
	    	System.assertEquals(accountServList.size(),recordNumber);
		}
		
    	
    }
    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		Integer recordNumber=10;
			DataTestFactory.recordNumber=recordNumber;
	    	list<Account> accounts=DataTestFactory.createAccounts();
	    	AccountServices accServices=new AccountServices();
	    	map<String,Object> accountServList=accServices.convertAccountToBillerMap(accounts);
	    	System.assertEquals(accountServList.keyset().size(),recordNumber);
    	}
    	
    	
    }
     static testMethod void myUnitTest2() {
     	System.runAs(DummyRecordCreator.platformUser) {
     		Integer recordNumber=10;
			DataTestFactory.recordNumber=recordNumber;
	    	list<Account> accounts=DataTestFactory.createAccounts();
	    	AccountServices accServices=new AccountServices();
	    	list<String> accountIds=new list<String>();
	    	for(Account accountRec: accounts){
	    		accountIds.add(accountRec.Id);
	    	}
	    	list<Account> accountRecords=accServices.retrieveAccountsById(accountIds);
	    	System.assertEquals(accountRecords.size(), recordNumber);
     	}
     	
    	
    }
}