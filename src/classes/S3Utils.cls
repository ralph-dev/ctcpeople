public class S3Utils {
	public static Boolean testFlag = false;
	public static final String SEPERATOR = ':::';
	public JSONObject jobj = new JSONObject();
	public boolean test = false;
	// s3Map, key is bucketname, value is files in this bucket
	public S3Utils(String orgid, map<String, String> s3Map){
		Set<String> buckets = s3Map.keySet();
		for(String bucket:buckets){
			String fs = s3Map.get(bucket);
			jobj.putOpt(bucket, new JSONObject.value(fs));
		}
		jobj.putOpt('orgid', new JSONObject.value(orgid));
		//System.debug('http string = '+jobj.valueToString());
	}
	
	
	@future(callout=true)
	public static void deleteS3files(String jsonString){
		Http h = new Http();
		HttpRequest req = new HttpRequest();
		req.setMethod('POST');
		req.setEndpoint(Endpoint.getcpewsEndpoint()+'/ctcutil/S3Utils'); 
		req.setBody(jsonString);	
		//System.debug('post string = '+jsonString);
		if(Test.isRunningTest()){
			return;
		}else{
			h.send(req);
		}
	}
	
	
	public testmethod static void testS3Utils(){
		System.runAs(DummyRecordCreator.platformUser) {
		S3Utils.testFlag = true;
		map<String, String> s3Map = new map<String, String>();
		s3Map.put('temp003','tonygao12989475247203388.txt');
		S3Utils t = new S3Utils('00D9000000012r8', s3Map);
		system.assert(t.jobj!=null);
		S3Utils.deleteS3files(t.jobj.valueToString());
		}
	}
}