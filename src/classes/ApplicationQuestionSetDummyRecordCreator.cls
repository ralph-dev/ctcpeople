/**
 * This is the dummy record creator for Application_Question_Set object
 * This class is used for testing purpose
 *
 * Created by: Lina Wei
 * Created on: 27/03/2017
 *
 */

@isTest
public with sharing class ApplicationQuestionSetDummyRecordCreator {

    public static Application_Question_Set__c createActiveSeekScreen(Advertisement_Configuration__c account) {
        Application_Question_Set__c aqs = new Application_Question_Set__c();
        aqs.Name = 'Seek Screen Test';
        aqs.RecordTypeId = DaoRecordType.seekScreenRT.Id;
        aqs.Advertisement_Account__c = account.Id;
        aqs.Active__c = true;
        aqs.ID_API_Name__c = '12345';
        insert aqs;
        return aqs;
    }

    public static Application_Question_Set__c createInactiveSeekScreen(Advertisement_Configuration__c account) {
        Application_Question_Set__c aqs = new Application_Question_Set__c();
        aqs.Name = 'Seek Screen Test';
        aqs.RecordTypeId = DaoRecordType.seekScreenRT.Id;
        aqs.Advertisement_Account__c = account.Id;
        aqs.Active__c = false;
        aqs.ID_API_Name__c = '12345';
        insert aqs;
        return aqs;
    }
}