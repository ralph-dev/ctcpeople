public with sharing class OauthRefreshTokenController {
    private String authorizationCode;
    public String refreshToken;
    public String clientId;
    public String clientSecret;
    public String message;
    private ApexPages.message msg;
    
    public PageReference getRefreshToken() {
        SFOauthToken__c sfOauthToken = SFOauthToken__c.getInstance('oAuthToken');
        clientId = sfOauthToken.ConsumerKey__c;
        clientSecret = sfOauthToken.SecretKey__c;
        refreshToken = sfOauthToken.RefreshToken__c;
        
        authorizationCode = Apexpages.currentpage().getParameters().get('code');
        String responseJson = '';
        //system.debug('authorizationCode ='+ authorizationCode + clientId);
        if(authorizationCode!= null) {
            if(clientId == null || clientSecret == null) {
                message='Client Id or Client Secret Key missing. Please contact your admin!';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
                return null;
            }else {
                String redirectUri = OauthSettings.URL_CALLBACK;
            
                PageReference sfOauthPage = new PageReference(OauthSettings.URL_REFRESH_TOKEN);
                sfOauthPage.setRedirect(true);
                sfOauthPage.getParameters().put('code',authorizationCode);
                sfOauthPage.getParameters().put('client_id',clientId);
                sfOauthPage.getParameters().put('grant_type','authorization_code');
                sfOauthPage.getParameters().put('client_secret',clientSecret);
                sfOauthPage.getParameters().put('redirect_uri',redirectUri);
                String URL = sfOauthPage.getUrl();
                //system.debug('URL ='+ URL);
                
                OauthRefreshTokenService oauthRfTS = new OauthRefreshTokenService();
                HttpResponse response = oauthRfTS.getRequestByAuthorization(URL);
                
                if(response.getStatusCode() ==200){
                    responseJson = response.getbody();
                
                    if(responseJson!= null && responseJson!= ''){
                        boolean isSuccess = true;
                        SFOauthTokenService sfOauthTokenService = new SFOauthTokenService();
                        isSuccess = sfOauthTokenService.saveTokentoCustomSetting(responseJson);
                        if(isSuccess) {
                            message = 'Generate Refresh Token and Access Token Successfully!';
                            msg = new ApexPages.message(ApexPages.severity.INFO, message);
                        }else {
                            message = 'Save Refresh Token and Access Token to Customer Setting Unsuccessfully!';
                            msg = new ApexPages.message(ApexPages.severity.ERROR, message);
                        }
                    }
                }else {
                    message = 'Generate Refresh Toekn Unsuccessfully. Please contact your admin!';
                    msg = new ApexPages.message(ApexPages.severity.ERROR, message);
                }
                Apexpages.addMessage(msg);
                return null;
            }
        }else{
            return null;
        }
    }
}