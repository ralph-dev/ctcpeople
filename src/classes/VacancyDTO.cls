public with sharing class VacancyDTO {
	private String vacancyCompanyName;
	private String vacancyName;
	private String vacancyEndDate;
	private String vacancyStartDate;
	private String vacancyId;
	private String vacancyCompanyId;
	private String vacancyRateType;
	private String vacancyCategory;
	private String vacancySpecialty;
	private String vacancySeniority;
	private String vacancyRecordTypeId;
	private String vacancyCompanyShippingStreet;
    private String vacancyCompanyShippingCity;
    private String vacancyCompanyShippingPostalCode;
    private String vacancyCompanyShippingState;
    private String vacancyCompanyShippingCountry;
    private String vacancyCompanyShippingLatitude;
    private String vacancyCompanyShippingLongitude;
	
	public String getVacancyCompanyName() {
		return vacancyCompanyName;
	}
	public void setVacancyCompanyName(String vacancyCompanyName) {
		this.vacancyCompanyName = vacancyCompanyName;
	}
	public String getVacancyName() {
		return vacancyName;
	}
	public void setVacancyName(String vacancyName) {
		this.vacancyName = vacancyName;
	}
	public String getVacancyEndDate() {
		return vacancyEndDate;
	}
	public void setVacancyEndDate(String vacancyEndDate) {
		this.vacancyEndDate = vacancyEndDate;
	}
	public String getVacancyStartDate() {
		return vacancyStartDate;
	}
	public void setVacancyStartDate(String vacancyStartDate) {
		this.vacancyStartDate = vacancyStartDate;
	}
	public String getVacancyId() {
		return vacancyId;
	}
	public void setVacancyId(String vacancyId) {
		this.vacancyId = vacancyId;
	}
	public String getVacancyCompanyId() {
		return vacancyCompanyId;
	}
	public void setVacancyCompanyId(String vacancyCompanyId) {
		this.vacancyCompanyId = vacancyCompanyId;
	}
	/*public String getVacancyRate() {
		return vacancyRate;
	}
	public void setVacancyRate(String vacancyRate) {
		this.vacancyRate = vacancyRate;
	}*/
	public String getVacancyRateType() {
		return vacancyRateType;
	}
	public void setVacancyRateType(String vacancyRateType) {
		this.vacancyRateType = vacancyRateType;
	}
	public String getVacancyCategory() {
		return vacancyCategory;
	}
	public void setVacancyCategory(String vacancyCategory) {
		this.vacancyCategory = vacancyCategory;
	}
	public String getVacancySpecialty() {
		return vacancySpecialty;
	}
	public void setVacancySpecialty(String vacancySpecialty) {
		this.vacancySpecialty = vacancySpecialty;
	}
	public String getVacancySeniority() {
		return vacancySeniority;
	}
	public void setVacancySeniority(String vacancySeniority) {
		this.vacancySeniority = vacancySeniority;
	}
	public String getVacancyRecordTypeId() {
		return vacancyRecordTypeId;
	}
	public void setVacancyRecordTypeId(String vacancyRecordTypeId) {
		this.vacancyRecordTypeId = vacancyRecordTypeId;
	}
	public String getVacancyCompanyShippingStreet() {
		return vacancyCompanyShippingStreet;
	}
	public void setVacancyCompanyShippingStreet(String vacancyCompanyShippingStreet) {
		this.vacancyCompanyShippingStreet = vacancyCompanyShippingStreet;
	}
	public String getVacancyCompanyShippingCity() {
		return vacancyCompanyShippingCity;
	}
	public void setVacancyCompanyShippingCity(String vacancyCompanyShippingCity) {
		this.vacancyCompanyShippingCity = vacancyCompanyShippingCity;
	}
	public String getVacancyCompanyShippingPostalCode() {
		return vacancyCompanyShippingPostalCode;
	}
	public void setVacancyCompanyShippingPostalCode(String vacancyCompanyShippingPostalCode) {
		this.vacancyCompanyShippingPostalCode = vacancyCompanyShippingPostalCode;
	}
	public String getVacancyCompanyShippingState() {
		return vacancyCompanyShippingState;
	}
	public void setVacancyCompanyShippingState(String vacancyCompanyShippingState) {
		this.vacancyCompanyShippingState = vacancyCompanyShippingState;
	}
	public String getVacancyCompanyShippingCountry() {
		return vacancyCompanyShippingCountry;
	}
	public void setVacancyCompanyShippingCountry(String vacancyCompanyShippingCountry) {
		this.vacancyCompanyShippingCountry = vacancyCompanyShippingCountry;
	}
	public String getVacancyCompanyShippingLatitude() {
		return vacancyCompanyShippingLatitude;
	}
	public void setVacancyCompanyShippingLatitude(String vacancyCompanyShippingLatitude) {
		this.vacancyCompanyShippingLatitude = vacancyCompanyShippingLatitude;
	}
	public String getVacancyCompanyShippingLongitude() {
		return vacancyCompanyShippingLongitude;
	}
	public void setVacancyCompanyShippingLongitude(String vacancyCompanyShippingLongitude) {
		this.vacancyCompanyShippingLongitude = vacancyCompanyShippingLongitude;
	}
}