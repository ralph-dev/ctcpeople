@isTest
private class ScheduledPullOccupLibTest {	
	@isTest static void test_method_one() {
	  DataTestFactory.createCTCPeopleSettings();
      ScheduledPullOccupLib scheduledOccupLibs=new ScheduledPullOccupLib();
      String CRON_EXP = '0 0 0 3 9 ?';
      String jobId = System.schedule('TestApex',CRON_EXP,scheduledOccupLibs);
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
        NextFireTime
        FROM CronTrigger WHERE id = :jobId];
         System.assertEquals(CRON_EXP, 
         ct.CronExpression);
      
		system.assert(true);
	}
	
}