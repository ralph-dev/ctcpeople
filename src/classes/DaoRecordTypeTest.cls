@isTest
private class DaoRecordTypeTest {
	
	static testmethod void testGetRecordType(){
		System.runAs(DummyRecordCreator.platformUser) {
        System.assert(DaoRecordType.corpCandidateRT != null);
        System.assert(DaoRecordType.corpContactRT != null);
        System.assert(DaoRecordType.indCandidateRT != null);
        System.assert(DaoRecordType.candidateRTs.size() > 0);
        System.assert(DaoRecordType.nonCandidateRTs.size() > 0);
        System.assert(DaoRecordType.nonCandidateRTIds.size() > 0);
        System.assert(DaoRecordType.corpContactRTs.size() > 0);
        System.assert(DaoRecordType.xeroOauthRT != null);
        System.assert(DaoRecordType.candidatePoolRT != null);
        System.assert(DaoRecordType.contractVacRT != null);
        System.assert(DaoRecordType.ASSaveSearchRT != null);
        System.assert(DaoRecordType.GoogleSaveSearchRT != null);
        System.assert(DaoRecordType.temporaryDataRT != null);
        System.assert(DaoRecordType.googleSearchUserPerformacne != null);
        System.assert(DaoRecordType.ScreenCandidateTempDataRT != null);
        System.assert(DaoRecordType.DefaultSkillGroupRT != null);

        System.assert(DaoRecordType.IndeedRT != null);
        System.assert(DaoRecordType.JXTAdRT != null);
        System.assert(DaoRecordType.linkedInAdRT != null);
        System.assert(DaoRecordType.seekAdRT != null);
        System.assert(DaoRecordType.myCareerAdRT != null);
        System.assert(DaoRecordType.careerOneAdRT != null);
        System.assert(DaoRecordType.templateAdRT != null);

        System.assert(DaoRecordType.seekaccountOldRT != null);
        System.assert(DaoRecordType.seekAccountRT != null);
        System.assert(DaoRecordType.linkedInaccountRT != null);
        System.assert(DaoRecordType.IndeedAdRT != null);
        System.assert(DaoRecordType.jxtAccountRT != null);
        System.assert(DaoRecordType.jobBoardAccRT!=null);
        System.assert(DaoRecordType.adTemplateRT != null);
        System.assert(DaoRecordType.ApplicationFormStyleRT != null);

        System.assert(DaoRecordType.seekScreenRT != null);
        System.assert(DaoRecordType.adConfigLogoRT != null);
        System.assert(DaoRecordType.adConfigTemplateRT != null);
        System.assert(DaoRecordType.SeekScreenFieldRT!=null);
		}
    }
}