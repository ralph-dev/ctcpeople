@isTest
public class SearchAdminControllerTest{
    
    public static testMethod void testController(){
    	System.runAs(DummyRecordCreator.platformUser) {
		SearchAdminController thecontroller = new SearchAdminController();
        thecontroller.pageinit();
        
        thecontroller.setSelectedRecordType('0');
        thecontroller.doRecordTypeSelection();
        
        thecontroller.init();
        //system.debug(thecontroller.allStdfields);
        //system.debug(thecontroller.allCumfields);
        
        List<FieldsClass.AvailableField>  temp = thecontroller.getAllStdfields();
        Integer count =0;
        for(FieldsClass.AvailableField af : temp)
        {
            if(af.getFieldApi()=='Phone' || af.getFieldApi()=='Email'
            || af.getFieldApi()=='LastName' || af.getFieldApi()=='FirstName' ||  af.getFieldApi()=='ReportsToId')
            {
                af.setSelected(true);
            }
            //system.debug('api='+af.getFieldApi());
        }
        List<FieldsClass.AvailableField>  temp2 = thecontroller.getAllCumfields();
        for(FieldsClass.AvailableField af : temp2)
        {
            if(af.getFieldApi()==PeopleCloudHelper.getPackageNamespace()+'Current_Salary__c' || af.getFieldApi()==PeopleCloudHelper.getPackageNamespace()+'Gender__c'
            || af.getFieldApi()==PeopleCloudHelper.getPackageNamespace()+'Personal_Interests__c' || af.getFieldApi()==PeopleCloudHelper.getPackageNamespace()+'Candidate_From_Web__c' 
                || af.getFieldApi() == PeopleCloudHelper.getPackageNamespace()+'Skills__c')
            {
                af.setSelected(true);
            }
        }
        thecontroller.Next();
        //system.debug(thecontroller.getResult());
        ApexPages.currentPage().getParameters().put('f',PeopleCloudHelper.getPackageNamespace()+'Gender__c');
        thecontroller.getPicklistValues();
        thecontroller.Confirm();
        ApexPages.currentPage().getParameters().put('f','ReportsToId');
        thecontroller.getReferenceFields();
        //system.debug('selectedFields=' + thecontroller.selectedFields);
        // for(FieldsClass.AvailableField a : thecontroller.field_options)
        // {
        //     if(a.getFieldApi()=='Email')
        //     {
        //         a.setSelected(true);
        //     }
        // }
        system.assertEquals(thecontroller.SaveRefSelection(), null);
        //system.debug('selectedReferenceFields=' + thecontroller.selectedReferenceFields);
        thecontroller.Preview();
        thecontroller.setParam ('FirstName');
        thecontroller.setSide('left');
        thecontroller.moveUp();
        thecontroller.moveDown();
        ApexPages.currentPage().getParameters().put('f','FirstName');
        thecontroller.SwitchToRight();
        thecontroller.SwitchToLeft();
        // system.debug('columnItems=' + thecontroller.columnItems);
        thecontroller.getColumnItems();
        thecontroller.Column_1 = 'FirstName';
        thecontroller.Column_2 = 'Email';
        thecontroller.Column_3 = 'Phone';
        thecontroller.Submit();
        
        thecontroller.nonAction();
        thecontroller.getNone_Selected();
        thecontroller.getSelected_Referencefields();
        thecontroller.getWarning_msg();
        thecontroller.getMsg_1();
        //thecontroller.getErrmsg();
    	}
        
    }
    
    static testMethod void testGetRt_options() {
    	System.runAs(DummyRecordCreator.platformUser) {
        SearchAdminController c = new SearchAdminController();
        system.assertNotEquals(null, c.getRt_options());
    	}
    }
    
    static testMethod void testPageinit() {
    	System.runAs(DummyRecordCreator.platformUser) {
        ApexPages.currentPage().getParameters().put('o', 'Contact');
        SearchAdminController c = new SearchAdminController();
        system.assertEquals(null, c.pageinit());
    	}
    }
    
    static testMethod void testDoRecordTypeSelection() {
    	System.runAs(DummyRecordCreator.platformUser) {
        SearchAdminController c = new SearchAdminController();
        system.assertNotEquals(null, c.doRecordTypeSelection());
    	}
    }
    
    static testMethod void testOnObjectSelect() {
    	System.runAs(DummyRecordCreator.platformUser) {
        SearchAdminController c = new SearchAdminController();
        system.assertNotEquals(null, c.onObjectSelect());
    	}
    }
    
    static testMethod void testAttributes() {
    	System.runAs(DummyRecordCreator.platformUser) {
        SearchAdminController c = new SearchAdminController();
        
        system.assertEquals(null, c.getIsNext());
        system.assertEquals(null, c.getIsPreview());
        system.assertEquals(null, c.getShow_PicklistPanel());
        system.assertEquals(null, c.getShow_Option_List());
        system.assertEquals(null, c.getShow_FieldsPanel());
        system.assertEquals(null, c.getParam ());
        system.assertEquals(null, c.getSide ());
        system.assertNotEquals(null, c.getLeft_fields ());
        system.assertNotEquals(null, c.getRight_fields ());
        system.assertNotEquals(null, c.getBottom_fields ());
        system.assertEquals(null, c.getSelectedRecordType());
        
        c.setSelectedObject ('test');
        system.assertNotEquals(null, c.getSelectedObject ());
        system.assertNotEquals(null, c.getAllfields());
        system.assertNotEquals(null, c.getResult());
        system.assertNotEquals(null, c.getAvailableoptions ());
        system.assertNotEquals(null, c.getField_options ());
        system.assertEquals(null, c.getSf ());
        // system.assertNotEquals(null, c.);
    	}
    }
}