public with sharing class TradeMePost {
    public Advertisement__c job{get; set;}
    public Advertisement__c newJob{get; set;}
    public StyleCategory__c sc;
    public String[] photourls {get; set;}
    public IEC2WebService iec2ws{get; set;}
    
    /*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}    
    /*************End Add skill parsing param********************************************************/
    
    public TradeMePost(ApexPages.StandardController stdController){
    	//Init skill parsing param. Add by Jack on 23/05/2013
        enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
        enableSkillGroupList = new List<Skill_Group__c>();
        enableSkillGroupList = SkillGroups.getSkillGroups();
        defaultskillgroups = new List<String>();
        //end skill parsing param.
        
        iec2ws = new EC2WebServiceImpl(); 
        job = (Advertisement__c) stdController.getRecord();
        if(job.Skill_Group_Criteria__c!=null&&job.Skill_Group_Criteria__c!=''){
        	String defaultskillgroupstring = job.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
	        defaultskillgroups = defaultskillgroupstring.split(',');
        }
        try{
        	CommonSelector.checkRead(StyleCategory__c.sObjectType, 'ProviderCode__c, CompanyCode__c, OfficeCodes__c');
            sc = [select ProviderCode__c, CompanyCode__c, OfficeCodes__c from StyleCategory__c where WebSite__c='Trademe' and templateActive__c=true]; 
        }catch(System.Exception e){
        }
    }
    public List<SelectOption> getItems(){
        return TrademeUtil.getOfficeCodes(sc.OfficeCodes__c);  
    } 
     public List<SelectOption> getStyleOptions(){ 
        return JobBoardUtils.getStyleOptions('Trademe');
    }
    
    
    public PageReference insertEc2(){
    	CommonSelector.checkRead(User.sObjectType, 'AmazonS3_Folder__c ');
        User currentUser = [select AmazonS3_Folder__c from User where Id=:UserInfo.getUserId()];
        String jobRefCode=UserInfo.getOrganizationId().subString(0, 15)+':'+newJob.Id;
 
        
        CommonSelector.checkRead(StyleCategory__c.sObjectType, 
        	'Id, Header_EC2_File__c, Header_File_Type__c,Div_Html_EC2_File__c, Div_Html_File_Type__c,  Footer_EC2_File__c, Footer_File_Type__c ');
        StyleCategory__c sc = [select Id, Header_EC2_File__c, Header_File_Type__c,
            Div_Html_EC2_File__c, Div_Html_File_Type__c, 
             Footer_EC2_File__c, Footer_File_Type__c  
                from StyleCategory__c where id = :newJob.Application_Form_Style__c limit 1];
        
		/**Insert the job information which is stored in the StyleCategory__c into the
		    EC2 server such jobrefcode, organizationid, Ad ID, the template name like "trademe", "JXT", "Seek" 
		    This method will invoke a function called insertDetailTable in Class EC2WebServiceImpl.
		                                                                            notated by Alvin   
		*/   
        boolean result = iec2ws.insertDetailTable(jobRefCode, UserInfo.getOrganizationId().subString(0, 15),
            String.valueOf(newJob.id), currentUser.AmazonS3_Folder__c, JobBoardUtils.blankValue(sc.Header_EC2_File__c), 
              JobBoardUtils.blankValue(sc.Header_File_Type__c), sc.Div_Html_EC2_File__c,
                sc.Div_Html_File_Type__c,  JobBoardUtils.blankValue(sc.Footer_EC2_File__c), 
                 JobBoardUtils.blankValue(sc.Footer_File_Type__c), '', 'trademe', 
                     JobBoardUtils.blankValue(newJob.Job_Title__c));
        if(result){
            newJob.Status__c = 'Active';
            newJob.Application_URL__c = Endpoint.getcpewsEndpoint()+'/peoplecloud/GetApplicationForm?jobRefCode='+jobRefCode+'&website=trademe';
            newJob.ListingID__c = newJob.Id;
            job.Job_Content__c = TrademeUtil.filter(newJob.Job_Content__c);
            checkFLS();
            update newJob; 
        }else{								//If insert DB not successful, Set the Status value is Not Valid and URL is null; Add by Jack on 10/09/2014
        	newJob.Status__c = 'Not Valid';
        	newJob.Job_Posting_Status__c = 'Insert/Update URL Failed';
        	newJob.Application_URL__c =  '';
        	newJob.ListingID__c = newJob.Id;
            job.Job_Content__c = TrademeUtil.filter(newJob.Job_Content__c);
            checkFLS();
            update newJob;
        }
        return null; 
    }
    
    public PageReference saveAd(){
        newJob = job.clone(false, true);
        newJob.RecordTypeId = JobBoardUtils.getRecordId('Trade_Me');
        newJob.Website__c = 'Trademe';
        newJob.Preferred_Application_Mode__c = 'O';
        newJob.Placement_Date__c = System.today();
        newJob.Provider_Code__c = sc.ProviderCode__c;
        newJob.Company_Name__c = sc.CompanyCode__c;
        if(newJob.Online_Footer__c != null){
            newJob.Online_Footer__c = newJob.Online_Footer__c.replace('[%User Name%]',UserInfo.getUserName());
            System.debug(newJob.Online_Footer__c);
            if(newJob.Reference_No__c!=null){
	            newJob.Online_Footer__c = newJob.Online_Footer__c.replace('[%Vacancy Referece No%]',newJob.Reference_No__c);
	        }
	        System.debug(newJob.Online_Footer__c);
        }
        //insert skill group ext id
	    if(enableskillparsing){
	    	String tempString  = String.valueOf(defaultskillgroups);
	        tempString = tempString.replaceAll('\\(', '');
	        tempString = tempString.replaceAll('\\)', '');
	        tempString = tempString.replace(' ','');
	        newJob.Skill_Group_Criteria__c = tempString;
	    }
	    //end insert skill group ext id
        newJob.photos__c=TrademeUtil.makeUrlString(photourls);
        //check FLS
        checkFLS();
        insert newJob;
        return null;
    }
    public List<SelectOption> getPhotoItems(){
        return TrademeUtil.getPhotos();
    }
    
    //insert the skill group picklist value option
    public List<SelectOption> getSelectSkillGroupList(){
	  	List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
	  	List<SelectOption> displaySelectOption = new List<SelectOption>();
	  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
	  		displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
	  	}
	  	return displaySelectOption;
	}

    private void checkFLS(){
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Advertisement__c.Website__c,
            Advertisement__c.RecordTypeId,
            Advertisement__c.Job_Content__c,
            Advertisement__c.Online_Footer__c,
            Advertisement__c.Skill_Group_Criteria__c,
            Advertisement__c.Reference_No__c,
            Advertisement__c.Application_Form_Style__c,
            Advertisement__c.Job_Title__c,
            Advertisement__c.Application_URL__c,
            Advertisement__c.Status__c,
            Advertisement__c.Job_Posting_Status__c,
            Advertisement__c.ListingID__c,
            Advertisement__c.Preferred_Application_Mode__c,
            Advertisement__c.Placement_Date__c,
            Advertisement__c.Provider_Code__c,
            Advertisement__c.Company_Name__c,
            Advertisement__c.Photos__c
        };
        fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
        fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    }
}