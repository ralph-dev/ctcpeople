@isTest
public with sharing class EmploymentDummyRecordCreator {
	private List<Employment_History__c> emList {get; set;}

    public EmploymentDummyRecordCreator() {
		emList = new List<Employment_History__c>();
	}

	private List<Contact> generateDummyContactRecord(){
		List<Contact> conList = new List<Contact>();
		for(Integer i=0; i<10; i++){
			Contact con = new Contact(LastName = 'testContact'+i);
			conList.add(con);
		}
		insert conList;
		return conList;
	}

	public List<Id> generateDummyRecord() {
		List<Id> conIds = new List<Id>();
		List<Contact> conList = generateDummyContactRecord();
		for(Contact con : conList) {
			Employment_History__c em = new Employment_History__c(Contact__c = con.Id);
			emList.add(em);
			conIds.add(con.Id);
		}

		insert emList;
		return conIds;
	}
}