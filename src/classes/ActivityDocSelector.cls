/**
* modified by andy for security review II
*/
public with sharing class ActivityDocSelector extends CommonSelector{
	
	private static ActivityDocSelector selector = new ActivityDocSelector( );
	public ActivityDocSelector(){
		super(Task.sObjectType);
	}

    public static Task GetTaskById(String id) {
        Task returnResult = new Task();
        String taskQuery = 'SELECT Id, OwnerId, WhoId,Who.name,Who.type, WhatId, What.name,What.type, (SELECT Id, Name FROM Attachments) FROM Task where Id =: id';
        
        try{
        	/**
            returnResult = (Task) selector.addSubselect(Attachment.sObjectType,'Id, Name','')
            	.setParam('id',id)
            	.getOne(
            		'Id, OwnerId, WhoId,Who.name,Who.type, WhatId, What.name,What.type',
            		'Id =: id');
            
            */
            
            selector.checkRead('Id, OwnerId, WhoId,Who.name, WhatId, What.name,What.type');
            CommonSelector.checkRead(Attachment.sObjectType,'Id, Name ');
            
           returnResult = Database.query(taskQuery);
        }catch(Exception e) {
            returnResult = null;
        }
        return returnResult;
    }
    
    public static List<SelectOption> GetResumeFileSelectOption() {
        List<SelectOption> picklistValue = new List<SelectOption>();
        picklistValue = SearchAdminControllerUtils.ConvertPickListtoSelectOption(PeopleCloudHelper.getPackageNamespace()+'Web_Document__c', PeopleCloudHelper.getPackageNamespace()+'Document_Type__c');
        return picklistValue;
    }    
        
    //insert the skill group picklist value option
    public static List<SelectOption> GetSelectSkillGroupList(){
        List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
        List<SelectOption> displaySelectOption = new List<SelectOption>();
        for(Skill_Group__c tempskillgroup : tempskillgrouplist){
            displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
        }
        return displaySelectOption;
    }
}