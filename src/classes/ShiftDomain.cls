public with sharing class ShiftDomain extends SObjectDomain{
	private Shift__c sglShift;
	
	final static String SHIFT_STATUS_CLOSED = 'Closed';
	final static String SHIFT_STATUS_OPEN = 'Open';
	
	public ShiftDomain(List<Shift__c> shifts) {
		super(shifts);
	}

	public ShiftDomain(Shift__c shift) {
		super(new List<Shift__c>{shift});
		this.sglShift = shift;
	}

	public ShiftDomain(){
		super(new List<Shift__c>{});
	}
	
	/**
	* Split Shift that is stored in the SF into shiftDTO for display
	* 
	**/
	public List<Shift__c> splitShiftForDisplay(){
		List<List<Shift__c>> shiftListCollection = new List<List<Shift__c>>();
		List<Shift__c> shiftListForDisplayTotal = new List<Shift__c>();
		for(SObject rec : records){
			Shift__c inputShift = (Shift__c)rec;
			List<Shift__c> shiftsForDisplay = new List<Shift__c>();
			//convert start date, end date to datetime format
			DateTime startDateTime = WizardUtils.convertDateToDateTime(inputShift.Start_Date__c);
			DateTime endDateTime = WizardUtils.convertDateToDateTime(inputShift.End_Date__c);
			
			//single recur dates from start date to recur end date
			List<DateTime> recurDTs = new List<DateTime>();
			
			if(inputShift.Recurrence_End_Date__c == null){
				inputShift.Recurrence_End_Date__c = inputShift.End_Date__c;
			}
			
			//recurWeekdays is a multipicklist value
			//convert to a (weekday, weekday) map
			String recurWeekdays = inputShift.Recurrence_Weekdays__c;
			if(recurWeekdays == '' || recurWeekdays == null){
				return null;
			}
			
			System.debug('recurWeekdays: '+recurWeekdays);
			Map<String,String> recurWeekdaysMap = WizardUtils.convertStringToMap(recurWeekdays);
			System.debug('recurWeekdaysMap Size: '+recurWeekdaysMap.size());
			
			DateTime recurEndDateTime = WizardUtils.convertDateToDateTime(inputShift.Recurrence_End_Date__c);
			
			for(DateTime i = startDateTime; i < WizardUtils.nextDT(recurEndDateTime); i = WizardUtils.nextDT(i)){
				String iWeekday = i.format('EEE');
	
				if(recurWeekdaysMap.containsKey(iWeekday)){
					recurDTs.add(i);
				}
			}
				
			List<Shift__c> tempShiftArr = new List<Shift__c>();
			for(Integer i=0; i<recurDTs.size();i++){
				Shift__c sglShift = new Shift__c();
				sglShift.Start_Date__c = Date.newInstance(recurDTs[i].year(),recurDTs[i].month(), recurDTs[i].day());
				sglShift.End_Date__c = Date.newInstance(recurDTs[i].year(),recurDTs[i].month(), recurDTs[i].day());
				tempShiftArr.add(sglShift);
			}
			
			//System.debug('tempShiftArr'+tempShiftArr);
				
			for(Integer i=0; i<tempShiftArr.size();i++){
				Shift__c sglShift = tempShiftArr[i];
				ShiftHelper.mergeShift(shiftsForDisplay,sglShift);
			}
			
			for(Integer i=0;i<shiftsForDisplay.size();i++){
				Shift__c sglShift = shiftsForDisplay[i];
				ShiftHelper.assignValuesToShift(sglShift, inputShift);
			}
			shiftListCollection.add(shiftsForDisplay);
		}
		
		for(List<Shift__c> sList : shiftListCollection){
			for(Shift__c s : sList){
				shiftListForDisplayTotal.add(s);
			}
		}
		
		return shiftListForDisplayTotal;
	}
	
	public void updateShiftPlacedPositionsNumber(){
		List<String> inputShiftIdList = new List<String>();
		
		for(SObject rec : records){
			Shift__c inputShift = (Shift__c)rec;
			inputShiftIdList.add(inputShift.Id);
		}
		
		Map<String,Integer> shiftIdPlacedRosterNumberMap = ShiftHelper.getShiftIdAndRelatedPlacedRosterNumberMap(inputShiftIdList);
		//System.debug('shiftIdPlacedRosterNumberMap'+shiftIdPlacedRosterNumberMap);
		List<Shift__c> shiftListToUpdate = new List<Shift__c>();
		for(SObject rec : records){
			Shift__c shift = (Shift__c)rec;
			Integer noOfPositionPlaced = shiftIdPlacedRosterNumberMap.get(shift.Id);
			System.debug('noOfPositionPlaced'+noOfPositionPlaced);
			shift.No_of_Position_Placed__c = noOfPositionPlaced;
			if(shift.No_of_Position_Placed__c >= shift.No_of_Position_Requried__c){
				shift.Shift_Status__c = SHIFT_STATUS_CLOSED;
			}else{
				shift.Shift_Status__c = SHIFT_STATUS_OPEN;
			}
			shiftListToUpdate.add(shift);
		}
		
		//System.debug('shiftListToUpdate'+shiftListToUpdate);
		//check FLS
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			Shift__c.No_of_Position_Placed__c,
			Shift__c.Shift_Status__c
		};
		fflib_SecurityUtils.checkInsert(Shift__c.SObjectType, fieldList);
		fflib_SecurityUtils.checkUpdate(Shift__c.SObjectType, fieldList);
		upsert shiftListToUpdate;
	}	
}