@isTest
private class TaskHandlerTest {

	private static void init(){
		TriggerHelper.disableAllTrigger();
		PCAppSwitch__c pcSwitch = PCAppSwitch__c.getInstance();
		pcSwitch.Enable_Last_Contact_Date__c = true;
		upsert pcSwitch;
		TriggerHelper.DisableTriggerOnTask = false;
	}

	static testMethod void testAfterInsertAndUpdateTask(){	
		System.runAs(DummyRecordCreator.platformUser){
			init();
			Contact con = new Contact(LastName='testCon1', Email='blabla@blabla.com');
			Insert con;
			Date date1 = Date.newInstance(2017, 9, 20);
			Task t = new Task(Subject='Call', WhoId=con.Id, ActivityDate=date1, Status='Completed');
			insert t;
			Contact con1 = [Select id, Last_Activity_Date__c from Contact where id = :con.Id];
			Datetime datetime1 = con1.Last_Activity_Date__c;
			System.assertEquals(date1, datetime1.date());

			Date date2 = Date.newInstance(2017,  9,  21);
			t.ActivityDate = date2;
			update t;
			Contact conUpdated = [Select id, Last_Activity_Date__c from Contact where id = :con.Id];
			Datetime datetime2 = conUpdated.Last_Activity_Date__c;
			System.assertEquals(date2, datetime2.date());
		}
	}

	static testMethod void testAfterDeleteTask(){	
		System.runAs(DummyRecordCreator.platformUser){
			init();
			Contact con = new Contact(LastName='testCon2', Email='caca@baba.com');
			insert con;
			Date date1 = Date.newInstance(2017, 09, 20);
			Date date2 = Date.newInstance(2017, 09, 22);
			Task t1 = new Task(Subject = 'test Task 1', WhoId = con.Id, ActivityDate = date1, Status='Completed');
			insert t1;
			Task t2 = new Task(Subject = 'test Task 2', WhoId = con.Id, ActivityDate = date2, Status='Completed');
			insert t2;

			delete t2;
			Contact con1 = [Select Id, Last_Activity_Date__c From Contact Where Id = :con.Id]; 
			Datetime datetime1 = con1.Last_Activity_Date__c;
			System.assertEquals(date1, datetime1.date());

			delete t1;
			Contact con2 = [Select Id, Last_Activity_Date__c From Contact Where Id = :con.Id]; 
			System.assertEquals(null, con2.Last_Activity_Date__c);
		}
		
	}

}