/*
    Wrapper Class for SendResumeEmailTemp.cls
    include the folder and Email Template under the folder
*/

public with sharing class SendResumeEmailTem {
    
    public WrapperFolder emailWrapperfolder{get; set;}
    public List<EmailTemplate> emailTemplateFolder {get; set;}
    public EmailTemplate newEmailTemplate;
    
    public SendResumeEmailTem(WrapperFolder newWrapperFolder){
        emailWrapperfolder = newWrapperFolder;
        //emailTemplateFolder = DaoEmailTemplate.getAllActiveTemplatesById(emailWrapperfolder.folderId);
        
        // added by kevin 
        // to fix the view state limit bug
        emailTemplateFolder = DaoEmailTemplate.getAllActiveTemplatesWithLimitedFieldsById(emailWrapperfolder.folderId);
    }   
}