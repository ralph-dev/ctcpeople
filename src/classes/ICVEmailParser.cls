public interface ICVEmailParser {
    
    
    void setEmail(Messaging.InboundEmail email);
    CVContent parse(); 
    JXTContent parseJxtEmail();  
    
}