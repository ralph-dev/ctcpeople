public with sharing class UserServices {
		/***
		*
		*Convert User Info to Owner.
		*			---Alvin
		*
		***/
		
	public list<map<String,Object>> convertUserToOwner(list<User> userRecords){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> userOwnerMap=ctcPeopleHelperService.getUserOwnerMap('Owner_Mapping');
		list<map<String,Object>> ownerInfos=new list<map<String,Object>>();	
		for(User record : userRecords){
			map<String,Object> ownerMapping= ctcPeopleHelperService.convertToAPFormat(record, userOwnerMap);
			if(ownerMapping.get('USERTYPE')==null){
				ownerMapping.put('USERTYPE','Recruiter');
			}
			if(ownerMapping.get('name_first')==null)
				ownerMapping.put('name_first','-');
			ownerInfos.add(ownerMapping); 
		}
		return 	ownerInfos;				
	}

	public Integer updateNumberOfRecordsShowing(Integer numOfRecords) {
		User u = new UserSelector().getCurrentUser();
		u.Number_of_Records_Showing__c = String.valueOf(numOfRecords);
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
				User.Number_of_Records_Showing__c
		};
		try {
			fflib_SecurityUtils.checkUpdate(User.SObjectType, fieldList);
			update u;
		} catch(Exception e) {
			System.debug('Fail to update number of records showing for user setting. ' + e);
			return null;
		}
		return numOfRecords;
	}

}