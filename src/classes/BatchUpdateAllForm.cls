global class BatchUpdateAllForm implements Database.Batchable<sObject>,Database.AllowsCallouts{
 //** batchable process the auto-update application form according to the change user made for the fields
 //** this is called by class AutoUpdateProcess which will be run by a scheduler 'scheduledBatchUpdateForm'
    public String query;
    public Map<string, SObjectField> allfields;
    public String fieldapi; 

global database.querylocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query);}

global void execute(Database.BatchableContext BC, List<sObject> scope){
    String theid = '';
    Object idobj ;
    StyleCategory__c temp;
    
    for(sObject s : scope)
    {
       temp = (StyleCategory__c)s;
       idobj = temp.get(fieldapi);
       theid = (String)idobj;
       //system.debug('each id =' + theid);
       if(idobj != null)
       {   theid = (String)idobj;
           //system.debug('each id =' + theid);
           AutoUpdateAppForm autoupdate = new AutoUpdateAppForm(allfields,theid);
       }
       
    }
   
    
}

global void finish(Database.BatchableContext BC){

}


}