public with sharing class LinkedInExt {
    Placement__c p;
    public String jobtitle{get;set;}
    public LinkedInExt(ApexPages.StandardController controller){
        p = (Placement__c)controller.getRecord();
        CommonSelector.checkRead(Placement__c.sObjectType,'Online_Job_Title__c,Id');
        Placement__c temp = [select Online_Job_Title__c,Id from Placement__c where Id=: p.Id];
        jobtitle = temp.Online_Job_Title__c;
    }
    public static testMethod void testcon(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Placement__c p = new Placement__c(Name='testthis ha' ,Online_Job_Title__c='Manager');
        CommonSelector.quickInsert( p);
        ApexPages.StandardController con = new ApexPages.StandardController(p);
        LinkedInExt thecontroller = new LinkedInExt(con);
        system.assertEquals(thecontroller.jobtitle, 'Manager');		
    	}
    }
}