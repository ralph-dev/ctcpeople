public with sharing class AvailabilityEntity {
	public String eventReason;
	public String eventStatus; 
    public String startDate; 
    public String endDate; 
    public String id;
    public String candidateId;
    public String candidateManagementId;
    public String candidateManagementName;
    public String clientName; 
    public String vacancyName;
}