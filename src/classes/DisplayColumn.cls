public with sharing class DisplayColumn {
	public String ColumnName{get; set;}        //Display Name
	public String Field_api_name{get; set;}    //Search Field Name
	public String Order{get; set;}             //Display Order
	public String fromasc{get; set;}          //ASC or DESC
	public Boolean defaultorder{get; set;}      //Default sort column
	public String fieldtype {get; set;}        //Display Field type
	
	public String referenceFieldName{get;set;} // field name of the reference field if applicable
	public Boolean isCustomField{get;set;} // indicate whether this field is a costom field or not
	public Boolean sortable{get;set;}	// indicates whether a query can sort on this field (true) or not (false).
	
	public void removeIDFieldOnReferenceField(){
		ColumnName = PeopleSearchHelper.removeIDPrefixIfReferenceField(ColumnName, fieldtype);
	}
}