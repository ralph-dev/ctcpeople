public with sharing class WebDocumentTriggerSelector {

    public static List<Attachment> getContactAttachment(List<String> conIdList) {
        //generate contact record type list
        PCAppSwitch__c ps = PCAppSwitch__c.getInstance();
        String contactRecordTypeListString = ps.Contact_RecordType_for_Create_Documents__c;
        Set<String> contactRecordIdSet = new Set<String>();
        
        if (contactRecordTypeListString != null && contactRecordTypeListString != '') {            
            contactRecordIdSet = new Set<String>(contactRecordTypeListString.split(','));
        }else {
            contactRecordIdSet = new Set<String>(DaoRecordType.nonCandidateRTIds);
        }
        
        CommonSelector.checkRead(Contact.SObjectType,'id, recordTypeId');
        List<Contact> currentlyContactList = [select id, recordTypeId from Contact where id in: conIdList];
       
        List<Contact> corpConList = new List<Contact>();
        for(Contact con: currentlyContactList) {
            if(contactRecordIdSet.contains(String.valueOf(con.RecordTypeId))) {
                corpConList.add(con);
            }
        }
        List<Attachment> attList = new List<Attachment>();
        CommonSelector.checkRead(Attachment.SObjectType,'id');
        attList = [Select id from Attachment where ParentId in: corpConList and Name like 'CD_%'];
        return attList;
    }
    
    /**
     * This method take in a list of candidate Id and return a map of <CandidateID, LastModfiedResume>
     * Added by Lina
     */ 
    public static Map<Id, Web_Document__c> getCandResumeMap(List<Id> conIdList, List<Id> newResumes) {
        
        Map<Id, Web_Document__c> candResumeMap = new Map<Id, Web_Document__c>();  
        List<Web_Document__c> allResumes = new List<Web_Document__c>();
        Set<Id> newResumeIds = new Set<Id>();
        newResumeIds.addAll(newResumes);

        // Select all resumes for the candidate in conIdList
        CommonSelector.checkRead(Contact.SObjectType,'Id');
        CommonSelector.checkRead(Web_Document__c.SObjectType,'Id, Name, Document_Related_To__c, S3_Folder__c, ObjectKey__c, LastModifiedDate');
        List<Contact> contacts = [Select Id, (Select Id, Name, Document_Related_To__c, S3_Folder__c, ObjectKey__c, LastModifiedDate, CreatedDate 
                                              From Contact.Web_Documents__r Where (Document_Type__c = 'Resume' OR Type__c = 'Resume') order by CreatedDate DESC)
                                  From Contact Where Id in: conIdList];
        for (Contact con : contacts) {
            for(Web_Document__c docTmp : con.Web_Documents__r){
                if(newResumeIds.contains(docTmp.Id)) {
                    continue;
                } else {
                    candResumeMap.put(docTmp.Document_Related_To__c, docTmp);
                    break;
                }
            }
        }
        
        return candResumeMap;
    } 
}