/**
* Description: This is a service class for all services related to vacancy in advanced people search/roster
* Author: Emily Jiang
* Date: 18.06.2015
**/
public with sharing class VacancyService {
	
	// update/insert vacancy from advance people search wizard
	public List<Placement__c> upsertVacancyFromWizard(List<VacancyDTO> vacDTOList){
		if(vacDTOList == null || vacDTOList.size() == 0){
			return null;
		}
		
		List<Placement__c> vacList = new List<Placement__c>();
		List<String> vacIdList = new List<String>();
		List<Placement__c> vacListSelected = new List<Placement__c>();
		
		//convert VacancyDTO to Vacancy sObject
		for(VacancyDTO vacDTO : vacDTOList){
			Placement__c vacancy = new Placement__c();
			VacancyHelper.convertVacancyDTOToVacancy(vacancy,vacDTO);
			vacancy.From_Wizard__c = true; //use this field to get around customise validation rule 
			vacList.add(vacancy);
		}
			
		upsert vacList; 
		
		for(Placement__c vac : vacList){
			vacIdList.add(vac.Id);
		}
		
		//fetch full info of vacancy
		VacancySelector vacSelector = new VacancySelector();
		vacListSelected = vacSelector.fetchVacancyListByIdList(vacIdList);
		
		return vacListSelected;
	}
}