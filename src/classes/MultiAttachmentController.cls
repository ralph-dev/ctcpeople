/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

/*
 * Controller for multi attachment component
 */
 
public with sharing class MultiAttachmentController 
{
	public List<Document> myfiles{get; set;}
	public static final Integer NUM_ATTACHMENTS_TO_ADD=1;
	//public Blob blobObj {get; set;}
	public String optInlocalDocString {get ; set;}
	public String erroroptInlocalDocString {get ; set;}
	public Boolean triggerParentPage {get; set;}
	public Integer myfilesize;
	
	public PeopleCloudErrorInfo errormesg; 
	public Boolean msgSignalhasmessage{get ;set;} //show error message or not
	
	public void MultiAttachmentController()
	{
		// instantiate the list with a single document		
		triggerParentPage = false;
		msgSignalhasmessage = false;
		myfiles = new List<Document>{new Document(),new Document(),new Document()};	
	}	
	
	public void addMore(){
		myfilesize = myfiles.size();
		if(myfilesize<10){
    		myfiles.clear();
	    	for(integer idx=0; idx<myfilesize+1; idx++){
	    		myfiles.add(new Document());
	    	}
    	}
    	else{
    		customerErrorMessage(PeopleCloudErrorInfo.WARN_TOO_MANY_LOCDOCS);	
    	}
    }
	
	public void createDummyDoc(){
		erroroptInlocalDocString= '';
		optInlocalDocString = '';
    	DocRowRec dummyDocRowRec = null;
		String filenameString = '';		
		Id localDocUserId = Userinfo.getUserId();
    	Integer id = 0; 
    	try{    		
	    	for(Document newDoc : myfiles){
	    		String startTimeStamp = ''+Datetime.now().getTime();	
	    		if(newDoc.Body != null){
		    		Document tempdoc = new Document();		    		
		    		if((newDoc.Name).contains('.')){
						tempdoc.Type = (newDoc.Name).substring((newDoc.Name).lastIndexOf('.')+1,(newDoc.Name).length());
						filenameString = (newDoc.Name).substring(0, (newDoc.Name).lastIndexOf('.')); 
					}	
					else{
						filenameString = newDoc.Name;
						tempdoc.Type = 'Unknown';
					}
		    		Pattern p = Pattern.compile('[^a-zA-Z0-9]*');//elmitate other characters besides character and number
		    		String filename = p.matcher(filenameString).replaceall('');	
		    		if(filename.length()> 10){
		    			filename = filename.substring(0,10);
		    		}
		    		triggerParentPage = true;
		    		if((newDoc.body).size() < 5120000){    		
				    	tempdoc.Name = newDoc.Name;
						tempdoc.Body = newDoc.body; 					
						tempdoc.DeveloperName = 'SR_'+UserInfo.getUserId() + '_' + filename+'_'+startTimeStamp;
						system.debug('DeveloperName =' + tempdoc.DeveloperName);
				        tempdoc.FolderId = UserInfo.getUserId();
				        //check FLS
				        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
				        	Document.Type,
				        	Document.Name,
				        	Document.Body,
				        	Document.DeveloperName,
				        	Document.FolderId
				        };
				        fflib_SecurityUtils.checkInsert(Document.SObjectType, fieldList);
				        insert tempdoc;			        
				        optInlocalDocString = optInlocalDocString + 'id' + id + '=' + tempdoc.id + ';';
				        id++;
		    		}
		    		else{
			        	erroroptInlocalDocString = erroroptInlocalDocString + newDoc.Name +';';
						newDoc.Body = null;	
						continue;
			        }
			        newDoc.Body = null;
	    		}
	    	} 
    	}
    	catch(Exception e){    		
    		myfiles.clear();// show system exception, clear the myfile list firstly
    		triggerParentPage = false;
    		customerErrorMessage(PeopleCloudErrorInfo.ERR_UPLOAD_DOCUMENT_FAIL);					
			NotificationClass.notifyErr2Dev('MultiAttachmentController/createDummyDoc', e);
    	}
    	//return null;
	}  

	private void customerErrorMessage(Integer msgcode){
        errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
                msgSignalhasmessage = errormesg.returnresult;
                for(ApexPages.Message newMessage:errormesg.errorMessage){
                        ApexPages.addMessage(newMessage);
                }               
    }

	static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){//Send error message
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());    
    }  	
}