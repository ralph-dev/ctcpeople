@isTest
private class CredentialSettingRESTTest {

	private static testMethod void testGetProtectedCrendential() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        
	        RestRequest req = new RestRequest();
    	    RestResponse res = new RestResponse();
    	    
    	    req.requestURI = '/services/apexrest/credential';
    	    req.addParameter('name','test');
    	    req.httpMethod = 'GET';
    	    restContext.request = req;
    	    restContext.response = res;
    	    
    	    
    	    Test.startTest();
    	    
    	    ProtectedCredential testpc = new ProtectedCredential();
    	    testpc.name = 'test';
    	    testpc.password = 'password';
    	    testpc.relatedTo = 'test';
    	    
    	    CredentialSettingService.insertCredential(testpc);
    	    
    	    ProtectedCredential pc = CredentialSettingREST.getProtectedCrendential();
    	    
    	    Test.stopTest();
    	    
    	    //System.assertEquals(200, res.statusCode);
    	    
    	    System.assertEquals('password', pc.password);
	        
	    }
	    
	    

	}
	
	private static testMethod void testSaveProtectedCrendential() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        
	        RestRequest req = new RestRequest();
    	    RestResponse res = new RestResponse();
    	    
    	    req.requestURI = '/services/apexrest/credential';
    	    req.httpMethod = 'POST';
    
    	    restContext.request = req;
    	    restContext.response = res;
    	    
    	    
    	    Test.startTest();
    	    
    	    ProtectedCredential pc = CredentialSettingREST.saveProtectedCrendential('','','test','','','','password','','','','test');
    	    
    	    Test.stopTest();
    	    
    	    
    	    
    	    System.assertEquals('password', pc.password);
    	    
    	    pc = CredentialSettingService.getCredential('test');
    	    
    	    System.assertEquals('password', pc.password);
	        
	    }
	    
	    

	}
	
	



}