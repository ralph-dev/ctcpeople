@isTest
private class OAuthCallbackControllerTest {

    private static OAuthCallbackController construct(){
        Test.setCurrentPageReference(new PageReference('Page.OAuthCallback'));
        System.currentPageReference().getParameters().put('refresh_token', 'dummy_refresh_token');
        System.currentPageReference().getParameters().put('access_token', 'dummy_access_token');
        System.currentPageReference().getParameters().put('signature', 'dummy_signature');
        System.currentPageReference().getParameters().put('return_url', System.URL.getSalesforceBaseUrl().toExternalForm() + '/apex/AppsAuth');
        System.currentPageReference().getParameters().put('app_id', 'Test_Connected_App');
        OAuthCallbackController occ = new OAuthCallbackController();
        return occ;
    }
    
    @isTest static void test_redirect() {
        OAuthCallbackController occ = OAuthCallbackControllerTest.construct();
        PageReference pr = occ.redirect();
        system.assert(pr!=null);
    }
    
    @isTest static void test_saveSFOAuth() {
        OAuthCallbackController occ = OAuthCallbackControllerTest.construct();
        SFOauthToken__c sfOauthToken = SFOauthTokenSelector.querySFOauthToken('oAuthToken');
        List<SFOauthToken__c> sfOauthTokenList = new List<SFOauthToken__c>();
        sfOauthTokenList.add(sfOauthToken);
        occ.saveSFOAuth(sfOauthTokenList);
        system.assertNotEquals(null, sfOauthToken);
    }

    @isTest static void test_saveRedirectErrorPage() {
        OAuthCallbackController occ = new OAuthCallbackController();
        PageReference pr = occ.redirect();
        system.assert(pr!=null);
    }
    
}