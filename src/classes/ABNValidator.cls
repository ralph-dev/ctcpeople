public with sharing class ABNValidator {
    public static void validateAccABN(List<Account> accInputList) {
        String pid=UserInfo.getProfileId();

        PCAppSwitch__c CSInstance = PCAppSwitch__c.getinstance(pid);


        if(CSInstance == null || !CSInstance.Turn_On_ABN_Validator_Switch__c){
            return;
        }

        for(Account acc : accInputList){
            if(acc.ABN__c == null || acc.ABN__c == '' ){
                continue;
            }

            if(!GlobalUtility.ValidateABN(acc.ABN__c)){
                acc.addError('Invalid ABN number');
                continue;
            }
        
            String ABNRemovedSpace
                = GlobalUtility.getNonBlankNumericStringWithoutWhitespace
                        (acc.ABN__c);
            acc.ABN_Not_Formatted__c = ABNRemovedSpace;
            //System.debug('ABNRemovedSpace'+ABNRemovedSpace);
            String ABNFormatted=ABNRemovedSpace.substring(0,2)+' '
                                +ABNRemovedSpace.subString(2,5)+' '
                                +ABNRemovedSpace.subString(5,8)+' '
                                +ABNRemovedSpace.subString(8,11);
            acc.ABN__c = ABNFormatted;    
        }
    }

 }