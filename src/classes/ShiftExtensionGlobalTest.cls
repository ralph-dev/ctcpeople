@isTest
private class ShiftExtensionGlobalTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
       Account acc = TestDataFactoryRoster.createAccount();
       Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
       Shift__c shift = TestDataFactoryRoster.createShift(vac);
       ShiftDTO sDTO = TestDataFactoryRoster.createShiftDTO(acc,vac,shift);
       //System.debug('Unit Test Shift DTO: '+sDTO);
       
       List<ShiftDTO> sDTOList = new List<ShiftDTO>();
       sDTOList.add(sDTO);
      
       /**
       * Since when shift is created, not every field is set. Need to create/update the shift first, then 
       * test the rest methods.
       *
       **/
       //test method ShiftExtensionGlobal createShifts(String shiftJson>)
       String shiftDTOStr = JSON.serialize(sDTO);
       List<Shift__c> shiftListCreated = ShiftExtensionGlobal.createShifts(shiftDTOStr);
       System.assertEquals(shiftListCreated,null);
       
       //test method ShiftExtensionGlobal updateShifts(String shiftJson>)
       //System.debug('shiftDTOStr'+shiftDTOStr);
       List<Shift__c> shiftDBListUpdated = ShiftExtensionGlobal.updateShiftDB(shiftDTOStr);
       System.assertEquals(shiftDBListUpdated,null);
       //System.debug('shiftDBListUpdated'+shiftDBListUpdated);
       
       //test method ShiftExtensionGlobal retrieveShiftsByVacancyId(VacancyId)
       List<Shift__c> shiftListRetrievedByVacancyId = ShiftExtensionGlobal.retrieveShiftsByVacancyId(vac.Id);
       System.assertEquals(shiftListRetrievedByVacancyId,null);
       
       //test method ShiftExtensionGlobal retrieveShiftsDBByVacancyId(VacancyId)
       List<Shift__c> shiftDBListRetrievedByVacancyId = ShiftExtensionGlobal.retrieveShiftsDBByVacancyId(vac.Id);
       System.assertEquals(shiftDBListRetrievedByVacancyId, null);
       
       //test method ShiftExtensionGlobal retrieveShiftsByShiftId(shiftId)
       List<Shift__c> shiftListRetrievedByShiftId = ShiftExtensionGlobal.retrieveShiftsByShiftId(shift.Id);
       System.assertNotEquals(shiftListRetrievedByShiftId.size(),0);
     
       //test method ShiftExtensionGlobal deleteAllRecurShifts(String shiftId)
       Boolean isSuccessDeleted = ShiftExtensionGlobal.deleteAllRecurShifts('abc');
       System.assertNotEquals(isSuccessDeleted,true);
       
       //test method ShiftExtensionGlobal describeShift()
       Map<String,List<Schema.PicklistEntry>> describeResult = ShiftExtensionGlobal.describeShift();
       System.assertNotEquals(describeResult,null); 
    	}
    }
}