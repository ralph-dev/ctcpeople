public with sharing class PlacCandUpdEvOnInterviewTriggerHelper {
    public Event get_InternalInterviewEvent_IfExistent  (Placement_Candidate__c pc, List<Event> eventsFromPlacementCandidate){
        System.debug('#################################### PlacCandUpdEvOnInterviewTriggerHelper.get_InternalInterviewEvent_IfExistent.Start');
        for (Event ev1 : eventsFromPlacementCandidate) {
            if( ev1.WhatId  == pc.id && 
                ev1.SUBJECT.startsWith('Internal Interview with Candidate') ) {
                return ev1;
            }
        }
        return null;
    }
    
    public Event createInternalInterviewEvent(Placement_Candidate__c pc){
        System.debug('#################################### PlacCandUpdEvOnInterviewTriggerHelper.createInternalInterviewEvent.Start');
        Datetime i_specific_date_time;              
        Datetime i_ACTIVITYDATETIME; 
        Datetime i_STARTDATETIME;           
        Datetime i_ENDDATETIME;             
        Datetime i_REMINDERDATETIME;  
        Date     i_ACTIVITYDATE;
        Event    ev1;   
        i_specific_date_time = pc.Date_and_Time_Internal__c;                
        i_ACTIVITYDATETIME = i_specific_date_time; 
        i_STARTDATETIME    = i_specific_date_time;              
        i_ENDDATETIME      = i_specific_date_time.addHours(1);              
        i_REMINDERDATETIME = i_specific_date_time.addMinutes(-30);                                  
        i_ACTIVITYDATE     = i_specific_date_time.date(); 
         
        System.debug('***************************** i_specific_date_time '     + i_specific_date_time);
        System.debug('***************************** i_ACTIVITYDATETIME '       + i_ACTIVITYDATETIME);
        System.debug('***************************** i_STARTDATETIME '          + i_STARTDATETIME);
        System.debug('***************************** i_ENDDATETIME '            + i_ENDDATETIME);
        System.debug('***************************** i_REMINDERDATETIME '       + i_REMINDERDATETIME  );
        System.debug('***************************** i_ACTIVITYDATE '           + i_ACTIVITYDATE);
        
        ev1 = new Event(
            WHATID              =   pc.id 
            ,SUBJECT            =   'Internal Interview with Candidate ' + pc.Candidate__r.Name     
            ,ISALLDAYEVENT      =   FALSE   
            ,ACTIVITYDATETIME   =   i_ACTIVITYDATETIME  
            ,ACTIVITYDATE       =   i_ACTIVITYDATE  
            ,DURATIONINMINUTES  =   60  
            ,STARTDATETIME      =   i_STARTDATETIME
            ,ENDDATETIME        =   i_ENDDATETIME   
            ,DESCRIPTION        =   pc.Internal_Interview_Details__c   //'Internal Interview with Candidate'            
            ,ISPRIVATE          =   FALSE   
            ,SHOWAS             =   'Busy'                                      
            ,ISRECURRENCE       =   FALSE                                       
            ,REMINDERDATETIME   =   i_REMINDERDATETIME
            ,ISREMINDERSET      =   TRUE
            ,Ownerid            =   pc.Internal_Interview_Consultant__c);   
        
        return ev1; 
    }
}