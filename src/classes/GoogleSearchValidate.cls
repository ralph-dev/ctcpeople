/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

/***********************************************
*
*GoogleSearchValidate Class is check the User Perference record 
*If the record doesn't exist, create a new one according the APS column
*If the APS columns contains not validate field, only validate field can add to User Perference record
*Author by Jack on 4 DEC 2013
*
/***********************************************/

public with sharing class GoogleSearchValidate {
	//Create Google Search Performance Record 
	//public static User_Preference__c checkUserPerformanceValidate(){
	//	User_Preference__c currentGoogleSearchPerformance = new User_Preference__c();
	//	String Userid = Userinfo.getUserId();
	//	String RecordTypeId = DaoRecordType.googleSearchUserPerformacne.id;
	//	String GoogleSearchUserPreferenceName = 'GoogleSearchUserPreferenceName_'+Userid;
		
	//	// get user preference record
	//	DisplayColumnHandler gscHandler = new DisplayColumnHandler(Userid, RecordTypeId ,GoogleSearchUserPreferenceName);	
	//	User_Preference__c up = gscHandler.getUserPreference();		
	//	system.debug('GoogleSearchValidate ----up:' + up);
	//	if(up != null && up.Google_Search_Column__c != null && up.Google_Search_Column__c!='' && up.Google_Search_Column__c!='[]'){
	//		return up;
	//	}
		 
	//	// if user preference does not exist, get the json string from xml
	//	String GoogleSearchColumn =  gscHandler.getSearchColumnJSONFromXml();//General column JSON String
		
	//	if(GoogleSearchColumn == null){
	//		currentGoogleSearchPerformance = null;
	//	}else{
	//		try{
	//			CommonSelector.checkRead(User_Preference__c.sObjectType, 
	//				'Id, Name, Google_Search_Column__c, Google_Search_View__c, Screen_Candidate_Column__c,'
	//				+'Screen_Candidate_Sort_Column_API_Name__c, Screen_Candidate_Sort_Order__c,'
	//				+'Number_of_Records_Per_Page__c,Sort_Column_Api_Name__c, Sort_Order__c ');
					
	//			currentGoogleSearchPerformance = [select Id, Name, Google_Search_Column__c, Google_Search_View__c, Screen_Candidate_Column__c,
	//												Screen_Candidate_Sort_Column_API_Name__c, Screen_Candidate_Sort_Order__c,
	//												Number_of_Records_Per_Page__c,Sort_Column_Api_Name__c, Sort_Order__c  from User_Preference__c where RelatedTo__c =: Userid and Recordtypeid =:RecordTypeId and Unique_User_Preference_Name__c =:GoogleSearchUserPreferenceName limit 1];
	//			//If the user preference record exist, update the column field
	//			currentGoogleSearchPerformance.Google_Search_Column__c = GoogleSearchColumn;
	//			//check FLS
	//			List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
	//				User_Preference__c.Name,
	//				User_Preference__c.Google_Search_Column__c,
	//				User_Preference__c.Google_Search_View__c,
	//				User_Preference__c.Screen_Candidate_Column__c,
	//				User_Preference__c.Screen_Candidate_Sort_Column_API_Name__c,
	//				User_Preference__c.Screen_Candidate_Sort_Order__c,
	//				User_Preference__c.Number_of_Records_Per_Page__c,
	//				User_Preference__c.Sort_Column_Api_Name__c,
	//				User_Preference__c.Sort_Order__c
	//			};
	//			fflib_SecurityUtils.checkUpdate(User_Preference__c.SObjectType, fieldList);
	//			update currentGoogleSearchPerformance;
	//		}
	//		catch(Exception e){
	//			// if the user preference record does not exit, create a new one
	//			gscHandler.createNewUserPreference();
	//		}
	//	}
	//	return currentGoogleSearchPerformance;
	//}
	
	
}