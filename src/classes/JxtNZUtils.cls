/**

A class used for composing Web Service request (JobXML) which will be sent to JobX
to post/update a job.

**/
public class JxtNZUtils {
	public static String getFeed(Advertisement__c ad){
		XMLDom.Element job = new XMLDom.Element('JOB'); 
        job.attributes.put('Reference',UserInfo.getOrganizationId().subString(0, 15)+':'+ad.Id);
        
        XMLDom.Element jobadtype = new XMLDom.Element('JOBADTYPE');
        jobadtype.nodeValue = ad.Job_Type__c;
        job.childNodes.add(jobadtype);
        
        XMLDom.Element whitelabel = new XMLDom.Element('WHITELABEL');
        whitelabel.nodeValue = ad.white_label__c;
        job.childNodes.add(whitelabel);
        
        XMLDom.Element title = new XMLDom.Element('TITLE');
        title.nodeValue = JobBoardUtils.wrappedWithCDATA(ad.Job_Title__c);
        job.childNodes.add(title);
        
        XMLDom.Element description = new XMLDom.Element('DESCRIPTION');
        description.nodeValue = JobBoardUtils.wrappedWithCDATA(ad.JXT_Short_Description__c);
        job.childNodes.add(description);
        
        XMLDom.Element addetails = new XMLDom.Element('ADDETAILS');
        addetails.nodeValue = JobBoardUtils.wrappedWithCDATA(ad.Job_Content__c);
        job.childNodes.add(addetails); 
        
        XMLDom.Element applicationemail = new XMLDom.Element('APPLICATIONEMAIL');
        applicationemail.nodeValue = JobBoardUtils.wrappedWithCDATA(JobBoardUtils.blankValue(ad.Email__c));
        job.childNodes.add(applicationemail); 
        
        XMLDom.Element contactdetails = new XMLDom.Element('CONTACTDETAILS');
        contactdetails.nodeValue = JobBoardUtils.wrappedWithCDATA(JobBoardUtils.blankValue(ad.Job_Contact_Name__c));
        job.childNodes.add(contactdetails);
        
        XMLDom.Element publictransport = new XMLDom.Element('PUBLICTRANSPORT');
        publictransport.nodeValue = JobBoardUtils.wrappedWithCDATA(JobBoardUtils.blankValue(ad.Nearest_Transport__c));
        job.childNodes.add(publictransport); 
        
        XMLDom.Element residentsonly = new XMLDom.Element('RESIDENTSONLY');
        if(ad.Residency_Required__c){
            residentsonly.nodeValue = 'YES';
        }else{
            residentsonly.nodeValue = 'NO';
        }
        job.childNodes.add(residentsonly);
        
        XMLDom.Element hidelocationdetails = new XMLDom.Element('HIDELOCATIONDETAILS');
        if(ad.Location_Hide__c){
            hidelocationdetails.nodeValue = 'YES';
        }else{
            hidelocationdetails.nodeValue = 'NO';
        }
        job.childNodes.add(hidelocationdetails);
        
        XMLDom.Element templateid = new XMLDom.Element('TEMPLATEID');
        templateid.nodeValue=JobBoardUtils.blankValue(ad.Template__c);
        job.childNodes.add(templateid);
        
        XMLDom.Element templatelogoname = new XMLDom.Element('TEMPLATELOGONAME');
        templatelogoname.nodeValue=JobBoardUtils.blankValue('');
        job.childNodes.add(templatelogoname);
        
        XMLDom.Element companyname = new XMLDom.Element('COMPANYNAME');
        companyname.nodeValue=JobBoardUtils.wrappedWithCDATA(JobBoardUtils.blankValue(ad.Company_Name__c));
        job.childNodes.add(companyname);
        
        XMLDom.Element categories = new XMLDom.Element('CATEGORIES');
        job.childNodes.add(categories);
        
        XMLDom.Element category = new XMLDom.Element('CATEGORY');
        categories.childNodes.add(category);
        
        XMLDom.Element classification = new XMLDom.Element('CLASSIFICATION');
        classification.nodeValue = JobBoardUtils.removeAmpersand(ad.Classification__c);
        category.childNodes.add(classification);
        
        XMLDom.Element subclassification = new XMLDom.Element('SUBCLASSIFICATION');
        subclassification.nodeValue = JobBoardUtils.removeAmpersand(ad.SubClassification_Seek_Label__c);
        category.childNodes.add(subclassification);
        
        if(ad.Classification2__c!='0'){
	        XMLDom.Element category2 = new XMLDom.Element('CATEGORY');
	        categories.childNodes.add(category2);
	        
	        XMLDom.Element classification2 = new XMLDom.Element('CLASSIFICATION');
	        classification2.nodeValue = JobBoardUtils.removeAmpersand(ad.Classification2__c);
	        category2.childNodes.add(classification2);
	        
	        XMLDom.Element subclassification2 = new XMLDom.Element('SUBCLASSIFICATION');
	        subclassification2.nodeValue = JobBoardUtils.removeAmpersand(ad.SubClassification2__c);
	        category2.childNodes.add(subclassification2);
        }
        
        XMLDom.Element listing = new XMLDom.Element('LISTING');
        job.childNodes.add(listing);
        
        XMLDom.Element worktype = new XMLDom.Element('CLASSIFICATION');
        worktype.attributes.put('Name','WORKTYPE');
        worktype.nodeValue = ad.WorkType__c;
        listing.childNodes.add(worktype);
        
        XMLDom.Element sector = new XMLDom.Element('CLASSIFICATION');
        sector.attributes.put('Name','SECTOR');
        sector.nodeValue = ad.jxt_sectors__c;
        listing.childNodes.add(sector);
        
        XMLDom.Element industry = new XMLDom.Element('CLASSIFICATION');
        industry.attributes.put('Name','INDUSTRY');
        industry.nodeValue = JobBoardUtils.removeAmpersand(ad.Industry__c);
        listing.childNodes.add(industry);
        
        XMLDom.Element country = new XMLDom.Element('CLASSIFICATION');
        country.attributes.put('Name','COUNTRY');
        country.nodeValue = JobBoardUtils.wrappedWithCDATA(JobBoardUtils.blankValue(ad.Country__c));
        listing.childNodes.add(country); 
         
        XMLDom.Element suburb = new XMLDom.Element('CLASSIFICATION');
        suburb.attributes.put('Name','SUBURB');
        suburb.nodeValue =  JobBoardUtils.wrappedWithCDATA(JobBoardUtils.blankValue(ad.suburb__c));
        listing.childNodes.add(suburb);
        
        XMLDom.Element postcode = new XMLDom.Element('CLASSIFICATION');
        postcode.attributes.put('Name','POSTCODE');
        postcode.nodeValue =  JobBoardUtils.wrappedWithCDATA(JobBoardUtils.blankValue(ad.zipcode__c));
        listing.childNodes.add(postcode);
        
        
        XMLDom.Element streetaddress = new XMLDom.Element('CLASSIFICATION');
        streetaddress.attributes.put('Name','STREETADDRESS');
        streetaddress.nodeValue =  JobBoardUtils.wrappedWithCDATA(JobBoardUtils.blankValue(ad.Street_Adress__c));
        listing.childNodes.add(streetaddress);
        
        XMLDom.Element tags = new XMLDom.Element('CLASSIFICATION');
        tags.attributes.put('Name','TAGS');
        //ad.Search_Tags__c
        tags.nodeValue = JobBoardUtils.removeAmpersand(JobBoardUtils.blankValue(ad.Search_Tags__c));
        listing.childNodes.add(tags);
        
        /*
        XMLDom.Element location = new XMLDom.Element('CLASSIFICATION');
        location.attributes.put('Name','LOCATION');
        location.nodeValue = ad.Location__c;
        listing.childNodes.add(location);
        */
        /*
        XMLDom.Element area = new XMLDom.Element('CLASSIFICATION');
        area.attributes.put('Name','AREA');
        area.nodeValue = JobBoardUtils.removeAmpersand(ad.SearchArea_Label_String__c);
        listing.childNodes.add(area);
        */
        //SALARY 
        XMLDom.Element salary = new XMLDom.Element('SALARY');
        salary.attributes.put('type',ad.Salary_Type__c);
		salary.attributes.put('amount','');
		salary.attributes.put('AdditionalText',JobBoardUtils.removeAmpersand((JobBoardUtils.blankValue(ad.Salary_Description__c))));
		salary.attributes.put('min',ad.SeekSalaryMin__c);
		salary.attributes.put('max',ad.SeekSalaryMax__c);
		salary.attributes.put('hideSalary',transferBooleanToNumber(ad.No_salary_information__c)); 
		job.childNodes.add(salary);
        
        XMLDom.Element applicationmethod = new XMLDom.Element('APPLICATIONMETHOD');
        applicationmethod.attributes.put('type','URL');
        String orgId = UserInfo.getOrganizationId();
        applicationmethod.nodeValue= JobBoardUtils.removeAmpersand(Endpoint.getcpewsEndpoint()+'/peoplecloud/GetApplicationForm?jobRefCode='+orgId.substring(0, 15)+':'+ad.id+'&website=jxt_nz');
        job.childNodes.add(applicationmethod);
        
        XMLDom.Element hotjob = new XMLDom.Element('HOTJOB');
        if(ad.Hot_Job__c){
            hotjob.nodeValue = 'TRUE';
        }else{
            hotjob.nodeValue = 'FALSE';
        }
        job.childNodes.add(hotjob);
 
		XmlDom.Element referral = new XmlDom.Element('REFERRAL');
		
		XmlDom.Element hasreferralfee = new XmlDom.Element('HASREFERRALFEE');
		hasreferralfee.nodeValue=JobBoardUtils.booleanValue(ad.Has_Referral_Fee__c);
		referral.appendChild(hasreferralfee);
		
		XmlDom.Element referralfee = new XmlDom.Element('REFERRALFEE');
		referralfee.nodeValue = JobBoardUtils.blankValue(String.valueOf(ad.Referral_Fee__c));
		referral.appendChild(referralfee);
		
		XmlDom.Element referralurl = new XmlDom.Element('REFERRALURL');
		if('http://'==ad.Terms_And_Conditions_Url__c || ad.Terms_And_Conditions_Url__c==null || ad.Terms_And_Conditions_Url__c==''){
			ad.Terms_And_Conditions_Url__c = '';
		}
		referralurl.nodeValue = JobBoardUtils.wrappedWithCDATA(ad.Terms_And_Conditions_Url__c);
		referral.appendChild(referralurl);
		job.childNodes.add(referral);
        
        XMLDom.Element bulletpoints = new XMLDom.Element('BULLETPOINTS');
        XMLDom.Element bullet1 = new XMLDom.Element('BULLET1');
        bullet1.nodeValue = JobBoardUtils.wrappedWithCDATA(JobBoardUtils.blankValue(ad.Bullet_1__c));
        bulletpoints.childNodes.add(bullet1);
        
        XMLDom.Element bullet2 = new XMLDom.Element('BULLET2');
        bullet2.nodeValue = JobBoardUtils.wrappedWithCDATA(JobBoardUtils.blankValue(ad.Bullet_2__c));
        bulletpoints.childNodes.add(bullet2); 
        
        XMLDom.Element bullet3 = new XMLDom.Element('BULLET3');
        bullet3.nodeValue = JobBoardUtils.wrappedWithCDATA(JobBoardUtils.blankValue(ad.Bullet_3__c));
        bulletpoints.childNodes.add(bullet3);
        
        job.childNodes.add(bulletpoints);
		return job.toXmlString();
	}
	
	public static String transferBooleanToNumber(Boolean b){
		if(b){
			return '1';
		}else{
			return '0';
		}
	}
	public static List<SelectOption> getWriteLabels(){
		CommonSelector.checkRead(StyleCategory__c.sObjectType,'White_labels__c');
		StyleCategory__c sc = [select White_labels__c from StyleCategory__c where WebSite__c='JXT_NZ' and White_labels__c!=null limit 1];
	 	 List<SelectOption> options = new List<SelectOption>();
	 	 if(sc!=null){
	 	 	String[] writeLables = sc.White_labels__c.split(';');
	 	 	for(String writeLabel : writeLables){
	 	 		options.add(new SelectOption(writeLabel,writeLabel));
	 	 	}
	 	 	return options;
	 	 }else{
	 	 	return null;
	 	 }
	}
	
	public static List<SelectOption> getReferralItems(){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('FALSE','This job has no referral fee'));
		options.add(new SelectOption('TRUE','Include a referral fee to increase your chances of getting the right candidate'));
		return options; 
	}
	
	public static List<SelectOption> getClassifications(){
		Document doc = DaoDocument.getCTCConfigFileByDevName('jobx_classifications');
		XmlDom dom = new XmlDom(doc.body.toString());
		List<XmlDom.Element> elementList = dom.getElementsByTagName('classification');
		
		List<SelectOption> options = new List<SelectOption>();
		for(XmlDom.Element e:elementList){
			if(e.getAttribute('display')=='true'){
				options.add(new SelectOption(e.nodeValue, e.nodeValue));
			}
		}
		return options; 
	}
	
	public static List<SelectOption> get2ndClassifications(){
		Document doc = DaoDocument.getCTCConfigFileByDevName('jobx_classifications');
		XmlDom dom = new XmlDom(doc.body.toString());
		List<XmlDom.Element> elementList = dom.getElementsByTagName('classification');
		
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('0','--none--'));
		for(XmlDom.Element e:elementList){
			if(e.getAttribute('display')=='true'){
				options.add(new SelectOption(e.nodeValue, e.nodeValue));
			}
		}
		return options; 
	}
	
}