/**
 * @author Raymond
 * 
 * RemoteAPIDescriptor is designed for capturing invocation information of RemoteActions
 * defined in one Apex extension or controller.
 * 
 * Since SF doesn't provide method to extract meta info(like param type) of RemoteActions, it's hard for
 * frontend to know valid param types in a remote action. RemoteAPIDescriptor is to capture 
 * those information so that it can be serialized as JSON and return to frontend.
 * */
public class RemoteAPIDescriptor {
    public class RemoteActionParam{
        public String paramType;
        public String paramName;
        public String paramDescription;
        public RemoteActionParam(String type, String name, String description){
            paramType = type;
            paramName = name;
            paramDescription = description;
        }
    }
    
    
    public class RemoteAction{
        public String actionName;
        // 1st version param list which only store type of param
        public List<String> paramTypes;
        // 2st version param list which provide more info of input param
        public List<RemoteActionParam> params;
        public Boolean active;

        
        
        public RemoteAction(){
            paramTypes = new List<String>();
            params = new List<RemoteActionParam>();
            active = true;
        }
        public RemoteAction(String name){
            this();
            actionName = name;
        }
        
        public RemoteAction param(Type paramType){
            paramTypes.add(paramType.getName());
            params.add(new RemoteActionParam(paramType.getName(), '', ''));
            return this;
        }

        public RemoteAction param(Type paramType, String paramName, String paramDescription){
            params.add(new RemoteActionParam(paramType.getName(), paramName, paramDescription));
            return this;
        }

        public RemoteAction param(Type paramType, String paramDescription){
            params.add(new RemoteActionParam(paramType.getName(), '', paramDescription));
            return this;
        }
        
        public RemoteAction active(Boolean a) {
            active = a;
            return this;
        }        
    }
    
    public Map<String, RemoteAction> paramTypesMap;
    public String description;
    
    public RemoteAPIDescriptor(){
        paramTypesMap = new Map<String, RemoteAction>();
    }
    
    public RemoteAction action(String name){
        if (paramTypesMap.get(name) == null) {
            paramTypesMap.put(name, new RemoteAction(name));
        }
        return paramTypesMap.get(name);
    }
    
    public RemoteAPIDescriptor description(String d){
        this.description = d;
        return this;
    }
}