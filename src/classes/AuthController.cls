/*********
CPE-1084 *Fix CSRF issues, add checking in constructor
         *completeAuthorization() method is not auto-run anymore, linked with a button 

 CPE-1082: Add cancelAuthorize() function to fix CSRF issues, 13/09/2017, added by Zoe
*********/

public with sharing class AuthController {

    public String service { get; set; }
    
    public String authUrl { get; set; }
    
    public boolean authSuccess { get; set; }

    public String message { get; set; }
     
    public string searchUrl { get; set; }
    
    public String returnURL {get;set;}
    
    public Boolean redirectToLightning {get;set;}

    private String token;
    
    public AuthController(){
        
       
        authSuccess = false;
        //CPE-1084, Added by Ralph - To fix CSRF
        token = ApexPages.currentPage().getParameters().get('oauth_token');
        if(String.isNotBlank(token)){
            authSuccess = true;
        }
        
        returnURL = ApexPages.currentPage().getParameters().get('returl');
        
        
        if((returnURL.contains('isdtp=p1') &&  ApexPages.currentPage().getParameters().get('isdtp') == null)){
            redirectToLightning = true;
           
           
        }else{
            redirectToLightning = false;
        }
         
    }

    public String SearchInfo {//get the Contact ID
        get{
             searchUrl = ApexPages.currentPage().getParameters().get('returl');
            
             return searchUrl;
             }
        set;
    }
    
    public PageReference authorize() {
        
        OAuth oa = new OAuth(); 
        authUrl = oa.newAuthorization(SearchInfo,'linkedin');//Oauth function with the ContactID
        searchUrl = ApexPages.currentPage().getParameters().get('returl');        
        if(authUrl==null) {
            this.message = oa.message;
            return null;
        } else {
            PageReference newAuthPage = new PageReference(authUrl);
            newAuthPage.setRedirect(true);
            if(searchUrl != null && searchUrl != '')
            	searchUrl = EncodingUtil.urlEncode(searchUrl,'UTF-8');
        
            newAuthPage.getParameters().put('returl',searchUrl); 
            return newAuthPage; 
        }
    }

    
    /* 
     * Redirect the user to the previous page (advertisement detail page)
     * 
     * CPE-1082,13/09/2017,added by Zoe
     * 
     */ 
    public PageReference cancelAuthorize() {
        String returnURL = ApexPages.currentPage().getParameters().get('returl');
        return new PageReference(returnURL);
    }
    
    public PageReference completeAuthorization() {
        //String token = ApexPages.currentPage().getParameters().get('oauth_token');
        //System.debug('token in complete:'+token);
        searchUrl = ApexPages.currentPage().getParameters().get('returl');
        if(searchUrl!=null && searchUrl!= ''){
            if(searchUrl.toLowerCase().startsWith('http')){
                
                String curhost = ApexPages.currentPage().getHeaders().get('Host').toLowerCase();
                
               
                if(searchUrl.toLowerCase().startsWith('https://' + curhost)){
                   
                }else{
                    message = 'It was refused to redirect to another website !' ;
                    return null;
                }
                
            }
            else {
                
                if(searchUrl.startsWith('/')){
                    searchUrl = searchUrl.replaceFirst('/+','');
                }    
                
                searchUrl = '/' +  searchUrl;//EncodingUtil.urlEncode(searchUrl,'UTF-8'); 
               
                
            }
	        if(authSuccess)
	        {
	            String verifier = ApexPages.currentPage().getParameters().get('oauth_verifier');
	            OAuth oa = new OAuth();
	            authSuccess = oa.completeAuthorization(token,verifier);
	            this.message = oa.message;

	        }else{
	            message = 'Invalid request. Missing parameter oauth_token';
	           	
	        }
            return new PageReference(searchUrl);
        }else{
        	return null;
        }
    }
    
}