/**
 * This is the test class for CandidateSelector
 * Created by: Lina
 * Created on: 27/06/2017
 */

@isTest
private class CandidateSelectorTest {

    static CandidateSelector selector;

    static {
        TriggerHelper.disableAllTrigger();
        selector = new CandidateSelector();
    }

    static testMethod void testSelectContactWithBasicFieldsById() {
        System.runAs(DummyRecordCreator.platformUser) {
            Contact con = DataTestFactory.createSingleContact();
            Contact conSelected = selector.selectContactWithBasicFieldsById(con.Id);
            System.assert(conSelected != null);
        }
    }
}