/**
 * This is the dummy record creator for Advertisement_Configuration_Detail__c object
 * This class is used for testing purpose
 *
 * Created by: Lina Wei
 * Created on: 24/03/2017
 *
 */

@isTest
public with sharing class AdConfigDetailDummyRecordCreator {

    public static Advertisement_Configuration_Detail__c createActiveTemplate(Advertisement_Configuration__c ac) {
        Advertisement_Configuration_Detail__c acd = new Advertisement_Configuration_Detail__c();
        acd.Name = 'Test Template 1';
        acd.recordTypeId = DaoRecordType.adConfigTemplateRT.Id;
        acd.Advertisement_Configuration__c = ac.Id;
        acd.Active__c = true;
        acd.ID_API_Name__c = '12345';
        insert acd;
        return acd;
    }

    public static Advertisement_Configuration_Detail__c createActiveTemplateWithoutInsert(Advertisement_Configuration__c ac) {
        Advertisement_Configuration_Detail__c acd = new Advertisement_Configuration_Detail__c();
        acd.Name = 'Test Template 1';
        acd.recordTypeId = DaoRecordType.adConfigTemplateRT.Id;
        acd.Advertisement_Configuration__c = ac.Id;
        acd.Active__c = true;
        acd.ID_API_Name__c = '12345';
        return acd;
    }

    public static Advertisement_Configuration_Detail__c createInactiveTemplate(Advertisement_Configuration__c ac) {
        Advertisement_Configuration_Detail__c acd = new Advertisement_Configuration_Detail__c();
        acd.Name = 'Test Template 2';
        acd.recordTypeId = DaoRecordType.adConfigTemplateRT.Id;
        acd.Advertisement_Configuration__c = ac.Id;
        acd.Active__c = false;
        acd.ID_API_Name__c = '12345';
        insert acd;
        return acd;
    }

    public static Advertisement_Configuration_Detail__c createActiveLogo(Advertisement_Configuration__c ac) {
        Advertisement_Configuration_Detail__c acd = new Advertisement_Configuration_Detail__c();
        acd.Name = 'Test Logo 1';
        acd.recordTypeId = DaoRecordType.adConfigLogoRT.Id;
        acd.Advertisement_Configuration__c = ac.Id;
        acd.Active__c = true;
        acd.ID_API_Name__c = '12345';
        insert acd;
        return acd;
    }

    public static Advertisement_Configuration_Detail__c createActiveLogoWithoutInsert(Advertisement_Configuration__c ac) {
        Advertisement_Configuration_Detail__c acd = new Advertisement_Configuration_Detail__c();
        acd.Name = 'Test Logo 1';
        acd.recordTypeId = DaoRecordType.adConfigLogoRT.Id;
        acd.Advertisement_Configuration__c = ac.Id;
        acd.Active__c = true;
        acd.ID_API_Name__c = '12345';
        return acd;
    }

    public static Advertisement_Configuration_Detail__c createInactiveLogo(Advertisement_Configuration__c ac) {
        Advertisement_Configuration_Detail__c acd = new Advertisement_Configuration_Detail__c();
        acd.Name = 'Test Logo 2';
        acd.recordTypeId = DaoRecordType.adConfigLogoRT.Id;
        acd.Advertisement_Configuration__c = ac.Id;
        acd.Active__c = false;
        acd.ID_API_Name__c = '12345';
        insert acd;
        return acd;
    }
}