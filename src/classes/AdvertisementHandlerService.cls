/**
 * Created by linaw on 28/03/2017.
 */

public with sharing class AdvertisementHandlerService {
    public static String seekRecordTypeId;
    public static String jxtRecordTypeId;
    public static Map<Id, JobBoardAndRequests> jobBoardAndRequestsMap;

    static {
        seekRecordTypeId = DaoRecordType.seekAdRT.Id;
        jxtRecordTypeId = DaoRecordType.JXTAdRT.Id;
    }

    public static void archiveAds(List<Advertisement__c> adList) {
        jobBoardAndRequestsMap = new Map<Id, JobBoardAndRequests>();
        for (Advertisement__c ad : adList) {
            // later when jxt also change to use generic controller
            // can remove this if statement
            if (ad.RecordTypeId == jxtRecordTypeId) {
                JXTPostingCallWebService.callJxtFeedWebService(ad, 'Archive');
            } else {
                JobBoardAndRequests jobBoardRequests = jobBoardAndRequestsMap.get(ad.RecordTypeId);
                if (jobBoardRequests == null) {
                    jobBoardRequests = new JobBoardAndRequests(ad.RecordTypeId);
                    jobBoardRequests.jobBoard.actionType = 'archive';
                    jobBoardAndRequestsMap.put(ad.RecordTypeId, jobBoardRequests);
                }
                String requestBody = jobBoardRequests.jobBoard.generateRequestBody(ad);
                jobBoardRequests.requestBodyList.add(requestBody);
            }
        }
        // archive seek ad
        JobBoardAndRequests jobBoardRequests = jobBoardAndRequestsMap.get(seekRecordTypeId);
        if (jobBoardRequests != null) {
            jobBoardRequests.jobBoard.callWebService(jobBoardRequests.requestBodyList);
        }
    }

    /**
     * This method is used to reduce usage when the job posting is failed.
     * Currently only implement for seek
     * @param failedAds   List of Advertisement that post failed
     */
    public static void updateUserUsage(List<Advertisement__c> failedAds) {
        Set<String> userIdSet = new Set<String>();
        Map<String, Integer> userAndNumOfSeekAdsFail = new Map<String, Integer>();

        for (Advertisement__c ad : failedAds) {
            if (ad.RecordTypeId.equals(seekRecordTypeId)) {
                String lastModifiedUser = ad.LastModifiedById;
                userIdSet.add(lastModifiedUser);
                if (userAndNumOfSeekAdsFail.get(lastModifiedUser) != null ) {
                    Integer tmp = userAndNumOfSeekAdsFail.get(lastModifiedUser);
                    userAndNumOfSeekAdsFail.put(lastModifiedUser, tmp + 1);
                } else {
                    userAndNumOfSeekAdsFail.put(lastModifiedUser, 1);
                }
            }
            // need to implement for other jobboard later
        }

        List<User> users = new UserSelector().getUserList(userIdSet);

        for (User u : users) {
            Integer usage = String.isEmpty(u.Seek_Usage__c) ? 0 : Integer.valueOf(u.Seek_Usage__c);
            Integer failedNum = userAndNumOfSeekAdsFail.get(u.Id);
            if (usage >= failedNum) {
                u.Seek_Usage__c = String.valueOf(usage - failedNum);
            } else {
                u.Seek_Usage__c = String.valueOf(0);
            }
        }

        fflib_SecurityUtils.checkFieldIsUpdateable(User.SObjectType, User.Seek_Usage__c);
        update users;
    }


    public class JobBoardAndRequests {
        public JobBoard jobBoard;
        public List<String> requestBodyList;
        public JobBoardAndRequests(String recordTypeId) {
            jobBoard = JobBoardFactory.getJobBoard(recordTypeId);
            requestBodyList = new List<String>();
        }  
    }
}