@isTest
private class WebDocumentTriggerHandlerTest
{


    public static void init() {
    	
        PCAppSwitch__c pcSwitch = PCAppSwitch__c.getInstance();
        pcSwitch.Disable_Daxtra_SQS_Request__c = false;
        upsert pcSwitch;
        SQSCredential__c sqsc = new SQSCredential__c(Name='Daxtra Feed SQS');
        sqsc.URL__c = 'https://sqs.ap-southeast-2.amazonaws.com/729402844354/CandidateFeedRequest';
        sqsc.Access_Key__c = '123';
        sqsc.Secret_Key__c = '123';
        upsert sqsc;
    	
    }
    
	static testMethod void testAfterInsertWebDoc(){
		System.runAs(DummyRecordCreator.platformUser) {
		S3Utils.testFlag = true;
		Web_Document__c webdocument = new Web_Document__c(name='web001', Type__c='Resume');
		Contact con = new Contact(LastName='name001',email='aabb@aabb.com');
		insert webdocument;
        
        System.debug(webdocument.Id);
        
		webdocument.name = 'test001';
		update webdocument;
        
        Web_Document__c test = [select name from Web_Document__c where Id =: webdocument.Id];
        
        System.assertEquals('test001', test.name);
        
		delete webdocument;
        
        System.assertEquals(0,[select count() from Web_Document__c where Id =: webdocument.Id]);
		
		Web_Document__c webdocument2 = new Web_Document__c(name='web002', Type__c='Resume');
		insert con;
        
        System.debug(con.id);
        
		webdocument2.Document_Related_To__c = con.id;
		insert webdocument2;
        
        System.debug(webdocument2.id);
        
		webdocument2.name='test002';
		update webdocument2;	
        
        test = [select name from Web_Document__c where Id =: webdocument2.Id];
        
        System.assertEquals(webdocument2.name, test.Name);
        
		delete webdocument2;
        
		System.assertEquals(0,[select count() from Web_Document__c where Id =: webdocument2.Id]);        
		
		Web_Document__c[] webdocuments = new List<Web_Document__c>();
		
		webdocuments.add(new Web_Document__c(name='web003', Type__c='Resume'));
		webdocuments.add(new Web_Document__c(name='web004', Type__c='Resume'));
		insert webdocuments;
     
   		System.debug(webdocuments[0].id);
        System.debug(webdocuments[1].id);
        
		webdocuments[0].name='web004';
		webdocuments[1].name = 'web003';
		update webdocuments;
        
        test= [select name from Web_Document__c where Id =: webdocuments[0].Id];
        System.assertEquals(webdocuments[0].name,test.name);
        
        test= [select name from Web_Document__c where Id =: webdocuments[1].Id];
        System.assertEquals(webdocuments[1].name,test.name);
        
		delete webdocuments;
     
        System.assertEquals(0,[select count() from Web_Document__c where Id =: webdocuments[0].Id]); 
        System.assertEquals(0,[select count() from Web_Document__c where Id =: webdocuments[1].Id]);
		}
	}
	
	static testMethod void testInsertResumeWhenCandHasNoResumeForDaxtra() {
		System.runAs(DummyRecordCreator.platformUser) {
	    init();
	    // Create Data
	    Contact con = new Contact(LastName='test',email='test@test.com');
	    insert con;
	    Web_Document__c doc = new Web_Document__c(name='web001', Document_Type__c='Resume', Document_Related_To__c=con.Id, ObjectKey__c='test001', S3_Folder__c='test0001');
		insert doc;
		system.debug('test record = ' + doc.Id);
	    PersonFeed feed = new PersonFeed();
		for (PersonFeed f : PersonFeedListCreater.PERSONFEEDLIST) {
		    feed = f;
        }
        System.assertEquals(1, PersonFeedListCreater.PERSONFEEDLIST.size());
        System.assertEquals(con.Id, feed.candidateId);
        System.assertEquals('Add', feed.action);
        System.assertEquals('test', feed.lastName);
        System.assertEquals('test001', feed.resumeName);
        System.assertEquals('test0001', feed.resumeBucket);
		}
	}
	
	static testMethod void testInsertResumeWhenCandHasResumeForDaxtra() {
		System.runAs(DummyRecordCreator.platformUser) {
	    init();
	    // Create Data
	    Contact con = new Contact(LastName='test',email='test@test.com');
	    insert con;
	    TriggerHelper.DisableTriggerOnWebDocument = true;
	    Web_Document__c doc1 = new Web_Document__c(name='web001', Document_Type__c='Resume', 
	                                               Document_Related_To__c=con.Id, ObjectKey__c='test001', 
	                                               S3_Folder__c='test0001');
		insert doc1;
		TriggerHelper.DisableTriggerOnWebDocument = false;
	    Web_Document__c doc2 = new Web_Document__c(name='web002', Document_Type__c='Resume', 
	                                               Document_Related_To__c=con.Id, ObjectKey__c='test002', 
	                                               S3_Folder__c='test0001');
		insert doc2;
		PersonFeed feed = new PersonFeed();
		for (PersonFeed f : PersonFeedListCreater.PERSONFEEDLIST) {
		    feed = f;
        }
        System.assertEquals(1, PersonFeedListCreater.PERSONFEEDLIST.size());
        System.assertEquals(con.Id, feed.candidateId);
        System.assertEquals('Update', feed.action);
        System.assertEquals('test', feed.lastName);
        System.assertEquals('test002', feed.resumeName);
        System.assertEquals('test0001', feed.resumeBucket);
		}
	}

	static testMethod void testUpdateResumeWhenCandHasNoResumeForDaxtra() {
	    init();
	    // Create Data
	    Contact con1 = new Contact(LastName='test1',email='test1@test.com');
	    Contact con2 = new Contact(LastName='test2',email='test2@test.com');
	    insert con1;
	    insert con2;
	    TriggerHelper.DisableTriggerOnWebDocument = true;
	    Web_Document__c doc = new Web_Document__c(name='web001', Document_Type__c='Resume', 
	                                              Document_Related_To__c=con1.Id, ObjectKey__c='test001', 
	                                              S3_Folder__c='test0001');
		insert doc;
		TriggerHelper.DisableTriggerOnWebDocument = false;
	    doc.Document_Related_To__c = con2.Id;
		update doc;
		PersonFeed feed = new PersonFeed();
		for (PersonFeed f : PersonFeedListCreater.PERSONFEEDLIST) {
		    feed = f;
        }
        System.assertEquals(1, PersonFeedListCreater.PERSONFEEDLIST.size());
        System.assertEquals(con2.Id, feed.candidateId);
        System.assertEquals('Add', feed.action);
        System.assertEquals('test2', feed.lastName);
        System.assertEquals('test001', feed.resumeName);
        System.assertEquals('test0001', feed.resumeBucket);
	}
	
	static testMethod void testUpdateResumeWhenCandHasResumeForDaxtra() {
		System.runAs(DummyRecordCreator.platformUser) {
	    init();
	    // Create Data
	    Contact con1 = new Contact(LastName='test1',email='test1@test.com');
	    Contact con2 = new Contact(LastName='test2',email='test2@test.com');
	    insert con1;
	    insert con2;
	    TriggerHelper.DisableTriggerOnWebDocument = true;
	    Web_Document__c doc1 = new Web_Document__c(name='web001', Document_Type__c='Resume', 
	                                               Document_Related_To__c=con1.Id, ObjectKey__c='test001', 
	                                               S3_Folder__c='test0001');
	    insert doc1;                                   
	    Web_Document__c doc2 = new Web_Document__c(name='web002', Document_Type__c='Resume', 
	                                               Document_Related_To__c=con2.Id, ObjectKey__c='test002', 
	                                               S3_Folder__c='test0001');
		insert doc2;                                           
		TriggerHelper.DisableTriggerOnWebDocument = false;
	    doc2.Document_Related_To__c = con1.Id;
		update doc2;
		PersonFeed feed = new PersonFeed();
		for (PersonFeed f : PersonFeedListCreater.PERSONFEEDLIST) {
            feed = f;
        }
        System.assertEquals(1, PersonFeedListCreater.PERSONFEEDLIST.size());
        System.assertEquals(con1.Id, feed.candidateId);
        System.assertEquals('Update', feed.action);
        System.assertEquals('test1', feed.lastName);
        System.assertEquals('test002', feed.resumeName);
        System.assertEquals('test0001', feed.resumeBucket);     
		}   
	}
	
	static testMethod void testDeleteResumeWhenCandHasNoResumeForDaxtra() {
		System.runAs(DummyRecordCreator.platformUser) {
	    init();
	    // Create Data
	    Contact con = new Contact(LastName='test',email='test@test.com');
	    insert con;
	    TriggerHelper.DisableTriggerOnWebDocument = true;
	    Web_Document__c doc = new Web_Document__c(name='web001', Document_Type__c='Resume', 
	                                               Document_Related_To__c=con.Id, ObjectKey__c='test001', 
	                                               S3_Folder__c='test0001');
		insert doc;
		TriggerHelper.DisableTriggerOnWebDocument = false;
		delete doc;
		PersonFeed feed = new PersonFeed();
		for (PersonFeed f : PersonFeedListCreater.PERSONFEEDLIST) {
		    feed = f;
        }
        System.assertEquals(1, PersonFeedListCreater.PERSONFEEDLIST.size());
        System.assertEquals(con.Id, feed.candidateId);
        System.assertEquals('Delete', feed.action);
        System.assertEquals(null, feed.lastName);
        System.assertEquals(null, feed.resumeName);
        System.assertEquals(null, feed.resumeBucket);
		}
	}	

	static testMethod void testDeleteResumeWhenCandHasResumeForDaxtra() {
		System.runAs(DummyRecordCreator.platformUser) {
	    init();
	    // Create Data
	    Contact con = new Contact(LastName='test1',email='test1@test.com');
	    insert con;
	    TriggerHelper.DisableTriggerOnWebDocument = true;
	    Web_Document__c doc1 = new Web_Document__c(name='web001', Document_Type__c='Resume', 
	                                               Document_Related_To__c=con.Id, ObjectKey__c='test001', 
	                                               S3_Folder__c='test0001');
	    insert doc1;                                   
	    Web_Document__c doc2 = new Web_Document__c(name='web002', Document_Type__c='Resume', 
	                                               Document_Related_To__c=con.Id, ObjectKey__c='test002', 
	                                               S3_Folder__c='test0001');
		insert doc2;
		
		// Action 
		TriggerHelper.DisableTriggerOnWebDocument = false;
        delete doc2;
		PersonFeed feed = new PersonFeed();
		for (PersonFeed f : PersonFeedListCreater.PERSONFEEDLIST) {
            feed = f;
        }
        
        // Verify
        System.assertEquals(1, PersonFeedListCreater.PERSONFEEDLIST.size());
        System.assertEquals(con.Id, feed.candidateId);
        System.assertEquals('Update', feed.action);
        System.assertEquals('test1', feed.lastName);
        System.assertEquals('test001', feed.resumeName);
        System.assertEquals('test0001', feed.resumeBucket);       
		} 
	}
	
	static testMethod void testMultipleInsertOnOneCandidate() {
		System.runAs(DummyRecordCreator.platformUser) {
	    init();
        // Create data
        Contact con1 = new Contact(LastName='test1',email='test1@test.com');
        insert con1;
        // insert three resume for con1
        Web_Document__c doc1 = new Web_Document__c(name='doc1', Document_Related_To__c=con1.Id, Document_Type__c='Resume');
        Web_Document__c doc2 = new Web_Document__c(name='doc2', Document_Related_To__c=con1.Id, Document_Type__c='Resume');
        Web_Document__c doc3 = new Web_Document__c(name='doc3', Document_Related_To__c=con1.Id, Document_Type__c='Resume');
        List<Web_Document__c> docList = new List<Web_Document__c>{doc1, doc2, doc3};
        insert docList;
        
        PersonFeed feed = new PersonFeed();
		for (PersonFeed f : PersonFeedListCreater.PERSONFEEDLIST) {
            feed = f;
        }
        
        // Verify: this action should only generate one Add request
        System.assertEquals(1, PersonFeedListCreater.PERSONFEEDLIST.size());
        System.assertEquals('Add', feed.action);
		}
	}
	
	static testMethod void testMultipleUpdateOnOneCandidate() {
		System.runAs(DummyRecordCreator.platformUser) {
	    init();
	    TriggerHelper.DisableTriggerOnWebDocument = true;
	    
        // Create data
        Contact con1 = new Contact(LastName='test1',email='test1@test.com');
        Contact con2 = new Contact(LastName='test2',email='test1@test.com');
        Contact con3 = new Contact(LastName='test3',email='test1@test.com');
        insert con1;
        insert con2;
        insert con3;
        // insert three resume for con1
        Web_Document__c doc2 = new Web_Document__c(name='doc2', Document_Related_To__c=con1.Id, Document_Type__c='Resume', ObjectKey__c='test2');
        Web_Document__c doc3 = new Web_Document__c(name='doc3', Document_Related_To__c=con1.Id, Document_Type__c='Resume', ObjectKey__c='test3');
        Web_Document__c doc4 = new Web_Document__c(name='doc4', Document_Related_To__c=con1.Id, Document_Type__c='Resume', ObjectKey__c='test4');
        Web_Document__c doc31 = new Web_Document__c(name='doc31', Document_Related_To__c=con3.Id, Document_Type__c='Resume', ObjectKey__c='test31');
        List<Web_Document__c> docList = new List<Web_Document__c>{doc2, doc3, doc4, doc31};
        insert docList;
        // insert 1 resume for con1 with later LastModifiedDate
        sleep(1500);
        Web_Document__c doc5 = new Web_Document__c(name='doc5', Document_Related_To__c=con1.Id, Document_Type__c='Resume', ObjectKey__c='test5');
        insert doc5;
        sleep(1500);
        Web_Document__c doc1 = new Web_Document__c(name='doc1', Document_Related_To__c=con1.Id, Document_Type__c='Resume', ObjectKey__c='test1');
        insert doc1;
        
        // update 3 resume to con2, 2 resume to con3
        TriggerHelper.DisableTriggerOnWebDocument = false;
        doc1.Document_Related_To__c = con2.Id;
        doc2.Document_Related_To__c = con2.Id;
        doc3.Document_Related_To__c = con2.Id;
        doc4.Document_Related_To__c = con3.Id;
        doc5.Document_Related_To__c = con3.Id;
        List<Web_Document__c> docUpdateList = new List<Web_Document__c>{doc1, doc2, doc3, doc4, doc5};
        update docUpdateList;
        
        // Verify: this action should generate two request, 
        // one Add doc1 for con2
        // one Update doc5 for con3
        System.assertEquals(2, PersonFeedListCreater.PERSONFEEDLIST.size());
        for (PersonFeed feed : PersonFeedListCreater.PERSONFEEDLIST) {
            if (feed.candidateId == con2.Id) {
                System.assertEquals('Add', feed.action);
                System.assertEquals('test1', feed.resumeName);
            } else {
                System.assertEquals('Update', feed.action);
                System.assertEquals('test3', feed.LastName);
                System.assertEquals('test5', feed.resumeName);
            }
        }    
		}    
	}
	
	static testMethod void testMultipleDeleteOnOneCandidate() {
		System.runAs(DummyRecordCreator.platformUser) {
	    init();
	    TriggerHelper.DisableTriggerOnWebDocument = true;
	    
        // Create data
        Contact con1 = new Contact(LastName='test1',email='test1@test.com');
        Contact con2 = new Contact(LastName='test2',email='test1@test.com');
        Contact con3 = new Contact(LastName='test3',email='test1@test.com');
        insert con1;
        insert con2;
        insert con3;
        // insert three resume for con1
        Web_Document__c doc11 = new Web_Document__c(name='doc11', Document_Related_To__c=con1.Id, Document_Type__c='Resume');
        Web_Document__c doc21 = new Web_Document__c(name='doc21', Document_Related_To__c=con2.Id, Document_Type__c='Resume');
        Web_Document__c doc22 = new Web_Document__c(name='doc22', Document_Related_To__c=con2.Id, Document_Type__c='Resume');
        Web_Document__c doc31 = new Web_Document__c(name='doc31', Document_Related_To__c=con3.Id, Document_Type__c='Resume');
        Web_Document__c doc32 = new Web_Document__c(name='doc32', Document_Related_To__c=con3.Id, Document_Type__c='Resume');
        Web_Document__c doc33 = new Web_Document__c(name='doc33', Document_Related_To__c=con3.Id, Document_Type__c='Resume');
        Web_Document__c doc34 = new Web_Document__c(name='doc34', Document_Related_To__c=con3.Id, Document_Type__c='Resume', ObjectKey__c='test34');
        List<Web_Document__c> docList = new List<Web_Document__c>{doc11, doc21, doc22, doc31, doc32, doc33};
        insert docList;
        sleep(1500);
        insert doc34;
        
        // delete con1, 2 resume for con2, and 2 resume for con3
        TriggerHelper.DisableTriggerOnWebDocument = false;
        delete con1; 
        List<Web_Document__c> docDeleteList = new List<Web_Document__c>{doc21, doc22, doc31, doc32};
        delete docDeleteList;
        
        // Verify: this action should generate two request 
        // one Delete for con2 as there is no resume left
        // one Update on doc34(most recent last modified date) for con3 
        System.assertEquals(2, PersonFeedListCreater.PERSONFEEDLIST.size());
        for (PersonFeed feed : PersonFeedListCreater.PERSONFEEDLIST) {
            if (feed.candidateId == con2.Id) {
                System.assertEquals('Delete', feed.action);
            } else {
                System.assertEquals('Update', feed.action);
                System.assertEquals('test3', feed.LastName);
                System.assertEquals('test34', feed.resumeName);
            }
        }
		}
	}

    // Allow thread to sleep for 1s 
    // @param sleepTime = 1000
    public static void sleep(Integer sleepTime) {
        Long startTime = DateTime.now().getTime();
        Long finishTime = DateTime.now().getTime();
        while ((finishTime - startTime) < sleepTime) {
            //sleep for 9s
            finishTime = DateTime.now().getTime();
        }
    }	
}