/**
* CandidateMaker is a implementation of IEmailHandler interface, which can parse candidate resume  
* from one email, record the activities of each email.
* Each CandidateMaker MUST have a reference of ICVEmailParser to execute parsing email content.
*/

public with sharing class CandidateMaker implements ICVEmailHandler{
	
	private static final String ACCOUNT_ADMIN_NAME = 'ctcpeopleAdmin(Do not Delete)';
	
	private static final String TASK_STATUS='Completed';
	private static final String TASK_PRIORITY = 'Normal';
	
	//parser for enquiry email
	private ICVEmailParser parser;
	
	private Messaging.InboundEmail email;
	
	public CandidateMaker() { 
		
	}
	
	
	public CandidateMaker(ICVEmailParser parser) {
		this();
		this.parser = parser;
	}
	
	public void setEmail(Messaging.InboundEmail srcEmail){
		this.email = srcEmail;	
	}
	
	public Messaging.InboundEmail getEmail(){
		return this.email;
	}
	
	//execute handle processing
	public void handle(){
		if(this.parser != null){
			
			//invoke make method of this object
			this.make();
		}
	}
	
	public void make() {
		
		//parse candidate email to CandidateContent object for next processing
		CVContent content = parser.parse();
		
		//create a activity/task related to ACCOUNT_ADMIN_NAME
		Task t = recordTask( getEmail() );
		if(t != null ){
			//create an attachment related to current activity/task;
			Attachment a = makeAttachment(t, content);
			if(a != null ){
				//call webservice to parse resume
				try{
					callWebService(t, a );
				}catch(Exception e){
					ActivityDocOperator.updateActivityStatus('Error','Failed to call web service', t.id);
				}
			}else{
				ActivityDocOperator.updateActivityStatus('Error','Failed to create attachment ['+content.getCvFilename()+']', t.id);
           
			}	
		}	
	}
	
	public Attachment makeAttachment(Task t, CVContent content ){
		
		Attachment a = new Attachment();
		
		//a.Name = formatFilename( content.getCvFilename() );
		a.Name =  content.getCvFilename();
		a.Body = content.getContent();
		a.ParentId = t.Id;
		
	    try{
	    	//check FLS
	    	List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
	    		Attachment.Name,
	    		Attachment.Body,
	    		Attachment.ParentId
	    	};
	    	fflib_SecurityUtils.checkInsert(Attachment.SObjectType, fieldList);
	    	 insert a;
	    	 
	    }catch(Exception e){
	    	System.debug(e);
	    	a = null;
	    }
	   
	   return a;
	}
	
	
	
	public static String formatFilename(String name){
		if(name == null || name.equals(''))
			return name;
			
		String formatted = '';
		String[] namePhases = name.split('\\.');
		if(namePhases.size() > 1){
			for(Integer i = 0; i < namePhases.size() ; i ++){
				if(i == namePhases.size() - 1){
					formatted += '_' + DateTime.now().getTime() + '.'+ namePhases.get(i);
				}else if( i == 0) {
					formatted = namePhases.get(i);
				}else{
					formatted += '.' + namePhases.get(i);
				}
			}
		}else{
			formatted = name + '_' + DateTime.now().getTime();
		}
		
		return formatted;
	}
	
	public String callWebService(Task t, Attachment a ){
		
		User currentUser = UserInformationCon.getUserDetails;
		
		if(! Test.isRunningTest()){
			if(currentUser.AmazonS3_Folder__c == null || currentUser.AmazonS3_Folder__c == ''){
				System.debug('Failed to find the AmazongS3 Folder of user ['+currentUser.name+']');
				System.debug('Abort to call web service !');
				
				ActivityDocOperator.updateActivityStatus('Error','Failed to find the AmazongS3 Folder of user ['+currentUser.name+']', t.id);
				return '';
			}
		}
		
			
	
		List<ActivityDoc> activityDocs = new List<ActivityDoc>();
		
		//build one ActivityDoc object
		ActivityDoc activityDoc = buildActivityDoc(t, a, currentUser);
        
        //
        activityDocs.add(activityDoc);
        
        //serialize activityDocs to json object
        String jsonString = JSON.serialize(activityDocs);
        
        //Added by Alvin for fixing the encoding issues when there are some special characters.
        //encode json string
        jsonString=EncodingUtil.urlEncode(jsonString,'UTF-8');
        
        //encode session id
        String sessionId = UserInfo.getSessionId();
        if(sessionId != null){
        	sessionId = EncodingUtil.urlEncode(UserInfo.getSessionId(), 'UTF-8');
        } else{
        	sessionId = '';
        }
        //build and encode url
        String surl = EncodingUtil.urlEncode(URL.getSalesforceBaseUrl().toExternalForm() +'/services/Soap/u/25.0/'+UserInfo.getOrganizationId(),'UTF-8');
       
        //invoke web service
        if(! Test.isRunningTest()){
        	ActivityDocServices.ImportCandidateByEc2(jsonString, sessionId, surl);
        	ActivityDocOperator.updateActivityStatus('In Progress','Request has been sent out', t.id);
        }   
        
        return surl;
	}
	
	public ActivityDoc buildActivityDoc(Task t, Attachment a, User currentUser){
		
		Boolean skillParsingEnabled = SkillGroups.isSkillParsingEnabled();
		
		//build one ActivityDoc object
		ActivityDoc activityDoc = new ActivityDoc();
		//org id
        activityDoc.orgId = Userinfo.getorganizationid().subString(0,15);
        //activity id
        activityDoc.activityId = t.Id;               
        
        //who map
        String whoId = t.WhoId;
        if(whoId == null){
        	whoId = '';
        }
        activityDoc.whoMap.put(whoId,getLookUpFieldNameViaObjectName(t.Who.type));
        
        //what map
        activityDoc.whatMap.put(t.WhatId,getLookUpFieldNameViaObjectName(t.What.type));
        //doc type
        activityDoc.docType = 'Resume';
        //attachment name
       	activityDoc.attachmentName = a.Name;
        //activityDoc.attachmentName = formatFilename(a.Name);
        //attachment id
        activityDoc.attachmentId = a.Id;
        //amazonS3 folder
        activityDoc.amazonS3Folder = currentUser.AmazonS3_Folder__c;
        //name space
        activityDoc.nameSpace = PeopleCloudHelper.getPackageNamespace();
        //enable skill parsing
        activityDoc.enableSkillParsing = String.valueOf( skillParsingEnabled );
        //from email service
        activityDoc.isEmailService = true;
        
        //is selected
        activityDoc.isSelected=true;
       
		//set default skill groups       
        if( skillParsingEnabled ){
        	//get orginal json string of default skill groups from SF
        	String defaultSkillGroupString = getDefautSkillGroups();
        	if( defaultSkillGroupString != null && !defaultSkillGroupString.equals('')){
        		try{
        			//deserialize it to List<String> and assign to activityDoc.skillGroupIds
        			activityDoc.skillGroupIds = (List<String>) JSON.deserialize(defaultSkillGroupString, List<String>.class);
        			
        		}catch(Exception e){
        			System.debug(e);
        		}	
        	}			
		}
		
		return activityDoc;
	}
	
	/**
	* Record a task/activity according to 
	*/
	public Task recordTask( Messaging.InboundEmail email ){
		
		Id aid = getDefaultAdminAccount();
		if(aid == null ){
			
			return null;
		}
		
		Task task = new Task();
		//task.OwnerId = Userinfo.getUserId();
		task.WhatId = aid;
		task.Subject = 'Email Service: ' + email.subject;
		task.ActivityDate = Date.today(); //System.now();
		task.Description = email.plainTextBody;
		task.status = TASK_STATUS;
		task.Priority = TASK_PRIORITY;
	    try{
	    	//check FLS
	    	List<String> fieldList = new List<String>{
	    		'WhatId',
	    		'Subject',
	    		'ActivityDate',
	    		'Description',
	    		'Status',
	    		'Priority'
	    	};
	    	fflib_SecurityUtils.checkInsert(task.getSObjectType(), fieldList);
	    	 insert task;
	    	 System.debug('Success to record task ['+task.subject+'] to ['+task.WhatId+']');
	    }catch(Exception e){
	    	System.debug(e);
	    	System.debug('Failed to record task ['+task.subject+'] to ['+task.WhatId+']');
	    	task = null;
	    }
	   
	   return task;
	}
	
	
	public Id getDefaultAdminAccount(){
		Id adminId = null;
		try{
			CommonSelector.checkRead(Account.sObjectType, 'Id');
			adminId = [select id from Account where name = :ACCOUNT_ADMIN_NAME].id;
		}catch(Exception e){
			System.debug('Account ['+ACCOUNT_ADMIN_NAME+'] does not exist ! ');
			System.debug(e);
		}
		
		if(adminId == null){
			Account a = new Account(name=ACCOUNT_ADMIN_NAME);
			try{
				//check FLS
				fflib_SecurityUtils.checkFieldIsInsertable(Account.SObjectType, Account.Name);
				insert a;
				
				adminId = a.Id;
				System.debug('Success to create account['+adminId+'] ['+a.name+']');
			}catch(Exception e){
				System.debug(e);
			}
		}
		
		return adminId;
		
	}
	
	public String getDefautSkillGroups(){
		String groups = '';
		RecordType defaultSkillGroupRecordType = DaoRecordType.DefaultSkillGroupRT;
		try{
			CommonSelector.checkRead(CTCPeopleSettingHelper__c.sObjectType, 'Default_Skill_Groups__c');
			groups = [Select c.Default_Skill_Groups__c From CTCPeopleSettingHelper__c c where RecordTypeId =: defaultSkillGroupRecordType.Id limit 1].Default_Skill_Groups__c;
		}catch(Exception e){
			System.debug(e);
		}
		return groups;
	}
	

	public String getLookUpFieldNameViaObjectName(String objectApiName){
        //all fields of resume and files 
        //key is the field api name 
        Map<String,Schema.SObjectField> resumeObjectFields 
                    = Web_Document__c.SObjectType.getDescribe().fields.getMap();
                    
        //map contains resume lookup object api name and corresponding filed api name
        Map<String,String> resumeLookUpObjectFieldMap=new Map<String,String>();
        String fieldApiName='';
        for(String key:resumeObjectFields.KeySet())
        {
            Schema.SObjectField s=resumeObjectFields.get(key);
            String resumefieldApiName = s.getDescribe().getName();
            String resumeRelatedOjectName='';
            
            //Check if the field is lookup field
            if(s.getDescribe().getReferenceTo().size()>0)
            {
                resumeRelatedOjectName
                    =s.getDescribe().getReferenceTo().get(0).getDescribe().getName();
            }
            
            //put lookup object name and field name in the map
            if(resumeRelatedOjectName != '' && resumeRelatedOjectName != null)
            {
                resumeLookUpObjectFieldMap.put(resumeRelatedOjectName,resumefieldApiName);
            }
        }
        
        fieldApiName=resumeLookUpObjectFieldMap.get(objectApiName);
        
        return fieldApiName;
    }
	
	
	
}