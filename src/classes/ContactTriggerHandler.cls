public class ContactTriggerHandler {
	// this method should be beforeDelContact
	public static List<Web_Document__c> WebdocumentList{get; set;}
	public static Boolean isDocumentTriggerFired=true;
	public static void afterUpdateDocs(List<Contact> conlist){
		List<Web_Document__c> pre_deleted_webdocs=new List<Web_Document__c>();		
		
		CommonSelector.checkRead(Web_Document__c.sObjectType, 'Id, Name');
		pre_deleted_webdocs= [select Id, Name from Web_Document__c where isDelete__C=true and Document_Related_To__c in: conlist limit 1000];
		for(Web_Document__c webdoc: pre_deleted_webdocs){
			webdoc.isDelete__c=false;
		}
		fflib_SecurityUtils.checkFieldIsUpdateable(Web_Document__c.SObjectType, Web_Document__c.IsDelete__c);
		update pre_deleted_webdocs;
		
		
		CommonSelector.checkRead(Web_Document__c.sObjectType, 'Id, Name');
		List<Web_Document__c> wds = [select Id, Name from Web_Document__c where Document_Related_To__c in : conlist];
		List<Web_Document__c> webdocsneedtobeupdated=new List<Web_Document__c>();
		for(Web_Document__c webdoc: wds){
			webdoc.isDelete__c=true;
			webdocsneedtobeupdated.add(webdoc);
		}
		fflib_SecurityUtils.checkFieldIsUpdateable(Web_Document__c.SObjectType, Web_Document__c.IsDelete__c);
		update 	webdocsneedtobeupdated;		
		
	}
	//This method should be after Delete Contact
	//Update Docs
	
	public static void UndeleteDocs(Set<ID> masterconlist){
		
		if(masterconlist.size()>0){
			
			CommonSelector.checkRead(Web_Document__c.sObjectType, 'Id, Name');
			List<Web_Document__c> wds = [select Id, Name from Web_Document__c where Document_Related_To__c in : masterconlist];
			List<Web_Document__c> webdocsneedtobeupdated=new List<Web_Document__c>();
			for(Web_Document__c webdoc: wds){
				webdoc.isDelete__c=false;
				webdocsneedtobeupdated.add(webdoc);
			}
			fflib_SecurityUtils.checkFieldIsUpdateable(Web_Document__c.SObjectType, Web_Document__c.IsDelete__c);
			update 	webdocsneedtobeupdated;	
		}	
	}
	//Delete Docs;
	public static void afterDelContact(List<Web_Document__c> webdoclist){		//List<Contact> conlist
		List<Web_Document__c> wds = new List<Web_Document__c>();
		if(webdoclist!= null && webdoclist.size()>0){
			
			CommonSelector.checkRead(Web_Document__c.sObjectType, 'Id, Name');
			wds = [select Id, Name from Web_Document__c where isDelete__C=true and Id in: webdoclist limit 1000];
			if(wds.size()>0){
				fflib_SecurityUtils.checkObjectIsDeletable(Web_Document__c.SObjectType);
				delete wds;
			} 
		}
	} 
	
	public static void afterInsert(List<Contact> cons){
		List<Id> cids = new List<Id>();
		List<Id> aids = new List<Id>();
		Set<Id> aidset = new Set<Id>();
		List<Id> vids = new List<Id>();
		List<Placement_Candidate__c> mcs = new List<Placement_Candidate__c>();
		
		for(Contact con : cons){
			if(con.job_reference__c!=null && con.job_reference__c.indexOf(':')!=-1) { // candidate, advertisement for seek export api
				cids.add(con.Id);   // candidate list
				aids.add(con.job_reference__c.split(':')[1]);   // advertisement id list
				aidset.add(con.job_reference__c.split(':')[1]);  // advertisement id set
			}
		}
		
		if(cids.size()==0){
			return;
		}
		
		CommonSelector.checkRead(Advertisement__c.sObjectType, 'Id, Vacancy__c, Vacancy__r.Id');
	   	Map<Id, Advertisement__c> adMap = new Map<Id, Advertisement__c>([select Id, Vacancy__c, Vacancy__r.Id from Advertisement__c p where id in :aidset]);
		for(Id aid:aids){
			vids.add(adMap.get(aid).Vacancy__r.Id);
		}
		for(Integer i=0; i<cids.size(); i++){ 
			mcs.add(new Placement_Candidate__c(Placement__c=vids.get(i), Candidate__c=cids.get(i), Online_Ad__c=aids.get(i)));
		}
		//check FLS
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			Placement_Candidate__c.Placement__c,
			Placement_Candidate__c.Candidate__c,
			Placement_Candidate__c.Online_Ad__c
		};
		fflib_SecurityUtils.checkInsert(Placement_Candidate__c.SObjectType, fieldList);
		insert mcs;
	} 
	
	/**
	 * Send delete request to Daxtra to deleted candidate in Daxtra database
	 * Add by Lina
	 */ 
	public static void deleteDaxtraSearch(List<Contact> cons) {
	    List<PersonFeed> feedList = PersonFeedListCreater.getDeleteCandidatePersonFeedList(cons);
	    SQSHelper.sendSQSRequests(feedList);
	}	
	
}