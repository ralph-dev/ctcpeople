/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ContactServicesTest {
	 static testMethod void myUnitTest0(){
	 	System.runAs(DummyRecordCreator.platformUser) {
	 		Integer recordNumber=10;
	   		DataTestFactory.recordNumber=recordNumber;
	   		list<Contact> contacts=DataTestFactory.createContacts(); 	
	    	ContactServices contServices=new ContactServices();
	    	list<map<String,Object>> billerConts=contServices.convertContactToBillerContact(contacts);
	    	System.assertEquals(billerConts.size(), recordNumber);
	 	}
   		
    }
   static testMethod void myUnitTest1(){
   	System.runAs(DummyRecordCreator.platformUser) {
   		Integer recordNumber=10;
   		DataTestFactory.recordNumber=recordNumber;
   		list<Contact> contacts=DataTestFactory.createContacts(); 	
    	ContactServices contServices=new ContactServices();
    	map<String,Object> billerConts=contServices.convertContactToBillerContactMap(contacts);
    	System.assertEquals(billerConts.keyset().size(), recordNumber);
   	}
    }
   static testMethod void myUnitTest2(){
   	System.runAs(DummyRecordCreator.platformUser) {
    	Integer recordNumber=10;
   		DataTestFactory.recordNumber=recordNumber;
   		list<Contact> contacts=DataTestFactory.createContacts(); 	
    	ContactServices contServices=new ContactServices();
    	list<map<String,Object>> approverConts=contServices.convertContactToApprover(contacts);
    	System.assertEquals(approverConts.size(), recordNumber);
   	}
    	
    }
   static testMethod void myUnitTest3(){
   	System.runAs(DummyRecordCreator.platformUser) {
   	Integer recordNumber=10;
   		DataTestFactory.recordNumber=recordNumber;
   		list<Contact> contacts=DataTestFactory.createContacts(); 	
    	ContactServices contServices=new ContactServices();
    	map<String,Object> candidateConts=contServices.convertCandidateToEmployeeMap(contacts);
    	System.assertEquals(candidateConts.keyset().size(), recordNumber);
   	}
    }
    static testMethod void myUnitTest4(){
    	System.runAs(DummyRecordCreator.platformUser) {
   		Integer recordNumber=10;
   		DataTestFactory.recordNumber=recordNumber;
   		list<Contact> contacts=DataTestFactory.createContacts(); 
   		list<String> contactIds=new list<String>();
		for(Contact cond: contacts){
			contactIds.add(cond.Id);
		}
		ContactServices contServices=new ContactServices();
    	list<Contact> conts=contServices.retrieveBillerContactsByIds(contactIds);
    	System.assertEquals(conts.size(), recordNumber);
    	}
    }
    
    static testMethod void myUnitTest5(){
    	System.runAs(DummyRecordCreator.platformUser) {
   		Integer recordNumber=10;
   		DataTestFactory.recordNumber=recordNumber;
   		list<Contact> contacts=DataTestFactory.createContacts(); 
   		list<String> contactIds=new list<String>();
		for(Contact cond: contacts){
			contactIds.add(cond.Id);
		}
		ContactServices contServices=new ContactServices();
    	list<Contact> conts=contServices.retrieveApproverContactsByIds(contactIds);
    	System.assertEquals(conts.size(), recordNumber);
    	}
    }
    
    static testMethod void myUnitTest6(){
    	System.runAs(DummyRecordCreator.platformUser) {
   		Integer recordNumber=10;
   		DataTestFactory.recordNumber=recordNumber;
   		list<Contact> contacts=DataTestFactory.createContacts(); 
   		list<String> contactIds=new list<String>();
		for(Contact cond: contacts){
			contactIds.add(cond.Id);
		}
		ContactServices contServices=new ContactServices();
    	list<Contact> conts=contServices.retrieveCandidateToEmployeesByIds(contactIds);
    	System.assertEquals(conts.size(), recordNumber);
    	}
    }
    
     static testMethod void myUnitTest7(){
     	System.runAs(DummyRecordCreator.platformUser) {
   		Integer recordNumber=10;
   		DataTestFactory.recordNumber=recordNumber;
   		list<Contact> contacts=DataTestFactory.createContacts(); 
		ContactServices contServices=new ContactServices();
    	map<String,Object> conts=contServices.convertContactToApproverMap(contacts);
    	System.assertEquals(conts.keyset().size(), recordNumber);
     	}
    }
    static testMethod void myUnitTest8(){
    	System.runAs(DummyRecordCreator.platformUser) {
   		Integer recordNumber=10;
   		DataTestFactory.recordNumber=recordNumber;
   		list<Contact> contacts=DataTestFactory.createCands(); 
   		list<Placement_Candidate__c> placements=DataTestFactory.createCandidateManagements(); 
		ContactServices contServices=new ContactServices();
		map<String,String> candidatePlacementMap=new map<String,String>();
		for(Placement_Candidate__c cm: placements){
			candidatePlacementMap.put(cm.Candidate__c,cm.ID);
		}
    	list<map<String,Object>> conts=contServices.convertCandidateToEmployee(contacts,candidatePlacementMap);
    	System.assertEquals(conts.size(), recordNumber);
    	}
    }
    
    // Can not be tested unless the field set is created as Fax and MobilePhone
    private static testMethod void testGetRequiredFieldSet() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Set<String> expected = new Set<String>{'Fax', 'MobilePhone'};
        // // System.debug(cs.getRequiredFieldSet());
        // System.assertEquals(false, expected.addAll(ContactServices.getRequiredFieldSet()));
        System.assertNotEquals(null, ContactServices.getRequiredFieldSet());
    	}
    }
    
    private static testMethod void testGetContactsForDedupUpdate() {
    	System.runAs(DummyRecordCreator.platformUser) {
        List<Contact> init = DataTestFactory.createContacts();
        
        List<Id> ids = new List<Id>();
        for(Contact c : init) {
            ids.add(c.Id);
        }
        
        List<Contact> res = ContactServices.getContactsForDedupUpdate(ids);
        try {
            System.debug(res.get(0).Resume__c);
        } catch (Exception e) {
            System.assert(false);
        }
        
        System.assertEquals(init.size(), res.size());
    	}
    }
    
    private static testMethod void testUpdatedContactForDedup() {
    	System.runAs(DummyRecordCreator.platformUser) {
        Contact c1 = new Contact(lastname = 't1');
        Contact c2 = new Contact(lastname = 't2', Resume__c = 'exp');
        
        Contact c = ContactServices.updatedContactForDedup(c1, new List<Contact>{c2});
        
        System.assertEquals(c2.Resume__c, c.Resume__c);
    	}
    }
}