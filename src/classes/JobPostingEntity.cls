public class JobPostingEntity {
    public String searcharea {get;set;}
    public Set<String> idustry {get;set;}
    public String category {get;set;}
    public Set<String> occupations {get;set;}
    public String thiskey {get;set;}
    
    public JobPostingEntity(String s, String c){
        searcharea = s;
        category = c;
        thiskey = s + '__' + c;
        idustry = new Set<String>();
        occupations = new Set<String>();
    }
    
    public static testMethod void testJobPostingEntity(){
    	System.runAs(DummyRecordCreator.platformUser) {
        JobPostingEntity thecontroller = new JobPostingEntity('sydney','IT');       
        system.assertEquals(thecontroller.searcharea, 'sydney');
        system.assertEquals(thecontroller.category, 'IT');
        system.assertEquals(thecontroller.thiskey, 'sydney__IT');
    	}
    }
}