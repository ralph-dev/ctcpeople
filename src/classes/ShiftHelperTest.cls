@isTest
private class ShiftHelperTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
       Account acc = TestDataFactoryRoster.createAccount();
       Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
       Shift__c shift = TestDataFactoryRoster.createShift(vac);
       ShiftDTO sDTO = TestDataFactoryRoster.createShiftDTO(acc,vac,shift);
       
       //test method ShiftHelper.deserializeShiftsJson(String shiftJson>)
       String shiftDTOStr = JSON.serialize(sDTO);
       List<ShiftDTO> shiftDTOList = ShiftHelper.deserializeShiftsJson(shiftDTOStr);
       System.assertNotEquals(shiftDTOList, null);
       ShiftDTO shiftDTO = shiftDTOList[0];
       
       //test method ShiftHelper.convertShiftDTOToShiftSObject(Shift__c s, ShiftDTO sDTO)
       Shift__c shiftConverted = new Shift__c();
       ShiftHelper.convertShiftDTOToShiftSObject(shiftConverted,shiftDTO);
       System.assertEquals(shiftConverted.Id,shift.Id);
       
       //test method ShiftHelper.convertShiftDTOToShiftSObjectForShiftDB(Shift__c s, ShiftDTO sDBDTO)
       Shift__c shiftConvertedForShiftDTO = new Shift__c();
       ShiftHelper.convertShiftDTOToShiftSObjectForShiftDB(shiftConvertedForShiftDTO,shiftDTO);
       System.assertEquals(shiftConvertedForShiftDTO.Id,shift.Id);
       
       //test method ShiftHelper.assignValuesToShift(Shift__c sglShift, Shift__c inputShift)
       Shift__c shiftAfterAssignedValue = new Shift__c();
       ShiftHelper.assignValuesToShift(shiftAfterAssignedValue,shift);
       System.assertEquals(shiftAfterAssignedValue.Id,shift.Id);
       
       //test method ShiftHelper.convertShiftSObjectToShiftDTO(Shift__c inputShift)
       ShiftDTO shiftDTOConverted = ShiftHelper.convertShiftSObjectToShiftDTO(shift);
       System.assertEquals(shiftDTOConverted.getsId(), shift.Id);
       
       List<Shift__c> shiftList  = TestDataFactoryRoster.createShiftList(vac);
       //test method mergeShift(List<Shift__c> shiftList, Shift__c sglShift)
       List<Shift__c> shiftListAfterMerge = new List<Shift__c>();
       for(Shift__c sglShift : shiftList){
       		ShiftHelper.mergeShift(shiftListAfterMerge,sglShift);
       }
       System.assertEquals(shiftListAfterMerge.size(),1);
       
       //test method groupShiftsOfSameVacancy(List<Shift__c> inputShiftList)
       Map<String,List<Shift__c>> vacancyShiftsMap = ShiftHelper.groupShiftsOfSameVacancy(shiftList);
       System.assertNotEquals(vacancyShiftsMap,null);
       
       //test method groupVacancyIdNameFromShifts(List<Shift__c> inputShiftList)
       Map<String,ShiftDTO> vacancyIdNameMap = ShiftHelper.groupVacancyIdNameFromShifts(shiftList);
       System.assertNotEquals(vacancyIdNameMap,null);
       
       //test method checkEndDateNeedToBeUpdated(List<Shift__c> inputShiftList)
       List<Shift__c> shiftList1 = TestDataFactoryRoster.createShiftListToCheckEndDateNeedToBeUpdated(vac);
       List<Shift__c> shiftList1ToReturn = ShiftHelper.updateShiftEndDate(shiftList1);
       System.assertNotEquals(shiftList1ToReturn,null);
    	}
    }
}