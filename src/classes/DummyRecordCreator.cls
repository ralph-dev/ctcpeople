@isTest
public with sharing class DummyRecordCreator {
	public static Integer defaultDummyRecordCount{get;set;}
    public static String TEST_ENDPOINT = 'https://abc';
    
    static{
        defaultDummyRecordCount = 10;
    }

    /**
    * Record set is a data structure which contains the test record
    * created during the test. It has various data structure to store
    * the data for a easy access during the test. 
    **/
    public class DefaultDummyRecordSet{
    	public List<User> users {get; set;}
    	public List<Account> clients {get; set;}
    	public List<Contact> contacts {get; set;}
    	public List<Contact> candidates {get; set;}
    	public List<Placement__c> vacancies {get; set;}
    	public List<Placement_Candidate__c> cms {get; set;}
        public List<Web_Document__c> resumes {get; set;}
        public List<Skill_Group__c> skillGroups {get; set;}
    	
    	public Set<Id> userIdSet {get; set;}
    	public Set<Id> clientIdSet {get; set;}
    	public Set<Id> contactIdSet {get; set;}
    	public Set<Id> candidateIdSet {get; set;}
    	public Set<Id> vacancyIdSet {get; set;}
    	public Set<Id> cmIdSet {get; set;}
        public Set<Id> resumeIdSet {get; set;}
        public Set<Id> skillGroupIdSet {get; set;}

    	public Map<Id, user> userMap {get; set;}
    	public Map<Id, Account> clientMap {get; set;}
    	public Map<Id, Contact> contactMap {get; set;}
    	public Map<Id, Contact> candidateMap {get; set;}
    	public Map<Id, Placement__c> vacancyMap {get; set;}
    	public Map<Id, Placement_Candidate__c> cmMap {get; set;}
        public Map<Id, Web_Document__c> resumeMap {get; set;}
        public Map<Id, Skill_Group__c> skillGroupMap {get; set;}
    	
    	public DefaultDummyRecordSet() {
    		users = new List<User>();
    		clients = new List<Account>();
    		contacts = new List<Contact>();
    		candidates = new List<Contact>();
    		vacancies = new List<Placement__c>();
    		cms = new List<Placement_Candidate__c>();
            resumes = new List<Web_Document__c>();
            skillGroups = new List<Skill_Group__c>();

    		userIdSet = new Set<Id>();
    		clientIdSet = new Set<Id>();
    		contactIdSet = new Set<Id>();
    		candidateIdSet = new Set<Id>();
    		vacancyIdSet = new Set<Id>();
    		cmIdSet = new Set<Id>();
            resumeIdSet = new Set<Id>();
            skillGroupIdSet = new Set<Id>();

    		userMap = new Map<Id, user>();
    		clientMap = new Map<Id, Account>();
    		contactMap = new Map<Id, Contact>();
    		candidateMap = new Map<Id, Contact>();
    		vacancyMap = new Map<Id, Placement__c>();
    		cmMap = new Map<Id, Placement_Candidate__c>();
            resumeMap = new Map<Id, Web_Document__c>();
            skillGroupMap = new Map<Id, Skill_Group__c>();
    	}
    }
    
     public static void assignCTCAdminPermissionSet(User u){
    	System.runAs(u){
                    try{
                        PermissionSet ps = [select id,name from PermissionSet where name='CTC_Admin_PermissionSet'];
                        System.debug('permission set = ' + ps );
                        if(ps != null){
                            PermissionSetAssignment psa = new PermissionSetAssignment();
                            psa.AssigneeId = u.id;
                            psa.PermissionSetId = ps.id;
                            
                            System.debug('Create permission assignment');
                            insert psa;
                        }
                    }catch(Exception e){
                        System.debug(e);
                    }
                }
    }
    
    public static User admin{
	    get{
	        if(admin ==null){
	        	admin = new User(Username='ctcPeopleAdmin75869037476@clicktocloud.com',
	                    LastName='lnAdmin',Email='admin@clicktocloud.com', Alias='ctcp',
	                    CommunityNickname='ctcPeople75869037476@clicktocloud.com',
	                    TimeZoneSidKey='Australia/Sydney', LocaleSidKey='en_AU',
	                    EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US',
	                    ProfileId= adminProfile.Id, isActive=true, AmazonS3_Folder__c='test');
	            system.debug('Create Admin.');                    
	            //system.debug(admin);
                checkUserFLS();
	            insert admin;
	            
	            assignCTCAdminPermissionSet(admin);
	        }
	        return admin;
	    }    
	}
	
    public static User adminCareerOne{
	    get{
	        if(adminCareerOne == null){
	        	adminCareerOne = new User(Username='ctcPeopleAdminCareerOne75869037476@clicktocloud.com',
	                    LastName='lnCarrerOne',Email='adminCareerOne@clicktocloud.com', Alias='ctcp',
	                    CommunityNickname='ctcPeople75869037477@clicktocloud.com',
	                    TimeZoneSidKey='Australia/Sydney', LocaleSidKey='en_AU',
	                    EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US',
	                    //ProfileId= adminProfile.Id, isActive=true, AmazonS3_Folder__c='test',
	                    ProfileId= standardPlatformProfile.Id, isActive=true, AmazonS3_Folder__c='test',
	                    CareerOne_Usage__c='1',Monthly_Quota_CareerOne__c='2');
                checkUserFLS();
	            insert adminCareerOne;
	            
	            assignCTCAdminPermissionSet(adminCareerOne);
	        }
	        return adminCareerOne;
	    }    
	}	
	

	 
	
    public static User platformUser {
        get {
            if(platformUser == null) {
                platformUser = new User(Username='ctcPeoplePlatform75869037476@clicktocloud.com',
                        LastName='lnPlatform',Email='adminPlatformUser@clicktocloud.com', Alias='ctcp',
                        CommunityNickname='ctcPeople75869037478@clicktocloud.com',
                        TimeZoneSidKey='Australia/Sydney', LocaleSidKey='en_AU',
                        EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US',
                        //ProfileId= standardPlatformProfile.Id, 
                        //ProfileId= customStandardPlatformProfile.Id, 
                        
                        ProfileId= securityStandardPlatformProfile.Id,
                        
                        isActive=true, 
                        AmazonS3_Folder__c='test');
                checkUserFLS();
                insert platformUser;
                
                assignCTCAdminPermissionSet(platformUser);
                
                /*
               PermissionSet ps = [ SELECT Id FROM PermissionSet where name='ctcPeople_Tes_Code' limit 1];
                if(ps != null){
                	//System.debug('PermissionSet : ' + ps.Id);
                	PermissionSetAssignment psa = new PermissionSetAssignment();
                	psa.PermissionSetId = ps.Id;
                	psa.AssigneeId = platformUser.Id;
                	
                	insert psa;
                	//System.debug('assigned  : ' + psa.PermissionSetId + ' to user '+ psa.AssigneeId);
                }*/
            
            }
            return platformUser;
        }
    }
	
	private static Profile adminProfile{
        get{
            if(adminProfile ==null){
                adminProfile = [select Id, Name from Profile where UserType='Standard' and Name='System Administrator' limit 1];
            }
            return adminProfile;
        }    
    }

    private static Profile standardPlatformProfile{
        get{
            if(standardPlatformProfile==null){
                standardPlatformProfile = [select Id, Name from Profile where UserType='Standard' and Name='ctcPeople Tes Code' limit 1];
            }
            return standardPlatformProfile;
        }
    }
    
    private static Profile customStandardPlatformProfile{
        get{
            if(customStandardPlatformProfile==null){
                customStandardPlatformProfile = [select Id, Name from Profile where UserType='Standard' and Name='Custom - Standard Platform User' limit 1];
            }
            return customStandardPlatformProfile;
        }
    }
    
    private static Profile securityStandardPlatformProfile{
        get{
            if(securityStandardPlatformProfile==null){
            	try{
            		securityStandardPlatformProfile = [select Id, Name from Profile where UserType='Standard' and Name='Security - Standard Platform User' limit 1];
            	}catch(Exception e){
            		
            	}
                
                if(securityStandardPlatformProfile == null){
                	securityStandardPlatformProfile = standardPlatformProfile;
                }
            }
            return securityStandardPlatformProfile;
        }
    }

	public static DefaultDummyRecordSet createDefaultDummyRecordSet(){
		DefaultDummyRecordSet rs = new DefaultDummyRecordSet();
	    // Create Users
        //createUsers(rs);

	    if (admin == null) {
	    	system.debug(LoggingLevel.WARN, 'When deploying code to brand new instance, DummyRecordCreator need the ability to create super admin and use it to create test data in order to pass all tests');
	        createRecordSet(rs);
	    } else {
	        System.runAs(admin){
		     	createRecordSet(rs);
		    }
		}

		return rs;
	}

	public static void createUsers(DefaultDummyRecordSet rs) {
		for (Integer i = 0; i < defaultDummyRecordCount; i++) {
        	String orgId = UserInfo.getOrganizationId();
		    String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
		    Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
		    String uniqueName = orgId + dateString + randomInt;
    
            User user = new User(Username='ctcpeople_'+uniqueName+'@clicktocloud.com',
                LastName='ln',Email='ctcepeople_'+i+'@test.com',
                Alias='pc1A1',CommunityNickname=uniqueName.right(40),
                TimeZoneSidKey='Australia/Sydney',LocaleSidKey='en_AU',
                EmailEncodingKey='ISO-8859-1',
                ProfileId=standardPlatformProfile.Id,
                AmazonS3_Folder__c ='temp003',
                LanguageLocaleKey='en_US');
            rs.users.add(user);
        }
        checkUserFLS();
        insert rs.users;
        // Create a map for quicker access to listings by their ids
        rs.userMap = new Map<Id, User>(rs.users);
        for(User u: rs.users) {
            rs.userIdSet.add(u.id);
        }        
	}

	private static void createRecordSet(DefaultDummyRecordSet rs) {
		// Create accounts
        createAccounts(rs);

        // Create contacts
        createContacts(rs);

        createCandidates(rs);

        createVacancies(rs);

        createResumes(rs);

        createSkillGroups(rs);
	}

	private static void createAccounts(DefaultDummyRecordSet rs){
        for (Integer i = 0; i < defaultDummyRecordCount; i++) {
            Account acct = new Account();
            acct.Name = 'testchildqueriesacct';
            rs.clients.add(acct);
        }
        //checkFLS
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Account.Name
        };
        fflib_SecurityUtils.checkInsert(Account.SObjectType, fieldList);
        insert rs.clients;
        // Create a map for quicker access to accounts by their ids
        rs.clientMap = new Map<Id, Account>(rs.clients);
        for(Account acct: rs.clients) {
            rs.clientIdSet.add(acct.id);
        }
    }

    /**
     * create Contact needs to be called after calling createAccount.
     * */
    private static void createContacts(DefaultDummyRecordSet rs){
    	RecordType contactRecordType = DaoRecordType.corpContactRT;
        System.debug('in DummyRecordCreator'+contactRecordType);
        for (Integer i = 0; i < defaultDummyRecordCount; i++) {
            Contact cont = new Contact(FirstName='testFirstName' + i, LastName='testLastName' + i, RecordTypeId = contactRecordType.Id);
            cont.AccountId = rs.clients.get(i).Id;
            rs.contacts.add(cont);  
        }
        //checkFLS
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Contact.FirstName,
            Contact.LastName,
            Contact.RecordTypeId,
            Contact.AccountId
        };
        fflib_SecurityUtils.checkInsert(Contact.SObjectType, fieldList);
        insert rs.contacts;     
        // Create a map for quicker access to contacts by their ids
        rs.contactMap = new Map<Id, Contact>(rs.contacts);
        //System.debug('Contact Map:' + rs.contactMap);
        for(Contact cont: rs.contacts) {
            rs.contactIdSet.add(cont.id);
        }
    }

    /**
     * create Candidate.
     * */
    private static void createCandidates(DefaultDummyRecordSet rs){
        Integer jobApplcationId = 100000001;
    	RecordType contactRecordType = DaoRecordType.indCandidateRT;
        for (Integer i = 0; i < defaultDummyRecordCount; i++) {
            Contact cont = new Contact(FirstName='testCanFirstName' + i, LastName='testCanLastName' + i, Email=i+'test@test.com' , RecordTypeId = contactRecordType.Id, jobApplication_Id__c = jobApplcationId+ i);
            rs.candidates.add(cont);  
        }
        //checkFLS
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Contact.FirstName,
            Contact.LastName,
            Contact.Email,
            Contact.RecordTypeId,
            Contact.jobApplication_Id__c
        };
        fflib_SecurityUtils.checkInsert(Contact.SObjectType, fieldList);
        insert rs.candidates;     
        // Create a map for quicker access to contacts by their ids
        rs.candidateMap = new Map<Id, Contact>(rs.candidates);
        for(Contact cont: rs.contacts) {
            rs.candidateIdSet.add(cont.id);
        }
    }

    /**
     * create vacancies
     **/
    private static void createVacancies(DefaultDummyRecordSet rs) {
    	for (Integer i = 0; i < defaultDummyRecordCount; i++) {
    		Placement__c vacancy = new Placement__c(Name='testVacancy'+i, Company__c=rs.clients.get(i).Id);
    		rs.vacancies.add(vacancy);
    	}
        //check FLS
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Placement__c.Name,
            Placement__c.Company__c
        };
        fflib_SecurityUtils.checkInsert(Placement__c.SObjectType, fieldList);
    	insert rs.vacancies;
    	rs.vacancyMap = new Map<Id, Placement__c>(rs.vacancies);
    	for(Placement__c vac : rs.vacancies) {
    		rs.vacancyIdSet.add(vac.Id);
    	}
    }

    private static void createResumes(DefaultDummyRecordSet rs) {
        for (Integer i = 0; i < defaultDummyRecordCount; i++) {
            Web_Document__c resume = new Web_Document__c(Name='Resume_'+ rs.candidates[i].Name, Document_Related_To__c = rs.candidates[i].Id, ObjectKey__c = 'Resume_'+ rs.candidates[i].Name+'.doc');
            rs.resumes.add(resume);
        }
        //check FLS
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Web_Document__c.Name,
            Web_Document__c.Document_Related_To__c,
            Web_Document__c.ObjectKey__c
        };
        fflib_SecurityUtils.checkInsert(Web_Document__c.SObjectType, fieldList);
        insert rs.resumes;

        rs.resumeMap = new Map<Id, Web_Document__c>(rs.resumes);
        for(Web_Document__c res : rs.resumes) {
            rs.resumeIdSet.add(res.Id);
        }
    }
    
    /**
     * Create dummy document
     **/
    public static Document CreateDocument(){
    	Document tempdoc = new Document();
    	tempdoc.Name = 'test.doc';
    	tempdoc.Body = Blob.valueOf('test');
    	tempdoc.FolderId = UserInfo.getUserId();
        //check FLS
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Document.Name,
            Document.Body,
            Document.FolderId
        };
        fflib_SecurityUtils.checkInsert(Document.SObjectType, fieldList);
    	insert tempdoc;
    	return tempdoc;
	}
	
	/**
	 * Create Skill Group
	 **/ 
    public static void createSkillGroups(DefaultDummyRecordSet rs) {
        
        Skill_Group__c sg = new Skill_Group__c();
        sg.Name = 'test';
        sg.Enabled__c = true;
        sg.Default__c = false;
        sg.Skill_Group_External_Id__c = 'G001';
        rs.skillGroups.add(sg);
        checkSGFLS();
        insert rs.skillGroups;
        
        rs.skillGroupMap = new Map<Id, Skill_Group__c>(rs.skillGroups);
        for(Skill_Group__c skillGroup : rs.skillGroups) {
            rs.skillGroupIdSet.add(skillGroup.Id);
        }
    }
    
	/**
	 * Create Skill Group
	 **/ 
    public static Skill_Group__c createSkillGroup() {
        Skill_Group__c sg = new Skill_Group__c();
        sg.Name = 'test';
        sg.Enabled__c = true;
        sg.Default__c = false;
        sg.Skill_Group_External_Id__c = 'G001';
        checkSGFLS();
        insert sg;
        return sg;
    }
    
    /**
     * Create Skill
     **/
    public static Skill__c createSkill() {
        Skill_Group__c sg = createSkillGroup();
        Skill__c s = new Skill__c();
        s.Name = 'test';
        s.Skill_Group__c = sg.Id;
        s.Ext_Id__c = 'S001';
        //check FLS
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Skill__c.Name,
            Skill__c.Skill_Group__c,
            Skill__c.Ext_Id__c
        };
        fflib_SecurityUtils.checkInsert(Skill__c.SObjectType, fieldList);
        insert s;
        return s;
    }
    
    /**
	 * Create default Skill Group
	 **/ 
    public static Skill_Group__c createDefaultSkillGroup() {
        Skill_Group__c sg = new Skill_Group__c();
        sg.Name = 'test';
        sg.Enabled__c = true;
        sg.Default__c = true;
        sg.Skill_Group_External_Id__c = 'DSG001';
        checkSGFLS();
        insert sg;
        return sg;
    }

    private static void checkSGFLS(){
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Skill_Group__c.Name,
            Skill_Group__c.Enabled__c,
            Skill_Group__c.Default__c,
            Skill_Group__c.Skill_Group_External_Id__c
        };
        fflib_SecurityUtils.checkInsert(Skill_Group__c.SObjectType, fieldList);
    }

    private static void checkUserFLS(){
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            User.UserName,
            User.LastName,
            User.Email,
            User.Alias,
            User.CommunityNickName,
            User.TimeZoneSidKey,
            User.LocaleSidKey,
            User.EmailEncodingKey,
            User.LanguageLocaleKey,
            User.ProfileId,
            User.isActive,
            User.AmazonS3_Folder__c,
            User.ProfileId,
            User.CareerOne_Usage__c,
            User.Monthly_Quota_CareerOne__c
        };
        fflib_SecurityUtils.checkInsert(User.SObjectType, fieldList);
    }
}