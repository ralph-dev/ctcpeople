@isTest
private class CandidateSkillServicesTest{
    @isTest
    static void testsearchCandidatesBySkills(){
        Contact dummycon = TestDataFactoryRoster.createContact();     //create a contact
        Skill_Group__c sGroup = TestDataFactoryRoster.createSkillGroup();
        Skill__c skill = new Skill__c();
		skill.Skill_Group__c = sGroup.Id;
		skill.Name = 'Java';
		skill.Ext_Id__c = 'Java';
		insert skill;
		Skill__c skill2 = new Skill__c();
		skill2.Skill_Group__c = sGroup.Id;
		skill2.Name = 'Html';
		skill2.Ext_Id__c = 'Html';
		insert skill2;
		Skill__c skill3 = new Skill__c();
		skill3.Skill_Group__c = sGroup.Id;
		skill3.Name = 'css';
		skill3.Ext_Id__c = 'css';
		insert skill3;
        Candidate_Skill__c cSkill = TestDataFactoryRoster.createCandidateSkill(dummycon, skill);
        Candidate_Skill__c cSkill2 = TestDataFactoryRoster.createCandidateSkill(dummycon, skill);//create candidate skill
        
        Map<String, String> skillsMap = new Map<String, String>();
        skillsMap.put(skill.Id, 'and');
        skillsMap.put(skill2.Id, 'not');
        skillsMap.put(skill3.Id, 'else');

        candidateDTO conDTO = new candidateDTO();
        conDTO.setCandidate(dummycon);
        conDTO.setRadiusRanking(1);
        
        Map<String, candidateDTO> conDTOMap = new Map<String, candidateDTO>();
        conDTOMap.put(dummycon.id,conDTO);

        CandidateSkillServices cSkillServices = new CandidateSkillServices();
        Map<String, candidateDTO> returnResult = cSkillServices.searchCandidatesBySkills(skillsMap, conDTOMap);
        system.assertEquals(returnResult.size(),1);

    }
}