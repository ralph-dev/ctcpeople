/**
 * This is the dummy record creator for StyleCategory__c object
 * This class is used for testing purpose
 *
 * Updated by: Lina Wei
 * Updated on: 24/03/2017
 *
 * Modified by andy for security review II
 *
 */
@isTest
public class StyleCategoryDummyRecordCreator {

	public static void createStyleCategoryDummyRecord() {
		
		List<StyleCategory__c> sCategorys = new List<StyleCategory__c>();
		StyleCategory__c sCategory = new StyleCategory__c();
		sCategory.WebSite__c = 'JXT_NZ';
		sCategory.White_labels__c = 'a;b';
		sCategorys.add(sCategory);
		
		StyleCategory__c sTradeMeCategory = new StyleCategory__c();
		sTradeMeCategory.ProviderCode__c = 'abc';
		sTradeMeCategory.CompanyCode__c = 'abc';
		sTradeMeCategory.OfficeCodes__c = 'abc';
		sTradeMeCategory.WebSite__c = 'Trademe';
		sCategorys.add(sTradeMeCategory);
		
		system.assert(sCategorys.size()>0);
		CommonSelector.quickInsert( sCategorys);
	}
	
	public static StyleCategory__c setJobboardStyleCategory(){
		StyleCategory__c JXTjobboard = new StyleCategory__c();
		//JXTjobboard.JXT_Username__c = 'test';
		JXTjobboard.WebSite__c = 'JXT';
		JXTjobboard.Advertiser_Id__c = '1234567';
		CommonSelector.quickInsert(  JXTjobboard );
		CredentialSettingService.insertJXTCredential(JXTjobboard.id,'test','');
		
		//Create dummy dynamic application form
		StyleCategory__c dummydynamicapplicationform = new StyleCategory__c();
		String dafid = DaoRecordType.ApplicationFormStyleRT.Id;
		dummydynamicapplicationform.RecordTypeId = dafid;
		dummydynamicapplicationform.Name = 'test';
		dummydynamicapplicationform.Active__c = true;
		CommonSelector.quickInsert( dummydynamicapplicationform);
		
		//Create dummy template
		StyleCategory__c dummytemplate = new StyleCategory__c();
		String dtid = DaoRecordType.adTemplateRT.Id;
		dummytemplate.RecordTypeId = dtid;
		dummytemplate.templateActive__c = true;
		dummytemplate.templateJXT__c = '1234567';
		dummytemplate.templateJXTNZ__c = '1234567';
		dummytemplate.templateMYC__c = '123467';
		dummytemplate.templateSeek__c = '1234567';
		dummytemplate.templateC1__c = '1234567';
		CommonSelector.quickInsert( dummytemplate);
		
		return dummydynamicapplicationform;
	}
	
	public static StyleCategory__c createScWithOwnerAndRT(String ownerId, String rtId){
		StyleCategory__c sc = new StyleCategory__c();
		sc.OwnerId = ownerId;
		sc.RecordTypeId = rtId;
		CommonSelector.quickInsert( sc);
		return sc;
	}

	public static StyleCategory__c createLinkedInAccount() {
		StyleCategory__c LinkedInAccount = new StyleCategory__c();
		LinkedInAccount.Name = 'LinkedIn Account';
		LinkedInAccount.RecordTypeId = DaoRecordType.linkedInaccountRT.Id;
		LinkedInAccount.Advertiser_Id__c = '1234';
		LinkedInAccount.Account_Active__c = true;
		LinkedInAccount.LinkedIn_Partner_ID__c = '1328';
		CommonSelector.quickInsert( LinkedInAccount);
		return LinkedInAccount;
	}
	
	public static StyleCategory__c buildCareerOneAccount() {
		StyleCategory__c sc = new StyleCategory__c();
		sc.Name = 'name';
		sc.Advertiser_Uploadcode__c = 'test';
		sc.WebSite__c= 'CareerOne';
		sc.Account_Active__c = true;
		sc.RecordTypeId = DaoRecordType.careerOneAccountRT.Id;
		sc.Advertiser_Uploadcode__c = 'test';
    		
    	return sc;
	}
	
	

	public static StyleCategory__c createCareerOneAccount(){
		StyleCategory__c scCO = buildCareerOneAccount();
		checkUpdatableOrInsertableSC();
		insert scCO;
		return scCO;
	}

	public static StyleCategory__c createJXTNZAccount(){
		StyleCategory__c sc = new StyleCategory__c();
		sc.WebSite__c = 'JXT_NZ';
		sc.White_labels__c = 'a;b';
		checkUpdatableOrInsertableSC();
		insert sc;
		return sc;
	}

	public static StyleCategory__c createTradeMeAccount(){
		StyleCategory__c sTradeMeCategory = new StyleCategory__c();
		sTradeMeCategory.ProviderCode__c = 'abc';
		sTradeMeCategory.CompanyCode__c = 'abc';
		sTradeMeCategory.OfficeCodes__c = 'abc';
		sTradeMeCategory.WebSite__c = 'Trademe';
		checkUpdatableOrInsertableSC();
		insert sTradeMeCategory;
		return sTradeMeCategory;
	}


	private static void checkUpdatableOrInsertableSC(){

		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
				StyleCategory__c.Account_Active__c,
				StyleCategory__c.Advertiser_Id__c,
				StyleCategory__c.Advertiser_Uploadcode__c,
				StyleCategory__c.CareerOne_Premium_Package__c,
				StyleCategory__c.CompanyCode__c,
				StyleCategory__c.Name,
				StyleCategory__c.OfficeCodes__c,
				StyleCategory__c.ProviderCode__c,
				StyleCategory__c.RecordTypeId,
				StyleCategory__c.White_labels__c,
				StyleCategory__c.WebSite__c,
				StyleCategory__c.LinkedIn_Partner_ID__c
		};

		fflib_SecurityUtils.checkInsert(StyleCategory__c.SObjectType, fieldList);
		fflib_SecurityUtils.checkUpdate(StyleCategory__c.SObjectType, fieldList);
	}
}