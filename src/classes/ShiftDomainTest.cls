@isTest
private class ShiftDomainTest {
	final static STRING ROSTER_STATUS_FILLED = 'Filled';
	@isTest
    static void splitShiftForDisplayTest() {
       Account acc = TestDataFactoryRoster.createAccount();
       Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
       List<Shift__c> inputShiftList = TestDataFactoryRoster.createShiftListForSplitShiftFunction(acc,vac);
       
       //test method ShiftDomain splitShiftForDisplay()
       ShiftDomain sDomain = new ShiftDomain(inputShiftList);
       List<Shift__c> shiftsForDisplay = sDomain.splitShiftForDisplay();
       System.assertNotEquals(shiftsForDisplay.size(),0);
    }
    
    @isTest
    static void updateShiftPlacedPositionsNumberTest() {
	   	Account acc = TestDataFactoryRoster.createAccount();
		Contact con = TestDataFactoryRoster.createContact();
	    Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
	    Shift__c shift = TestDataFactoryRoster.createShift(vac);
	    Days_Unavailable__c roster = TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_FILLED);
	    List<Shift__c> shiftList = new List<Shift__c>();
	    shiftList.add(shift);
	    ShiftDomain sDomain = new ShiftDomain(shiftList);
	    sDomain.updateShiftPlacedPositionsNumber();
	    system.assert(shiftList[0].Shift_Status__c == 'Open' );
    }
    
    
}