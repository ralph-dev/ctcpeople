public with sharing class PlacementAstutePayrollImplementation {
	
	public static void syncPlacements(list<String> placementIds, String session){
		PlacementServices service=new PlacementServices();
		list<Placement_Candidate__c> placements=service.retrievePlacementsByIds(placementIds);
		list<map<String,Object>> objSTR=new list<map<String,Object>>();
		map<String,Object> messagesJson=new map<String,Object>();
		objSTR=service.convertPlacementToEmployee(placements);
		messagesJson.put('userSaves',objSTR);
		String messages=JSON.serialize(messagesJson);
		String endPointStr=Endpoint.getAstuteEndpoint(); 
		endPointStr=endPointStr+'/AstutePayroll/employee';
		AstutePayrollFutureJobs.pushHandler(messages, endpointStr, session);
	}
	/***
		Synchronize Placements to Astute Payroll
		1. Initiate Sync   
		2. Update Placements Only
		
		"Is Ready for Astute Payroll" has higher priority than "Is Pushing To AP"
	*****/
	
	public static void astutePayrollTrigger(list<Placement_Candidate__c> newRecords){
		CTCPeopleSettingHelperServices ctcPeopleSettingHelperServ=new CTCPeopleSettingHelperServices();
    	if(!ctcPeopleSettingHelperServ.hasApAccount()){
    		return;
    	}
    	map<String,Placement_Candidate__c> placementReferenceMap=new map<String,Placement_Candidate__c>();
    	list<String> placementIds=new list<String>();
    	list<String> placementIdsForUpdate=new list<String>();
    	list<String> placementIdsForInitiation=new list<String>();
		for(Placement_Candidate__c singlePlacement: newRecords){
    		placementIds.add(singlePlacement.Id);
    		if(singlePlacement.Is_Ready_For_Astute_Payroll__c){
                 singlePlacement.Is_Ready_For_Astute_Payroll__c=false;
                 singlePlacement.Astute_Payroll_Upload_Status__c ='On Hold for uploading all records';
                 placementIdsForInitiation.add(singlePlacement.Id);
                 continue;
            }
    		if(singlePlacement.Is_Pushing_To_AP__c){
    			singlePlacement.Is_Pushing_To_AP__c=false;
    			singlePlacement.Astute_Payroll_Upload_Status__c='On Hold';
    			placementIdsForUpdate.add(singlePlacement.Id);
    		}
    	}
    	list<Placement_Candidate__c> placementWithReferences=new list<Placement_Candidate__c>();
    	
    	CommonSelector.checkRead(Placement_Candidate__c.sObjectType,'id, Work_place__r.Workplace_Id__c, Company_Entity__r.Company_Entity_ID__c'
    		+', Approver__r.Approver_User_Name__c, Rate_Card__r.Rate_Card_ID__c, Rule_Group__r.Rule_Group_ID__c,'
    		+' Second_Approver__r.Approver_User_Name__c, Placement__r.Company__r.Biller_Id__c'
    		+' ,Placement__r.Biller_Contact__r.Biller_Contact_Id__c, Occupation_Library__r.Occupation_Libraries_Id__c ');
    		
    	placementWithReferences=[select id, Work_place__r.Workplace_Id__c, Company_Entity__r.Company_Entity_ID__c
    		, Approver__r.Approver_User_Name__c, Rate_Card__r.Rate_Card_ID__c, Rule_Group__r.Rule_Group_ID__c,
    		 Second_Approver__r.Approver_User_Name__c, Placement__r.Company__r.Biller_Id__c
    		 ,Placement__r.Biller_Contact__r.Biller_Contact_Id__c, Occupation_Library__r.Occupation_Libraries_Id__c 
    		 from Placement_Candidate__c where id in: placementIds];
    	for(Placement_Candidate__c perPlacement: placementWithReferences){
    		placementReferenceMap.put(perPlacement.Id,perPlacement );
    	}
    	for(Placement_Candidate__c singlePlacement_Candidate: newRecords){
    		 //To ensure that the corresponding fields could be filled to replace the use of formular fields
			//Please notice that no more than 10 different references could be used in the formula per object.
			//This includes fields in package and out of package.    
			  Placement_Candidate__c referencePlacement=placementReferenceMap.get(singlePlacement_Candidate.Id);
			  if(referencePlacement!=null){
			  	singlePlacement_Candidate.WorkplaceId__c=referencePlacement.Work_place__r.Workplace_Id__c;
			  	singlePlacement_Candidate.FirstApproverID__c=referencePlacement.Approver__r.Approver_User_Name__c;
			  	singlePlacement_Candidate.SecondApproverId__c=referencePlacement.Second_Approver__r.Approver_User_Name__c;
			  	singlePlacement_Candidate.RateCardId__c=referencePlacement.Rate_Card__r.Rate_Card_ID__c;
			  	singlePlacement_Candidate.RuleGroupID__c=referencePlacement.Rule_Group__r.Rule_Group_ID__c;
			  	singlePlacement_Candidate.CompanyEntityId__c=referencePlacement.Company_Entity__r.Company_Entity_ID__c;
			  	singlePlacement_Candidate.BillerId__c=referencePlacement.Placement__r.Company__r.Biller_Id__c;
			  	singlePlacement_Candidate.BillerContactId__c=referencePlacement.Placement__r.Biller_Contact__r.Biller_Contact_Id__c;
			  	singlePlacement_Candidate.Occupation_LibraryId__c=referencePlacement.Occupation_Library__r.Occupation_Libraries_Id__c;
			  }
    	}
    	//If the first update is fired. Call future job to implement it.
    	if(placementIdsForInitiation.size()>0){
    		AstutePayrollFutureJobs.pusthToAPFirstTime(placementIdsForInitiation,UserInfo.getSessionId());
    	}
    	//If the update is fired, call futre job for just placements.
    	if(placementIdsForUpdate.size()>0){
    		AstutePayrollFutureJobs.syncPlacementsFuture(placementIdsForUpdate,UserInfo.getSessionId());
    	}
    	
    	
    			
	}
	
    //First time push to AP.
    public void astutePayrollFirstClass(list<Placement_Candidate__c> placement_Candidates){
    	CTCPeopleSettingHelperServices ctcPeopleSettingHelperServ=new CTCPeopleSettingHelperServices();
    	if(!ctcPeopleSettingHelperServ.hasApAccount()){
    		return;
    	}
       list<String> placementIds=new list<String>();
        for(Placement_Candidate__c placement : placement_candidates){
            if(placement.Astute_Payroll_Upload_Status__c=='On Hold for uploading all records'){
             	placementIds.add(placement.Id);
            }
        }
        if(placementIds.size()>0){
            AstutePayrollFutureJobs.pusthToAPFirstTime(placementIds,UserInfo.getSessionId());
        }
    }  

}