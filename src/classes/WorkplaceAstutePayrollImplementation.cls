public with sharing class WorkplaceAstutePayrollImplementation {
	public static void syncWorkplaces(list<String> workplaceIds, String session){
		WorkplaceServices wkServices=new WorkplaceServices();
		list<Workplace__c>workplaces=wkServices.retrieveWorkplaceByIds(workplaceIds);
		if(workplaces.size()==0){
			return;
		}
		list<map<String,Object>> objSTR=wkServices.convertWorkplaceToBillerWorkplace(workplaces);
		map<String,Object> messagesJson=new map<String,Object>();
		messagesJson.put('billerWorkplaceSaves',objSTR);
		String messages=JSON.serialize(messagesJson);
		String endpointAstute=Endpoint.getAstuteEndpoint();
		endpointAstute=endpointAstute+'/AstutePayroll/workplace';
        
		AstutePayrollFutureJobs.pushHandler(messages,endpointAstute,session);
		
	}
  
  /****
  		Synchronize Workplaces to Astute Payroll
  		
  										-Alvin
  ******/
  public static void astutePayrollTrigger(list<Workplace__c> workplaces){
  	CTCPeopleSettingHelperServices ctcPeopleSettingHelperServ=new CTCPeopleSettingHelperServices();
  	if(!ctcPeopleSettingHelperServ.hasApAccount()){
    		return;
    }
  	list<String> workplaceIds=new list<String>();
  	list<String> workplaceIdsForFix=new list<String>();
  	for(Workplace__c wkPlace : workplaces){
  		if(wkPlace.Is_Fixing_Data_For_AP__c){
  			wkPlace.Is_Fixing_Data_For_AP__c=false; 
  			wkPlace.Astute_Payroll_Upload_Status__c='On Hold'; 		
  			workplaceIdsForFix.add(wkPlace.Id);
  			continue;
  		}
  		if(wkPlace.Is_Pushing_To_AP__c){
  			wkPlace.Is_Pushing_To_AP__c=false; 
  			wkPlace.Astute_Payroll_Upload_Status__c='On Hold'; 			
  			workplaceIds.add(wkPlace.Id);
  		}
  		
  	}
  	if(workplaceIds.size()>0){
  		AstutePayrollFutureJobs.syncWorkplacesFuture(workplaceIds,AstutePayrollUtils.GetloginSession());
  	}
  	if(workplaceIdsForFix.size()>0){
  	 	PlacementServices placementServ=new PlacementServices();
  	 	placementServ.fixWorkplaces(workplaceIdsForFix);
  	}
  	
  }
  	
  	

}