@isTest
public class UpdateSelectTest {
    private static testMethod void unitTest1(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Advertisement__c adv = DataTestFactory.createAdvertisement();
        ApexPages.currentPage().getParameters().put('Id',adv.id);
        ApexPages.StandardController controller = new ApexPages.StandardController([select Id from Advertisement__c where Id =: adv.Id]); 
        UpdateSelect thecontroller = new UpdateSelect(controller);
        PageReference nullStatusPage =  thecontroller.selectPage();
        system.assertNotEquals(nullStatusPage, null);
        adv.Job_Posting_Status__c = 'In Queue';
        adv.Status__c='Active';
        update adv;
        controller = new ApexPages.StandardController([select Id from Advertisement__c where Id =: adv.Id]);
        thecontroller = new UpdateSelect(controller);
        PageReference inQueueStatusPage =  thecontroller.selectPage();
        system.assertNotEquals(inQueueStatusPage, null);
        adv.Job_Posting_Status__c = null;
        adv.webSite__c = 'seek';
        update adv;
        controller = new ApexPages.StandardController([select Id from Advertisement__c where Id =: adv.Id]);
        thecontroller = new UpdateSelect(controller);
        PageReference seekPage =  thecontroller.selectPage();
        system.assertNotEquals(seekPage, null);
        adv.webSite__c = 'careerone';
        update adv;
        controller = new ApexPages.StandardController([select Id from Advertisement__c where Id =: adv.Id]);
        thecontroller = new UpdateSelect(controller);
        PageReference careeronePage =  thecontroller.selectPage();
        system.assertNotEquals(careeronePage, null);
        adv.webSite__c = 'trademe';
        update adv;
        controller = new ApexPages.StandardController([select Id from Advertisement__c where Id =: adv.Id]);
        thecontroller = new UpdateSelect(controller);
        PageReference trademePage =  thecontroller.selectPage();
        system.assertNotEquals(trademePage, null);
        adv.webSite__c = 'jxt';
        update adv;
        controller = new ApexPages.StandardController([select Id from Advertisement__c where Id =: adv.Id]);
        thecontroller = new UpdateSelect(controller);
        PageReference jxtPage =  thecontroller.selectPage();
        system.assertNotEquals(jxtPage, null);
        adv.webSite__c = 'jxt_nz';
        update adv;
        controller = new ApexPages.StandardController([select Id from Advertisement__c where Id =: adv.Id]);
        thecontroller = new UpdateSelect(controller);
        PageReference jxt_nzPage =  thecontroller.selectPage();
        system.assertNotEquals(jxt_nzPage, null);
        adv.webSite__c = 'website';
        update adv;
        controller = new ApexPages.StandardController([select Id from Advertisement__c where Id =: adv.Id]);
        thecontroller = new UpdateSelect(controller);
        PageReference websitePage =  thecontroller.selectPage();
        system.assertNotEquals(websitePage, null);
    	}
    }
}