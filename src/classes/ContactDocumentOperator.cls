/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

public class ContactDocumentOperator {
    public List<ActivityDoc> getActivityDocList(List<Contact> conList, Map<String, Web_Document__c> contactDoc) {
        system.debug('Start to generate ActivityDoc List');
        
        List<ActivityDoc> adList = new List<ActivityDoc>();
        User currentUser = UserInformationCon.getUserDetails;
        
        //generate contact record type list
        PCAppSwitch__c ps = PCAppSwitch__c.getInstance();
        String contactRecordTypeListString = ps.Contact_RecordType_for_Create_Documents__c;
        //Set<String> contactRecordIdSet = new Set<String>(); //as set contains function is not working, use the string contains function to replace.

        if (contactRecordTypeListString != null && contactRecordTypeListString != '') {
            //List<String> idList = contactRecordTypeListString.split(',');
            //contactRecordIdSet = new Set<String>();
            //contactRecordIdSet.addAll(idList);
        }else {
            //contactRecordIdSet = new Set<String>(DaoRecordType.nonCandidateRTIds);
            contactRecordTypeListString = String.valueOf(DaoRecordType.nonCandidateRTIds);
        }
        //system.debug('Contact RecordType List'+ contactRecordIdSet);
        
        List<Attachment> tmpattList = new List<Attachment>();
        for(Contact con : conList) {
            //system.debug('contactDoc ='+ contactDoc);
            system.debug('contactdoc is not existing ='+  (contactDoc == null || contactDoc.size()== 0 ||!contactDoc.keySet().contains(con.id)));
            system.debug('con.RecordTypeId ='+ String.valueOf(con.RecordTypeId).subString(0,15));
            system.debug('contactRecordIdcontains ='+ contactRecordTypeListString.contains(String.valueOf(con.RecordTypeId).subString(0,15)));
            if((contactDoc == null || contactDoc.size()== 0 ||!contactDoc.keySet().contains(con.id)) && contactRecordTypeListString.contains(String.valueOf(con.RecordTypeId).subString(0,15))) {
                Attachment tmpatt = new Attachment();
                tmpatt = createDocumentForContact(con.id);
                tmpattList.add(tmpatt);
            }
        }
        //system.debug('ActivityDoc LIst =' + adList);
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Attachment.Name,
            Attachment.ContentType,
            Attachment.Body,
            Attachment.ParentId
        };
        fflib_SecurityUtils.checkInsert(Attachment.SObjectType, fieldList);
        insert tmpattList;
        
        for(Attachment att : tmpattList) {
            ActivityDoc ad = new ActivityDoc();
            //system.debug('att contact id =' + att.ParentId);
            if(att.ParentId!= null) {
                ad.orgId = userinfo.getOrganizationId().subString(0,15);
                ad.docType = 'Resume';
                ad.whoMap = new Map<String,String>{att.ParentId => PeopleCloudHelper.getPackageNamespace()+'Document_Related_To__c'};
                ad.attachmentId = att.id;
                ad.attachmentName = att.Name;
                ad.isSelected = true;
                ad.amazonS3Folder = currentUser.AmazonS3_Folder__c;
                ad.nameSpace = PeopleCloudHelper.getPackageNamespace();
                ad.enableSkillParsing = 'false';
                adList.add(ad);
            }
        }
        return adList;
    }
    
    private Attachment createDocumentForContact(String contactId) {
        String startTimeStamp = ''+Datetime.now().getTime();
        
        Attachment doc = new Attachment();
        doc.Name = 'CD_'+contactId+'.txt';
        doc.ContentType = 'txt';
        doc.Body = Blob.valueOf(contactId);
        doc.ParentId  = contactId;
        // doc.FolderId = UserInfo.getUserId();
        return doc;
    }
}