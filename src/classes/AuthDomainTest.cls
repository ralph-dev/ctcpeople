@isTest
private class AuthDomainTest {
    
    @isTest
    static void test_authApp() {
        String instanceUrl = System.URL.getSalesforceBaseUrl().toExternalForm();
        String appId = 'Test_Connected_App';
        String returnUrl = instanceUrl + '/apex/AppsAuth';

        AuthDomain authDomain = new AuthDomain();
        String authUrl = authDomain.authApp(instanceUrl,appId,returnUrl);
        system.assert(authUrl!=null);
    }

}