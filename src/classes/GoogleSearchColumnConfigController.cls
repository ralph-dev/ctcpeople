/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

public with sharing class GoogleSearchColumnConfigController{
	// user details and user preference
	//String Userid;
	//String RecordTypeId;
	//String GoogleSearchUserPreferenceName;
 //   DisplayColumnHandler gscHandler;
	
	//// column values
	//List<SelectOption> columnItems = new List<SelectOption>();
 //   public String column1 {get;set;}
 //   public String column2 {get;set;}
 //   public String column3 {get;set;}
 //   public String column4 {get;set;}
 //   public String column5 {get;set;}
 //   public String column6 {get;set;}
 //   public DisplayColumn previousColumn1 {get;set;}
 //   public DisplayColumn previousColumn2 {get;set;}
 //   public DisplayColumn previousColumn3 {get;set;}
 //   public DisplayColumn previousColumn4 {get;set;}
 //   public DisplayColumn previousColumn5 {get;set;}
 //   public DisplayColumn previousColumn6 {get;set;}
 //   public Boolean columnSwitch1 {get;set;}
 //   public Boolean columnSwitch2 {get;set;}
 //   public Boolean columnSwitch3 {get;set;}
 //   public Boolean columnSwitch4 {get;set;}
 //   public Boolean columnSwitch5 {get;set;}
 //   public Boolean columnSwitch6 {get;set;}
 //   public String selectedObject {get;set;}
 //   public String confirmPopup {get;set;}
 //   public String noneValue {get;set;}
 //   public String noneLabel {get;set;}
 //   public List<String> defaultExcludedFieldsList;
 //   public final String DEFAULT_OBJECT = 'Contact';

    
    
 //   FieldsClass sfObject; 
 //   List<FieldsClass.AvailableField> allfields = new List<FieldsClass.AvailableField>{};
 //   List<DisplayColumn> selectedDisplayColumn = new List<DisplayColumn>{};

	//// constructor
	//public GoogleSearchColumnConfigController(){
	//}

	//// init 
	//public void init(){		
	//	noneValue = 'none';
	//	noneLabel = '--None--';
		
	//	selectedObject = ApexPages.currentPage().getParameters().get('obj');
	//	system.debug('GoogleSearchColumnConfigController ---- selectedObject = ' + selectedObject);	
		
	//	confirmPopup = ApexPages.currentPage().getParameters().get('confirmPopup');
	//	system.debug('GoogleSearchColumnConfigController ---- confirmPopup = ' + confirmPopup);	
		
		
	//	initUserDetail();
	//	List<DisplayColumn> columnList = getColumnFromUserPreference();
	//	populatePreviouslySelectedColumnValues(columnList);
		
		
	//	// get all fields under selected object
	//	sfObject = new FieldsClass(selectedObject);
 //       allfields = sfObject.getAvailableFields();
 //       system.debug('allfields size =' + allfields.size());
 //       getColumnFields(allfields);
        
	//}

 //   public List<SelectOption> getColumnItems(){
 //       return columnItems;
 //   }

	//// generate picklist 
 //   public void getColumnFields(List<FieldsClass.AvailableField> allcolumns){
 //       columnItems = new List<SelectOption>();
 //       List<SelectOption> columnOptions = new List<SelectOption>();
 //       //columnItems.add(new SelectOption('null','--None--'));
 //       if(allcolumns != null){
 //       	for(FieldsClass.AvailableField a : allcolumns){
 //               if(a.getFieldType().toLowerCase().trim() != 'textarea'){
 //                   columnOptions.add(new SelectOption(a.getFieldApi(),a.getFieldLabel()));
 //               }
 //           }
 //           columnOptions.sort();
 //       }
 //       system.debug('columnOptions before sort ='+ columnOptions);
 //       columnOptions = FieldsClass.selectOptionSortByLabel(columnOptions);
 //       system.debug('columnOptions ='+ columnOptions);
 //       columnItems.addAll(columnOptions);  
        
 //   } 
    
 //   // get column from user preference
 //   public List<DisplayColumn> getColumnFromUserPreference(){
 //   	String columnJSONString = null;
 //   	List<DisplayColumn> columnList = new List<DisplayColumn>();
    	
 //   	if(selectedObject == 'Contact'){
 //   		columnJSONString = gscHandler.getSearchColumnJSONFromUserPreference();
 //   	}else if(selectedObject == 'Placement_Candidate__c'){
 //   		columnJSONString = gscHandler.getScreenCandidateColumnJSONFromUserPreference();
 //   	}
    	
 //   	if(columnJSONString != null && columnJSONString != ''){
 //   		columnList = (List<DisplayColumn>)JSON.deserialize(columnJSONString, List<DisplayColumn>.class);
 //   		system.debug('columnList='+columnList);
 //   	}
 //   	return columnList;
 //   }
    
 //   // init selected column values
 //   private List<DisplayColumn> initPreviousColumnsValues(){
 //   	 List<DisplayColumn> previousColumnList = new List<DisplayColumn>();
 //   	 previousColumn1 = new DisplayColumn();
 //   	 previousColumn2 = new DisplayColumn();
 //   	 previousColumn3 = new DisplayColumn();
 //   	 previousColumn4 = new DisplayColumn();
 //   	 previousColumn5 = new DisplayColumn();
 //   	 previousColumn6 = new DisplayColumn();
    	 
 //   	 previousColumn1.ColumnName = '--None--';
 //   	 previousColumn2.ColumnName = '--None--';
 //   	 previousColumn3.ColumnName = '--None--';
 //   	 previousColumn4.ColumnName = '--None--';
 //   	 previousColumn5.ColumnName = '--None--';
 //   	 previousColumn6.ColumnName = '--None--';
    	 
 //   	 previousColumn1.Field_api_name = 'null';
 //   	 previousColumn2.Field_api_name = 'null';
 //   	 previousColumn3.Field_api_name = 'null';
 //   	 previousColumn4.Field_api_name = 'null';
 //   	 previousColumn5.Field_api_name = 'null';
 //   	 previousColumn6.Field_api_name = 'null'; 
    	 
 //   	 previousColumnList.add(previousColumn1);
 //   	 previousColumnList.add(previousColumn2);
 //   	 previousColumnList.add(previousColumn3);
 //   	 previousColumnList.add(previousColumn4);
 //   	 previousColumnList.add(previousColumn5);
 //   	 previousColumnList.add(previousColumn6);
    	 
 //   	 return previousColumnList;
 //   }
    
 //   // populate previous selected values to the picklist
 //   public void populatePreviouslySelectedColumnValues(List<DisplayColumn> columnList){
 //   	List<DisplayColumn> previousColumnList = new List<DisplayColumn>();
 //   	previousColumnList = initPreviousColumnsValues();
    	
 //   	if(columnList == null || columnList.size() ==0){
 //   		return;
 //   	}
    	 
 //   	Integer index = 0;
 //   	for(DisplayColumn column:columnList){
 //   		previousColumnList.get(index).ColumnName = column.ColumnName;
 //   		previousColumnList.get(index).Field_api_name = column.Field_api_name;
			
	//		// rendered each none select option
	//		if(index == 0){
	//			columnSwitch1 = true;
	//		}else if(index == 1){
	//			columnSwitch2 = true;
	//		}else if(index == 2){
	//			columnSwitch3 = true;
	//		}else if(index == 3){
	//			columnSwitch4 = true;
	//		}else if(index == 4){
	//			columnSwitch5 = true;
	//		}else if(index == 5){
	//			columnSwitch6 = true;
	//		}
			
 //   		index++;
 //   	}
    	
 //   	system.debug('columnSwitch1=' + columnSwitch1);
 //   	system.debug('columnSwitch2=' + columnSwitch2);
 //   	system.debug('columnSwitch3=' + columnSwitch3);
 //   	system.debug('columnSwitch4=' + columnSwitch4);
 //   	system.debug('columnSwitch5=' + columnSwitch5);
 //   	system.debug('columnSwitch6=' + columnSwitch6);
 //   }
    
 //   // update user preference with updated column json string
 //   public void updateColumnsInUserPreference(){
 //   	String updatedColumnJSON = constructColumnJsonString();
 //   	if(updatedColumnJSON == null){
 //   		return;
 //   	}
		
	//	if(selectedObject == 'Placement_Candidate__c'){
	//		gscHandler.updateScreenCandidateColumnJSON(updatedColumnJSON);		// update screen candidate column json
	//	}else{
	//		gscHandler.updateGoogleSearchColumnJSON(updatedColumnJSON);		// update google search column json
	//	}

 //   }
   
 //  // check if the new field is unique and if the new field should be excluded  
 //   public void addUniqueColumnField2List(List<String> columnList, String newFieldApiName, List<String> excludedFields){
	//	// check exclusion 
	//	for(String excludedField:excludedFields){
	//		if(excludedField == newFieldApiName){
	//			return; 
	//		}
	//	}

	//	// check uniqueness
	//	for(String existingFieldApiName:columnList){
	//		if(existingFieldApiName == newFieldApiName){
	//			return;
	//		}
	//	}
		
	//	// add the new field if it is both unique and allowed to be added
	//	columnList.add(newFieldApiName);
 //   } 
    
    
 //   // convert selected columns to DisplayColumn format
 //   private List<DisplayColumn> convert2DisplayColumn(List<String> columnList){
 //   	List<DisplayColumn> displayColumnList = new List<DisplayColumn>{};
 //   	for(String columnApiName:columnList){
 //   		if(columnApiName == null || columnApiName == ''){
 //   			continue;
 //   		}
    		
 //   		for(FieldsClass.AvailableField field: allfields){
 //               if(field.getFieldApi().trim() == columnApiName.trim()){
	//				DisplayColumn dc = new DisplayColumn();
	//				dc.ColumnName = field.getFieldLabel();
	//				dc.fieldtype = field.getFieldType();
	//				dc.Field_api_name = field.getFieldApi();
	//				dc.isCustomField = field.getIsCustom();
	//				dc.referenceFieldName = field.getRelationshipName();
	//				displayColumnList.add(dc);
 //               }
 //           }	
 //   	}
 //   	return displayColumnList;
 //   }
    
 //   // initialise user details for user preference
 //   private void initUserDetail(){
 // 		Userid = Userinfo.getUserId();
	//	RecordTypeId = DaoRecordType.googleSearchUserPerformacne.id;
	//	GoogleSearchUserPreferenceName = 'GoogleSearchUserPreferenceName_'+Userid;
 //   	gscHandler = new DisplayColumnHandler(Userid, RecordTypeId ,GoogleSearchUserPreferenceName);
 //   }
  
 //   // generate column json string
 //   private String constructColumnJsonString(){
 //   	List<String> excludedFieldsList = generateExcludedFieldsList();
    	
 //   	system.debug('column1=' + column1);
 //   	system.debug('column2=' + column2);
 //   	system.debug('column3=' + column3);
 //   	system.debug('column4=' + column4);
 //   	system.debug('column5=' + column5);
 //   	system.debug('column6=' + column6);
    	
 //   	List<String> columnList = new List<String>();
 //   	addUniqueColumnField2List(columnList,column1,excludedFieldsList);
 //   	addUniqueColumnField2List(columnList,column2,excludedFieldsList);
 //   	addUniqueColumnField2List(columnList,column3,excludedFieldsList);
 //   	addUniqueColumnField2List(columnList,column4,excludedFieldsList);
 //   	addUniqueColumnField2List(columnList,column5,excludedFieldsList);
 //   	addUniqueColumnField2List(columnList,column6, excludedFieldsList);
 //   	system.debug('constructColumnJsonString ---- columnList=' + columnList);
    	
 //   	List<DisplayColumn> selectedColumnList = convert2DisplayColumn(columnList);
 //   	system.debug('selectedColumnList='+selectedColumnList);
    	
 //   	if(selectedColumnList == null || selectedColumnList.size() == 0){
 //   		return '';
 //   	}
    	
 //   	return JSON.serialize(selectedColumnList);
 //   }
    
 //   // generate a list of fields that are default fields and should be excluded when generating the picklist
 //   private List<String> generateExcludedFieldsList(){
 //   	List<String> excludedFieldList = new List<String>();
 //   	if(selectedObject == 'Contact'){
 //   		excludedFieldList.add('Name');
 //   		// please add api name of Contact fields here
 //   	}else if(selectedObject == 'Placement_Candidate__c'){
 //   		excludedFieldList.add('Id');
 //   		excludedFieldList.add('Name');
 //   		excludedFieldList.add(PeopleCloudHelper.getPackageNamespace()+'Status__c');
 //   		excludedFieldList.add(PeopleCloudHelper.getPackageNamespace()+'Candidate_Status__c');
 //   		excludedFieldList.add(PeopleCloudHelper.getPackageNamespace()+'Candidate__c');
 //   		excludedFieldList.add(PeopleCloudHelper.getPackageNamespace()+'Rating__c');
 //   		// please add api name of Candidate Management fields here
 //   	}
		 
	//	return excludedFieldList;	
 //   }
          
}