public with sharing class AvailabilityService {
	final static String UPDATE_ERROR = 'error';
	final static String UPDATE_ERROR_DESC = 'Exception during updating records.';
	final static String UPDATE_ERROR_REC_NOT_EXIST = 'The record does not exist.';
	final static String UPDATE_SUCCESS = '';
	
	final static String DELETE_ERROR = 'error';
	final static String DELETE_ERROR_DESC = 'Exception during deleting records.';
	final static String DELETE_SUCCESS = '';
	
	final static String CREATE_ERROR = 'error';
	final static String CREATE_ERROR_DESC = 'Exception during creating new records.';
	final static String CREATE_SUCCESS = '';
	
	// create new avl records
	public static String createNewAvlRecords(List<AvailabilityEntity> avlList, List<Days_Unavailable__c> relatedAvlList, boolean isDupCheckEnabled){
		//System.debug('============= avlList:'+avlList);
		//System.debug('============= relatedAvlList:'+relatedAvlList);
		
		try{
			List<Days_Unavailable__c> avlListToCreate = new List<Days_Unavailable__c>();
		
			for(AvailabilityEntity avl : avlList ){
				resolveAllConflicts4SingleAvl(avl,relatedAvlList, isDupCheckEnabled);		// resolve conflicts	

				Days_Unavailable__c avlAfterResolvingConflict = createDaysUnavailableRecFromAvlRec(avl);		// convert object type	
			
				if(avlAfterResolvingConflict != null){
					//When the available status is available, there shouldn't have any value in candidate management.
					if(avlAfterResolvingConflict.Event_Status__c=='Available'){
						avlAfterResolvingConflict.Candidate_Management__c=null;
					}
					avlListToCreate.add(avlAfterResolvingConflict);	
				}					
			}
		
			//System.debug('avlListToCreate:' + avlListToCreate);
			if(avlListToCreate!= null && avlListToCreate.size() > 0){
				//check FLS
				List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
					Days_Unavailable__c.Start_Date__c, 	
					Days_Unavailable__c.End_Date__c,		
					Days_Unavailable__c.EventReason__c, 
					Days_Unavailable__c.Event_Status__c,
					Days_Unavailable__c.Contact__c,
					Days_Unavailable__c.Candidate_Management__c
				};
				fflib_SecurityUtils.checkInsert(Days_Unavailable__c.SObjectType, fieldList);
				insert avlListToCreate;	
				return CREATE_SUCCESS;
			}	
			return CREATE_SUCCESS;
		}catch(Exception e){
			return constructReturnMessage(CREATE_ERROR, CREATE_ERROR_DESC);
		}
	}
	
	
	// update an existing avl record given a record id 
	public static String updateAvlRecord(String avlId, List<AvailabilityEntity> avlList , List<Days_Unavailable__c> relatedAvlList,boolean isDupCheckEnabled){
		//System.debug('************* avlList:'+avlList);
		//System.debug('************* relatedAvlList:'+relatedAvlList);
		System.debug('************* avlId:'+avlId);	
		
		AvailabilityEntity avl = avlList.get(0);
		
		resolveAllConflicts4SingleAvl(avl,relatedAvlList, isDupCheckEnabled);		// resolve conflicts					
		
		Days_Unavailable__c avl2Update = updateDaysUnavailableFields(avlId, avl);
		
		if(avl2Update == null){
			return constructReturnMessage(UPDATE_ERROR, UPDATE_ERROR_REC_NOT_EXIST);	
		}
		
		try{
			// if the record is not tentative, update it. Otherwise, delete it
			if(!avl2Update.Event_Status__c.equalsIgnoreCase('tentative')){
				//When status is Available, the candidate management shouldn't have any record
				if(avl2Update.Event_Status__c=='Available'){
					avl2Update.Candidate_Management__c=null;
				}
				fflib_SecurityUtils.checkFieldIsUpdateable(Days_Unavailable__c.SObjectType, Days_Unavailable__c.Candidate_Management__c);
				update avl2Update;
				return UPDATE_SUCCESS;		
			}else{
				fflib_SecurityUtils.checkObjectIsDeletable(Days_Unavailable__c.SObjectType);
				delete avl2Update;	
				return UPDATE_SUCCESS;
			}	
		}catch(Exception e){
			return constructReturnMessage(UPDATE_ERROR, UPDATE_ERROR_DESC);	
		}
	}
	

	// delete a specific record 	
	public static String deleteAvlRecord(Days_Unavailable__c avl2Del){
		try{
		    List<Days_Unavailable__c> avl2DelList = new List<Days_Unavailable__c>();
		    avl2DelList.add(avl2Del);
		    fflib_SecurityUtils.checkObjectIsDeletable(Days_Unavailable__c.SObjectType);
			delete avl2DelList;	
			return DELETE_SUCCESS;
		}catch(Exception e){
			return constructReturnMessage(DELETE_ERROR, DELETE_ERROR_DESC);	
		}
	}
	
	// create a days unavailable record from an avl record
	private static Days_Unavailable__c createDaysUnavailableRecFromAvlRec(AvailabilityEntity avl){
		//system.debug('createDaysUnavailableRecFromAvlRec ----  entered:' + avl);
		Days_Unavailable__c avl2Create = new Days_Unavailable__c();
		
		// if the current new avl record is not 'tentative', return the avl record
		if(!avl.eventStatus.equalsIgnoreCase('tentative')){
			avl2Create.Start_Date__c 	= Date.valueOf(avl.startDate);
			avl2Create.End_Date__c		= Date.valueOf(avl.endDate);
			avl2Create.EventReason__c = avl.eventReason;
			avl2Create.Event_Status__c	= avl.eventStatus;
			avl2Create.Contact__c		= avl.candidateId;
			
			// if candidate management id is not null, assign the value to the avl record to be created
			if(avl.candidateManagementId != null && avl.candidateManagementId != ''){
				avl2Create.Candidate_Management__c = avl.candidateManagementId;
			}
			
			//system.debug('createDaysUnavailableRecFromAvlRec ----  avl2Create:' + avl2Create);
			return avl2Create;	
		}else{
			return null;
		}	
	}
	
	// return an updated days unavailable record
	private static Days_Unavailable__c updateDaysUnavailableFields(String avlId, AvailabilityEntity avl){
		Days_Unavailable__c avl2Update = AvailabilitySelector.getAvlRecordById(avlId);
		//System.debug('updateDaysUnavailableFields ------ avl2Update:' + avl2Update);
		
		// if the record doesn't exist, return null
		if(avl2Update == null){
			return null;
		}
		avl2Update.Start_Date__c 	= Date.valueOf(avl.startDate);
		avl2Update.End_Date__c		= Date.valueOf(avl.endDate);
		avl2Update.EventReason__c =avl.eventReason;
		avl2Update.Event_Status__c	= avl.eventStatus;
		
		return avl2Update;		
	}

	// resolve all conflicts for each avl record
	public static void resolveAllConflicts4SingleAvl(AvailabilityEntity avl, List<Days_Unavailable__c> relatedAvlList, boolean isDupCheckEnabled){
		//System.debug('---------- avl:'+avl);
		//System.debug('---------- relatedAvlList:'+relatedAvlList);
		
		//change the insert, update and delete method as Security Review requirement.
		List<Days_Unavailable__c> insertDaysUnavailableList = new List<Days_Unavailable__c>();
		List<Days_Unavailable__c> updateDaysUnavailableList = new List<Days_Unavailable__c>();
		List<Days_Unavailable__c> deleteDaysUnavailableList = new List<Days_Unavailable__c>();
		
		if(relatedAvlList != null){
			for(Days_Unavailable__c relatedAvl: relatedAvlList){
				
				// skip resolving conflicts if both are unavailable events and dupcheck is enabled
				if(!isDupCheckEnabled && relatedAvl.Event_Status__c.equalsIgnoreCase('unavailable') && avl.eventStatus.equalsIgnoreCase('unavailable')){
					continue;	
				}
				
				resolveSingleConflict(avl,relatedAvl, insertDaysUnavailableList, updateDaysUnavailableList, deleteDaysUnavailableList);				
			}	
		}	
		//Fields for checking
		//check FLS
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
				Days_Unavailable__c.Start_Date__c, 	
				Days_Unavailable__c.End_Date__c,		
				Days_Unavailable__c.EventReason__c, 
				Days_Unavailable__c.Event_Status__c,
				Days_Unavailable__c.Contact__c,
				Days_Unavailable__c.Candidate_Management__c
		};
		//Bulk insert
		if(insertDaysUnavailableList!=null&&insertDaysUnavailableList.size()>0)	{
			fflib_SecurityUtils.checkInsert(Days_Unavailable__c.SObjectType, fieldList);
			insert insertDaysUnavailableList;
		}

		//Bulk update
		if(updateDaysUnavailableList!=null&&updateDaysUnavailableList.size()>0)	{
			fflib_SecurityUtils.checkUpdate(Days_Unavailable__c.SObjectType, fieldList);
			update updateDaysUnavailableList;
		}

		//Bulk delete
		if(deleteDaysUnavailableList!=null&&deleteDaysUnavailableList.size()>0)	{
			fflib_SecurityUtils.checkObjectIsDeletable(Days_Unavailable__c.SObjectType);
			delete deleteDaysUnavailableList;
		}
	}
	
	// resolve conflict between a single avl record and another sf record
	public static void resolveSingleConflict(AvailabilityEntity avl, Days_Unavailable__c relatedAvl, List<Days_Unavailable__c> insertDaysUnavailableList
										, List<Days_Unavailable__c> updateDaysUnavailableList, List<Days_Unavailable__c> deleteDaysUnavailableList){
		Date sd = Date.valueOf(avl.startDate);
		Date ed = Date.valueOf(avl.endDate);
		Date sfsd = relatedAvl.Start_Date__c;
		Date sfed = relatedAvl.End_Date__c;
		//When status is Available, the candidate management shouldn't have any record
		if(relatedAvl.Event_Status__c=='Available'){
			relatedAvl.Candidate_Management__c=null;
		}
		System.debug('---------- sd:'+sd);
		System.debug('---------- ed:'+ed);
		System.debug('---------- sfsd:'+sfsd);
		System.debug('---------- sfed:'+sfed);		
		
		// sd < sfsd < ed < sfed
		// 		|---------|
		//  |---------|
		if(sd<=sfsd && sfsd<=ed && ed<sfed ){
			System.debug('resolveSingleConflict 1');
			relatedAvl.Start_Date__c = ed + 1;		// update sf avl record start date 
			updateDaysUnavailableList.add(relatedAvl);		
		}
		// sfsd < sd < sfed < ed
		// 	|---------|
		//  	|---------|
		else if(sfsd < sd && sd <= sfed && sfed <= ed){
			System.debug('resolveSingleConflict 2');
			relatedAvl.End_Date__c = sd - 1;		// update sf avl record end date
			updateDaysUnavailableList.add(relatedAvl);	
		}
		// sd < sfsd < sfed = ed
		// 	    |---------|
		//  |-------------|
		else if(sd< sfsd && ed == sfed){
			System.debug('resolveSingleConflict 3');
			deleteDaysUnavailableList.add(relatedAvl);		// delete sf avl record as it is fully overriden		
		}
		// sd = sfsd < sfed < ed
		// 	|---------|
		//  |-------------|
		else if(sd == sfsd &&  sfed < ed){
			System.debug('resolveSingleConflict 4');
			deleteDaysUnavailableList.add(relatedAvl);		// delete sf avl record as it is fully overriden			
		}
		// sd = sfsd < sfed = ed 
		// 	|---------|
		//  |---------|
		else if(sd == sfsd && ed == sfed){
			System.debug('resolveSingleConflict 5');
			deleteDaysUnavailableList.add(relatedAvl);		// delete sf avl record as it is fully overriden		
		}
		// sd < sfsd < sfed < ed
		// 	   |-----|
		//  |-------------|
		else if(sd < sfsd && sfed < ed){
			System.debug('resolveSingleConflict 6');
			deleteDaysUnavailableList.add(relatedAvl);		// delete sf avl record as it is fully overriden			
		}
		// sfsd < sd < ed < sfed
		// 	|-------------|
		//      |-----|
		else if(sfsd < sd && ed < sfed){
			System.debug('resolveSingleConflict 7');
			
			Days_Unavailable__c clonedAvlRecord = relatedAvl.clone(false, true, true, false);
			clonedAvlRecord.End_Date__c		= sd - 1;	// change the start date
			insertDaysUnavailableList.add(clonedAvlRecord);		// create new cloned sf avl record
					
			relatedAvl.Start_Date__c 	= ed + 1;		// change the end date
			updateDaysUnavailableList.add(relatedAvl);	// update changed sf avl record					
		}
		// 	            |-------------|
		//     |-----|		
		else if(sfsd > ed || sfed < sd){
			System.debug('resolveSingleConflict 8');
			// no need to do anything on existing sf avl records
		}	
	}
	


	
	// check if there are any duplicate available or unavailable records among the request and existing salesforce records
	public static String checkAvlDupRecords(List<AvailabilityEntity> avlList, List<Days_Unavailable__c> relatedAvlList){
		String returnMessage = '';
		
		for(AvailabilityEntity avl : avlList){
			if(!avl.eventStatus.equalsIgnoreCase('available') && !avl.eventStatus.equalsIgnoreCase('unavailable')){
				continue;		
			}
			for(Days_Unavailable__c sfAvl: relatedAvlList){
				if(avl.eventStatus.equalsIgnoreCase(sfAvl.Event_Status__c)){
					String errorType = 'duplicate ' + sfAvl.Event_Status__c;
					String errorDesc = 'The process was not successful due to duplicate ' + sfAvl.Event_Status__c + ' records.';
					returnMessage = constructReturnMessage(errorType,errorDesc);
					break;
				}		
			}	
		}
		
		return returnMessage;
	}
	
	// construct return message string
	public static String constructReturnMessage(String errorType, String errorDesc){
		Map<String,String> messageMap = new Map<String,String>();	
		messageMap.put(errorType,errorDesc);
		return JSON.serializePretty(messageMap);		
	}
	
	// ranking candidates by candidate availability record
	public Map<String, candidateDTO> rankingCandidatesByAvailability(List<ShiftDTO> shiftDTOs, Map<String, candidateDTO> candidateDTOMap) {
		//system.debug('shiftDTOs ='+ shiftDTOs);
		//system.debug('candidateDTOMap ='+ candidateDTOMap);
		List<Days_Unavailable__c> availabilities = new List<Days_Unavailable__c>();

		AvailabilitySelector aSelector = new AvailabilitySelector();
		availabilities = aSelector.getAllAvailabiltyRecordByShifts(shiftDTOs, candidateDTOMap.keySet());

		AvailabilityCompare aCompare = new AvailabilityCompare();
		candidateDTOMap = aCompare.compareShiftAndAvailability(shiftDTOs, availabilities, candidateDTOMap);
		//system.debug('candidateDTOMap ='+ candidateDTOMap);
		return candidateDTOMap;
	}
}