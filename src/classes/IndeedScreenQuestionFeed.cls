public with sharing class IndeedScreenQuestionFeed {
		private String apiname;
		private String name;
		private String question;
		private Integer sequence;
		private boolean isRequired;
		private Map<String,String> conditionField;
		private Map<String,String> conditionOption;
		public String getApiname() {
			return apiname;
		}
		public void setApiname(String apiname) {
			this.apiname = apiname;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getQuestion() {
			if(question!=null && question!=''){
				return question;
			}else{
				return name;
			}
		}
		public void setQuestion(String question) {
			this.question = question;
		}
		public Integer getSequence() {
			return sequence;
		}
		public void setSequence(Integer sequence) {
			this.sequence = sequence;
		}
		public boolean getIsRequired() {
			if(isRequired==null){
				isRequired=false;
			}
			return isRequired;
		}
		public void setIsRequired(boolean isRequired) {
			this.isRequired = isRequired;
		}
		public Map<String, String> getConditionField() {
			if(conditionField==null){
				conditionField=new map<String,String>();
			}
			return conditionField;
		}
		public void setConditionField(Map<String, String> conditionField) {
			this.conditionField = conditionField;
		}
		public Map<String, String> getConditionOption() {
			if(conditionOption==null){
				conditionOption=new map<String,String>();
			}
			return conditionOption;
		}
		public void setConditionOption(Map<String, String> conditionOption) {
			this.conditionOption = conditionOption;
		}
}