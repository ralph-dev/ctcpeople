/**
 * This is a test class for WebDocumentSelector
 * 
 * Created by: Lina
 * Created on: 19/07/2016
 * 
**/ 

@isTest
public class WebDocumentSelectorTest {
    
    static testmethod void testGetWebDocsByIds() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Create data
		Web_Document__c doc1 = new Web_Document__c(name='web001', Type__c='Resume');
		Web_Document__c doc2 = new Web_Document__c(name='web002', Type__c='Resume');
		insert doc1;
		insert doc2;
        List<String> docIds = new List<String>{doc1.Id, doc2.Id};
        
        // Verify
        List<Web_Document__c> docList = WebDocumentSelector.getWebDocsByIds(docIds);
		System.assertEquals(2, docList.size());
    	}
    }
    
}