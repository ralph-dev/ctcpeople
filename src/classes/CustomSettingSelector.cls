/*
	This class to get custom setting details
*/

public with sharing class CustomSettingSelector {
	private String PROFILE_ID = UserInfo.getProfileId(); 
	
	public CustomSettingSelector() {
		
	}

	public S3Credential__c getOrgS3Credential() {
		S3Credential__c s3Credential = S3Credential__c.getOrgDefaults();
		return s3Credential;
	}
}