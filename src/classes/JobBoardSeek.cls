/**
 * This is the concrete class for Seek Job Posting
 * This class
 *
 * Created by: Lina Wei
 * Created on: 01/03/2017.
 */

public with sharing class JobBoardSeek extends JobBoard{
    public static final Map<String, String> itemFieldApiMap;

    static {
        if (itemFieldApiMap == null) {
            itemFieldApiMap = new Map<String, String>{
                    'BULLET1' => 'Bullet1_Item__c',
                    'BULLET2' => 'Bullet2_Item__c',
                    'BULLET3' => 'Bullet3_Item__c',
                    'CONTACTNAME' => 'ContactName_Item__c',
                    'LOCATION' => 'Location_Item__c',
                    'PHONENO' => 'PhoneNo_Item__c',
                    'REFERENCE' => 'Reference_Item__c',
                    'REFNUMBER' => 'RefNumber_Item__c',
                    'SALARY' => 'Salary_Item__c',
                    'SUBHEADING' => 'Subheading_Item__c',
                    'SUBHEADING1' => 'Subheading1_Item__c',
                    'SUBHEADING2' => 'Subheading2_Item__c',
                    'SUBHEADING3' => 'Subheading3_Item__c'
            };
        }
    }
    public JobBoardSeek() {
        super();
    }

    public JobBoardSeek(Advertisement__c ad) {
        super(ad);
        itemFieldMapSelected = new Map<String, String>();
        templateHasItems = false;
    }

    /************************************************/
    /************* abstract init method *************/
    /************************************************/

    /*
     * init job board specific variables
     */
    public override void initJobBoardVariable() {
        jobBoardType = 'Seek';
        accountRecordTypeId = DaoRecordType.seekAccountRT.Id;
    }

    /*
     * init other specific variables
     */
    public override void initOtherVariable() {
        // init bullet point
        if (String.isEmpty(ad.Search_Bullet_Point_1__c)) {
            ad.Search_Bullet_Point_1__c = ad.Bullet_1__c;
        }
        if (String.isEmpty(ad.Search_Bullet_Point_2__c)) {
            ad.Search_Bullet_Point_2__c = ad.Bullet_2__c;
        }
        if (String.isEmpty(ad.Search_Bullet_Point_3__c)) {
            ad.Search_Bullet_Point_3__c = ad.Bullet_3__c;
        }
        ad.Bullet1_Item__c = ad.Bullet_1__c;
        ad.Bullet2_Item__c = ad.Bullet_2__c;
        ad.Bullet3_Item__c = ad.Bullet_3__c;
        ad.RefNumber_Item__c = ad.Reference_No__c;
    }

    /*
     * check posting privilege
     */
    public override void checkPostPrivilege() {
        // check quota remaining
        if (!actionType.equals('repost')) {
            checkRemainingQuotaWhenInit();
            if (!show_page) {
                return;
            }
        }
        if (!JobBoardUtils.checkUserHasActiveJobBoardAccount(u.Seek_Account__c, DaoRecordType.seekAccountRT.Id)) {
            msgSignalhasmessage = addErrorMessage('Currently you have no privilege to post Seek Advertisement, because you have no active seek account assigned!');
            show_page = false;
        }
    }
 
    /*
     * check updating privilege
     */
    public override void checkUpdatePrivilege() {

        /***************** Check if ad status is ok for update *****************/
        checkAdStatusForUpdate();
        if (!show_page) {
            return;
        }

        /***************** Check if posting account is still active *****************/
        if (!JobBoardUtils.checkUserHasActiveJobBoardAccount(ad.Advertisement_Account__c, accountRecordTypeId)) {
            msgSignalhasmessage = addErrorMessage('Currently you have no privilege to update this Seek Advertisement, because this seek account is not active anymore!');
            show_page = false;
            return;
        }

        /***************** Check if posting account is still assigned to the user *****************/
        if (String.isNotEmpty(u.Seek_Account__c)) {
            if (u.Seek_Account__c.containsIgnoreCase(ad.Advertisement_Account__c)) {
                return;
            }
        }
        msgSignalhasmessage = addErrorMessage('You are not permitted to update this Seek Advertisement, because you do not have a Seek account assigned to you. Please contact your administrator!');
        show_page = false;
    }

    /*
     * initialise variables for email service setting
     */
    public override void checkEmailServiceSetting() {
        // Check if user enable email service for seek job posting
        JobPostingSettings__c userJobPostingSetting = SeekJobPostingSelector.getJobPostingCustomerSetting();
        enableEmailService = userJobPostingSetting.Posting_Seek_Ad_with_User_Email__c;
        if (enableEmailService) {
            if (actionType.equals('post')) {
                ad.Prefer_Email_Address__c = SeekJobPostingSelector.getUserPreferEmailAddress();
            }
        } else {
            ad.Prefer_Email_Address__c = null;
        }
    }

    /*
     * get web service endpoint
     */
    public override String getWebServiceEndpoint() {
        return Endpoint.getSeekDefaultEndpoint();
    }



    /****************************************************************************/
    /************* abstract method for Ad post/update/clone/archive *************/
    /****************************************************************************/

    /*
     * set required value on the ad, such as ad status
     */
    public override void setAdValue() {
        /************* set online footer *************/
        String tempfooter = '';
        ad.Online_footer__c = jobOnlineFooterTemp;
        if(ad.Online_footer__c != null) 
        {
            tempfooter = ad.Online_footer__c.replaceAll('\\[\\%User Name\\%\\]',UserInfo.getName());
            tempfooter = tempfooter.replaceAll('\\[\\%Vacancy Referece No\\%\\]',ad.Reference_No__c );
        }
        ad.Online_Footer__c = tempfooter;

        /************* set skill groups *************/
        if(enableskillparsing){
            String tempString  = String.valueOf(defaultskillgroups);
            tempString = tempString.replaceAll('\\(', '');
            tempString = tempString.replaceAll('\\)', '');
            tempString = tempString.replace(' ','');
            ad.Skill_Group_Criteria__c = tempString;
        }

        /************* set standout field *************/
        if (!ad.isStandOut__c) {
            ad.AdvertiserJobTemplateLogoCode__c = null;
            ad.Logo_Name__c = null;
            ad.Search_Bullet_Point_1__c = null;
            ad.Search_Bullet_Point_2__c = null;
            ad.Search_Bullet_Point_3__c = null;
        }

        /************* set not used template item to null ************/
        if (itemFieldMapSelected != null && itemFieldMapSelected.size() > 0) {
            for (String item : itemFieldApiMap.keySet()) {
                if (itemFieldMapSelected.get(item) == null) {
                    ad.put(itemFieldApiMap.get(item), null);
                }
            }
        } else {
            for (String item : itemFieldApiMap.keySet()) {
                ad.put(itemFieldApiMap.get(item), null);
            }
        }

        /************* set other fields *************/
        ad.WebSite__c = 'Seek';
        ad.Job_Content__c = jobContentTemp;
        if (actionType.equals('post') || actionType.equals('repost') || actionType.equals('cloneToPost')) {
            ad.Status__c = 'Active';
            ad.Job_Posting_Status__c = 'In Queue';
        }
    }

    /*
     * actions need to do after save new ad
     */
    public override void afterSaveAd() {
        /************* update user quota *************/
        usage = String.isEmpty(u.Seek_Usage__c) ? 0 : Integer.valueOf(u.Seek_Usage__c);
        u.Seek_Usage__c = String.valueOf(usage + 1);
        fflib_SecurityUtils.checkFieldIsUpdateable(User.SObjectType, User.Seek_Usage__c);
        update u;
        remainingquota = remainingquota -1;
        if (remainingquota <= 0) {
            remaining_quota_enough = false;
        }
    }

    /*
     * Generate job content in xml or JSON format depend on the specific job board
     */
    public override String generateJobContent(Advertisement__c ad, String refNo) {
        return SeekJobPostingUtils.generateJobContent(ad, selectedAccount, refNo, itemFieldMapSelected);
    }



    /****************************************************************/
    /************* abstract method for call web service *************/
    /****************************************************************/
    /*
     * Get ad posting web service endpoint
     */
    public override String getPostingEndpoint() {
        String action = actionType;
        if (actionType.equals('repost') || actionType.equals('cloneToPost')) {
            action = 'post';
        }
        return webServiceEndpoint + '/SeekAdPostWeb/job/' + action + 'JobAd';
    }

    /*
     * Get ad posting request headers
     */
    public override Map<String, String> getHeaders() {
        Map<String, String> headers = new Map<String, String>();
        headers.put('Content-Type', 'application/json');
        return headers;
    }

    /*
     * Generate request body
     */
    public override String generateRequestBody(Advertisement__c adv, String refNo) {
        JobPostingRequest request = new JobPostingRequest();
        request.referenceNo = refNo;
        request.postingStatus = adv.Job_Posting_Status__c;
        if (!actionType.equals('archive')) {
            request.jobContent = adv.JobXML__c;
            request.advertiserId = selectedAccount.Advertisement_Account__c;
        }
        request.jobBoard = jobBoardType;

        if (actionType.equals('update') || actionType.equals('archive')) {
            request.externalAdId = adv.Online_Job_Id__c;
        }
        return JSON.serialize(request);
    }



    /******************************************************/
    /******* abstract method for get select options *******/
    /******************************************************/
    /**
     * Get default list endpoint
     */
    public override String getDefaultListEndpoint() {
        return webServiceEndpoint + '/SeekAdPostWeb/default/list';
    }

    /*
     * Get account list and all account config and generate JSON string for account and account config
     */
    public override String generateAccountsInfo() {
        /************* Get available seek account *************/
        AdvertisementConfigurationSelector adConfigSel = new AdvertisementConfigurationSelector();
        if(String.isNotEmpty(u.Seek_Account__c)){ //check user seek account
            List<String> accountIds = u.Seek_Account__c.split(';');
            jobBoardAccounts = adConfigSel.getActiveAccountByIds(accountIds);
        }
        if (jobBoardAccounts == null || jobBoardAccounts.size() <= 0) {
            return null;
        }

        // Generate map with account Id and jobBoardAccount
        Map<String, JobBoardAccountInfo.JobBoardAccount> accountInfoMap =
                new Map<String, JobBoardAccountInfo.JobBoardAccount>();

        // Generate different select types for different account type
        List<String> mandatory = new List<String> {'Stand Out'};
        List<String> selective = new List<String> {'Classic', 'Stand Out'};
        List<String> never = new List<String> {'Classic'};

        // Store the list of account ids enabled SAE and need to get screen id
        Set<Id> accountEnabledSAE = new Set<Id>();

        for (Advertisement_Configuration__c account : jobBoardAccounts) {
            // store in SAE account list
            if (account.Seek_Application_Export_Enabled__c) {
                accountEnabledSAE.add(account.Id);
            }
            // store in account info map
            JobBoardAccountInfo.JobBoardAccount accountInfo = new JobBoardAccountInfo.JobBoardAccount(account.Id, account.Name, false);
            if (String.isNotEmpty(u.Default_Seek_Account__c) && u.Default_Seek_Account__c.equals(account.Id)) {
                accountInfo.isDefault = true;
            }
            if (account.Account_Type__c.equals('Mandatory')) {
                accountInfo.availableTypes = mandatory;
            } else if (account.Account_Type__c.equals('Selective')){
                accountInfo.availableTypes = selective;
            } else {
                accountInfo.availableTypes = never;
            }
            accountInfoMap.put(account.Id, accountInfo);
        }

        /************* Get JSON String for account and account related object for frontend *************/
        AdvertisementConfigurationDetailSelector adConfigDetailSel = new AdvertisementConfigurationDetailSelector();

        // Get template and logo
        List<Advertisement_Configuration_Detail__c> adConfigDetails =
                adConfigDetailSel.getActiveAdConfigDetailsByAccountIds(accountInfoMap.keySet());
        if (adConfigDetails != null && adConfigDetails.size() > 0) {
            String templateRT = DaoRecordType.adConfigTemplateRT.Id;
            String logoRT = DaoRecordType.adConfigLogoRT.Id;
            for (Advertisement_Configuration_Detail__c adConfigDetail : adConfigDetails) {
                JobBoardAccountInfo.JobBoardAccount account = accountInfoMap.get(adConfigDetail.Advertisement_Configuration__c);
                JobBoardAccountInfo.AccountInfo accountInfo =
                        new JobBoardAccountInfo.AccountInfo(adConfigDetail.Id, adConfigDetail.ID_API_Name__c, adConfigDetail.Name, adConfigDetail.Default__c);
                if (adConfigDetail.RecordTypeId == templateRT) {
                    if (account.templates == null) {
                        account.templates = new List<JobBoardAccountInfo.AccountInfo>{
                                accountInfo
                        };
                    } else {
                        account.templates.add(accountInfo);
                    }
                } else if (adConfigDetail.RecordTypeId == logoRT) {
                    if (account.logos == null) {
                        account.logos = new List<JobBoardAccountInfo.AccountInfo>{
                                accountInfo
                        };
                    } else {
                        account.logos.add(accountInfo);
                    }
                }
            }
        }

        // Get seek screen
        ApplicationQuestionSetSelector apsSelector = new ApplicationQuestionSetSelector();
        if (accountEnabledSAE.size() > 0) {
            List<Application_Question_Set__c> seekScreenList = apsSelector.getActiveSeekScreenByAccountIds(accountEnabledSAE);
            if (seekScreenList != null && seekScreenList.size() > 0) {
                for (Application_Question_Set__c seekScreen : seekScreenList) {
                    JobBoardAccountInfo.JobBoardAccount account = accountInfoMap.get(seekScreen.Advertisement_Account__c);
                    JobBoardAccountInfo.AccountInfo accountInfo =
                            new JobBoardAccountInfo.AccountInfo(seekScreen.Id, seekScreen.ID_API_Name__c, seekScreen.Name, seekScreen.Default__c);
                    if (account.screens == null) {
                        account.screens = new List<JobBoardAccountInfo.AccountInfo>{
                                accountInfo};
                    } else {
                        account.screens.add(accountInfo);
                    }
                }
            }
        }

        List<JobBoardAccountInfo.JobBoardAccount> accountList = new List<JobBoardAccountInfo.JobBoardAccount>();
        for (String accountId : accountInfoMap.keySet()) {
            accountList.add(accountInfoMap.get(accountId));
        }

        return JSON.serialize(new JobBoardAccountInfo(accountList));
    }

    public override void assignTemplateItems() {
        if (String.isEmpty(selectedTemplate)) {
            itemFieldMapSelected = new Map<String, String>();
            templateHasItems = false;
            return;
        }
        Advertisement_Configuration_Detail__c template =
                new AdvertisementConfigurationDetailSelector().getTemplateById(selectedTemplate);
        itemFieldMapSelected = new Map<String, String>();
        templateHasItems = false;
        if (template != null) {
            if (String.isNotEmpty(template.Template_Items__c)) {
                List<String> items = template.Template_Items__c.split(';');
                for (String item : items) {
                    try {
                        itemFieldMapSelected.put(item, itemFieldApiMap.get(item));
                    } catch (Exception e) {
                        String msg = 'Could not find matching field for item ' + item + ' in template ' + template.Name + '.';
                        msgSignalhasmessage = addErrorMessage(msg);
                    }
                }
            }
        } else {
            String msg = 'The template (' + selectedTemplate + ') you have selected is no longer available in your org.';
            msgSignalhasmessage = addErrorMessage(msg);
        }

        if (itemFieldMapSelected != null && itemFieldMapSelected.size() > 0) {
            templateHasItems = true;
        }
    }
}