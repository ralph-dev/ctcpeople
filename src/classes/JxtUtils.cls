public class JxtUtils {

	public static final String ORGNISATION_ID_15 = UserInfo.getOrganizationId().subString(0, 15);
	public static final String NAMESPACE = PeopleCloudHelper.getPackageNamespace();
    
    public static String getFeed(Advertisement__c ad){
    
        //Get enableEmailService from custom setting
        JobPostingSettings__c jobPostingSetting = JobPostingSettings__c.getInstance(UserInfo.getUserId());
        Boolean enableEmailService = jobPostingSetting != null ? jobPostingSetting.Enable_JXT_Email_Service__c : false;
        
	    UserSelector userSelector = new UserSelector();
	    User adOwnerUser = userSelector.getUser(ad.OwnerId);
	    
        JXTJobContent job = new JXTJobContent();
        job.jobAdType = ad.Job_Type__c!=null ?  ad.Job_Type__c : '' ;
        job.referenceNo = ORGNISATION_ID_15 + ':' + ad.Id ;
        job.jobTitle = String.isNotBlank(ad.Job_Title__c) ? ad.Job_Title__c : '' ;
        job.jobUrl = String.isNotBlank(ad.Job_Title__c) ? ad.Job_Title__c : '' ;
        job.shortDescription = String.isNotBlank(ad.JXT_Short_Description__c) ? ad.JXT_Short_Description__c : '' ;
        job.bulletpoints.BulletPoint1 = String.isNotBlank(ad.Bullet_1__c) ? ad.Bullet_1__c : '';
        job.bulletpoints.BulletPoint2 = String.isNotBlank(ad.Bullet_2__c) ? ad.Bullet_2__c : '';
        job.bulletpoints.BulletPoint3 = String.isNotBlank(ad.Bullet_3__c) ? ad.Bullet_3__c : '';
        job.jobFullDescription = String.isNotBlank(ad.Job_Content__c) ? ad.Job_Content__c : '' ;
        job.contactDetails = String.isNotBlank(ad.Job_Contact_Name__c) ? ad.Job_Contact_Name__c : '';
        //This column is not currently available in the platform.
        job.companyName = String.isNotBlank(ad.Company_Name__c) ? ad.Company_Name__c : '';
        //ad owner user's consultant code 
        job.consultantID = adOwnerUser!=null&&adOwnerUser.ConsultantCode__c!=null ? adOwnerUser.ConsultantCode__c : '' ;
        job.publicTransport = String.isNotBlank(ad.Nearest_Transport__c) ? ad.Nearest_Transport__c : '' ;
        job.residentsOnly = ad.Residency_Required__c;
        //
        job.isQualificationsRecognised = ad.IsQualificationsRecognised__c;
        job.showLocationDetails = !ad.Location_Hide__c;
        job.jobTemplateID = String.isNotBlank(ad.TemplateCode__c) ? ad.TemplateCode__c : '';
        //
        job.advertiserJobTemplateLogoID = String.isNotBlank(ad.AdvertiserJobTemplateLogoCode__c) ? ad.AdvertiserJobTemplateLogoCode__c : '';
        //Make it as default 30 days
        //job.expiryDate = ad.Ad_Closing_Date__c!=null ? String.valueof(ad.Ad_Closing_Date__c) : '';
        
        //Categories
        if(ad.ClassificationCode__c!=null) {
            JXTJobContent.Category cate1 = new JXTJobContent.Category();
            cate1.classification = String.isNotBlank(ad.ClassificationCode__c) ? ad.ClassificationCode__c : '';
            cate1.subClassification = String.isNotBlank(ad.SubClassificationCode__c) ? ad.SubClassificationCode__c : '';
            if(cate1!=null) {
                job.categories.category.add(cate1);
            }
        }
        if(ad.Classification2Code__c!=null && String.isNotBlank(ad.Classification2Code__c)) {
            JXTJobContent.Category cate2 = new JXTJobContent.Category();
            cate2.classification = String.isNotBlank(ad.Classification2Code__c) ? ad.Classification2Code__c : '';
            cate2.subClassification = String.isNotBlank(ad.SubClassification2Code__c) ? ad.SubClassification2Code__c : '';
            if(cate2!=null) {
                job.categories.category.add(cate2);
            }
        }
        
        //ListingClassification
        job.listingClassification.workType = String.isNotBlank(ad.WorkTypeCode__c) ? ad.WorkTypeCode__c : '';
        //This column is not currently available in the platform. Set to 0 as defaut
        job.listingClassification.sector = '0';
        //job.listingClassification.sector = String.isNotBlank(ad.SectorCode__c) ? ad.SectorCode__c : '';
        job.listingClassification.streetAddress = String.isNotBlank(ad.Street_Adress__c) ? ad.Street_Adress__c : '';
        job.listingClassification.tags = String.isNotBlank(ad.Search_Tags__c) ? ad.Search_Tags__c : '';
        job.listingClassification.country = String.isNotBlank(ad.CountryCode__c) ? ad.CountryCode__c : '';
        job.listingClassification.location = String.isNotBlank(ad.LocationCode__c) ? ad.LocationCode__c : '';
        job.listingClassification.area = String.isNotBlank(ad.AreaCode__c) ? ad.AreaCode__c : '';
        
        //Salary
        job.salary.salaryType = String.isNotBlank(ad.Salary_Type__c) ? ad.Salary_Type__c : '';
        job.salary.min = String.isNotBlank(ad.SeekSalaryMin__c) ? ad.SeekSalaryMin__c : '';
        job.salary.max = String.isNotBlank(ad.SeekSalaryMax__c) ? ad.SeekSalaryMax__c : '';
        job.salary.additionalText = String.isNotBlank(ad.salary_description__c) ? ad.salary_description__c : '';
        job.salary.showSalaryDetails = !ad.No_salary_information__c;
        
        //ApplicationMethod  
        job.applicationMethod.jobApplicationType = enableEmailService ? 'Default' : 'URL';
        job.applicationMethod.applicationUrl = job.applicationMethod.jobApplicationType=='URL' ? 
        Endpoint.getcpewsEndpoint()+'/peoplecloud/GetApplicationForm?jobRefCode='+ORGNISATION_ID_15+':'+ad.id+'&website=jxt' : '';
        job.applicationMethod.applicationEmail = job.applicationMethod.jobApplicationType=='Default' ?
        ad.Prefer_Email_Address__c : '';
        
        //ReferralFee, This column is not currently available in the platform
        job.referral.hasReferralFee = ad.Has_Referral_Fee__c!=null ? Boolean.valueOf(ad.Has_Referral_Fee__c) : false;
        job.referral.amount = job.referral.hasReferralFee ? String.valueOf(ad.Referral_Fee__c) : '';
        job.referral.referralUrl = IsUrlValid(ad.Terms_And_Conditions_Url__c) ? ad.Terms_And_Conditions_Url__c : '';
        
        return JSON.serialize(job);
    }
    
    public static List<SelectOption> getReferralItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('FALSE','This job has no referral fee'));
        options.add(new SelectOption('TRUE','Include a referral fee to increase your chances of getting the right candidate'));
        return options;
    }
    
    public static Boolean IsUrlValid(String url) {
        if('http://'==url || url==null || url==''){
            return false;
        } else {
            return true;
        }
    }

    public static string getPickListFieldMapJsonString(){
        String result = '';
        if(!Test.isRunningTest()){
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(Endpoint.getJXTDefaultEndpoint()+'/jxtfeedweb/default/list/'+ ORGNISATION_ID_15 +'/jxt/' + NAMESPACE);
            req.setMethod('GET');
            
            HttpResponse res = h.send(req);
            result = res.getBody();
        }else {

        }
        return result;
    }
}