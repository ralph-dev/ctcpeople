/*
   This Apex Class is used as a custom Visualforce controller and provides a way to control users' access to CTC admin Tab. 
*/

public with sharing class UserCTCAdminAccessController {

    User[] Users;
    Document[] ds;
    Document[] newdocs = new Document[]{};
    String bodystr = '<?xml version="1.0"?><pages></pages>';
    User[] userstoupdate = new User[]{};
    public List<User> getUsers(){
        CommonSelector.checkRead(User.SObjectType,'Id, Name, Username, View_CTC_Admin__c, Profile.Name, Manage_Quota__c,Profile.Id,PeopleSearchFile__c, VacancySearchFile__c,ClientSearchFile__c');
        Users = [select Id, Name, Username, View_CTC_Admin__c, Profile.Name, Manage_Quota__c,
        Profile.Id,PeopleSearchFile__c, VacancySearchFile__c, 
        ClientSearchFile__c from User where isActive = true and UserType = 'Standard' order by Profile.Name  ];
        return Users;    
    }
    
    public void setUsers(List<User> us){
        Users = us;
    }
    public boolean is_Successful{get;set;}
    public void init(){
        CommonSelector.checkRead(document.SObjectType,'id , developername, name');
        ds = [select id , developername, name from document where type = 'xml'];
        is_Successful = false;            
    }
    
    public void processuserinfo(User u , String obj){
        String uid = String.valueOf(u.id).substring(0,15);
        String docname = 'Result_' + obj.trim() + '_' + uid;
        boolean flag = false;
        for(document d: ds)
        {
            if(d.developername.trim() ==  docname)
            {
                flag = true;
                break;            
            }
        }
        if(!flag)
        {            
            addDocument(docname, u);            
        }    
    }
    
    public void addDocument(String dn , User uu){        
        Document dd = new Document(Name = dn, FolderId = uu.id, type= 'xml', body = Blob.valueOf(bodystr));
        newdocs.add(dd);
    }
    public PageReference updateUsers(){
        is_Successful = false;
        boolean flag;
        try{
            if(ds != null)
            {
                for(User u: Users)
                {   String uid = String.valueOf(u.id).substring(0,15);
                    flag = false;
                    if(u.PeopleSearchFile__c == null || u.PeopleSearchFile__c == '')
                    {
                        processuserinfo(u, 'Contact');
                        u.PeopleSearchFile__c = 'Result_Contact_' + uid;
                        flag = true;
                    }
                    if(u.VacancySearchFile__c == null || u.VacancySearchFile__c == '')
                    {
                        processuserinfo(u, 'Vacancy');
                        u.VacancySearchFile__c = 'Result_Vacancy_' + uid;
                        flag = true;
                    }
                    if(u.ClientSearchFile__c == null || u.ClientSearchFile__c == '')
                    {
                        processuserinfo(u, 'Client');

                        u.ClientSearchFile__c = 'Result_Client_' + uid;
                        flag = true;
                    }
                    if(flag)
                    {
                        userstoupdate.add(u);
                    }
                    
                
                }
                //check FLS
                List<Schema.SObjectField> docFieldList = new List<Schema.SObjectField>{
                    Document.Name,
                    Document.FolderId,
                    Document.Type,
                    Document.Body
                };
                fflib_SecurityUtils.checkInsert(Document.SObjectType, docFieldList);
                insert newdocs;
                //check FLS
                List<Schema.SObjectField> userFieldList = new List<Schema.SObjectField>{
                    User.PeopleSearchFile__c,
                    User.VacancySearchFile__c,
                    User.ClientSearchFile__c
                };
                fflib_SecurityUtils.checkUpdate(User.SObjectType, userFieldList);
                update userstoupdate;
                is_Successful = true;
            }
            
            
        }catch(DMLException ex){  
         
            ApexPages.addMessages(ex);
            return null;
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
            return null;
        }
        return null;

    }
}