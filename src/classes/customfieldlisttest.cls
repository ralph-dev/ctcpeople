/*
    This is a test code for the class CustomfieldList.
    Assuming that you have already have a fieldset which is 
    in Contact Object. 
    
    The name of fieldset is "RevMarketCandiateFieldSet"; 
    If there is no such fieldset, please manually create one.

*/
@isTest   
public class customfieldlisttest{
static testmethod void testcustomfieldlist(){
	System.runAs(DummyRecordCreator.platformUser) {
		String nsPrefix=PeopleCloudHelper.getPackageNamespace();
        CustomFieldList cufl=new CustomFieldList('Contact','RevMarketCandiateFieldSet');
        cufl.setfieldsets(nsPrefix+'RevMarketCandiateFieldSet');
        cufl.setobjects('Contact');
        System.assertEquals(cufl.getobjects(),'Contact');
        System.assertEquals(cufl.getfieldsets(),nsPrefix+'RevMarketCandiateFieldSet');
        map<String,Schema.FieldSet> fsMap = cufl.getCustomFieldSetMap('Contact');
        System.assertNotEquals(fsMap.size(),0);
		list<Schema.FieldSetMember> field_list = cufl.getCustomFieldList();
		System.assertNotEquals(field_list.size(),0);
	}
        
    }
    
 }