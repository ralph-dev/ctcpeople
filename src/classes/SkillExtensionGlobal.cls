global with sharing class SkillExtensionGlobal {
	
	global SkillExtensionGlobal(Object obj) {}
	
	@RemoteAction
	global static List<Skill_Group__c> searchSkillGroups() {
		List<Skill_Group__c> skillGroupList = new List<Skill_Group__c>();
		skillGroupList = SkillGroups.getSkillGroups();
		return skillGroupList;
	}

	@RemoteAction
    global static String retrieveSkillsJSON(List<String> queryString){//, String skillKeyWords and Name like: '%'+skillKeyWords+'%'
        String skillGroupId = queryString.get(0);
        List<Skill__c> skillsList = SkillSelector.getSkillsForSkillGroup(skillGroupId);
        return JSON.serialize(skillsList);
    }
}