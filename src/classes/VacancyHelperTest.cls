@isTest
private class VacancyHelperTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // TO DO: implement unit test
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        VacancyDTO vacDTO = TestDataFactoryRoster.createVacancyDTO(acc,vac);
        
        List<Placement__c> vacList = new List<Placement__c>();
        vacList.add(vac);
        //test method VacancyHelper.deserializeVacancyDTOJson(String vacDTOJson)
        String vacDTOJsonStr = JSON.serialize(vacDTO);
        List<VacancyDTO> vacDTOListSerialized = VacancyHelper.deserializeVacancyDTOJson(vacDTOJsonStr);
        System.assertNotEquals(vacDTOListSerialized,null);
        
        //test method VacancyHelper.convertVacancyListToVacancyDTO(List<Placement__c> vacList)
        List<VacancyDTO> vacDTOListConverted = VacancyHelper.convertVacancyListToVacancyDTO(vacList);
        System.assertNotEquals(vacDTOListConverted.size(), 0);
        
        //test method VacancyHelper.convertVacancyDTOToVacancy(Placement__c vacancy, VacancyDTO vacDTO)
        Placement__c vacConverted = new Placement__c();
        VacancyHelper.convertVacancyDTOToVacancy(vacConverted,vacDTO);
        System.assertEquals(vacConverted.Id, vacDTO.getVacancyId());
    	}
    }
}