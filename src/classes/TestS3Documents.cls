@isTest
private class TestS3Documents {
	 public static testmethod void S3Documents(){
	 	System.runAs(DummyRecordCreator.platformUser) {
    	Contact con = new Contact(LastName='zhangsan');
		insert con;
    	Web_Document__c newdoc = new Web_Document__c();
    	newdoc.S3_Folder__c = 'test';
    	newdoc.Document_Related_To__c = con.id;
    	newdoc.Name = 'Resume_Andy_Wu1320712844040254513263375868151040.doc';
    	insert newdoc;
    	Blob newS3Doc = S3Documents.getS3Doc(newdoc.S3_Folder__c, newdoc.ObjectKey__c);
    	system.assert(newS3Doc == null);
	 	}
    } 
    
    public static testmethod void CreateDummyDocument(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	MultiAttachmentController newdummydoc = new MultiAttachmentController();
    	//folder localDocFolder = DaoFolder.getFolderByDevName('Quick_Send_Resume_Demo'); 
    	Document tempdoc = new Document();
    	tempdoc.Name = 'test.doc';
    	tempdoc.Body = Blob.valueOf('test');
    	tempdoc.FolderId = userinfo.getUserId();
    	insert tempdoc;
    	newdummydoc.myfiles = new List<document>();		
    	newdummydoc.myfiles.add(tempdoc);
    	newdummydoc.createDummyDoc();
    	system.assert(newdummydoc.optInlocalDocString != null);
    	newdummydoc.addMore();
    	system.assert(newdummydoc.myfiles.size() == 2);    	
    	Document newdoc = DaoDocument.getSendResumeDummyDoc(tempdoc.id);
    	system.assert(newdoc != null);
    	}
    }
    
    public static testmethod void testGetS3LinksForCandidate() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // create data
        Contact con = new Contact(LastName='test');
        insert con;
        Web_Document__c newdoc = DataTestFactory.createWebDocument(con);
        List<Contact> conList=DaoContact.getCandsContactsByIdsSR(new List<String>{con.Id});
    	
    	DocRowRec drr = new DocRowRec(newdoc, new ConRowRec(conList.get(0)), 1);
    	
    	String s3Link = S3Documents.generateS3Link(drr, 10000, 'Candidate');
    	System.assert(true, s3Link.contains('Resume - test'));
    	}
    }
    
    public static testmethod void testGetS3LinksForUser() {
        // create data

		User u = DummyRecordCreator.platformUser;
		System.runAs(u) {
			Web_Document__c newdoc = DataTestFactory.createUserWebDocument();
			DocRowRec drr = new DocRowRec(newdoc, new UsrRowRec(UserSelector.getUsrById(u.Id).get(0)), 1);
			String s3Link = S3Documents.generateS3Link(drr, 10000, 'User');
			System.assert(true, s3Link.contains(UserInfo.getOrganizationName() + ' - Resume'));
		}
    }
}