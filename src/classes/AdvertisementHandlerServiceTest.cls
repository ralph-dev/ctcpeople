/**
 * This is the test class for AdvertisementHandlerService
 *
 * Created by: Lina Wei
 * Created on: 28/03/2017
 *
 */

@isTest
private class AdvertisementHandlerServiceTest {

    static {
        TriggerHelper.disableAllTrigger();
    }

    static testMethod void testArchiveAds() {
        System.runAs(DummyRecordCreator.platformUser) {
            //Create data
            Advertisement__c jxtAd = AdvertisementDummyRecordCreator.generateJXTAdDummyRecord();
            Advertisement__c seekAd = AdvertisementDummyRecordCreator.generateSeekAdDummyRecord();

            Test.startTest();
            AdvertisementHandlerService.archiveAds(new List<Advertisement__c>{
                    jxtAd, seekAd
            });
            Test.stopTest();

            System.assertEquals(1, AdvertisementHandlerService.jobBoardAndRequestsMap.size());
            AdvertisementHandlerService.JobBoardAndRequests jobBoardRequests =
                    AdvertisementHandlerService.jobBoardAndRequestsMap.get(DaoRecordType.seekAdRT.Id);
            System.assertEquals('Seek', jobBoardRequests.jobBoard.jobBoardType);
            System.assertEquals(1, jobBoardRequests.requestBodyList.size());
            JobPostingRequest request = (JobPostingRequest) JSON.deserialize(jobBoardRequests.requestBodyList.get(0), JobPostingRequest.Class);
            String orgId = UserInfo.getOrganizationId().substring(0, 15);
            System.assertEquals(orgId + ':' + seekAd.Id, request.referenceNo);
            System.assertEquals(null, request.jobContent);
            System.assertEquals('SeekAdId', request.externalAdId);
        }
    }

    /**
     * Test method for updateUserUsage, please check AdvertisementHandlerTest
     */
}