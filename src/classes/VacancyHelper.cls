public with sharing class VacancyHelper {
	
	public static List<VacancyDTO> deserializeVacancyDTOJson(String vacDTOJson){
		List<VacancyDTO>  vacDTOList = new List<VacancyDTO>();
    	try{
    		vacDTOList = (List<VacancyDTO>)JSON.deserialize(vacDTOJson,List<VacancyDTO>.class);
    	}catch(Exception e){
    		VacancyDTO vacDTO = (VacancyDTO)JSON.deserialize(vacDTOJson,VacancyDTO.class);
    		vacDTOList.add(vacDTO);
    	}
    	return vacDTOList;
	}
	/**	
	 *	convert Vacancy sObject list to VacancyDTO list
	 *	
	 **/
	public static List<VacancyDTO> convertVacancyListToVacancyDTO(list<Placement__c> vacancyList){
		List<VacancyDTO> vacancyDTOList = new List<VacancyDTO>();
		
		for(Placement__c vacancy : vacancyList){
			VacancyDTO vacDTO = new VacancyDTO();
			vacDTO.setVacancyName(vacancy.Name);
			vacDTO.setVacancyCompanyName(vacancy.company__r.Name);
			//Start Date and End Date will be in milliseconds format when prepopulated on the page
			//Using string.valueOf(), the StartDate and EndDate will be in 'YYYY-MM-DD' (2015-12-01) format
			vacDTO.setVacancyEndDate(string.valueOf(vacancy.End_Date__c));
			vacDTO.setVacancyStartDate(string.valueOf(vacancy.Start_Date__c));
			vacDTO.setVacancyId(vacancy.Id);
			vacDTO.setVacancyCompanyId(vacancy.Company__c);
			vacDTO.setVacancyRateType(vacancy.Rate_Type__c);
			vacDTO.setVacancyCategory(vacancy.Category__c);
			vacDTO.setVacancySpecialty(vacancy.Specialty__c);
			vacDTO.setVacancySeniority(vacancy.Seniority__c);
			vacDTO.setVacancyRecordTypeId(vacancy.RecordTypeId);
			vacDTO.setVacancyCompanyShippingStreet(vacancy.Company__r.ShippingStreet);
			vacDTO.setVacancyCompanyShippingCity(vacancy.Company__r.ShippingCity);
			vacDTO.setVacancyCompanyShippingPostalCode(vacancy.Company__r.ShippingPostalCode);
			vacDTO.setVacancyCompanyShippingState(vacancy.Company__r.ShippingState);
			vacDTO.setVacancyCompanyShippingCountry(vacancy.Company__r.ShippingCountry);
			vacDTO.setVacancyCompanyShippingLatitude(string.valueOf(vacancy.Company__r.ShippingLatitude));
			vacDTO.setVacancyCompanyShippingLongitude(string.valueOf(vacancy.Company__r.ShippingLongitude));	
			vacancyDTOList.add(vacDTO);
		}
		
		return vacancyDTOList;
	}
	
	/**	
	 *	convert VacancyDTO to Vacancy sObject
	 *	else statement is used to update fields like (Company__c) to empty.
	 *  otherwise those fields will not be updated
	 *	
	 **/
	public static void convertVacancyDTOToVacancy(Placement__c vacancy, VacancyDTO vacDTO){
		vacancy.Name = vacDTO.getVacancyName();
		vacancy.Category__c = vacDTO.getVacancyCategory();
		vacancy.Specialty__c = vacDTO.getVacancySpecialty();
		vacancy.Seniority__c = vacDTO.getVacancySeniority();
		vacancy.Rate_Type__c = vacDTO.getVacancyRateType();
		
		if(vacDTO.getVacancyCompanyId() != '' && vacDTO.getVacancyCompanyId() != null){
			vacancy.Company__c = vacDTO.getVacancyCompanyId();
		}else{
			vacancy.Company__c = null;
		}
		
		// moment.js will generate 'Invalid date' sometimes,
		// condition 'vacDTO.getVacancyStartDate() != 'Invalid date''will fix this problem
		if(	vacDTO.getVacancyStartDate() != '' &&  vacDTO.getVacancyStartDate() != null && 
			vacDTO.getVacancyStartDate() != 'Invalid date'){		
			vacancy.Start_Date__c = Date.valueOf(vacDTO.getVacancyStartDate());
		}else{
			vacancy.Start_Date__c = null;
		}
			
		if(	vacDTO.getVacancyEndDate() != '' && vacDTO.getVacancyEndDate() != null && 
			vacDTO.getVacancyEndDate() != 'Invalid date'){
			vacancy.End_Date__c = Date.valueOf(vacDTO.getVacancyEndDate());
		}else{
			vacancy.End_Date__c = null;
		}
		
		if(vacDTO.getVacancyRecordTypeId() != '' && vacDTO.getVacancyRecordTypeId() != null){
			vacancy.RecordTypeId = vacDTO.getVacancyRecordTypeId();
		}
		
		if((String)vacDTO.getVacancyId() != '' && (String)vacDTO.getVacancyId() != null){
			vacancy.Id = vacDTO.getVacancyId();
		}
	}
}