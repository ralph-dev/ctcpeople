/**
 * @Author Jack ZHOU
 * @Date   02 Aug 2016
 * The controller is reference by DefaultSkillGroupPage. To save the default skill group for other function.
 */
 public with sharing class DefaultSkillGroupController {
	public Boolean isSkillSearchEnable {get;set;}
	public Boolean isSkillGroupValid {get; set;}
	public List<SelectOption> skillGroupList {get; set;}
	public List<String> selectedSkillGroupList {get; set;}
	private List<Skill_Group__c> defaultSkillGroup;

	public DefaultSkillGroupController() {

		defaultSkillGroup = new List<Skill_Group__c>();
		isSkillGroupValid = true;
		isSkillSearchEnable = SkillGroups.isSkillParsingEnabled();
		skillGroupList = JobBoardUtils.getSelectSkillGroupList();
		selectedSkillGroupList = getSelectSkillGroupList();
		

		if(!isSkillSearchEnable) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning, 'Skill Parsing function is disabled, Please Contact your Admin!'));
		}else if(skillGroupList.isEmpty() || skillGroupList.size() == 0) {
			isSkillGroupValid = false;
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning, 'Skill Group is not existing, Please add at least one Skill Group to system!'));
		}
	}

	public PageReference updateDefaultSkillGroup() {
		try {
			defaultSkillGroup = new SkillGroupService().updateDefaultSkillGroup(new Set<Skill_Group__c>(defaultSkillGroup), new Set<String>(selectedSkillGroupList));
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Confirm, 'Default Skill Group has been updated!'));
		}catch(Exception e) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning, 'Default Skill Group cannot be updated, Please Cotact your Admin!'));
		}
		
		return null;
	}

	private List<String> getSelectSkillGroupList() {
		List<String> selectedSkillGroupList = new List<String>();
		try {
			defaultSkillGroup = SkillGroups.getDefaultSkillGroups();
			if(defaultSkillGroup!=null && defaultSkillGroup.size()>0) {
				for(Skill_Group__c dsg : defaultSkillGroup) {
				    selectedSkillGroupList.add(dsg.Skill_Group_External_Id__c);
				}
			} else {
				selectedSkillGroupList = new List<String>();
			}
		}catch(Exception e) {
			selectedSkillGroupList = new List<String>();
		}
		return selectedSkillGroupList;
	}
}