@isTest
private class TestTradeMePost{
    
    public static StyleCategory__c createStyleCategory() {
        StyleCategory__c sca = new StyleCategory__c();
        sca.Name='a';
        sca.WebSite__c='Trademe';
        sca.templateActive__c=true;
        sca.ProviderCode__c = 'test';
        sca.CompanyCode__c = 'test';
        sca.OfficeCodes__c = 'test';
        insert sca;    
        return sca;
    }
    
    static testmethod void testPost(){
    	System.runAs(DummyRecordCreator.platformUser) {
 	    RecordType adtemplate=new RecordType();
 		adtemplate=[Select Id, Name from RecordType where DeveloperName = 'Template_Record'];
 		
        StyleCategory__c sca = createStyleCategory();
 		
        Placement__c v = new Placement__c();
        insert v;
        Advertisement__c ad = new Advertisement__c();
        ad.Recordtypeid=adtemplate.id;
        ad.WebSite__c='a';   
        ad.Location__c='a';    
        ad.SearchArea_Label_String__c='a';
        ad.Classification__c='a';   
        ad.Vacancy__c=v.id;     
        ad.SubClassification2__c='a';
        ad.Classification2__c='a';  
        ad.SubClassification_Seek_Label__c='a';  
        ad.WorkType__c='a';
        ad.Reference_No__c='atrademe advertisement';     
        ad.Job_Title__c='a';        
        ad.Company_Name__c='a';
        ad.Template__c='a';     
        ad.Application_Form_Style__c=sca.id; 
        ad.Bullet_1__c='a';     
        ad.Bullet_2__c='a';     
        ad.Bullet_3__c='a';         
        ad.Job_Type__c='a'; 
        ad.Residency_Required__c=true;      
        ad.Search_Tags__c='a';      
        ad.Street_Adress__c='a';
        ad.Nearest_Transport__c='a';        
        ad.Job_Contact_Name__c='a';     
        ad.Job_Contact_Phone__c='12345';
        ad.Location_Hide__c =false;         
        ad.Salary_Type__c='a';          
        ad.SeekSalaryMin__c='123';
        ad.SeekSalaryMax__c='2353';         
        ad.Salary_Description__c='a';   
        ad.No_salary_information__c=false;
        ad.Job_Content__c='a';          
        ad.Has_Referral_Fee__c='a'; 
        ad.Referral_Fee__c=500;
        ad.Terms_And_Conditions_Url__c = Endpoint.getPeoplecloudWebSite()+''; 
        ad.Online_Footer__c='a';
        insert ad;                        
        TradeMePost trademe = new TradeMePost(new ApexPages.StandardController(ad));
        trademe.iec2ws = new EC2WebServiceTestImpl();
        trademe.getItems(); 
        trademe.getPhotoItems(); 
        trademe.getStyleOptions();
        trademe.saveAd();         
        Advertisement__c trademead=new Advertisement__c();
        String trademerecordtypeid=JobBoardUtils.getRecordId('Trade_Me');        
        trademead=[select id, name, recordtypeid, WebSite__c,Placement_Date__c,Provider_Code__c,Company_Name__c from Advertisement__c where Reference_No__c='atrademe advertisement' and recordtypeid =: trademerecordtypeid ];
        system.assertequals(trademead.WebSite__c,'TradeMe');
        system.assertequals(trademead.Placement_Date__c,System.today());
        trademe.insertEc2();
        trademe.getSelectSkillGroupList();
        StyleCategory__c sc = [select ProviderCode__c, CompanyCode__c, OfficeCodes__c from StyleCategory__c where WebSite__c='Trademe' and templateActive__c=true];         
        system.assertequals(trademead.Provider_Code__c,sc.ProviderCode__c);
    	}
    }
}