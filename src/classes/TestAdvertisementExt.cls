@isTest
public with sharing class TestAdvertisementExt {
	public static testMethod void testthiscontroller(){
		System.runAs(DummyRecordCreator.platformUser) {
        RecordType rt = [Select Id from RecordType where DeveloperName = 'Template_Record' limit 1];
        if(rt != null){
	        Advertisement__c a = new Advertisement__c(RecordTypeId=rt.Id, Job_Title__c = 'tst', Job_Content__c ='haha');
	        insert a;
	        ApexPages.currentPage().getParameters().put('id',a.Id);
	        AdvertisementExt thecontroller = new AdvertisementExt();
	        thecontroller.dosave();
	        PageReference temp = thecontroller.dosave();
	        system.assert(temp !=null );
	        PageReference canceltemp = thecontroller.docancel();
	        system.assert(canceltemp !=null );
        }  
		}  
    }
    
}