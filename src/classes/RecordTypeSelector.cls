public with sharing class RecordTypeSelector {

	//private String queryStr='select id, Name, developerName from RecordType ';
	
	public list<RecordType> fetchRecordTypesByName(list<String> developerNames){
		//String queryString=queryStr+' where developerName in : developerNames';
		list<RecordType> recordTypes=new list<RecordType>();
		//recordTypes=database.query(queryString);
		recordTypes = DaoRecordType.getRecordTypesInList(developerNames);
		return recordTypes;
	}
	
	public list<RecordType> fetchRecordTypesByName(String developerName){
        list<String> developerNames=new list<String>();
        developerNames.add(developerName);
		return fetchRecordTypesByName(developerNames);
	}
	
	public list<RecordType> fetchRecordTypeList(){
        List<RecordType> rtList = new List<RecordType>();
        //rtList = Database.query(queryStr);
		rtList = DaoRecordType.getAllRecordTypes();
		return rtList;
	}

	public List<RecordType> fetchRecordTypesByObjectName(String objectname) {
		//String queryString=queryStr+' where sobjecttype =: objectname';
		List<RecordType> rtList = new List<RecordType>();
        //rtList = Database.query(queryString);
		rtList = DaoRecordType.getRecordTypesByGivenObjectType(objectname);
		return rtList;
	}
}