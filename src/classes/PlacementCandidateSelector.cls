public with sharing class PlacementCandidateSelector extends CommonSelector{
	
	final static String CANDIDATEMANAGEMENT_QUERY_FIELDS = 'Id, Candidate__c, Placement__c, Candidate__r.Name, Placement__r.Name, Status__c, Start_Date__c, End_Date__c ';
	
	final static String CANDIDATEMANAGEMENT_QUERY_STRING = 'Select '+CANDIDATEMANAGEMENT_QUERY_FIELDS+' from Placement_Candidate__c';

	public PlacementCandidateSelector() {
		super(Placement_Candidate__c.SObjectType);
	}

	

    public override LIST<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField>{
		    Placement_Candidate__c.Id,
		    Placement_Candidate__c.Name,
		    Placement_Candidate__c.Status__c,
		    Placement_Candidate__c.Start_Date__c,
		    Placement_Candidate__c.End_Date__c
		};
    }

	public List<String> getAllSObjectFieldList(){
		List<String> result = new List<String>();
		Map<String, SObjectField> allFieldsMap = GetfieldsMap.getFieldMap(PeopleCloudHelper.getPackageNamespace()+'Placement_Candidate__c');
		if(allFieldsMap!=null && allFieldsMap.size()>0){
			for(Schema.SObjectField field: allFieldsMap.values()){
				result.add(field.getDescribe().getName());
			}
		}
		return result;
	}

	public Set<String> getScreenCandidateFieldList() {
		return new Set<String> {
				'Id',
				'Name',
				'Status__c',
				'Recruiter_Notes__c',
				'Shortlist__c',
				'Shortlist_Time__c'
		};
	}

	public Set<String> getContactFieldList() {
		return new Set<String> {
		        'Candidate__r.Id',
				'Candidate__r.Name',
				'Candidate__r.Resume__c'
		};
	} 

    // private boolean isAddressCompoundField(String fieldName) {
    //     return fieldName.equalsIgnoreCase('MailingAddress')
    //         || fieldName.equalsIgnoreCase('OtherAddress');
    // }

	public Map<String, Placement_Candidate__c> getCandidateMangementByVacancyIdandContacts(String vacId, Set<String> conIds) {
		Map<String, Placement_Candidate__c> candidateCMMap = new Map<String, Placement_Candidate__c>();
		
		String queryString = generateQueryString(vacId);
		
		checkRead(CANDIDATEMANAGEMENT_QUERY_FIELDS);
		List<Placement_Candidate__c> cms = database.query(queryString);

		for(Placement_Candidate__c cm : cms) {
			candidateCMMap.put(cm.Candidate__c, cm);
		}
		return candidateCMMap;
	}

	public List<Placement_Candidate__c> getCandidateMangementByVacancyId(String vacancyId) {
		SimpleQueryFactory qf = simpleQuery()
				.selectFields(getAllSObjectFieldList())
				.setCondition('Placement__c =: vacancyId');
		return (List<Placement_Candidate__c>) Database.query(qf.toSOQL());
	}

	/**
     * Get the candidate managements as per the providing vacancy Id and customised column selection.
     * @param vacancyId - Id of vacancy.
     * @param selectedColumn - JSON string of user's preference for customised columns.
     * @return - A list of candidate managements.
     * Added by Lina on 22/06/2017
     */
	public List<Placement_Candidate__c> getCandidateMangementByVacancyIdAndSelectedColumn(String vacancyId, String selectedColumn) {
		Set<String> selectedFields = FieldsHelper.getFieldsForScreenCandidate(selectedColumn);
        selectedFields.add('CreatedDate');
		SimpleQueryFactory qf = simpleQuery()
				.selectFields(selectedFields)
				.setCondition('Placement__c =: vacancyId')
				.addOrdering('CreatedDate', fflib_QueryFactory.SortOrder.DESCENDING,true);
		return (List<Placement_Candidate__c>) Database.query(qf.toSOQL());
	}

	public List<Placement_Candidate__c> getCandidateMangementByVacancyIdAndSelectedColumn(String vacancyId, String selectedColumn, List<Id> cmListId) {
		Set<String> selectedFields = FieldsHelper.getFieldsForScreenCandidate(selectedColumn);
        selectedFields.add('CreatedDate');
		SimpleQueryFactory qf = simpleQuery()
				.selectFields(selectedFields)
				.setCondition('Placement__c =: vacancyId And Id in :cmListId')
				.addOrdering('CreatedDate', fflib_QueryFactory.SortOrder.DESCENDING,true);
		return (List<Placement_Candidate__c>) Database.query(qf.toSOQL());
	}

	private String generateQueryString (String vacId) {
		String query  = CANDIDATEMANAGEMENT_QUERY_STRING + ' where Placement__c = \'' + vacId + '\' AND Candidate__c in: conIds';
		return query;
	}

	public List<Placement_Candidate__c> getTop5CandidateManagementByContactId(String contactId) {
	    SimpleQueryFactory qf = simpleQuery();
	    String condition = '';
	    qf.selectFields(getSObjectFieldList());
		qf.selectFields(new List<String>{'Candidate__r.Name', 'Placement__r.Name', 'Company__r.Name'});
		qf.setLimit(5);
		system.debug('String.isNotBlank(contactId)='+ String.isNotBlank(contactId));
		if(String.isNotBlank(contactId)) {
			condition += 'Candidate__c =: contactId';
		}
		qf.setCondition(condition);
		qf.getOrderings().clear();
		qf.addOrdering('CreatedDate', fflib_QueryFactory.SortOrder.DESCENDING,true);
		String soqlStr = qf.toSOQL();
		//system.debug('soqlStr ='+ soqlStr);

		List<Placement_Candidate__c> cms = Database.query(soqlStr);
		return cms;
	}
	
	//get cm by contact ids
	public List<Placement_Candidate__c> getCmByContactIdsAndVacId(Set<Id> candIds, String vacancyid) {
	    SimpleQueryFactory qf = simpleQuery();
	    String condition = '';
	    qf.selectFields(getSObjectFieldList());
		qf.selectFields(new List<String>{'Candidate__r.Name', 'Placement__r.Name', 'Company__r.Name', 'Id', 'Status__c'});
		if(candIds != null && candIds.size()>0) {
			condition += 'Candidate__c IN: candIds And Placement__c =: vacancyid ';
		}
		qf.setCondition(condition);
		qf.getOrderings().clear();
		qf.addOrdering('CreatedDate', fflib_QueryFactory.SortOrder.DESCENDING,true);
		String soqlStr = qf.toSOQL();
		List<Placement_Candidate__c> cms = Database.query(soqlStr);
		return cms;
	}
	
	//get cms by contact id and vacancy id
	public List<Placement_Candidate__c> getCmByCandidateIdAndVacId(String candId, String vacancyid) {
	    SimpleQueryFactory qf = simpleQuery();
	    String condition = '';
	    qf.selectFields(getSObjectFieldList());
		qf.selectFields(new List<String>{'Candidate__r.Name', 'Placement__r.Name', 'Company__r.Name', 'Id', 'Status__c'});
		if(candId != null && candId!='' && vacancyid!=null && vacancyid!='') {
			condition += 'Candidate__c =: candId And Placement__c =: vacancyid ';
		}
		qf.setCondition(condition);
		qf.getOrderings().clear();
		qf.addOrdering('LastModifiedDate', fflib_QueryFactory.SortOrder.DESCENDING,true);
		String soqlStr = qf.toSOQL();
		List<Placement_Candidate__c> cms = Database.query(soqlStr);
		return cms;
	}
	
	//get cms by candidate fistname, lastname, email and vacancyId
	public List<Placement_Candidate__c> getCmByCandidateAndVacId(String candFirstName, String candLastName, String candEmail, String vacancyid) {
	    SimpleQueryFactory qf = simpleQuery();
	    String condition = '';
	    qf.selectFields(getSObjectFieldList());
		qf.selectFields(new List<String>{'Candidate__r.FirstName', 'Candidate__r.LastName', 'Candidate__r.Email', 'Placement__c', 'Id', 'Status__c'});
		condition += 'Candidate__r.FirstName=:candFirstName And Candidate__r.LastName=:candLastName AND Candidate__r.Email=:candEmail AND Placement__c =:vacancyid ';
		qf.setCondition(condition);
		qf.getOrderings().clear();
		qf.addOrdering('LastModifiedDate', fflib_QueryFactory.SortOrder.DESCENDING,true);
		String soqlStr = qf.toSOQL();
		List<Placement_Candidate__c> cms = Database.query(soqlStr);
		return cms;
	}

	//get ranked vacancy candidate workflow
	public List<Placement_Candidate__c> getAllVacancyCandidateWorkflow(String searchColumnString, String sortByColumnApiName, String sortingOrder, List<String> cmIdList){
		SimpleQueryFactory qf = simpleQuery();
		String condition = '';
		String ss = searchcolumnString.deleteWhitespace();
		qf.selectFields(getSObjectFieldList());
		qf.selectFields(new List<String>{'Candidate_Status__c', 'Candidate_Source1__c','Rating__c', 'Recruiter_Notes__c', 'Candidate__c', 'Candidate__r.Id', 'Candidate__r.Name', 'Candidate__r.FirstName', 'Candidate__r.LastName'});
		//add custom col
		qf.selectFields(ss.split(','));
		condition += 'id in :cmIdList';
		qf.setCondition(condition);
		qf.getOrderings().clear();
		if(sortingOrder == null || sortingOrder == ''){
			sortingOrder = 'DESC';
		}
		if(sortByColumnApiName != null && sortByColumnApiName != ''){
			if(sortingOrder == 'DESC'){
				qf.addOrdering(sortByColumnApiName, fflib_QueryFactory.SortOrder.DESCENDING,true);
			}else{
				qf.addOrdering(sortByColumnApiName, fflib_QueryFactory.SortOrder.ASCENDING,true);
			}
			
		}
		String soqlStr = qf.toSOQL();

		List<Placement_Candidate__c> vcwf = Database.query(soqlStr);
		return vcwf;
	}

	public List<Placement_Candidate__c> getAllVacancyCandidateWorkflow(String searchColumnString, String sortByColumnApiName, String sortingOrder, List<Placement_Candidate__c> cmIdList){
		SimpleQueryFactory qf = simpleQuery();
		String condition = '';
		String ss = searchcolumnString.deleteWhitespace();
		qf.selectFields(getSObjectFieldList());
		qf.selectFields(new List<String>{'Candidate_Status__c', 'Candidate_Source1__c','Rating__c', 'Recruiter_Notes__c', 'Candidate__c', 'Candidate__r.Id', 'Candidate__r.Name', 'Candidate__r.FirstName', 'Candidate__r.LastName'});
		//add custom col
		qf.selectFields(ss.split(','));
		condition += 'id in :cmIdList';
		qf.setCondition(condition);
		qf.getOrderings().clear();
		if(sortingOrder == null || sortingOrder == ''){
			sortingOrder = 'DESC';
		}
		if(sortByColumnApiName != null && sortByColumnApiName != ''){
			if(sortingOrder == 'DESC'){
				qf.addOrdering(sortByColumnApiName, fflib_QueryFactory.SortOrder.DESCENDING,true);
			}else{
				qf.addOrdering(sortByColumnApiName, fflib_QueryFactory.SortOrder.ASCENDING,true);
			}
			
		}
		String soqlStr = qf.toSOQL();

		List<Placement_Candidate__c> vcwf = Database.query(soqlStr);
		return vcwf;
	}
}