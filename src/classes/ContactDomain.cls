public with sharing class ContactDomain extends SObjectDomain{
    
    //construct by a list of contacts
    public ContactDomain(List<Contact> records) {
        super(records);
    }
    
    //construct by a Contact
    public ContactDomain(Contact record) {
        super(new List<Contact>{record});
    }
    
    //sort contact list
    //require the field name of the date field
    public List<Contact> sortContactsByDate(String fieldName) {
        List<Contact> ret = new List<Contact>();
        
        List<ContactWrapper> cwList = new List<ContactWrapper>();
        for(SObject c : records) {
            ContactWrapper cw = new ContactWrapper((Contact)c, fieldName);
            cwList.add(cw);
        }
        cwList.sort();
        
        for(ContactWrapper cw : cwList) {
            ret.add(cw.con);
        }
        
        return ret;
    }
    
    // merge contacts have newer date update previous one
    public Contact mergeContacts(Contact baseContact, Set<String> requiredFields, String orderByfieldName) {
        List<Contact> sortedRecords = sortContactsByDate(orderByfieldName);
        Contact ret = baseContact;
        
        for(Contact c : sortedRecords) {
            for(String fn : requiredFields) {
                if(c.get(fn) != null && c.get(fn) != '' && !fn.equals('Skill_Group__c')) {
                    ret.put(fn, c.get(fn));
                } else if(fn.equals('Skill_Group__c')) {
                    ret.put(fn, addNotExistSkillGrouptoOrignalContact((String)ret.get('Skill_Group__c'), (String)c.get(fn)));
                }
            }
        }
        
        return ret;
    }
    
    // iterate all fields for two objects return true if all fields have same value
    // else false
    public static boolean isEqual(Contact c1, Contact c2) {
        List<Schema.DescribeSObjectResult> describeSObjectResult = Schema.describeSObjects(new String[]{'Contact'});
        Schema.DescribeSObjectResult describe = null;
        
        if(describeSObjectResult.size() == 0){
            throw new DomainException('Can not retrive DescribeSObjectResult');
        } else {
            describe = describeSObjectResult.get(0);
            
            //for(String fn : describe.fields.getMap().keySet()){
            //    System.debug(c1.get(fn));
            //}
        }
        
        return true;
    }
    
    //retrive the field name list for a field set
    public static List<String> getFieldSetKeyByName(String name) {
        List<String> ret = new List<String>();
        Map<String, Schema.FieldSet> FsMap = Schema.SObjectType.Contact.fieldSets.getMap();
        
        if(FsMap.get(name) != null){
            for(Schema.FieldSetMember fsm : FsMap.get(name).getFields()){
                ret.add(fsm.getFieldPath());
            }
        }
        
        return ret;
    }
    
    //Add not exist skill group from duplicate contact to original contact. Author by Jack on 24 Jul 2013
	public static String addNotExistSkillGrouptoOrignalContact(String existingContact, String duplicateContact){
		String tempString = '';
		set<String> finalContactSkillGroup = new Set<String>();
		List<String> originalContactSkillGroup = new List<String>();
        List<String> duplicateContactSkillGroup = new List<String>();
        if(existingContact!= null){
       		originalContactSkillGroup = (existingContact).split(',');
        }
        if(duplicateContact!= null){
        	duplicateContactSkillGroup = (duplicateContact).split(',');
        }
        finalContactSkillGroup.addall(originalContactSkillGroup);
        finalContactSkillGroup.addall(duplicateContactSkillGroup);
        tempString = String.valueOf(finalContactSkillGroup);
		tempString = tempString.replaceAll('\\{', '');
		tempString = tempString.replaceAll('\\}', '');
	    tempString = tempString.replace(' ','');
		return tempString;
	}

}