public with sharing class ImportCandidate {
    public String orgid{get; set;}
    public String bucketname {get; set;}
    public String vid{get; set;}
    public String skills {get; set;}
    public boolean skillParsingEnabled {get;set;}
    public List<Skill_Group__c> skillGroupList {get; set;}
    public String JSONSringSkillGroups {get;set;}
    public String serviceEndPoint {get;set;}
    
    private ApexPages.Standardsetcontroller setCon;
    
    public ImportCandidate(ApexPages.StandardSetController setCon){
    	CommonSelector.checkRead(User.sObjectType, 'Id, AmazonS3_Folder__c');
        User u = [select Id, AmazonS3_Folder__c from User where id = :Userinfo.getUserId() limit 1];        
        bucketname = u.AmazonS3_Folder__c;
        orgid = Userinfo.getOrganizationId().substring(0, 15);
        
        //serviceEndPoint = Endpoint.getPeoplecloudWebSite();
        serviceEndPoint = Endpoint.getcpewsEndpoint();
        //system.debug('serviceEndPoint:' + serviceEndPoint);
        
        skillParsingEnabled = isSkillParsingEnabled();
        JSONSringSkillGroups ='null';
        skillGroupList = null;
        if(skillParsingEnabled){
        	skillGroupList = getSkillGroups();
        	JSONSringSkillGroups = JSON.serialize(skillGroupList);		
        }
        //system.debug('!!!!!! JSONSringSkillGroups:' + JSONSringSkillGroups + ' !!!!!!!');
    }
    
    
    public String namespace{
        get{
            return PeopleCloudHelper.getPackageNamespace();
        }
    }
    
    public List<Skill_Group__c> getSkillGroups(){
        List<Skill_Group__c> skillGroupObjList = new List<Skill_Group__c>();
        CommonSelector.checkRead(Skill_Group__c.sObjectType, 'Name,Id, Enabled__c, Default__c, Skill_Group_External_Id__c');
        skillGroupObjList = [select Name,Id, Enabled__c, Default__c, Skill_Group_External_Id__c from Skill_Group__c 
        					 where Enabled__c=true];
        return skillGroupObjList;
    }
    
    public boolean isSkillParsingEnabled(){
        PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
        return cs.Enable_Daxtra_Skill_Parsing__c; 
    }
}