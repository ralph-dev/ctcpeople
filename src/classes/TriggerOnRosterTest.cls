@isTest
private class TriggerOnRosterTest {
	final static String ROSTER_STATUS_FILLED = 'Filled';
	final static String ROSTER_STATUS_PENDING = 'Pending';
    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
       	Account acc = TestDataFactoryRoster.createAccount();
		Contact con = TestDataFactoryRoster.createContact();
	    Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
	    Shift__c shift = TestDataFactoryRoster.createShift(vac);
	    Days_Unavailable__c roster = TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_FILLED);
	    
	    roster.Event_Status__c = ROSTER_STATUS_PENDING;
	    
	    update roster;
        
        Days_Unavailable__c test = [select Event_Status__c from Days_Unavailable__c where Id =: roster.Id];
        
        System.assertEquals(roster.Event_Status__c, test.Event_Status__c);
        
	    roster.Event_Status__c = ROSTER_STATUS_FILLED;
	    
	    update roster;
        
        test = [select Event_Status__c from Days_Unavailable__c where Id =: roster.Id];
        
        System.assertEquals(roster.Event_Status__c, test.Event_Status__c);
	    
	    delete roster;
        
        System.assertEquals(0, [select count() from Days_Unavailable__c where Id =: roster.Id]);
    	}
    }
}