@isTest
private class VacancySearchCriteriaTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	//test VacancySearchCriteria DTO getter and setter method
    	Account acc = TestDataFactoryRoster.createAccount();
        VacancySearchCriteria searchCriteria = TestDataFactoryRoster.createVacancySearchCriteria(acc);
        String vacancyEntered = searchCriteria.getVacancyEntered();
        System.assertEquals(vacancyEntered,'');
        String companyId = searchCriteria.getCompanyId();
        System.assertNotEquals(companyId,'');
    	}
    }
}