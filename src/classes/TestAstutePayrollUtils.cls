@isTest
private class TestAstutePayrollUtils {

	/**
	* modified by andy for security review II
	*/
    static testMethod void unitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // TO DO: implement unit testRecordType APRecordType=new RecordType();
		RecordType APRecordType= DaoRecordType.AstutePayrollAccountRT;// [select id from RecordType where developername='AstutePayrollAccount'];
		
        StyleCategory__c apAccount=new StyleCategory__c();
        apAccount.RecordTypeId=APRecordType.id;
        //apAccount.AstutePayroll_api_key__c='key';
        //apAccount.AstutePayroll_api_password__c='password';
        //apAccount.AstutePayroll_api_username__c='name';
        
        CommonSelector.quickInsert(apAccount);
        CredentialSettingService.insertAstutePayrollCredential(apAccount.id, 'name','password','key');
        
         //assign user with AP account
         User currentuser= (User) new CommonSelector(User.sObjectType)
         						.setParam('userId',Userinfo.getUserId())
         						.getOne('id, AstutePayrollAccount__c',
         							'id =: userId');//[select id, AstutePayrollAccount__c from User where id =: Userinfo.getUserId()];
         
         currentuser.AstutePayrollAccount__c=apAccount.id;
         CommonSelector.quickUpdate( currentuser);
        
        
        StyleCategory__c apTestAccount=AstutePayrollUtils.GetAstutePayrollAccount();
        System.assertEquals(apTestAccount.id, apAccount.id);
        System.assertEquals(AstutePayrollUtils.GetloginSession(),UserInfo.getSessionId() );
        map<String,String> testMap=new map<String,String>();
    	AstutePayrollUtils.BillerContactMap(testMap);
    	AstutePayrollUtils.BillerMap(testMap);
    	AstutePayrollUtils.EmployeeMap(testMap);
    	AstutePayrollUtils.PlacementMap(testMap);
    	String srtQuery=AstutePayrollUtils.ReturnQuery(testMap);
    	System.assertEquals(srtQuery.length()>0, true);
    	
    	Account testAccount=new Account();
    	testAccount.Name='TestAccount';
    	testAccount.BillingCity='Sydney';
    	testAccount.BillingPostalCode='2000';
    	testAccount.BillingState='NSW';
    	testAccount.BillingStreet='15 york Street';
    	CommonSelector.quickInsert( testAccount);
    	
    	Account testAccountDetails = (Account)new CommonSelector(Account.sObjectType)
    								.setParam('aId',testAccount.id)
    								.getOne('id, name,billingPostalcode,billingstate,billingcity,billingstreet,phone',
    								'id=:aId');
    	
    	//Account testAccountDetails=[select id, name,billingPostalcode,billingstate,billingcity,billingstreet,phone
    	//	from Account where id=:testAccount.id];
    	
    	map<String,String> accountMaps=new map<String,String>();
    	AstutePayrollUtils.BillerMap(accountMaps);
    	String jsonstr=AstutePayrollUtils.WriteRequest(accountMaps, testAccountDetails);
    	System.assert(jsonstr.length()>0);  	
    	}
        
    }
    
}