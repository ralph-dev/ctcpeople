/**
 * This class is for generating set of fields for query
 * Currently mainly used for
 *    * People Search
 *    * Screen Candidate
 * Created by Lina
 * Created on 22/06/2017
 */


public with sharing class FieldsHelper {
    // The name used as contact object name for customise columns
    public static final String CONTACTNAME = 'Contact';
    // The name used as candidate management object name for customise columns
    public static final String CMNAME = 'Placement_Candidate__c';
    public static String cmLabel;
    public static String contactLabel;
    public static Map<String, Schema.SObjectField> contactRawFields;
    public static Map<String, Schema.SObjectField> cmRawFields;

    static {
        if (contactRawFields == null) {
            contactRawFields = Contact.SObjectType.getdescribe().fields.getmap();
        }
    }

    //this will return all the fields excluding the unsupported fields
    public static Set<String> getFieldsForPeopleSearch() {
        return getFieldsForPeopleSearch(null);
    }
    
    public static void getLabelNames(){
        List<String> types = new List<String>{CMNAME,CONTACTNAME};
		List<Schema.DescribeSobjectResult> results = Schema.describeSObjects(types);
        if(results!=null && results.size()==2){
        	cmLabel = results.get(0).getLabel();
            contactLabel = results.get(1).getLabel();
        }
	}

    //When selectedColumn is not null the fields will be returned will only be the selected columns
    public static Set<String> getFieldsForPeopleSearch(String selectedColumn) {
        Set<String> output = new Set<String>();
        Map<String, Schema.SObjectField> selectedFields = new Map<String, Schema.SObjectField>();

        //getting selected columns
        if (!String.isEmpty(selectedColumn)) {
            try {
                List<PeopleSearchDTO.FieldColumn> fieldsObj = (List<PeopleSearchDTO.FieldColumn>) JSON.deserialize(selectedColumn, List<PeopleSearchDTO.FieldColumn>.class);

                Set<String> neededFields = new Set<String>{
                        'Id',
                        'RecordTypeId',
                        'Name'
                };
                //get selected field names
                for (PeopleSearchDTO.FieldColumn fObj : fieldsObj) {
                    String fieldName = fObj.Field_api_name;
                    if (!String.isEmpty(fieldName)) {
                        neededFields.add(fieldName);
                    }
                }

                for (String neededFieldName : neededFields) {
                    String fieldName = neededFieldName.toLowerCase();
                    //check if the field is already added (duplicate field)
                    //and check if the field is available in the contact object
                    if (!selectedFields.containsKey(fieldName) && contactRawFields.containsKey(fieldName)) {
                        selectedFields.put(fieldName, contactRawFields.get(fieldName));
                    }
                }
            } catch (System.JSONException jsonError) {
                system.debug(LoggingLevel.ERROR, '\n\nInvalid Selected Column JSON\nWill default in using all fields.\n\n');
            } catch (Exception generalError) {
                system.debug(LoggingLevel.ERROR, '\n\nFailed to get selected columns\nWill default in using all fields.\n\n');
            }
        }

        if (selectedFields == null || selectedFields.isEmpty()) {
            selectedFields = contactRawFields;
        }

        for (Schema.SObjectField f : selectedFields.values()) {
            DescribeFieldResult fieldToUse = f.getDescribe();
            if (fieldToUse.isAccessible() && PeopleSearchHelper.isFieldNotExcluded(fieldToUse)) {
                output.add(fieldToUse.getName());
                if (isReferenceFieldButNotPersonAccountField(fieldToUse) && isReferenceFieldButNotPartnerNetwork(fieldToUse)) {
                    output.add(fieldToUse.getRelationshipName() + '.Name');
                }
            } else {
                system.debug(LoggingLevel.INFO, '\nExcluded: ' + fieldToUse.getName() + '\n');
            }
        }
        return output;
    }

    /**
     * @override
     *
     **/
    public static  Boolean isReferenceFieldButNotPersonAccountField(Schema.DescribeFieldResult dfResult) {
        return (dfResult.getType() == Schema.DisplayType.Reference)
                && !dfResult.getName().endsWithIgnoreCase('__pc') && !dfResult.getName().equalsIgnoreCase('PersonContactId');
    }

    public static Boolean isReferenceFieldButNotPartnerNetwork(Schema.DescribeFieldResult dfResult) {
        return (dfResult.getType() == Schema.DisplayType.Reference)
                && !dfResult.getName().endsWithIgnoreCase('__pc')
                && (
                !dfResult.getRelationshipName().contains('PartnerNetworkConnection')
                        && !dfResult.getRelationshipName().contains('PartnerNetworkRecordConnection')
                        && !dfResult.getRelationshipName().equalsIgnoreCase('ConnectionReceived')
                        && !dfResult.getRelationshipName().equalsIgnoreCase('ConnectionSent')
        );
        //&& !dfResult.getRelationshipName().equalsIgnoreCase('ConnectionReceived') && !dfResult.getRelationshipName().equalsIgnoreCase('ConnectionSent')
    }


    public static Set<String> getFieldsForScreenCandidate(String selectedColumn){
        getLabelNames();
        Set<String> output = new Set<String>();
        PlacementCandidateSelector cmSelector = new PlacementCandidateSelector();

        if (!String.isEmpty(selectedColumn)) {
            try {
                Map<String, List<PeopleSearchDTO.FieldColumn>> objSelectedColumnMap = (Map<String, List<PeopleSearchDTO.FieldColumn>>) JSON.deserialize(selectedColumn, Map<String, List<PeopleSearchDTO.FieldColumn>>.class);

                // Handle selected column for PEOPLE object
                List<PeopleSearchDTO.FieldColumn> fieldColumns = objSelectedColumnMap.get(contactLabel);
                Set<String> contactFields = new Set<String>();
                if (fieldColumns != null && fieldColumns.size() > 0) {
                    contactFields = generateSetOfFieldsByObject(fieldColumns, contactRawFields);
                }
                // Add relationship prefix
                for (String field : contactFields) {
                    output.add('Candidate__r.' + field);
                }
                // Handle selected column for CANDIDATE MANAGEMENT object
                cmRawFields = Placement_Candidate__c.SObjectType.getdescribe().fields.getmap();
                fieldColumns = objSelectedColumnMap.get(cmLabel);
                Set<String> cmFields = new Set<String>();
                if (fieldColumns != null && fieldColumns.size() > 0) {
                    cmFields = generateSetOfFieldsByObject(fieldColumns, cmRawFields);
                }
                output.addAll(cmFields);
            } catch (System.JSONException jsonError) {
                system.debug(LoggingLevel.ERROR, 'Invalid Selected Column JSON\nWill default in using all fields.');
            } catch (Exception generalError) {
                system.debug(LoggingLevel.ERROR, 'Failed to get selected columns\nWill default in using all fields.');
            }
        }
        // Add compulsory contact field list
        output.addAll(cmSelector.getContactFieldList());
        // Add compulsory cm field list
        output.addAll(cmSelector.getScreenCandidateFieldList());
        return output;
    }

    /**
     * Given list of field column, and object raw fields map to generate a set of fields used for query
     * The method will check if the field column selection is valid
     * and if the field is a referenced field, name will also be added as a field for selection
     *
     * @param fieldsColumns        list of selected columns
     * @param rawFields            SObject fields map
     * @return Set<String>         set of fields api name
     */
    public static Set<String> generateSetOfFieldsByObject(List<PeopleSearchDTO.FieldColumn> fieldColumns,
            Map<String, Schema.SObjectField> rawFields){

        Set<String> output = new Set<String>();
        Map<String, Schema.SObjectField> selectedFields = new Map<String, Schema.SObjectField>();

        if (fieldColumns != null && fieldColumns.size() > 0) {
            for (PeopleSearchDTO.FieldColumn fObj : fieldColumns) {
                if (String.isNotEmpty(fObj.Field_api_name)) {
                    String fieldName = fObj.Field_api_name.toLowerCase();
                    if(fieldName =='star_rating__c'){
                        fieldName = 'rating__c';
                    }
                    if (rawFields.containsKey(fieldName)) {
                        selectedFields.put(fieldName, rawFields.get(fieldName));
                    }
                }
            }
        }

        for (Schema.SObjectField f : selectedFields.values()) {
            DescribeFieldResult fieldToUse = f.getDescribe();
            if (fieldToUse.isAccessible()) {
                output.add(fieldToUse.getName());
                if (isReferenceFieldButNotPartnerNetwork(fieldToUse)) {
                    output.add(fieldToUse.getRelationshipName() + '.Name');
                }
            } else {
                system.debug(LoggingLevel.INFO, '\nExcluded: ' + fieldToUse.getName() + '\n');
            }
        }

        return output;
    }
}