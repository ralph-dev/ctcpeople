/**
 * This is the extension class of remote actions for screen candidates
 *
 * Created by: Lina Wei
 * Created on: 18/04/2017
 */

public with sharing class ScreenCandidatesExtension {

    public ScreenCandidatesExtension(){}

    public ScreenCandidatesExtension(Object obj){}

    // Create different records by different recordtype
    public List<Placement_Candidate__c> dummyCMList{
        get{
            List<Placement_Candidate__c> cmlist = new List<Placement_Candidate__c>();
            Schema.DescribeSObjectResult d = Placement_Candidate__c.SobjectType.getDescribe();
            List<Schema.RecordTypeInfo> RTList = d.getRecordTypeInfos();
            for(Schema.RecordTypeInfo rt: RTList){
                if(rt.getName() != 'Master'){
                    Placement_Candidate__c cm = new Placement_Candidate__c(RecordTypeId = rt.getRecordTypeId());
                    cmlist.add(cm);
                }
              
            }
            return cmlist;
        }    
    }


    @RemoteAction
    public static String getScreenCandidatesDisplayColumns(){
        return new ScreenCandidatesService().getScreenCandidatesDisplayColumns();
    }

    @RemoteAction
    public static String saveScreenCandidatesDisplayColumns(String jsonColumn){
        return new ScreenCandidatesService().saveScreenCandidatesDisplayColumn(jsonColumn);
    }

    @RemoteAction
    public static String generateBulkEmailURI(List<String> contactIds) {
        String docBody = JSON.serialize(contactIds);
        Document doc = new DocumentService().createTempDocumentByBodyForBulkEmail(docBody);
        String URI = '/apex/' + PeopleCloudHelper.getPackageNamespace()+ 'bulkemailcandidatelistpage?cjid='+doc.Id
                +'&sourcepage=screenCandidate';
        return URI;
    }
    
    @RemoteAction
    public static String generateBulkEmailURIWithTargetObjectsMap(List<String> contactIds, Map<String,String> targetObjectsMap) {
        String docBody = JSON.serialize(contactIds);
        Document doc = new DocumentService().createTempDocumentByBodyForBulkEmail(docBody);
        ///
        String URI = '/apex/' + PeopleCloudHelper.getPackageNamespace()+ 'bulkemailcandidatelistpage?cjid='+doc.Id
                +'&sourcepage=screenCandidate';
                
        String extraParams = generateTargetObjectsQueryString(targetObjectsMap);
        if(!extraParams.equals('')){
        	URI += '&' +extraParams;
        }
        
        return URI;
    }
    
     private static String generateTargetObjectsQueryString(Map<String,String> targetObjectsMap){
    	String extraParams = '';
        if(targetObjectsMap != null && targetObjectsMap.size() > 0){
        	String namespace = PeopleCloudHelper.getPackageNamespace();
        	for(String name : targetObjectsMap.keySet()){
        		String idvalue = targetObjectsMap.get(name);
        		if(idValue == null){
        			continue;
        		}
        		name = name.trim();
        		idvalue = idvalue.trim();
        		
        		if(name.toLowerCase().endsWith('__c') && namespace != null && !namespace.equals('')){
        			if(name.toLowerCase().startsWith(namespace.toLowerCase())){
        				
        			}else{
        				name = namespace + name;
        			}
        			
        		}
        		
        		String[] param = new String[]{name, idvalue};
        		if(param.size() == 2 ){
        			if(extraParams.equals('')){
        				extraParams = param[0] +'='+ param[1];
        			}else{
        				extraParams += '&' + param[0] +'='+ param[1];
        			}
        		}
        	}
        }
        
        return extraParams;
    }
    
    @RemoteAction
    public static String generateBulkEmailURIWithTargetObjectsList(List<String> contactIds, List<String> objectNameandIds) {
        String docBody = JSON.serialize(contactIds);
        Document doc = new DocumentService().createTempDocumentByBodyForBulkEmail(docBody);
        ///
        String URI = '/apex/' + PeopleCloudHelper.getPackageNamespace()+ 'bulkemailcandidatelistpage?cjid='+doc.Id
                +'&sourcepage=screenCandidate';
        
        if(objectNameandIds != null && objectNameandIds.size() > 0){
        	Map<String,String> targetObjectsMap = new Map<String,String>();
        	for(String nameAndId : objectNameandIds){
        		String[] param = nameAndId.split(':');
        		if(param.size() == 2 ){
        			targetObjectsMap.put(param[0],param[1]);
        		}
        	}
        	String extraParams = generateTargetObjectsQueryString(targetObjectsMap);
	        if(!extraParams.equals('')){
	        	URI += '&' +extraParams;
	        }
        }
        
        return URI;
    }

    @RemoteAction
    public static Map<String,String> getObjectsLabelName(){
        return new ScreenCandidatesService().getObjectsLabelName();
    }

    /**
     *  Provide additional information on the remote action API
     **/
    @RemoteAction
    public static Map<String, RemoteAPIDescriptor.RemoteAction> apiDescriptor(){
        RemoteAPIDescriptor descriptor = new RemoteAPIDescriptor();
        descriptor.description('CRUD & describe remote action for Screen Candidates.');

        descriptor.action('getScreenCandidatesDisplayColumns');
        descriptor.action('saveScreenCandidatesDisplayColumns').param(String.class, 'JSON string of conlumns');
        descriptor.action('generateBulkEmailURI').param(List<String>.class, 'List of contact Ids');
        descriptor.action('generateBulkEmailURIWithTargetObjectsMap').param(List<String>.class, 'List of contact Ids').param(Map<String,String>.class,'Map of target object name and id value');
        descriptor.action('generateBulkEmailURIWithTargetObjectsList').param(List<String>.class, 'List of contact Ids').param(List<String>.class,'List of the string joined with target object name and id value by colon');
        descriptor.action('getObjectsLabelName');

        return descriptor.paramTypesMap;
    }
}