public with sharing class IndeedApplicationQuestionSelector extends CommonSelector {
	
	public IndeedApplicationQuestionSelector(){
		super(Application_Question__c.sObjectType);
	}
    //Query Questions from according to ad config Id
    public list<Application_Question__c> retreiveApplicationQuestions(String questionSetId){
    	
    	checkRead('id,Questions__c,name,Field_Label_Name__c,Required__c,Sequence__c,Condition_Field__c,Condition_Option__c ');
        list<Application_Question__c> applicationQuestions = [select id,Questions__c,name,Field_Label_Name__c,Required__c,Sequence__c,Condition_Field__c,Condition_Option__c from Application_Question__c where Application_Question_Set__c =: questionSetId order by sequence__c];
        return applicationQuestions;
    } 

}