public with sharing class OauthRefreshTokenService {
    
    /*
        @Author Jack
        @param url
        @return HttpResponse    
    */
    public HttpResponse getRequestByAuthorization(String url) {
        Http h = new Http();
        HttpResponse res = new HttpResponse();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(url);
        if(!Test.isRunningTest()) {
            res = h.send(req);
        }else {
            String testBody ='{"id":"https://login.salesforce.com/id/00D90000000xVFTEA2/005900000037muKAAQ","issued_at":"1411901204196","scope":"id custom_permissions full api visualforce web openid refresh_token chatter_api","instance_url":"https://ap1.salesforce.com","token_type":"Bearer","refresh_token":"5Aep8617VFpoP.M.4uTrrOLn5evTFgIcJqrZUMp42vfwzVuL2b2TZSBOG5rQjm5nhU9MuivkFw4ZWXtb7ZrLCas","id_token":"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjE5MCJ9.eyJleHAiOjE0MTE5MDEzMjQsInN1YiI6Imh0dHBzOi8vbG9naW4uc2FsZXNmb3JjZS5jb20vaWQvMDBEOTAwMDAwMDB4VkZURUEyLzAwNTkwMDAwMDAzN211S0FBUSIsImF0X2hhc2giOiJMZWdRUXBjRXBLN255VEQzZ1k3NHJBIiwiYXVkIjoiM01WRzlZNmRfQnRwNHhwN2QxSEVqblVTcEZnXzRZZkd2b2xwWHRsb3JvZFYzOU80UlFsVmNoQ1Z4ZkVDX3F3S0R2eTZPTklYT2RaSVZzSDJtbjRTSSIsImlzcyI6Imh0dHBzOi8vbG9naW4uc2FsZXNmb3JjZS5jb20iLCJpYXQiOjE0MTE5MDEyMDR9.VK6ZkH_lI6yid4LR-aC-lyAkz9UIafxekntB8yY8_-L4AQMoFUD4EhgKm7ho_A-vqq_EyM4ne7AI1P4aBpn-ni6UNXVSI4qmm93nFxlwKbEtq1k7AkbSDwt3zjHxw1ng2pPhYD_SCIyNtm76IfGnmCMF7eglRieps_E_CZJKZzfE1K4TStyj-_1yNI_7LtsTE4gNGlQRlkZFU_U-t-L6gCJ5CPLKpENWUY7nXD-YVVmwNEt2fObK_-WWKYknqkQ4WwyKbcdHC1HUg9bDF19-B_jU0LBpuVLYWQwYN0zVQqZtpmUcvyzx2OteN_xIXPyg3VojXKzCTViK5w8K6Ca2xMm_Zjho0a58E8gTFumLvh1ltcvzyiaW7H27pi4BaMsjAxIlxJF4W-0GYwIOsLNmpkkAMNaVPPbZOUE7gVniDXV4mFsHDEYsrGwT4YE9pcIc6lDVuk8mmk2zRgrCvIl_Do2FL5y8AsGIEjynxDDZwjd3a9RAZsITAt0Q0M85rr2hAbufyINFn7rzUFaB2DcEq-oq_VIGB9KxusJC8xF_zz01x123bzVKIArEK-YysG4r33xA_0-6mDmxuoZJGC-vyhZ8L3964zPA0IzCsWdaiBi1gT6OMC8U30DJRKCPJhxUn8GxFNvI7gTXDNNmxq5KlYl9D75woPn5ePbTEKSqC_c","signature":"A1LKRGzU7hHx1fN/CLEtWk0j/Yuoe6Xsr3u7QXtlcws=","access_token":"00D90000000xVFT!AQgAQIyapBh97ssmLIarc6zBtZwMJ9j5EE1uEWRMKSTZWe24VanzeE5m.FH28lWhYCMZ1H6vKFN5eMLutUqc_rkRxN1F21ZY"}';
            res.setBody(testBody);
            res.setStatusCode(200); 
        }
        return res;
    }
}