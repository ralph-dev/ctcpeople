public with sharing class JxtNZEdit {
    public String short_description_contentTemp {get; set;}
    public String jobContentTemp {get; set;}
    public Advertisement__c ad {get; set;}
    public List<SelectOption> styleOptions = new List<SelectOption>();
    public List<SelectOption> templateOptions = new List<SelectOption>();
    public IEC2WebService iec2ws{get; set;}
    
    //Init Error Message for customer, Add by Jack on 10/09/2014
    public PeopleCloudErrorInfo jxtNZ_Errormesg; 
    public Boolean jxtNZMsgSignalhasmessage{get ;set;} //show error message or not
    
    /*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}    
    /*************End Add skill parsing param********************************************************/
    
    public JxtNZEdit(ApexPages.StandardController stdController){
    	//Init skill parsing param. Add by Jack on 23/05/2013
        enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
        enableSkillGroupList = new List<Skill_Group__c>();
        enableSkillGroupList = SkillGroups.getSkillGroups();
        defaultskillgroups = new List<String>();
        //end skill parsing param.
        
        ad = (Advertisement__c) stdController.getRecord();
        iec2ws = new EC2WebServiceImpl();  
        
        if(ad.Skill_Group_Criteria__c!=null&&ad.Skill_Group_Criteria__c!=''){
        	String defaultskillgroupstring = ad.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
	        defaultskillgroups = defaultskillgroupstring.split(',');
        }
    } 
    
    public PageReference feedXml(){
    	Boolean insertDBSuccess = true; //Status of Insert DB 
    	Integer updateDBSuccess = 1;    //Status of Update DB
        
        try{
        	String jobRefCode=UserInfo.getOrganizationId().subString(0, 15)+':'+String.valueOf(ad.id);
        	CommonSelector.checkRead(User.sObjectType,'AmazonS3_Folder__c');
            User currentUser = [select AmazonS3_Folder__c from User where Id=:UserInfo.getUserId()];
            
            CommonSelector.checkRead(StyleCategory__c.sObjectType,
            	'Id, Header_EC2_File__c, Header_File_Type__c,'
                +'Div_Html_EC2_File__c, Div_Html_File_Type__c, '
                +' Footer_EC2_File__c, Footer_File_Type__c ');
            StyleCategory__c sc = [select Id, Header_EC2_File__c, Header_File_Type__c,
                Div_Html_EC2_File__c, Div_Html_File_Type__c, 
                 Footer_EC2_File__c, Footer_File_Type__c  
                    from StyleCategory__c where id = :ad.Application_Form_Style__c limit 1];
           	
            if(ad.Status__c.equalsignorecase('not valid')|| ad.Status__c.equalsignorecase('clone to post')){//If status is not valid, that means the insert record failed. Need to insert again
            	insertDBSuccess = iec2ws.insertDetailTable(jobRefCode, UserInfo.getOrganizationId().subString(0, 15),
					            String.valueOf(ad.id), currentUser.AmazonS3_Folder__c, JobBoardUtils.blankValue(sc.Header_EC2_File__c), 
					              JobBoardUtils.blankValue(sc.Header_File_Type__c), sc.Div_Html_EC2_File__c,
					                sc.Div_Html_File_Type__c,  JobBoardUtils.blankValue(sc.Footer_EC2_File__c), 
					                 JobBoardUtils.blankValue(sc.Footer_File_Type__c), '', 'jxt_nz', 
					                     JobBoardUtils.blankValue(ad.Job_Title__c));
            }else{ 
				//updateDBSuccess = iec2ws.updateDetailTable(ad.id, sc.Header_EC2_File__c, sc.Header_File_Type__c, 
				//		                 sc.Div_Html_EC2_File__c, sc.Div_Html_File_Type__c, 
				//		                    sc.Footer_EC2_File__c, sc.Footer_File_Type__c, UserInfo.getOrganizationId().subString(0, 15));
                updateDBSuccess = iec2ws.updateDetailTable(ad.id, sc.Header_EC2_File__c, sc.Header_File_Type__c, 
                                         sc.Div_Html_EC2_File__c, sc.Div_Html_File_Type__c, 
                                            sc.Footer_EC2_File__c, sc.Footer_File_Type__c, UserInfo.getOrganizationId().subString(0, 15), JobBoardUtils.blankValue(ad.Job_Title__c));
            }          
            ad.JobXML__c = JxtNZUtils.getFeed(ad);
            
            /**    added by Kevin and Jack for bug fixing    **/
            CommonSelector.checkRead(Advertisement__c.sObjectType, 'id, Status__c, name');
            Advertisement__c currentAd = [select id, Status__c, name from Advertisement__c where id=:ad.id];        
            if(insertDBSuccess == false){
            	ad.Status__c = 'Not Valid';
	    		ad.Job_Posting_Status__c = 'Insert/Update URL Failed';
	    		ad.Application_URL__c = '';
	    		jxtNZMsgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.INSERT_OR_UPDATE_DYNAMIC_APPLICATION_FORM_FAILED);
	    		checkFLS();
                update ad;
				return null;  
            }else if(updateDBSuccess != 1){//if update fail, update posting status to Insert/update URL failed. And show error message
	    		ad.Status__c = 'Active';
	    		ad.Job_Posting_Status__c = 'Insert/Update URL Failed';
	    		jxtNZMsgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.INSERT_OR_UPDATE_DYNAMIC_APPLICATION_FORM_FAILED);
	    		checkFLS();
                update ad;
				return null;  
			}else{
	            if(currentAd.Status__c == 'active'){
	                ad.Job_Posting_Status__c = 'active';
	                ad.Status__c = 'Active';
	            }else if(currentAd.Status__c == 'Clone to Post' || currentAd.Status__c.equalsignorecase('not valid')){
		            ad.Job_Posting_Status__c = 'In Queue'; 
		            ad.Status__c = 'Active';           
	        	}else{
	                ad.Status__c = currentAd.Status__c;
	            }
            }
            /**    End of bug fixing    **/
            
            //ad.Job_Posting_Status__c = 'active';
            //ad.Status__c = 'Active';
            system.debug('before update, ad.status = '+ad.Status__c);
            checkFLS();
            update ad;
            return new ApexPages.Standardcontroller(ad).view();
        }catch(System.Exception e){
            CareerOneDetail.notificationsendout('orgid='+Userinfo.getOrganizationId()+' userid='+UserInfo.getUserId()+' jxt cannot post detail information to EC2 database '+e.getMessage() + e, 'ec2 callout', 'jxtnzedit', 'feedXml()');
        	return null;
        }
    }
    public List<SelectOption> getTemplateOptions(){
        return JobBoardUtils.getTemplateOptions('jxt_nz');
    }
    public List<SelectOption> getStyleOptions(){
        return JobBoardUtils.getStyleOptions('jxt_nz');
    }
    
    public List<SelectOption> getClassificationItems(){
        return JxtNZUtils.getClassifications();
    }
    public List<SelectOption> getClassificationItems2nd(){
        return JxtNZUtils.get2ndClassifications();
    }
    
    public PageReference saveAd(){
    	PageReference returnPage = null;
        ad.JXT_Short_Description__c = short_description_contentTemp;
        ad.Job_Content__c = jobContentTemp; 
        String[] ss = ad.JXTNZ_Location__c.split(',');
       /*
        ad.Location__c = ss[3].trim(); 
        ad.SearchArea_Label_String__c=ss[2].trim();
        */
        
        ad.suburb__c = ss[0].trim(); 
        ad.zipcode__c = ss[3].trim();
        ad.Country__c = ss[2].trim();
        //insert skill group ext id
	    if(enableskillparsing){
	    	String tempString  = String.valueOf(defaultskillgroups);
	        tempString = tempString.replaceAll('\\(', '');
	        tempString = tempString.replaceAll('\\)', '');
	        tempString = tempString.replace(' ','');
	        ad.Skill_Group_Criteria__c = tempString;
	    }
	    //end insert skill group ext id
        returnPage = feedXml();
        return returnPage;
    }  
    public List<SelectOption> getReferralItems() {
        return JxtNZUtils.getReferralItems();
    }
    public List<SelectOption> getWriteLabels() {
         return JxtNZUtils.getWriteLabels();        
    }
    
    //insert the skill group picklist value option
    public List<SelectOption> getSelectSkillGroupList(){
	  	List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
	  	List<SelectOption> displaySelectOption = new List<SelectOption>();
	  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
	  		displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
	  	}
	  	return displaySelectOption;
	}
	
	//Generate Error message, Add by Jack on 10/09/2014
    public static Boolean customerErrorMessage(Integer msgcode){
        PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        Boolean msgSignalhasmessage = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
                ApexPages.addMessage(newMessage);
            }
        return msgSignalhasmessage;               
    }

    private void checkFLS(){
      List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
        Advertisement__c.Reference_No__c,
        Advertisement__c.Job_Title__c,
        Advertisement__c.Company_Name__c,
        Advertisement__c.Job_Contact_Name__c,
        Advertisement__c.White_Label__c,
        Advertisement__c.Template__c,
        Advertisement__c.Application_Form_Style__c,
        Advertisement__c.Skill_Group_Criteria__c,
        Advertisement__c.Bullet_1__c,
        Advertisement__c.Bullet_2__c,
        Advertisement__c.Bullet_3__c,
        Advertisement__c.Hot_Job__c,
        Advertisement__c.Residency_Required__c,
        Advertisement__c.Search_Tags__c,
        Advertisement__c.Street_Adress__c,
        Advertisement__c.Nearest_Transport__c,
        Advertisement__c.Location_Hide__c,
        Advertisement__c.Classification__c,
        Advertisement__c.Classification2__c,
        Advertisement__c.Salary_Type__c,
        Advertisement__c.SeekSalaryMin__c,
        Advertisement__c.SeekSalaryMax__c,
        Advertisement__c.Salary_Description__c,
        Advertisement__c.No_salary_information__c,
        Advertisement__c.Job_Content__c,
        Advertisement__c.Has_Referral_Fee__c,
        Advertisement__c.Referral_Fee__c,
        Advertisement__c.Terms_And_Conditions_Url__c
      };

      fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
      fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    }    
    
}