/**
 * This is the test class for PlacementCandidateSelector
 * 
 * Create by: Lina
 * Created on: 17/06/2016
 * 
 */ 

@isTest
private class PlacementCandidateSelectorTest {
    static Placement__c vacancy;
    static Contact con;
    static PlacementCandidateSelector selector;
    static String SCREENCANDIDATECOLUMN;// = '{\"Candidate Management\":[{\"sortable\":null,\"referenceFieldName\":\"LastModifiedBy\",\"Order\":0,\"isCustomField\":false,\"fromasc\":null,\"fieldtype\":\"REFERENCE\",\"Field_api_name\":\"LastModifiedById\",\"defaultorder\":null,\"ColumnName\":\"Last Modified By\"}],\"People\":[{\"sortable\":null,\"referenceFieldName\":\"Account\",\"Order\":1,\"isCustomField\":false,\"fromasc\":null,\"fieldtype\":\"REFERENCE\",\"Field_api_name\":\"AccountId\",\"defaultorder\":null,\"ColumnName\":\"Account\"},{\"sortable\":null,\"referenceFieldName\":\"CreatedBy\",\"Order\":2,\"isCustomField\":false,\"fromasc\":null,\"fieldtype\":\"REFERENCE\",\"Field_api_name\":\"CreatedById\",\"defaultorder\":null,\"ColumnName\":\"Created By\",\"columnObject\":{\"name\":\"People\"}}]}';

    static {
        TriggerHelper.disableAllTrigger();
        vacancy = DataTestFactory.createSingleVacancy();
        con = DataTestFactory.createSingleContact();
        selector = new PlacementCandidateSelector();
        FieldsHelper.getLabelNames();
        SCREENCANDIDATECOLUMN = '{"'+FieldsHelper.cmLabel+'":[{\"sortable\":null,\"referenceFieldName\":\"LastModifiedBy\",\"Order\":0,\"isCustomField\":false,\"fromasc\":null,\"fieldtype\":\"REFERENCE\",\"Field_api_name\":\"LastModifiedById\",\"defaultorder\":null,\"ColumnName\":\"Last Modified By\"}],"'+FieldsHelper.contactLabel+'":[{\"sortable\":null,\"referenceFieldName\":\"Account\",\"Order\":1,\"isCustomField\":false,\"fromasc\":null,\"fieldtype\":\"REFERENCE\",\"Field_api_name\":\"AccountId\",\"defaultorder\":null,\"ColumnName\":\"Account\"},{\"sortable\":null,\"referenceFieldName\":\"CreatedBy\",\"Order\":2,\"isCustomField\":false,\"fromasc\":null,\"fieldtype\":\"REFERENCE\",\"Field_api_name\":\"CreatedById\",\"defaultorder\":null,\"ColumnName\":\"Created By\",\"columnObject\":{\"name\":\"'+FieldsHelper.contactLabel+'"}}]}';
    }
    
    static testmethod void testGetCandidateMangementByVacancyIdandContacts() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // create data
        List<String> candIds = DataTestFactory.createContactListIds(3);
        List<Placement_Candidate__c> cmList = DataTestFactory.createCandidateManagementListForSameVacancy(vacancy.Id, candIds);
        Set<String> conIds = new Set<String>();
        conIds.addAll(candIds);
        
        // call method
        Map<String, Placement_Candidate__c> candCMMap = selector.getCandidateMangementByVacancyIdandContacts(vacancy.Id, conIds);
        
        // verify
        system.assertEquals(3, candCMMap.keySet().size());
        system.assertEquals(cmList[0].Id, candCMMap.get(candIds[0]).Id);
    	}
        
    }
    
    static testmethod void testGetTop5CandidateManagementByContactId() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // create data
        List<String> vacancyIdList = DataTestFactory.createVacancyListIds(6);
        List<Placement_Candidate__c> cmList = DataTestFactory.createCandidateManagementListForSameCandidate(vacancyIdList, con.Id);
        
        // call method
        List<Placement_Candidate__c> cms = selector.getTop5CandidateManagementByContactId(con.Id);
        
        // verify
        System.assertEquals(5, cms.size());
    	}
    }
    
    static testmethod void getCmByContactIdsAndVacId() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // create data
        List<String> vacancyIdList = DataTestFactory.createVacancyListIds(6);
        List<Placement_Candidate__c> cmList = DataTestFactory.createCandidateManagementListForSameCandidate(vacancyIdList, con.Id);
        
        // call method
        Set<Id> conIdSet = new Set<Id>();
        conIdSet.add(con.Id);
        List<Placement_Candidate__c> cms = selector.getCmByContactIdsAndVacId( conIdSet,vacancyIdList[0]);
        // verify
        System.assertEquals(1, cms.size());
    	}
        
    }
    
    static testmethod void testGetCmByCandidateIdAndVacId() {
    	DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
        System.runAs(DummyRecordCreator.platformUser) {
        
        CandidateManagementDummyRecordCreator cmCreator = new CandidateManagementDummyRecordCreator();
        List<Placement_Candidate__c> cms = cmCreator.generateCandidateManagementDummyRecord(rs);
        
        // call method
        List<Placement_Candidate__c> cmList = selector.getCmByCandidateIdAndVacId(rs.candidates[0].Id, rs.vacancies[0].Id);
        // verify
        System.assertEquals(1, cmList.size());
        }
        
    }
    
    static testmethod void testGetCmByCandidateAndVacId() {
    	DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
    	CandidateManagementDummyRecordCreator cmCreator = new CandidateManagementDummyRecordCreator();
        List<Placement_Candidate__c> cms = cmCreator.generateCandidateManagementDummyRecord(rs);
        
        System.runAs(DummyRecordCreator.platformUser) {
        // call method
        List<Placement_Candidate__c> cmList = selector.getCmByCandidateAndVacId( 'testCanFirstName0', 'testCanLastName0', '0test@test.com', rs.vacancies[0].Id);
        // verify
        System.assertEquals(1, cmList.size());
        }
        
        
    }

    static testmethod void testGetAllVacancyCandidateWorkFlow(){
    	DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
    	CandidateManagementDummyRecordCreator cmCreator = new CandidateManagementDummyRecordCreator();
        List<Placement_Candidate__c> cms = cmCreator.generateCandidateManagementDummyRecord(rs);
    	System.runAs(DummyRecordCreator.platformUser) {
        
        
        List<String> cmIdList = new List<String>();
        for(Placement_Candidate__c c: cms){
            cmIdList.add(c.Id);
        }

        List<Placement_Candidate__c> cmList  = selector.getAllVacancyCandidateWorkflow('Second_Approver__c','Status__c', 'DESC', cmIdList);
        System.assertEquals(10, cmList.size());

        List<Placement_Candidate__c> cmList2 = selector.getAllVacancyCandidateWorkflow('Second_Approver__c','Status__c', 'ACS', cms);
        System.assertEquals(10, cmList2.size());
    	}
    }

    static testMethod void testGetCandidateMangementByVacancyId() {
        List<String> candIds = DataTestFactory.createContactListIds(3);
        List<Placement_Candidate__c> cmList = DataTestFactory.createCandidateManagementListForSameVacancy(vacancy.Id, candIds);
        System.runAs(DummyRecordCreator.platformUser) {
        
        List<Placement_Candidate__c> cms = selector.getCandidateMangementByVacancyId(vacancy.Id);
        System.assertEquals(3, cms.size());
        }
    }

    static testMethod void testGetCandidateMangementByVacancyIdAndSelectedColumn() {
        List<String> candIds = DataTestFactory.createContactListIds(3);
        List<Placement_Candidate__c> cmList = DataTestFactory.createCandidateManagementListForSameVacancy(vacancy.Id, candIds);
        System.runAs(DummyRecordCreator.platformUser) {
        List<Placement_Candidate__c> cms = selector.getCandidateMangementByVacancyIdAndSelectedColumn(
                vacancy.Id, SCREENCANDIDATECOLUMN);
        System.assertEquals(3, cms.size());
        System.assert(cms.get(0).LastModifiedBy.Name != null);
        }
    } 

}