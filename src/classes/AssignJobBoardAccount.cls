/****************************************************
*
*Author by Jack on 28/06/2013
*AssignJobBoardAccount is controller of assignJobBoardAccount visual page
*Admin can assign jobboard account to different user
*
*****************************************************/

public with sharing class AssignJobBoardAccount {
    public List<UserAssignedAccount> userAssignedAccounts { get; set; }
    public List<SelectOption> jobBoardAccountDetails { get; set; }        //generate the Seek Account picklist field
    public List<SelectOption> linkedInjobBoardAccountDetails { get; set; }//generate the LinkedIn Account picklist field
    public List<SelectOption> IndeedJobBoardAccountDetails { get; set; }  //generate the Indeed Account picklist field
    public List<User> currentActiveUsers { get; set; }
    public boolean hiddenupdatebutton { get; set; }
    public String userToChange { get; set; }
    public Map<String, String> seekAccountIdNameMap;
    
    //Init Error Message for customer
    public PeopleCloudErrorInfo errormesg; 
    public Boolean msgSignalhasmessage{get ;set;} //show error message or not
    
    public AssignJobBoardAccount(){
        hiddenupdatebutton = true;
        seekAccountIdNameMap = new Map<String, String>();
        generalJobPostingAccountPicklistValue();

        userAssignedAccounts = new List<UserAssignedAccount>(); 
        currentActiveUsers = DaoUsers.getActiveUsers();
        for(User thisUser : currentActiveUsers){
            if(thisUser.Job_Posting_Email__c == null || thisUser.Job_Posting_Email__c == ''){
                thisUser.Job_Posting_Email__c = thisUser.Email;
            }
            
            List<String> assignedSeekAccounts = new List<String>();
            List<SelectOption> availableSeekAccounts = new List<SelectOption>();
            if( thisUser.Seek_Account__c != null && thisUser.Seek_Account__c != ''){
                assignedSeekAccounts = thisUser.Seek_Account__c.split(';');
            }
            if (assignedSeekAccounts != null && assignedSeekAccounts.size() > 0) {
                availableSeekAccounts.add(new SelectOption('','--Select--'));
                for(String account : assignedSeekAccounts){
                    if (seekAccountIdNameMap.get(account) != null) {
                        availableSeekAccounts.add(new SelectOption(account, seekAccountIdNameMap.get(account)));
                    }
                }
            }
            if (availableSeekAccounts.size() <= 1) {
                availableSeekAccounts = new List<SelectOption>{ new SelectOption('','No Assigned Seek Account!')};
            }
            userAssignedAccounts.add(new UserAssignedAccount(thisUser,assignedSeekAccounts, availableSeekAccounts));
        }
    }
    
    //General PickList value for jobboard account
    public void generalJobPostingAccountPicklistValue(){        
        List<Advertisement_Configuration__c> seekAccountList = new List<Advertisement_Configuration__c>();
        List<StyleCategory__c> linkedInAccountList = new List<StyleCategory__c>();
        List<Advertisement_Configuration__c> IndeedAccountList = new List<Advertisement_Configuration__c>();
        try{
            //
            AdvertisementConfigurationSelector acSelector = new AdvertisementConfigurationSelector();
            seekAccountList = acSelector.getActiveAccountByRecordTypeId( DaoRecordType.seekAccountRT.Id);
            //
            linkedInAccountList = DaoStyleCategory.getLinkedInAccount();
            AdvertisementConfigurationSelector ac = new AdvertisementConfigurationSelector();
            IndeedAccountList = ac.getIndeedAccounts();
        }catch(Exception e){
             msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CANDIDATE_SELECTED);
        }
        
        jobBoardAccountDetails = new List<SelectOption>();
        linkedInjobBoardAccountDetails = new List<SelectOption>();
        IndeedJobBoardAccountDetails = new List<SelectOption>();
        
        if(seekAccountList.size()>0 && seekAccountList != null){
            for(Advertisement_Configuration__c seekAccount : seekAccountList){
                jobBoardAccountDetails.add(new SelectOption(seekAccount.id, seekAccount.Name));
                seekAccountIdNameMap.put(seekAccount.id, seekAccount.Name);
            }
        }
        else{
            jobBoardAccountDetails.add(new SelectOption('','No Active Seek Account!'));
        }
        
        if(linkedInAccountList.size()>0 && linkedInAccountList != null){
            linkedInjobBoardAccountDetails.add(new SelectOption('','--Select--'));
            for(StyleCategory__c linkedInAccount : linkedInAccountList){
                linkedInjobBoardAccountDetails.add(new SelectOption(linkedInAccount.id, linkedInAccount.Name));
            }
        }
        else{
            linkedInjobBoardAccountDetails.add(new SelectOption('','No Active LinkedIn Account!'));
        }

        if(IndeedAccountList.size()>0 && IndeedAccountList != null){
            IndeedJobBoardAccountDetails.add(new SelectOption('','--Select--'));
            for(Advertisement_Configuration__c IndeedAccount : IndeedAccountList){
                IndeedJobBoardAccountDetails.add(new SelectOption(IndeedAccount.id, IndeedAccount.Name));
            }
        }
        else{
            IndeedJobBoardAccountDetails.add(new SelectOption('','No Active Indeed Account!'));
        }
    }
    
    public PageReference updateuserdetails(){
        
        for(UserAssignedAccount uaa : userAssignedAccounts){
            String assignedAccountString = '';
           if( uaa.assignedSeekAccounts != null) {
               for(String a : uaa.assignedSeekAccounts ){
                   if( assignedAccountString == ''){
                       assignedAccountString = a;
                   }else{
                       assignedAccountString += ';'+a;
                   }
               }
           }
           uaa.user.Seek_Account__c = assignedAccountString;
        }
        try{
            if(currentActiveUsers.size()>0){
                fflib_SecurityUtils.checkFieldIsUpdateable(User.SObjectType, User.Job_Posting_Email__c);
                update currentActiveUsers;
            }
        }
        catch(Exception e){
            msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.UPDATE_USER_INFO_ERROR);
        }
        return null;
    }

    public void updateAvailableSeekAccounts() {
        for(UserAssignedAccount u : userAssignedAccounts){
            if (u.user.Id.equals(userToChange)) {
                u.availableSeekAccounts = new List<SelectOption>();
                if (u.assignedSeekAccounts != null && u.assignedSeekAccounts.size() > 0) {
                    u.availableSeekAccounts.add(new SelectOption('', '--Select--'));
                    for (String account : u.assignedSeekAccounts) {
                        if (seekAccountIdNameMap.get(account) != null) {
                            u.availableSeekAccounts.add(new SelectOption(account, seekAccountIdNameMap.get(account)));
                        }
                    }
                } 
                if (u.availableSeekAccounts.size() <= 1) {
                    u.availableSeekAccounts.add(new SelectOption('', 'No Assigned Seek Account!'));
                }
            }
        }
    }
    
    //Generate Error message
    public static Boolean customerErrorMessage(Integer msgcode){
        PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        Boolean msgSignalhasmessage = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
                ApexPages.addMessage(newMessage);
            }
        return msgSignalhasmessage;               
    }
    
    //Send Error Message
    static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){//Send error message
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());    
    }
    
    public with sharing class UserAssignedAccount{
        
        public User user { get; set; }
        public List<String> assignedSeekAccounts { get; set; }
        public List<SelectOption> availableSeekAccounts { get; set; }
        
        public UserAssignedAccount(User u, List<String> accounts, List<SelectOption> availableAccounts){
            user = u;
            assignedSeekAccounts = accounts;
            availableSeekAccounts = availableAccounts;
        }
        
        public List<String> getAssignedSeekAccounts(){
            if( assignedSeekAccounts == null ){
                assignedSeekAccounts = new List<String>();
            }
            return assignedSeekAccounts;
        }
        
        public void addAssignedSeekAccount(String a){
            getAssignedSeekAccounts().add(a);
        }

    }
}