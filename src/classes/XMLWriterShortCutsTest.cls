/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class XMLWriterShortCutsTest {

    static testMethod void myUnitTest() {
		XMLWriterShortCuts xmlShortCut=new XMLWriterShortCuts();
		XMLStreamWriter writer=new XMLStreamWriter();
		xmlShortCut.WriteXML2Elements(writer, 'tagName', 
			'elementName1', 'value', 'elementName2', 'value2');
		xmlShortCut.WriteXML2ElementsAndValues(writer, 'tagName', 
			'elementName1', 'value', 'elementName2', 'value2', 'feedValue');
		xmlShortCut.WriteXML3Elements(writer, 'tagName', 
			'elementName1', 'value', 'elementName2', 'value2', 'elementName3', 'value3');
		xmlShortCut.WriteXML3ElementsAndValue(writer, 'tagName', 
			'elementName1', 'value', 'elementName2', 'value2','elementName3', 'value3', 'feedValue');
		xmlShortCut.WriteXML4Elements(writer, 'tagName', 
			'elementName1', 'value', 'elementName2', 'value2','elementName3', 'value3', 'elementName4',
					 'value4');
		xmlShortCut.WriteXML6Elements(writer, 'tagName', 
			'elementName1', 'value', 'elementName2', 'value2','elementName3', 'value3', 'elementName4',
					 'value4', 'elementName5', 'value5', 'elementName6', 'value6');
		xmlShortCut.WriteXMLChaAndElement(writer, 'tagName', 'elementName', 'value', 'feedValue');
		xmlShortCut.WriteXMLCharacters(writer, 'tagName', 'values');
		xmlShortCut.writeXMLCDataCharacters(writer, 'tagName', 'value');
		xmlShortCut.WriteXMLElement(writer, 'tagName', 'elementName', 'value');
		System.assert(writer.getXmlString()!=null);
					 
		
    }
    
}