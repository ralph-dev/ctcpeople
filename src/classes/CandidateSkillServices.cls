/**/

public with sharing class CandidateSkillServices {
    public candidateskillServices() {
        
    }

    //Search candidate by skills
    public Map<String, candidateDTO> searchCandidatesBySkills(Map<String, String> skillsMap, Map<String, candidateDTO> candidatesBySearchCriteria) {
        //system.debug('skillsMap ='+ skillsMap);
        Map<String, Set<String>> skillsCandidateSetMap = new Map<String, Set<String>>();

        List<Candidate_Skill__c> cSkills = new List<Candidate_Skill__c>();

        CandidateSkillSelector cSkillSelector = new CandidateSkillSelector();

        cSkills = cSkillSelector.getCandidateSkillByCandidates(skillsMap, candidatesBySearchCriteria.keySet());
        //system.debug('cSkills ='+ cSkills);
        if(cSkills!=null&&cSkills.size()>0){
            Set<String> allCandidateSet = new Set<String>();       //keep all candidate set in the skill search
            for(Candidate_Skill__c cSkill : cSkills) {
                allCandidateSet.add(cSkill.Candidate__c);

                Set<String> candidatesSet = new Set<String>();     //keep skill candidate Map value set

                if(skillsCandidateSetMap.containsKey(cSkill.Skill__c)){
                    candidatesSet = skillsCandidateSetMap.get(cSkill.Skill__c);
                }
                
                candidatesSet.add(cSkill.Candidate__c);

                skillsCandidateSetMap.put(cSkill.Skill__c, candidatesSet);
            }
            candidatesBySearchCriteria.keySet().retainAll(allCandidateSet);   //only keep candidate from skill search
            
            Set<String> candidatesResult = new Set<String>();
            for(String skillId : skillsMap.keySet()) {
                Set<String> candidatesCompareResult = new Set<String>();
                if(skillsCandidateSetMap.containsKey(skillId)) {
                    candidatesCompareResult = skillsCandidateSetMap.get(skillId);
                }
                //system.debug('candidatesCompareResult ='+ candidatesCompareResult);
                //system.debug('candidatesResult ='+ candidatesResult);
                if(skillsMap.get(skillId).equalsignorecase('and')) {
                    if(candidatesResult!= null && candidatesResult.size()>0) {
                        candidatesResult.retainAll(candidatesCompareResult);
                    }else {
                        candidatesResult = candidatesCompareResult;
                    }
                }else if(skillsMap.get(skillId).equalsignorecase('not')) {
                    if(candidatesResult!= null && candidatesResult.size()>0) {
                        candidatesResult.removeAll(candidatesCompareResult);
                    }
                }else{
                    if(candidatesResult!= null && candidatesResult.size()>0) {
                        candidatesResult.addAll(candidatesCompareResult);
                    }else {
                        candidatesResult = candidatesCompareResult;
                    }
                }
                //system.debug('candidatesResultAfterCompare ='+ candidatesResult);
            }
            candidatesBySearchCriteria.keySet().retainAll(candidatesResult);
        }else{
            candidatesBySearchCriteria = new Map<String, candidateDTO>();
        }
        return candidatesBySearchCriteria;
    }
}