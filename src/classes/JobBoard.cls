/**
 *
 * This is the interface for Job Posting Function
 *
 * Created by: Lina Wei
 * Created on: 01/03/2017
 *
 */

public abstract with sharing class JobBoard{ 

    /************* Advertisement Params *************/
    public Advertisement__c ad { get; set; } 
    public Advertisement__c newAd { get; set; }
    public String jobBoardType;
    public String actionType { get; set; }
    /************************************************/

    /************* Other Params *************/
    public User u;
    public String orgId;
    public String bucketname;
    public PageReference returnPage { get; set; }
    /****************************************/

    /************* Skill Parsing Params *************/
    public Boolean enableskillparsing { get; set; } //check the skill parsing customer setting status
    public List<String> defaultskillgroups { get; set; } //get the default skill group value
    public string skillparsingstyleclass { get; set; }   //not used by seek, need implement later
    /***********************************************/

    /************* Email Service Params *************/
    public Boolean enableEmailService { get; set; } // check if the user enable email service
    /***********************************************/

    /************* Quota Check Params *************/
    public Integer usage { get; set; }
    public Integer remainingquota { get; set; }
    public boolean remaining_quota_enough { get; set; }
    /***********************************************/

    /************* Error Message Params *************/
    public Boolean show_page { get; set;}
    public PeopleCloudErrorInfo errormesg;
    public Boolean msgSignalhasmessage { get; set; } //show error message or not
    public String msgAfterSubmit { get; set; }
    public Boolean hasError { get; set; }
    public Boolean show_msg { get; set; }
    /***********************************************/

    /************* Account Related Params *************/
    public List<Advertisement_Configuration__c> jobBoardAccounts;
    public Advertisement_Configuration__c selectedAccount;
    public String accountRecordTypeId;
    /***********************************************/

    /************* Endpoint Params *************/
    public String webServiceEndpoint;
    public String postingEndpoint;
    /***********************************************/

    /************* Seek Ad Params *************/
    public Map<String, String> itemFieldMapSelected { get; set; }
    public Boolean templateHasItems { get; set; }
    public String selectedTemplate { get; set; }
    public String jobContentTemp { get; set; }
    public String jobOnlineFooterTemp { get; set; }
    
    public JobBoard() {
        u = new UserSelector().getCurrentUser();
        orgId = UserInfo.getOrganizationId().substring(0, 15);
        webServiceEndpoint = getWebServiceEndpoint();
        initJobBoardVariable();
    }
 
    public JobBoard(Advertisement__c advertisement) {
        ad = advertisement;
        u = new UserSelector().getCurrentUser();
        orgId = UserInfo.getOrganizationId().substring(0, 15);
        show_page = true;
        hasError = false;
        show_msg = false;
        webServiceEndpoint = getWebServiceEndpoint();
        initJobBoardVariable();
    }

    /******************************************************************************************/
    /**************************** Init Method for different Action ****************************/
    /******************************************************************************************/

    // init for posting ad
    public void postInit() {
        actionType = 'post';
        initCommonVariable();
        initOtherVariable();
    }

    // init for update ad
    public void updateInit() {
        if (String.isNotEmpty(ad.Status__c) && ad.Status__c.equalsIgnoreCase('Clone to Post')) {
            actionType = 'cloneToPost';
        } else if (String.isNotEmpty(ad.Job_Posting_Status__c) && ad.Job_Posting_Status__c.equalsIgnoreCase('Posting Failed')) {
            actionType = 'repost';
            checkPostPrivilege();
        } else {
            actionType = 'update';
            checkUpdatePrivilege();
            if (!show_page) {
                return;
            }
        }
        initCommonVariable();
    }

    // init for clone ad
    public void cloneInit() {
        actionType = 'cloneToPost';
        checkPostPrivilege();
    }

    public void archiveInit() {
        actionType = 'archive';
        checkAdStatusForUpdate();
    }

    /*
     * init some generic information, such as skill group and email service setting
     */
    private void initCommonVariable() {
        enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active
        defaultskillgroups = new List<String>();
        if(String.isNotEmpty(ad.Skill_Group_Criteria__c)){
            String defaultskillgroupstring = this.ad.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
            defaultskillgroups = defaultskillgroupstring.split(',');
        }
        checkEmailServiceSetting();
    }

    /************************************************/
    /************* abstract init method *************/
    /************************************************/
    /*
     * init job board specific variables
     */
    protected abstract void initJobBoardVariable();
    /*
     * init other specific variables
     */
    protected abstract void initOtherVariable();

    /*
     * check posting privilege
     */
    protected abstract void checkPostPrivilege();
    /*
     * check updating privilege
     */
    protected abstract void checkUpdatePrivilege();
    /*
     * initialise variables for email service setting
     */
    protected abstract void checkEmailServiceSetting();
    /*
     * get web service endpoint
     */
    public abstract String getWebServiceEndpoint();


    /*************************************************************/
    /************* generic method to check privilege *************/
    /*************************************************************/

    // Check remainingQuota
    protected void checkRemainingQuotaWhenInit() {
        /***************** Check if there is still quota *****************/
        remainingquota = UserInformationCon.checkRemainingQuota(jobBoardType, u);
        if(remainingquota <= 0)
        {
            msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.AD_POST_NO_QUOTA_ERROR);
            show_page = false;
        }
    }

    // Check if account still active for the current user
    protected void checkIsAccountValid() {
        if (!JobBoardUtils.checkUserHasActiveJobBoardAccount(ad.Advertisement_Account__c, accountRecordTypeId)) {
        	msgSignalhasmessage = addErrorMessage('Currently you have no privilege to update this Seek Advertisement!');
            //ApexPages.Message error = new ApexPages.Message(ApexPages.severity.WARNING,'Currently you have no privilege to update this Seek Advertisement!');
            //ApexPages.addMessage(error);
            show_page = false;
        }
    }
    // Check if current user has S3 bucket
    protected void checkS3Bucket() {
        if (u.AmazonS3_Folder__c != null) {
            bucketname = u.AmazonS3_Folder__c.trim();
        } else {
            msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.ERR_S3_ACCOUNT_FAILURE);
            show_page = false;
        }
    }

    // Check if ad status is ok for update
    protected void checkAdStatusForUpdate() {
        if (String.isNotEmpty(ad.Status__c) && ad.Status__c.equals('Active')) {
            if (String.isNotEmpty(ad.Job_Posting_Status__c) &&
                   (ad.Job_Posting_Status__c.equals('Posted Successfully')  ||
                    ad.Job_Posting_Status__c.equals('Updated Successfully') ||
                    ad.Job_Posting_Status__c.equals('Update Failed')        ||
                    ad.Job_Posting_Status__c.equals('Archiving Failed'))) {
                return;
            } 
        }
        show_page = false;
        msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.ADVERTISEMT_AD_STATUS_ERROR);
    }


    /***********************************************************************************************/
    /**************************** Ad post/update/clone/archive Method ******************************/
    /***********************************************************************************************/

    // method to post ad
    public PageReference postAd() {
        // init variables control message after submit
        returnPage = null;
        hasError = false;
        show_msg = true;

        
        try {
        	AdvertisementConfigurationSelector adConfigSel = new AdvertisementConfigurationSelector();
	        selectedAccount = adConfigSel.getActiveAccountByIds(new List<Id>{ad.Advertisement_Account__c}).get(0);
	        checkRemainingQuotaAfterSubmit();
	        if (remainingquota <= 0) {
	            return null;
	        }
	        setAdValue();
	        
            //check FLS
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Advertisement__c.Status__c,
                Advertisement__c.Job_Posting_Status__c,
                Advertisement__c.Ad_Posting_Status_Description__c,
                Advertisement__c.Placement_Date__c,
                Advertisement__c.Online_Ad__c,
                Advertisement__c.Online_Ad_URL__c
            };
            fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
            newAd = ad.clone(false, true);
            insert newAd;
            String refNo = orgId + ':' + String.valueOf(newAd.Id);
            newAd.JobXML__c = generateJobContent(newAd, refNo);
            fflib_SecurityUtils.checkFieldIsUpdateable(Advertisement__c.SObjectType, Advertisement__c.JobXML__c);
            update newAd;
            afterSaveAd();
            callWebService(newAd, refNo);
            returnPage = new ApexPages.StandardController(newAd).view();
        } catch (Exception e) {
            hasError = true;
            
            msgSignalhasmessage = addErrorMessage('Job Post Unsuccessfully! - ' + e.getMessage());
            
            String err_msg = 'Job Post Unsuccessfully!<br/>';
            err_msg += 'Detail: ' + e + '; Message: ' + e.getMessage() + '('+e.getTypeName() +')';
            err_msg += '<br/> advertisement Id: ' + ad.Id +'<br/>';
            notificationsendout(err_msg,'codefail', jobBoardType, 'postAd');
        }
        return null;
    }

    // method to update ad
    public PageReference updateAd() {
        msgAfterSubmit = null;
        
        try {
        	AdvertisementConfigurationSelector adConfigSel = new AdvertisementConfigurationSelector();
	        selectedAccount = adConfigSel.getActiveAccountByIds(new List<Id>{ad.Advertisement_Account__c}).get(0);
	        if (actionType.equals('cloneToPost')) {
	            checkRemainingQuotaAfterSubmit();
	            if (remainingquota <= 0) {
	                return null;
	            }
	        }
	        setAdValue();
	        if (actionType.equals('update')) {
	            ad.Ad_Posting_Status_Description__c = 'Sending update request ...';
	        } else {
	            ad.Ad_Posting_Status_Description__c = '';
	        }
	        String refNo = orgId + ':' + String.valueOf(ad.Id);
	        ad.JobXML__c = generateJobContent(ad, refNo);
	        
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Advertisement__c.Ad_Posting_Status_Description__c,
                Advertisement__c.JobXML__c
            };
            fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
            update ad;
            callWebService(ad, refNo);
            if (actionType.equals('cloneToPost') || actionType.equals('repost')) {
                afterSaveAd();
            }
        } catch (Exception e) {
        	
        	msgSignalhasmessage = addErrorMessage('Job Update Unsuccessfully! - ' + e.getMessage());
        	
            String err_msg = 'Job Update Unsuccessfully!<br/>';
            err_msg += 'Detail: ' + e + '; Message: ' + e.getMessage() + '('+e.getTypeName() +')';
            err_msg += '<br/> advertisement Id: ' + ad.Id +'<br/>';
            notificationsendout(err_msg,'codefail', jobBoardType, 'updateAd');
            
            return null;
        }
        return new ApexPages.StandardController(ad).view();
    }

    // method to clone ad
    public PageReference cloneAd() {
        if (!show_page) {
            return null;
        }
        try{
            Advertisement__c adToClone = new AdvertisementSelector().getAdForSeekClone(ad.Id);
            newAd = adToClone.clone(false, true);
            newAd.Status__c = 'Clone to Post';
            newAd.Job_Posting_Status__c = '';
            newAd.Ad_Posting_Status_Description__c = '';
            newAd.Placement_Date__c = null;
            newAd.Online_Ad__c = '';
            newAd.Online_Ad_URL__c = '';
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Advertisement__c.Status__c,
                Advertisement__c.Job_Posting_Status__c,
                Advertisement__c.Ad_Posting_Status_Description__c,
                Advertisement__c.Placement_Date__c,
                Advertisement__c.Online_Ad__c,
                Advertisement__c.Online_Ad_URL__c
            };
            fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
            insert newAd;
            PageReference postPage = Page.seekEdit;
            postPage.getParameters().put('id', newAd.id);
            return postPage;
        }catch(Exception e){
            show_page = false;
            msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.CLONE_AD_ERROR);
            return null;
        }
    }

    // method to archive ad
    public PageReference archiveAd() {
        ad.Status__c = 'Archived';
        ad.Ad_Posting_Status_Description__c = 'Sending archive request ... ';
        try {
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Advertisement__c.Status__c,
                Advertisement__c.Ad_Posting_Status_Description__c
            };
            fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
            update ad;
            String refNo = orgId + ':' + String.valueOf(ad.Id);
            callWebService(ad, refNo);
        } catch(Exception e) {
            String err_msg = 'Job Archive Unsuccessfully!<br/>';
            err_msg += 'Detail: ' + e + '; Message: ' + e.getMessage() + '('+e.getTypeName() +')';
            err_msg += '<br/> advertisement Id: ' + ad.Id +'<br/>';
            notificationsendout(err_msg,'codefail', jobBoardType, 'updateAd');
        }
        return new ApexPages.StandardController(ad).view();
    }

    /*
     * Check remaining quota before save new ad
     */
    protected void checkRemainingQuotaAfterSubmit() {
        /************* Check remaining quota again *************/
        u = new UserSelector().getCurrentUser();
        remainingquota = UserInformationCon.checkRemainingQuota(jobBoardType, u);
        if (remainingquota > 0) {
            remaining_quota_enough = true;
        } else {
            remaining_quota_enough = false;
            msgAfterSubmit = 'Please request more quota from your Administrator to post an ad (0 quota remaining).';
        }
    }

    /****************************************************************************/
    /************* abstract method for Ad post/update/clone/archive *************/
    /****************************************************************************/
    /*
     * set required value on the ad, such as ad status
     */
    protected abstract void setAdValue();
    /*
     * actions need to do after save new ad
     */ 
    protected abstract void afterSaveAd();
    /*
     * Generate job content in xml or JSON format depend on the specific job board
     */
    protected abstract String generateJobContent(Advertisement__c adv, String refNo);



    /*************************************************************************************************/
    /**************************** Method to call job posting webservice  *****************************/
    /*************************************************************************************************/

    /*
     * call web service to send job posting request
     */
    private void callWebService(Advertisement__c adv, String refNo) {
        String endpoint = getPostingEndpoint();
        Map<String, String> headers = getHeaders();
        String requestBody = generateRequestBody(adv, refNo);
        JobPostingWebService.sendRequest(endpoint, new List<String>{requestBody}, headers);
    }

    public void callWebService(List<String> bodyList) {
        String endpoint = getPostingEndpoint();
        Map<String, String> headers = getHeaders();
        JobPostingWebService.sendRequest(endpoint, bodyList, headers);
    }

    /****************************************************************/
    /************* abstract method for call web service *************/
    /****************************************************************/
    /*
     * Get ad posting web service endpoint
     */
    public abstract String getPostingEndpoint();
    /*
     * Get ad posting request headers
     */
    public abstract Map<String, String> getHeaders();
    /*
     * Generate request body without refNo
     */
    public String generateRequestBody(Advertisement__c adv) {
        String refNo = orgId + ':' + String.valueOf(adv.Id);
        return generateRequestBody(adv, refNo);
    }
    /*
     * Generate request body
     */
    protected abstract String generateRequestBody(Advertisement__c adv, String refNo);


    /********************************************************************************************/
    /************************** method for get required select options **************************/
    /********************************************************************************************/

    /**
     * Get available skill groups select options
     */
    public List<SelectOption> getSkillGroupSelectOptions(){
        return JobBoardUtils.getSelectSkillGroupList();
    }

    /**
     * Get available application form style select options
     * This need to be implement later as seek currently not using this
     */
    public List<SelectOption> getApplicationStyleSelectOptions() {
        return JobBoardUtils.getStyleApplicationFormSelectOption();
    }

    // Get Video Position picklist value as select options
    public List<SelectOption> getVideoPositionSelectOptions() {
        List<SelectOption> selectOptions = new List<SelectOption>();
        selectOptions.add(new SelectOption('Below','Below'));
        selectOptions.add(new SelectOption('Above','Above'));
        return selectOptions;
    }

    /*
     * Get JSON String of default pick list value
     */
    public String getPickListDefaultList() {
        String result = '';
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(getDefaultListEndpoint());
        req.setMethod('GET');
        if (!Test.isRunningTest()) {
            HttpResponse res = h.send(req);
            result = res.getBody();
        } else {
            return getDefaultListEndpoint();
        }
        return result;
    }

    /*
     * return Account and Account Config in JSON String
     */
    public String getAccountsInfo() {
        return generateAccountsInfo();
    }

    /******************************************************/
    /******* abstract method for get select options *******/
    /******************************************************/
    /**
     * Get default list endpoint
     */
    protected abstract String getDefaultListEndpoint();
    /*
     * Get account list and all account config and generate JSON string for account and account config
     */
    protected abstract String generateAccountsInfo();



    /********************************************************************************/
    /********************* Some special method for page control *********************/
    /********************************************************************************/
    // Cancel clone
    public PageReference cancelClone() {
        return new ApexPages.StandardController(ad).view();
    }

    // Cancel Archive
    public PageReference cancelArchive() {
        return new ApexPages.StandardController(ad).view();
    }

    public PageReference backToAdDetailPage(){
        return returnPage;
    }

    /***************************************************************************/
    /************************ method for Error Handling ************************/
    /***************************************************************************/
    /*
     * Send out notification
     */
    public static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
                UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());
    }

    /*
     * Generate Error message
     */
    public static Boolean customerErrorMessage(Integer msgcode){
        PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(msgcode,true);
        Boolean msgSignalhasmessage = errormesg.returnresult;
        for(ApexPages.Message newMessage:errormesg.errorMessage){
            ApexPages.addMessage(newMessage);
        }
        return msgSignalhasmessage;
    }

    /**
     * Generate Error Message
     */
    public static Boolean addErrorMessage(String msg) {
        ApexPages.Message error = new ApexPages.Message(ApexPages.severity.WARNING, msg);
        ApexPages.addMessage(error);
        return true;
    }

    /******************************************************/
    /******* Special method for Seek job posting **********/
    /******************************************************/
    /**
     * Assign items mapping to the itemFieldMapSelected
     */
    public abstract void assignTemplateItems();
}