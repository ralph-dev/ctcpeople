public class JobboardgeneralXML {
	
	//General Seek ad XML
	public static String generalSeekJobXML(Advertisement__c seekAd, String templateId, String screenId, String LogoID, String JobRefCode){
		//Add check the Email posting function
		JobPostingSettings__c userJobPostingSetting = SeekJobPostingSelector.getJobPostingCustomerSetting();
        boolean enablePostingAdWithEmail = userJobPostingSetting.Posting_Seek_Ad_with_User_Email__c;
		
		String urlbase = Endpoint.getcpewsEndpoint()+'/peoplecloud/GetApplicationForm?';
        String footer='';
        if(seekAd.online_footer__c !=null){
            footer = seekAd.online_footer__c;
        }
        String jobXml = '<Job Reference="'+JobRefCode+'" TemplateID="'+ templateId +'" ScreenID="'+ screenId +'"><Title><![CDATA['+seekAd.Job_Title__c+']]>'+
        				'</Title><SearchTitle><![CDATA['+seekAd.Online_Search_Title__c+']]></SearchTitle><Description>'+
        				'<![CDATA['+seekAd.Online_Summary__c+']]></Description><AdDetails><![CDATA['+seekAd.Job_Content__c+'<p>'+footer+'</p>'+']]></AdDetails>';
         
        //If the enablePostingAdWithEmail field is true at custom setting and email address is not null. Add the email to XML
        if(!enablePostingAdWithEmail || seekAd.Prefer_Email_Address__c == null || seekAd.Prefer_Email_Address__c == '') {
        	jobXml += '<ApplicationEmail></ApplicationEmail>';
        }else {
        	jobXml += '<ApplicationEmail>'+seekAd.Prefer_Email_Address__c+'</ApplicationEmail>';
        }
        
        if(seekAd.Seek_Default_Application_Form__c){
            // set the applicaiton url blank
            jobXml += '<ApplicationURL></ApplicationURL>';
        }else{
            jobXml += '<ApplicationURL>'+ urlbase + 'jobRefCode='+ JobRefCode +'&amp;website=seek'+ '</ApplicationURL>';
        }
        
        if(seekAd.Residency_Required__c){
            jobXml += '<ResidentsOnly>Yes</ResidentsOnly>';
        }else{
            jobXml += '<ResidentsOnly>No</ResidentsOnly>';
        }
        
        //general bullet
        jobXml += '<Items><Item Name="Jobtitle">'+'<![CDATA['+seekAd.Job_Title__c+']]></Item><Item Name="Bullet1"><![CDATA['+seekAd.Bullet_1__c+']]>'+'</Item>';
        if(seekAd.Bullet_2__c!=''){
        	jobXml+='<Item Name="Bullet2">'+'<![CDATA['+seekAd.Bullet_2__c+']]></Item>';
        	}
        if(seekAd.Bullet_3__c!=''){
        	jobXml+='<Item Name="Bullet3">'+'<![CDATA['+seekAd.Bullet_3__c+']]></Item>';
        	}
        jobXml += '</Items><Listing MarketSegments="'+seekAd.SeekMarketSegment__c+'">';
        if(seekAd.RealLocation_Seek__c!='0' && seekAd.RealLocation_Seek__c!='' && seekAd.RealLocation_Seek__c!=null){
        	jobXml+='<Classification Name="Location">'+seekAd.RealLocation_Seek__c+'</Classification>';
        	}
        if(seekAd.RealArea_Seek__c!='0' && seekAd.RealArea_Seek__c!='' && seekAd.RealArea_Seek__c!=null){
        	jobXml+='<Classification Name="Area">'+seekAd.RealArea_Seek__c+'</Classification>';
        	}
        if(seekAd.WorkType__c!='0' && seekAd.WorkType__c!='' && seekAd.WorkType__c!=null){
        	jobXml+='<Classification Name="WorkType">'+seekAd.WorkType__c+'</Classification>';
        	}
        if(seekAd.SeekClassification__c!='0' && seekAd.SeekClassification__c!='' && seekAd.SeekClassification__c!=null){
        	jobXml+='<Classification Name="Classification">'+seekAd.SeekClassification__c+'</Classification>';
        	}
        if(seekAd.SeekSubClassification__c!='0' && seekAd.SeekSubClassification__c!='' && seekAd.SeekSubClassification__c!=null){
        	jobXml+='<Classification Name="SubClassification">'+seekAd.SeekSubClassification__c+'</Classification>';
        	}
        if(seekAd.SeekFunction__c!='0' && seekAd.SeekFunction__c!='' && seekAd.SeekFunction__c!=null){
        	jobXml+='<Classification Name="Function">'+seekAd.SeekFunction__c+'</Classification>';
        	}
        jobXml += '</Listing>';
        
        String tempSalaryText = '';
        
        if(seekAd.SeekSalaryText__c!=null && seekAd.SeekSalaryText__c!='')
        {
            tempSalaryText = AdvertisementUtilities.ConvertString(seekAd.SeekSalaryText__c);
        }
        else
        {
            seekAd.SeekSalaryText__c='';
            tempSalaryText = '';
        }
        
        String b_1 = '';
        String b_2 = '';
        String b_3 = '';
        jobXml += '<Salary Type="'+seekAd.SeekSalaryType__c+'" Min="'+seekAd.SeekSalaryMin__c+'" Max="'+seekAd.SeekSalaryMax__c+'" AdditionalText="'+tempSalaryText+'"/>';
        if(seekAd.isStandOut__c){
            if(seekAd.Search_Bullet_Point_1__c!=null && seekAd.Search_Bullet_Point_1__c!=''){
               	b_1 = AdvertisementUtilities.ConvertString(seekAd.Search_Bullet_Point_1__c);
            }
            if(seekAd.Search_Bullet_Point_2__c!=null && seekAd.Search_Bullet_Point_2__c!=''){   
                b_2 = AdvertisementUtilities.ConvertString(seekAd.Search_Bullet_Point_2__c);
            }   
            if(seekAd.Search_Bullet_Point_3__c!=null && seekAd.Search_Bullet_Point_3__c!=''){
               	b_3 = AdvertisementUtilities.ConvertString(seekAd.Search_Bullet_Point_3__c);
            }
            jobXml += '<StandOut IsStandOut="true" LogoID="'+ LogoID +'" Bullet1="'+b_1+'" Bullet2="'+b_2+'" Bullet3="'+b_3+'"/>';
        }else{
            jobXml += '<StandOut IsStandOut="false" LogoID="" Bullet1="" Bullet2="" Bullet3=""/>';
        }
        
        if(seekAd.Video_ID__c!=null && seekAd.Video_ID__c!=''){
            jobXml += '<VideoLinkAd VideoLink="&lt;object width=&quot;480&quot; height=&quot;385&quot;&gt;&lt;param name=&quot;movie&quot; value=&quot;http://www.youtube.com/v/'+seekAd.Video_ID__c+'&amp;hl=en_GB&amp;fs=1&amp;rel=0&quot;&gt;&lt;/param&gt;&lt;param name=&quot;allowFullScreen&quot; value=&quot;true&quot;&gt;&lt;/param&gt;&lt;param name=&quot;allowscriptaccess&quot; value=&quot;always&quot;&gt;&lt;/param&gt;&lt;embed src=&quot;http://www.youtube.com/v/'+seekAd.Video_ID__c+
            			'&amp;hl=en_GB&amp;fs=1&amp;rel=0&quot; type=&quot;application/x-shockwave-flash&quot; allowscriptaccess=&quot;always&quot; allowfullscreen=&quot;true&quot; width=&quot;480&quot; height=&quot;385&quot;&gt;&lt;/embed&gt;&lt;/object&gt;" VideoPosition="2"/>';
        }
        jobXml += '</Job>';        
        //System.debug('jobXml = '+jobXml); 
		return jobXml;
	}
	
	
}