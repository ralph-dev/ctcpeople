@isTest
private class QuotaControllerTest {

	private static testMethod void testSetQuotaRecord() {
		System.runAs(DummyRecordCreator.platformUser) {
	    TriggerHelper.disableAllTrigger();
	    Id recordTypeId = DaoRecordType.jobBoardAccRT.Id;
	    List<StyleCategory__c> scList = new List<StyleCategory__c>();
	    StyleCategory__c sc=new StyleCategory__c();
        sc.Monthly_Quota__c = '100';
        sc.Standard_Quota_Per_User__c = 1000;
        sc.Account_Active__c=true;
        sc.WebSite__c = 'seek';
        sc.RecordtypeId = recordTypeId;
        StyleCategory__c sc2=new StyleCategory__c();
        sc2.Monthly_Quota__c = '100';
        sc2.Standard_Quota_Per_User__c = 1000;
        sc2.Account_Active__c=true;
        sc2.WebSite__c = 'careerone';
        sc2.RecordtypeId = recordTypeId;
        scList.add(sc);
        scList.add(sc2);
        insert scList;
        
        Test.startTest();
        User currentUser = new UserSelector().getCurrentUser();
        currentUser.LinkedIn_Account__c = sc.Id;
        currentUser.Seek_Account__c = sc.Id;
        update currentUser;
        QuotaController qc = new QuotaController();
        qc.showSeek = true;
        qc.showC1 = true;
        qc = new QuotaController();
        qc.companyQuota = scList;
        system.assertNotEquals(null, qc.setQuotaRecord('seek'));
        qc.refresh();
	    qc.saveUserInfo();
	    qc.savestdquota();
	    qc.saveCompanyInfo();
	    Test.stopTest();
		}
	}

}