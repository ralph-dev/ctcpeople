public with sharing class AddCandidatestoVacancyController {
    public ApexPages.StandardSetController thecontroller{set;get;}
    public List<Contact> conts{set;get;}
    public List<Contact> cands{set;get;}
    public String contactfieldname{set;get;}
    public Boolean nocandidateselected{set;get;}
    public String vacancyfieldname{set;get;}
    public List<String> contactfieldlist{set;get;}
    public List<Datatransferclass> contactcolumns{set;get;}
    //public String queryForVacancy{set;get;}
   // public String queryForContact{set;get;}
    public PCAppSwitch__c customSetting{get;set;}
   // public String vacancyqueryprefix{set;get;}
    public String clauseforvacancy{set;get;}
    public String jsoncontactstr{set;get;}
    public String jsonvacancystr{set;get;}
    public boolean switchforpaginator{set;get;}
    public List<Datatransferclass> vacancycolumns{set;get;}
    public List<Placement__c> vacancies{set;get;}
    public String backPage{set;get;}
    public String contJsonStr{set;get;}
    public AddCandidatestoVacancyController(ApexPages.StandardSetController acon){      
        /*****Retrieve fieldset from custom setting**********/
        customSetting=PCAppSwitch__c.getinstance();
         try{
                contactfieldname=customSetting.Contact_fieldset_name__c; 
                 
            }catch(Exception ex){
                contactfieldname=null;
            }
            try{
                vacancyfieldname=customSetting.Vacancy_fieldset_name__c;
                
            }catch(Exception ex){
                vacancyfieldname=null;
         }            
        /******Initiate Data*********/
        vacancycolumns=new List<Datatransferclass>();   
        contactcolumns=new List<Datatransferclass>();
        vacancies=new List<Placement__c>();
        cands=new List<Contact>();
        nocandidateselected=true;       
        List<Contact> conts=new List<Contact>();
        list<Id> contIds=new list<Id>();
        contactfieldlist=new List<String>();   
        String candidatejsonid ='';
        String tempDataId='';
        String returnurls='';
        
        
		//Get url from sourcepage             
        String sourcepage= ApexPages.currentPage().getParameters().get('sourcepage');
        if(sourcepage!=null && sourcepage!=''){
        	if(sourcepage=='googlesearch'){
	        	candidatejsonid = ApexPages.currentPage().getParameters().get('cjid');
	        	contIds = GoogleSearchUtil2.fetchCandidateIdList(candidatejsonid);
	        	//system.debug('contIds =' + contIds);
	        	
        	}else{
        		if(sourcepage=='screencandidate'){
        			tempDataId=ApexPages.currentPage().getParameters().get('tempdata');
        			System.debug('The candidates id is '+tempDataId);
        			if(tempDataId!=null && tempDataId!=''){
        				contIds=getIdList(tempDataId);
        			}
        			
        		}        		        		
        	}
        	returnurls=ApexPages.CurrentPage().getParameters().get('returnurl');
        	returnurls = PeopleCloudHelper.secureUrl(returnurls);
           
        	for(Id idStr: contIds){
	        		Contact con=new Contact(id=idstr);
	        		conts.add(con);
	        		
	        	}
        }else{
        	conts = (List<Contact>)acon.getSelected();
       
        	
        	returnurls=PeopleCloudHelper.makeRetURL(ApexPages.CurrentPage());
        }
       //return urls
       	if(returnurls!=null){
        	returnurls=EncodingUtil.urlDecode(returnurls, 'UTF-8');   
        	backPage = returnurls;
       	}else{
       		
       		backPage = '/';
       		
       	}
     	//No Candidate select error        
        if(conts.size()==0 && !Test.isRunningTest()){
            nocandidateselected=false;
            Integer ERR_NO_ContactSELECTED=27;
            PeopleCloudErrorInfo errormsg=new PeopleCloudErrorInfo(ERR_NO_ContactSELECTED,true);
            return;
        }
        
       //convert candidates to json string and move to page
        contJsonStr='{'; 
        for(Contact cont: conts){
        	contJsonStr=contJsonStr+'"'+cont.id+'" : true,';
        }
        contJsonStr=contJsonStr.substring(0,contJsonStr.length()-1)+'}';
        
        //retrieve fieldsets fields if there exist fieldset
		if(contactfieldname!=null){
			contactcolumns=returnFieldsMap(contactfieldname,'Contact');
		}
		if(vacancyfieldname!=null){
			vacancycolumns=returnFieldsMap(vacancyfieldname,'Placement__c');
		}
		
		//use default if there is no fieldset.        
        if(contactcolumns==null || contactcolumns.size()==0){
        	initCandDefaultFieldsMap();
        }
        if(vacancycolumns==null || vacancycolumns.size()==0){
        	initVacancyFieldsMap();
        }
        
        /**
        * modified by andy for security review II
        */
        //Initiate queries
        //queryForContact='select '+getQueryFromMap(contactcolumns)+' from contact where id in: conts order by LastModifiedDate desc';
        
        //vacancyqueryprefix='select '+getQueryFromMap(vacancycolumns) +' from placement__c';	
        //queryForVacancy=vacancyqueryprefix+' order by LastModifiedDate desc limit 1000';

        //initiate queries  
        //cands=database.query(queryForContact);
        //vacancies=database.query(queryForVacancy);
        
        vacancies = new CommonSelector(placement__c.sObjectType)
			.addOrdering('LastModifiedDate', CommonSelector.SortOrder.DESCENDING)
			.get(getQueryFromMap(vacancycolumns), '',1000);
        
        cands = new CommonSelector(contact.sObjectType)
        	.setParam('conts',conts)
			.addOrdering('LastModifiedDate', CommonSelector.SortOrder.DESCENDING)
			.get(getQueryFromMap(contactcolumns), 'id in: conts');
			
        
    }
    //Write query fields from map
    public String getQueryFromMap(List<Datatransferclass> dataTransferClasses){
    	String fieldsQuery=' id ';
    	for(Datatransferclass dataTransfer: dataTransferClasses){
    		fieldsQuery=fieldsQuery+', '+dataTransfer.getapiname()+' ';
    	}
    	return  fieldsQuery;
    }
    
    public list<Id> getIdList(String tempDataId){
    	Temporary_Data__c tempData=new Temporary_Data__c();
    	list<Id> idList=new list<Id>();
    	try{
    		/**
	        * modified by andy for security review II
	        */
	        tempData = (Temporary_Data__c)new CommonSelector(Temporary_Data__c.sObjectType)
	        			.setParam('tempDataId',tempDataId)
	        			.getOne('Data_Content__c','id =: tempDataId');
	    	//tempData=[select Data_Content__c from  Temporary_Data__c where id =: tempDataId];
	    	String jsonStr=tempData.Data_Content__c;
			jsonStr=jsonStr.replace('[','');
			jsonStr=jsonStr.replace(']','');
			jsonStr=jsonStr.replace('"','');
			//System.debug('jsonStr is '+jsonStr);
			for(String idUnit: jsonStr.split(',')){
				idList.add(idUnit);
				//System.debug('idunit is '+idUnit);
			}	    	
	    	
    	}catch( Exception ex){
    		//none exception 
    		idList=new list<Id>();
    	}
    	
    	return idList;
    	
    }
    public list<Datatransferclass> returnFieldsMap(String fieldsetname,String objectname){
        list<Datatransferclass> fieldsMap=new List<Datatransferclass>();  
        try{
            /*****if the fieldsetname has values that mean the custom setting has been fulfilled.Using the column from the field set********/
            List<Schema.FieldSetMember> fieldsetlist=new List<Schema.FieldSetMember>();         
            fieldsetlist=generatefieldsetmember(objectname,fieldsetname);           
            boolean firsttime=true;
			                    
            for(Schema.FieldSetMember fieldset: fieldsetlist){
                contactfieldlist.add(fieldset.getFieldPath());
           /*store the data in the class Datatransferclass, which is used to transfer through the component**/
                String apiname=fieldset.getFieldPath();
                String labelname=fieldset.getLabel();
                if(apiname=='id'){
                    continue;
                }     
                fieldsMap.add(new Datatransferclass(labelname,apiname));
                
            }
           
        }catch(Exception ex){
           fieldsMap=new List<Datatransferclass>();  
        }

        return fieldsMap;
    }
    // public PageReference backPage(){
    //     return returnpageRef;
    // }
    
    
    public void errormessageforNoContact(){
        Integer ERR_NO_ContactSELECTED=27;
        PeopleCloudErrorInfo errormsg=new PeopleCloudErrorInfo(ERR_NO_ContactSELECTED,true);
    }
    public void errormessageforNoVacancy(){
        Integer ERR_NO_VACANCY_SELECTED=28;
        PeopleCloudErrorInfo errormsg=new PeopleCloudErrorInfo(ERR_NO_VACANCY_SELECTED,true);
        
    }
    
    /****Method dealing with the json string*****/
    public void insertdata(){
        Map<String,Object> contactmap=(Map<String,Object>)JSON.deserializeUntyped(jsoncontactstr);
        Map<String,Object> vacancymap=(Map<String,Object>)JSON.deserializeUntyped(jsonvacancystr);              
        List<String>  contactid=new List<String>();
        List<String>  vacancytid=new List<String>();
        List<Placement_Candidate__c> cms=new List<Placement_Candidate__c>();
        try{            
            for(String key : contactmap.keySet()){
                if((boolean)contactmap.get(key)){
                    contactid.add(key);
                }           
            }
            for(String key : vacancymap.keySet()){
                if((boolean)vacancymap.get(key)){
                    vacancytid.add(key);
                }           
            }
            if(vacancytid.size()>10){
                Integer ERR_TOO_MANY_VACANCY=29;
                PeopleCloudErrorInfo errormsg=new PeopleCloudErrorInfo(ERR_TOO_MANY_VACANCY,true);
                   
                return;
            }
            for(String van: vacancytid){            
                for(String con:contactid){
                    Placement_Candidate__c cm=new Placement_Candidate__c();
                    cm.Placement__c=van;
                    cm.Candidate__c=con;
                    cm.Status__c='New';
                    cms.add(cm);
                }
            }
            if(cms.size()>0){
            	try{
                    //check FLS
                    List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                        Placement_Candidate__c.Placement__c,
                        Placement_Candidate__c.Candidate__c,
                        Placement_Candidate__c.Status__c
                    };
                    fflib_SecurityUtils.checkInsert(Placement_Candidate__c.SObjectType, fieldList);
                    insert cms;
            	}catch(Exception ex){
            		Integer ERR_Delete_Record=37;
            		PeopleCloudErrorInfo Successmessage=new PeopleCloudErrorInfo(ERR_Delete_Record,true);
            		return;
            	}
                Integer CONFIRM_CMS_UPDATE_SUCC=201;
                PeopleCloudErrorInfo Successmessage=new PeopleCloudErrorInfo(CONFIRM_CMS_UPDATE_SUCC,true);
            }           
        }catch(Exception ex){
        	
            List<String> emails=new List<String>();
            emails.add('developers@clicktocloud.com');
            String reportsmessage= '------------------ This email generated by Apex code, please do not reply ----------------<br/><br/>';
            reportsmessage+='Error happened in insertomg candidat emanagement <br/>';
            reportsmessage+='The error is: '+ex+'<br/>';
            sendmail(emails, reportsmessage);
            
        }
    }
    public void sendmail(List<String> email,String htmlbody){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(email);
        mail.setSubject('Add Candidate to vacancy');
        mail.setHtmlBody(htmlbody);
        if(!Test.isRunningTest()){
        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }else{
        	system.debug(mail);
        }
        
    }
    public void onsearch(){
        clauseforvacancy=clauseforvacancy.trim();
        VacancySelector selector = new VacancySelector();
        vacancies = selector.fetchVacancyListBySearch(clauseforvacancy);   
    }
    
    //Method used in getting values from field set
    public List<Schema.FieldSetMember> generatefieldsetmember(String objname,String fieldname){
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sobjType = gd.get(objname);  
        Schema.DescribeSObjectResult describesobject = sobjType.getDescribe();
        Schema.FieldSet fs = describesobject.fieldSets.getMap().get(fieldname);
        List<Schema.FieldSetMember> fielda = fs.getFields();
        gd.clear();
        return fielda;
    }
    public void initCandDefaultFieldsMap(){
    	contactcolumns=new List<Datatransferclass>();  
    	contactcolumns.add(new Datatransferclass('Name','Name'));
    	contactcolumns.add(new Datatransferclass('Email','Email'));
    	contactcolumns.add(new Datatransferclass('Mobile','MobilePhone'));
    	contactcolumns.add(new Datatransferclass('Phone','Phone'));
    	contactcolumns.add(new Datatransferclass('Record Type','RecordTypeID'));
    	contactcolumns.add(new Datatransferclass('Last Modified Date','LastModifiedDate'));
    }
    public void initVacancyFieldsMap(){
    	vacancycolumns=new List<Datatransferclass>();
    	vacancycolumns.add(new Datatransferclass('Name','Name'));
    	vacancycolumns.add(new Datatransferclass('Client','Company__c'));
    	vacancycolumns.add(new Datatransferclass('Stage','Stage__c'));
    	vacancycolumns.add(new Datatransferclass('Record Type','RecordTypeID'));
    	vacancycolumns.add(new Datatransferclass('Owner','OwnerId'));
    	vacancycolumns.add(new Datatransferclass('Last Modified Date','LastModifiedDate'));
    }
}