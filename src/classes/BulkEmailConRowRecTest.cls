@isTest
public class BulkEmailConRowRecTest {
    public static testMethod void myUnitTest(){  
    	System.runAs(DummyRecordCreator.platformUser) {
    		List<Contact> conList = DataTestFactory.createContacts();
	        BulkEmailConRowRec bulkEmail = new BulkEmailConRowRec(conList[0], true);
	        bulkEmail = new BulkEmailConRowRec(conList[0].Id, true);
	        bulkEmail.beConRowRec = conList[1];
	        bulkEmail.selected = false;
	        bulkEmail.ContactID = conList[1].Id;
	        system.assert(!bulkEmail.selected);
	        system.assertEquals(bulkEmail.beConRowRec , conList[1]);
	        system.assertEquals(bulkEmail.ContactID , conList[1].Id);
    	}
        
    }
}