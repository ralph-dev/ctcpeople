/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

/**
 * This is the test class for DaxtraCredentialAdminController
 * Created by: Lina
 * Created Date: 19/05/2016
 **/

@isTest
public class DaxtraCredentialAdminControllerTest {
  //  static testmethod void testUpdateDaxtraCredential() {
        
  //      // Create data
  //      DaxtraCredential__c dc = new DaxtraCredential__c();
  //      dc.Service_Endpoint__c = 'www.test.com';
  //      dc.Username__c = 'username';
  //      dc.Password__c = 'password';
  //      dc.Database__c = 'database';
  //      dc.Name = 'test';
  //      insert dc;
	
		//DaxtraCredential__c dcBefore = DaxtraCredentialSelector.getDaxtraCredential();
		//System.assertEquals('www.test.com', dcBefore.Service_Endpoint__c);
  //      System.assertEquals('username', dcBefore.Username__c);
  //      System.assertEquals('password', dcBefore.Password__c);
  //      System.assertEquals('database', dcBefore.Database__c);
        
  //      DaxtraCredentialAdminController daxtraAdminCon = new DaxtraCredentialAdminController();
  //      daxtraAdminCon.serviceEndpoint = 'www.daxtra.com';
  //      daxtraAdminCon.username = 'a';
  //      daxtraAdminCon.password = 'b';
  //      daxtraAdminCon.databaseName = 'c';
  //      daxtraAdminCon.saveCredential();
  //      List<ApexPages.Message> msgList = ApexPages.getMessages();
  //      for(ApexPages.Message msg :  ApexPages.getMessages()) {
  //          System.assertEquals(ApexPages.Severity.CONFIRM, msg.getSeverity());
  //      }
  //      DaxtraCredential__c dcAfter = DaxtraCredentialSelector.getDaxtraCredential();
  //      System.assertEquals('www.daxtra.com', dcAfter.Service_Endpoint__c);
  //      System.assertEquals('a', dcAfter.Username__c);
  //      System.assertEquals('b', dcAfter.Password__c);
  //      System.assertEquals('c', dcAfter.Database__c);
    
  //  }
    
  //  static testmethod void testInsertDaxtraCredential() {

		//DaxtraCredential__c dcBefore = DaxtraCredentialSelector.getDaxtraCredential();
		//System.assertEquals(null, dcBefore);

  //      DaxtraCredentialAdminController daxtraAdminCon = new DaxtraCredentialAdminController();
  //      daxtraAdminCon.serviceEndpoint = 'www.daxtra.com';
  //      daxtraAdminCon.username = 'a';
  //      daxtraAdminCon.password = 'b';
  //      daxtraAdminCon.databaseName = 'c';
  //      daxtraAdminCon.saveCredential();
  //      List<ApexPages.Message> msgList = ApexPages.getMessages();
  //      for(ApexPages.Message msg :  ApexPages.getMessages()) {
  //          System.assertEquals(ApexPages.Severity.CONFIRM, msg.getSeverity());
  //      }
  //      DaxtraCredential__c dcAfter = DaxtraCredentialSelector.getDaxtraCredential();
  //      System.assertEquals('www.daxtra.com', dcAfter.Service_Endpoint__c);
  //      System.assertEquals('a', dcAfter.Username__c);
  //      System.assertEquals('b', dcAfter.Password__c);
  //      System.assertEquals('c', dcAfter.Database__c);
    
  //  }
  //  static testmethod void testInvalidInput() {
  //      DaxtraCredentialAdminController daxtraAdminCon = new DaxtraCredentialAdminController();
  //      daxtraAdminCon.saveCredential();
  //      List<ApexPages.Message> msgList = ApexPages.getMessages();
  //      for(ApexPages.Message msg :  ApexPages.getMessages()) {
  //          System.assertEquals('Please enter values for ALL FIELD!', msg.getSummary());
  //          System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
  //      }
  //  }
    
}