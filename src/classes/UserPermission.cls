/*
    *Author Lorena
    *The class is used to for User configuration
*/

public with sharing class UserPermission {
    public Boolean skillParsingEnabled;
    public Boolean isValidCloudStorageAccount;
    public Boolean isBulkEmailEnabled;
    public Boolean isBulkSMSEnabled;
    public Boolean isScreenCandidateNoteEnabled;
    public Integer numOfRecordsShowing;
    
    public UserPermission() {
        skillParsingEnabled = SkillGroups.isSkillParsingEnabled();
        isValidCloudStorageAccount = ActivityDocControllerHelper.isValidCloudStorageAccount();
        PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
        isBulkEmailEnabled = cs.Enable_Bulk_Email__c;
        isBulkSMSEnabled = cs.Enable_Send_Bulk_SMS__c;
        isScreenCandidateNoteEnabled = cs.Enable_Screen_Candidate_Note__c;
        try {
            //System.debug(new UserSelector().getCurrentUser().Number_of_Records_Showing__c);
            numOfRecordsShowing = Integer.valueOf(new UserSelector().getCurrentUser().Number_of_Records_Showing__c);
        } catch (Exception e) {
            System.debug('Fail to get number of records showing for the current user: ' + e);
        }
    }
}