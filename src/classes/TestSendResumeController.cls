/** 
 * This is a test class for SendResumeController
 * 
 * Created by: Lina
 * Created on: 21/07/2016
 * 
**/ 

@isTest
private class TestSendResumeController {
   
    //Test initBackPreviouspage function
    static testmethod void testinitPrePages() {
    	System.runAs(DummyRecordCreator.platformUser) {
        TestSendResumeController newTest = new TestSendResumeController();
        Contact caninitBackPreviouspage = DataTestFactory.createCandidateWithAccount();
        Contact coninitBackPreviouspage = DataTestFactory.createCorpContactWithAccount();
        Contact coninitWithEmail = DataTestFactory.createCorpContactWithAccountAndEmail();  
        Test.startTest();
        newTest.initBackPreviouspage(caninitBackPreviouspage.id, 'backtocandidate', SendResumeController.EXTRACT_CAN, '/'+ caninitBackPreviouspage.id, false);
        newTest.initBackPreviouspage(coninitBackPreviouspage.id, 'backtocontact', SendResumeController.EXTRACT_CON , '/'+ coninitBackPreviouspage.id, true);
        newTest.initBackPreviouspage(coninitWithEmail.id, 'backtocontact', SendResumeController.EXTRACT_CON , '/'+ coninitBackPreviouspage.id, true);        
        newTest.initBackPreviouspage(null, 'backtohomepage', SendResumeController.EXTRACT_CAN , '/home/home.jsp', true);        
        Test.stopTest();
    	}
    }
    
    static testmethod void testinitPrePagesForCandidateManagement(){
    	System.runAs(DummyRecordCreator.platformUser) {
        // Create data
        TestSendResumeController newTest = new TestSendResumeController();
        Placement_Candidate__c newpctest = DataTestFactory.createCandiateManangement();
        Test.startTest();
        newTest.initBackPreviouspage(newpctest.candidate__c, 'backtovacancy', SendResumeController.EXTRACT_CAN, '/'+ newpctest.id, false); 
        newTest.initBackPreviouspage(newpctest.candidate__c, 'backtoshowall', SendResumeController.EXTRACT_CAN, '/'+ newpctest.id, false);  
        newTest.initBackPreviouspage(newpctest.candidate__c, 'backtocm', SendResumeController.EXTRACT_CAN, '/'+ newpctest.id, false); 
        newTest.initBackPreviouspage(newpctest.candidate__c, 'backtoContactview', SendResumeController.EXTRACT_CAN, '/'+ newpctest.id, false);
        newTest.initBackPreviouspage(newpctest.candidate__c, 'backtoOther', SendResumeController.EXTRACT_CAN, '/'+ newpctest.id, true);
        newTest.initBackPreviouspage(newpctest.candidate__c, null, SendResumeController.EXTRACT_CAN, '/'+ newpctest.id, true);
        Test.stopTest();
    	}
    }    
    
    public void initBackPreviouspage(Id contactid, String backto, String redirectType , String returnurl, boolean issearch){   
    	System.runAs(DummyRecordCreator.platformUser) {         
        Test.setCurrentPage(Page.SendResume);
        ApexPages.currentPage().getParameters().put('arg0', contactid);
        ApexPages.currentPage().getParameters().put('backto', backto);
        ApexPages.currentPage().getParameters().put('redirectType',redirectType);
        ApexPages.currentPage().getParameters().put('returnurl',returnurl);
        SendResumeController newSendResumeController = new SendResumeController();
        newSendResumeController.initSR();
        newSendResumeController.initBackPreviouspage();
        if(backto != null && backto != 'backtoOther'){
            system.assert(newSendResumeController.commandlinkvalue != null);
        }
        else{
            system.assert(newSendResumeController.commandlinkvalue == null);
        }
        system.assert(newSendResumeController.isShowSearchFunction == issearch);
       // PageReference pr0 = newSendResumeController.backToPreviouspage();
        system.assert(newSendResumeController.backtoPage != null);
    	}
    }
    
    public static testmethod void testSearchContactAndAddToSelected() {
    	System.runAs(DummyRecordCreator.platformUser) {
        SendResumeController newTestSearch = new SendResumeController();
        Contact cand = DataTestFactory.createCandidateWithAccount();
        Contact cand2 = DataTestFactory.createCandidate();
        // Create existing document for candidate
        Web_Document__c canddoc1 = DataTestFactory.createWebDocument(cand);
        Web_Document__c canddoc2 = DataTestFactory.createWebDocument(cand);
        // Create existing document for user
        Web_Document__c usrdoc1 = DataTestFactory.createUserWebDocument();
        Web_Document__c usrdoc2 = DataTestFactory.createUserWebDocument();
        
        newTestSearch.initSR();
        newTestSearch.contactName = 'test';
        newTestSearch.searchContact();
        system.assert(newTestSearch.pagedSearchResult != null);
        system.assert(!newTestSearch.isConSearchResultEmpty);
        system.assert(!newTestSearch.isselectedUsrDocEmpty);
        
        // check if pagination function works       
        if(newTestSearch.searchResult.size()==SendResumeController.RECORD_PER_PAGE){
            system.assertEquals(newTestSearch.pagedSearchResult.size(),SendResumeController.RECORD_PER_PAGE);
            newTestSearch.nextPageOfCands();
            system.assertEquals(newTestSearch.pagedSearchResult.size(),SendResumeController.RECORD_PER_PAGE);
            newTestSearch.prevPageOfCands();
            system.assertEquals(newTestSearch.pagedSearchResult.size(),SendResumeController.RECORD_PER_PAGE);
        }else if(newTestSearch.searchResult.size()<SendResumeController.RECORD_PER_PAGE){
            system.assert(newTestSearch.pagedSearchResult.size()<SendResumeController.RECORD_PER_PAGE);
            newTestSearch.nextPageOfCands();
            system.assertEquals(newTestSearch.pagedSearchResult.size(),newTestSearch.searchResult.size());
            newTestSearch.prevPageOfCands();
            system.assertEquals(newTestSearch.pagedSearchResult.size(),newTestSearch.searchResult.size());
        }else if(newTestSearch.searchResult.size()>SendResumeController.RECORD_PER_PAGE){
            system.assertEquals(newTestSearch.pagedSearchResult.size(),SendResumeController.RECORD_PER_PAGE);
            Decimal totalNum = newTestSearch.searchResult.size();
            Decimal decTotalPageNum =totalNum / SendResumeController.RECORD_PER_PAGE;
            Integer totalPages = decTotalPageNum.round(System.RoundingMode.CEILING).intvalue();
            for(Integer i=1;i<totalPages;i++){
                system.assertEquals(newTestSearch.pagedSearchResult.size(),SendResumeController.RECORD_PER_PAGE);
                system.assert(newTestSearch.hasNextPageOfCands);
                newTestSearch.nextPageOfCands();
            }
            system.assert(newTestSearch.pagedSearchResult.size()<=SendResumeController.RECORD_PER_PAGE);
            
            for(Integer i=totalPages;i>1;i--){
                system.assert(newTestSearch.hasPrevPageOfCands);
                newTestSearch.prevPageOfCands();
                system.assertEquals(newTestSearch.pagedSearchResult.size(),SendResumeController.RECORD_PER_PAGE);
            }           
        }
        
        system.assert(!newTestSearch.hasNextPageOfCands);
        system.assert(!newTestSearch.hasPrevPageOfCands);
        
        // Test add selected candidate document to selected document tab
        Boolean hasAddResume = false;
        Boolean hasAddUsrDoc = false;
        Integer noOfResumeTry2Add = 0;
        system.assert(newTestSearch.allSelDocList.size()==0);
        system.assert(newTestSearch.isSelDocListEmpty);
        system.assert(newTestSearch.isSelLargeDocListEmpty);
        
        // add one doc to the list 
        newTestSearch.allSelDocList.add(newTestSearch.searchResult[0].docList[0]);
        
        for(ConRowRec con:newTestSearch.searchResult){
             // Check if show contact detail
            newTestSearch.selectedConId=con.contactRec.Id;          
            newTestSearch.refreshConDetails();
            if(newTestSearch.selectedCon.docList.size()>0){
                system.assert(!newTestSearch.isselectedConDocEmpty);
            }
            else{
                system.assert(newTestSearch.isselectedConDocEmpty);
            }
            // test properties which indidate if a contact record has
            // been selected to display its detail
            system.assert(newTestSearch.isContactSelected);
            
            // select resumes
            system.assert(newTestSearch.selectedCon.contactRec!=null);
            system.assertEquals(newTestSearch.selectedCon.contactRec.Id,con.contactRec.Id);
            for(DocRowRec doc:con.docList){
                doc.isSelected=true;
                noOfResumeTry2Add++;
                hasAddResume = true;
            }
            newTestSearch.add2Attachment();
        }
        
        if(hasAddResume){
            system.assert(newTestSearch.allSelDocList.size()==noOfResumeTry2Add);
            system.assertEquals(1, newTestSearch.s3LinksMap.size());
        }else{
            system.assert(newTestSearch.allSelDocList.size()==0);
        }
        
        // Test remove candidate document from selected document list
        newTestSearch.delDocId = canddoc2.Id;
        newTestSearch.removeDoc();
        system.assert(newTestSearch.allSelDocList.size() == 1);
        
        // Test add selected user document to selected document tab
        // Add one document to the selected list to test isContainUsrDoc
        // Also test remove one 
        for (DocRowRec doc : newTestSearch.currentUser.docList) {
            newTestSearch.allSelDocList.add(doc);
            doc.isSelected=true;
        }
        newTestSearch.delDocId = usrdoc1.Id;
        newTestSearch.removeDoc();
        system.assertEquals(2,newTestSearch.allSelDocList.size());
        for (DocRowRec doc : newTestSearch.currentUser.docList) {
            doc.isSelected=true;
        }        
        newTestSearch.add2UsrAttachment();
        system.assertEquals(3, newTestSearch.allSelDocList.size());
        system.assertEquals(1, newTestSearch.s3LinksMap.size());

    	}

        
    }
    
    
    static testmethod void testAddUploadedDocumentAndOtherFunction() {
    	System.runAs(DummyRecordCreator.platformUser) {
        SendResumeController newTestSearch = new SendResumeController();
        Contact cand = DataTestFactory.createCandidateWithAccount();
    
        newTestSearch.initSR();
        newTestSearch.contactName = 'test';
        newTestSearch.searchContact();
        newTestSearch.selectedConId=newTestSearch.searchResult[0].contactRec.Id;          
        newTestSearch.refreshConDetails();
        Integer noOfResumeTry2Add = 0;
        
        // Test add uploaded document to selected list
        Web_Document__c newdoc = DataTestFactory.createWebDocument(newTestSearch.searchResult[0].contactRec);
        newTestSearch.docsId = newdoc.Id;
        newTestSearch.docsFrom = 'Candidate';
        newTestSearch.addUploadedDocument();
        noOfResumeTry2Add ++;
        system.assertEquals(noOfResumeTry2Add, newTestSearch.allSelDocList.size());
        
        Web_Document__c usrdoc = DataTestFactory.createUserWebDocument();
        newTestSearch.docsId = usrdoc.Id;
        newTestSearch.docsFrom = 'User';
        newTestSearch.addUploadedDocument();
        noOfResumeTry2Add ++;
        system.assertEquals(2, newTestSearch.allSelDocList.size());
        system.assertEquals(noOfResumeTry2Add, newTestSearch.allSelDocList.size() );  
        
        // Test isNewWebDocTypeEnabled
        PCAppSwitch__c pcSwitch = new PCAppSwitch__c();
	    pcSwitch.Features__c = 'WebDocNewType';
	    insert pcSwitch;
	    system.assert(newTestSearch.isNewWebDocTypeEnabled);
        
        //Recipient function
        newTestSearch.addMoreRecepients();
        system.assertEquals(2, newTestSearch.contactResult.size());
        Account acc = [Select Id, Name from Account where Id =: cand.AccountId];
        ConRowRec newConRowRec = new ConRowRec(cand, acc, 3, 'lina@clicktocloud.com', 'lina@clicktocloud.com', 'lina@clicktocloud.com'); 

		(newTestSearch.contactResult).add(newConRowRec);

		//EmailTemplate Function
		EmailTemplate result =  [Select TimesUsed, TemplateType, TemplateStyle, SystemModstamp, Subject, OwnerId, NamespacePrefix, 
			                     Name, Markup, LastUsedDate, LastModifiedDate, LastModifiedById, IsActive, Id, HtmlValue, FolderId, 
			                     Encoding, DeveloperName, Description, CreatedDate, CreatedById, BrandTemplateId, Body, ApiVersion 
			                     From EmailTemplate where (TemplateStyle='none' and TemplateType='text') limit 1];
		newTestSearch.selectedTempId = result.Id;
		PageReference pr1 = newTestSearch.previewTemp();
		system.assert(pr1 == null);
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		System.runAs ( thisUser ) {
			Test.startTest();
			newTestSearch.createCustomTemplate();
			system.assert(newTestSearch.sendOutTemp != null);
			newTestSearch.sendMail();
			system.assert((ApexPages.getMessages()).size() > 0);
			newTestSearch.delCustomTemplate();
			system.assert(newTestSearch.sendOutTemp == null);
			newTestSearch.sendMail();
			system.assert((ApexPages.getMessages()).size()>0);
			newTestSearch.CustomEmailSubject = '';
			newTestSearch.sendMail();
			Test.stopTest();
			
		}
        
        // Test removeConDetail 
        newTestSearch.delConId = '3'; 
        newTestSearch.removeConDetail();
        system.assert((newTestSearch.contactResult).size() == 2);      
    	}    
        
    }
    
    static testmethod void testSortDocList(){
        System.runAs(DummyRecordCreator.platformUser) {
            String userId = UserInfo.getUserId();
            UsrRowRec currentUser = new UsrRowRec(UserSelector.getUsrById(userId).get(0)); 
            List<Web_Document__c> createdDocs = DataTestFactory.createResumeForUsr(userId);
            List<String> ids = new List<String>();
            for(Web_Document__c doc : createdDocs){
                ids.add(doc.Id);
            }
            List<Web_Document__c> tempDocs = WebDocumentSelector.getWebDocsByIds(ids); 
            
            List<DocRowRec> docList = new List<DocRowRec>();
            
            for (Web_Document__c d : tempDocs) {
                DocRowRec tempdoc = new DocRowRec(d, currentUser);
               
                docList.add(tempdoc);
               
            }
            
            SendResumeController.DocListSort testDocListSort = new SendResumeController.DocListSort('docLis','','');
            testDocListSort.docList = docList;
            
            System.debug('Test sorted by : ' +  testDocListSort.sortBy + ' - ' +  testDocListSort.direction);
            
            testDocListSort.sortList();
            for(Integer i = 0; i < docList.size() -1 ; i ++){
                
                System.assert(docList.get(i).doc.CreatedDate <= docList.get(i+1).doc.CreatedDate);
            }
            
            testDocListSort.sortBy = 'doc.CreatedDate';
            testDocListSort.direction = 'DESC';
            System.debug('Test sorted by : ' +  testDocListSort.sortBy + ' - ' +  testDocListSort.direction);
            testDocListSort.sortList();
            for(Integer i = 0; i < docList.size() -1 ; i ++){
                System.assert(docList.get(i).doc.CreatedDate >= docList.get(i+1).doc.CreatedDate);
            }
            
            testDocListSort.sortBy = 'doc.Name';
            testDocListSort.direction = 'ASC';
            System.debug('Test sorted by : ' +  testDocListSort.sortBy + ' - ' +  testDocListSort.direction);
            testDocListSort.sortList();
            for(Integer i = 0; i < docList.size()  ; i ++){
                
                System.assert(docList.get(i).doc.Name == 'Test Resume'+i);
            }
            
            testDocListSort.direction = 'DESC';
            System.debug('Test sorted by : ' +  testDocListSort.sortBy + ' - ' +  testDocListSort.direction);
            testDocListSort.sortList();
            for(Integer i = 0; i < docList.size()  ; i ++){
               
                System.assert(docList.get(i).doc.Name == 'Test Resume'+(9-i));
            }
            
            
            testDocListSort.sortBy = 'doc.Size';
            testDocListSort.direction = 'ASC';
            System.debug('Test sorted by : ' +  testDocListSort.sortBy + ' - ' +  testDocListSort.direction);
            testDocListSort.sortList();
            for(Integer i = 0; i < docList.size()  - 1; i ++){
                System.debug(docList.get(i).doc.File_Size__c);
                System.assert( DocRowRec.convertSizeStrToLong(docList.get(i).doc.File_Size__c) <= DocRowRec.convertSizeStrToLong(docList.get(i + 1).doc.File_Size__c));
                
            }
            
            testDocListSort.direction = 'DESC';
            System.debug('Test sorted by : ' +  testDocListSort.sortBy + ' - ' +  testDocListSort.direction);
            testDocListSort.sortList();
            for(Integer i = 0; i < docList.size() - 1  ; i ++){
                System.debug(docList.get(i).doc.File_Size__c );
                System.assert( DocRowRec.convertSizeStrToLong(docList.get(i).doc.File_Size__c) >= DocRowRec.convertSizeStrToLong(docList.get(i + 1).doc.File_Size__c));
                
            }
            
        }
    }
    
    static testmethod void testProcessFileSize(){
        System.debug(DocRowRec.convertSizeStrToLong('1,   5.89 kl'));
        System.assert( DocRowRec.convertSizeStrToLong('15') == 15);
        System.assert( DocRowRec.convertSizeStrToLong('15.6 KB ') == Decimal.valueOf(15.6 * 1024 + '').longValue());
        System.assert( DocRowRec.convertSizeStrToLong('123,456 bytes ') == Long.valueOf('123456'));
        
        System.assert( DocRowRec.convertSizeStrToLong('2.61 MB ') == Decimal.valueOf(2.61 * 1024 * 1024 + '').divide(1,2).longValue());
    }

    static testmethod void testOtherMethod() {
    	System.runAs(DummyRecordCreator.platformUser) {
        SendResumeController newTestSearch = new SendResumeController();
        // Test reset resetEmailContent()
        newTestSearch.resetEmailContent();
        
        // Test customerErrorMessage
        newTestSearch.customerErrorMessage(403, 'test');
        //system.assert((ApexPages.getMessages()).size() > 0);
        
        // Test searchConAndAccSR
        String result = SendResumeController.searchConAndAccSR('test');
        system.assert(result != null);
    	}
    }
   

}