@isTest
private class RateAstutePayrollImplementationTest {
    static testMethod void unitTest1(){
    	System.runAs(DummyRecordCreator.platformUser) {
        list<Rate__c> rates=DataTestFactory.createRates();
        RateAstutePayrollImplementation  ratAProllImplementation=new RateAstutePayrollImplementation ();
        for(Rate__c rate: rates){
            rate.Astute_Payroll_Upload_Status__c='On Hold';
            
			}
        ratAProllImplementation.addRates(rates);
        System.assert(rates.size()>0);
    	}
    }
    
	 static testMethod void unitTest2(){
	 	System.runAs(DummyRecordCreator.platformUser) {
        list<Rate__c> rates=DataTestFactory.createRates();
         HttpResponse res=new HttpResponse ();
         list<Object> allList=new list<Object>();
         for(Rate__c rate: rates){
              map<String,Object> obj=new map<String,Object>();
             obj.put('salesforceId',rate.Id);
             obj.put('PRID',100);
             obj.put('errorMessage','-');
             allList.add(obj);
         }
         res.setBody(JSON.serialize(allList));
         res.setStatusCode(200);
         RateAstutePayrollImplementation.updateAddRateResults(res);
         System.assert(rates.size()>0);
	 	}
     }
    static testMethod void unitTest3(){
    	System.runAs(DummyRecordCreator.platformUser) {
         RateAstutePayrollImplementation  ratAProllImplementation=new RateAstutePayrollImplementation ();
        list<Rate__c> rates=DataTestFactory.createRates();
        for(Rate__c rate: rates){
            rate.PRID__c=2;
        }
        ratAProllImplementation.deleteRates(rates);
        System.assert(rates.size()>0);
    	}
     }
    
}