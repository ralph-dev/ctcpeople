/**
 * This is the test class for SQSCredentialAdminController
 * Created by: Lina
 * Created Date: 13/12/2016
 **/

@isTest
public class SQSCredentialAdminControllerTest {
    
    private static final String SQSNAME = 'Daxtra Feed SQS';
    private static SQSCredentialSelector selector = new SQSCredentialSelector();
    
    static testmethod void testUpdateSQSCredential() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Create data
        SQSCredential__c sqs = new SQSCredential__c();
        sqs.Name = 'Test Name'; 
        sqs.URL__c = 'www.test.com';
        sqs.Access_Key__c = 'username';
        sqs.Secret_Key__c = 'password';
        insert sqs;
	
		SQSCredential__c sqsBefore = selector.getSQSCredential('Test Name');
		System.assertEquals('www.test.com', sqsBefore.URL__c);
        System.assertEquals('username', sqsBefore.Access_Key__c);
        System.assertEquals('password', sqsBefore.Secret_Key__c);
        
        SQSCredentialAdminController SQSAdminCon = new SQSCredentialAdminController();
        
        List<SelectOption> sqsNames = SQSCredentialAdminController.getListOfSQS();
        System.assert(sqsNames.size() >= 1);
        
        SQSAdminCon.seturl('www.SQS.com');
        SQSAdminCon.setaccessKey('a');
        SQSAdminCon.setsecretKey('b');
        SQSAdminCon.name = 'Test Name';
        SQSAdminCon.saveCredential();
        List<ApexPages.Message> msgList = ApexPages.getMessages();
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals(ApexPages.Severity.CONFIRM, msg.getSeverity());
        }
        SQSCredential__c sqsAfter = selector.getSQSCredential('Test Name');
        System.assertEquals('www.SQS.com', sqsAfter.URL__c);
        System.assertEquals('a', sqsAfter.Access_Key__c);
        System.assertEquals('b', sqsAfter.Secret_Key__c);
    	}
    }
    
    static testmethod void testInsertSQSCredential() {
    	System.runAs(DummyRecordCreator.platformUser) {
		SQSCredential__c sqsBefore = selector.getSQSCredential('Daxtra Feed SQS');
		System.assertEquals(null, sqsBefore);

        SQSCredentialAdminController SQSAdminCon = new SQSCredentialAdminController();
        SQSAdminCon.seturl('www.SQS.com');
        SQSAdminCon.setaccessKey('a');
        SQSAdminCon.setsecretKey('b');
        SQSAdminCon.name = 'Daxtra Feed SQS';
        SQSAdminCon.saveCredential();
        List<ApexPages.Message> msgList = ApexPages.getMessages();
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals(ApexPages.Severity.CONFIRM, msg.getSeverity());
        }
        SQSCredential__c sqsAfter = selector.getSQSCredential('Daxtra Feed SQS');
        System.assertEquals('www.SQS.com', sqsAfter.URL__c);
        System.assertEquals('a', sqsAfter.Access_Key__c);
        System.assertEquals('b', sqsAfter.Secret_Key__c);
    	}
    }
    
    static testmethod void testInvalidInput() {
    	System.runAs(DummyRecordCreator.platformUser) {
        SQSCredentialAdminController SQSAdminCon = new SQSCredentialAdminController();
        SQSAdminCon.saveCredential();
        List<ApexPages.Message> msgList = ApexPages.getMessages();
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals('Please enter values for ALL FIELDS!', msg.getSummary());
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
        }
    	}
    }
    
}