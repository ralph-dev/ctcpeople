/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 *
 * @Author Raymond
 * This class is used to test S3RestAPI class
 */
// Must enable seeAllData as S3 credential setting may be came from a SF document record
@isTest
private class TestS3RestAPI {

    /*
        Test getFileSize method in unknow error scenario. As SF test envrionment will
        always disallow callout, S3RestAPI will encounter unknow exception.
    */
    public static testmethod void testGetFileSizeUnkownCalloutFailure(){
    	System.runAs(DummyRecordCreator.platformUser) {
       S3Credential c = PeopleCloudHelper.getS3CredentialFromCustomSetting();
       S3RestAPI api = new S3RestAPI(c);
       Integer fileSize = -1;
       Boolean hasException = false;
       try{
           api.getFileSize('jobwire','error1271827663547858.txt'); 
       }catch(S3RestAPIException e){
           system.debug(e.getMessage());
           //system.assertEquals(S3RestAPIException.S3RestAPIExceptionCode.UNKNOWN,e.errorCode);
           hasException = true;
       }
       system.assert(hasException); // hasException should be true;
    	}
    }
    
    /*
        Test getFileSize method in empty credential scenario.
    */
    public static testmethod void testGetFileSizeNullCredential(){
    	System.runAs(DummyRecordCreator.platformUser) {
       S3Credential c = new S3Credential(null,'asdfasdfasd');
       S3RestAPI api = new S3RestAPI(c);
       Boolean hasException = false;
       try{
           api.getFileSize('jobwire','error1271827663547858.txt'); 
       }catch(S3RestAPIException e){
           system.debug(e.getMessage());
           system.assertEquals(S3RestAPIException.S3RestAPIExceptionCode.NULL_CREDENTIAL,e.errorCode);
           hasException = true;
       }
       system.assert(hasException); // hasException should be true;
       
       c = new S3Credential('asdfasdfasd',null);
       api = new S3RestAPI(c);
       hasException = false;
       try{
           api.getFileSize('jobwire','error1271827663547858.txt'); 
       }catch(S3RestAPIException e){
           system.debug(e.getMessage());
           system.assertEquals(S3RestAPIException.S3RestAPIExceptionCode.NULL_CREDENTIAL,e.errorCode);
           hasException = true;
       }       
       system.assert(hasException); // hasException should be true;
    	}
       
    }    
}