@isTest
public with sharing class EducationDummyRecordCreator {
	private List<Education_History__c> edList {get; set;}

    public EducationDummyRecordCreator() {
		edList = new List<Education_History__c>();
	}

	private List<Contact> generateDummyContactRecord(){
		List<Contact> conList = new List<Contact>();
		for(Integer i=0; i<10; i++){
			Contact con = new Contact(LastName = 'testContact'+i);
			conList.add(con);
		}
		insert conList;
		return conList;
	}

	public List<Id> generateDummyRecord() {
		List<Id> conIds = new List<Id>();
		List<Contact> conList = generateDummyContactRecord();
		for(Contact con: conList) {
			Education_History__c ed = new Education_History__c(Contact__c = con.Id);
			edList.add(ed);
			conIds.add(con.Id);
		}

		insert edList;
		return conIds;
	} 
}