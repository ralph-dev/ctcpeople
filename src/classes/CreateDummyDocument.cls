@isTest
public with sharing class CreateDummyDocument {
    public List<Document> myfiles{get; set;}
    public static final Integer NUM_ATTACHMENTS_TO_ADD=5;
    public String filename {get; set;}
    public Blob blobObj {get; set;}
    public List<DocRowRec> optInlocalDocList {get ; set;}
    public List<Id> deleteDummyDocIds;
    private DocRowRec dummyDocRowRec;
    public String delDocId{get;set;}
    public void CreateDummyDocument(){                  
        myfiles = new List<Document>{new Document()};   
    }
    
    public PageReference save(){
        Contact tempcontact = new Contact();//Change the DocRowRec type on 26 Sep 12 by Jack
        optInlocalDocList = new List<DocRowRec>();
        DocRowRec dummyDocRowRec = null;
        String filenameString = '';         
        String startTimeStamp = ''+Datetime.now().getTime();
        Id localDocUserId = Userinfo.getUserId();
        folder localDocFolder = DaoFolder.getFolderByDevName('CTC_Admin_Folder');
        try{
            for(Document newDoc : myfiles){
                if(newDoc.Body != null){
                    Document tempdoc = new Document();
                    //system.debug('newDoc ='+newDoc);
                    filenameString = (newDoc.Name).substring(0, (newDoc.Name).lastIndexOf('.'));
                    tempdoc.Name = filenameString;
                    tempdoc.Body = newDoc.Body; 
                    tempdoc.Type = (newDoc.Name).substring((newDoc.Name).lastIndexOf('.')+1,(newDoc.Name).length());    
                    tempdoc.DeveloperName = 'QSR_'+UserInfo.getUserId()+ filenameString+'_'+startTimeStamp;
                    tempdoc.FolderId = localDocFolder.Id;
                    insert tempdoc;
                    dummyDocRowRec = new DocRowRec(tempdoc,tempcontact,Userinfo.getUserId()) ;
                    optInlocalDocList.add(dummyDocRowRec);
                }
            }
        }
        catch(Exception e){
            
        }
        return null;
    }
}