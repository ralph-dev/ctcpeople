/**
    All the sub function related to CTCPeopleSettingHelper__c should come from this end.

*****/
public with sharing class CTCPeopleSettingHelperServices {
    private String apAccountId='';
    private Map<String,Schema.SObjectType> globalDescriptions=new map<String,Schema.SObjectType>();
    private list<StyleCategory__c> accountList;
    private Boolean disableDeleteRates=false;
    private String astutePayrollEndpoint='';
    public Boolean getDisableDeleteRates(){
        return this.disableDeleteRates;
    }
    /**
    
        Return query according to the mappings.
    
    ***/
    public String formQueryFromMap(map<String,String> mappings){
        String queryStr='';
        //To avoid duplicate developer names.
        Set<String> apiNames=new Set<String>();
        for(String key : mappings.keySet()){
            String apiName=mappings.get(key);
            if(apiNames.contains(apiName)){
                continue;
            }else{
                apiNames.add(apiName);
            }
            queryStr=apiName+','+queryStr;
        }
        if(queryStr!=''){
            queryStr=queryStr.substring(0, queryStr.length()-1);
        }
        return queryStr;
    }
    public list<StyleCategory__c> getAccountList(){
        if(accountList==null){
            StyleCategorySelector styleCategorySel=new StyleCategorySelector();
            accountList=styleCategorySel.getUserAstutePayrollAccount(getApAccountId());
        }
        return accountList;
    }
    /***
    * To check if the user could be able to use Astute Payroll or Not.
    *
    **/
    public Boolean hasApAccount(){
        if(getApAccountId()!='' || test.isRunningTest() )
            return true;
        else
            return false;
         
    }
    /****
    *   To check if the field API Name is existing or not.
    *
    *
    *
    *
    ****/
    public Boolean hasAPINameExisting(String sobjectName, String fieldName){
        Boolean isApiNameExisted=true;
        if(globalDescriptions.size()==0){
            globalDescriptions= Schema.getGlobalDescribe();
        }       
        Schema.SObjectType sobjType = globalDescriptions.get(sobjectName);
        Map<String,Schema.SObjectField> sObjectFields =sobjType.getDescribe().fields.getMap();
        if(sObjectFields.get(fieldName)==null){
            isApiNameExisted=false;
        }
        return isApiNameExisted;

    }
    /***
    *   To check the api name bulkly
    *
    *
    ****/
    public void hasAPINameExistingForAstutePayroll(list<CTCPeopleSettingHelper__c> ctcPeopleSettingHelpers){
        list<CTCPeopleSettingHelper__c> ctcPeopleSettingHelpersForChecking=new list<CTCPeopleSettingHelper__c>();
        map<String,String> rTSobjectMap=new map<String,String>();
        rTSobjectMap.put('Approver_Mapping','Contact');
        rTSobjectMap.put('Biller_Contacts_Mapping','Contact');
        rTSobjectMap.put('Biller_Mapping','Account');
        rTSobjectMap.put('Owner_Mapping','User');
        rTSobjectMap.put('Placement_Mapping','Placement_Candidate__c');
        rTSobjectMap.put('Employee_Mapping','Contact');
        rTSobjectMap.put('Invoice_Line_Item_Mapping','Invoice_Line_Item__c');
        list<String> recordTypeValues=new list<String>();
        recordTypeValues.addAll(rTSobjectMap.keySet());

        RecordTypeSelector recordSelector=new RecordTypeSelector();
        list<RecordType> recordTypes=
            recordSelector.fetchRecordTypesByName(recordTypeValues);
        map<String,String> rTNameIdMap=new map<String,String>();
        for(RecordType rType: recordTypes){
            rTNameIdMap.put(rType.Id,rType.developerName);
        }
        for(CTCPeopleSettingHelper__c ctcPeopleSettingHelper: ctcPeopleSettingHelpers){
            String developerName=rTNameIdMap.get(ctcPeopleSettingHelper.recordTypeId);
            //System.debug('The developerName is '+developerName);
            if(developerName!=null){
                String fieldName=ctcPeopleSettingHelper.name;
                String sobjectName=rTSobjectMap.get(developerName);
                //System.debug('The object name is '+sobjectName);
                if(!hasAPINameExisting(sobjectName,fieldName)){
                    ctcPeopleSettingHelper.name.addError('In the object: "'
                        +sobjectName +'" there is no such field called: '+fieldName);
                }
            }

        }

    }

	public String getEndpoint(){
		getApAccountId();							
		return astutePayrollEndpoint;		                    
	}


    public String getApAccountId(){
        if(apAccountId==''){
        	astutePayrollEndpoint='';
            String pid=Userinfo.getProfileId();
            Astute_Payroll_Account__c apAccount= Astute_Payroll_Account__c.getInstance(pid);
            apAccountId=apAccount.Astute_Payroll_Account_Id__c;
            if(apAccountId!=null){
                if(apAccount.Disabled_account__c){
                    apAccountId='';
                }
                disableDeleteRates=apAccount.Disable_delete_rate_feature__c;
            }else{
                apAccountId='';
            }
            
            if(astutePayrollEndpoint!=null){
            	astutePayrollEndpoint=	apAccount.endPoint__c;
            }
        }
        return apAccountId;
    }
    public list<CTCPeopleSettingHelper__c> getCustomizeMapsForSelectedType(String recordTypeName){
        list<String> developerNames=new list<String>();
        list<CTCPeopleSettingHelper__c> ctcPeopleSettingHelpers=new list<CTCPeopleSettingHelper__c>();
        developerNames.add(recordTypeName);
        RecordTypeSelector rTSelector=new RecordTypeSelector();
        list<RecordType> recordTypes=rTSelector.fetchRecordTypesByName(developerNames);
        if(recordTypes.size()==0){
            return ctcPeopleSettingHelpers;
            
        }
        String recordTypeIdValue=recordTypes.get(0).Id;
        CTCPeopleSettingHelperSelector ctcPeopleSettingSelector=new CTCPeopleSettingHelperSelector();
        ctcPeopleSettingHelpers=
            ctcPeopleSettingSelector.fetchCTCPeopleHelperSettingByRT(recordTypeIdValue,getApAccountId());
        return ctcPeopleSettingHelpers;
        
        
    }
    /***
        Mapping for contact - Employee
    
    ***/
    public map<String,String> getContactEmployeeMap(){
        map<String,String> contactEmployeeMap=new map<String,String>();
        contactEmployeeMap.put('name_first','firstname');
        contactEmployeeMap.put('name_last','lastname');
        contactEmployeeMap.put('email','email');
        contactEmployeeMap.put('mobile_phone','MobilePhone');
        contactEmployeeMap.put('phone_mobile','phone');
        contactEmployeeMap.put('address_street','mailingstreet');
        contactEmployeeMap.put('address_locality','mailingcity');
        contactEmployeeMap.put('address_region','mailingstate');
        contactEmployeeMap.put('address_postcode','mailingPostalCode');
        contactEmployeeMap.put('remoteid','id');
        contactEmployeeMap.put('date_birth','birthdate');
        contactEmployeeMap.put('gender','gender__c');
        return contactEmployeeMap;
    }
    public map<String,String> getContactEmployeeMap(String developerName){
        map<String,String> contactEmployeeMap=getContactEmployeeMap();
        list<CTCPeopleSettingHelper__c> customizeMap=getCustomizeMapsForSelectedType(developerName);
        if(customizeMap!=null){
            for(CTCPeopleSettingHelper__c customizeBillerContact: customizeMap){
                contactEmployeeMap.put(customizeBillerContact.AP_Employee_Fields__c,
                        customizeBillerContact.name);
            }
        }
        return contactEmployeeMap;
    }
    /***
        Mapping for Placement - Employee
        developerName could be null
    ***/
    public map<String,String> getPlacementEmployeeMap(){
        map<String,String> placementFieldsMap=new map<String,String>();
        placementFieldsMap.put('remoteid','Candidate__c');
        placementFieldsMap.put('job_code','id');
        placementFieldsMap.put('date_start','start_date__c');
        placementFieldsMap.put('date_finish','end_date__c');
        placementFieldsMap.put('pay_rate_interval','timesheet_type__c');
        placementFieldsMap.put('job_title','job_title__c');
        placementFieldsMap.put('timesheet_enabled','timesheetenabled__c');
        placementFieldsMap.put('fixedpayitem_enabled','fixedpayitemenabled__c');
        placementFieldsMap.put('employment_type','Employment_Type__c');
        placementFieldsMap.put('pay_oncosts_percent','Pay_Oncost_Percent__c');
        placementFieldsMap.put('pay_rate','Pay_Rate__c');
        placementFieldsMap.put('pay_charge_rate','Pay_Charge_Rate__c');
        placementFieldsMap.put('pay_type','Pay_Type__c');
        //placementFieldsMap.put('pay_frequency','Pay_Cycle__c');
        placementFieldsMap.put('AID','RuleGroupID__c');
        placementFieldsMap.put('PCGID','RateCardId__c');
        placementFieldsMap.put('CM_CID','CompanyEntityId__c');
        placementFieldsMap.put('OL_OID', 'Occupation_LibraryId__c');
        placementFieldsMap.put('WID','WorkplaceId__c');
        placementFieldsMap.put('BID','BillerId__c');
        placementFieldsMap.put('BCID','BillerContactId__c');
        placementFieldsMap.put('manager_user_id','FirstApproverID__c');
        placementFieldsMap.put('manager2_user_id','SecondApproverId__c');
        return  placementFieldsMap;
    }
    public map<String,String> getPlacementEmployeeMap(String developerName){
        map<String,String> placementFieldsMap=getPlacementEmployeeMap();
        list<CTCPeopleSettingHelper__c> customizeMap=getCustomizeMapsForSelectedType(developerName);
        if(customizeMap!=null){
            for(CTCPeopleSettingHelper__c customizeBillerContact: customizeMap){
                placementFieldsMap.put(customizeBillerContact.AP_Employee_Fields__c,
                        customizeBillerContact.name);
            }
        }
        
        return placementFieldsMap;
    }
    public map<String,String> getUserOwnerMap(String developerName){
        map<String,String> userOwnerMap=getUserOwnerMap();
        list<CTCPeopleSettingHelper__c> customizeMap=getCustomizeMapsForSelectedType(developerName);
        if(customizeMap!=null){
            for(CTCPeopleSettingHelper__c customizeUserOwner: customizeMap){
                userOwnerMap.put(customizeUserOwner.AP_Owner_Fields__c,
                        customizeUserOwner.name);
            }
        }
        return userOwnerMap;
    }
    
    public map<String,String> getUserOwnerMap(){
        map<String,String> userOwnerMap=new map<String,String>();
        userOwnerMap.put('name_first','firstName');
        userOwnerMap.put('name_last','lastName');
        userOwnerMap.put('email','email');
        userOwnerMap.put('phone','phone');
        userOwnerMap.put('remoteid','id');
        userOwnerMap.put('USERTYPE','Astute_Payroll_User_Type__c');
        return userOwnerMap;
    }
    /***
        Mapping for Contact - Approver
        developerName could be null
    ***/
    public map<String,String> getContactApproverMap(String developerName){
        map<String,String> contactApproverMap=getContactApproverMap();
        list<CTCPeopleSettingHelper__c> customizeMap=getCustomizeMapsForSelectedType(developerName);
        if(customizeMap!=null){
            for(CTCPeopleSettingHelper__c customizeBillerContact: customizeMap){
                contactApproverMap.put(customizeBillerContact.AP_Approver_Fields__c,
                        customizeBillerContact.name);
            }
        }
        return contactApproverMap;      
    }
    public map<String,String> getContactApproverMap(){
        map<String,String> contactApproverMap=new map<String,String>();
        contactApproverMap.put('name_first','firstName');
        contactApproverMap.put('name_last','lastName');
        contactApproverMap.put('email','email');
        contactApproverMap.put('phone_mobile','phone');
        contactApproverMap.put('remoteid','id');
        return contactApproverMap;
    }
    /****
        Mapping for Contact - Biller contact.
        developerName could be empty
    ***/
    public map<String,String> getContactBillerContactMap(){
        map<String,String> contactBillerContactMap=new map<String,String>();                        
        contactBillerContactMap.put('phone','phone');
        contactBillerContactMap.put('email','email');
        contactBillerContactMap.put('address_street','mailingstreet');
        contactBillerContactMap.put('address_region','mailingstate');
        contactBillerContactMap.put('address_locality','mailingcity');
        contactBillerContactMap.put('address_postcode','mailingPostalCode');
        contactBillerContactMap.put('BID','Biller_Id__c');
        contactBillerContactMap.put('remoteid','id');
        return contactBillerContactMap;
    }
    public map<String,String> getContactBillerContactMap(String developerName){
        map<String,String> contactBillerContactMap=getContactBillerContactMap();
        list<CTCPeopleSettingHelper__c> customizeMap=getCustomizeMapsForSelectedType(developerName);
        if(customizeMap!=null){
            for(CTCPeopleSettingHelper__c customizeBillerContact: customizeMap){
                contactBillerContactMap.put(customizeBillerContact.AP_Biller_Contact_Fields__c,
                        customizeBillerContact.name);
            }
        }               
        return contactBillerContactMap;
    }
    /***
    
    
        Mapping for Invoice_Line_Item__c - Invoice Line Item.   
        developerName could be empty
    ****/
    public map<String,String> getinvoiceItemMap(){
        map<String,String> invoiceItemMap=new map<String,String>();                     
        invoiceItemMap.put('quantity','Quantity__c');
        invoiceItemMap.put('amount','Unit_Cost__c');
        invoiceItemMap.put('description','Description__c');
        invoiceItemMap.put('billing_contact_BCID','Biller_Contact_ID__c');
        invoiceItemMap.put('customer_BID','Biller_ID__c');
        invoiceItemMap.put('CM_CID','Default_Company_Entity__c');    
        invoiceItemMap.put('remoteid','id');
        return invoiceItemMap;
    }
    public map<String,String> getinvoiceItemMap(String developerName){
        map<String,String> invoiceItemMap=getinvoiceItemMap();
        list<CTCPeopleSettingHelper__c> customizeMap=getCustomizeMapsForSelectedType(developerName);
        if(customizeMap!=null){
            for(CTCPeopleSettingHelper__c customizeInvoiceItem: customizeMap){
                invoiceItemMap.put(customizeInvoiceItem.AP_Invoice_Item_Fields__c,customizeInvoiceItem.name);
            }
        }               
        return invoiceItemMap;
    }
    /***
        Mapping for rate - rate change.

    ****/
    public map<String,String> getRateChangeMap(){
        map<String,String> rateChangeMap=new map<String,String>();   
        rateChangeMap.put('finish_date','End_Date__c');
        rateChangeMap.put('start_date','Start_Date__c');
        rateChangeMap.put('pay_charge_rate','Pay_charge_rate__c');
        rateChangeMap.put('pay_oncosts_percent','Pay_Oncosts_Percent__c');
        rateChangeMap.put('pay_rate','Pay_Rate__c');
        rateChangeMap.put('user_id','Employee_User_Name__c');
        rateChangeMap.put('user_remoteid','User_remote_id__c');
        rateChangeMap.put('job_code','Job_code__c');
        rateChangeMap.put('UID','UID__c');
        rateChangeMap.put('PRID','PRID__c');
        rateChangeMap.put('remoteid','ID');
        return rateChangeMap;
    }
    
    /***
    
    
        Mapping for Account - Biller.   
        developerName could be empty
    ****/
    public map<String,String> getAccountBillerMap(){
        map<String,String> accountBillerMap=new map<String,String>();                       
        accountBillerMap.put('name','name');
        accountBillerMap.put('phone','phone');
        accountBillerMap.put('billing_street','billingstreet');
        accountBillerMap.put('billing_locality','billingcity');
        accountBillerMap.put('billing_region','billingstate');
        accountBillerMap.put('billing_postcode','billingPostalcode');
        accountBillerMap.put('trading_name','Trading_Name__c');
        accountBillerMap.put('abn','ABN__c');
        accountBillerMap.put('contact','Default_Billing_Contact_Name__c');
        accountBillerMap.put('email','Default_Billing_Contact_Email__c');
        accountBillerMap.put('remoteid','id');
        return accountBillerMap;
    }
    public map<String,String> getAccountBillerMap(String developerName){
        map<String,String> accountBillerMap=getAccountBillerMap();
        list<CTCPeopleSettingHelper__c> customizeMap=getCustomizeMapsForSelectedType(developerName);
        if(customizeMap!=null){
            for(CTCPeopleSettingHelper__c customizeAccountBiller: customizeMap){
                accountBillerMap.put(customizeAccountBiller.AP_Biller_Fields__c,customizeAccountBiller.name);
            }
        }               
        return accountBillerMap;
    }
    
    /**
        Used to convert workplace to biller workplace in Astute Payroll info
    
    ****/
    public map<String,String> getWorkplaceMap(){
        map<String,String> workplaceMap=new map<String,String>();
        workplaceMap.put('BID','BID__c');
        workplaceMap.put('postcode','Postcode__c');
        workplaceMap.put('remoteid','Id');
        workplaceMap.put('region','State__c');
        workplaceMap.put('locality','Suburb_Town__c');
        workplaceMap.put('name','Name');
        workplaceMap.put('street','Street_Address__c');
        return workplaceMap;            
        
    }
    
    /**
        Used to convert record to Astute Payroll info
    
    ****/
    public map<String,Object> convertToAPFormat(SObject record, map<String,String> apSfMap){
        map<String,Object> aPInfo=new map<String,Object>();
        //Get the Astute Payroll Account
        if(getAccountList().size()>0){
        	
        	/**
        	* modified by andy for security review II
        	*/
        	ProtectedCredential pc = CredentialSettingService.getCredential(getAccountList().get(0).Id);
        	if(pc != null){
        		aPInfo.put('api_key',pc.key);
	            aPInfo.put('api_username',pc.username);
	            aPInfo.put('api_password',pc.password);
	            
	            for(String aPFieldName : apSfMap.keyset()){
	                //system.debug('apSfMap ='+ aPFieldName);
		            String sfField=apSfMap.get(aPFieldName);
		            if(record.get(sfField)!=null){
		            	Object resultStr=record.get(sfField);
		            	resultStr=EncodingUtil.urlEncode(resultStr+'','UTF-8');
		                if(sfField.endsWith('state'))
		                {
		                    aPInfo.put(aPFieldName,resultStr+', AU');
		                }else{
		                    aPInfo.put(aPFieldName,resultStr);
		                }
		            }
		            else
		                aPInfo.put(aPFieldName,null);           
	            }
        	}
            
        }
        return aPInfo;              
        
    }
    
    /**
     * 
     * Call SOAP service to Pull data from Astute Payroll.
    *****/
    public DOM.Document pullEntity( String commandStr){
        String apiKey='';
        String userName='';
        String password='';    
        if(getAccountList().size()>0){
        	/**
        	* modified by andy for security review II
        	*/
        	ProtectedCredential pc = CredentialSettingService.getCredential(getAccountList().get(0).Id);
        	if(pc != null){
        		apiKey=pc.key;
	            userName=pc.username;
	            password=pc.password;
        	}
            
        }

        String requestStr='<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:'
                    +commandStr+'"><soapenv:Header/><soapenv:Body><urn:'+commandStr+' soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><parms_in xsi:type="urn:userGet" xmlns:urn="urn:tsoIntegrator"><api_key xsi:type="xsd:string">'
                    +apiKey+'</api_key><api_username xsi:type="xsd:string">'+userName+'</api_username><api_password xsi:type="xsd:string">'+password+'</api_password></parms_in></urn:'+commandStr
                    +'></soapenv:Body></soapenv:Envelope>';
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.astutepayroll.com/webservice/');
        req.setMethod('POST');
        req.setBody(requestStr);
        Http http = new Http();
        req.setHeader('Content-Type', 'text/xml; charset=utf-8');
        req.setHeader('SOAPAction', 'urn:'+commandStr+'#'+commandStr);
        req.setHeader('Operation',commandStr);
        DOM.Document specificResultDocument= new DOM.Document();   
        if(test.isRunningTest()){
            return specificResultDocument;
        }
        HTTPResponse res = http.send(req);
        DOM.Document resultDoc = res.getBodyDocument();        
        AstutePayrollResponseParser  APResponseParser=new AstutePayrollResponseParser ();
        //System.debug('The Result Doc is '+resultDoc.toXmlString());
        specificResultDocument=APResponseParser.parseCompanyEntity(resultDoc);
        
        return specificResultDocument;
    }
    public String getRecordTypeId(String rTName){
        RecordTypeSelector recordTypeSel=new RecordTypeSelector();
        list<String> developerNames=new list<String>();
        developerNames.add(rTName);
        list<RecordType> rTs=recordTypeSel.fetchRecordTypesByName(developerNames);
        String rTID='';
        if(rTs.size()>0){
            rTID=rTs.get(0).Id;
        }
        return rTID;
    }
    
    public list<CTCPeopleSettingHelper__c> pullCompanyEntities(){
        DOM.Document resultDocument= new DOM.Document();   
        resultDocument=pullEntity('CompanyEntityQuery');
        String rTID=getRecordTypeId('Company_Entity');
        list<CTCPeopleSettingHelper__c> companyEntities=new list<CTCPeopleSettingHelper__c>();
        if(Test.isRunningTest()){
            return companyEntities;
        }
        for(Dom.XmlNode userDoc : resultDocument.getRootElement().getChildElements()){
            CTCPeopleSettingHelper__c companyEntity=new CTCPeopleSettingHelper__c();
            companyEntity.Company_Entity_ID__c=userDoc.getChildElement('CM_CID', null).getText();
            companyEntity.name=userDoc.getChildElement('name', null).getText();
            companyEntity.RecordTypeId=rTID;
            companyEntity.isActive__c=true;
            companyEntities.add(companyEntity);
        }
        return companyEntities;
    }
    public list<CTCPeopleSettingHelper__c> pullRuleGroups(){
        DOM.Document resultDocument= new DOM.Document();
        resultDocument=pullEntity('RuleGroupQuery');
        String rTID=getRecordTypeId('Rule_Group');
        list<CTCPeopleSettingHelper__c> ruleGroups=new list<CTCPeopleSettingHelper__c>();
        if(Test.isRunningTest()){
            return ruleGroups;
        }
        for(Dom.XmlNode userDoc : resultDocument.getRootElement().getChildElements()){
            CTCPeopleSettingHelper__c ruleGroup=new CTCPeopleSettingHelper__c();
            ruleGroup.Rule_Group_ID__c=userDoc.getChildElement('RGID', null).getText();
            ruleGroup.name=userDoc.getChildElement('name', null).getText();
            ruleGroup.RecordTypeId=rTID;
            ruleGroup.isActive__c=true;
            ruleGroups.add(ruleGroup);
        }
        return ruleGroups;
    }
    
    public list<CTCPeopleSettingHelper__c> pullRateCards(){
        DOM.Document resultDocument= new DOM.Document();
        resultDocument=pullEntity('RateCardQuery');
        String rTID=getRecordTypeId('Rate_Card');
        list<CTCPeopleSettingHelper__c> rateCards=new list<CTCPeopleSettingHelper__c>();
        if(Test.isRunningTest()){
            return rateCards;
        }
        for(Dom.XmlNode userDoc : resultDocument.getRootElement().getChildElements()){
            CTCPeopleSettingHelper__c rateCard=new CTCPeopleSettingHelper__c();
            rateCard.Rate_Card_ID__c=userDoc.getChildElement('PCGID', null).getText();
            rateCard.name=userDoc.getChildElement('name', null).getText();
            rateCard.RecordTypeId=rTID;
            rateCard.isActive__c=true;
            rateCards.add(rateCard);
        }
        return rateCards ;
    }
    
    public list<CTCPeopleSettingHelper__c> pullOccupLibs(){
        DOM.Document resultDocument= new DOM.Document();
        resultDocument=pullEntity('OccupationQuery');
        String rTID=getRecordTypeId('Occupation_Libraries');
        list<CTCPeopleSettingHelper__c> occupLibs=new list<CTCPeopleSettingHelper__c>();
        if(Test.isRunningTest()){
            return occupLibs;
        }
        for(Dom.XmlNode userDoc : resultDocument.getRootElement().getChildElements()){
            CTCPeopleSettingHelper__c occupLib=new CTCPeopleSettingHelper__c();
            occupLib.Occupation_Libraries_Id__c=userDoc.getChildElement('OL_OID', null).getText();
            occupLib.name=userDoc.getChildElement('name', null).getText();
            occupLib.Occupation_Library_Rate_Card__c = userDoc.getChildElement('rate_card_id', null).getText();
            occupLib.Occupation_Library_Rule_Group__c = userDoc.getChildElement('rule_group_id', null).getText();
            occupLib.RecordTypeId=rTID;
            occupLib.isActive__c=true;
            //system.debug('occupLib='+occupLib);
            occupLibs.add(occupLib);
        }
        return occupLibs ;
    }    

    /**
     * Get default Skill Group from CTCPeopleSettingHelper Object
     * @Auther Jack ZHOU
     * @Date   02 Aug 2016
     */
    public List <CTCPeopleSettingHelper__c> getDefaultSkillGroups() {
        RecordType defaultSkillGroupRecordType = DaoRecordType.DefaultSkillGroupRT;
        List<CTCPeopleSettingHelper__c> defaultSkillGroup = new List<CTCPeopleSettingHelper__c>();
        defaultSkillGroup = new CTCPeopleSettingHelperSelector().fetchCTCPeopleHelperSettingByRT(defaultSkillGroupRecordType.Id);
        return defaultSkillGroup;
    }
}