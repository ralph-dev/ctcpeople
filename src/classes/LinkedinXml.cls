public class LinkedinXml{

    XMLDOM theXmlDom;
    String xml {get; set;}
    String rootName {get; set;}
    String nodeName {get; set;}
    
    public String locName; 
    public String locCode; 
    public String totalPeopleList {get; set;}
    public String countPeopleList {get; set;}
    public String startPeopleList {get; set;}
    public Map<String,LinkedinPersonProfileXml.peopleList> perPro {get; set;}
    public List<Map<String,LinkedinPersonProfileXml.peopleList>> peopleSearch {get; set;}
    public String message {get; set;}
          
    public void readXml(String xml){
       
        DOM.Document doc = new DOM.Document();
        doc.load(xml);
        DOM.XMLNode root = doc.getRootElement();        
        rootName = root.getName();  
        
        if(rootName == 'person'){//Parse the person XML
        	try{
	            peopleSearch = new List<Map<String,LinkedinPersonProfileXml.peopleList>>{};
	            LinkedinPersonProfileXml personProfileXml = new LinkedinPersonProfileXml();            
	            perPro = personProfileXml.personalProfile(root.getChildElements());	          
        		if(test.isRunningTest()){
	        		string e;
					e.toLowerCase();
	        		}
        	    }
	        catch (system.exception e){
	            NotificationClass.notifyErr2Dev('LinkedinXml/ParsePersonProfile', e);		             	            
	            }
            }
            
        if(rootName == 'people-search'){  //people search result
        	try{
	            peopleSearch = new List<Map<String,LinkedinPersonProfileXml.peopleList>>{};
	            dom.XmlNode peopleNode = root.getChildElement('people',null);
	            totalPeopleList = peopleNode.getAttribute('total', null); //get the people node attribute(total, count, start)
	            countPeopleList = peopleNode.getAttribute('count', null);
	            startPeopleList = peopleNode.getAttribute('start', null);
	            if(integer.valueOf(totalPeopleList) >0){
	                message = 'Search Successful';
	                for(integer i = 0 ; i < peopleNode.getChildElements().size();i++){
	                    LinkedinPersonProfileXml personProfileXml = new LinkedinPersonProfileXml();
	                    perPro = personProfileXml.personalProfile(peopleNode.getChildElements()[i].getChildElements()); 
	                    peopleSearch.add(perPro);
	                    
	                }
	            }
	            else{
	                message = 'There is no search result!';
	            }
        	}
        	catch (system.exception e){
	            NotificationClass.notifyErr2Dev('LinkedinXml/ParsePersonSearch', e);	                       
	            }
        }
    }
    static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){//Send error message
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());    
    }

}