/**
 * This is the generic Controller class for Job Posting for different Job Boards
 * Created by: Lina Wei
 * Created on: 02/03/2017.
 */

public with sharing class JobBoardPostingController{

    public JobBoard jobBoard { get; set; }
    public Advertisement__c ad { get; set; }
    public Advertisement__c adToPost { get; set; }

    public JobBoardPostingController(ApexPages.StandardController stdController){
        ad = (Advertisement__c) stdController.getRecord();
    }

    // init for posting ad
    public void postInit(){
        adToPost = new AdvertisementSelector().getAdForSeekClone(ad.Id);
        adToPost.RecordTypeId = ad.RecordTypeId;
        jobBoard = JobBoardFactory.getJobBoard(adToPost);
        jobBoard.postInit();
        ad.RecordTypeId = DaoRecordType.templateAdRT.Id;
    }

    // init for update ad
    public void updateInit() {
        jobBoard = JobBoardFactory.getJobBoard(ad);
        jobBoard.updateInit();
    }

    // init for clone ad
    public void cloneInit() {
        jobBoard = JobBoardFactory.getJobBoard(ad);
        jobBoard.cloneInit();
    }

    // init for archive ad
    public void archiveInit() {
        jobBoard = JobBoardFactory.getJobBoard(ad);
        jobBoard.archiveInit();
    }

    // method for post ad
    public PageReference postAd(){
        return jobBoard.postAd();
    }

    // method to update ad
    public PageReference updateAd() {
        return jobBoard.updateAd();
    }

    // method to clone ad
    public PageReference cloneAd() {
        return jobBoard.cloneAd();
    }

    // method to archive ad
    public PageReference archiveAd() {
        return jobBoard.archiveAd();
    }

    public PageReference cancelClone() {
        return jobBoard.cancelClone();
    }

    public PageReference cancelArchive() {
        return jobBoard.cancelArchive();
    }
    /*
     * Get all enabled skill groups as select options
     **/
    public List<SelectOption> getSkillGroupSelectOptions() {
        return jobBoard.getSkillGroupSelectOptions();
    }

    /*
     * Get all available application form style
     */
    public List<SelectOption> getApplicationStyleSelectOptions() {
        return jobBoard.getApplicationStyleSelectOptions();
    }

    /*
     * Get Video Position picklist value as select options
     */
    public List<SelectOption> getVideoPositionSelectOptions() {
        return jobBoard.getVideoPositionSelectOptions();
    }

    public PageReference backToAdDetailPage(){
        return jobBoard.backToAdDetailPage();
    }
}