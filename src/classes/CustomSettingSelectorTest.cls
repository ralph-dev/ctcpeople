@isTest
private class CustomSettingSelectorTest
{
	@isTest
	static void getS3S3CredentialTest()
	{
		S3Credential__c s3Credential = S3Credential__c.getOrgDefaults();
		s3Credential.Access_Key_ID__c = '123456789';
		s3Credential.Secret_Access_Key__c = '123456789';
		insert s3Credential;

		CustomSettingSelector cSettingSelector = new CustomSettingSelector();
		S3Credential__c selectCredential = cSettingSelector.getOrgS3Credential();
		system.assertEquals(selectCredential.Access_Key_ID__c, '123456789');
	}
}