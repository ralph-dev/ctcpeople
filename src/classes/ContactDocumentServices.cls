public class ContactDocumentServices {
    @future(callout=true)
     public static void UploaddocumentsByEc2(String jsonString){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        String searchEndpoint = EndPoint.getcpewsEndpoint()+'/CTCPeopleUploadFiles/docs';
        
        jsonString='{"activityDocs":' + jsonString + '}'; //Wrap Json string to activityDocs JSON string, according Alvin Request
        jsonString = 'activityParams=' + jsonString;
        
        req.setEndpoint(searchEndpoint);
        req.setMethod('POST'); 
        req.setTimeout(120000);
        req.setBody(jsonString);
        HttpResponse res = new HttpResponse();
        if(!Test.isRunningTest()) {
            res = h.send(req);
        }else {
            res.setStatusCode(200);
            res.setBody('Successfully');
        }          
        Integer responsecode = res.getStatusCode();
    }
}