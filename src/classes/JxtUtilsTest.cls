/**
 * This is the test class for JxtUtils
 * 
 * Created by: Lina & Lorena
 * Created on: 14/10/2016
 **/ 

@isTest
public class JxtUtilsTest {
    
    // This test is mainly used to cover some if/else code, major test is done in JXTPostTest
    static testmethod void testGenerateJobXML() {
    	System.runAs(DummyRecordCreator.platformUser) {
        //create data
        StyleCategory__c sc = new StyleCategory__c();
		sc = StyleCategoryDummyRecordCreator.setJobboardStyleCategory();
	    Placement__c v = new Placement__c();
		insert v;
		Advertisement__c ad = AdvertisementDummyRecordCreator.generateJXTAdDummyRecord();
		ad.Vacancy__c=v.id;
		ad.Application_Form_Style__c = sc.Id;
		ad.Residency_Required__c = false;
		ad.Location_Hide__c = true;
		ad.Salary_Description__c = 'a\'';
		ad.Hot_Job__c = false;
		ad.Has_Referral_Fee__c = 'FALSE';
		ad.ClassificationCode__c = '0';
		ad.SubClassificationCode__c = '0';
		ad.Classification2Code__c = '0';
		ad.SubClassification2Code__c = '0';
        insert ad;
        String jobJson = JxtUtils.getFeed(ad);
        system.assert(String.isNotBlank(jobJson));
    	}
    }
    
	private static testmethod void getReferralItemsTest() {
		System.runAs(DummyRecordCreator.platformUser) {
	    system.assert(JxtUtils.getReferralItems().size()==2);
		}
	}
	
	private static testmethod void IsUrlValidTest() {
		System.runAs(DummyRecordCreator.platformUser) {
	    system.assert(!JxtUtils.IsUrlValid(''));
	    system.assert(JxtUtils.IsUrlValid('https://'));
		}
	}
	
	
}