public with sharing class RatingPeopleController {
    Contact con;
    public RatingPeopleController(ApexPages.StandardController controller) {
        this.con = (Contact)controller.getRecord();
    }
    public PageReference Save(){
        fflib_SecurityUtils.checkFieldIsUpdateable(Contact.SObjectType, Contact.People_Rating_c__c);
        update con;
        return null;
    }
    public static testMethod void testController(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Account a = new Account(Name = 'test');
        insert a ; 
        
        Contact c = new Contact(LastName ='test',AccountId = a.Id, Email ='test@user2test.com');
        insert c;
        
        ApexPages.StandardController controller = new ApexPages.StandardController(c);
        RatingpeopleController thecontroller = new RatingPeopleController(controller);
        pageReference testpagereference = thecontroller.save();
        system.assert(testpagereference == null);
    	}
    }


}