/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ContactAstutePayrollImplementationTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		list<Contact> billerContacts=DataTestFactory.createContacts();
	    	for(Integer i=0;i<billerContacts.size();i++){
	    		
	    		
	    		if(math.mod(i,5)==0){
	    			billerContacts.get(i).Is_Biller_Contact_Pushing_To_AP__c =true;
	    			billerContacts.get(i).Biller_Contact_Id__c=i;
	    		}
	    		if(math.mod(i,5)==1){
	    			billerContacts.get(i).Is_Fixing_Data_For_Biller_Contact_in_AP__c=true;
	    			billerContacts.get(i).Approver_User_Name__c='test'+i;
	    		}
	    		if(math.mod(i,5)==2){
	    			billerContacts.get(i).Biller_Contact_Id__c=i;
	    		}
	    		if(math.mod(i,5)==3){
	    			billerContacts.get(i).Is_Fixing_Data_for_Approver_in_AP__c= true;
	    		}
	    		if(math.mod(i,5)==4){
	    			billerContacts.get(i).Is_Fixing_Data_for_Approver_in_AP__c= false;
	    			billerContacts.get(i).Is_Approver_Pushing_To_AP__c= true;
	    		}
	    	}
	    	List<String> contactIds = new List<String>();
	    	list<Contact> cands=DataTestFactory.createCands();
	    	DataTestFactory.createCandidateManagements();
	    	for(Contact cand: cands){
	    		cand.Is_Employee_Pushing_To_AP__c=true;
	    		contactIds.add(cand.Id);
	    	}
	    	ContactAstutePayrollImplementation.astutePayrollTrigger(billerContacts);
	    	ContactAstutePayrollImplementation.astutePayrollTrigger(cands);
	    	System.assertEquals(cands.get(0).Astute_Payroll_Upload_Status__c,'On Hold');
	    	String session = AstutePayrollUtils.GetloginSession();
	        ContactAstutePayrollImplementation.syncBillerContacts(contactIds, session);
	    	ContactAstutePayrollImplementation.syncApproverContacts(contactIds, session);
	    	ContactAstutePayrollImplementation.syncEmployeeContacts(contactIds, session);
    	}
    	
    }
}