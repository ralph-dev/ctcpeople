@isTest
private class DaoDocumentTest {

	private static testMethod void test() {
		System.runAs(DummyRecordCreator.platformUser) {
        system.assertEquals(null, DaoDocument.getDocById('a049000000u5iOJ'));
        system.assertNotEquals(null, DaoDocument.getDocsByFolder('a049000000u5iOJ'));
        system.assertEquals(null, DaoDocument.getSendResumeDummyDoc('a049000000u5iOJ'));
        system.assertEquals(null, DaoDocument.getCandidateRecordDummyDoc('a049000000u5iOJ'));
        system.assertNotEquals(null, DaoDocument.getOldSendResumeDummyDoc('a049000000u5iOJ', 'test'));
        system.assertNotEquals(null, DaoDocument.getCTCSearchCriteriaDocument('a049000000u5iOJ', 'test'));
        system.assertEquals(null, DaoDocument.fetchdocuments('a049000000u5iOJ', 'test'));
        system.assertNotEquals(null, DaoDocument.createdocunderpersonalfolder('<xml></xml>', 'Contact', 'Contact'));
        system.assertNotEquals(null, DaoDocument.createdocunderCTCADMINfolder('<xml></xml>', 'Contact', 'Contact'));
		}
	}

	public static testmethod void testGetCTCConfigFileByDevName(){
		System.runAs(DummyRecordCreator.platformUser) {
		Document doc;
		String fileName;
		
		fileName = 'ClicktoCloud_Xml_Placement';
		doc = DaoDocument.getCTCConfigFileByDevName(fileName);
		system.assert(doc.Name.contains('ClicktoCloud_Xml'));
		
		String docfile = '';
		String ObjectString = 'Contact';
		String bodyString = '<xml></xml>';
		Document tempdoc = new Document();
		tempdoc = DaoDocument.createdocunderpersonalfolder(bodyString,  ObjectString, ObjectString);
		system.assert(tempdoc!=null);
		
		system.assertNotEquals(null, DaoDocument.createdocunderCTCADMINfolder(bodyString,  ObjectString, ObjectString));
		}
	}
}