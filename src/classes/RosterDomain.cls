public with sharing class RosterDomain extends SObjectDomain {
	private Days_Unavailable__c sglRoster;
	private static String NAME_SPACE_PREFIX = PeopleCloudHelper.getPackageNamespace();
	
	public RosterDomain(List<Days_Unavailable__c> rosters) {
		super(rosters);
	}

	public RosterDomain(Days_Unavailable__c roster) {
		super(new List<Days_Unavailable__c>{roster});
		this.sglRoster = roster;
	}

	public RosterDomain(){
		super(new List<Days_Unavailable__c>{});
	}
	
	public List<Days_Unavailable__c> splitRosterForDisplay(){
		List<List<Days_Unavailable__c>> rosterListCollection = new List<List<Days_Unavailable__c>>();
		List<Days_Unavailable__c> rosterListForDisplayTotal = new List<Days_Unavailable__c>();
		
		for(SObject rec : records){
			List<Days_Unavailable__c> rosterListForDisplay = new List<Days_Unavailable__c>();
			Days_Unavailable__c roster = (Days_Unavailable__c)rec;
			
			//convert start date, end date to datetime format
			DateTime startDateTime = WizardUtils.convertDateToDateTime(roster.Start_Date__c);
			DateTime endDateTime = WizardUtils.convertDateToDateTime(roster.End_Date__c);
			
			//single recur dates from start date to recur end date
			List<DateTime> recurDTs = new List<DateTime>();
			
			//recurWeekdays is a multipicklist value
			//convert to a (weekday, weekday) map
			String recurWeekdays = roster.Recurrence_Weekdays__c;
			if(recurWeekdays == '' || recurWeekdays == null){
				return null;
			}
			
			//System.debug('recurWeekdays: '+recurWeekdays);
			Map<String,String> recurWeekdaysMap = WizardUtils.convertStringToMap(recurWeekdays);
			System.debug('recurWeekdaysMap Size: '+recurWeekdaysMap.size());
			
			if(roster.Recurrence_End_Date__c == null){
				roster.Recurrence_End_Date__c = roster.End_Date__c;
			}
			
			DateTime recurEndDateTime = WizardUtils.convertDateToDateTime(roster.Recurrence_End_Date__c);
			
			for(DateTime i = startDateTime; i < WizardUtils.nextDT(recurEndDateTime); i = WizardUtils.nextDT(i)){
				String iWeekday = i.format('EEE');
	
				if(recurWeekdaysMap.containsKey(iWeekday)){
					recurDTs.add(i);
				}
			}
			
			List<Days_Unavailable__c> tempRosterArr = new List<Days_Unavailable__c>();
			
			for(Integer i=0; i<recurDTs.size();i++){
				Days_Unavailable__c sglRoster = new Days_Unavailable__c();
				sglRoster.Start_Date__c = Date.newInstance(recurDTs[i].year(),recurDTs[i].month(), recurDTs[i].day());
				sglRoster.End_Date__c = Date.newInstance(recurDTs[i].year(),recurDTs[i].month(), recurDTs[i].day());
				tempRosterArr.add(sglRoster);
			}
			//System.debug('tempRosterArr'+tempRosterArr);
			
			for(Integer i=0; i<tempRosterArr.size();i++){
				Days_Unavailable__c sglRoster = tempRosterArr[i];
				RosterHelper.mergeRoster(rosterListForDisplay,sglRoster);
			}
			
			for(Integer i=0;i<rosterListForDisplay.size();i++){
				Days_Unavailable__c sglRoster = rosterListForDisplay[i];
				RosterHelper.assignValuesToRoster(sglRoster, roster);
			}
			rosterListCollection.add(rosterListForDisplay);
		}
		
		for(List<Days_Unavailable__c> rList : rosterListCollection){
			for(Days_Unavailable__c r : rList){
				rosterListForDisplayTotal.add(r);
			}
		}
		//System.debug('rosterListForDisplayTotal: '+rosterListForDisplayTotal);	
		
		return rosterListForDisplayTotal;
	}
	
	public void updateRosterBasedOnShift(Map<String,Shift__c> shiftMap){
		List<Days_Unavailable__c> rostersNeedToBeUpdated = new List<Days_Unavailable__c>();
		
		for(SObject record : records){
			Days_Unavailable__c roster = (Days_Unavailable__c) record;
			
			if(shiftMap.containsKey(roster.Shift__c)){
				Shift__c s = shiftMap.get(roster.Shift__c);
				roster.Start_Date__c = s.Start_Date__c;
				roster.End_Date__c = s.End_Date__c;
				roster.Start_Time__c = s.Start_Time__c;
				roster.End_Time__c = s.End_Time__c;
				roster.Shift_Type__c = s.Shift_Type__c;
				roster.Weekly_Recurrence__c = s.Weekly_Recurrence__c;
				roster.Recurrence_Weekdays__c = s.Recurrence_Weekdays__c;
				roster.Recurrence_End_Date__c = s.Recurrence_End_Date__c;
				roster.Rate__c = s.Rate__c;
			}
			
			rostersNeedToBeUpdated.add(roster);
		}
		
		if(rostersNeedToBeUpdated.size() != 0 &&  rostersNeedToBeUpdated != null){
			CommonSelector.quickUpdate(rostersNeedToBeUpdated,'Start_Date__c,End_Date__c,Start_Time__c,End_Time__c,Shift_Type__c,Weekly_Recurrence__c,Recurrence_Weekdays__c,Recurrence_End_Date__c,Rate__c');
			//update rostersNeedToBeUpdated;
		}
	}

	//create Rostering Records
	public Boolean createRosteringRecords(String vacancyId, List<Shift__c> shifts, List<candidateDTO> candidateDTOs) {
		List<Days_Unavailable__c> rosterings = new List<Days_Unavailable__c>();
		if(shifts!=null&&candidateDTOs!=null&&shifts.size()>0&&candidateDTOs.size()>0) {
			
			//get Roster record type
			RecordTypeServices rtServices = new RecordTypeServices();
			Map<String, String> rtMap = rtServices.getRecordTypeMap();
			String rtId = rtMap.get('Roster');
			
			Map<String, Placement_Candidate__c> conCMMap = new Map<String, Placement_Candidate__c>();
			PlacementCandidateDomain pCandidateDomain = new PlacementCandidateDomain();
			conCMMap = pCandidateDomain.createPlacementCandidates(candidateDTOs, vacancyId);

			Set<String> candidatesId = new Set<String>();
			candidatesId = conCMMap.keySet();
			
			RosterSelector rSelector = new RosterSelector();
			Map<String, Days_Unavailable__c> getDuplicateRosterRecord = new Map<String, Days_Unavailable__c>();
			getDuplicateRosterRecord =rSelector.getDuplicateRoster(candidatesId, shifts);

			for(Shift__c shift: shifts) {
				for(candidateDTO canDTO : candidateDTOs){
					String rosterUniqueId = shift.Id+':'+canDTO.getCandidate().Id;
					system.debug('rosterUniqueId ='+ rosterUniqueId);
					if(!getDuplicateRosterRecord.containsKey(rosterUniqueId)){
						Days_Unavailable__c rostering = new Days_Unavailable__c();
						rostering.recordTypeId = rtId;
						rostering.Shift__c = shift.Id;
						rostering.Start_Date__c = shift.Start_Date__c;
						rostering.End_Date__c = shift.End_Date__c;
						rostering.Start_Time__c = shift.Start_Time__c;
						rostering.End_Time__c = shift.End_Time__c;
						rostering.Shift_Type__c = shift.Shift_Type__c;
						rostering.Event_Status__c = canDTO.getEventStatus();
						rostering.Location__c = shift.Location__c;
						rostering.Recurrence_Weekdays__c = shift.Recurrence_Weekdays__c;
						rostering.Recurrence_End_Date__c = shift.Recurrence_End_Date__c;
						rostering.Contact__c = canDTO.getCandidate().Id;
						rostering.Accounts__c = shift.Account__c;
						rostering.Weekly_Recurrence__c = shift.Weekly_Recurrence__c;
						rostering.Candidate_Management__c = conCMMap.get(canDTO.getCandidate().Id).Id;
						rostering.Rate__c = shift.Rate__c;
						rosterings.add(rostering);
					}
				}
			}
			
		}
		//system.debug('rosterings ='+ rosterings);
		//check FLS
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			Days_Unavailable__c.RecordTypeId,
			Days_Unavailable__c.Shift__c,
			Days_Unavailable__c.Start_Date__c,
			Days_Unavailable__c.End_Date__c,
			Days_Unavailable__c.Start_Time__c,
			Days_Unavailable__c.End_Time__c,
			Days_Unavailable__c.Shift_Type__c,
			Days_Unavailable__c.Event_Status__c,
			Days_Unavailable__c.Location__c,
			Days_Unavailable__c.Recurrence_Weekdays__c,
			Days_Unavailable__c.Recurrence_End_Date__c,
			Days_Unavailable__c.Candidate_Management__c,
			Days_Unavailable__c.Rate__c
		};
		fflib_SecurityUtils.checkInsert(Days_Unavailable__c.SObjectType, fieldList);
		insert rosterings;
		
		return true;
	}
}