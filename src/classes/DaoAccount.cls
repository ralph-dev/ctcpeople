public with sharing class DaoAccount extends CommonSelector{

public DaoAccount(){
	super(Account.sObjectType);
}


private static DaoAccount dao = new DaoAccount();
private static String defaultFields = 'id, Name,Website, Type, Site, Sic, '
				+'ShippingStreet, ShippingState, ShippingPostalCode, '
				+'ShippingCountry, ShippingCity, Rating, Phone, '
			 	+'Type__c, Total_Staff__c, '
				+'Status__c, Sales_Staff__c, '
				+'Projected_Annual_Account_Value__c, '
			 	+'Average_Placement_Value_8__c, '
				+'Average_Placement_Value_7__c, Average_Placement_Value_6__c, '
				+'Average_Placement_Value_5__c, Average_Placement_Value_4__c, '
				+'Average_Placement_Value_3__c, Average_Placement_Value_2__c,' 
				+'Average_Placement_Value_1__c,  OwnerId, NumberOfEmployees,' 
				+'Industry, Fax, Description, BillingStreet, '
				+'BillingState, BillingPostalCode, BillingCountry, BillingCity, '
				+'AnnualRevenue, AccountNumber,RecordTypeID';
				
static public List<Account> getAccsContainName(String name){
	name= String.escapeSingleQuotes(name); //To avoid SOQL injection
    name= name.endsWith('%') ? name: name+ '%';
    name= name.startsWith('%') ? name: '%' + name;
    List<Account> result = null;
    try{
    	
    	dao.checkRead(defaultFields);
    	CommonSelector.checkRead(Contact.sObjectType,'Id, Name,Email,AccountId,createdDate');
    	result=[select id, Name,Website, Type, Site, Sic, 
				ShippingStreet, ShippingState, ShippingPostalCode, 
				ShippingCountry, ShippingCity, Rating, Phone, 
			 	Type__c, Total_Staff__c, 
				Status__c, Sales_Staff__c, 
				Projected_Annual_Account_Value__c, 
			 	Average_Placement_Value_8__c, 
				Average_Placement_Value_7__c, Average_Placement_Value_6__c, 
				Average_Placement_Value_5__c, Average_Placement_Value_4__c, 
				Average_Placement_Value_3__c, Average_Placement_Value_2__c, 
				Average_Placement_Value_1__c,  OwnerId, NumberOfEmployees, 
				Industry, Fax, Description, BillingStreet, 
				BillingState, BillingPostalCode, BillingCountry, BillingCity, 
				AnnualRevenue, AccountNumber,RecordTypeID,(select Id, Name,Email,AccountId,createdDate from Account.Contacts) 
            	from Account where Name like :name order by Name];
    }catch(Exception e){
    	result = new List<Account>();
    }
	return result; 
}

static public List<Account> getAccsByIds(List<Id> accIds){
	List<Account> result = null;
    try{
    	
    	dao.checkRead(defaultFields);
    	CommonSelector.checkRead(Contact.sObjectType,'Id, Name,Email,AccountId');
    	
		result =[select id, Name,Website, Type, Site, Sic, 
				ShippingStreet, ShippingState, ShippingPostalCode, 
				ShippingCountry, ShippingCity, Rating, Phone, 
				Type__c, Total_Staff__c, 
				Status__c, Sales_Staff__c, 
				Projected_Annual_Account_Value__c, 
			    Average_Placement_Value_8__c, 
				Average_Placement_Value_7__c, Average_Placement_Value_6__c, 
				Average_Placement_Value_5__c, Average_Placement_Value_4__c, 
				Average_Placement_Value_3__c, Average_Placement_Value_2__c, 
				Average_Placement_Value_1__c,  OwnerId, NumberOfEmployees, 
				Industry, Fax, Description, BillingStreet, 
				BillingState, BillingPostalCode, BillingCountry, BillingCity, 
				AnnualRevenue, AccountNumber,RecordTypeID,(select Id, Name,Email,AccountId from Account.Contacts) 
            	from Account where Id in:accIds order by Name];
    }catch (Exception e){
    	result = new List<Account>();
    }
    return result;
}

static public List<Account> getAccsByRecordTypeIdInDescCreatedDate(Id rtId){
	List<Account> result = null;
    try{
    	
    	dao.checkRead('id, Name,RecordTypeID,createdDate');
    	
		result =[select id, Name,RecordTypeID,createdDate
            	from Account where RecordTypeId =:rtId order by CreatedDate desc];
    }catch (Exception e){
    	result = new List<Account>();
    }
    return result;
}

static public Account getAccsById(Id accId){
	Account result = null;
    try{
    	
    	dao.checkRead(defaultFields);
    	CommonSelector.checkRead(Contact.sObjectType,'Id, Name,Email,AccountId');
    	
    	
		result =[select id, Name,Website, Type, Site, Sic, 
				ShippingStreet, ShippingState, ShippingPostalCode, 
				ShippingCountry, ShippingCity, Rating, Phone, 
				Type__c, Total_Staff__c, 
				Status__c, Sales_Staff__c, 
				Projected_Annual_Account_Value__c, 
			    Average_Placement_Value_8__c, 
				Average_Placement_Value_7__c, Average_Placement_Value_6__c, 
				Average_Placement_Value_5__c, Average_Placement_Value_4__c, 
				Average_Placement_Value_3__c, Average_Placement_Value_2__c, 
				Average_Placement_Value_1__c,  OwnerId, NumberOfEmployees, 
				Industry, Fax, Description, BillingStreet, 
				BillingState, BillingPostalCode, BillingCountry, BillingCity, 
				AnnualRevenue, AccountNumber,RecordTypeID,(select Id, Name,Email,AccountId from Account.Contacts) 
            	from Account where Id =:accId order by Name];
    }catch (Exception e){
    	result = null;
    }
    return result;
}

static public List<Account> getAccsContainNameSR(String name){
	name= String.escapeSingleQuotes(name); //To avoid SOQL injection
    name= name.endsWith('%') ? name: name+ '%';
    name= name.startsWith('%') ? name: '%' + name;
    List<Account> result = null;
    try{
    	
    	dao.checkRead(defaultFields);
    	
    	result=[select id, Name,Website, Type, Site, Sic, 
				ShippingStreet, ShippingState, ShippingPostalCode, 
				ShippingCountry, ShippingCity, Rating, Phone, 
			 	Type__c, Total_Staff__c, 
				Status__c, Sales_Staff__c, 
				Projected_Annual_Account_Value__c, 
			 	Average_Placement_Value_8__c, 
				Average_Placement_Value_7__c, Average_Placement_Value_6__c, 
				Average_Placement_Value_5__c, Average_Placement_Value_4__c, 
				Average_Placement_Value_3__c, Average_Placement_Value_2__c, 
				Average_Placement_Value_1__c,  OwnerId, NumberOfEmployees, 
				Industry, Fax, Description, BillingStreet, 
				BillingState, BillingPostalCode, BillingCountry, BillingCity, 
				AnnualRevenue, AccountNumber,RecordTypeID 
            	from Account where Name like :name order by Name];
    }catch(Exception e){
    	result = new List<Account>();
    }
	return result; 
}
}