public with sharing class DaoStyleCategory extends CommonSelector {
	
	public DaoStyleCategory(){
		super(StyleCategory__c.sObjectType);
	}
	
	private static DaoStyleCategory dao = new DaoStyleCategory();
	
	static public StyleCategory__c previousSaveSearchCriteriaRecord(String recordtypeid, String recordid){
		StyleCategory__c result;
		try{
			dao.checkRead('id, Name, asquerystring__c, ascomponents__c, assearchobject__c');
			result = [select id, Name, asquerystring__c, ascomponents__c, assearchobject__c from StyleCategory__c where RecordTypeId =: recordtypeid and id=: recordid];
		}
		catch(Exception e){
	    	result = new StyleCategory__c();
	    }
	    return result;
	}
	
	static public List<StyleCategory__c> getOldStyleCategoryLikeDevName(String devName){
		List<StyleCategory__c> result = new List<StyleCategory__c>();
		devName = '%'+devName+'%';
	    try{ 
	    	dao.checkRead('id, Name, asquerystring__c, ascomponents__c, assearchobject__c');
	    	result = [select id, Name, asquerystring__c, ascomponents__c, assearchobject__c from StyleCategory__c where Developer_Name__c like:devName and CreatedDate < today];
	    }catch(Exception e){
	    	result = new List<StyleCategory__c>();
	    }
	    return result;
	}
	
	//Fetch LinkedIn account from StyleCategory. Author by Jack
	static public List<StyleCategory__c> getLinkedInAccount(){
		RecordType linkedInAccountRT = DaoRecordType.linkedInaccountRT;
		List<StyleCategory__c> result = new List<StyleCategory__c>();
		try{
			dao.checkRead('Id, name, Advertiser_Id__c, Max_Jobs__c ');
			result = [select Id, name, Advertiser_Id__c, Max_Jobs__c 
						from StyleCategory__c where RecordTypeId =: linkedInAccountRT.Id and Account_Active__c = true];
		}catch(Exception e){
			result = new List<StyleCategory__c>();
		}
		return result;
	}
	
	//Fetch LinkedIn account from StyleCategory by ID. Author by Jack
	static public StyleCategory__c getLinkedInAccountById(String linkedinaccountid){
		RecordType linkedInAccountRT = DaoRecordType.linkedInaccountRT;
		StyleCategory__c result = new StyleCategory__c();
		try{
			dao.checkRead('Id, name, Advertiser_Id__c, Max_Jobs__c ');
			result = [select Id, name, Advertiser_Id__c, Max_Jobs__c 
						from StyleCategory__c where RecordTypeId =: linkedInAccountRT.Id and Account_Active__c = true and id=:linkedinaccountid];
		}catch(Exception e){
			result = null;
		}
		return result;
	}
	
	//Fetch dynamic application form from StyleCategory. Author by Jack on 24/06/2013
	static public List<StyleCategory__c> getApplicationFormStyle(){
		RecordType applicationFormStyleRT = DaoRecordType.ApplicationFormStyleRT;
		List<StyleCategory__c> result = new List<StyleCategory__c>();
		try{
			
			dao.checkRead('Id, name, Advertiser_Id__c, Max_Jobs__c ');
			result = [select Id, name, Advertiser_Id__c, Max_Jobs__c 
						from StyleCategory__c where RecordTypeId =: applicationFormStyleRT.Id and Active__c = true];
		}catch(Exception e){
			result = new List<StyleCategory__c>();
		}
		return result;
	}
	
	static public boolean checkJobBoardAccountActive(String stylecategoryid){
		Boolean jobBoardAccountActive = false;
		StyleCategory__c currentJobBoardAccountActive = new StyleCategory__c();
		try{
			dao.checkRead('Id, Account_Active__c ');
			currentJobBoardAccountActive = [select Id, Account_Active__c 
						from StyleCategory__c where Id =: stylecategoryid];
			if(currentJobBoardAccountActive!=null){
				jobBoardAccountActive = currentJobBoardAccountActive.Account_Active__c;
			}
		}catch(Exception e){
		
		}
		return jobBoardAccountActive;
	}


	/****************************************************************************************/
	/*************** Depreciated, no longer used ********************************************/
	/*************** This method is used by SeekDetail and SeekEdit *************************/
	/****************************************************************************************/
	//Get Seek accout according StyleCategory id
	static public StyleCategory__c getSeekAccountByStyleCategoryId(String StyleCategoryId){
		StyleCategory__c result = new StyleCategory__c();
		try{
			dao.checkRead('Id, name, Advertiser_Id__c, Min_Jobs__c, Max_Jobs__c, Seek_Default_Application_Form__c ');
			result = [select Id, name, Advertiser_Id__c, Min_Jobs__c, Max_Jobs__c, Seek_Default_Application_Form__c 
						from StyleCategory__c where Id =:StyleCategoryId and Account_Active__c = true];
		}
		catch(Exception e){
			result = new StyleCategory__c();
		}
		return result;
	}
	//Get Seek Screen selection
	static public List<StyleCategory__c> getSeekScreenId(String seekAccountId){
		List<StyleCategory__c> result = new List<StyleCategory__c>();
		try{
			dao.checkRead('Id, Name, screenId__c');
			result = [Select Id, Name, screenId__c from StyleCategory__c where Active__c = true 
	                                    and RecordType.DeveloperName ='seekScreenStyle' and Seek_Account__c =:seekAccountId ORDER BY Name];
		}catch(Exception e){
			result = new List<StyleCategory__c>();
		}
		return result;
	}
	//Fetch seek account from StyleCategory. Author by Jack on 24/06/2013
	static public List<StyleCategory__c> getSeekAccount(){
		RecordType seekAccountRT = DaoRecordType.seekaccountOldRT;
		List<StyleCategory__c> result = new List<StyleCategory__c>();
		try{
			dao.checkRead('Id, name, Advertiser_Id__c, Min_Jobs__c, Max_Jobs__c, Seek_Default_Application_Form__c');
			result = [select Id, name, Advertiser_Id__c, Min_Jobs__c, Max_Jobs__c, Seek_Default_Application_Form__c
			from StyleCategory__c where RecordTypeId =: seekAccountRT.Id and Account_Active__c = true];
		}catch(Exception e){
			result = new List<StyleCategory__c>();
		}
		return result;
	}
	/****************************************************************************************/
	/****************************************************************************************/


	/****************************************************************************************/
	/****************** Depreciated, no longer used *****************************************/
	/****************** This method is used by GoogleSearch2.cls ****************************/
	/****************************************************************************************/
	//Get Seek accout according StyleCategory id
	static public List<StyleCategory__c> getGSRStyleCategoryLikeUniqueName(String uniqueName){
		List<StyleCategory__c> result = new List<StyleCategory__c>();
		try{
			dao.checkRead('id, Name');
			result = [select id, Name from StyleCategory__c where Unique_Search_Record_Name__c =: uniqueName];
		}catch(Exception e){
			result = new List<StyleCategory__c>();
		}
		return result;
	}

	static public List<StyleCategory__c> getGoogleSavedSearchesNotOwner(Id ownerId){
		List<StyleCategory__c> result = new List<StyleCategory__c>();
		try{
			RecordType googleSavedSearchRT = DaoRecordType.GoogleSaveSearchRT;
			dao.checkRead('id, Name,Unique_Search_Record_Name__c, ascomponents__c, assearchobject__c');
			result = [select id, Name,Unique_Search_Record_Name__c, ascomponents__c, assearchobject__c
			from StyleCategory__c where ownerId!=:ownerId and RecordTypeId=:googleSavedSearchRT.id
			order by Name];
		}catch(Exception e){
			result = new List<StyleCategory__c>();
		}
		return result;
	}

	static public List<StyleCategory__c> getGoogleSavedSearchesByOwner(Id ownerId){
		List<StyleCategory__c> result = new List<StyleCategory__c>();
		try{
			RecordType googleSavedSearchRT = DaoRecordType.GoogleSaveSearchRT;
	    	/**Edited by Alvin --To Filter the stylecategory for dummy records**/
	    	
	    	dao.checkRead('id, Name,Unique_Search_Record_Name__c, ascomponents__c, assearchobject__c');
			result = [select id, Name,Unique_Search_Record_Name__c, ascomponents__c, assearchobject__c
			from StyleCategory__c where ownerId=:ownerId and RecordTypeId=:googleSavedSearchRT.id
			and (Not Name like  'Bulk_SEND_TEMP_%')  order by Name];
		}catch(Exception e){
			result = new List<StyleCategory__c>();
		}
		return result;
	}
	/****************************************************************************************/
	/****************************************************************************************/

}