@isTest
private class AdvertisementUtilitiesTest
{
	@isTest
	static void testConvertString(){
		String stringWithSpecialChar = 'M&T is our customer.\'';
		stringWithSpecialChar = AdvertisementUtilities.ConvertString(stringWithSpecialChar);
		system.debug('stringWithSpecialChar ='+ stringWithSpecialChar);
		system.assert(!stringWithSpecialChar.contains('\\'));
	}
}