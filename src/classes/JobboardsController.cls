/**
	This controller is to control what job board tabs will be 
	showing during the job posting process.
**/
public with sharing class JobboardsController{
    //***** this controller is used in pages jobboards and PostJobStep1 both

    public User currentUser {get;set;}
    Public boolean hasError{get;set;}
    String jobboards ;
    public String warninginfo{get;set;}
    
    /*-- flag attribute for controlling the display of job board option--*/
    public Boolean client_has_seek{get;set;}
    public Boolean client_has_c1{get;set;}
    public Boolean client_has_trademe{get;set;}
    public Boolean client_has_JXT{get;set;}
    public Boolean client_has_JXT_NZ{get;set;}
    public Boolean client_has_linkedIn{get; set;}
    public Boolean client_has_Indeed{get; set;}
    public Boolean client_has_website{get;set;}
    public Boolean client_has_mycareer{get; set;}
    
    public Boolean seek_quota_exceed {get;set;}
    public Boolean career1_quota_exceed {get;set;}
    public integer seek_count {get;set;}
    public integer career1_count {get;set;}
    public integer seek_remaining {get;set;}
    public integer career1_remaining {get;set;}
    public integer seek_quota {get;set;}
    public integer career1_quota {get;set;}
    
    public Boolean hasWebSite{get; set;}
    
    /*-- capture if corresponding job board has been selected --*/
    public Boolean seek_selected{get;set;}
    public Boolean career1_selected{get;set;}
    public Boolean trademe_selected{get;set;}
    public Boolean jxt_selected{get;set;}
    public Boolean jxt_nz_selected{get;set;}
    public Boolean website_selected{get;set;}
    public Boolean linkedin_selected{get; set;}
    public Boolean Indeed_selected{get; set;}
    
    /**
    	Constructor. Constructor will initilized the members and check if user has
    	enough privilege to post ads. After constructor is called,  method JobBoardsInit
    	is called to check if user have quota and what job boards user has chosen to post job to.
    */
    public JobboardsController() {
         currentUser = new UserSelector().getCurrentUser();
         hasError = false;
         client_has_seek = false;
         client_has_c1 = false;
         client_has_trademe = false;
         client_has_JXT = false;
         client_has_JXT_NZ = false;
         client_has_linkedIn = false;
         client_has_Indeed = false;
         client_has_website = false;
         warninginfo = '';
         seek_quota_exceed = true;
         career1_quota_exceed = true;
         checkPrivilege();
         
         if(Test.isRunningTest()) {
             hasWebSite = true;
         } else {
             hasWebSite = PCAppSwitch__c.getInstance(UserInfo.getUserId()).Web_Site__c;
         }
        
    }

    public void quotaChecking(){
        //**************************** quota usage ******************************
        seek_quota = (currentUser.Monthly_Quota_Seek__c == null )? 0 : Integer.valueOf(currentUser.Monthly_Quota_Seek__c);
        career1_quota = (currentUser.Monthly_Quota_CareerOne__c == null )? 0 : Integer.valueOf(currentUser.Monthly_Quota_CareerOne__c);

        seek_count = (currentUser.Seek_Usage__c == null )? 0 : Integer.valueOf(currentUser.Seek_Usage__c);
        career1_count = (currentUser.CareerOne_Usage__c == null)? 0 : Integer.valueOf(currentUser.CareerOne_Usage__c);

        seek_remaining = seek_quota - seek_count;
        if (seek_remaining > 0) {
            seek_quota_exceed = false;
        } else {
            addwarningmsg('You have run out of this month quota for Seek job posting!');
        }

        career1_remaining = career1_quota - career1_count;
        if (career1_remaining > 0) {
            career1_quota_exceed = false;
        } else {
            addwarningmsg('You have run out of this month quota for Seek job posting!');
        }
    }

    public void checkPrivilege(){
        hasError = false;

        // Check If the current user has been assigned with active account

        if (JobBoardUtils.checkUserHasActiveJobBoardAccount(currentUser.Seek_Account__c, DaoRecordType.seekAccountRT.Id)) {
            client_has_seek = true;
        }
        if (JobBoardUtils.available('careerone')) {
            client_has_c1 = true;
        }
        if(JobBoardUtils.trademeAvailable()){
            client_has_trademe = true;
        } 
        if(JobBoardUtils.checkUserHasActiveJobBoardAccount(DaoRecordType.jxtAccountRT.Id)){
            client_has_JXT = true;
        }
        if(JobBoardUtils.available('JXT_NZ') && JobBoardUtils.whiteLabelAvailable('JXT_NZ')){
            client_has_JXT_NZ = true;
        }
        if(JobBoardUtils.JobBoardAvailableCheck(currentUser.LinkedIn_Account__c)){
            client_has_linkedIn = true;
        }
        if(JobBoardUtils.checkUserHasActiveJobBoardAccount(currentUser.Indeed_Account__c, DaoRecordType.IndeedAdRT.Id)){
            client_has_Indeed = true;
        }
        if(currentUser.AmazonS3_Folder__c == null || currentUser.AmazonS3_Folder__c == ''){
            ApexPages.Message error= new ApexPages.Message(ApexPages.severity.INFO,'Your user details are not valid to post job online. Please contact your System Administrator.');
            ApexPages.addMessage(error);
            hasError = true;
        }     
    }

    public void adderrormsg(String errinfo){
        ApexPages.Message err = new ApexPages.Message(ApexPages.severity.INFO,errinfo);
        ApexPages.addMessage(err);
        hasError = true;
    }

    public void addwarningmsg(String info){
        warninginfo += info +  '<br/>';
    }

	public pageReference gotoJobBoards(){
	    String aId = ApexPages.currentPage().getParameters().get('aid');
        if(aId == null)
	    {
	        return null;
	    }
	    string url = '/apex/jobBoards?id='+aId;
	    if(seek_selected != null)
	    {
	        url += seek_selected ? '&s=1' : ''; 
	    }
	    
	    if(career1_selected != null)
	    {
	        url += career1_selected ? '&c=1' : ''; 
	    }
	    if(trademe_selected != null)
	    {
	        url += trademe_selected ? '&t=1' : ''; 
	    }
	    if(jxt_selected != null)
	    {
	        url += jxt_selected ? '&j=1' : ''; 
	    }
	    if(jxt_nz_selected != null)
	    {
	        url += jxt_nz_selected ? '&jnz=1' : ''; 
	    }
	    if(website_selected != null)
	    {
	        url += website_selected ? '&ws=1' : ''; 
	    }
	    if(linkedin_selected != null)
	    {
	        url += linkedin_selected ? '&li=1' : ''; 
	    }
        if(Indeed_selected != null)
        {
            url += Indeed_selected ? '&ind=1' : ''; 
        }
	    PageReference pageRef = new PageReference(url);
	    pageRef.setRedirect(true);
	    return pageRef;
	}
	
	public String s_selected{get;set;}
	public String m_selected{get;set;}
	public String c_selected{get;set;}
	public String t_selected{get;set;}
	public String j_selected{get;set;}
	public String jnz_selected{get;set;}
	public String ws_selected{get;set;}
	public String li_selected{get;set;}
    public String id_selected{get;set;}
	//******* jobboards page ********
	
	public void JobBoardsInit(){
         quotaChecking();
         s_selected = ApexPages.currentPage().getParameters().get('s');
         m_selected = ApexPages.currentPage().getParameters().get('m');
         c_selected = ApexPages.currentPage().getParameters().get('c');
         t_selected = ApexPages.currentPage().getParameters().get('t');
         j_selected = ApexPages.currentPage().getParameters().get('j');
         jnz_selected = ApexPages.currentPage().getParameters().get('jnz');
         ws_selected = ApexPages.currentPage().getParameters().get('ws');
         li_selected = ApexPages.currentPage().getParameters().get('li');
         id_selected = ApexPages.currentPage().getParameters().get('ind');
         if(seek_quota_exceed || s_selected == null)
         {
            s_selected  ='0';
         }
         if(career1_quota_exceed || c_selected == null)
         {
            c_selected  ='0';
         }
         if(j_selected == null)
         {
            j_selected  ='0';
         }
         if(jnz_selected == null)
         {
            jnz_selected  ='0';
         }if(id_selected == null) {
            id_selected = '0';
         }
	}

	public class AppFormStyleController{
	    public String headerid{get;private set;}
	    public String footerid{get;private set;}
	    public String divid{get;private set;}   
	    public String divhtmlstr{get; set;}     
	    public AppFormStyleController(){
	        String styleId = ApexPages.currentPage().getParameters().get('id');
	        if(styleId  != null && styleId != '')
	        {
	        	CommonSelector.checkRead(StyleCategory__c.sObjectType, 'Id, Div_Html_File_ID__c, Footer_File_ID__c, Header_File_ID__c ');
		        StyleCategory__c sc = [Select Id, Div_Html_File_ID__c,
		                            Footer_File_ID__c, Header_File_ID__c 
		                            from StyleCategory__c where Id=:styleId ];
		        if(sc != null)
		        {
		            
		            if(sc.Div_Html_File_ID__c != null)
		            {
		                divid = sc.Div_Html_File_ID__c;    
		                CommonSelector.checkRead(   Document.sObjectType, 'Id, Body');
		                Document d = [select Id, Body from Document where id=: divid];
		                divhtmlstr = d.body.toString();
		            }
		            if(sc.Footer_File_ID__c != null)
		            {
		                footerid = sc.Footer_File_ID__c;           
		            }
		            if(sc.Header_File_ID__c != null)
		            {
		                headerid = sc.Header_File_ID__c ;           
		            }
				}                    
	        }
	    }
	}
}