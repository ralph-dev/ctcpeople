/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AstutePayrollFutureJobsTest {

    static testMethod void myUnitTest1() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		HttpResponse httpRes = AstutePayrollFutureJobs.pushHandler('This is a test message', 'TestEndpoint', 'Session');
        	System.assertEquals(httpRes.getBody(), 'Success');
    	}
        
    }
    static testMethod void myUnitTest2() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		list<String> placementIds=new list<String>();
	    	for(Placement_Candidate__c cm: DataTestFactory.createCandidateManagements()){
	    		placementIds.add(cm.Id);
	    	}
	    	HttpResponse httpRes = AstutePayrollFutureJobs.firstTimePush(placementIds, 'Session');
	    	System.assertEquals(httpRes.getBody(), 'Success');
    	}
    	
    }
      static testMethod void myUnitTest3() {
      	TriggerHelper.UpdateAccountField=false;
    	
      	System.runAs(DummyRecordCreator.platformUser) {
      		list<String> invoiceIds=new list<String>();
	    	for(Invoice_Line_Item__c invoiceItem: DataTestFactory.createInvoiceLineItems()){
	    		invoiceIds.add(invoiceItem.Id);
	    	}
	    	HttpResponse httpRes = AstutePayrollFutureJobs.allocateBrandNewInvoiceForAP(invoiceIds, 'Session');
	    	System.assertEquals(httpRes.getBody(), 'Success');
      	}
      	
    }
}