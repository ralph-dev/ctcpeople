@isTest
private class InvoiceItemSelectorTest {
	static testMethod void uniTest(){
		TriggerHelper.UpdateAccountField=false;
		System.runAs(DummyRecordCreator.platformUser) {
		list<Account> accounts=DataTestFactory.createAccounts();
		list<Contact> contacts=DataTestFactory.createContacts();
		list<Invoice_Line_Item__c> invoiceItems=DataTestFactory.createInvoiceLineItems();
		list<String> accountIds=new list<String>();
		list<String> contactIds=new list<String>();
		for(Account accountRec: accounts){
			accountIds.add(accountRec.Id);
		}
		for(Contact contactRec: contacts){
			contactIds.add(contactRec.Id);
		}
		InvoiceItemSelector invoiceItemSel=new InvoiceItemSelector();
		list<Invoice_Line_Item__c> invoiceLineItemsFromAccount=invoiceItemSel.getInvoiceItemsByBillerId(accountIds);
		list<Invoice_Line_Item__c> invoiceLineItemsFromContacts=invoiceItemSel.getInvoiceItemsByBillingContactId(contactIds);
		System.assert(invoiceLineItemsFromAccount.size()>0);
		System.assert(invoiceLineItemsFromContacts.size()>0);
		}
	}
}