@isTest
private class AvailabilityCompareTest {
	private static String ROSTER_STATUS_PENDING = 'Pending';
	
	@isTest static void test_setUpShiftBinaryMapping() {
		AvailabilityCompare aCompare = new AvailabilityCompare();
		Map<String , String> sBinaryMapping = new Map<String , String>();
		sBinaryMapping = aCompare.setUpShiftBinaryMapping();
		system.assert(sBinaryMapping.size()==4);
	}
	
	@isTest static void test_convertShifttoMap() {
		Account acc = TestDataFactoryRoster.createAccount();          //create an account
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);  //create a vacancy
        
        Contact dummycon = TestDataFactoryRoster.createContact();     //create a contact
        Set<String> candidateIds = new Set<String>{dummycon.id};
        Shift__c shift = TestDataFactoryRoster.createShift(vac);      //create a shift
    	List<Days_Unavailable__c> rosters =                           //create a roster
    		TestDataFactoryRoster.createRostersForSplitRosterFunction(dummycon, shift, acc,ROSTER_STATUS_PENDING);
    	//system.debug('rosters ='+rosters);
    	
    	List<ShiftDTO> shiftDTOs = new List<ShiftDTO>();              //create a shiftDTO
    	ShiftDTO shiftDTO = new ShiftDTO();
    	shiftDTO = TestDataFactoryRoster.createShiftDTO(acc, vac, shift);
    	shiftDTOs.add(shiftDTO);
    	//system.debug('shiftDTOs ='+ shiftDTOs);
    	
    	List<Days_Unavailable__c> availabilities = new List<Days_Unavailable__c>();
    	AvailabilitySelector aSelector = new AvailabilitySelector();
    	availabilities = aSelector.getAllAvailabiltyRecordByShifts(shiftDTOs, candidateIds);

    	//system.debug('availabilities ='+ availabilities);
		AvailabilityCompare aCompare = new AvailabilityCompare();
		
		Contact con = [SELECT Id, FirstName, LastName,Title,
											Phone, Email, MailingCity, MailingCountry,
											MailingStreet, MailingPostalCode, MailingState, CreatedDate FROM Contact where id =: dummycon.id];
		candidateDTO conDTO = new candidateDTO();
		conDTO.setCandidate(con);
		conDTO.setRadiusRanking(1);

		Map<String, candidateDTO> conDTOMap = new Map<String, candidateDTO>();
		conDTOMap.put(con.id,conDTO);
		conDTOMap = aCompare.compareShiftAndAvailability(shiftDTOs, availabilities , conDTOMap);
		//system.debug('conDTOMap ='+ conDTOMap);
		//system.debug('candidateDTO='+ conDTOMap.values());
		conDTO = conDTOMap.get(con.id);
		system.assert(conDTO.getAvailabilityPercentage() != 1.00);
	}

	@isTest static void test_availabilityObjectRecordTypeMap() {
		Map<String, String> availRecordTypeMap = new Map<String, String>();
		availRecordTypeMap = AvailabilityCompare.availabilityObjectRecordTypeMap();
		//system.debug('availRecordTypeMap ='+ availRecordTypeMap);
		system.assertEquals(availRecordTypeMap.size(),2);
	}
}