public with sharing class PeopleSearchHelper {
	public static Set<Schema.DisplayType> peopleSearchExcludedType;
	static{
		//build set of excluded search fields
		if(peopleSearchExcludedType == null){
			peopleSearchExcludedType = new Set<Schema.DisplayType>{
				Schema.DisplayType.TextArea,
				Schema.DisplayType.Address
			};
		}
	}
	
    public static Set<String> getContactFieldAPIName(List<Schema.SObjectField> fields){    	
    	Set<String> output = new Set<String>();
    	for(Schema.SObjectField rawField : fields){
    		output.add(rawField.getDescribe().getName());
    	}
    	return output; 
    }
    
    //Returns true if the field is needed to be included
    private static Boolean complexExcludeCheck(DescribeFieldResult fieldToCheck){
    	if(fieldToCheck.getType() == Schema.DisplayType.TextArea && fieldToCheck.getLength() <= 255){
    		//include text area fields that has length of 255 and below
    		return true;
    	}
    	return false;
    }
    
    //Check if the fields type is not excluded
    public static Boolean isFieldNotExcluded(DescribeFieldResult fieldToCheck){
    	return !peopleSearchExcludedType.contains(fieldToCheck.getType()) || complexExcludeCheck(fieldToCheck);
    }
    
    public static String getFieldLabel(DescribeFieldResult fieldToCheck){
    	return removeIDPrefixIfReferenceField(fieldToCheck.getLabel(), fieldToCheck.getType().name());
    }
    
    public static String removeIDPrefix(String label){
    	return label.removeEnd(' Id').removeEnd(' ID').removeEnd(' id').removeEnd(' iD');
    }
    
    public static String removeIDPrefixIfReferenceField(String label, String fieldtype){
    	final String RECORDTYPE_LABEL = 'record type id';
    	String output = label;
    	Boolean isReference = fieldtype != null && fieldtype.toLowerCase() == 'reference';
    	if(isReference || label.toLowerCase() == RECORDTYPE_LABEL){
    		output = removeIDPrefix(label);
    	}
    	return output;
    }
    
    //"2016-06-13T14:00:00.000Z"
    //Date: YYYY-MM-DD
    //Time: hh:mm:ss.sss
    public static String getLocalDateTime(String rawDateString){
        if(rawDateString!=null && rawDateString!='null' && String.isNotEmpty(rawDateString)){
            Datetime dt = (datetime)json.deserialize('"'+rawDateString+'"', datetime.class);
            return dt.format('YYYY-MM-dd');
        }
    	return null;
    	
    	/*
    	String t = dt.format('hh:mm:ss.sss');
    	if(type == PeopleSearchDTO.F_DATE){
    		return d;
    	}else if(type == PeopleSearchDTO.F_DATE_TIME){
    		return d+'T'+t+'Z';
    	}
    	return rawDateString;
    	*/
    }
}