@RestResource(urlMapping='/unsecuredcredential/all')
global with sharing class UnsecuredCredentialREST {
    
    @HttpGet
    global static CredentialList getCrendentials(){
        List<ProtectedCredential> cs = new List<ProtectedCredential>();
        
        try{
            //Indeed
            List<ProtectedCredential> credentials =  getIndeedCredentials();
            cs.addAll(credentials);
            
            //JXT
            credentials =  getJXTCredentials();
            cs.addAll(credentials);
            
            //JXT NZ
            credentials =  getJXTNZCredentials();
            cs.addAll(credentials);
            
            //LINKEDIN
            credentials =  getLinkedInCredentials();
            cs.addAll(credentials);
            
            //CAREERONE
            credentials =  getCareerOneCredentials();
            cs.addAll(credentials);
            
            //ASTUTE PAYROLL
            credentials =  getAstutePayrollCredentials();
            cs.addAll(credentials);
            
            //Daxtra SQS
            credentials = getDaxtraSQSCredentials();
            cs.addAll(credentials);
        }catch(Exception e){
            
            System.debug(e);
            
            if(!Test.isRunningTest()){
            	 RestResponse res = RestContext.response;

	            // set the status code and response body for test case
	            res.statusCode = 500;
	            res.responseBody = Blob.valueOf('Failed to get old credentials - ' + e);
            }
            
            
            
        }
        
        CredentialList clist = new CredentialList();
        clist.setCredentialList(cs);

        return clist;
        
    }
    
    private static List<ProtectedCredential> getIndeedCredentials(){
        List<ProtectedCredential> cs = new List<ProtectedCredential>();
        List<Advertisement_Configuration__c> accounts = new AdvertisementConfigurationSelector()
        	.setParam('indeedAccountRTId', DaoRecordType.IndeedAdRT.Id)
        	.get('Id,Indeed_API_Token__c','RecordTypeId =: indeedAccountRTId');
        if(accounts != null && accounts.size() > 0 ){
            for(Advertisement_Configuration__c a : accounts){
                if(a.Indeed_API_Token__c != null){
                    ProtectedCredential c = new ProtectedCredential();
                    c.type = ProtectedCredential.TYPE_ADVERTISEMENT;
                    c.provider = ProtectedCredential.PROVIDER_INDEED;
                    c.relatedTo = ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG;
                    
                    c.name = a.Id;
                    c.token = a.Indeed_API_Token__c ;
                    
                    cs.add(c);
                }
                
            }
            
        }
        
        return cs;
    }
    
     private static List<ProtectedCredential> getJXTCredentials(){
        List<ProtectedCredential> cs = new List<ProtectedCredential>();
        
        Advertisement_Configuration__c a = (Advertisement_Configuration__c)new AdvertisementConfigurationSelector()
        	.setParam('jxtAccountRT',DaoRecordType.jxtAccountRT.Id)
        	.getOne('Id,Password__c,UserName__c,','RecordTypeId =: jxtAccountRT');
        if(a != null ){
            
            
                ProtectedCredential c = new ProtectedCredential();
                c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
                c.provider = ProtectedCredential.PROVIDER_JXT;
                c.relatedTo = ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG;
                
                c.name = a.Id;
                c.username = a.UserName__c ;
                c.password = a.Password__c;
                
                cs.add(c);
            
            
        }
        
        return cs;
    }
    
     private static List<ProtectedCredential> getLinkedInCredentials(){
        List<ProtectedCredential> cs = new List<ProtectedCredential>();
        
        
        try {
            
            CommonSelector selector = new CommonSelector(OAuth_Service__c.SObjectType);
            List<OAuth_Service__c> ss = (List<OAuth_Service__c> ) selector.get('Id,Name, Access_Token_URL__c, Consumer_Key__c, Consumer_Secret__c', 'Active__c = true');
            
            
                
            if(ss != null && ss.size() > 0){
                
                
                for(OAuth_Service__c s : ss){
                    
                    
                    ProtectedCredential c = new ProtectedCredential();
                    c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
                    c.provider = ProtectedCredential.PROVIDER_LINKEDIN;
                    c.relatedTo = ProtectedCredential.RELATED_TO_OAUTHSERVICE;
                    
                    c.name = s.Id;
                    c.key = s.Consumer_Key__c ;
                    c.secret = s.Consumer_Secret__c;
                    
                    
                    cs.add(c);
                    
                }
            }
             
        } catch(System.QueryException e) {
            
            System.debug(e);
            throw e;
           
        }
        
        try {
            CommonSelector tokenselector = new CommonSelector(OAuth_Token__c.SObjectType);
            List<OAuth_Token__c> ts = (List<OAuth_Token__c> ) tokenselector.get('Id,Name, secret__c, token__c', '');
           
                
            if(ts != null && ts.size() > 0){
                
                
                for(OAuth_Token__c t : ts){
                    
                    
                    ProtectedCredential c = new ProtectedCredential();
                    c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
                    c.provider = ProtectedCredential.PROVIDER_LINKEDIN;
                    c.relatedTo = ProtectedCredential.RELATED_TO_OAUTHTOKEN;
                    
                    c.name = t.Id;
                    c.secret = t.secret__c ;
                    c.token = t.token__c;
                    
                    
                    cs.add(c);
                    
                }
            }
             
        } catch(System.QueryException e) {
            
            System.debug(e);
            throw e;
           
        }

        
        return cs;
    }
   
   
   private static List<ProtectedCredential> getCareerOneCredentials(){
       List<ProtectedCredential> cs = new List<ProtectedCredential>();
       
       try{
            StyleCategorySelector selector = new StyleCategorySelector();
            StyleCategory__c[] websiteacc = selector.get('Id,Advertiser_Password__c,Advertiser_Name__c,Name', 'WebSite__c=\'CareerOne\' and Account_Active__c = true and RecordType.DeveloperName like \'%WebSite_Admin_Record%\'');
            if(websiteacc != null && websiteacc.size() >0 ){
                
                for(StyleCategory__c a : websiteacc){
                    ProtectedCredential c = new ProtectedCredential();
                    c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
                    c.provider = ProtectedCredential.PROVIDER_CAREERONE;
                    c.relatedTo = ProtectedCredential.RELATED_TO_STYLECATEGORY;
                    
                    c.name = a.Id;
                    
                    c.username = a.Advertiser_Name__c;
                    c.password =  a.Advertiser_Password__c;
                    
                    
                    cs.add(c);
                }
                
                
            }
       }catch(System.QueryException e){
           throw e;
       }
       
       return cs;
        
    }
    
     private static List<ProtectedCredential> getJXTNZCredentials(){
       List<ProtectedCredential> cs = new List<ProtectedCredential>();
       
       try{
           
           
            StyleCategorySelector selector = new StyleCategorySelector();
            
            StyleCategory__c[] accounts = selector.get('Id,Advertiser_Id__c,Jobx_Username__c,Jobx_Password__c,Name','WebSite__c=\'JXT_NZ\' and Advertiser_Id__c!=null and Jobx_Username__c!=null and Jobx_Password__c!=null');
             if(accounts != null && accounts.size() >0 ){
                
                for(StyleCategory__c account : accounts){
                    ProtectedCredential c = new ProtectedCredential();
                    c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
                    c.provider = ProtectedCredential.PROVIDER_JXT;
                    c.relatedTo = ProtectedCredential.RELATED_TO_STYLECATEGORY;
                    
                    c.name = account.Id;
                    
                    c.username = account.Jobx_Username__c;
                    c.password =  account.Jobx_Password__c;
                    
                    
                    cs.add(c);
                }
                
                
            }
       }catch(System.QueryException e){
           throw e;
       }
       
       return cs;
        
    }
    
    private static List<ProtectedCredential> getAstutePayrollCredentials(){
        
    List<ProtectedCredential> cs = new List<ProtectedCredential>();
       
       try{
           
            StyleCategorySelector selector = new StyleCategorySelector();
            StyleCategory__c[] apAccounts = selector.get('Id,Name, AstutePayroll_api_username__c,AstutePayroll_api_password__c, AstutePayroll_api_key__c', 'RecordType.DeveloperName = \'AstutePayrollAccount\'');
            
            if(apAccounts != null && apAccounts.size() > 0){
                for(StyleCategory__c a : apAccounts){
                    ProtectedCredential c = new ProtectedCredential();
                    c.type=ProtectedCredential.TYPE_PAYROLL;
                    c.provider = ProtectedCredential.PROVIDER_ASTUTE;
                    c.relatedTo = ProtectedCredential.RELATED_TO_STYLECATEGORY;
                    
                    c.name = a.Id;
                    c.username = a.AstutePayroll_api_username__c;
                    c.password =  a.AstutePayroll_api_password__c;
                    c.key = a.AstutePayroll_api_key__c;
                    
                    
                    cs.add(c);
                }
            }
            
            
       }catch(System.QueryException e) {
            
            System.debug(e);
            throw e;
           
        }
        
        return cs;
    }
    
    private static List<ProtectedCredential> getDaxtraSQSCredentials(){
    	List<ProtectedCredential> cs = new List<ProtectedCredential>();
    	
    	ProtectedCredential c = new ProtectedCredential();
    	c.key = 'AKIAIJNY5C3MQJCBUDPQ';
    	c.secret = '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';
    	c.username = 'https://sqs.ap-southeast-2.amazonaws.com/729402844354/CandidateFeedRequest';
    	c.name = 'Daxtra Feed SQS';
    	
    	c.type=ProtectedCredential.TYPE_SQS;
        c.provider = ProtectedCredential.PROVIDER_AMAZON;
               
    	
    	cs.add(c);
    	
    	return cs;
    	
	    
        
	
    }
   

    global with sharing class CredentialList{
        List<ProtectedCredential> credentialList = new List<ProtectedCredential>();

        public List<ProtectedCredential> getCredentialList() {
            return credentialList;
        }

        public void setCredentialList(List<ProtectedCredential> credentialList) {
            this.credentialList = credentialList;
        }
        
        
    }
}