/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

/**

Advertisement clone function for seek

**/
public with sharing class SeekClone {
    Integer usage,quota,reminingquota;
    public String err_msg{get;set;}
    public boolean has_error ;
    private ApexPages.StandardController stdCon;
    
     //Init Error Message for customer
    public PeopleCloudErrorInfo errormesg; 
    public Boolean msgSignalhasmessage{get ;set;} //show error message or not
    
    public SeekClone(ApexPages.StandardController stdController){
    	stdCon = stdController;
        has_error = false;
        UserInformationCon usercls = new UserInformationCon();
		reminingquota = usercls.checkquota(1);
		usage = usercls.usage;
        quota = usercls.quota;
        //checkquota();
        if(reminingquota <= 0)
        {
            msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.AD_POST_NO_QUOTA_ERROR); 
            has_error = true;
        }
        system.debug('remaining =' + reminingquota );
    }
    /*public void checkquota(){
        
       User u = [select Id, Phone, Email, AmazonS3_Folder__c,Seek_Usage__c,Monthly_Quota_Seek__c, JobBoards__c from User where Id=:UserInfo.getUserId()];
       usage = (u.Seek_Usage__c == null)? 0 : Integer.valueOf(u.Seek_Usage__c);
       quota = (u.Monthly_Quota_Seek__c == null)? 0 : Integer.valueOf(u.Monthly_Quota_Seek__c);
       reminingquota = quota - usage;
    }*/
    public PageReference cloneAd(){
        if(has_error){
            return null;
        }
        try{
            String id = ApexPages.currentPage().getParameters().get('id');
            AdvertisementSelector adSelector = new AdvertisementSelector();
            Advertisement__c ad = adSelector.getAdForSeekClone(id);
        	//system.debug('ad =' + ad);
	        Advertisement__c newAd = ad.clone(false, true);
	        newAd.Status__c = 'Clone to Post'; //** added by suzan
	        newAd.Use_18_Characters_as_Job_Ref__c = true;
	        newAd.Job_Posting_Status__c = '';
	        newAd.Prefer_Email_Address__c = ''; //clone new ad, but don't clone the email address
            //checkFLS
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Advertisement__c.Status__c,
                Advertisement__c.Use_18_Characters_as_Job_Ref__c,
                Advertisement__c.Job_Posting_Status__c,
                Advertisement__c.Prefer_Email_Address__c
            };
            fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
	        insert newAd; 
	        PageReference editPage = Page.seekEdit;
	        editPage.getParameters().put('id', newAd.id);
	        editpage.getParameters().put('isedit','false');//The action is about clone ad not update
	        return editPage;
        }catch(Exception e){
        	has_error = true;
        	msgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.CLONE_AD_ERROR); 
            return null;
        }
    }
    
    public PageReference cancelClone(){
    	return stdCon.view();
    }
    
     //Generate Error message, Add by Jack on27/06/2013
    public static Boolean customerErrorMessage(Integer msgcode){
        PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        Boolean msgSignalhasmessage = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
                ApexPages.addMessage(newMessage);
            }
        return msgSignalhasmessage;               
    }    
}