public virtual class SObjectCRUDService {
    private DynamicSelector dynamicSel;
    private Schema.SObjectType sobjType;
    private fflib_SObjectDescribe sobjectDescribe;
    public Enum OperationType { CREATE, READ, MODIFY, DEL }
    
    public SObjectCRUDService(Schema.SObjectType sobjType){
        dynamicSel = new DynamicSelector(sobjType.getDescribe().getName(), false, true, true);
        this.sobjType = sobjType;
        sobjectDescribe = fflib_SObjectDescribe.getDescribe(sobjType);
    }
    

    /**
     * Construct sobject using given mapping. Key of the map is SObject field,
     * value of the map is field value.
     * 
     * */
    protected List<SObject> constructSObjectsForCRUD(List<Map<String, Object>> mappings, OperationType oType){
        List<SObject> sobjects = new List<SObject>();
        for (Map<String, Object> m : mappings) {
            SObject so = sobjType.newSObject();
            for (String field : m.keySet()) {
                Schema.SObjectField sof = sobjectDescribe.getField(field);
                Schema.DescribeFieldResult dfr;
                // Check field existence
                if (sof != null && !field.containsIgnoreCase('__r') 
                    && !field.equalsIgnoreCase('Owner') && !field.equalsIgnoreCase('RecordType')) {
                    dfr = sof.getDescribe();
                    try{
                        if (oType == OperationType.CREATE) {
                            // Check if field is insertable to user
                            fflib_SecurityUtils.checkFieldIsInsertable(sobjType, sof);   
                        } else if (oType == OperationType.MODIFY) {
                            if (!dfr.getName().equalsIgnoreCase('Id')) {
                                fflib_SecurityUtils.checkFieldIsUpdateable(sobjType, sof);    
                            }
                        }
                    } catch (fflib_SecurityUtils.FlsException e) {
                        continue;
                    }
                } else {
                    continue;
                }
                
                // Date & Datetime is format as number in JSON, therefore we handle it differently
               if (dfr.getType() == Schema.DisplayType.Date && m.get(field) != null) {
                    so.put(field, Datetime.newInstance(((Decimal)m.get(field)).longValue()).date());
                } else if (dfr.getType() == Schema.DisplayType.Datetime && m.get(field) != null) {
                    so.put(field, Datetime.newInstance(((Decimal)m.get(field)).longValue()));
                } else {
                    so.put(field, m.get(field));
                }
            }
            sobjects.add(so);
        }
        return sobjects;
    }

    protected List<SObject> queryLatestCopy(List<SObject> sobjs){
        //system.debug('query latest copy:'+sobjs);
        Set<Id> idToQuery = new Set<Id>();
        for (SObject sobj : sobjs) {
            
            if (sobj.Id != null) {
             idToQuery.add(sobj.Id);   
            }
        }
        return dynamicSel.selectSObjectByIds(idToQuery);
    }

    
    /**
     * Create sobjects and return new records
     * 
     * */    
    public virtual List<SObject> createRecords(List<Map<String, Object>> sobjs){
        List<SObject> sobjToPersist =constructSObjectsForCRUD(sobjs, OperationType.CREATE);
        fflib_SObjectDomain sobjDomain = new fflib_SObjectDomain(sobjToPersist);
        sobjDomain.create();
        return queryLatestCopy(sobjToPersist);
    }

    /**
     * Create sobjects and return new records
     * 
     * */    
    public virtual List<SObject> createRecords(List<SObject> sobjs){
        fflib_SObjectDomain sobjDomain = new fflib_SObjectDomain(sobjs);
        sobjDomain.create();
        return sobjs;
    }    

    public virtual List<SObject> getRecords(Set<Id> idSet) {
        return dynamicSel.selectSObjectByIds(idSet);
    }

    public virtual List<SObject> getRecordsByOffset(Integer offset, Integer limitCount) {
        return dynamicSel.selectSObjectByOffsetLimit(offset, limitCount);
    }
    
    /**
     * Update sobjects and return latest copy of updated records
     * 
     * */
    public virtual List<SObject> updateRecords(List<Map<String, Object>> sobjs){
        List<SObject> sobjToPersist =constructSObjectsForCRUD(sobjs, OperationType.MODIFY);
        fflib_SObjectDomain sobjDomain = new fflib_SObjectDomain(sobjToPersist);
        sobjDomain.modify();
        return queryLatestCopy(sobjToPersist);
    }

    /**
     * Update sobjects and return latest copy of updated records
     * 
     * */
    public virtual List<SObject> updateRecords(List<SObject> sobjs){
        fflib_SObjectDomain sobjDomain = new fflib_SObjectDomain(sobjs);
        sobjDomain.modify();
        return sobjs;
    }
    
    /**
     *  Delet sobjects and return ids of deleted records
     * 
     * */
    public List<Id> deleteRecords(List<Map<String, Object>> sobjs){
        List<Id> ids = new List<Id>();
        List<SObject> sobjToDel = new List<SObject>();
        for (Map<String, Object> sobj : sobjs) {
            Object sobjId = sobj.get('Id') != null?sobj.get('Id'):null;
            if (sobjId != null) {
                ids.add((String)sobjId);
                SObject so = sobjType.newSObject();
                so.put('id', (String)sobjId);
                sobjToDel.add(so);
            }
        }
        fflib_SObjectDomain sobjDomain = new fflib_SObjectDomain(sobjToDel);        
        sobjDomain.del();
        return ids;
    }

    /**
     *  Delet sobjects and return ids of deleted records
     * 
     * */
    public List<Id> deleteRecords(List<SObject> sobjs){
        List<Id> ids = new List<Id>();
        List<SObject> sobjToDel = new List<SObject>();
        for (SObject sobj : sobjs) {
            if (sobj.Id != null) {
                ids.add(sobj.Id);
                sobjToDel.add(sobj);
            }
        }
        fflib_SObjectDomain sobjDomain = new fflib_SObjectDomain(sobjToDel);        
        sobjDomain.del();
        return ids;
    }      
}