@isTest
private class VacancyExtensionGlobalTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        VacancySearchCriteria searchCriteria = TestDataFactoryRoster.createVacancySearchCriteria(acc);
        String searchCriteriaStr = JSON.serialize(searchCriteria);
        VacancyDTO vacDTO =  TestDataFactoryRoster.createVacancyDTO(acc,vac);
        String vacDTOJson = JSON.serialize(vacDTO);
        
        //test VacancyExtensionGlobal getVacancy(vacancyId) method
        Placement__c vac1 = VacancyExtensionGlobal.getVacancy(vac.Id);
        System.assertNotEquals(vac1,null);
        
        //test VacancyExtensionGlobal searchVacancy(searchCriteria) method
        String vacDTOListStr = VacancyExtensionGlobal.searchVacancy(searchCriteriaStr);
        System.assertNotEquals(vacDTOListStr,'');
        
        //test VacancyExtensionGlobal upsertVacancy(String vacDTOJson) method
        String vacDTOListDisplay = VacancyExtensionGlobal.upsertVacancy(vacDTOJson);
        System.assertNotEquals(vacDTOListDisplay,'');
        
        //test VacancyExtensionGlobal describeVacancy() method
        Map<String,List<Schema.PicklistEntry>> describeResult = VacancyExtensionGlobal.describeVacancy();
        System.assertNotEquals(describeResult,null);
        
        //test VacancyExtensionGlobal fetchRecordTypeInfo() method
        String recordTypeInfo = VacancyExtensionGlobal.fetchRecordTypeInfo();
        System.assertNotEquals(recordTypeInfo,'');
    	}
        
    }
}