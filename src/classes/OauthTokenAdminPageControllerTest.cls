@isTest
private class OauthTokenAdminPageControllerTest {

   @isTest 
    public static void testSave() {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ]; 
        System.runAs ( thisUser ) {  
            SFOauthToken__c sfo = new SFOauthToken__c(Name='oAuthToken');
            insert sfo;
            
            OauthTokenAdminPageController controller = new OauthTokenAdminPageController();
            controller.consumerKey = 'testConsumerKey';
            controller.secretKey = 'testSecretKey';
            controller.instanceURL = 'testInstanceURL';
            controller.oauthToken = 'testOauthToken';
            controller.refreshToken = 'testRefreshToken';
            controller.save();
            
            SFOauthToken__c sfOauthToken = SFOauthTokenSelector.querySFOauthToken('oAuthToken');
            system.assert(sfOauthToken.ConsumerKey__c == 'testConsumerKey');
            system.assert(sfOauthToken.SecretKey__c == 'testSecretKey');
            system.assert(sfOauthToken.InstanceURL__c == 'testInstanceURL');
            system.assert(sfOauthToken.OAuthToken__c == 'testOauthToken');
            system.assert(sfOauthToken.RefreshToken__c == 'testRefreshToken');
        } 
        
    }
}