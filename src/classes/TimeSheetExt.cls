/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

public with sharing class TimeSheetExt{
    //** timesheet extension
    //*** for viewtimesheet page which override the View button for timesheet obj
    //**** workforce management
 //   public Placement__c thevacancy {get;set;}
	//public User theuser;
 //   public Contact thecandidate {get;set;}
 //   public Integer theIndex;
 //   public List<TimeSheetClass.TimeSheet> timesheets{get;set;}
 //   public Timesheet_Entry__c tempTS{get;set;}
 //   public String theseq{get;set;}
 //   public String CompanyName {get;set;}
 //   public String CandidateName {get;set;}
 //   public String theType{get;set;}
 //   public String selectedRTId{get;set;}
 //   public String theApprover {get;set;}
 //   public String selectedTS {get;set;} //id
 //   public List<SelectOption> timesheetoptions {get;set;}
 //   public TimeSheet__c[] existing_timesheet;
 //   public TimeSheet__c thetimesheet{get;set;}
 //   public Map<Id,TimeSheet__c> map_timesheet = new Map<Id,TimeSheet__c>();
 //   public boolean showNew{get;set;}
 //   public boolean show{get;set;}
 //   public string err{get;set;}
 //   public string errTS{get;set;}
 //   public string msgTS{get;set;}
 //   public List<SelectOption> approvers {get;set;}
 //   public List<SelectOption> approvers2 {get;set;}
 //   public List<SelectOption> candidates {get;set;}
 //   public String selectedapprover {get;set;}
 //   public String selectedapprover2 {get;set;} // backup approver
 //   public String selectedCandidate{get;set;}
 //   public String TS_StartTime {get;set;}
 //   public String TS_EndTime {get;set;}
    
 //   public String remindmsg{get;set;}
 //   public String remindmsg2{get;set;}
 //   public String remindmsg3{get;set;}
 //   public String remindmsg4{get;set;}
    
 //   //ERROR MESSAGE
 //   public static final String NO_APPLICABLE_PROCESS = 'Approval Process for time sheet may not be enabled or existed, or your time sheet hasn\'t been assigned to a approver. Please try again, if this error persists, please contact system admin. Recent time sheet didn\'t submit in this operation.';
 //   public static final String ALREADY_IN_PROCESS = 'Your time sheet has been submitted, please don\'t submit it again. If the time sheet status is "SYS ERR", a error may arise when updating your time sheet status. Please contact System Admin.';  
    
 //   SelectOption FirstOpt = new SelectOption('','--Please Select--');
    
 //   String class_error ='entryerror'; //** css class name. see the page
 //   String class_non_error ='entrycorrect'; //** css class name. see the page
 //   public TimeSheetExt(ApexPages.StandardController stdController){
 //       this.thetimesheet = (TimeSheet__c)stdController.getRecord();
 //       theIndex = 0;
 //       showNew = false;
 //       tempTS = new Timesheet_Entry__c(); 
 //       show = true;
 //       timesheets = new List<TimeSheetClass.TimeSheet>();
 //       //*****
 //       initSelectOptions();
 //       isLocked = false;
        
 //       initTimesheet(); 
 //       initTimesheetEntries();
        
 //   	isNew = false;
 //   }
     
	//public void init(){ //** page action
	////** for sf.com internal users, not portal users
	//	UserInformationCon userDetail = new UserInformationCon();
 //       theuser = userDetail.getUserDetail();
 //       system.debug('theuser.Manage_TimeSheet__c =' + theuser.Manage_TimeSheet__c);
 //       if(theuser != null)
 //       {
 //       	if(theuser.Manage_TimeSheet__c == true)
 //       	{
 //       		isLocked = false; //** no matter what status, the admin can edit the timesheet. other users can not do change. 
 //       	}
 //       }
 //       system.debug('islocked =' + isLocked);
	 
	//}
	
 //   public void initTimesheet(){ // called by construct class 
 //       try{
 //           String tid = ApexPages.currentPage().getParameters().get('id');
 //           system.debug('tid = '+tid);
 //           if(tid != null && tid!= '')
 //           {

 //               thetimesheet = TimeSheetClass.getTheTimeSheet(tid);//** fetch record from database
                
 //               if(thetimesheet != null)
 //               {
 //                   CandidateName = thetimesheet.Candidate__r.Name ;
 //                   selectedCandidate = thetimesheet.Candidate__c ;
 //                   tempTS.Vacancy__c = thetimesheet.Vacancy__c;
 //                   tempTS.Candidate__c = thetimesheet.Candidate__c;
 //                   selectedTS = thetimesheet.Id;
 //                   TS_StartTime = TimeSheetClass.DecimaltoTimeStr(thetimesheet.Start_Time__c);
 //                   TS_EndTime = TimeSheetClass.DecimaltoTimeStr(thetimesheet.End_Time__c);
 //                   if(thetimesheet.Vacancy__c != null)
 //                   {
 //                       //** populate vacancy
 //                       initVacancy(thetimesheet.Vacancy__c);
                        
 //                   }
 //                   if(thetimesheet.Approver_Contact__c != null)
 //                   {
 //                   	selectedapprover = thetimesheet.Approver_Contact__c;
 //                   }
 //                   else
 //                   {
 //                   	selectedapprover = thevacancy.TimeSheet_Approver__c;
 //                   }
 //                   if(thetimesheet.Backup_Approver__c != null)
 //                   {
 //                   	selectedapprover2 = thetimesheet.Backup_Approver__c;
 //                   }
 //                   else
 //                   {
 //                   	selectedapprover2 = thevacancy.Backup_Approver__c;
 //                   }
 //                   if(thetimesheet.TimeSheet_Status__c!= null)
 //                   {
 //                   	if(thetimesheet.TimeSheet_Status__c == 'Approved')
 //                   	{
 //                   		isLocked = true;
 //                   	}
 //                   	else
 //                   	{
 //                   		isLocked = false;
 //                   	}
 //                   	theApprover = getApproverDetail(thetimesheet.TimeSheet_Status__c, thetimesheet.Id);
 //                   }

 //                   system.debug('is locked =' + isLocked);
 //               }
 //           }
 //           else
 //           {
 //               thetimesheet = new TimeSheet__c();
 //           }            
            
 //       }
 //       catch(system.Exception e)
 //       {
 //           show = false;
 //           system.debug('change timesheet='+e);
 //       }

            
 //   }
 //   public String getApproverDetail(String tsstatus,String parentid){
	//   //** track the TS history to check who latest approve or reject this timesheet. 
	//   //** the queryresult sorted by createddate DESC which make sure always tests the very latest value
	//   //** return the userid , display contact name rather than corresponding user name
	//    Id portal_user_id ;
 //   	if(tsstatus != '' && (tsstatus == 'Approved' || tsstatus == 'Rejected'))
 //   	{
 //   		for(TimeSheet__History h : TimeSheetClass.HistoryTracking(true,'TimeSheet_Status__c',null, parentid))
 //           {
                
 //               if(h.NewValue == tsstatus)
 //               {
 //                  portal_user_id = h.CreatedById;
 //                  break;
                   
 //               }
 //           }
    	
 //   	}
 //   	else
 //   	{
 //   		return null;
 //   	}
 //   	if(portal_user_id != null)
 //   	{
 //   		CommonSelector.checkRead(User.sObjectType,'Id , ContactId');
 //   		User u = [	select Id , ContactId
	//					from User where Id=:portal_user_id
	//				 ];
	//		CommonSelector.checkRead(Contact.sObjectType, ' Id , FirstName , LastName');
	//		Contact c = [select Id , FirstName , LastName from Contact where Id =: u.ContactId];
			
	//		return c.FirstName + ' ' + c.LastName;
 //   	}
	    
 //   	return null;
 //   }
 //   public void initVacancy(String vid){
    
 //   					CommonSelector.checkRead(Placement__c.sObjectType, 'Id, Company__r.Name,Company__c, Name,RecordType.Name,RecordTypeId,Backup_Approver__c,TimeSheet_Approver__c,Internal_Approver__c');
 //   					CommonSelector.checkRead(TimeSheet__c.sObjectType, 'Id, Name, Start_Date__c, End_Date__c, Start_Time__c, End_Time__c, TimeSheet_Status__c ');
 //   					thevacancy = [select Id, Company__r.Name,Company__c, Name,
 //                       			RecordType.Name,RecordTypeId,
 //                 					Backup_Approver__c,TimeSheet_Approver__c,Internal_Approver__c,
 //                                   (select Id, Name, Start_Date__c, End_Date__c, Start_Time__c, End_Time__c, 
 //                                   TimeSheet_Status__c from TimeSheets__r)  
 //                                   from Placement__c  where Id =: vid];

 //                       if(thevacancy != null)
 //                       {

 //                           CompanyName = thevacancy.Company__r.Name;

 //                           theType = thevacancy.RecordType.Name;
                            
 //                           //*** populate approver dropdowns
 //                           List<SelectOption> temp = TimeSheetClass.initApprovers(thevacancy.Company__c, thevacancy);
 //                           approvers.addAll(temp);
 //          					approvers2.addAll(temp);
 //          					//*** populate candidate dropdown
 //          					List<SelectOption> temp2 = TimeSheetClass.populateCandidate(thevacancy);
	//						candidates.addAll(temp2);
							
 //                       }
    	
 //   }
 //   public void initTimesheetEntries(){
 //       try
 //       {          
 //           Integer initcount;
            
 //           CommonSelector.checkRead(TimeSheet_Entry__c.sObjectType,
 //           	'Id, Name ,Break_End_Time__c,Break_End_Time_2__c,Break_End_Time_3__c,Break_Start_Time__c,Break_Start_Time_2__c, Break_Start_Time_3__c,Candidate__c, Date__c,Shift_End_Time__c,Shift_Start_Time__c,Vacancy__c,Shift_Type__c, Comments__c,Break_Type_1__c,Break_Type_2__c,Break_Type_3__c');
 //           TimeSheet_Entry__c[] tss = [select Id, Name ,Break_End_Time__c,Break_End_Time_2__c,Break_End_Time_3__c,Break_Start_Time__c,
 //                                       Break_Start_Time_2__c, Break_Start_Time_3__c,Candidate__c, Date__c,Shift_End_Time__c,
 //                                       Shift_Start_Time__c,Vacancy__c,Shift_Type__c, Comments__c,Break_Type_1__c,Break_Type_2__c,Break_Type_3__c
 //                                       from TimeSheet_Entry__c 
 //                                       where TimeSheet__c =:ApexPages.currentPage().getParameters().get('id') limit 1000];
 //           if(tss != null)
 //           {
                
 //               initcount = tss.size();
 //               theIndex = tss.size();
 //               for(integer i = 1 ; i<= initcount ; i++)
 //               {
 //                   timesheets = TimeSheetClass.pushNewEntry(tss[i-1],i,timesheets);
 //               }
                
 //           }
 //           if (timesheets.size() <=0)
 //           {
 //               initcount = 1;
 //               theIndex = 0; 
 //               createNewEntry();
 //           }
            
 //       }
 //       catch(system.exception e)
 //       {
 //       	ApexPages.addMessages(e);
 //       }
    
 //   }

 //   public void nonaction(){
    
    
 //   }
 //   public void initSelectOptions(){
 //   	approvers = new List<SelectOption>(); //** for salesforce internal user selection
 //       approvers2 = new List<SelectOption>(); //** for salesforce internal user selection
 //       candidates = new List<SelectOption>();
        
 //       approvers2.add(FirstOpt);
 //       approvers.add(FirstOpt);
 //       candidates.add(FirstOpt);

 //   }
 //   public void switchVacancy(){
 //       //** called by VF page select list changed action 
 //		//** display information for user
 //		try{
	//        initSelectOptions();
	//        if(thetimesheet != null)
	//        {
	//            if(thetimesheet.Vacancy__c != null)
	//            {
	//                initVacancy(thetimesheet.Vacancy__c);
	//            }
	//        }
	        
	        
	//        selectedapprover = thevacancy.TimeSheet_Approver__c;
	//        selectedapprover2 = thevacancy.Backup_Approver__c;
	        
	//        String temp1 = approvers.size() == 1? 'There is no available Approver currently!' : 'Please make sure Approver selection is correct.';
	//        String temp2 = approvers2.size() == 1? 'There is no available Backup Approver currently!' : 'Please make sure Backup Approver selection is correct.';
	//        String temp3 = candidates.size() == 1? 'There is no available Candidate currently!' : 'Please make sure Candidate selection is correct.';

	//        updatemsgs('*** Vacancy has been changed to "'+thevacancy.Name+'". Please do not forget to check Candidate, Approver and Backup Approver as well.',
	//        temp1,temp2,temp3);
 //		}
 //		catch(system.exception e)
 //		{
 			
 //			updatemsgs('Error: Please make sure you select correct vacancy.','','','');
 //		}
        
 //   }
 //   public void updatemsgs(String s1, String s2, String s3, String s4){
 //   	remindmsg = s1;
 //   	remindmsg2 = s2;
 //   	remindmsg3 = s3;
 //   	remindmsg4 = s4;
    
    
 //   }   
 //   public PageReference updateTimesheet(){
 //   	//** called by page cmd action
 //   	//** update timesheet detail
 //   	//** internal page not portal one 
 //       msgTS = '';
 //       errTS = '';
 //       boolean f = false;
 //       try{
	//        thetimesheet.Approver_Contact__c = selectedapprover;
	//        thetimesheet.Backup_Approver__c = selectedapprover2;
	//        thetimesheet.Candidate__c = selectedCandidate;
	        
	//        if(TS_StartTime == null || TS_StartTime == '' || TS_EndTime == null || TS_EndTime == '')
	//        {
	        	
	//        	errTS = 'Validation Error: Failed. Both Start Time and End Time are required fields.';
	//        	return null;
	//        }
	//        thetimesheet.Start_Time__c = TimeSheetClass.TimeStrtoDecimal(TS_StartTime);
 //       	thetimesheet.End_Time__c = TimeSheetClass.TimeStrtoDecimal(TS_EndTime);
        	
	//        f = TimeSheetClass.validateTimeSheet(thetimesheet);
	//        errTS = TimeSheetClass.TimeSheetClassMsg;
	//        if(f)
 //       	{
	//            //check FLS
 //               List<Schema.SObjectField> fieldList = new List<Schema.SObjectField> {
 //                   TimeSheet__c.Approver_Contact__c,
 //                   TimeSheet__c.Backup_Approver__c,
 //                   TimeSheet__c.Candidate__c,
 //                   TimeSheet__c.Start_Time__c,
 //                   TimeSheet__c.End_Time__c
 //               };
 //               fflib_SecurityUtils.checkUpdate(TimeSheet__c.SObjectType, fieldList);
	//        	update thetimesheet;
	//        	msgTS = 'TimeSheet Information Updated Successfully!';
 //       		thetimesheet = TimeSheetClass.getTheTimeSheet(thetimesheet.Id);//** refresh
        	
 //       	}
        	
        	
 //       }
 //       catch(Exception e)
 //       {
 //       	ApexPages.addMessages(e);
 //       	system.debug(e);
 //       	errTS = 'Updated Unsuccessfully! Systeme error: ' + e.getMessage();
 //       }
        
 //       return null;
    
 //   }
 //   public Boolean BeforeSubmit(TimeSheet_Entry__c te,TimeSheet__c ts){
 //   	//** validate those timesheet entry start date and end date
 //   	try{
    		
 //   		if(ts.Start_Date__c != null && te.Date__c != null)
 //   		{
 //   			if(te.Date__c < ts.Start_Date__c)
 //   			{
 //   				err = 'Validation Error: The date must be equal to or greater than TimeSheet Start Date! ';
 //   				return false;}
 //   		}
 //   		if(ts.End_Date__c != null)
 //   		{
 //   			if(te.Date__c > ts.End_Date__c)
 //   			{
 //   				err = 'Validation Error: The date must be equal to or less than TimeSheet End Date! ';
 //   				return false;}
 //   		}
    		
 //   	}
 //   	catch(exception e)
 //   	{
 //   		return false;
 //   	}
 //   	return true;
 //   }
 //   public pageReference createNewEntry(){
 //   	//** called by page button action
 //   	try{
	//        theIndex = theIndex + 1;
	//        //** the index is the sequence of the record. that could be 1, 3, 10 etc. because 2,4,5,6,7,8,9 have been deleted from page.
	
	//        TimeSheet_Entry__c te = new TimeSheet_Entry__c(TimeSheet__c = thetimesheet.Id, 
	//                                Candidate__c= thetimesheet.Candidate__c,
	//                                Vacancy__c = thetimesheet.Vacancy__c,
	//                                Shift_Start_Time__c = thetimesheet.Start_Time__c,
	//                                Shift_End_Time__c = thetimesheet.End_Time__c
	//                                );
			
	//        timesheets = TimeSheetClass.pushNewEntry(te, theIndex,timesheets);
 //   	}
 //   	catch(exception e)
 //   	{
 //   		ApexPages.addMessages(e);
 //   	}
    	
 //   	return null;
 //   }
 //   public void cloneLastEntry(){
 //   	//** clone last timesheet entry line
 //   	try{
    		
 //   		TimeSheet_Entry__c e = timesheets[timesheets.size()-1].getTimeSheetEntry();
 //   		TimeSheet_Entry__c ts = e.clone(false,true);
 //   		ts.TimeSheet__c = thetimesheet.Id;
 //   		ts.Candidate__c = thetimesheet.Candidate__c;
 //   		ts.Date__c = ts.Date__c.addDays(1);
	//        ts.Vacancy__c = thetimesheet.Vacancy__c;

 //   		theIndex = theIndex + 1;
 //   		timesheets = TimeSheetClass.pushNewEntry(ts, theIndex,timesheets);

 //   	}
 //   	catch(system.exception e){
 //   		ApexPages.addMessages(e);
 //   	}
    	
    
 //   }
 //   public void newTimeSheet(){
         
        
 //   }
 //   public void initHistories(){
    	
    
    
 //   }
    
 //   public String msg{get;set;}

 //   public pageReference doSubmit(){
 //       //** update timesheet entries 

 //       msg = '';
 //       err = '';
 //       boolean has_error = false;
 //       try{
            
 //           Timesheet_Entry__c[] sheets = new Timesheet_Entry__c[]{};
 //           Timesheet_Entry__c te;

 //           if(timesheets != null)
 //           {
 //               if(timesheets.size()>0)
 //               {
 //                   for(TimeSheetClass.TimeSheet ts: timesheets)
 //                   {
 //                       te = ts.getTimeSheetEntry();
	//					if(BeforeSubmit(te,thetimesheet))
 //                       {	
                        	
 //                       	te.Shift_Start_Time__c = TimeSheetClass.TimeStrtoDecimal(ts.Shift_StartTime);
	//						te.Shift_End_Time__c = TimeSheetClass.TimeStrtoDecimal(ts.Shift_EndTime);
	//						te.Break_Start_Time__c = TimeSheetClass.TimeStrtoDecimal(ts.Break_StartTime_1);
	//						te.Break_Start_Time_2__c = TimeSheetClass.TimeStrtoDecimal(ts.Break_StartTime_2);
	//						te.Break_Start_Time_3__c = TimeSheetClass.TimeStrtoDecimal(ts.Break_StartTime_3);
	//						te.Break_End_Time__c = TimeSheetClass.TimeStrtoDecimal(ts.Break_EndTime_1);
	//						te.Break_End_Time_2__c = TimeSheetClass.TimeStrtoDecimal(ts.Break_EndTime_2);
	//						te.Break_End_Time_3__c = TimeSheetClass.TimeStrtoDecimal(ts.Break_EndTime_3);
 //                       	sheets.add(te);
 //                       	ts.setClass_name(class_non_error);
 //                       }
 //                       else
 //                       {
 //                       	has_error= true;
 //                       	ts.setClass_name(class_error);
 //                       	break;
 //                       }
 //                   }
 //                   //system.debug('timesheets=' + timesheets);
 //                   if(!has_error)
 //                   {
	//                    checkFLS();
	//                    Database.upsert(sheets);
	//                    msg = 'TimeSheet Entries Submitted Successfully!';
	                    
 //                   }
 //                   else
 //                   {
 //                   	err += ' Please correct your input and submit again.';
 //                   }
 //               }
 //               else
 //               {
 //                   err = 'Please fill in new TimeSheets first!';
 //               }
 //           }
 //           else
 //           {
 //               err = 'Please fill in new TimeSheets first!';
 //           }
          
 //       }
 //       catch(system.Exception e)
 //       {
 //           err = 'Unsuccessfully updated timeSheets! Please contact your admin.';
 //       }
 //       return null;
 //   }
 //   public void deleteEntry(){
      
 //       try{
            
           
 //           timesheets = TimeSheetClass.deleteEntry(theseq,timesheets);
 //           msg = TimeSheetClass.TimeSheetClassMsg;
 //           err ='';
 //       }
 //       catch(system.Exception e)
 //       {
 //           msg ='';
 //           err = 'Delete Unsuccessfully. Please contact your admin.';
 //       }
        
 //   }
 //  //********************************** below methods for Portal pages****************************
 //  public boolean isNew{get;set;}
 //  public boolean isLocked {get;set;}
 //  public void validate(){
 //  //** for visualforce page: timesheet
 //  //** validate user information because the security concern regarding login from customer portal
 //  // code 
 //  	try{
 //  		String is_new = ApexPages.currentPage().getParameters().get('isnew');
 //  		if(is_new != null && is_new != '')
 //  		{
 //  			if(is_new.toLowerCase() == 'yes')
 //  			{
 //  				isNew = true;
 //  			}
 //  		}
   	
 //  	}
 //  	catch(system.exception e)
 //  	{
   		
 //  	}
 //  }
 //  public void dosubmitTS(){
 //  	   //** from portal user. pick up those default approvers
	//   if(thevacancy != null && thevacancy.internal_approver__c != null){
	//   		try{
	//   			//system.debug('approver:'+thevacancy.internal_approver__c);
	//   			//update timesheet approver in the stituation of existence of dirty timesheet data.
	//   			if(thetimesheet.Approver__c == null){
	//   				thetimesheet.Approver__c = thevacancy.internal_approver__c;
 //                   fflib_SecurityUtils.checkFieldIsUpdateable(TimeSheet__c.SObjectType, TimeSheet__c.Approver__c);
	//   				update thetimesheet;
	//   			}				
	//			//system.debug('setup approver:'+thevacancy.internal_approver__c);
	//			Approval.Processsubmitrequest appReq = new Approval.Processsubmitrequest();
	//			//system.debug('wait to approve:'+thetimesheet.Id);
	//			appReq.setObjectId(thetimesheet.Id);
	//			Approval.Processresult rslt=Approval.process(appReq);
	//			//system.debug('is success:'+rslt.isSuccess());
	//			//if(rslt.getErrors()!=null && ((List<Database.Error>)rslt.getErrors()).size()>0){
	//			//	for(Database.Error e:rslt.getErrors())
	//			//		system.debug('errors?:'+e.getMessage());
	//			//}
	//			thetimesheet.TimeSheet_Status__c = 'Submitted';
	//   		}catch(Exception e){
	//   			if(e.getMessage().contains('ALREADY_IN_PROCESS')){
	//   				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,TimeSheetExt.ALREADY_IN_PROCESS));
	//   			}else if(e.getMessage().contains('NO_APPLICABLE_PROCESS')){
	//   				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,TimeSheetExt.NO_APPLICABLE_PROCESS));
	//   			}
	//   			thetimesheet.TimeSheet_Status__c = 'SYS ERR';
	//   			NotificationClass.notifyErr2Dev('TimeSheetExt/dosubmitTS', e);
	//   		}
	//   	}else if(thevacancy != null){
 //       	thetimesheet.Approver_Contact__c = thevacancy.TimeSheet_Approver__c;
 //       	thetimesheet.Backup_Approver__c = thevacancy.Backup_Approver__c;
 //       	thetimesheet.TimeSheet_Status__c = 'Submitted';
 //       }
 //       //check FLS
 //       List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
 //           TimeSheet__c.Approver__c,
 //           TimeSheet__c.Backup_Approver__c,
 //           TimeSheet__c.TimeSheet_Status__c,
 //           TimeSheet__c.Approver_Contact__c
 //       };
 //       fflib_SecurityUtils.checkUpdate(TimeSheet__c.SObjectType, fieldList);
	//   	update thetimesheet;
	//   	msg = 'This TimeSheet has been submitted successfully.';
	//   	err ='';
	//   	msgTS ='';
	//   	isLocked = false; 
	//   	isNew = false;
 //  }
 //   public pageReference saveTimesheet(){
    	
 //       errTS ='';
 //       msgTS ='';
 //       boolean f = false;
 //       try
 //       {   
 //       	thetimesheet.Start_Time__c = TimeSheetClass.TimeStrtoDecimal(TS_StartTime);
 //       	thetimesheet.End_Time__c = TimeSheetClass.TimeStrtoDecimal(TS_EndTime);
 //       	f = TimeSheetClass.validateTimeSheet(thetimesheet);
 //       	errTS = TimeSheetClass.TimeSheetClassMsg;
 //       	if(f)
 //       	{
 //               //check FLS
 //               List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
 //                   TimeSheet__c.Start_Time__c,
 //                   TimeSheet__c.End_Time__c
 //               };
 //               fflib_SecurityUtils.checkUpdate(TimeSheet__c.SObjectType, fieldList);
	//	        update thetimesheet;
	//	        //system.debug('timesheet =' +thetimesheet);
 //       	}
 //       }
 //       catch(DmlException ex)
 //       {
 //       	ApexPages.addMessages(ex);
        
 //       }
 //       return null;
    
 //   }
 //   public pageReference backtolist(){
    
 //   	PageReference pageRef = new PageReference('/apex/TimeSheetList');
	//    pageRef.setRedirect(true);
	//    return pageRef; 
    
 //   }
    
 //   public boolean hasNoTimeSheetEntry{
 //		get{
 //			CommonSelector.checkRead(TimeSheet_Entry__c.sObjectType, 'Id');
 //			List<TimeSheet_Entry__c> entry=[select Id from TimeSheet_Entry__c
 //                                       where TimeSheet__c =:ApexPages.currentPage().getParameters().get('id')];
 //           return entry.size() ==0;
 //		}
 //	}


 //   private void checkFLS(){
 //       List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
 //           TimeSheet_Entry__c.Shift_Start_Time__c,
 //           TimeSheet_Entry__c.Shift_End_Time__c,
 //           TimeSheet_Entry__c.Break_Start_Time__c, 
 //           TimeSheet_Entry__c.Break_Start_Time_2__c,
 //           TimeSheet_Entry__c.Break_Start_Time_3__c,
 //           TimeSheet_Entry__c.Break_End_Time__c,
 //           TimeSheet_Entry__c.Break_End_Time_2__c, 
 //           TimeSheet_Entry__c.Break_End_Time_3__c 
 //       };
 //       fflib_SecurityUtils.checkInsert(TimeSheet_Entry__c.SObjectType, fieldList);
 //       fflib_SecurityUtils.checkUpdate(TimeSheet_Entry__c.SObjectType, fieldList);
 //   }
    
    
 //   //--------------- test Method -------------------
 //   //test internal approver
 //	static testmethod void testcontroller() {
 //		System.runAs(DummyRecordCreator.platformUser) {
 //		Profile p = [select Id, Name from Profile where Name='System Administrator' limit 1];
	//	Account comp = new Account(Name='abc');
	//	insert comp;
		
	//	//insert internal approver 'iaX'
	//	User ia1 = new User(Username='Peoplecloud_ia1@clicktocloud.com',LastName='ln',Email='peoplecloud1_a1@test.com',Alias='pc1A1',CommunityNickname='peoplecloud1_as1',TimeZoneSidKey='Australia/Sydney',LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',ProfileId=p.id,LanguageLocaleKey='en_US');
 //		insert ia1;
 		 		   
 //		Placement__c v = new Placement__c(Name='Test timesheet extension',Internal_Approver__c=ia1.id);
 //		insert v;
 //		Contact c = new Contact(LastName='justtry', Email='justtry@test.com');
 //		insert c;
 //		TimeSheet__c ts = new TimeSheet__c(TimeSheet_Status__c ='In Progress', Vacancy__c =v.Id, Candidate__c = c.Id,Approver__c = ia1.id);
 //		insert ts;
 //		ApexPages.StandardController stdCon = new ApexPages.StandardController(ts);
 //		Test.StartTest();
 //		ApexPages.currentPage().getParameters().put('id',ts.Id);
 //		TimeSheetExt tse = new TimeSheetExt(stdCon);
 //		tse.init();
 //		tse.thetimesheet.Vacancy__c = v.Id;
 //		tse.switchVacancy();
 //		tse.updateTimesheet();
 //		tse.thetimesheet.Start_Date__c = Date.today();
 //		tse.thetimesheet.End_Date__c = Date.today();
 //		tse.updateTimesheet();
 //		tse.createNewEntry();
 //		tse.doSubmit();
 //		tse.theseq = '1';
 //		tse.deleteEntry();
 //		ApexPages.currentPage().getParameters().put('isnew','yes');
 //		tse.validate();
 //		tse.dosubmitTS();
 //		tse.saveTimesheet();
 //		tse.backtolist();
 //		TimeSheet__c tschk = [select id,TimeSheet_Status__c from TimeSheet__c where id=:ts.id];
 //		if(ApexPages.getMessages().size()==0)
 //			system.assertEquals(tschk.TimeSheet_Status__c, 'Submitted');
 //		else
 //			system.assertEquals(tschk.TimeSheet_Status__c, 'SYS ERR');
 //		Test.StopTest();
 //		}
 //	}
 	
 	

 	
 	
 //	//test contact approver
 //	static testmethod void testContactApprover() {
 //		System.runAs(DummyRecordCreator.platformUser) {
 //		Profile p = [select Id, Name from Profile where Name='System Administrator' limit 1];
	//	Account comp = new Account(Name='abc');
	//	insert comp;
		
	//	//insert internal approver 'iaX'
	//	User ia1 = new User(Username='Peoplecloud_ia1@clicktocloud.com',LastName='ln',Email='peoplecloud1_a1@test.com',Alias='pc1A1',CommunityNickname='peoplecloud1_as1',TimeZoneSidKey='Australia/Sydney',LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',ProfileId=p.id,LanguageLocaleKey='en_US');
 //		insert ia1;
 		 		   
 //		Placement__c v = new Placement__c(Name='Test timesheet extension');
 //		insert v;
 //		Contact c = new Contact(LastName='justtry', Email='justtry@test.com');
 //		insert c;
 //		TimeSheet__c ts = new TimeSheet__c(TimeSheet_Status__c ='In Progress', Vacancy__c =v.Id, Candidate__c = c.Id);
 //		insert ts;
 //		ApexPages.StandardController stdCon = new ApexPages.StandardController(ts);
 //		Test.StartTest();
 //		ApexPages.currentPage().getParameters().put('id',ts.Id);
 //		TimeSheetExt tse = new TimeSheetExt(stdCon);
 //		tse.init();
 //		tse.thetimesheet.Vacancy__c = v.Id;
 //		tse.switchVacancy();
 //		tse.updateTimesheet();
 //		tse.thetimesheet.Start_Date__c = Date.today();
 //		tse.thetimesheet.End_Date__c = Date.today();
 //		tse.updateTimesheet();
 //		tse.createNewEntry();
 //		tse.doSubmit();
 //		tse.theseq = '1';
 //		tse.deleteEntry();
 //		ApexPages.currentPage().getParameters().put('isnew','yes');
 //		tse.validate();
 //		tse.dosubmitTS();
 //		tse.saveTimesheet();
 //		tse.backtolist();
 //		TimeSheet__c tschk = [select id,TimeSheet_Status__c from TimeSheet__c where id=:ts.id];
 //		if(ApexPages.getMessages().size()==0)
 //			system.assertEquals(tschk.TimeSheet_Status__c, 'Submitted');
 //		else
 //			system.assertEquals(tschk.TimeSheet_Status__c, 'SYS ERR');
 //		Test.StopTest();
 		
 //		tse.cloneLastEntry();
 //		}
 //	}
}