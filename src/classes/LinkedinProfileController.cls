/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

public with sharing class LinkedinProfileController {
	public  Contact con{set;get;}
	public PageReference pageRef{set;get;}
	public LinkedinProfileController(ApexPages.StandardController stdController) {
		Pattern StartwithLinkedInPage = Pattern.Compile('^http(s)?://www.linkedin.com.*$');
		        
        
        this.con = (Contact)stdController.getRecord();
        Contact newcon=new Contact();
        newcon=[select id, public_Profile_Url__c,FirstName, LastName,MailingCountry from Contact where id=: con.id];
        Boolean isSearchPage=false;
        if(newcon.public_Profile_Url__c!=NULL ){
        	 Matcher LinkedInMatch = StartwithLinkedInPage.matcher(newcon.public_Profile_Url__c);
        	if(LinkedInMatch.Matches()	){
        	 	pageRef= new PageReference(newcon.public_Profile_Url__c);
        	}else{
        		
        		isSearchPage=true;
        	}
        }else{
        	isSearchPage=true;
        	
        }
        if(isSearchPage){
        	String searchpage='http://www.linkedin.com/pub/dir/?last='+newcon.LastName;
        	if(newcon.FirstName!=NULL && newcon.FirstName!='' ){
        		searchpage=searchpage+'&first='+newcon.FirstName;
        	}
        	if(newcon.MailingCountry!=NULL && newcon.MailingCountry!=''){
        		searchpage=searchpage+'&Country='+newcon.MailingCountry;
        	}
        	
        	pageRef= new PageReference(searchpage);
        	
        }
        
    }
    public PageReference returnPage()
	{
   		return pageRef;
	}
	

}