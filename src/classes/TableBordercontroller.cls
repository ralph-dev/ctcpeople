public with sharing class TableBordercontroller {
	public List<Sobject> allrecords;
	public List<SelectOption> options{set;get;}
	public PaginatorForSObj parinatorunits{set;get;}	
	public Integer currentpage{set;get;}
	public Integer recperpage{set;get;}	
	public List<Sobject> selectedlist{set;get;}
	/**isready is very important.It will avoid the repeat of the invoke of the set function for all records.**/
	public Boolean isready{set;get;}
	public TableBordercontroller(){
		currentpage=1;
		recperpage=10;
		isready=true;
					
	}
	
	public Boolean gethiderecords(){
		if(recperpage==10){
			return false;
		}else{
			return true;
		}
	}
	
	public void changerectoten(){		
		recperpage=10;
		parinatorunits.changerecPerpage(recperpage);
		currentpage=1;
		selectedlist=parinatorunits.getNthPageCollection(1);		
	}
	
	public void changerectohund(){		
		recperpage=200;
		parinatorunits.changerecPerpage(recperpage);
		System.debug('The records per page is '+parinatorunits.recPerPage);
		currentpage=1;
		selectedlist=parinatorunits.getNthPageCollection(1);
		System.debug('The table size should be '+selectedlist.size());
	}
	public void setallrecords(List<Sobject> listallrec){								
		if((listallrec.size()>0 && isready) ||(allrecords!=null && listallrec.size()>0 &&allrecords!=listallrec )){
			//System.debug('the all records is '+listallrec);						
			parinatorunits=new PaginatorForSObj(listallrec);
			parinatorunits.changerecPerpage(recperpage);			
			selectedlist=parinatorunits.getFirstPageCollection();
			allrecords=listallrec;
			isready=false;
		}

		
	}
	public List<Sobject> getallrecords(){
		return allrecords;
	}
	public Boolean gethasnext(){
		if(parinatorunits!=null){
			return parinatorunits.hasNextPage();
		}else{
			return false;
		}
	}
	public Boolean gethaspre(){
		if(parinatorunits!=null){
			return parinatorunits.hasPrevPage();
		}else{
			return false;
		}
	}
	public void firstpage(){
		isready=false;
		selectedlist=parinatorunits.getFirstPageCollection();
		currentpage=parinatorunits.getCurrentPage();
	}
	public void lastpage(){
		isready=false;
		selectedlist=parinatorunits.getLastPageCollection();
		currentpage=parinatorunits.getCurrentPage();
	}
	public void nextpage(){
		System.debug('The next is here');
		isready=false;
		selectedlist=parinatorunits.getNextPageCollection();
		currentpage=parinatorunits.getCurrentPage();
		System.debug('This is the end of the next');
	}
	public void prepage(){
		isready=false;
		selectedlist=parinatorunits.getPrevPageCollection();
		currentpage=parinatorunits.getCurrentPage();
	}
}