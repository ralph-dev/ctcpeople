/**********************************************************
*
*Test class for GetfieldsMap class, Author by Jack on 02 JUL 2013
*
***********************************************************/
@isTest
public class TestGetfieldsMap {
	public static testMethod void testGetfieldsMapNoWhereClause(){
		System.runAs(DummyRecordCreator.platformUser) {
		String objectname = 'Contact';
		String whereClause = '';
		String returnresult = '';
		String ExcludeFieldsString = 'CreatedDate, CreatedById, OwnerId'+','+PeopleCloudHelper.getPackageNamespace()+'Placement_Date__c';
		Set<String> ExcludeFieldsSet = new Set<String>();
		ExcludeFieldsSet = GetfieldsMap.getExcludeFieldsForJobPosting(ExcludeFieldsString);
		returnresult = GetfieldsMap.getCreatableFieldsSOQL(objectname,ExcludeFieldsSet, whereClause);
		system.assert(!returnresult.contains('Where'));
		system.assert(!returnresult.contains('CreatedDate'));
		system.assert(!returnresult.contains(PeopleCloudHelper.getPackageNamespace()+'Placement_Date__c'));
		}
	}
	
	public static testMethod void testGetfieldsMapWithWhereClause(){
		System.runAs(DummyRecordCreator.platformUser) {
		String objectname = 'Account';
		String whereClause = 'id !=\'\'';
		String returnresult = '';
		String ExcludeFieldsString = 'CreatedDate, CreatedById, OwnerId'+','+PeopleCloudHelper.getPackageNamespace()+'Placement_Date__c';
		Set<String> ExcludeFieldsSet = new Set<String>();
		ExcludeFieldsSet = GetfieldsMap.getExcludeFieldsForJobPosting(ExcludeFieldsString);
		returnresult = GetfieldsMap.getCreatableFieldsSOQL(objectname,ExcludeFieldsSet, whereClause);
		system.assert(returnresult.contains('WHERE'));
		system.assert(!returnresult.contains('CreatedDate'));
		system.assert(!returnresult.contains(PeopleCloudHelper.getPackageNamespace()+'Placement_Date__c'));
		}
	}
	
	public static testMethod void testGetfieldsMapWithWrongObjectName(){
		System.runAs(DummyRecordCreator.platformUser) {
		String objectname = 'Accounts';
		String whereClause = 'id !=\'\'';
		String returnresult = '';
		String ExcludeFieldsString = 'CreatedDate, CreatedById, OwnerId'+','+PeopleCloudHelper.getPackageNamespace()+'Placement_Date__c';
		Set<String> ExcludeFieldsSet = new Set<String>();
		ExcludeFieldsSet = GetfieldsMap.getExcludeFieldsForJobPosting(ExcludeFieldsString);
		returnresult = GetfieldsMap.getCreatableFieldsSOQL(objectname,ExcludeFieldsSet, whereClause);
		system.assertEquals(returnresult,null);
		}
	}
}