/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PlacementSelectorTest {

    static testMethod void myUnitTest1() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	list<Contact> cands=DataTestFactory.createCands();
    	list<Placement_Candidate__c> placements=DataTestFactory.createCandidateManagements();
    	list<String> candIds=new list<String>();
    	for(Contact cand: cands){
    		candIds.add(cand.Id);
    	}
    	PlacementSelector pSelector=new PlacementSelector();
    	list<Placement_candidate__c> cms=pSelector.getPlacedCMFromCandidates(candIds);
    	 System.assert(cms.size()>0);
    	 
    	}
    	 
    }
     static testMethod void myUnitTest2() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	list<Account> accounts=DataTestFactory.createAccounts();
    	list<Placement_Candidate__c> placements=DataTestFactory.createCandidateManagements();
    	list<String> accountIds=new list<String>();
    	for(Account acc: accounts){
    		accountIds.add(acc.Id);
    	}
    	PlacementSelector pSelector=new PlacementSelector();
    	list<Placement_candidate__c> cms=pSelector.getPlacedCMFromAccounts(accountIds);
    	 System.assert(cms.size()>0);
    	}
    	 
    }
    static testMethod void myUnitTest3() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	list<Contact> conds=DataTestFactory.createContacts();
    	list<Placement_Candidate__c> placements=DataTestFactory.createCandidateManagements();
    	list<String> condIds=new list<String>();
    	for(Contact con: conds){
    		condIds.add(con.Id);
    	}
    	PlacementSelector pSelector=new PlacementSelector();
    	list<Placement_candidate__c> cms=pSelector.getPlacedCMFromApprover(condIds);
    	 System.assert(cms.size()>0);
    	}
    }
    static testMethod void myUnitTest5() {
    	
    	System.runAs(DummyRecordCreator.platformUser) {
    	list<Contact> conds=DataTestFactory.createContacts();
    	list<Placement_Candidate__c> placements=DataTestFactory.createCandidateManagements();
    	list<String> condIds=new list<String>();
    	for(Contact con: conds){
    		condIds.add(con.Id);
    	}
    	PlacementSelector pSelector=new PlacementSelector();
    	list<Placement_candidate__c> cms=pSelector.getPlacedCMFromBillingContact(condIds);
    	 System.assert(cms.size()>0);
    	}
    }
    static testMethod void myUnitTest4() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	list<Workplace__c> workplaces=DataTestFactory.createWorkplaces();
    	list<Placement_Candidate__c> placements=DataTestFactory.createCandidateManagements();
    	list<String> workplaceIds=new list<String>();
    	for(Workplace__c workplace: workplaces){
    		workplaceIds.add(workplace.Id);
    	}
    	PlacementSelector pSelector=new PlacementSelector();
    	list<Placement_candidate__c> cms=pSelector.getPlacedCMFromWorkplaces(workplaceIds);
    	 System.assert(cms.size()>0);
    	 
    	}
    }
}