@isTest
private class CandidatePlacedTest
{
     static testmethod void runCandidatePlactTest(){
     	
     	System.runAs(DummyRecordCreator.platformUser) {
     		// insert PCAppSwitch__c
	        PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
	        //cs.Name = 'PlacedCandidate_1';
	        cs.Placed_Candidate_Condition__c = 'Interested|Evaluation - Hiring Manager';//create custom setting condidtion field. Text field can be insert, but the checkbox can't be.
	        insert cs;
	        system.debug('cs ='+ cs);
	
			// insert a new account	
	        Account acct = new Account(Name = 'test');        
	        insert acct ; 
	        
	        // insert new vacancies
	        List<Placement__c> vanList = new List<Placement__c>();
	        Placement__c v1 = new Placement__c(Company__c=acct.Id,Name='vacancy1');
	        Placement__c v2 = new Placement__c(Company__c=acct.Id,Name='vacancy2');
	        vanList.add(v1);
	        vanList.add(v2);
	        insert vanList;    
	         
	        // insert new candidate
	        List<Contact> conList = new List<Contact>();
	        Contact con1 = new Contact(LastName = 'con1', Email = 'test1@user2test.com');        
	        Contact con2 = new Contact(LastName = 'con2', Email = 'test2@user2test.com'); 
	        Contact con3 = new Contact(LastName = 'con3', Email = 'test3@user2test.com'); 
	        Contact con4 = new Contact(LastName = 'con4', Email = 'test4@user2test.com');        
	        Contact con5 = new Contact(LastName = 'con5', Email = 'test5@user2test.com'); 
	        Contact con6 = new Contact(LastName = 'con6', Email = 'test6@user2test.com');
	        Contact con7 = new Contact(LastName = 'con7', Email = 'test7@user2test.com');
	        conList.add(con1); 
	        conList.add(con2);
	        conList.add(con3);
	        conList.add(con4);
	        conList.add(con5);
	        conList.add(con6);
	        conList.add(con7);
	        insert conList;
	        
	        
	        // insert new candidate managements
	        List<Placement_Candidate__c> cmList = new List<Placement_Candidate__c>();
	        Placement_Candidate__c cm1 = new Placement_Candidate__c(Placement__c = v1.Id ,Candidate__c = con1.Id, Status__c = 'Placed'); 
	        Placement_Candidate__c cm2 = new Placement_Candidate__c(Placement__c = v2.Id ,Candidate__c = con2.Id, Status__c = 'Placed'); 
	        Placement_Candidate__c cm3 = new Placement_Candidate__c(Placement__c = v1.Id ,Candidate__c = con3.Id); 
	        Placement_Candidate__c cm4 = new Placement_Candidate__c(Placement__c = v2.Id ,Candidate__c = con4.Id); 
	        cmList.add(cm1);
	        cmList.add(cm2);
	        cmList.add(cm3);
	        cmList.add(cm4);
	        insert cmList;
	        
	        // debug and assert after initial insertion
	        Placement__c v1Original = [select id, Placed_Candidate__c from Placement__c where id =: v1.id];
	        Placement__c v2Original = [select id, Placed_Candidate__c from Placement__c where id =: v2.id];
	        System.debug('******************** cm1 id:'+ cm1.id) ;
	        System.debug('******************** cm2 id:'+ cm2.id) ; 
	        System.debug('******************** con1 id:'+ con1.id) ;
	        System.debug('******************** con2 id:'+ con2.id) ;       
	        System.debug('******************** v1 pcid:'+ v1Original.Placed_Candidate__c) ;
	        System.debug('******************** v2 pcid:'+ v2Original.Placed_Candidate__c) ;
	        System.assert(v1Original.Placed_Candidate__c == con1.Id);
	        System.assert(v2Original.Placed_Candidate__c == con2.Id);
	
	        //wait();
	        //wait();
	        
	        
	        // test: insert two new candidate management
	        List<Placement_Candidate__c> cmList4InsertTest = new List<Placement_Candidate__c>();
	        Placement_Candidate__c cm5 = new Placement_Candidate__c(Placement__c = v1.Id ,Candidate__c = con5.Id, Status__c = 'Interested' , Candidate_Status__c  = 'Placed'  );
	        Placement_Candidate__c cm6 = new Placement_Candidate__c(Placement__c = v2.Id ,Candidate__c = con6.Id, Status__c = 'Interested' , Candidate_Status__c  = 'Placed'  );
	        cmList4InsertTest.add(cm5);
	        cmList4InsertTest.add(cm6);
	        insert cmList4InsertTest;
	        
	        // assert after insertion
	        Placement__c v1AfterInsertion = [select id,name, Placed_Candidate__c from Placement__c where id =: v1.id];
	        Placement__c v2AfterInsertion = [select id,name, Placed_Candidate__c from Placement__c where id =: v2.id];
	        Map<String,String> conIdMap1= new Map<String,String>();
	        conIdMap1.put(con1.id,con1.id);
	        conIdMap1.put(con5.id,con5.id);
	        
	        Map<String,String> conIdMap2 = new Map<String,String>();
	        conIdMap2.put(con2.id,con2.id);
	        conIdMap2.put(con6.id,con6.id); 
	        
	        System.debug('******************** v1 id:'+ v1.id) ;
	        System.debug('******************** v2 id:'+ v2.id) ;
	        System.debug('******************** cm5 id:'+ cm5.id) ;
	        System.debug('******************** cm6 id:'+ cm6.id) ;
	        System.debug('********************* v1AfterInsertion.Placed_Candidate__c:'+v1AfterInsertion.Placed_Candidate__c) ; 
	        System.debug('********************* v2AfterInsertion.Placed_Candidate__c:'+v2AfterInsertion.Placed_Candidate__c) ;
	        System.debug('*********************** con5.Id:'+con5.Id) ; 
	        System.debug('*********************** con6.Id:'+con6.Id) ;
	        System.assert(conIdMap1.containsKey(v1AfterInsertion.Placed_Candidate__c) == true);
	        System.assert(conIdMap2.containsKey(v2AfterInsertion.Placed_Candidate__c) == true);
	 
	        //wait();
	        //wait();
	        
	        // test: update two existing candidate management
	        List<Placement_Candidate__c> cmList4UpdateTest = new List<Placement_Candidate__c>();
	        cm3.Status__c = 'Interested';
	        cm3.Candidate_Status__c = 'Evaluation - Hiring Manager';
	        cm4.Candidate_Status__c = 'Placed';
	        cmList4UpdateTest.add(cm3);
	        cmList4UpdateTest.add(cm4);
	        update cmList4UpdateTest;
	        
	        // assert after update
	        Placement__c v1AfterUpdate = [select id,name, Placed_Candidate__c from Placement__c where id =: v1.id];
	        Placement__c v2AfterUpdate = [select id,name, Placed_Candidate__c from Placement__c where id =: v2.id];
	        conIdMap1.put(con3.id,con3.id);
	        conIdMap2.put(con4.id,con4.id);
	        System.debug('******************** v1 id:'+ v1.id) ;
	        System.debug('******************** cm3 id:'+ cm3.id) ;
	        System.debug('******************** v2 id:'+ v2.id) ;
	        System.debug('******************** cm4 id:'+ cm4.id) ;
	        System.debug('********************* v1AfterUpdate.Placed_Candidate__c:'+v1AfterUpdate.Placed_Candidate__c) ; 
	        System.debug('********************* v2AfterUpdate.Placed_Candidate__c:'+v2AfterUpdate.Placed_Candidate__c) ;
	        System.debug('*********************** con3.Id:'+con3.Id) ; 
	        System.debug('*********************** con4.Id:'+con4.Id) ;
	        System.assert(conIdMap1.containsKey(v1AfterUpdate.Placed_Candidate__c) == true);
	        System.assert(conIdMap2.containsKey(v2AfterUpdate.Placed_Candidate__c) == true);
	        
	        
	        // insert a non-placed candidate, placed candidate should not be changed.
	        Placement_Candidate__c cm7 = new Placement_Candidate__c(Placement__c = v1.Id ,Candidate__c = con7.Id, Status__c = 'New' );
	        insert cm7;
	        //System.assert(v1.Placed_Candidate__c == con3.Id);
	        System.debug(v1.Placed_Candidate__c) ;    
	        
	        // update a candidate management record (not placed), placed candidate should not be changed.
	        cm1.Status__c = 'New';
	        cm1.Candidate_Status__c  = 'New';  
	        update cm1;
	        //System.assert(v1.Placed_Candidate__c == con3.Id);
	        System.debug(v1.Placed_Candidate__c) ;   
     	}
        
              
    }
    
    static testmethod void runmore200CandidatePlactTest(){
    	System.runAs(DummyRecordCreator.platformUser) {
    		Account acct = new Account(Name = 'test');        
	        insert acct ; 
	        
	        // insert new vacancies
	        Placement__c v1 = new Placement__c(Company__c=acct.Id,Name='vacancy1');
	        insert v1;
	        
	    	List<Contact> tempcontactlist = contactlist();
	    	List<Placement_Candidate__c> cmList = new List<Placement_Candidate__c>();
	    	for(Contact con : tempcontactlist){
	    		Placement_Candidate__c cm5 = new Placement_Candidate__c(Placement__c = v1.Id ,Candidate__c = con.Id, Status__c = 'Interested' , Candidate_Status__c  = 'Placed'  );
	    		cmList.add(cm5);
	    	}
	    	insert cmList;
	
	        v1 = [select id, Placed_Candidate__c from Placement__c where id =: v1.id];
	        system.assertNotEquals(v1.Placed_Candidate__c, null);
    	}
    	
    }
    
    static List<Contact> contactlist(){        
        List<Contact> contactlist = new List<Contact>();
        
    	for(Integer i=0;i<600;i++){
    		Contact con1 = new Contact(LastName = 'con'+i, Email = 'test_'+i+'@user2test.com');
    		contactlist.add(con1);
    	}
    	insert contactlist;
    	return contactlist;
    }
}