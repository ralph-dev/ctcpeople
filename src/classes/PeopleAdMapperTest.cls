@isTest
public class PeopleAdMapperTest
{
	@isTest
	static void peopleAdMapperTest()
	{
		Advertisement__c ad = createTestAd();
		PeopleAdMapper peopleAd = new PeopleAdMapperIndeed(ad);
		peopleAd.setAd(ad);
		peopleAd.getFieldMap4NewXML();
		system.assert(peopleAd.fieldMap4NewXML.size()>0);		
	}

	public static Advertisement__c createTestAd() {
		Advertisement_Configuration__c tempIndeedAccount  = AdConfigDummyRecordCreator.createDummyIndeedAccount();
		Advertisement__c dummyad = new Advertisement__c();
		String Indeedrecordtypeid = JobBoardUtils.getRecordId('Indeed');
		dummyad.Advertisement_Account__c = tempIndeedAccount.id;
		dummyad.recordtypeid = Indeedrecordtypeid;
		dummyad.Job_Title__c = 'test';
		dummyad.Status__c = 'Active';
		dummyad.Placement_Date__c = System.today().addDays(-29);
		dummyad.Job_Function__c = 'Administrative';
		dummyad.Industry__c = 'Accounting';
		dummyad.Job_Type_Text__c = 'Full-time';
		dummyad.Experience_Level__c = 'Executive';
		dummyad.Poster_Role__c = 'Hiring Manager';
		insert dummyad;
		return dummyad;
	
	}
}