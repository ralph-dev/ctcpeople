public with sharing class JXTEdit {
    public String short_description_contentTemp {get; set;}
    public String jobContentTemp {get; set;}
    public Advertisement__c ad {get; set;}
    public List<SelectOption> styleOptions = new List<SelectOption>();
    public List<SelectOption> templateOptions = new List<SelectOption>();
    public IEC2WebService iec2ws{get; set;}
    public Boolean enableEmailService {get; set;}
    public String applicationEmail {get; set;}
    public Advertisement__c currentAd {get; set;}
    
    /*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    /*************End Add skill parsing param********************************************************/
    
    //Init Error Message for customer, Add by Jack on 10/09/2014
    public PeopleCloudErrorInfo jxt_Errormesg; 
    public Boolean jxtMsgSignalhasmessage{get ;set;} //show error message or not
    
    //15 digit org Id
    public static final String ORGNISATION_ID_15 = UserInfo.getOrganizationId().subString(0, 15);
    
    public JXTEdit(ApexPages.StandardController stdController){
    	
    	//Init skill parsing param. Add by Jack on 23/05/2013
        enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
        enableSkillGroupList = new List<Skill_Group__c>();
        enableSkillGroupList = SkillGroups.getSkillGroups();
        defaultskillgroups = new List<String>();
        //end skill parsing param.
        
        ad = (Advertisement__c) stdController.getRecord();
        iec2ws = new EC2WebServiceImpl();  
        if(ad.Skill_Group_Criteria__c!=null&&ad.Skill_Group_Criteria__c!=''){
        	String defaultskillgroupstring = ad.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
	        defaultskillgroups = defaultskillgroupstring.split(',');
        }
        AdvertisementSelector adSelector = new AdvertisementSelector();
        currentAd = adSelector.getVacIdByAdId(ad.Id);
        checkEmailServiceSetting();
    } 
    
    public PageReference feedXml(){
        Boolean insertDBSuccess = true; //Status of Insert DB 
    	Integer updateDBSuccess = 1;    //Status of Update DB
        
        if (enableEmailService) { // using email service
            ad.Prefer_Email_Address__c = applicationEmail;
        } else {   // using application form
            Boolean changeToApplicationForm = false; // previously is using email service, but to change to application form when update
            if (String.isNotBlank(currentAd.Prefer_Email_Address__c)) {
                ad.Prefer_Email_Address__c = '';
                changeToApplicationForm = true;
            }
            
    	    String jobRefCode= ORGNISATION_ID_15 +':'+String.valueOf(ad.id);
    	    UserSelector userSelector = new UserSelector();
            User currentUser = userSelector.getUser(UserInfo.getUserId());
    	    StyleCategorySelector scSelector = new StyleCategorySelector();
    	    StyleCategory__c sc = scSelector.getStyleCatagoryById(ad.Application_Form_Style__c);
            
            //If status is not valid, clone to post, or application method changed from email service to application form, need to insert again    
            if (ad.Status__c.equalsignorecase('not valid') || ad.Status__c.equalsignorecase('clone to post') || changeToApplicationForm){
        	    insertDBSuccess = iec2ws.insertDetailTable(jobRefCode, ORGNISATION_ID_15,
					                String.valueOf(ad.id), currentUser.AmazonS3_Folder__c, JobBoardUtils.blankValue(sc.Header_EC2_File__c), 
					                JobBoardUtils.blankValue(sc.Header_File_Type__c), sc.Div_Html_EC2_File__c,
					                sc.Div_Html_File_Type__c,  JobBoardUtils.blankValue(sc.Footer_EC2_File__c), 
					                JobBoardUtils.blankValue(sc.Footer_File_Type__c), '', 'jxt', 
					                JobBoardUtils.blankValue(ad.Job_Title__c));
            } else {
                //updateDBSuccess = iec2ws.updateDetailTable(ad.id, sc.Header_EC2_File__c, sc.Header_File_Type__c, 
                //        sc.Div_Html_EC2_File__c, sc.Div_Html_File_Type__c, 
                //                    sc.Footer_EC2_File__c, sc.Footer_File_Type__c, ORGNISATION_ID_15);
	            updateDBSuccess = iec2ws.updateDetailTable(ad.id, sc.Header_EC2_File__c, sc.Header_File_Type__c, 
	                                sc.Div_Html_EC2_File__c, sc.Div_Html_File_Type__c, 
	                                sc.Footer_EC2_File__c, sc.Footer_File_Type__c, ORGNISATION_ID_15, JobBoardUtils.blankValue(ad.Job_Title__c));
            }
        }
        
        ad.JobXML__c = JxtUtils.getFeed(ad);
        
        /*****    added by kevin for bug fixing (PC-368)  28.06.2012  ****/
        
        if(insertDBSuccess == false){
        	ad.Status__c = 'Not Valid';
	    	ad.Job_Posting_Status__c = 'Insert/Update URL Failed';
	    	ad.Application_URL__c = '';
	    	jxtMsgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.INSERT_OR_UPDATE_DYNAMIC_APPLICATION_FORM_FAILED);
	    	if(!Test.isRunningTest()){
                checkFLS();
            }
            update ad;
			return null;  
        } else if(updateDBSuccess != 1){//if update fail, update posting status to Insert/update URL failed. And show error message
	    	ad.Status__c = 'Active';
	    	ad.Job_Posting_Status__c = 'Insert/Update URL Failed';
	    	jxtMsgSignalhasmessage = customerErrorMessage(PeopleCloudErrorInfo.INSERT_OR_UPDATE_DYNAMIC_APPLICATION_FORM_FAILED);
            checkFLS();
            update ad;
			return null;  
		}else{
	        if(currentAd.Status__c == 'Active'){
	            ad.Job_Posting_Status__c = 'active'; 
	            ad.Status__c = 'Active';
	        }else if(currentAd.Status__c == 'Clone to Post' || currentAd.Status__c.equalsignorecase('not valid')){
	            ad.Job_Posting_Status__c = 'In Queue'; 
	            ad.Status__c = 'Active';           
	        }else{
	            ad.Status__c = currentAd.Status__c;
	        }
        }     
        /*****    End of bug fixing (PC-368)    ****/
        if(!Test.isRunningTest()){
           checkFLS(); 
        }
        update ad;
        JXTPostingCallWebService.callJxtFeedWebService(ad, 'Update');
        
        return new ApexPages.Standardcontroller(ad).view();
    }
    public List<SelectOption> getTemplateOptions(){
        return JobBoardUtils.getTemplateOptions('jxt');
    }
    public List<SelectOption> getStyleOptions(){
        return JobBoardUtils.getStyleOptions('jxt');
    }
    public PageReference saveAd(){
    	PageReference returnPage = null;
        ad.JXT_Short_Description__c = short_description_contentTemp;
        ad.Job_Content__c = jobContentTemp;
        //insert skill group ext id
	    if(enableskillparsing){
	    	String tempString  = String.valueOf(defaultskillgroups);
	        tempString = tempString.replaceAll('\\(', '');
	        tempString = tempString.replaceAll('\\)', '');
	        tempString = tempString.replace(' ','');
	       	ad.Skill_Group_Criteria__c = tempString;
	    }
	    //end insert skill group ext id
        returnPage = feedXml();
        return returnPage;
    }  
    public List<SelectOption> getReferralItems() {
        return JxtUtils.getReferralItems();
    }
    
    //insert the skill group picklist value option
    public List<SelectOption> getSelectSkillGroupList(){
	  	List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
	  	List<SelectOption> displaySelectOption = new List<SelectOption>();
	  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
	  		displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
	  	}
	  	return displaySelectOption;
	}
	
	//Generate Error message, Add by Jack on 10/09/2014
    public static Boolean customerErrorMessage(Integer msgcode){
        PeopleCloudErrorInfo errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        Boolean msgSignalhasmessage = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
                ApexPages.addMessage(newMessage);
            }
        return msgSignalhasmessage;               
    }
    
	public void checkEmailServiceSetting() {
	    JobPostingSettings__c jobPostingSetting = JobPostingSettings__c.getInstance(UserInfo.getUserId());
	    if (jobPostingSetting != null) {
	        enableEmailService = jobPostingSetting.Enable_JXT_Email_Service__c;
	        if (String.isBlank(currentAd.Prefer_Email_Address__c)) {
	            applicationEmail = jobPostingSetting.Default_Email_Service_Address__c;
	        } else {
	            applicationEmail = currentAd.Prefer_Email_Address__c;
	        }
	    } else {
	        enableEmailService = false;
	    }
	}

     private void checkFLS(){
      List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Advertisement__c.Application_URL__c,
            Advertisement__c.ListingID__c,
            Advertisement__c.Ad_Closing_Date__c,
            Advertisement__c.Placement_Date__c,
            Advertisement__c.Advertisement_Account__c,
            Advertisement__c.Indeed_Question_URL__c,
            Advertisement__c.Vacancy__c,
            Advertisement__c.Skill_Group_Criteria__c,
            Advertisement__c.Job_Type__c,
            Advertisement__c.Job_Title__c,
            Advertisement__c.JXT_Short_Description__c,
            Advertisement__c.Bullet_1__c,
            Advertisement__c.Bullet_2__c,
            Advertisement__c.Bullet_3__c,
            Advertisement__c.Job_Content__c,
            Advertisement__c.Job_Contact_Name__c,
            Advertisement__c.Company_Name__c,
            Advertisement__c.Nearest_Transport__c,
            Advertisement__c.Residency_Required__c,
            Advertisement__c.IsQualificationsRecognised__c,
            Advertisement__c.Location_Hide__c,
            Advertisement__c.TemplateCode__c,
            Advertisement__c.AdvertiserJobTemplateLogoCode__c,
            Advertisement__c.ClassificationCode__c,
            Advertisement__c.SubClassificationCode__c,
            Advertisement__c.Classification2Code__c,
            Advertisement__c.SubClassification2Code__c,
            Advertisement__c.WorkTypeCode__c,
            Advertisement__c.SectorCode__c,
            Advertisement__c.Street_Adress__c,
            Advertisement__c.Search_Tags__c,
            Advertisement__c.CountryCode__c,
            Advertisement__c.LocationCode__c,
            Advertisement__c.AreaCode__c,
            Advertisement__c.Salary_Type__c,
            Advertisement__c.SeekSalaryMin__c,
            Advertisement__c.SeekSalaryMax__c,
            Advertisement__c.Salary_description__c,
            Advertisement__c.No_salary_information__c,
            Advertisement__c.Has_Referral_Fee__c,
            Advertisement__c.Referral_Fee__c,
            Advertisement__c.Terms_And_Conditions_Url__c,
            Advertisement__c.Status__c,
            Advertisement__c.Job_Posting_Status__c,
            Advertisement__c.JobXML__c,
            Advertisement__c.Prefer_Email_Address__c,
            Advertisement__c.RecordTypeId,
            Advertisement__c.Website__C
      };

      fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
      fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    } 
   
}