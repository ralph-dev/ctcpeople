public with sharing class PlacementCandidateExtension {
	public PlacementCandidateExtension() {}

	public PlacementCandidateExtension(Object obj) {}

	@RemoteAction
	public static List<Placement_Candidate__c> createRecords(List<Id> contactIds, List<Id> vacancyIds) {
		return new PlacementCandidateService().createRecords(contactIds, vacancyIds);
	}

	@RemoteAction
	public static List<Placement_Candidate__c> getTop5Records(String contactId) {
	    return new PlacementCandidateSelector().getTop5CandidateManagementByContactId(contactId);
	}

	@RemoteAction
	public static List<Placement_Candidate__c> getCMListByVacancyId(String vacancyId, String selectedColumn) {
		return new PlacementCandidateSelector().getCandidateMangementByVacancyIdAndSelectedColumn(vacancyId, selectedColumn);
	}

	@RemoteAction
	public static List<Placement_Candidate__c> getCMListByVacancyIdWithCMId(String vacancyId, String selectedColumn, List<Id> cmListId) {
		return new PlacementCandidateSelector().getCandidateMangementByVacancyIdAndSelectedColumn(vacancyId, selectedColumn, cmListId);
	}

	@RemoteAction
	public static List<Placement_Candidate__c> updateRecords(List<Map<String, Object>> cmList) {
		return new PlacementCandidateService().updateRecords(cmList);
	}

	@RemoteAction
	public static String getCMFieldsList(){
		return new PlacementCandidateService().getAllCMFields();
	}

	/**
     *  Provide additional information on the remote action API
     * */
    @RemoteAction
    public static Map<String, RemoteAPIDescriptor.RemoteAction> apiDescriptor(){
        RemoteAPIDescriptor descriptor = new RemoteAPIDescriptor();
        descriptor.description('CRUD & describe remote action for Search.');
		descriptor.action('createRecords').param(List<Id>.Class).param(List<Id>.Class);
		descriptor.action('getTop5Records').param(String.Class);
		descriptor.action('getCMListByVacancyId').param(String.Class).param(String.Class);
		descriptor.action('getCMListByVacancyIdWithCMId').param(String.Class).param(String.Class).param(List<Id>.Class);

		descriptor.action('updateRecords').param(List<Map<String, Object>>.class);
		descriptor.action('getCMFieldsList');
        return descriptor.paramTypesMap;
    } 
}