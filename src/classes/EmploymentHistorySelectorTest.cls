@isTest
public class EmploymentHistorySelectorTest {

	static testmethod void testGetContactEmploymentHistory(){
		TriggerHelper.disableAllTrigger();
	 	EmploymentDummyRecordCreator emCreator = new EmploymentDummyRecordCreator();
	    List<Id> conIds = emCreator.generateDummyRecord();
	    
		system.runAs(DummyRecordCreator.platformUser) {
			EmploymentHistorySelector emSel = new EmploymentHistorySelector();
			List<Employment_History__c> emList = emSel.getContactEmploymentHistory(conIds);
			system.assertNotEquals(null, emList);
		}
	}
}