/*
*Author by Jack 
*Generate the Contact and Contact's emails fields value
*
*/

public with sharing class GetContactJson {
	//Generate the dynamic SOQL for select Email detail
	public static string dynamicContactEmailField(){
		List<String> ContactEmailObjFields = new List<String>();	
		String ContactEmailsql = '';	
		try{	
			ContactEmailObjFields = FieldsClass.getContactEmailObjFields(Contact.SObjectType,'Email','Email');  //get all contact Email type fields
			for(String emailFieldAPIName : ContactEmailObjFields){	//generate the Email fields name as the part of the query string		
				if(emailFieldAPIName != null && emailFieldAPIName != ''  && emailFieldAPIName != 'Email'){
					if(ContactEmailsql != '' && ContactEmailsql != null ){
						ContactEmailsql = ContactEmailsql + ', ' + emailFieldAPIName;
					}
					else{
						ContactEmailsql = ','+ emailFieldAPIName;
					}
				}
			}			
		}	
		catch (system.exception e){
        	NotificationClass.notifyErr2Dev('GetContactJson/dynamicContactEmailField', e); 
		 }
		 return ContactEmailsql;
	}
	
	public static String generateSearchCriterial(String searchcrestep2, List<Account> tempaccount, List<Contact> tempcontact) {
	    String searchCri = '' ;
		if(searchcrestep2 != null && searchcrestep2 !=''){
			if(tempaccount.size()>0 && tempcontact.size()>0){
				searchCri = 'and (id in: tempcontact or Account.id in: tempaccount)';
			}
			else if(tempaccount.size()>0 && tempcontact.size()<1){
				searchCri = 'and Account.id in: tempaccount';
			}
			else if(tempaccount.size()<1 && tempcontact.size()>0){
				searchCri = 'and contact.id in: tempcontact';
			}
			else{
				searchCri = null;
			}
		}
		return searchCri;
	}
	
	public static String generateJson(String searchcrestep2){
		String tempContactJson = '';
		String ContactEmailJson = '';
		String contactJson = ''; 
		List<Contact> tempcontact = null;
		List<Account> tempaccount = null;
		
		if(searchcrestep2 != null && searchcrestep2 !=''){
		    tempaccount = DaoAccount.getAccsContainNameSR(searchcrestep2);
			tempcontact = DaoContact.getClientContactsbyNameSR(searchcrestep2);
		}
		
        String searchCri = generateSearchCriterial(searchcrestep2, tempaccount, tempcontact);
        if(searchCri == null) {
            searchCri = '';
            contactJson = '[{value:\"null:null\",label:\"---No matching Contacts or Companies have been found. Please try different letters---\", addemail:\'["<option value=\\\"NoEmail\\\">--No Email--<\\/option>"]\'}]';
			return contactJson;
        }
        
		List<String> ContactEmailObjFields = FieldsClass.getContactEmailObjFields(Contact.SObjectType,'Email','Email');
		
		//modified by andy for searching contact in send resume function
		//List<RecordType> contactRecordType = DaoRecordType.nonCandidateRTs;  //get all no candidate Recordtype
		List<RecordType> contactRecordType = DaoRecordType.contactRTs;  //get all contact Recordtype
		List<Contact> allContactInfo = new List<Contact>();
		
		//system.debug('searchCri: ' + searchCri);
		//system.debug('dynamicContactEmailField: ' + GetContactJson.dynamicContactEmailField());
		
		String dynamicContactEmailsql ='select id,Name,Account.id,Account.Name, Email' + GetContactJson.dynamicContactEmailField() +' from Contact where RecordTypeId in: contactRecordType and Email != null '+ searchCri; //+ tempEmailQueryCondition;
		//system.debug('dynamicContactEmailsql = '+dynamicContactEmailsql);
		
		CommonSelector.checkRead(Contact.sObjectType,'id,Name,Account.id,Account.Name, Email' + GetContactJson.dynamicContactEmailField());
		allContactInfo = Database.Query(dynamicContactEmailsql);  //get all contact detial and the contact include at least one email
		//system.debug('allContactInfo =' + allContactInfo);
		if(allContactInfo != null && allContactInfo.size()>0){
			try{
				for(Contact contact : allContactInfo){
					String tempContactEmailJson = '';
					system.debug('contactname = ' + contact.name);					
					//Generate ContactJson string
					if(contactJson.equals(null) ||contactJson.equals('')){
						for(String contctEmailFieldAPIName : ContactEmailObjFields){
							String tempcontactEmail = '';
							if(contctEmailFieldAPIName !='Email'){ //
								Object contactEmailType = contact.get(contctEmailFieldAPIName);
								tempcontactEmail = String.valueof(contactEmailType);																												
								if( tempcontactEmail != null && tempcontactEmail != ''){
									if(tempContactEmailJson != null && tempContactEmailJson != ''){
										tempContactEmailJson += ',"<option value = \\\"' + contact.id + ':' + tempcontactEmail+ '\\\">' + tempcontactEmail +'<\\/option>"';
									}
									else{
										tempContactEmailJson = '"<option value=\\\"'+ contact.id + ':NoEmail\\\">--Please Select Additional Email--<\\/option><option value = \\\"' + contact.id + ':' + tempcontactEmail+ '\\\">' + tempcontactEmail +'<\\/option>"';
									}
								}
								else{
									tempContactEmailJson += '';
								}
							}		
						}
						if(tempContactEmailJson == ''){
							tempContactEmailJson = '["<option value = \\\"' + contact.id + ':' +'NoAdditionalEmail\\\">--No Additional Email--<\\/option>"]';
						}
						else{
							tempContactEmailJson = '[' + tempContactEmailJson + ']';
						}				
						if(tempContactJson != null && tempContactJson != ''){
							tempContactJson += ',{value:"' + contact.Id + ':'+ contact.Account.id + '",label:"'+ contact.Name +' , '+ contact.Account.Name + '",desc:"'+contact.Email +'",addemail:\''+tempContactEmailJson+'\'}';						
						}
						else{
							tempContactJson = '{value:"' + contact.Id + ':'+ contact.Account.id + '",label:"'+ contact.Name +' , '+ contact.Account.Name + '",desc:"'+contact.Email +'",addemail:\''+tempContactEmailJson+'\'}';				
						}
					}
					else{
						tempContactJson ='';				
					}	
					
					//Generate ContactEmailJson
					
					//system.debug('tempContactEmailJson =' + tempContactEmailJson);						
				}
				contactJson = '['+tempContactJson+']';
				//contactEmailJson = '{ \"null:null\":["<option value=\\\"NoEmail\\\">--No Email--<\\/option>"],' + contactEmailJson + '}';
				//system.debug('contactJson = ' + contactJson);
				//system.debug('contactEmailJson = ' + contactEmailJson);	
			}
			catch (system.exception e){
	        	NotificationClass.notifyErr2Dev('GetContactJson/ContactJson', e); 
			 }
		}
		else{
			contactJson = '[{value:\"null:null\",label:\"---No Suitable Contact or Company, Please try to type another keywords---\", addemail:\'["<option value=\\\"NoEmail\\\">--No Email--<\\/option>"]\'}]';
		}
		return contactJson;	
	}
	
	static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){//Send error message
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());    
    }	

}