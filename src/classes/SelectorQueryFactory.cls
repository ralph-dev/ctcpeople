public class SelectorQueryFactory {
    public static String ASC_ORDER = 'ASC';
	public static String DESC_ORDER = 'DESC';
	
	private Schema.SObjectType type;	
	private Set<String> fieldsToSelect;
	private String condition;
	private Integer recLimit;
	private String recOrder;
	private String fieldToOrder;
	
	private Boolean enforceFieldLevelAccess;
	
	public SelectorQueryFactory(Schema.SObjectType t, Boolean enforceFieldLevelAccess){
		type = t;
		this.enforceFieldLevelAccess = enforceFieldLevelAccess;
		fieldsToSelect = new Set<String>();
		condition = '';
	}
	
	public SelectorQueryFactory selectFields(List<String> queryFields){
		fieldsToSelect.addAll(queryFields);
		return this;
	}
	
	public SelectorQueryFactory selectFields(Set<String> queryFields){
		fieldsToSelect.addAll(queryFields);
		return this;
	}
	
	public SelectorQueryFactory selectFields(LIST<Schema.SObjectField> queryFields){
		for(Schema.SObjectField qFields : queryFields){
			DescribeFieldResult fieldToUse = qFields.getDescribe();
			if(!enforceFieldLevelAccess || fieldToUse.isAccessible()){
				fieldsToSelect.add(fieldToUse.getName());
			}
		}
		return this;
	}
	
	public SelectorQueryFactory setCondition(String con){
		condition = con;
		return this;
	}
	
	public SelectorQueryFactory setOrder(String sOrder, Schema.SObjectField fToOrder){
		setOrder(sOrder, fToOrder.getDescribe().getName());
		return this;
	}
	
	public SelectorQueryFactory setOrder(String sOrder, String fToOrder){
		recOrder = sOrder;
		fieldToOrder = fToOrder;
		return this;
	}
	
	public SelectorQueryFactory setLimit(Integer i){
		recLimit = i;
		return this;
	}
	
	public override String toString(){
		String output = 'SELECT ';
		final String FIELD_SEPARATOR = ', ';
		if(fieldsToSelect != null && !fieldsToSelect.isEmpty()){
			for(String fs : fieldsToSelect){
				output += fs + FIELD_SEPARATOR;
			}
		}else{
			output += 'Id';
		}
		
		output = output.removeEnd(FIELD_SEPARATOR) + ' ';
		
		output += 'FROM ' + type.getDescribe().getName() + ' ';
		
		if(condition != null){
			output += 'WHERE ' + condition + ' ';
		}
		
		if(fieldToOrder != null){
			output += 'ORDER BY ' + ' ' + fieldToOrder +' '+recOrder+ ' ';
		}
		
		if(recLimit != null){
			output += 'LIMIT ' + recLimit + ' ';
		}
		
		output = output.removeEnd(' ');
		
		//system.debug(LoggingLevel.FINE, '\n\nQuery:\n' + output + '\n\n');
		return output;
	}
}