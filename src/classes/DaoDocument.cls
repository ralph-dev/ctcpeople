/**
* Author: Raymond
* 
* A Data access object class that used to encapsulate the data operations for Document object
*/
public with sharing class DaoDocument extends CommonSelector{
	
	private static DaoDocument dao = new DaoDocument();
	
	public DaoDocument(){
		super(Document.sObjectType);
	}
	
	public static Document getDocById(String docId){
		Document doc = null;
		try{
			
			dao.checkRead('Id,Name,DeveloperName, Body ');
			doc = [select Id,Name,DeveloperName, Body 
					from Document where Id = : docId];
		}catch(Exception e){
			return null;
		}
		
		return doc;
	}
	
	
	public static List<Document> getDocsByFolder(String folderId){
		List<Document> result;
    	try{  
    		dao.checkRead('Url, Type, NamespacePrefix, Name, Keywords, IsPublic, IsInternalUseOnly, IsDeleted, Id, FolderId, DeveloperName, Description, ContentType, BodyLength, AuthorId');
			result =  [Select Url, Type, NamespacePrefix, Name, 
					Keywords, IsPublic, IsInternalUseOnly, 
					IsDeleted, Id, FolderId, 
					DeveloperName, Description, ContentType, 
					BodyLength, AuthorId From Document where FolderId=:folderId];
		}catch(Exception e){
    		result = new List<Document>();
    	}
    	return result;
	}
	
	/*
		CTC contain configuration files in PeopleCloud Package which
		are stored as Document. From SF Win 12, those config files are
		no longer directly editable from program.
		
		This method is used to find or create config file based on packaged
		standard configure file. 
	*/
	public static Document getCTCConfigFileByDevName(String fileName){
		// build the file name
		String orgId = UserInfo.getOrganizationId();
		String realName = fileName+'_'+orgId;
		
		Document result = null;
		try{
			Folder ctcFolder = DaoFolder.getFolderByDevName('CTC_Admin_Folder');
			system.debug('folderId ='+ ctcFolder.Id);
			try{
				dao.checkRead('Id,Name,DeveloperName, Body ');
				result = [select Id,Name,DeveloperName, Body 
							from Document where Type ='xml' and DeveloperName like: realName+'%' 
							and Name =: realName and FolderId = : ctcFolder.Id];
				//system.debug('real name:'+realName);			
			}catch(Exception e1){
				// add by raymond
				// if requested file doesn't exist, select standard file from CTC_Admin_Folder
				// then create the requested file with content filled with standard file content				
				if(result == null){	
				    if(!Test.isRunningTest()) {
				    	dao.checkRead('Id,DeveloperName, Body ');
				    	result = [select Id,DeveloperName, Body 
							from Document where Type ='xml' and DeveloperName=: fileName 
							and FolderId = : ctcFolder.Id];
							Document newFile = new Document();
							newFile.DeveloperName=realName+'_'+ Datetime.now().getTime();
							newFile.Body=result.Body;
							newFile.FolderId=ctcFolder.Id;
							newFile.Name = realName;
							newFile.Type='xml';
							checkFLS();
							insert newFile;
							result = newFile;
					} else {
						if(fileName.contains('ClicktoCloud_Xml')) {
							result = new Document();
							String dummyResultContactString = '<?xml version="1.0"?><!--file body starts here--><fileroot><object objectname="Contact"><field><label>Email</label><type>EMAIL</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_Email" value=""/></div>]]></component><apiname>Email</apiname><location>left</location><sequence>0</sequence><filterable>true</filterable></field><field><label>Last Name</label><type>STRING(80)</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_LastName" value=""/></div>]]></component><apiname>LastName</apiname><location>left</location><sequence>1</sequence><filterable>true</filterable></field><field><label>Title</label><type>STRING(80)</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_Title" value=""/></div>]]></component><apiname>Title</apiname><location>left</location><sequence>2</sequence><filterable>true</filterable></field><field><label>Full Name</label><type>STRING(121)</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_Name" value=""/></div>]]></component><apiname>Name</apiname><location>left</location><sequence>3</sequence><filterable>true</filterable></field><field><label>Mailing Country</label><type>STRING(40)</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_MailingCountry" value=""/></div>]]></component><apiname>MailingCountry</apiname><location>right</location><sequence>0</sequence><filterable>true</filterable></field><field><label>Mobile Phone</label><type>PHONE</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_MobilePhone" value=""/></div>]]></component><apiname>MobilePhone</apiname><location>right</location><sequence>1</sequence><filterable>true</filterable></field><field><label>First Name</label><type>STRING(40)</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_FirstName" value=""/></div>]]></component><apiname>FirstName</apiname><location>right</location><sequence>2</sequence><filterable>true</filterable></field></object><columns><column><label>Created Date</label><obj>EMPTY_STRING</obj><api>CreatedDate</api><type>DATETIME</type><sortable>true</sortable></column></columns></fileroot>';
	      					result.Name = 'ClicktoCloud_Xml(Contact)';
				            result.FolderId = UserInfo.getUserId();
				            result.Body = Blob.valueOf(dummyResultContactString);
				            result.Type = 'xml';
				            result.DeveloperName = fileName+'_Test_Contact'+system.currentTimeMillis();
				        }else if(fileName.contains('jobx_classifications')) {
				        	result = new Document();
							String dummyResultContactString = '<classifications><classification display="true">Accounting</classification></classifications>';
  							result.Name = 'jobx_classifications';
				            result.FolderId = UserInfo.getUserId();
				            result.Body = Blob.valueOf(dummyResultContactString);
				            result.Type = 'xml';
				            result.DeveloperName = fileName+'_Test_Contact';
				        }
				        else if(fileName.contains('Trigger_Admin')) {
				        	result = new Document();
							String dummyResultContactString = '<fileroot><trigger><apiname>UpdateAccountField</apiname><targetObject>Contact</targetObject><status>Active</status><accountid>00190000002KiLz</accountid><recordtypeid>012900000008i4y</recordtypeid><note><![CDATA[This trigger is used to enable automation of updating client/account of people.<br/> If a person has not a client related to such as independent candidate, this trigger will automatically assign a client to it. ]]></note></trigger><trigger><apiname>UpdateRemining</apiname><targetObject>Placement_Candidate__c</targetObject><status>InActive</status><accountid></accountid><recordtypeid></recordtypeid><note><![CDATA[This trigger is used to enable a reminder which pops up an alert once a candidate placed. ]]></note></trigger></fileroot>';
							result.Name = realName;
				            result.FolderId = ctcFolder.Id;
				            result.Body = Blob.valueOf(dummyResultContactString);
				            result.Type = 'xml';
				            result.DeveloperName = realName+'_'+ Datetime.now().getTime();
				        }
				        checkFLS();
						insert result;
					}
				}
			} 
		}catch(Exception e){
			NotificationClass.notifyErr2Dev('DaoDocument.getCTCConfigFileByName encounter problems.', e);
		}
		return result;
	}
	
	/*
		Author by Jack
		Let the customer to download the doc and preview the doc.
	*/
	public static Document getSendResumeDummyDoc(Id dummyFileId){
		String orgId = UserInfo.getOrganizationId();
		//String realName = fileName+'_'+orgId;
		Document result = null;
		Id ctcFolderid = UserInfo.getUserId();
		try{
				dao.checkRead('Id,Name,DeveloperName,type ,BodyLength');
				result = [select Id,Name,DeveloperName,type ,BodyLength
							from Document where id = :dummyFileId  and FolderId = : ctcFolderId];
				//system.debug('real name:'+realName);
		}
		catch(Exception e){
			
		}	
		return result;
	}
	
	/*
		Author by Jack
		Let the Bulk SMS And Bulk Email to fetch the doc content.
	*/
	public static Document getCandidateRecordDummyDoc(Id dummyFileId){
		String folderid = Userinfo.getUserId();
		Document result = null;
		try{
				dao.checkRead('Id,Name,DeveloperName,type ,Body');
				result = [select Id,Name,DeveloperName,type ,Body
							from Document where id = :dummyFileId  and FolderId = : folderid];
				//system.debug('real name:'+realName);
		}
		catch(Exception e){
			
		}	
		return result;
	}
	
	public static List<Document> getOldSendResumeDummyDoc(String folderId , String folderName){
		String orgId = UserInfo.getOrganizationId();
		//String realName = fileName+'_'+orgId;
		List<Document> result = null;
		//Folder ctcFolder = DaoFolder.getFolderByDevName(folderId);
		try{
			dao.checkRead('Id,Name,DeveloperName,type ');
			result = [select Id,Name,DeveloperName,type 
							from Document where DeveloperName like: folderName+'%'  and FolderId = : folderId and CreatedDate < today];
			//system.debug('real name:'+realName);
		}
		catch(Exception e){
			result = new List<Document>();
		}	
		return result;
	}
	
	//General the Search Criteria Document to personal folder
	public static Document getCTCSearchCriteriaDocument(String docfile, String ObjectString){
		String folderid =  Userinfo.getUserId();	
		String bodystr = '<?xml version="1.0"?><pages></pages>';
		User tempUser = DaoUsers.getUserSearchFileName(folderid);
		String namestr = '';
		Document tempdoc = null;
		
		if(ObjectString == 'Contact'){
	    	namestr = 'Contact';
	    }
	    if(ObjectString == 'Placement'){
	    	namestr = 'Vacancy';
	    }
	    if(ObjectString == 'Account'){
	    	namestr = 'Client';
	    }    
	    
		if(docfile == ''|| docfile == null){
			docfile = 'Result_' + namestr.trim() + '_' + Userinfo.getUserId();	
		}	
		
		try{
			
			dao.checkRead('Id,Name,DeveloperName, Body ');
			tempdoc = [select Id,Name,DeveloperName, Body 
						from Document where Type ='xml' and DeveloperName like: docfile + '%'
						and FolderId = : folderid];
					
		}catch(Exception e){
			if(tempdoc == null ){	
				Document newFile = new Document();
				newFile.DeveloperName= docfile + '_' + Datetime.now().getTime();
				newFile.Body=Blob.valueOf(bodystr);
				newFile.FolderId=folderid ;
				newFile.Name = docfile;
				newFile.Type='xml';
				checkFLS();
				insert newFile;
				tempdoc = newFile;	
					
			}
		}
		//system.debug('tempdoc ='+ tempdoc);
		if(ObjectString == 'Contact'){
			tempUser.PeopleSearchFile__c = docfile;
		}
		if(ObjectString == 'Placement'){
			tempUser.VacancySearchFile__c = docfile;
		}
		if(ObjectString == 'Account'){
			tempUser.ClientSearchFile__c = docfile;
		}
		//check FLS
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			User.PeopleSearchFile__c,
			User.VacancySearchFile__c,
			User.ClientSearchFile__c
		};
		fflib_SecurityUtils.checkUpdate(User.SObjectType, fieldList);  
		update tempUser;   		
		return tempdoc;		
	}
	
	//Create doc under personal folder
	public static Document createdocunderpersonalfolder(String bodyString, String tempName, String tempdevelopername){
		Document tempdoc = null;
		String folderId = Userinfo.getUserId();
		tempName = tempName + '_'+folderId;
		tempdevelopername = tempdevelopername+ '_'+ folderId;
		try{
			dao.checkRead('Id,Name,DeveloperName,type, Body ');
			tempdoc = [select Id,Name,DeveloperName,type, Body 
							from Document where DeveloperName like: tempdevelopername+'%'  and FolderId = : folderId];
			if(tempdoc != null){
				tempdoc.body = blob.valueOf(bodyString);
				checkFLS();
				update tempdoc;
			}else{
				tempdoc.Name = tempName;
				tempdoc.DeveloperName = tempdevelopername;
				tempdoc.Body = blob.valueof(bodyString);
				tempdoc.FolderId = folderId;
				checkFLS();
				insert tempdoc;
			}
		}catch(Exception e){
			if(tempdoc == null){
				tempdoc = new Document();
				tempdoc.Name = tempName;
				tempdoc.DeveloperName = tempdevelopername;
				tempdoc.Body = blob.valueof(bodyString);
				tempdoc.FolderId = folderId;
				checkFLS();
				insert tempdoc;
			}else{
				tempdoc = new Document();
			}
		}				
		
		return tempdoc;
	}
	
	//Create doc under personal folder
	public static Document createdocunderCTCADMINfolder(String bodyString, String tempName, String tempdevelopername){
		Document tempdoc = null;
		Folder ctcFolder = DaoFolder.getFolderByDevName('CTC_Admin_Folder');
		String folderId = ctcFolder.id;
		tempName = tempName + '_'+folderId;
		tempdevelopername = tempdevelopername+ '_'+ folderId;
		try{
			
			dao.checkRead('Id,Name,DeveloperName,type, Body ');
			tempdoc = [select Id,Name,DeveloperName,type, Body 
							from Document where DeveloperName like: tempdevelopername+'%'  and FolderId = : folderId];
			if(tempdoc != null){
				tempdoc.body = blob.valueOf(bodyString);
				checkFLS();
				update tempdoc;
			}else{
				tempdoc.Name = tempName;
				tempdoc.DeveloperName = tempdevelopername;
				tempdoc.Body = blob.valueof(bodyString);
				tempdoc.FolderId = folderId;
				checkFLS();
				insert tempdoc;
			}
		}catch(Exception e){
			if(tempdoc == null){
				tempdoc = new Document();
				tempdoc.Name = tempName;
				if(Test.isRunningTest()) {
				    tempdoc.DeveloperName = tempdevelopername+'_Test_Doc';
				}else {
				    tempdoc.DeveloperName = tempdevelopername;
				}
				
				tempdoc.Body = blob.valueof(bodyString);
				tempdoc.FolderId = folderId;
				checkFLS();
				insert tempdoc;
			}else{
				tempdoc = new Document();
			}
		}				
		
		return tempdoc;
	}
	
	//Fetch the document according the folderid and folder name
	public static Document fetchdocuments(String folderId , String folderName){
		Document result = new Document();
		try{
			dao.checkRead('Id,Name,DeveloperName,type, body');
			result = [select Id,Name,DeveloperName,type, body 
							from Document where DeveloperName like: folderName+'%'  and FolderId = : folderId ];
			//system.debug('real name:'+realName);
		}
		catch(Exception e){
			result = null;
		}	
		return result;
	}

	private static void checkFLS(){
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			Document.Name,
			Document.DeveloperName,
			Document.Body,
			Document.FolderId,
			Document.Type
		};
		fflib_SecurityUtils.checkInsert(Document.SObjectType, fieldList);
		fflib_SecurityUtils.checkUpdate(Document.SObjectType, fieldList);
	}

}