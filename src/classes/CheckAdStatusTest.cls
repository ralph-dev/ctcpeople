@isTest
public class CheckAdStatusTest {
    public static testMethod void testCheckAdStatus(){
    	System.runAs(DummyRecordCreator.platformUser) {
    		Advertisement__c adv = DataTestFactory.createAdvertisement();
	        Integer i = CheckAdStatus.CheckPostingAdStatus(adv);
	        system.assertEquals(i, 155);
	        adv.Status__c = 'Deleted';
	        i = CheckAdStatus.CheckPostingAdStatus(adv);
	        system.assertEquals(i, 154);
	        adv.Status__c = 'Active';
	        i = CheckAdStatus.CheckPostingAdStatus(adv);
	        system.assertEquals(i, 155);
	        adv.Job_Posting_Status__c = 'In Queue';
	        update adv;
	        i = CheckAdStatus.CheckPostingAdStatus(adv);
	        system.assertEquals(i, 156);
	        adv.Job_Posting_Status__c = 'Active';
	        update adv;
	        i = CheckAdStatus.CheckPostingAdStatus(adv);
	        system.assertEquals(i, 207);
	        adv.Job_Posting_Status__c = '123';
	        i = CheckAdStatus.CheckPostingAdStatus(adv);
	        system.assertEquals(i, 155);
	        adv.Status__c = 'Ready to Post';
	        update adv;
	        i = CheckAdStatus.CheckPostingAdStatus(adv);
	        system.assertEquals(i, 207);
	        adv.Status__c = '123';
	        i = CheckAdStatus.CheckPostingAdStatus(adv);
	        system.assertEquals(i, 155);
    	}
        
    }

}