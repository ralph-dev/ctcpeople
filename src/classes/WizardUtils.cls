/**
* Description: This is a util class for wizard
* Author: Emily Jiang
* Date: 01.07.2015
**/
public with sharing class WizardUtils {
	/**
	* convert start date, end date, recur end date to datetime
	* the datetime class can use format('EEEE') to get weekday and format('w') to get weeks in year
	* adjustTimeZone function is used to adjust timezone problem
	* 
	**/
    public static DateTime convertDateToDateTime(Date inputDate){
    	Time t = Time.newInstance(0,0,0,0);
    	DateTime inputDT = DateTime.newInstance(inputDate,t);
		inputDT = adjustTimeZone(inputDT);
		return inputDT;
    }
    
    /**
	* Method to adjust timezone problem
	*
	**/
	public static DateTime adjustTimeZone(DateTime inputDT){
		TimeZone tz = UserInfo.getTimeZone();
		DateTime outputDT = inputDT.AddSeconds(tz.getOffset(inputDT)/1000);
		return outputDT;
	}
	
	 /**
	* convert recurWeekdays to recurWeekdays map
	* 
	**/
    public static Map<String,String> convertStringToMap(String inputString){
		List<String> inputStringList = inputString.split(';');
		Map<String,String> outputMap = convertListToMap(inputStringList);
		return outputMap;
	}
	
	/**
	* convert list to map
	*
	**/
	public static Map<String,String> convertListToMap(List<String> inputList){
		Map<String,String> outputMap = new Map<String,String>();
		for(String s : inputList){
			outputMap.put(s,s);
		} 
		return outputMap;
	}
	
	/**
	* next day time
	*
	**/
	public static DateTime nextDT(DateTime inputDT){	
		return inputDT.addDays(1);
	}
	
	/**
	* get dates for a date range
	*
	**/
	public static List<String> getDateStrListForDateRange(Date startDate, Date endDate){
		
		DateTime startDateTime = convertDateToDateTime(startDate);
		DateTime endDateTime = convertDateToDateTime(endDate);
		List<String> dateStrList = new List<String>();
	
		for(DateTime i = startDateTime; i < nextDT(endDateTime); i = nextDT(i)){
			dateStrList.add(String.valueOf(i.date()));
		}
		
		return dateStrList;
	}
	
	/**
	* 	getDifference between two date
	*
	**/
	public static Integer getDiffBetweenTwoDates(Date startDate, Date endDate){
		Integer diff = startDate.daysBetween(endDate);
		return diff;
	}
	
	/**
	*	get end date of a week based on start date
	*   in SF, the start date of the week is Sunday
	*   the end of the week should be 7 days later.
	**/
	public static Date getEndDateByStartDateAndWeekdays(Date startDate){
		Date startDateOfWeek = startDate.toStartOfWeek();
		Date endDateOfWeek = startDateOfWeek.addDays(7);
		return endDateOfWeek;
	}
	

}