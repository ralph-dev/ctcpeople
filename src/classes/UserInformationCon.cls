public class UserInformationCon{
	//*** current login user information
	public String UserId = UserInfo.getUserId();
	public String OrgId = UserInfo.getOrganizationId(); 
	public Integer usage{get;set;}
	public Integer quota{get;set;}
	
	public User getUserDetail(){
		CommonSelector.checkRead(User.SObjectType,'Id , ProfileId, Email,Phone,Country,WebJobFooter__c, View_CTC_Admin__c,'
				+'VacancySearchFile__c,ClientSearchFile__c,CareerOne_Usage__c,PeopleSearchFile__c,Seek_Usage__c,MyCareer_Usage__c,'
				+'Monthly_Quota_Seek__c,Monthly_Quota_MyCareer__c, Monthly_Quota_CareerOne__c,Manage_TimeSheet__c, Manage_Quota__c, ' +
				'JobBoards__c, Job_Posting_Email__c,Company_Description__c,AmazonS3_Folder__c,ContactId, AccountId, Job_Posting_Company_Name__c, Seek_Account__c, LinkedIn_Account__c');
		return  [select Id , ProfileId, Email,Phone,Country,
					WebJobFooter__c, View_CTC_Admin__c, VacancySearchFile__c, 
					ClientSearchFile__c,CareerOne_Usage__c,PeopleSearchFile__c,Seek_Usage__c, 
					MyCareer_Usage__c, Monthly_Quota_Seek__c, 
					Monthly_Quota_MyCareer__c, Monthly_Quota_CareerOne__c, 
					Manage_TimeSheet__c, Manage_Quota__c, JobBoards__c, Job_Posting_Email__c,Company_Description__c,
					AmazonS3_Folder__c,ContactId, AccountId, Job_Posting_Company_Name__c, Seek_Account__c, LinkedIn_Account__c 
					from User where Id=:UserId];
	  
	} // user id, profileId, etc.
	
	public static User getUserDetails{
		get{
			CommonSelector.checkRead(User.SObjectType,'Id , ProfileId, Email,Phone,Country,'
					+'WebJobFooter__c, View_CTC_Admin__c, VacancySearchFile__c,'
					+'ClientSearchFile__c,CareerOne_Usage__c,PeopleSearchFile__c,Seek_Usage__c,'
					+'MyCareer_Usage__c, Monthly_Quota_Seek__c,'
					+'Monthly_Quota_MyCareer__c, Monthly_Quota_CareerOne__c,'
					+'Manage_TimeSheet__c, Manage_Quota__c, JobBoards__c, Job_Posting_Email__c,Company_Description__c,'
					+'AmazonS3_Folder__c,ContactId, AccountId, Job_Posting_Company_Name__c, Seek_Account__c, LinkedIn_Account__c, Indeed_Account__c');
			return  [select Id , ProfileId, Email,Phone,Country, 
					WebJobFooter__c, View_CTC_Admin__c, VacancySearchFile__c, 
					ClientSearchFile__c,CareerOne_Usage__c,PeopleSearchFile__c,Seek_Usage__c, 
					MyCareer_Usage__c, Monthly_Quota_Seek__c, 
					Monthly_Quota_MyCareer__c, Monthly_Quota_CareerOne__c, 
					Manage_TimeSheet__c, Manage_Quota__c, JobBoards__c, Job_Posting_Email__c,Company_Description__c,
					AmazonS3_Folder__c,ContactId, AccountId, Job_Posting_Company_Name__c, Seek_Account__c, LinkedIn_Account__c, Indeed_Account__c
					from User where Id=:Userinfo.getUserId()];
		}
	}
	
	public static String CompanyName(){
		String CompanyName = '';
		if(getUserDetails.Job_Posting_Company_Name__c != ''&& getUserDetails.Job_Posting_Company_Name__c!= null&& getUserDetails.Job_Posting_Company_Name__c!='PeopleCloud Default'){
			CompanyName = getUserDetails.Job_Posting_Company_Name__c;
		}else{
		   	CompanyName = UserInfo.getOrganizationName();
		}
		return CompanyName;
	}
	
	public Integer checkquota(Integer n){
       //** for calculate the remainingQuota
       User u = getUserDetail();
       Integer remainingQuota;
       if( n == 1 || n == 2 || n == 3){
	       if(n == 1) //** seek
	       {
	       	   usage = (u.Seek_Usage__c == null || u.Seek_Usage__c == '' )? 0 : Integer.valueOf(u.Seek_Usage__c);
		       quota = (u.Monthly_Quota_Seek__c == null || u.Monthly_Quota_Seek__c == '' )? 0 : Integer.valueOf(u.Monthly_Quota_Seek__c);
	       	system.debug('the usage =' + usage);
	       }
	       else if(n == 2) //** mycareer
	       {
	       	   usage = (u.MyCareer_Usage__c == null || u.MyCareer_Usage__c == '' )? 0 : Integer.valueOf(u.MyCareer_Usage__c);
		       quota = (u.Monthly_Quota_MyCareer__c == null || u.Monthly_Quota_MyCareer__c == '' )? 0 : Integer.valueOf(u.Monthly_Quota_MyCareer__c);
		       
	       }
	       else if(n == 3) //** careerone
	       {
	       	   usage = (u.CareerOne_Usage__c == null || u.CareerOne_Usage__c == '' )? 0 : Integer.valueOf(u.CareerOne_Usage__c);
		       quota = (u.Monthly_Quota_CareerOne__c == null || u.Monthly_Quota_CareerOne__c == '' )? 0 : Integer.valueOf(u.Monthly_Quota_CareerOne__c);
		       
	       }
	       remainingQuota = quota - usage;
	       return remainingQuota; 
	       
       }
       return 0;
    }

	/**
	 * This method is to check quota for given job board account
	 * This method is used to replace the previous version, will need to depreciate the previous one
	 * after all the code changed
	 * @param jobBoard      job board account type
	 * @param user          current user
	 * @return Integer      remaining quota
	 */
	public static Integer checkRemainingQuota(String jobBoardType, User u){ 
		//** for calculate the remainingQuota
		Integer remainingQuota = 0;
		Integer adQuota = 0;
		Integer adUsage = 0;
		if (jobBoardType.equalsIgnoreCase('Seek')) {
			adUsage = (u.Seek_Usage__c == null || u.Seek_Usage__c == '' )? 0 : Integer.valueOf(u.Seek_Usage__c);
			adQuota = (u.Monthly_Quota_Seek__c == null || u.Monthly_Quota_Seek__c == '' )? 0 : Integer.valueOf(u.Monthly_Quota_Seek__c);
		} else if (jobBoardType.equalsIgnoreCase('MyCareer')) {
			adUsage = (u.MyCareer_Usage__c == null || u.MyCareer_Usage__c == '' )? 0 : Integer.valueOf(u.MyCareer_Usage__c);
			adQuota = (u.Monthly_Quota_MyCareer__c == null || u.Monthly_Quota_MyCareer__c == '' )? 0 : Integer.valueOf(u.Monthly_Quota_MyCareer__c);
		} else if (jobBoardType.equalsIgnoreCase('CareerOne')) {
			adUsage = (u.CareerOne_Usage__c == null || u.CareerOne_Usage__c == '' )? 0 : Integer.valueOf(u.CareerOne_Usage__c);
			adQuota = (u.Monthly_Quota_CareerOne__c == null || u.Monthly_Quota_CareerOne__c == '' )? 0 : Integer.valueOf(u.Monthly_Quota_CareerOne__c);
		}
		remainingQuota = adQuota - adUsage;
		return remainingQuota;
	}
}