/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

public with sharing class NewTimeSheetCon{
    //** workforce management
    //*** NewTimeSheet visulforce page
    //** TimeSheetClass involved
 //   public boolean has_error {get;set;}
 //   public Placement__c thevacancy {get;set;}
	//public String TS_StartTime {get;set;}
 //   public String TS_EndTime {get;set;}
 //   public String selectedroster {get;set;}
 //   public List<SelectOption> rosteroptions {get;set;}

 //   public Contact thecandidate {get;set;}
 //   public Integer theIndex;
 //   public List<TimeSheetClass.TimeSheet> timesheets{get;set;}
 //   public Timesheet_Entry__c tempTS{get;set;}
 //   public String theseq{get;set;}
 //   public String CompanyName {get;set;}
 //   public String CandidateName {get;set;}
 //   public String theType{get;set;}
 //   public String selectedRTId{get;set;}
 //   public List<SelectOption> roserttypeoptions {get;set;}
 //   public String selectedTS {get;set;} //id
 //   public List<SelectOption> timesheetoptions {get;set;}
 //   public TimeSheet__c[] existing_timesheet;
 //   public TimeSheet__c thetimesheet{get;set;}
 //   public Map<Id,TimeSheet__c> map_timesheet = new Map<Id,TimeSheet__c>();
 //   public boolean showNew{get;set;}
 //   public String whereami{get;set;}
 //   public String err{get;set;}
 //   public boolean showEntry{get;set;}
 //   public String peopletype{get;set;}
 //   public List<SelectOption> approvers {get;set;}
 //   public String selectedapprover {get;set;}
 //   public List<SelectOption> approvers2 {get;set;}
 //   public String selectedapprover2 {get;set;}
	//public List<SelectOption> candidates {get;set;}
	//public String selectedCandidate{get;set;}
	//public String msg{get;set;}
	//public String candidateId;
 //   public NewTimeSheetCon(ApexPages.StandardController stdController){
 //       timesheets = new List<TimeSheetClass.TimeSheet>();
 //       thetimesheet = new TimeSheet__c();
 //       showEntry = false;
 //       theIndex = 1;
 //       showNew = true;
 //       tempTS = new Timesheet_Entry__c(); 
 //       has_error = false;
 //       approvers = new List<SelectOption>();
 //       approvers2 = new List<SelectOption>();
 //       candidates = new List<SelectOption>();
 //       approvers.add(new SelectOption('','--Available Approvers--'));
 //       approvers2.add(new SelectOption('','--Available Approvers--'));
 //       candidates.add(new SelectOption('','--Available Candidates--'));
        
 //       try{
             
 //           String cid = ApexPages.currentPage().getParameters().get('cid'); //** button from people
 //           if(cid != null && cid != '')
 //           {
 //               whereami = 'candidate';
 //               initCandidate(cid);
 //               candidateId= cid;
 //           }
 //           else
 //           {
 //               String vid = ApexPages.currentPage().getParameters().get('vid'); //** button from vacancy
 //               if(vid != null && vid != '')
 //               {
 //                   whereami = 'vacancy';
 //                   initVacancy(vid);
 //               }
 //               else
 //               {
 //                   whereami = 'timesheet';
 //               }
 //           }
            
 //           initTimesheetEntries();
 //       }
 //       catch(system.Exception e){
 //           has_error = true;
 //       }
 //   } 
 //   public void switchVacancy(){
 //       //** whereami = candidate or timesheet. 
 
 //       if(thetimesheet != null)
 //       {
 //           if(thetimesheet.Vacancy__c != null)
 //           {
 //               initVacancy(thetimesheet.Vacancy__c);
 //           }
 //       }
        
        
 //   }   
 //   public void initCandidate(String cid){
 //       try{
            
            
 //           if(cid != null)
 //           {
 //           	CommonSelector.checkRead(Contact.sObjectType ,'FirstName,LastName,RecordType.Name,Id');
 //               Contact thecandidate = [select FirstName,LastName,RecordType.Name, Id from contact where id=:cid];
 //               if(thecandidate == null)
 //               {
 //                   has_error = true;
 //               }
 //               else
 //               {
 //                   tempTS.Candidate__c = thecandidate.Id;
 //                   CandidateName = thecandidate.FirstName +' '+ thecandidate.LastName ;
 //                   peopletype = thecandidate.RecordType.Name;
 //                   selectedCandidate = thecandidate.Id;
 //               }
 //           }
 //       }
 //       catch(system.Exception e)
 //       {
 //           has_error = true;
 //       }
    	
 //   }
 //   public void initVacancy(String vid){
 //       try{
            
            
 //           if(vid != null)
 //           {

	//			CommonSelector.checkRead(Placement__c.sObjectType, 'Id, Company__r.Name,Company__c, Name, RecordTypeId, RecordType.Name, Backup_Approver__c,TimeSheet_Approver__c');
	//			CommonSelector.checkRead(TimeSheet__c.sObjectType, 'Id, Name, Start_Date__c, End_Date__c, Start_Time__c, End_Time__c,  TimeSheet_Status__c');
 //               thevacancy =[select Id, Company__r.Name,Company__c, Name, 
 //                           RecordTypeId, RecordType.Name, Backup_Approver__c,TimeSheet_Approver__c,
 //                           (select Id, Name, Start_Date__c, End_Date__c, Start_Time__c, End_Time__c, 
 //                           TimeSheet_Status__c from TimeSheets__r)  
 //                           from Placement__c  where Id =: vid];
                
                

 //               if(thevacancy != null)
 //               {
 //                   theType = thevacancy.RecordType.Name;
 //                   CompanyName = thevacancy.Company__r.Name;

 //                   existing_timesheet = thevacancy.TimeSheets__r;
                    
 //                   tempTS.Vacancy__c = thevacancy.Id;
 //                   //initApprovers(thevacancy.Company__c);
 //                   List<SelectOption> temps = TimeSheetClass.initApprovers(thevacancy.Company__c, thevacancy);
 //                   approvers.addAll(temps);
	//				approvers2.addAll(temps);
	//				List<SelectOption> temp2 = TimeSheetClass.populateCandidate(thevacancy);
	//				candidates.addAll(temp2);

 //               }
              

 //           }

            
 //       }
 //       catch(system.Exception e)
 //       {
 //           // error
 //           has_error = true;
 //       }
    
 //   }
    
 //   public PageReference createTimesheet(){
 //       try{
 //           err ='';
 //           if(whereami == 'candidate' || whereami == 'timesheet')
 //           {
 //               switchVacancy(); // ** to get thevacancy
 //           }

            
 //           if(thevacancy == null)
 //           {   err = 'Please make sure vacancy information is not empty.';
 //               return null;
 //           }
            
 //           if(whereami != 'candidate')
 //           {
	//            if(selectedCandidate == null)
	//            {
	//                err = 'Please make sure candidate information is not empty.';
	//                return null;
	//            }
 //           }
 //           else
 //           {
 //           	selectedCandidate = candidateId;
 //           }
            
 //           if(whereami == 'vacancy' || whereami == 'timesheet')
 //           {
 //               initCandidate(tempTS.Candidate__c); //** to populate people information.
 //           }

 //           thetimesheet.Vacancy__c = thevacancy.Id ;

 //           thetimesheet.Candidate__c = selectedCandidate;//tempTS.Candidate__c ;
 //           thetimesheet.Approver_Contact__c = selectedapprover ;
 //           thetimesheet.Backup_Approver__c = selectedapprover2 ;
 //           thetimesheet.Start_Time__c = TimeSheetClass.TimeStrtoDecimal(TS_StartTime);
 //           thetimesheet.End_Time__c = TimeSheetClass.TimeStrtoDecimal(TS_EndTime);
 //           Boolean f = TimeSheetClass.validateTimeSheet(thetimesheet);
        	
 //       	if(!f || f == null)
 //       	{
 //       		err = TimeSheetClass.TimeSheetClassMsg;
 //       		return null;
 //       	}
 //           //check FLS
 //           List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
 //               TimeSheet__c.Vacancy__c,
 //               TimeSheet__c.Candidate__c,
 //               TimeSheet__c.Approver_Contact__c,
 //               TimeSheet__c.Backup_Approver__c,
 //               TimeSheet__c.Start_Time__c,
 //               TimeSheet__c.End_Time__c
 //           };
 //           fflib_SecurityUtils.checkInsert(TimeSheet__c.SObjectType, fieldList);
 //           fflib_SecurityUtils.checkUpdate(TimeSheet__c.SObjectType, fieldList);
 //           upsert thetimesheet;

 //           showEntry = true;
 //       }
 //       catch(system.exception e){
 //           showEntry = false;
 //           err = 'System error. Please contact your admin.';
 //       }
 //       if(thetimesheet != null)
 //       {
 //           if(thetimesheet.Id != null)
 //           {
               
 //               PageReference p = new ApexPages.StandardController(thetimesheet).view();

 //               p.setRedirect(true);
 //               return p;
 //           }
 //       }
 //       return null;
 //   }

 //   public void initTimesheetEntries(){
 //       try
 //       {
 //           Integer initcount = 1; // init num, how many rows will be created at the beginning.
 //           for(integer i=0 ; i<initcount; i++)
 //           {
 //              Timesheet_Entry__c te = new Timesheet_Entry__c();
 //              timesheets = TimeSheetClass.pushNewEntry(te, i+1,timesheets);
 //           }
 //       }
 //       catch(system.exception e)
 //       {}
    
 //   }

 //   public void nonaction(){
 //        //** non-action just for refresh page
    
 //   }
 //   public void createNewEntry(){
 //       msg ='';
 //       err ='';
 //       theIndex = theIndex +1;
 //       Timesheet_Entry__c te = new Timesheet_Entry__c();
 //       timesheets = TimeSheetClass.pushNewEntry(te,theIndex,timesheets);

 //   }
 //   public void newTimeSheet(){
 //        //** non-action just for refresh page
        
 //   }
 //   //****************************************
   

    
   
   
    
}