/**
 * This class is the DTO of JXT ad
 * 
 * Author: Lorena
 * Date: 7/12/2016
 * */
public with sharing class JXTJobContent {
    
    public String referenceNo;
    public String jobAdType;
    public String jobTitle;
    public String jobUrl;
    public String shortDescription;
    public String jobFullDescription;
    public String contactDetails;
    public String companyName;
    public String consultantID;
    public String publicTransport;
    public Boolean residentsOnly;
    public Boolean isQualificationsRecognised;
    public Boolean showLocationDetails;
    public String jobTemplateID;
    public String advertiserJobTemplateLogoID;
    
    public Bulletpoints bulletpoints;
    public ArrayOfCategory categories;
    public ListingClassification listingClassification;
    public Salary salary;
    public ApplicationMethod applicationMethod;
    public ReferralFee referral;
    //We don't set expiry date, leave it as 30 days
    public String expiryDate;
    
    public JXTJobContent(){
        bulletpoints = new Bulletpoints();
        categories = new ArrayOfCategory();
        listingClassification = new ListingClassification();
        salary = new Salary();
        applicationMethod = new ApplicationMethod();
        referral = new ReferralFee();
    }
    
    
    public class Bulletpoints{
        public String bulletPoint1;
        public String bulletPoint2;
        public String bulletPoint3;
    }
    
    public class ArrayOfCategory{
        public List<Category> category = new List<Category>();
    }
    
    public class Category{
        public String classification;
        public String subClassification;
    }
    
    public class ListingClassification{
        public String workType;
        public String sector;
        public String streetAddress;
        public String tags;
        public String country;
        public String location;
        public String area;
    }
    
    public class Salary{
        public String salaryType;
        public String min;
        public String max;
        public String additionalText;
        public Boolean showSalaryDetails;
    }
    
    public class ApplicationMethod{
        public String jobApplicationType;
        public String applicationUrl;
        public String applicationEmail;
    }
    
    public class ReferralFee{
        public Boolean hasReferralFee;
        public String amount;
        public String referralUrl;
    }

}