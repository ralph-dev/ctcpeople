/**
 * This is the test class for JobBoardFactory class
 * It also include test for JobPostingExtensionSeek
 *
 * Created by: Lina Wei
 * Created on: 30/02/2017
 */

@isTest
private class JobBoardFactoryTest {

    static testMethod void testGetJobBoardWithAd() {
    	System.runAs(DummyRecordCreator.platformUser) {
        Advertisement__c ad = new Advertisement__c();
        System.assertEquals(null, JobBoardFactory.getJobBoard(ad));
        ad = AdvertisementDummyRecordCreator.generateSeekAdDummyRecord();
        System.assertEquals('Seek', JobBoardFactory.getJobBoard(ad).jobBoardType);
    	}
    }

    static testMethod void testGetJobBoardWithRecordTypeId() {
    	System.runAs(DummyRecordCreator.platformUser) {
        System.assertEquals(null, JobBoardFactory.getJobBoard(''));
        System.assertEquals('Seek', JobBoardFactory.getJobBoard(DaoRecordType.seekAdRT.Id).jobBoardType);
    	}
    }

}