public with sharing class CreatEventOnInterviewTrigger {

 	public static void CreateInsertEventOnInterviewTrigger(List<Placement_Candidate__c> ids){
 		PlacCandUpdEvOnInterviewTriggerHelper helper = new PlacCandUpdEvOnInterviewTriggerHelper();		
		// Step 2 : Obtaining the data from placement candidate for the creation of the event
		
		CommonSelector.checkRead(Placement_Candidate__c.sObjectType,
			'id,  Date_and_Time_Internal__c, Internal_Interview_Consultant__c,Internal_Interview_Details__c,Internal_Interview_Event_Id__c,Candidate__r.Name   ');
	    List<Placement_Candidate__c> placementCandidates =
	        [SELECT 
	            id, 
	            Date_and_Time_Internal__c,
	            Internal_Interview_Consultant__c,
	            Internal_Interview_Details__c,
	            Internal_Interview_Event_Id__c,
	            Candidate__r.Name
	           FROM Placement_Candidate__c    
	          WHERE id IN :ids];
	          
	    // Step 3 : Obtaining the list of events to create and update their placement candidates
	    List<Event> eventsForInterview = new List<Event>();
	    
	    Map<Id, Event>  mapPlacementCandidates_for_Events_InternalInterview = new Map<Id, Event>();
	    
	    for (Placement_Candidate__c pc : placementCandidates) { 
	        // Internal Inverview event             
	        if(pc.Date_and_Time_Internal__c != null && pc.Internal_Interview_Consultant__c != null) {
	            Event ev1 = helper.createInternalInterviewEvent         (pc); 
	            eventsForInterview.add                                  (ev1);                      
	            mapPlacementCandidates_for_Events_InternalInterview.put (pc.id, ev1);           
	        }
	    }   
	    
	    // Step 4 : Commiting changes to the database. Events
	    //check FLS
	    List<String> eventFieldList = new List<String>{
			'WHATID'
            ,'SUBJECT'
            ,'ISALLDAYEVENT'
            ,'ACTIVITYDATE'
            ,'DURATIONINMINUTES'
            ,'STARTDATETIME'
            ,'ENDDATETIME'  
            ,'DESCRIPTION'            
            ,'ISPRIVATE'      
            ,'SHOWAS'        
	    };
	    fflib_SecurityUtils.checkInsert(Event.SObjectType, eventFieldList);
	    insert eventsForInterview;
	
	    // Step 5 : Updating PlacementCandidates with the events
	    for (Placement_Candidate__c pc : placementCandidates) {                 
	        // Internal Inverview event             
	        if(pc.Date_and_Time_Internal__c != null && pc.Internal_Interview_Consultant__c != null) {
	            pc.Internal_Interview_Event_Id__c = mapPlacementCandidates_for_Events_InternalInterview.get(pc.id).id;
	        }
	    }
	
	    // Step 6 : Commiting changes to the database. PlacementCandidates
	    TriggerHelper.PlacementCandidateEventOnInterviewTrigger = false;
	    //check FLS
	    fflib_SecurityUtils.checkFieldIsUpdateable(Placement_Candidate__c.SObjectType, Placement_Candidate__c.Internal_Interview_Event_Id__c);
	    update placementCandidates;
	}
	
	public static void CreateUpdateEventOnInterviewTrigger(List<Placement_Candidate__c> ids){
		PlacCandUpdEvOnInterviewTriggerHelper helper = new PlacCandUpdEvOnInterviewTriggerHelper();
		// Step 2A : Obtaining the data from placement candidate for updating the event
		
		CommonSelector.checkRead(Placement_Candidate__c.sObjectType,
			'id,  Date_and_Time_Internal__c, Internal_Interview_Consultant__c,Internal_Interview_Details__c,Internal_Interview_Event_Id__c,Candidate__r.Name   ');
	    List<Placement_Candidate__c> placementCandidates =
	        [SELECT 
	            id, 
	            Date_and_Time_Internal__c,
	            Internal_Interview_Consultant__c,
	            Internal_Interview_Details__c,
	            Internal_Interview_Event_Id__c,
	            Candidate__r.Name           
	           FROM Placement_Candidate__c    
	          WHERE id IN :ids];    
	          
	    // Step 2B : Obtaining the Events that are related to the placement candidates
	    
	    CommonSelector.checkRead(Event.sObjectType,'id, WhatId, DESCRIPTION, SUBJECT  ');
	    List<Event> eventsFromPlacementCandidate =
	        [SELECT id, WhatId, DESCRIPTION, SUBJECT  
	           FROM Event  
	          WHERE WHATID IN :ids];      
	          
	    //%%%%%%%%%%  PART B : OBTAINING THE INFORMATION ABOUT THE EVENTS THAT NEED TO BE CREATED AND UPDATED     
	          
	    // Step 3 : Creating objects for obtaining the list of events to CREATE and update their placement candidates     
	    List<Event>         eventsForInterview = new List<Event>(); 
	    Map<Id, Event>      mapPlacementCandidates_for_Events_InternalInterview = new Map<Id, Event>();
	    
	    // Step 4 : Creating a map for matching each event TO BE UPDATED with its placement candidate information
	    Map<Id, Datetime>   mapEventsForInterview_Date_InternalInterview            = new Map<Id, Datetime>();
	    Map<Id, String>     mapEventsForInterview_CandidateName_InternalInterview   = new Map<Id, String>();
	    Map<Id, String>     mapEventsForInterview_Details                           = new Map<Id, String>();
	    
	    // Step 5 : Obtaining the list of Events that need to be CREATED OR UPDATED
	    Id       idEvent;
	    Event    ev1_toBeModified;      
	    Boolean  needsToBeupdatedPlacementCandidates = false;   
	    for (Placement_Candidate__c pc : placementCandidates) {
	        if(pc.Date_and_Time_Internal__c != null && pc.Internal_Interview_Consultant__c != null) {
	            idEvent = pc.Internal_Interview_Event_Id__c;
	            if(idEvent == null) {  
	                System.debug('trigger.PlacementCandUpdateEventOnInterviewTrigger : placementCandidates has candidates, pc.Internal_Interview_Event_Id__c is null ');
	                ev1_toBeModified = helper.get_InternalInterviewEvent_IfExistent(pc, eventsFromPlacementCandidate);
	                if(ev1_toBeModified != null) {
	                    System.debug('trigger.PlacementCandUpdateEventOnInterviewTrigger : placementCandidates has candidates, pc.Internal_Interview_Event_Id__c is null, ev1_toBeModified != null ');
	                    idEvent = ev1_toBeModified.Id;
	                    pc.Internal_Interview_Event_Id__c = idEvent;
	                    mapEventsForInterview_Date_InternalInterview.put            (idEvent, pc.Date_and_Time_Internal__c);
	                    mapEventsForInterview_Details.put                           (idEvent, pc.Internal_Interview_Details__c);
	                    mapEventsForInterview_CandidateName_InternalInterview.put   (idEvent, pc.Candidate__r.Name);    
	                    needsToBeupdatedPlacementCandidates = true;             
	                } else {    
	                    ev1_toBeModified = helper.createInternalInterviewEvent      (pc);               
	                    eventsForInterview.add                                      (ev1_toBeModified);                     
	                    mapPlacementCandidates_for_Events_InternalInterview.put     (pc.id, ev1_toBeModified);
	                }
	            } else {            
	                mapEventsForInterview_Date_InternalInterview.put                (idEvent, pc.Date_and_Time_Internal__c);
	                mapEventsForInterview_Details.put                               (idEvent, pc.Internal_Interview_Details__c);
	                mapEventsForInterview_CandidateName_InternalInterview.put       (idEvent, pc.Candidate__r.Name);
	            }
	        }
	    }
	     //check FLS
	   List<String> eventFieldList = new List<String>{
			'WHATID'
            ,'SUBJECT'
            ,'ISALLDAYEVENT'
            ,'ACTIVITYDATE'
            ,'DURATIONINMINUTES'
            ,'STARTDATETIME'
            ,'ENDDATETIME'  
            ,'DESCRIPTION'            
            ,'ISPRIVATE'      
            ,'SHOWAS'        
	    };
	    fflib_SecurityUtils.checkInsert(Event.SObjectType, eventFieldList);
	    insert eventsForInterview;
	    
	    for (Placement_Candidate__c pc : placementCandidates) {                 
	        // Internal Inverview event             
	        if(pc.Date_and_Time_Internal__c != null && pc.Internal_Interview_Consultant__c != null) {
	            Event ev3 = mapPlacementCandidates_for_Events_InternalInterview.get(pc.id);
	            if(ev3 != null) {
	                pc.Internal_Interview_Event_Id__c = ev3.id;
	                needsToBeupdatedPlacementCandidates = true;
	            }
	        }
	    }
	     
	    //%%%%%%%%%%  PART C : OBTAINING AND UPDATING THE INTERNAL INTERVIEW EVENTS 
	     
	    // Step 6 : Obtain the list of the events to Update
	    CommonSelector.checkRead(Event.sObjectType,'id ');
	    List<Event> eventsForUpdate_InternalInterview =
	    [SELECT id  
	       FROM Event  
	      WHERE id IN :mapEventsForInterview_Date_InternalInterview.keySet()];
	      
	    //delete eventsForUpdate;
	    
	    for (Event eventObject : eventsForUpdate_InternalInterview) {       
	        String      value_Internal_Interview_Details    =   mapEventsForInterview_Details.get(eventObject.id);
	        //System.debug('trigger.PlacementCandUpdateEventOnInterviewTrigger : eventsForUpdate has event, value_Internal_Interview_Details = ' + value_Internal_Interview_Details);
	        Datetime value_Internal_Interview_Date          =   mapEventsForInterview_Date_InternalInterview.get(eventObject.id); //placementCandidateObject.Internal_Interview_Date__c;
	        //System.debug('trigger.PlacementCandUpdateEventOnInterviewTrigger : eventsForUpdate has event, value_Internal_Interview_Date = ' + value_Internal_Interview_Date);
	        //-----------------
	        Datetime i_ACTIVITYDATETIME     =   value_Internal_Interview_Date; //datetime.newInstance(2011, 12, 20, 23, 00, 0);  
	        Datetime i_STARTDATETIME        =   value_Internal_Interview_Date; //datetime.newInstance(2011, 12, 20, 23, 00, 0);  
	        
	        Datetime i_ENDDATETIME          =   value_Internal_Interview_Date.addHours(1); //datetime.newInstance(2011, 12, 21, 00, 00, 0); 
	        
	        Datetime i_REMINDERDATETIME     =   value_Internal_Interview_Date.addMinutes(-30); //datetime.newInstance(2011, 12, 20, 22, 45, 0); 
	        
	        Date i_ACTIVITYDATE             =   value_Internal_Interview_Date.date(); //date.newinstance(2011, 12, 21); 
	                
	        eventObject.DESCRIPTION         =   value_Internal_Interview_Details;   //'Internal Interview with Candidate'
	        //---------------
	        
	        eventObject.SUBJECT             =   'Internal Interview with Candidate ' + mapEventsForInterview_CandidateName_InternalInterview.get(eventObject.id); //pc.Candidate__r.Name;     
	        eventObject.ISALLDAYEVENT       =   FALSE;  
	        eventObject.ACTIVITYDATETIME    =   i_ACTIVITYDATETIME; 
	        eventObject.ACTIVITYDATE        =   i_ACTIVITYDATE; 
	        eventObject.DURATIONINMINUTES   =   60; 
	        eventObject.STARTDATETIME       =   i_STARTDATETIME;
	        eventObject.ENDDATETIME         =   i_ENDDATETIME;      
	        eventObject.ISPRIVATE           =   FALSE;  
	        eventObject.SHOWAS              =   'Busy';                                     
	        //eventObject.ISRECURRENCE      =   FALSE;      
	        eventObject.REMINDERDATETIME    =   i_REMINDERDATETIME;                                     
	        eventObject.ISREMINDERSET       =   TRUE;
	    }
	    
	    // Step 7 : Commiting changes to the database
	    //check FLS
	    fflib_SecurityUtils.checkUpdate(Event.SObjectType, eventFieldList);
	    update eventsForUpdate_InternalInterview;
	    
	    if(needsToBeupdatedPlacementCandidates) {
	    	//check FLS
	    	fflib_SecurityUtils.checkFieldIsUpdateable(Placement_Candidate__c.SObjectType, Placement_Candidate__c.Internal_Interview_Event_Id__c);
	        update placementCandidates;
	    }
	    TriggerHelper.PlacementCandidateEventOnInterviewTrigger=false;
	}
}