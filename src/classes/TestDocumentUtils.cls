@isTest
public with sharing class TestDocumentUtils {
	public static testmethod void testDocumentUtilMethod(){
		System.runAs(DummyRecordCreator.platformUser) {
		
		/**
		* added by andy for setting SQS credential
		*/
		SQSCredentialSelectorTest.createDummySQSCredential();
		
		String docName = 'testdoc';
		Blob docbody = blob.valueOf('testdoc');
		String DeveloperName = 'testdoc';
		String FolderId = Userinfo.getUserId();
		String tempid = DocumentUtils.CreateDocument(docName, docbody, DeveloperName, FolderId);
		system.debug(tempid != null);
		system.assertNotEquals(tempid, null);
		}
	}
}