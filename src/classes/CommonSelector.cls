/**
 * This class is a common class which extends fflib_SObjectSelector and provides a simple methods to select records from specified object with checking access to fields.
 * 
 * such as :
 * 
 * public List<SObject> get(String selectFieldString, String conditionString)
 * example for : get('Id,Advertiser_Password__c,Advertiser_Name__c,Name', 'WebSite__c=\'CareerOne\' and Account_Active__c = true and RecordType.DeveloperName like \'%WebSite_Admin_Record%\'')
 * 
 * 
 */

public virtual class CommonSelector  extends fflib_SObjectSelector {
    
  
    public enum SortOrder{ASCENDING, DESCENDING}
   
    public static SObject getFirst(List<SObject> sobjs){
    	if(sobjs == null || sobjs.size() ==0){
    		return null;
    	}
    	
    	return sobjs[0];
    }
    
     public static List<String> convertStringToList(String fieldNamesSplittedComma){
    	if(fieldNamesSplittedComma == null ){
	        return null;
	    }
	    
	    List<String> fieldList = new List<String>();
	    
	   
	    for(String f : fieldNamesSplittedComma.split(',')){
	        fieldList.add(f.trim() );
	    }
	    
	    return fieldList;
    }
    
   
    
    public static SObject getOne(Schema.SObjectType sobjType,String selectFieldString, String conditionString){
    	
    	return getFirst(get(sobjType, selectFieldString,conditionString));
    }
    public static List<SObject> get(Schema.SObjectType sobjType,String selectFieldString, String conditionString){
        
      return  new  CommonSelector(sobjType).get(selectFieldString,conditionString);
        
    }
    
    
    public static Boolean quickDelete(SObject sobj){
    	
    	return quickDelete(new List<SObject>{sobj});
    }
    
    public static Boolean quickDelete(List<SObject> sobjs){
    	
    	if(sobjs == null || sobjs.size() == 0){
    		return false;
    	}
    	
    	return new CommonSelector(sobjs[0].getSObjectType()).doDelete(sobjs);
    	
    }
    
    public static Boolean quickInsert(SObject sobj){
    	
    	return quickInsert(new List<SObject>{sobj});
    }
    
    public static Boolean quickInsert(List<SObject> sobjs){
    	if(sobjs == null || sobjs.size() == 0){
    		return false;
    	}
    	return new CommonSelector(sobjs[0].getSObjectType()).doInsert(sobjs);
    }
    
    public static Boolean quickUpdate(SObject sobj){
    	
    	return quickUpdate(new List<SObject>{sobj});
    }
    
    public static Boolean quickUpdate(List<SObject> sobjs){
    	if(sobjs == null || sobjs.size() == 0){
    		return false;
    	}
    	return new CommonSelector(sobjs[0].getSObjectType()).doUpdate(sobjs);
    }
    
     public static Boolean quickUpdate(SObject sobj,String fieldnames){
    	
    	return quickUpdate(new List<SObject>{sobj},fieldnames);
    }
    
     public static Boolean quickUpdate(List<SObject> sobjs,String fieldnames){
    	if(sobjs == null || sobjs.size() == 0){
    		return false;
    	}
    	return new CommonSelector(sobjs[0].getSObjectType()).doUpdate(sobjs, fieldnames);
    }
    
    public static Boolean quickUpsert(SObject sobj){
    	
    	return quickUpsert(new List<SObject>{sobj});
    }
    
    public static Boolean quickUpsert(List<SObject> sobjs){
    	if(sobjs == null || sobjs.size() == 0){
    		return false;
    	}
    	return new CommonSelector(sobjs[0].getSObjectType()).doUpsert(sobjs);
    }
    
     public static Boolean quickUpsert(SObject sobj,String fieldnames){
    	
    	return quickUpsert(new List<SObject>{sobj},fieldnames);
    }
    
     public static Boolean quickUpsert(List<SObject> sobjs,String fieldnames){
    	if(sobjs == null || sobjs.size() == 0){
    		return false;
    	}
    	return new CommonSelector(sobjs[0].getSObjectType()).doUpsert(sobjs, fieldnames);
    }
    
    
    public static List<Schema.SObjectField> getSObjectFieldList(Schema.SObjectType sobjType,String fieldNamesSplittedComma){
    	return new  CommonSelector(sobjType).getSObjectFieldList(fieldNamesSplittedComma);
    }
    
     public static void checkInsert(Schema.SObjectType sobjType, String fieldNames){
     	try{
     		fflib_SecurityUtils.checkInsert(sobjType, convertStringToList( fieldNames));
     		
     	}catch(fflib_SecurityUtils.SecurityException se){
     		NotificationClass.notifySecurityException( se);
     		
     		throw se;
     	}
    	
    	
    }
    
    public static void checkUpdate(Schema.SObjectType sobjType,String fieldNames){
    	
    	try{
     		fflib_SecurityUtils.checkUpdate(sobjType, convertStringToList(fieldNames));
     		
     	}catch(fflib_SecurityUtils.SecurityException se){
     		NotificationClass.notifySecurityException( se);
     		
     		throw se;
     	}
    	
    	
    }
    
    
    public static void checkDelete(Schema.SObjectType sobjType ){
    	try{
     		fflib_SecurityUtils.checkObjectIsDeletable(sobjType);
     		
     	}catch(fflib_SecurityUtils.SecurityException se){
     		NotificationClass.notifySecurityException( se);
     		
     		throw se;
     	}
    	
    	
    }
    
    public static void checkRead(Schema.SObjectType sobjType,String fieldNames){
    	
    	
    	
    	
    	if(fieldNames == null || fieldNames.trim()==''){
     		return;
     		
     	}
     	
     	
     		if(fieldNames.contains('.')){
     			
	     		new CommonSelector(sobjType).toSOQL(fieldNames);
	     		
	     	}else{
	     		
	     		try{
	     			fflib_SecurityUtils.checkRead(sobjType, convertStringToList(fieldNames));
	     		}catch(fflib_SecurityUtils.SecurityException se){
		     		NotificationClass.notifySecurityException( se);
		     		
		     		throw se;
		     	}
	     		
	     	}
     		
     	
     	
     	
     	
     	
     	/**
     	
     	List<String> fieldList = convertStringToList(fieldNames);
     	if(fieldList == null || fieldList.size() == 0 ){
     		return;
     	}
     	List<String> mainFieldList = new List<String>();
     	Map<String, List<String>>  referenceFieldMap  = new Map<String, List<String>>();
     	for(String field : fieldList){
     		List<String> fieldParts = field.split('\\.');
     		if(fieldParts.size() > 1 ){
     			String objName = fieldParts[fieldParts.size() - 2].trim();
     			String fieldName = fieldParts[fieldParts.size() -1 ].trim();
     			if(objName.endsWith('__r')){
    
     				List<String> fields = referenceFieldMap.get(objName);
     				if(fields == null){
     					fields = new List<String>();
     					referenceFieldMap.put(objName, fields);
     				}
     				fields.add(fieldName);
     			}else{
     				mainFieldList.add(field);
     			}
     		}else{
     			mainFieldList.add(field);
     		}
     	}
     	
     	
     	if(referenceFieldMap.size() > 0){
     		for(String objName : referenceFieldMap.keySet()){
     			fflib_SecurityUtils.checkRead(Schema.getGlobalDescribe().get(objName.replace('__r','__c')),referenceFieldMap.get(objName) );
				System.debug(objName.replace('__r','__c')); 
				System.debug(referenceFieldMap.get(objName));    		
     		}
     		
     	}
     	System.debug(mainFieldList); 
    	fflib_SecurityUtils.checkRead(sobjType, mainFieldList);
    	
		
		
		//fflib_SecurityUtils.checkRead(sobjType, convertStringToList(fieldNames));
		
		*/
    }
    
     public static void checkRead(String objectName,String fieldNames){
     	Schema.SObjectType sobjType = Schema.getGlobalDescribe().get(objectName);
     	if(sobjType == null){
     		return;
     	}
    	checkRead(sobjType, fieldNames);
    }
    
    
  
    //private SimpleQueryFactory queryFactory;
    private Schema.SObjectType sObjectType ;
    
    protected CommonSelector(){
       
        
    }
    
    public CommonSelector(Schema.SObjectType sobjType){
       
       this(sobjType, false);
        
    }
    
    public CommonSelector(Schema.SObjectType sobjType, Boolean includeFieldSetFields){
        
        super(includeFieldSetFields, true, true);
        sObjectType = sobjType;
        
    
    }
    
    protected CommonSelector(Boolean includeFieldSetFields, Boolean enforceCRUD, Boolean enforceFLS){
    	super(includeFieldSetFields, enforceCRUD, enforceFLS);
    }
    
    public void setSObjectType(Schema.SObjectType stype){
    	this.sObjectType = stype;
    }
    public virtual Schema.SObjectType getSObjectType(){
		return sObjectType;
	}
	
	/*
     *  Set default fields that needs to be retrieved by StyleCategorySelector
     */
	public virtual List<Schema.SObjectField> getSObjectFieldList(){
		
		
		return getSObjectFieldList('Id');
        //return new List<Schema.SObjectField>();
       
    }
    
   
   public void checkRead(String fieldNames){
     	
     	if(fieldNames == null || fieldNames.trim()==''){
     		return;
     		
     	}
    	CommonSelector.checkRead(getSObjectType(), fieldNames);
    }
    
    /**
     * get records with specified fields by conditionString
     */
    public List<SObject> get(List<Schema.SObjectField> selectFields, String conditionString) {
		return get(selectFields, conditionString, 0);
	}
	
	public List<SObject> get(List<Schema.SObjectField> selectFields, String conditionString, Integer limitation) {
									
		return Database.query(toSOQL(selectFields, conditionString, limitation));
	}
	
	/**
	* toSQL
	*/
	public String toSOQL(List<Schema.SObjectField> selectFields, String conditionString) {
		return toSOQL(selectFields, conditionString, 0);
	}
	
	public String toSOQL(List<Schema.SObjectField> selectFields, String conditionString, Integer limitation) {
	    
	    //if(queryFactory == null){
        //    queryFactory = new SimpleQueryFactory(this);
        //}
		
		SimpleQueryFactory queryFactory = new SimpleQueryFactory(this);
		queryFactory.selectFields(selectFields);
		
		if(limitation > 0){
			queryFactory.setLimit(limitation);
		}
		
		if(conditionString != null && conditionString.trim() != ''){
		    queryFactory.setCondition(conditionString);
		}
									
		return queryFactory.toSOQL();
	}
	
	 /**
     * get records with specified list of field names by conditionString
     */
    public List<SObject> get(List<String> selectFields, String conditionString) {
        
        
		return get(selectFields, conditionString, 0 );
	}
	
	public List<SObject> get(List<String> selectFields, String conditionString,Integer limitation) {
        					
		return Database.query(toSOQL(selectFields, conditionString, limitation));
	}
	
	public String toSOQL(List<String> selectFields, String conditionString) {
        
        
		return toSOQL(selectFields, conditionString, 0 );
	}
	
	public String toSOQL(List<String> selectFields, String conditionString,Integer limitation) {
        
       // if(queryFactory == null){
        //    queryFactory = new SimpleQueryFactory(this);
        //}
		
		return new SimpleQueryFactory(this).toSOQL(selectFields,conditionString,limitation );
									
	
	}
	
	 /**
     * get records with specified string of field names with ',' splitter by conditionString
     */
     
    public SObject getOne(String selectFieldString){
     	return getOne(selectFieldString, '');
    }
    
    public SObject getOne(String selectFieldString, String conditionString){
     	return getFirst(get(selectFieldString, conditionString));
    }
    
    public List<SObject> get(String selectFieldString){
	  
	    return get(selectFieldString, '');
	}
	
	public List<SObject> get(String selectFieldString, String conditionString){
	  
	    return get(selectFieldString, conditionString, 0);
	}
	
	public List<SObject> get(String selectFieldString, Integer limitation){
	  
	    return get(selectFieldString, '', limitation);
	}
	
	public SObject getOne(String selectFieldString, String conditionString, Integer limitation){
		return getFirst(get(selectFieldString, conditionString,limitation));
	}
	
	public List<SObject> get(String selectFieldString, String conditionString, Integer limitation){

	    return Database.query(toSOQL(selectFieldString, conditionString, limitation));
	}
	
	public String toSOQL(String selectFieldString){
	  
	    return toSOQL(selectFieldString, '');
	}
	
	public String toSOQL(String selectFieldString, String conditionString){
	  
	    return toSOQL(selectFieldString, conditionString, 0);
	}
	
	public String toSOQL(String selectFieldString, String conditionString, Integer limitation){
		
		
		
	    if(selectFieldString == null ){
	        return null;
	    }
	    
	   	if(selectFieldString.trim() == '*'){
	   		return toSOQL(getSObjectAllFields(), conditionString, limitation);
	   	}else{
	   		return toSOQL(CommonSelector.convertStringToList(selectFieldString), conditionString, limitation);
	   	}
	    
	    
	}
	
	
	
 
    private List<Schema.SObjectField> getSObjectAllFields(){
        
        return  getSObjectType().getDescribe().fields.getMap().values();
       
    }
    
    private Set<String> getSObjectNonCalculatedFieldNames(){
    	
    	Set<String> fieldSet = new Set<String>();
    	
    	for(SObjectField field :  getSObjectType().getDescribe().fields.getMap().values()){
    		
    		if(field.getDescribe().isCalculated()){
    			continue;
    		}
    		
    		fieldSet.add(field.getDescribe().getName().toLowerCase());
    		
    	}
    	
    	return fieldSet;
        
        //return  getSObjectType().getDescribe().fields.getMap().keySet();
        
    }
    
    
   
    
    public List<Schema.SObjectField> getSObjectFieldList(String fieldNamesSplittedComma){
    	if(fieldNamesSplittedComma == null ){
	        return null;
	    }
	    
	    List<String> fieldList = new List<String>();
	    
	   
	    for(String f : fieldNamesSplittedComma.split(',')){
	        fieldList.add(f.trim() );
	    }
	    
        return getSObjectFieldList(fieldList);
    }
    
    public List<Schema.SObjectField> getSObjectFieldList(List<String> fieldNames){
        List<Schema.SObjectField>  ret = new List<Schema.SObjectField>();
        if(fieldNames == null || fieldNames.size() == 0){
            return ret;
        }
        
        Map<String, Schema.SObjectField>  allSObjectFieldsMap = getSObjectType().getDescribe().fields.getMap();
        for(String fieldName : fieldNames){
            Schema.SObjectField field = allSObjectFieldsMap.get(fieldName);
            if(field != null){
                ret.add(field);
            }else{
                throw new BasicSelectorException(fieldName + ' does not exist in ' );
            }
        }
        
        return ret;
    }
    
    public Boolean validateFieldList(List<String> fieldNames){
        List<Schema.SObjectField>  ret = new List<Schema.SObjectField>();
        if(fieldNames == null || fieldNames.size() == 0){
            return false;
        }
        
        Map<String, Schema.SObjectField>  allSObjectFieldsMap = getSObjectType().getDescribe().fields.getMap();
        for(String fieldName : fieldNames){
            Schema.SObjectField field = allSObjectFieldsMap.get(fieldName);
            if(field != null){
               
            }else{
                return false;
            }
        }
        
        return true;
    }
    
    public void checkInsert(String fieldNames){
    	CommonSelector.checkInsert(getSObjectType(), fieldNames);
    	
    }
    
    public void checkUpdate(String fieldNames){
    	CommonSelector.checkUpdate(getSObjectType(), fieldNames);
    	
    }
    
    public void checkDelete( ){
    	CommonSelector.checkDelete(getSObjectType());
    	
    }
    
    
    public Boolean doInsert(SObject sObj){
    	
    	return doInsert(new List<SObject>{sObj});
     	
    }
    
    public Boolean doInsert(SObject sObj , String fieldNames){
    	
    	return doInsert(new List<SObject>{sObj}, fieldNames);
     	
    }
    
    public Boolean doInsert(List<SObject> sObjectList){
    	
    	if(sObjectList == null || sObjectList.size() == 0){
    		return false;
    	}
    	
    	String fieldNames = getNotNullFieldNamesWithComma(sObjectList);
    	
    	return doInsert(sObjectList, fieldNames);
    	
     	
    }
    public Boolean doInsert(List<SObject> sObjectList , String fieldNames){
    	
    	if(sObjectList == null || sObjectList.size() == 0 || fieldNames == null ){
    		return false;
    	}
    	
    	if(this.sObjectType != sObjectList[0].getSObjectType()    ){
    		return false;
    	}
    	
    	
		checkInsert(fieldNames);
	
		
		insert sObjectList;
    		

    	return true;
    }
    
    
   
  	public Boolean doUpdate(SObject sObj){
    	
    	return doUpdate(new List<SObject>{sObj});
     	
    }
    
    public Boolean doUpdate(SObject sObj , String fieldNames){
    	
    	return doUpdate(new List<SObject>{sObj}, fieldNames);
     	
    }
    
    public Boolean doUpdate(List<SObject> sObjectList){
    	
    	if(sObjectList == null || sObjectList.size() == 0){
    		return false;
    	}
    	
    	String fieldNames = getNotNullFieldNamesWithComma(sObjectList);
    	
    	return doUpdate(sObjectList, fieldNames);
    	
     	
    }
    
    public Boolean doUpdate(List<SObject> sObjectList , String fieldNames){
    	
    	if(sObjectList == null || sObjectList.size() == 0 || fieldNames == null ){
    		return false;
    	}
    	
    	if(this.sObjectType != sObjectList[0].getSObjectType()    ){
    		return false;
    	}
    	
		checkUpdate(fieldNames);
	
		update sObjectList;
    		
    	
    	
    	return true;
    }
    
    
    public Boolean doUpsert(SObject sObj){
    	
    	return doUpsert(new List<SObject>{sObj});
     	
    }
     
    public Boolean doUpsert(List<SObject> sObjectList){
    	
    	if(sObjectList == null || sObjectList.size() == 0){
    		return false;
    	}
    	
    	String fieldNames = getNotNullFieldNamesWithComma(sObjectList);
    	
    	return doUpsert(sObjectList, fieldNames);
    	
     	
    }
    
    public Boolean doUpsert(SObject sObj , String fieldNames){
    	
    	return doUpsert(new List<SObject>{sObj}, fieldNames);
     	
    }
    

    public Boolean doUpsert(List<SObject> sObjectList , String fieldNames){
    	
    	if(sObjectList == null || sObjectList.size() == 0 || fieldNames == null ){
    		return false;
    	}
    	
    	if(this.sObjectType != sObjectList[0].getSObjectType()    ){
    		return false;
    	}
    	
    	
    		
		checkUpdate(fieldNames);
		checkInsert(fieldNames);
		
		String listType = 'List<' + this.sObjectType + '>';
    	List<SObject> castRecords = (List<SObject>)Type.forName(listType).newInstance();
    	castRecords.addAll(sObjectList);
	
		upsert castRecords;
		
		
    	
    	return true;
    }
    
     public Boolean doDelete(SObject sObj ){
     	
     	return doDelete(new List<SObject>{sObj});
     }
    
     public Boolean doDelete(List<SObject> sObjectList ){
    	
    	if(sObjectList == null || sObjectList.size() == 0 ){
    		return false;
    	}
    	
    	if(this.sObjectType != sObjectList[0].getSObjectType()    ){
    		return false;
    	}
    	
    	
		checkDelete( );
	
		delete sObjectList;
    		
    	
    	
    	return true;
    }
    
   
    public String getNotNullFieldNamesWithComma(List<SObject> sObjs){
    	
    	if( sObjs == null || sObjs.size() == 0){
    		return null;
    	}
    	
    	Set<String> allNotNullFields = new Set<String>();
    	Set<String> allFieldNames = getSObjectNonCalculatedFieldNames();
    	Set<String> notNullFields = new Set<String>();
    	
		for(SObject sobj : sObjs){
			Set<String> names = getNotNullFieldNamesWithComma(sobj, allFieldNames);
			allNotNullFields.addAll(names);
		}
    	
    	if(allNotNullFields.size() > 0  ) {
    		return String.join(new List<String>(allNotNullFields),',');
    	}else{
    		return null;
    	}
    }
    
    private Set<String> getNotNullFieldNamesWithComma(SObject sObj, Set<String> allFieldNames){
    	Set<String> notNullFields = new Set<String>();
    	
    	if(sObj == null || allFieldNames == null )
    		return notNullFields;
    	
    	
    	for(String field : allFieldNames){
    		
    		if('id'.equals(field.toLowerCase())){
    			continue;
    		}
    		Object value = null;
    		try{
    			value = sObj.get(field);
	    		if( value != null){
	    			notNullFields.add(field);
	    		}
    		}catch(Exception e){
    			
    		}
    		
    	}
    	
    	return notNullFields;
    	
    }
    
    public SimpleQueryFactory addOrdering(String fieldName, SortOrder direction){
         //if(queryFactory == null){
         //    queryFactory = new SimpleQueryFactory(this);
         //}
	    
		return new SimpleQueryFactory(this).addOrdering(fieldName, direction);
	
	}
    
   
	public SimpleQueryFactory setParam(String name, Object value){
	     //if(queryFactory == null){
         //    queryFactory = new SimpleQueryFactory(this);
         //}
        return new SimpleQueryFactory(this).setParam(name, value);
	
	}
	
    
    
     public SimpleQueryFactory addSubselect(SObjectType related, String fieldnames,String condition){
         
         //if(queryFactory == null){
         //    queryFactory = new SimpleQueryFactory(this);
         //}
         
         return new SimpleQueryFactory(this).addSubselect( related,  fieldnames, condition);
       
    }
    
   
	public static SimpleQueryFactory simpleQuery(SobjectType sobjType){
    	return new SimpleQueryFactory(new CommonSelector(sobjType));
    }
    
    public SimpleQueryFactory simpleQuery(){
        return new SimpleQueryFactory(this);
    }
    
    
    
    
	
	public class SimpleQueryFactory {
		private fflib_QueryFactory qf;
		private fflib_QueryFactory currentSubqf;
		private Map<String,Object> params = new Map<String,Object>();
		private CommonSelector selector;
		
		public SimpleQueryFactory(CommonSelector selector){
		    this.selector = selector;
		    
		    try{
				qf = selector.newQueryFactory();
				qf.getOrderings().clear();
			}catch(fflib_SecurityUtils.SecurityException se){
	     		NotificationClass.notifySecurityException( se);
	     		
	     		throw se;
	     	}
	     	
			
			
		}
		public SimpleQueryFactory selectFields(String fieldnames){
			this.selectFields(CommonSelector.convertStringToList(fieldnames));
			return this;
		}
		
		public SimpleQueryFactory selectFields(Set<String> fieldnames){
			return selectFields(new List<String>( fieldnames));
		}
		
		public SimpleQueryFactory selectFields(List<String> fieldnames){
			try{
				qf.selectFields(fieldnames);
			}catch(fflib_SecurityUtils.SecurityException se){
	     		NotificationClass.notifySecurityException( se);
	     		
	     		throw se;
	     	}
			
			return this;
		}
		
		public SimpleQueryFactory selectFields(Set<Schema.SObjectField> fieldnames){
			return selectFields(new List<Schema.SObjectField>( fieldnames));
		}
		
		public SimpleQueryFactory selectFields(List<Schema.SObjectField> fieldnames){
			try{
				qf.selectFields(fieldnames);
			}catch(fflib_SecurityUtils.SecurityException se){
	     		NotificationClass.notifySecurityException( se);
	     		
	     		throw se;
	     	}
			
			return this;
		}
		
		public SimpleQueryFactory setCondition(String condition){
			qf.setCondition(condition);
			return this;
		}
		public SimpleQueryFactory setLimit(Integer limitation){
			qf.setLimit(limitation);
			return this;
		}
		public SimpleQueryFactory setParam(String name, Object value){
		    params.put(name, value);
		    return this;
		}
		public SimpleQueryFactory addOrdering(String fieldName, SortOrder direction){
		    if( direction ==SortOrder.ASCENDING){
		        qf.addOrdering(fieldName, fflib_QueryFactory.SortOrder.ASCENDING);
		    }else if(direction ==SortOrder.DESCENDING){
		        qf.addOrdering(fieldName, fflib_QueryFactory.SortOrder.DESCENDING);
		    }
			
			return this;
		}
		
		 
		
		public SimpleQueryFactory clearOrderings(){
			qf.getOrderings().clear();
			return this;
		}
		
		public List<fflib_QueryFactory.Ordering> getOrderings(){
			
        	return qf.getOrderings();
    	
		}
		
		
		
		public SimpleQueryFactory addOrdering(String fieldName, fflib_QueryFactory.SortOrder direction){
		   
		    qf.addOrdering(fieldName, direction);
		    
			
			return this;
		}
		
		public SimpleQueryFactory addOrdering(String fieldName, fflib_QueryFactory.SortOrder direction, Boolean b){
		   
		    qf.addOrdering(fieldName, direction, b);
		    
			
			return this;
		}
		
		public SimpleQueryFactory setOffset(Integer offset){
		   
		    qf.setOffset(offset);
		    
			
			return this;
		}
		
		
		
	    public SimpleQueryFactory addSubselect(sObjectType related,String fieldnames, String condition){
	    	
	    	try{
	    		 if(condition != null && condition.trim()!=''){
			        qf.subselectQuery(related).selectFields(CommonSelector.convertStringToList(fieldnames)).setCondition(condition);
			    }else{
			        qf.subselectQuery(related).selectFields(CommonSelector.convertStringToList(fieldnames));
			    }
	    	}catch(fflib_SecurityUtils.SecurityException se){
	     		NotificationClass.notifySecurityException( se);
	     		
	     		throw se;
	     	}
     		
		   
		     
		    
		    return this;
		}
		
		public SObject getOne(String fieldnames){
		    return getOne(fieldnames, '');
		}
		
		public SObject getOne(String fieldnames, String condition){
		    return getOne(fieldnames, condition, 0);
		}
			    
		public SObject getOne(String fieldnames, String condition, Integer limitation){
		    this.selectFields(fieldnames);
		    if(condition != null && condition.trim()!=''){
		        this.setCondition(condition);
		    }
		    
		    if( limitation != null && limitation > 0){
		        this.setLimit(limitation);
		    }
		    return getOne();
		}
		
		
		public List<SObject> get(String fieldnames){
		    return get(fieldnames, '');
		}
		
		
		public List<SObject> get(String fieldnames, String condition){
		    return get(fieldnames, condition, 0);
		}
		
		public List<SObject> get(String fieldnames, String condition, Integer limitation){
		    this.selectFields(fieldnames);
		    if(condition != null && condition.trim()!=''){
		        this.setCondition(condition);
		    }
		    
		    if( limitation != null && limitation > 0){
		        this.setLimit(limitation);
		    }
		    return get();
		}
		
		
		public String toSOQL(String fieldnames){
		    return toSOQL(fieldnames, '');
		}
		
		
		public String toSOQL(String fieldnames, String condition){
		    return toSOQL(fieldnames, condition, 0);
		}
		
		public String toSOQL(String fieldnames, String condition, Integer limitation){
			
		    this.selectFields(fieldnames);
		    if(condition != null && condition.trim()!=''){
		        this.setCondition(condition);
		    }
		    
		    if( limitation != null && limitation > 0){
		        this.setLimit(limitation);
		    }
		    
		    
		    return toSOQL();
		}
		
		
		public String toSOQL(List<String> fieldnames){
		    return toSOQL(fieldnames, '');
		}
		
		
		public String toSOQL(List<String> fieldnames, String condition){
		    return toSOQL(fieldnames, condition, 0);
		}
		
		public String toSOQL(List<String>  fieldnames, String condition, Integer limitation){
		    this.selectFields(fieldnames);
		    if(condition != null && condition.trim()!=''){
		        this.setCondition(condition);
		    }
		    
		    if( limitation != null && limitation > 0){
		        this.setLimit(limitation);
		    }
		    return toSOQL();
		}
		
		/*
		public String toSOQL(List<Schema.SObjectField> fieldnames){
		    return toSOQL(fieldnames, '');
		}
		
		
		public String toSOQL(List<Schema.SObjectField> fieldnames, String condition){
		    return toSOQL(fieldnames, condition, 0);
		}
		
		public String toSOQL(List<Schema.SObjectField>  fieldnames, String condition, Integer limitation){
		    this.selectFields(fieldnames);
		    if(condition != null && condition.trim()!=''){
		        this.setCondition(condition);
		    }
		    
		    if( limitation != null && limitation > 0){
		        this.setLimit(limitation);
		    }
		    return toSOQL();
		}
		*/
	
		
		public String toSOQL(){
			String sql =  qf.toSOQL();
			
			
			
			return sql;
		}
		
		public List<SObject> get(){
		    String sql = '';
			List<Object> values = params.values();
			Object reserved_var_0 = values.size() > 0 ? values[0]:null;
			Object reserved_var_1 = values.size() > 1 ? values[1]:null;
			Object reserved_var_2 = values.size() > 2 ? values[2]:null;
			Object reserved_var_3 = values.size() > 3 ? values[3]:null;
			Object reserved_var_4 = values.size() > 4 ? values[4]:null;
			Object reserved_var_5 = values.size() > 5 ? values[5]:null;
			Object reserved_var_6 = values.size() > 6 ? values[6]:null;
			Object reserved_var_7 = values.size() > 7 ? values[7]:null;
			Object reserved_var_8 = values.size() > 8 ? values[8]:null;
			Object reserved_var_9 = values.size() > 9 ? values[9]:null;
			
			
			/*System.debug('reserved_var_0 = ' + reserved_var_0);
			System.debug('reserved_var_1 = ' + reserved_var_1);
			System.debug('reserved_var_2 = ' + reserved_var_2);
			System.debug('reserved_var_3 = ' + reserved_var_3);
			System.debug('reserved_var_4 = ' + reserved_var_4);
			System.debug('reserved_var_5 = ' + reserved_var_5);
			System.debug('reserved_var_6 = ' + reserved_var_6);
			System.debug('reserved_var_7 = ' + reserved_var_7);
			System.debug('reserved_var_8 = ' + reserved_var_8);
			System.debug('reserved_var_9 = ' + reserved_var_9);*/
			
			
		    sql = toSOQL();
			
			if(params.size() > 0 ){
				Integer index = 0;
				for(String pname : params.keySet()){
					sql = sql.replaceAll(':\\s{0,10}'+pname+'\\b', ': reserved_var_'+ (index++) );
				}
			}
			
			//System.debug(sql);
			List<SObject>  results = Database.query(sql);
			
			return results;
		   
		}
		
		public SObject getOne(){
		   
		   
		    return  CommonSelector.getFirst(get());
		}
	}
	
	
	 /**
    public class Parameters{
    	Map<String,Object> params = new Map<String,Object>();
    	private CommonSelector comSelector;
    	private Subselector sub_selector;
    	public Parameters(CommonSelector comSelector){
			this.comSelector = comSelector;
			
		}
		
		public Parameters(Subselector sub_selector){
			this.sub_selector = sub_selector;
			
		}
		
		public Parameters setParam(String name, Object value){
			params.put(name,value);
			return this;
		}
		
		public SObject getOne(String fieldnames, String condition){
			return CommonSelector.getFirst(get( fieldnames,  condition));
		}
		
		public List<SObject> get(String fieldnames, String condition){
			return get( fieldnames,  condition,  0);
		}
		public SObject getOne(String fieldnames, String condition, Integer limitation){
			return CommonSelector.getFirst(get( fieldnames,  condition,limitation));
		}
		public List<SObject> get(String fieldnames, String condition, Integer limitation){
			String sql = '';
			List<Object> values = params.values();
			Object reserved_var_0 = values.size() > 0 ? values[0]:null;
			Object reserved_var_1 = values.size() > 1 ? values[1]:null;
			Object reserved_var_2 = values.size() > 2 ? values[2]:null;
			Object reserved_var_3 = values.size() > 3 ? values[3]:null;
			Object reserved_var_4 = values.size() > 4 ? values[4]:null;
			Object reserved_var_5 = values.size() > 5 ? values[5]:null;
			Object reserved_var_6 = values.size() > 6 ? values[6]:null;
			Object reserved_var_7 = values.size() > 7 ? values[7]:null;
			Object reserved_var_8 = values.size() > 8 ? values[8]:null;
			Object reserved_var_9 = values.size() > 9 ? values[9]:null;
			
			
			System.debug('reserved_var_0 = ' + reserved_var_0);
			System.debug('reserved_var_1 = ' + reserved_var_1);
			System.debug('reserved_var_2 = ' + reserved_var_2);
			System.debug('reserved_var_3 = ' + reserved_var_3);
			System.debug('reserved_var_4 = ' + reserved_var_4);
			System.debug('reserved_var_5 = ' + reserved_var_5);
			System.debug('reserved_var_6 = ' + reserved_var_6);
			System.debug('reserved_var_7 = ' + reserved_var_7);
			System.debug('reserved_var_8 = ' + reserved_var_8);
			System.debug('reserved_var_9 = ' + reserved_var_9);
			
			
			if(comSelector != null){
				sql = comSelector.toSOQL(fieldnames, condition, limitation);
			}
			
			else if(sub_selector != null){
				sql = this.sub_selector.toSOQL(fieldnames, condition, limitation);
			}
			
			if(params.size() > 0 ){
				Integer index = 0;
				for(String pname : params.keySet()){
					sql = sql.replaceAll(':\\s{0,10}'+pname+'\\b', ':reserved_var_'+ (index++) );
				}
			}
			
			System.debug(sql);
			return Database.query(sql);
		}
    }
    
    */

    /**
	public class Subselector {
		
		private CommonSelector comSelector;
		
		
		private List<Object[]> subselects = new List<Object[]>();
		
		public SubSelector(CommonSelector comSelector){
			this.comSelector = comSelector;
			
		}
		
		public SubSelector addSubselect(SObjectType related, String fieldnames,String condition){
			
			subselects.add(new Object[]{related, fieldnames, condition});
			
			return this;
			
		}
		
		public Parameters setParam(String name, Object value){
			return new Parameters(this).setParam(name,value);
		}
		
		public String toSOQL(String mainFieldnames, String mainCondition){
			return toSOQL(mainFieldnames, mainCondition, 0);
		}
		
		public String toSOQL(String mainFieldnames, String mainCondition,Integer limitation){
			
			fflib_QueryFactory qf = comSelector.newQueryFactory();
			
			qf.selectFields(CommonSelector.convertStringToList(mainFieldnames));
			if(mainCondition != null && mainCondition.trim() != ''){
				qf.setCondition(mainCondition);
			}
			
			if(limitation > 0 ){
				qf.setLimit(limitation);
			}
				
			
			for(Object[] sub : subselects){
				SObjectType related = (SObjectType)sub[0];
				String fieldnames = (String)sub[1];
				String condition = (String) sub[2];
				
				
				
				if(condition != null && condition !=''){
					qf.subselectQuery(related)
						.selectFields(CommonSelector.convertStringToList(fieldnames)).setCondition(condition);
				}else{
					qf.subselectQuery(related)
						.selectFields(CommonSelector.convertStringToList(fieldnames));
				}
			}
			
			return qf.toSOQL();
		}
		
		
		public SObject getOne(String mainFieldnames, String mainCondition){
			return CommonSelector.getFirst(get(mainFieldnames,mainCondition));
		}
		
		public List<SObject> get(String mainFieldnames, String mainCondition){
			
			return get(mainFieldnames, mainCondition, 0);
			
		}
		
		public SObject getOne(String mainFieldnames, String mainCondition,Integer limitation){
			return CommonSelector.getFirst(get(mainFieldnames,mainCondition,limitation));
		}
		
		public List<SObject> get(String mainFieldnames, String mainCondition, Integer limitation){
			
			String sql = toSOQL(mainFieldnames, mainCondition,limitation);
			Return Database.query(sql);
			
		}
	}
	*/
	
	 /**
    public Parameters setParam(String name, Object value){
		return new Parameters(this).setParam(name,value);
	}
	*/
	
	/**
    public Subselector addSubselect(SObjectType related, String fieldnames,String condition){
    	Subselector subselector = new Subselector(this);
    	subselector.addSubselect( related,  fieldnames, condition);
    	return subselector;
    }
    */
}