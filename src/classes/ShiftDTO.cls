public with sharing class ShiftDTO {
	private String startDate;
	private String sTime;
	private String endDate;
	private String eTime;
	private String sRequiredPositionNumber;
	private String sPlacedPositionNumber;
	private String sOpenPositionNumber;
	private String sLocation;
	private String sVacId;
	private String sCompanyId;
	private String sType;
	private String sId;
	private Boolean sWeeklyRecur;
	private String sRecurEndDate;
	private String sWeekdaysDisplay;
	private String sShiftStatus;
	private String sVacancyName;
	private String sRate;
	//private String sRateType;
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getsTime() {
		return sTime;
	}
	public void setsTime(String sTime) {
		this.sTime = sTime;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String geteTime() {
		return eTime;
	}
	public void seteTime(String eTime) {
		this.eTime = eTime;
	}
	public String getsRequiredPositionNumber() {
		return sRequiredPositionNumber;
	}
	public void setsRequiredPositionNumber(String sRequiredPositionNumber) {
		this.sRequiredPositionNumber = sRequiredPositionNumber;
	}
	public String getsPlacedPositionNumber() {
		return sPlacedPositionNumber;
	}
	public void setsPlacedPositionNumber(String sPlacedPositionNumber) {
		this.sPlacedPositionNumber = sPlacedPositionNumber;
	}
	public String getsOpenPositionNumber() {
		return sOpenPositionNumber;
	}
	public void setsOpenPositionNumber(String sOpenPositionNumber) {
		this.sOpenPositionNumber = sOpenPositionNumber;
	}
	public String getsLocation() {
		return sLocation;
	}
	public void setsLocation(String sLocation) {
		this.sLocation = sLocation;
	}
	public String getsVacId() {
		return sVacId;
	}
	public void setsVacId(String sVacId) {
		this.sVacId = sVacId;
	}
	public String getsCompanyId() {
		return sCompanyId;
	}
	public void setsCompanyId(String sCompanyId) {
		this.sCompanyId = sCompanyId;
	}
	public String getsType() {
		return sType;
	}
	public void setsType(String sType) {
		this.sType = sType;
	}
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public Boolean getsWeeklyRecur() {
		return sWeeklyRecur;
	}
	public void setsWeeklyRecur(Boolean sWeeklyRecur) {
		this.sWeeklyRecur = sWeeklyRecur;
	}
	public String getsRecurEndDate() {
		return sRecurEndDate;
	}
	public void setsRecurEndDate(String sRecurEndDate) {
		this.sRecurEndDate = sRecurEndDate;
	}
	public String getsWeekdaysDisplay() {
		return sWeekdaysDisplay;
	}
	public void setsWeekdaysDisplay(String sWeekdaysDisplay) {
		this.sWeekdaysDisplay = sWeekdaysDisplay;
	}
	public String getsShiftStatus() {
		return sShiftStatus;
	}
	public void setsShiftStatus(String sShiftStatus) {
		this.sShiftStatus = sShiftStatus;
	}
	public String getsVacancyName() {
		return sVacancyName;
	}
	public void setsVacancyName(String sVacancyName) {
		this.sVacancyName = sVacancyName;
	}
	public String getsRate() {
		return sRate;
	}
	public void setsRate(String sRate) {
		this.sRate = sRate;
	}
	/*public String getsRateType() {
		return sRateType;
	}
	public void setsRateType(String sRateType) {
		this.sRateType = sRateType;
	}*/
}