/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

@isTest
public class TestSeekDetails {
//	 public static testMethod void testDoPost(){
//	 	User currentuser =  currentUser();
//    	Advertisement__c ad1 = generaltempAd();
//        StyleCategory__c temptemplate = createTemplate();
//        StyleCategory__c SeekScreen = createSeekScreen(currentuser.Seek_Account__c);
//        Boolean tempEnableSAE = false;
//        Skill_Group__c tempSkillGroup = createNewSkillGroup();
//
//		ad1.Skill_Group_Criteria__c = tempSkillGroup.Id;
//        if(temptemplate!=null){
//        	ad1.Template__c = temptemplate.id;
//        }
//        else{
//        	ad1.Template__c = 'a0190000000UIwuAAG';
//        }
//        ad1.SeekScreen__c = SeekScreen.id;
//
//        ApexPages.StandardController stdController =new ApexPages.StandardController(ad1);
//        SeekDetail sd3 = new SeekDetail(stdController);
//        sd3.getSelectSkillGroupList();
//        sd3.init();
//        system.assert(sd3.org_level_seekAPI);
//        system.assertEquals(sd3.org_level_seekAPI, sd3.getStyles(sd3.org_level_seekAPI));
//        PageReference dopostpage = sd3.dopost();
//        system.assertEquals(dopostpage, null);
//    }
//
//     public static testMethod void testUpdate(){
//	 	User currentuser =  currentUser();
//    	Advertisement__c ad1 = generaltempAd();
//        StyleCategory__c temptemplate = createTemplate();
//        StyleCategory__c SeekScreen = createSeekScreen(currentuser.Seek_Account__c);
//        system.debug('current account =' + currentuser.Seek_Account__c);
//        Boolean tempEnableSAE = false;
//        Skill_Group__c tempSkillGroup = createNewSkillGroup();
//        ad1.Seek_Account__c = currentuser.Seek_Account__c;
//		ad1.Skill_Group_Criteria__c = tempSkillGroup.Id;
//        if(temptemplate!=null){
//        	ad1.Template__c = temptemplate.id;
//        }
//        else{
//        	ad1.Template__c = 'a0190000000UIwuAAG';
//        }
//        ad1.SeekScreen__c = SeekScreen.id;
//        ad1.status__c  = 'Active';
//        ad1.Job_Posting_Status__c = 'active';
//        insert ad1;
//
//        ApexPages.StandardController stdController =new ApexPages.StandardController(ad1);
//        SeekEdit se3 = new SeekEdit(stdController);
//        se3.is_Test = false;
//        se3.init();
//        system.assert(se3.org_level_seekAPI);
//        system.assertEquals(se3.org_level_seekAPI, se3.getStyles(se3.org_level_seekAPI));
//        PageReference dopostpage = se3.dopost();
//        system.assertnotEquals(dopostpage, null);
//    }
    
    /*public static testMethod void testcloneseekad(){
    	System.runAs(DummyRecordCreator.platformUser) {
        User currentuser =  currentUser();	 	
    	Advertisement__c ad1 = generaltempAd();
        StyleCategory__c temptemplate = createTemplate();
        StyleCategory__c SeekScreen = createSeekScreen(currentuser.Seek_Account__c);
        system.debug('current account =' + currentuser.Seek_Account__c);
        Boolean tempEnableSAE = false;
        Skill_Group__c tempSkillGroup = createNewSkillGroup();
        ad1.Seek_Account__c = currentuser.Seek_Account__c;
		ad1.Skill_Group_Criteria__c = tempSkillGroup.Id;
        if(temptemplate!=null){
        	ad1.Template__c = temptemplate.id;
        }
        else{
        	ad1.Template__c = 'a0190000000UIwuAAG';
        }
        ad1.SeekScreen__c = SeekScreen.id;
        ad1.status__c  = 'Active';
        ad1.Job_Posting_Status__c = 'active';
        insert ad1;
        ApexPages.StandardController con = new ApexPages.StandardController(ad1);
        SeekClone thecontroller = new SeekClone(con);
        ApexPages.currentPage().getParameters().put('id',ad1.Id);
        PageReference cloneAdpage = thecontroller.cloneAd();
        system.assertnotEquals(cloneAdpage, null);
		currentuser.Monthly_Quota_Seek__c = '0';
		update currentuser;
		SeekClone thecontroller1 = new SeekClone(con);
		system.assertEquals(thecontroller1.has_error, true);
    	}
    }
    
    public static testMethod void testDoPostNoScreen(){
    	System.runAs(DummyRecordCreator.platformUser) {
	 	User currentuser =  currentUsernoquota();	 	
    	Advertisement__c ad1 = generaltempAd();
        StyleCategory__c temptemplate = createTemplate();
        Boolean tempEnableSAE = false;
        Skill_Group__c tempSkillGroup = createNewSkillGroup();
        
       ad1.Skill_Group_Criteria__c = tempSkillGroup.Id;
        if(temptemplate!=null){
        	ad1.Template__c = temptemplate.id;
        }
        else{
        	ad1.Template__c = 'a0190000000UIwuAAG';
        }       
        ApexPages.StandardController stdController =new ApexPages.StandardController(ad1);
        SeekDetail sd3 = new SeekDetail(stdController);
        sd3.init();
        system.assert(sd3.org_level_seekAPI);
        system.assertEquals(sd3.org_level_seekAPI, !sd3.getStyles(sd3.org_level_seekAPI));
        PageReference dopostpage = sd3.dopost();
        system.assertEquals(dopostpage, null);
        }
        
    }
    
    public static testMethod void testUpdateNoScreen(){
    	System.runAs(DummyRecordCreator.platformUser) {
	 	User currentuser =  currentUsernoquota();	 	
    	Advertisement__c ad1 = generaltempAd();
        StyleCategory__c temptemplate = createTemplate();
        Boolean tempEnableSAE = false;
        Skill_Group__c tempSkillGroup = createNewSkillGroup();
        ad1.Seek_Account__c = currentuser.Seek_Account__c;
       	ad1.Skill_Group_Criteria__c = tempSkillGroup.Id;
        if(temptemplate!=null){
        	ad1.Template__c = temptemplate.id;
        }
        else{
        	ad1.Template__c = 'a0190000000UIwuAAG';
        }   
        ad1.status__c  = 'Active';
        ad1.Job_Posting_Status__c = 'active';
        insert ad1;
            
        ApexPages.StandardController stdController =new ApexPages.StandardController(ad1);
        SeekEdit sd3 = new SeekEdit(stdController);
        sd3.is_Test = false;
        sd3.init();
        system.assert(sd3.org_level_seekAPI);
        system.assertEquals(sd3.org_level_seekAPI, !sd3.getStyles(sd3.org_level_seekAPI));
        PageReference dopostpage = sd3.dopost();
        system.assert(dopostpage!=null);
    	}
    }
    
    public static testMethod void testDoPostNoSAE(){
    	System.runAs(DummyRecordCreator.platformUser) {
	 	User currentuser =  currentUsernosae();	 	
    	Advertisement__c ad1 = generaltempAd();
        StyleCategory__c temptemplate = createTemplate();
        Boolean tempEnableSAE = false;
        Skill_Group__c tempSkillGroup = createNewSkillGroup();
        
        ad1.Skill_Group_Criteria__c = tempSkillGroup.Id;
        if(temptemplate!=null){
        	ad1.Template__c = temptemplate.id;
        }
        else{
        	ad1.Template__c = 'a0190000000UIwuAAG';
        }      
        ApexPages.StandardController stdController =new ApexPages.StandardController(ad1);
        SeekDetail sd3 = new SeekDetail(stdController);
        sd3.init();
        system.assert(!sd3.org_level_seekAPI);
        PageReference dopostpage = sd3.dopost();
        system.assertEquals(dopostpage, null);
    	}
    }
    
    public static testMethod void testUpdateNoSAE(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	User currentuser =  currentUsernosae();	 	
    	Advertisement__c ad1 = generaltempAd();
        StyleCategory__c temptemplate = createTemplate();
        Boolean tempEnableSAE = false;
        Skill_Group__c tempSkillGroup = createNewSkillGroup();
        
        ad1.Skill_Group_Criteria__c = tempSkillGroup.Id;
        if(temptemplate!=null){
        	ad1.Template__c = temptemplate.id;
        }
        else{
        	ad1.Template__c = 'a0190000000UIwuAAG';
        }   
        ad1.status__c  = 'Active';
        ad1.Job_Posting_Status__c = 'active';
        insert ad1;
           
        ApexPages.StandardController stdController =new ApexPages.StandardController(ad1);
    	SeekEdit se3 = new SeekEdit(stdController);
    	se3.is_Test = true;
    	se3.init();
        system.assert(!se3.org_level_seekAPI);
        PageReference dopostpage = se3.dopost();
        system.assert(dopostpage!=null);
    	}
    }
    
    public static StyleCategory__c createSeekAccount(){
    	StyleCategory__c result = new StyleCategory__c();
    	result.RecordTypeId = DaoRecordType.seekaccountOldRT.Id;
    	result.Name = 'test';
    	result.Advertiser_Id__c = 'testAccount';
    	result.Min_Jobs__c = '1';
    	result.Max_Jobs__c = '2';
    	result.Seek_Default_Application_Form__c = true;
    	result.Account_Active__c = true;
    	try{
    		insert result;
    	}catch(Exception e){
    		result = new StyleCategory__c();
    	}
    	return result;
    }
    
    public static StyleCategory__c createSeekAccountNoActiveSAE(){
    	StyleCategory__c result = new StyleCategory__c();
    	result.RecordTypeId = DaoRecordType.seekaccountOldRT.Id;
    	result.Name = 'test';
    	result.Advertiser_Id__c = 'testAccountNoActiveSAE';
    	result.Min_Jobs__c = '1';
    	result.Max_Jobs__c = '2';
    	result.Seek_Default_Application_Form__c = false;
    	try{
    		insert result;
    	}catch(Exception e){
    		result = new StyleCategory__c();
    	}
    	return result;
    }
    
    public static User currentUser(){
    	StyleCategory__c seekaccount = createSeekAccount();
    	String UserId = UserInfo.getUserId();
    	User currentuser = [select Id , ProfileId, Email,Phone,
					WebJobFooter__c, View_CTC_Admin__c, VacancySearchFile__c, 
					ClientSearchFile__c,CareerOne_Usage__c,PeopleSearchFile__c,Seek_Usage__c, 
					MyCareer_Usage__c, Monthly_Quota_Seek__c, 
					Monthly_Quota_MyCareer__c, Monthly_Quota_CareerOne__c, 
					Manage_TimeSheet__c, Manage_Quota__c, JobBoards__c, 
					AmazonS3_Folder__c,ContactId, AccountId, Job_Posting_Company_Name__c, Seek_Account__c 
					from User where Id=:UserId];
		system.debug('seekaccount =' + seekaccount);
		currentuser.Seek_Account__c = seekaccount.Id;
		currentuser.Monthly_Quota_Seek__c = '10';
		currentuser.Seek_Usage__c = '0';
        currentuser.AmazonS3_Folder__c = 'testFolder';
		update currentuser;
		system.assertEquals([select Monthly_Quota_Seek__c from User where Id =:UserId].Monthly_Quota_Seek__c, '10');
		return currentuser;
    }
    
    public static User currentUsernoquota(){
    	StyleCategory__c seekaccount = createSeekAccount();
    	String UserId = UserInfo.getUserId();
    	User currentuser = [select Id , ProfileId, Email,Phone,
					WebJobFooter__c, View_CTC_Admin__c, VacancySearchFile__c, 
					ClientSearchFile__c,CareerOne_Usage__c,PeopleSearchFile__c,Seek_Usage__c, 
					MyCareer_Usage__c, Monthly_Quota_Seek__c, 
					Monthly_Quota_MyCareer__c, Monthly_Quota_CareerOne__c, 
					Manage_TimeSheet__c, Manage_Quota__c, JobBoards__c, 
					AmazonS3_Folder__c,ContactId, AccountId, Job_Posting_Company_Name__c, Seek_Account__c 
					from User where Id=:UserId];
		system.debug('seekaccount =' + seekaccount);
		currentuser.Seek_Account__c = seekaccount.Id;
		currentuser.Monthly_Quota_Seek__c = '0';
        currentuser.AmazonS3_Folder__c = 'testFolder';
		update currentuser;
		return currentuser;
    }
    
    public static User currentUsernosae(){
    	StyleCategory__c seekaccount = createSeekAccountNoActiveSAE();
    	String UserId = UserInfo.getUserId();
    	User currentuser = [select Id , ProfileId, Email,Phone,
					WebJobFooter__c, View_CTC_Admin__c, VacancySearchFile__c, 
					ClientSearchFile__c,CareerOne_Usage__c,PeopleSearchFile__c,Seek_Usage__c, 
					MyCareer_Usage__c, Monthly_Quota_Seek__c, 
					Monthly_Quota_MyCareer__c, Monthly_Quota_CareerOne__c, 
					Manage_TimeSheet__c, Manage_Quota__c, JobBoards__c, 
					AmazonS3_Folder__c,ContactId, AccountId, Job_Posting_Company_Name__c, Seek_Account__c 
					from User where Id=:UserId];
		system.debug('seekaccount =' + seekaccount);
		currentuser.Seek_Account__c = seekaccount.Id;
		currentuser.Monthly_Quota_Seek__c = '10';
		currentuser.Seek_Usage__c = '0';
        currentuser.AmazonS3_Folder__c = 'testFolder';
		update currentuser;
		return currentuser;
    }
    
    private static StyleCategory__c createTemplate(){
    	StyleCategory__c result = new StyleCategory__c();
    	result.RecordTypeId = DaoRecordType.adTemplateRT.Id;
    	result.LogoID__c = '123456';
    	result.templateSeek__c = '123456';
    	result.screenId__c = '123456';
    	
		try{
			insert result;
		}catch(Exception e){
			result = new StyleCategory__c();
		}
		return result;
	}
	
	private static StyleCategory__c createSeekScreen(String seekAccountId){
		StyleCategory__c result = new StyleCategory__c();
		result.RecordTypeId = DaoRecordType.seekScreenRT.Id;
		result.Active__c = true;
		result.screenId__c = '123456';
		result.Seek_Account__c = seekAccountId;
		try{
			insert result;
		}catch(Exception e){
			result = new StyleCategory__c();
		}
		return result;
	}
	
	private static Skill_Group__c createNewSkillGroup(){
		Skill_Group__c result = new Skill_Group__c();
		result.Name = 'test';
		result.Skill_Group_External_Id__c = '123456';
		result.Enabled__c = true;
		try{
			insert result;
		}
		catch(Exception e){
			result = new Skill_Group__c();
		}
		return result;
	}
	
	private static Advertisement__c generaltempAd(){
		Advertisement__c ad1 = new Advertisement__c();
		ad1.online_footer__c = 'online_footer__c';
        ad1.Template__c = 'Template__c';
        ad1.Bullet_1__c = 'Bullet_1__c';
        ad1.Bullet_2__c = 'Bullet_2__c';
        ad1.Bullet_3__c = 'Bullet_3__c';
        
        ad1.SeekLocation__c = 'SeekLocation__c';
        ad1.SeekArea__c = 'SeekArea__c';
        ad1.Worktype__c = 'SeekWorktype__c';
        ad1.SeekClassification__c = 'SeekClassification__c';
        ad1.SeekSubClassification__c = 'SeekSubClassification__c';
        ad1.SeekFunction__c = 'SeekFunction__c';
        ad1.isStandOut__c = true;
        ad1.Search_Bullet_Point_1__c = '1&<>/\'"';
        ad1.Search_Bullet_Point_2__c = '2&<>/\'"';
        ad1.Search_Bullet_Point_3__c = '3&<>/\'"';
        ad1.SeekSalaryText__c = '4&<>/\'"';
        ad1.Reference_No__c = 'Reference_No__c';
        ad1.Application_Form_Style__c = 'a0190000000UDS5AAO';   
		return ad1;
	}*/
}