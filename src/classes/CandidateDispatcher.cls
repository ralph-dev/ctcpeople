/**
*
* Author: Raymond
*
* SF doesn't allow too many candidate records to link to one single account record.
* This class is created so that everytime when a candidate got created, it will be linked
* to a candidate pool which is an account record and has been linked to less than 25k candidates
* at that moment.
*
*
*/

public with sharing class CandidateDispatcher {
	
	public Id independentCandId = null;
	private Integer contactlimit=DaoContact.MAXIMUM_CONTACT_LIMIT;
	public class CreateCandidatePoolException extends Exception{}
	
	// Retrieve all existing candidate pool accounts
	private RecordType candPoolRT;
	private List<Account> candPools;
	
	public Integer getContactLimit(){
		return contactLimit;
	}
	
	public CandidateDispatcher(Id independentCandId){
		this.independentCandId=independentCandId;
		// Change the limit on number of contact that linked to candidate pool to 3
		// if it is on testing condition
		system.debug('Is running test?'+Test.isRunningTest());
		if(Test.isRunningTest()){
			contactLimit = 200;
		}
		
	}
	
	/**
		Receive a contact list then update candidates within this list 
		and link them to a candidate pool that contain candidates less
		than DaoContact.MAXIMUM_CONTACT_LIMIT. If candidates have been
		assigned to a company (non-candidate-pool account), they will be
		ignored.
	*/
	public void dispatchCand(List<Contact> conList){
		candPoolRT = DaoRecordType.candidatePoolRT;
		candPools = DaoAccount.getAccsByRecordTypeIdInDescCreatedDate(candPoolRT.id);
		//system.debug('existing candidate pools:'+candPools);
		
		// Generate an Id->Account for search
		Map<Id,Account> candPoolMap = genAccMap(candPools);
		
		// Find all candidates of given contact list that haven't  
		// been assigned to a company
		List<Contact> candList = new List<Contact>();
		for(Contact con:conList){
		    //System.debug('error here: ' + con);
			if(con.RecordTypeId == independentCandId && (con.AccountId == null || candPoolMap.get(con.AccountId)!=null))
				candList.add(con);
		}
		if(candList.size() == 0)
			return;
		system.debug('candidates to dispatch '+candList.size());
		Account candPool = getProperCandidatePool(candPools);
		if(candPool != null){
			updateCand(candList, candPool.Id);
			fflib_SecurityUtils.checkFieldIsUpdateable(Contact.SObjectType, Contact.AccountId);
			update candList;
		}else
			NotificationClass.notifyErr2Dev('CandidateDispatcher can not retrieve proper candidate pool. Program stop.',  new CreateCandidatePoolException('CandidateDispatcher Error'));
		return;
	}
	
	/**
		Update candidates and link them to nominated candidate pool
	*/
	private void updateCand(List<Contact> candList,Id candPoolId){
		for(Contact con:candList){
			con.AccountId = candPoolId;
		}
	}

	/**
		Convert a given account list to an Id->Account map.
	*/
	private Map<Id,Account> genAccMap(List<Account> accList){
		Map<Id,Account> accMap = new Map<Id,Account>();
		for(Account acc:accList){
			accMap.put(acc.Id,acc);
		}
		return accMap;
	}


	/**
		Return a proper candidate pool which is either the latest one and it contains less than
		DaoContact.MAXIMUM_CONTACT_LIMIT candidate records, or a new one.
	*/
	private Account getProperCandidatePool(List<Account> candPools){
		Account candPool = null;
		if(candPools.size()!=0){
			// Get the latest created candidate pool
			Account tmp = candPools.get(0);
			// Count the number of contacts that are under this latest candidate pool
			Integer noOfContact = DaoContact.countContactForComp(tmp.id);
			system.debug('number of contact:'+noOfContact);
			// if exceed the limit, create a new candidate pool
			if(noOfContact>=contactLimit){
				candPool = createCandidatePool();
				if(candPool == null)
					candPool = tmp;
			}else
				candPool = tmp;
		}else{
			candPool = createCandidatePool();
		}
		return candPool;
	}
	
	/**
		Create a new candidate pool. 
	*/
	private Account createCandidatePool(){
		RecordType candPoolRT = DaoRecordType.candidatePoolRT;
		String candPoolName = 'Candidate Pool';
		if(PCAppSwitch__c.getInstance()!=null && PCAppSwitch__c.getInstance().Default_Name_Of_Candidate_Pool__c!=null){
			candPoolName = PCAppSwitch__c.getInstance().Default_Name_Of_Candidate_Pool__c;
		}
		
		Account candPool = new Account(Name=candPoolName,RecordTypeId=candPoolRT.Id);
		Boolean createCandPoolSucc = true;
		try{
			//check FLS
			List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
				Account.Name,
				Account.RecordTypeId
			};
			fflib_SecurityUtils.checkInsert(Account.SObjectType, fieldList);
			insert candPool;
		}catch(Exception e){
			createCandPoolSucc = false;
			NotificationClass.notifyErr2Dev('CandidateDispatcher fail on creating new candidate pool', e);
			return null;
		}
		return candPool;			
	}

}