@isTest
private class BatchUpdateAllFormTest {

	private static testMethod void test() {
		System.runAs(DummyRecordCreator.platformUser) {
			BatchUpdateAllForm bf = new BatchUpdateAllForm();
        
	        try {
	            system.assertEquals(null, bf.start(null));
	        } catch (Exception e) {
	            
	        }
	        try {
	            bf.execute(null, new List<SObject>{new Contact()});
	        } catch (Exception e) {
	            
	        }
	        bf.finish(null);
	        system.assert(true);
		}
        
	}

}