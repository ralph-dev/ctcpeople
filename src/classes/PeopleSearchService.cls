/*
 * Author: Booz S. Espiridion
 */
public with sharing class PeopleSearchService {	
    public static String PREFERENCE_BASE_NAME = 'ContactSearchUserPreferenceName_';
    
    private DescribeSObjectResult currentObject;
    private Map<String, Schema.SObjectField> currentObjectFields;
    
    /*
     * Update getDocName() method to include new values added
    */
    public Enum SearchObject{
        Contact,    //Contact object
        Vacancy,    //Placement__c object
        Client      //Account object
    }
    
    /*
     * Update this method to include new SearchObject
    */
    public String getDocName(SearchObject searchObj){
        String docName;
        User currentUser = new UserInformationCon().getUserDetail();
        if(currentUser != null){
            //get filename
            if(searchObj == SearchObject.Contact){
                docName = currentUser.PeopleSearchFile__c;
            }
            if(searchObj == SearchObject.Vacancy){
                docName = currentUser.VacancySearchFile__c;
            }
            if(searchObj == SearchObject.Client){
                docName = currentUser.ClientSearchFile__c;
            }
        }
        return docName;
    }
    
    /*
     * Update this method to include new SearchObject
    */
    public SObjectType getObjectType(SearchObject searchObj){
        if(searchObj == SearchObject.Contact){
            return Contact.SObjectType;
        }
        if(searchObj == SearchObject.Vacancy){
            return Placement__c.SObjectType;
        }
        if(searchObj == SearchObject.Client){
            return Account.SObjectType;
        }
        return null;
    }
    
    public PeopleSearchService(){
        
    }
    
    public String getSelectedDisplayColumn(){
        String output = getContactDisplayColumns();
        return String.isEmpty(output) ? getContactDefaultDisplayColumn() : output;
    }
    
    public List<PeopleSearchDTO.ContactSearchResult> searchCandidate(PeopleSearchDTO.SearchQuery search){
        List<PeopleSearchDTO.ContactSearchResult> output = new List<PeopleSearchDTO.ContactSearchResult>();
        List<Contact> searchedCandidates = new List<Contact>();

        String keyword = search.resumeKeywords;
        DaxtraKeywordSearchCriteria dKeywordSearchCriteria = new DaxtraKeywordSearchCriteria();
        List<SearchResult> daxtraSearchResult = new List<SearchResult>();
        Map<String, PeopleSearchDTO.ContactSearchResult> contactIdMap = new Map<String, PeopleSearchDTO.ContactSearchResult>();       

        //If Keyword Search used
        if (keyword!=''&&keyword!=null) {
            List<Contact> cons = new List<Contact>();
            //Use Daxtra Keyword Search to get the SearchResult List
            dKeywordSearchCriteria = generateDaxtraKeywordSearchCriteria(new Set<Id>(), keyword);
            String result = keywordSearchRequest(dKeywordSearchCriteria);
            if (result.contains('ErrorMessage')) {
                throw new keywordSearchException(result);
            }
            SearchResults sResults = (SearchResults)JSON.deserialize(result, SearchResults.class);
            daxtraSearchResult = sResults.searchResults;
            if (!daxtraSearchResult.isEmpty()) {
                for(SearchResult searchResult: daxtraSearchResult) {
                    PeopleSearchDTO.ContactSearchResult cSearchResult = new PeopleSearchDTO.ContactSearchResult();
                    cSearchResult.MatchResult = searchResult;
                    contactIdMap.put(searchResult.getCandidateId(), cSearchResult);
                }
                //If MetaData Search used (Keyword combine MetaData)
                if(search.criteria!=null&&!(search.criteria).isEmpty()){
                    cons = new CandidateSelector().selectContactByCriteria(search.criteria, getSelectedDisplayColumn(), contactIdMap.keySet()); 
                //If only Keyword Search used    
                } else {
                    cons = new CandidateSelector().selectContactByIds(contactIdMap.keySet(), getSelectedDisplayColumn());
                }
                if(!cons.isEmpty()) { 
                    for(Contact con: cons) {
                        if(contactIdMap.containsKey(con.Id)) {
                            PeopleSearchDTO.ContactSearchResult cSearchResult = new PeopleSearchDTO.ContactSearchResult();
                            cSearchResult = contactIdMap.get(con.Id);
                            cSearchResult.Contact = con;
                            output.add(cSearchResult);
                        }
                    }
                }
            }
        } else {
            //If only MetaData Search used
            if(search.criteria!=null&&!(search.criteria).isEmpty()){
                searchedCandidates = new CandidateSelector().selectContactByCriteria(search.criteria, getSelectedDisplayColumn());
                if(!searchedCandidates.isEmpty()) {
                    for(Contact candidate : searchedCandidates){
                        PeopleSearchDTO.ContactSearchResult searchResult = new PeopleSearchDTO.ContactSearchResult(candidate);
                        output.add(searchResult);
                    }
                }            
            }   
        }
        List<Contact> consSkillSearch = new List<Contact>();
        //MedaData Search, KeyWord Search together with Skill Search
        if(!output.isEmpty()) {
          
            if(search.skillsSearch != null && !String.isBlank(search.skillsSearch.query) ) {
                Set<String> consIdSet = new Set<String>();
                for(PeopleSearchDTO.ContactSearchResult con: output) {
                    consIdSet.add(con.Contact.Id);
                }
                consSkillSearch = new CandidateSelector().selectContactByQuery(search.skillsSearch, getSelectedDisplayColumn(), consIdSet);
                
                if(!consSkillSearch.isEmpty()) { 
                    output.clear();
                    for(Contact con: consSkillSearch) {
                        PeopleSearchDTO.ContactSearchResult cSearchResult = new PeopleSearchDTO.ContactSearchResult(con);
                        if(contactIdMap.containsKey(con.Id)) {
                            cSearchResult = contactIdMap.get(con.Id);
                            cSearchResult.Contact = con;
                        } 
                        output.add(cSearchResult);
                    }
                    
                } else {
                    output.clear();
                }
            }
            //Skill Search only
        } else if( (keyword =='' || keyword==null) && (search.criteria==null ||(search.criteria).isEmpty() ) && search.skillsSearch != null && !String.isBlank(search.skillsSearch.query) ) {
            consSkillSearch = new CandidateSelector().selectContactByQuery(search.skillsSearch, getSelectedDisplayColumn());
            if(!consSkillSearch.isEmpty()) { 
                for(Contact con: consSkillSearch) {
                    PeopleSearchDTO.ContactSearchResult searchResult = new PeopleSearchDTO.ContactSearchResult(con);
                    output.add(searchResult);
                }
            }
        } 
        return output;
    }

    public List<PeopleSearchDTO.Field> getContactCriteriaList(){
        return getFieldsList(SearchObject.Contact);
    }
    
    private void getCurrentSObjectDetail(SearchObject type){
        currentObject = Schema.getGlobalDescribe().get(getObjectType(type).getDescribe().getName()).getdescribe();
        currentObjectFields = currentObject.fields.getmap();
    }
    
    public List<PeopleSearchDTO.Field> getFieldsList(SearchObject type){
        List<PeopleSearchDTO.Field> output = new List<PeopleSearchDTO.Field>();
        
        XMLDom XMLDocument = getXMLDom(type);
        
        final String OBJECT_NAME = 'object';
        final String FIELD_NAME = 'field';
        
        getCurrentSObjectDetail(type);
        
        if (XMLDocument != null && XMLDocument.getElementsByTagName(OBJECT_NAME) != null){ 
            List<xmldom.Element> objelements = XMLDocument.getElementsByTagName(OBJECT_NAME);
            if(objelements != null && !objelements.isEmpty()){
                //Parse all field elements
                List<xmldom.Element> fieldElements = objelements[0].getElementsByTagName(FIELD_NAME);
                for(XMLDom.Element fieldElement : fieldElements){
                    PeopleSearchDTO.Field resultingField = buildField(fieldElement);
                    if(resultingField != null){
                        output.add(resultingField);
                    }
                }                   
            }
        }
        
        return output;
    }
    
    public PeopleSearchDTO.Field buildField(XMLDom.Element fieldElement){       
        PeopleSearchDTO.Field fd;
        if(fieldElement != null){
            String apiName = fieldElement.getValue('apiName');
            Schema.SObjectField fieldObj = currentObjectFields.get(apiName);
            //check if the field from the xml doc is available in the object
            if(fieldObj != null){
                DescribeFieldResult fieldDescribe = fieldObj.getDescribe();
                if(fieldDescribe.isAccessible()){//check the field level visibility
                    fd = new PeopleSearchDTO.Field();
                    fd.apiName =        apiName;
                    fd.label =          fieldElement.getValue('label');
                    fd.type =           getFieldType(fieldElement.getValue('type'));
                    fd.sequence =       fieldElement.getValue('sequence');
                    
                    if(fd.apiName != null && fd.type != null && fd.type.toUpperCase().contains(PeopleSearchDTO.F_PICKLIST)){
                        fd.picklistValues = getPicklistValue(fd.apiName);
                    }
                    fd.removeIDFieldOnReferenceField();
                }
            }
        }
        return fd;
    }
    
    public String getFieldType(String rawType){
        String output = rawType;
        if(rawType.contains(PeopleSearchDTO.F_STRING)){
            output = PeopleSearchDTO.F_STRING;
        }
        return output;
    }
    
    public List<PeopleSearchDTO.NamedValue> getPicklistValue(String picklistFieldNameRaw){
        List<PeopleSearchDTO.NamedValue> output = new List<PeopleSearchDTO.NamedValue>();
        String picklistFieldName = picklistFieldNameRaw.toLowerCase();
        Schema.SObjectField currentField = currentObjectFields.get(picklistFieldName);
        if(currentField != null){
            DescribeFieldResult fieldDescribe = currentField.getDescribe();
            if(picklistFieldName == PeopleSearchDTO.RECORDTYPEID){
                for(Schema.RecordTypeInfo recType : currentObject.getRecordTypeInfos()){
                    if(recType.getName().toLowerCase() != 'master'){//remove placeholder record type
                        output.add(new PeopleSearchDTO.NamedValue(recType.getName(), recType.getRecordTypeId()));
                    }
                }
            }else{
                for(Schema.PicklistEntry pickListValue : fieldDescribe.getPicklistValues()){
                    output.add(new PeopleSearchDTO.NamedValue(pickListValue.getLabel(), pickListValue.getLabel()));
                }
            }
        }
        return output;
    }
    
    public XMLDom getXMLDom(SearchObject searchObj){
        XMLDom output;
        if(searchObj != null){
            String namestr = searchObj.name();
            String docName = getDocName(searchObj);
            
            Document newdoc = DaoDocument.getCTCSearchCriteriaDocument(docName, namestr);//When there is no doc in personal documents, create one
            docName = newdoc.Name;
            
            namestr = PeopleSearchDTO.DOC_BASE_NAME + namestr;//ClicktoCloud_Xml_Contact  
            Document usedDoc = DaoDocument.getCTCConfigFileByDevName(namestr); //return document
            
            Blob docBodyBlob = usedDoc.Body;
            String XMLString = docBodyBlob.toString();
            
            output = new XMLDom(XMLString);
        }
        return output;
    }
    
    public List<PeopleSearchOperatorFactory.FieldType> getFieldTypeList(){
        return new PeopleSearchOperatorFactory().allSupportedFieldTypes();
    }
    
    // initialise user details for user preference
    public DisplayColumnHandler initUserDetail(Boolean useUniqueDefault){
        Id Userid = Userinfo.getUserId();
        Id RecordTypeId = DaoRecordType.googleSearchUserPerformacne.id;
        String GoogleSearchUserPreferenceName = PREFERENCE_BASE_NAME + Userid;
        DisplayColumnHandler output = new DisplayColumnHandler(Userid, RecordTypeId ,GoogleSearchUserPreferenceName);
        output.removeIDPrefix = true;
        if(useUniqueDefault){
            output.setUniqueDefaultXML(getDocName(SearchObject.Contact));
        }
        return output;
    }
    
    public String getContactDisplayColumns(){
        return initUserDetail(true).getSearchColumnJSONFromUserPreference();
    }
    
    public String getContactDefaultDisplayColumn(){
        return initUserDetail(true).getSearchColumnJSONFromXml();
    }
    
    public String saveContactDisplayColumn(String jsonColumn){
        DisplayColumnHandler handler = initUserDetail(true);
        handler.updateGoogleSearchColumnJSON(jsonColumn);
        
        return getContactDisplayColumns();
    }
    
    public String getAllContactFields(){
        Set<String> excludedContactColumn = PeopleSearchHelper.getContactFieldAPIName(new List<Schema.SObjectField>{
                Contact.Id
        });
        
        List<PeopleSearchDTO.FieldColumn> output = new List<PeopleSearchDTO.FieldColumn>();
        getCurrentSObjectDetail(SearchObject.Contact);
        if(currentObjectFields != null){
            for(Schema.SObjectField currentField : currentObjectFields.values()){
                if(!excludedContactColumn.contains(currentField.getDescribe().getName())){
                    output.add(new PeopleSearchDTO.FieldColumn(currentField));
                }else{
                    //system.debug(LoggingLevel.FINE, '\n\nExcluded field: ' +currentField.getDescribe().getName()+ '\n\n');
                }
            }
        }
        return JSON.serialize(output);
    }
    
    public String keywordSearchRequest(DaxtraKeywordSearchCriteria searchCriteria) {
        String requestBody = JSON.serialize(searchCriteria);
        String searchEndpoint = Endpoint.getPeopleSearchEndpoint();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(searchEndpoint + '/daxtrasearch/candidate');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setBody(requestBody);
        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        if(Test.isRunningTest()) {
            res.setBody('{"SearchResults":[]}');
            res.setStatusCode(200);
        } else {
            res = http.send(req);
        }
        return res.getBody();
    }
    
    public List<SkillSearchDTO.Skill> getAllSkills() {
        List<SkillSearchDTO.Skill> allSkills = new List<SkillSearchDTO.Skill>();
        List<Skill__c> skillList= SkillSelector.getAllSkills();
        for (Skill__c s : skillList) {
            SkillSearchDTO.Skill skill = new SkillSearchDTO.Skill(s.Id, s.Name, s.Skill_Group__c, s.Skill_Group__r.Name);
            allSkills.add(skill);
        }
        return allSkills;
    }    
    
    private DaxtraKeywordSearchCriteria generateDaxtraKeywordSearchCriteria(Set<Id> contactIds, String keyword) {
       DaxtraKeywordSearchCriteria dKeywordSearchCriteria = new DaxtraKeywordSearchCriteria();
       dKeywordSearchCriteria.Query = keyword;
       return dKeywordSearchCriteria;
    }
    
    //Inner Class
    private class DaxtraKeywordSearchCriteria {
        String OrgId;
        String Query;
        String Stem;
        List<DaxtraSearchCriterias> SearchCriterias;

        private DaxtraKeywordSearchCriteria() {
            this.OrgId = Userinfo.getOrganizationId();
            //this.Stem = 'on';
        }
    }

    private class DaxtraSearchCriterias {
        String FieldName;
        String Condition;
        Set<Id> Values;

        private DaxtraSearchCriterias(Set<Id> contactIds) {
            this.FieldName = 'CandidateId';
            this.Condition = 'in';
            this.Values = contactIds;
        }
    }

    private class SearchResults {
        private List<SearchResult> searchResults;
    }
    
    public class keywordSearchException extends Exception{}
}