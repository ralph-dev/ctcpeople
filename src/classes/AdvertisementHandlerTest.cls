/**
 * This is the test for AdvertisementHandler
 *
 * Created by: Lina Wei
 * Created on: 29/03/2017
 */

@isTest
private class AdvertisementHandlerTest {

    static {
        TriggerHelper.disableAllTrigger();
        TriggerHelper.DisableTriggerOnAdvertisement = false;
    }

    static testMethod void testArchiveAdWhenAutoArchiveStatusChange() {
        System.runAs(DummyRecordCreator.platformUser) {
            Advertisement__c ad = AdvertisementDummyRecordCreator.generateJXTAdDummyRecord();
            insert ad;
            Test.startTest();
            ad.JXT_Auto_Archive__c = true;
            update ad;
            Test.stopTest();
            Advertisement__c adAfter = new AdvertisementSelector().getAdvertisementById(ad.Id);
            System.assertEquals('Expired', adAfter.Status__c);
            System.assertEquals(false, adAfter.JXT_Auto_Archive__c);
        }
    }


    static testMethod void testUpdateUserUsage() {
        List<Advertisement__c> ads = new List<Advertisement__c>();
        User admin = DummyRecordCreator.admin;
        User platformUser = DummyRecordCreator.platformUser;
        platformUser.Seek_Usage__c = String.valueOf(1);
        update platformUser;

        //Create data
        Advertisement__c jxtAd = AdvertisementDummyRecordCreator.generateJXTAdDummyRecord();
        Advertisement__c seekAd = AdvertisementDummyRecordCreator.generateSeekAdDummyRecord();
        ads.add(jxtAd);
        ads.add(seekAd);

        System.runAs(platformUser) {
            insert ads;
       
            Test.startTest();
            jxtAd.Job_Posting_Status__c = 'Posting Failed';
            seekAd.Job_Posting_Status__c = 'Posting Failed';
            update ads;
            Test.stopTest();
            User userAfter = new UserSelector().getUser(platformUser.Id);
            System.assertEquals(String.valueOf(0), userAfter.Seek_Usage__c);
        }
    }

    static testMethod void testUpdateUserUsageWithMultipleFail() {
        List<Advertisement__c> ads = new List<Advertisement__c>();
        User admin = DummyRecordCreator.admin;
        User platformUser = DummyRecordCreator.platformUser;
        platformUser.Seek_Usage__c = String.valueOf(2);
        update platformUser;

        //Create data
        Advertisement__c seekAd1 = AdvertisementDummyRecordCreator.generateSeekAdDummyRecord();
        Advertisement__c seekAd2 = AdvertisementDummyRecordCreator.generateSeekAdDummyRecord();
        ads.add(seekAd1);
        ads.add(seekAd2);

        System.runAs(platformUser) {
            insert ads;
        
            Test.startTest();
            seekAd1.Job_Posting_Status__c = 'Posting Failed';
            seekAd2.Job_Posting_Status__c = 'Posting Failed';
            update ads;
            Test.stopTest();
            User userAfter = new UserSelector().getUser(platformUser.Id);
            System.assertEquals(String.valueOf(0), userAfter.Seek_Usage__c);
        }
    }

    static testMethod void testUpdateUserUsageIfUsageIsLessThanFailNumber() {
        List<Advertisement__c> ads = new List<Advertisement__c>();
        User admin = DummyRecordCreator.admin;
        User platformUser = DummyRecordCreator.platformUser;
        platformUser.Seek_Usage__c = String.valueOf(1);
        update platformUser;

        //Create data
        Advertisement__c seekAd1 = AdvertisementDummyRecordCreator.generateSeekAdDummyRecord();
        Advertisement__c seekAd2 = AdvertisementDummyRecordCreator.generateSeekAdDummyRecord();
        ads.add(seekAd1);
        ads.add(seekAd2);

        System.runAs(platformUser) {
            insert ads;
        
            Test.startTest();
            seekAd1.Job_Posting_Status__c = 'Posting Failed';
            seekAd2.Job_Posting_Status__c = 'Posting Failed';
            update ads;
            Test.stopTest();
            User userAfter = new UserSelector().getUser(platformUser.Id);
            System.assertEquals(String.valueOf(0), userAfter.Seek_Usage__c);
        }
    }

    static testMethod void testUnusedMethod() {
    	 System.runAs(DummyRecordCreator.platformUser) {
    	 	AdvertisementHandler handler = new AdvertisementHandler();
	        handler.bulkAfter();
	        handler.beforeInsert(null);
	        handler.beforeDelete(null);
	        handler.afterInsert(null);
	        handler.afterUpdate(null, null);
	        handler.afterDelete(null);
	        handler.afterUndelete(null);
	        System.assert(true);
    	 }
        
    }
}