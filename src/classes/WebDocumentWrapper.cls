/**
 * This is a wrapper class for the Object Web_Document__c and implement the Comparable interface
 * This class is used for sorting List of Web_Document__c based on LastModifiedDate
 * 
 * Created by: Lina
 * Created on: 31/05/2016
 */

global class WebDocumentWrapper implements Comparable {
    
    public Web_Document__c doc;
    
    // Constructor
    public WebDocumentWrapper(Web_Document__c webDoc) {
        doc = webDoc;
    }
    
    // Compare web documents based on the last modified date
    global Integer compareTo(Object compareTo) {
        // Cast argument to WebDocumentWrapper
        WebDocumentWrapper compareToDoc = (WebDocumentWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (doc.LastModifiedDate > compareToDoc.doc.LastModifiedDate) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (doc.LastModifiedDate < compareToDoc.doc.LastModifiedDate) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        return returnValue;          
    }
}