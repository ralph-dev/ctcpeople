public with sharing class IndeedPostingController {
	public Advertisement__c indeedAd;
	public String namespacePrefix{set;get;}
	public Advertisement__c adTemplate{get ;set;}
	
	public Boolean isEdit{get; set;}
	public Boolean isClone{get; set;}
	public Boolean isIndeedAccountEnable{get;set;}
	
	public String action;
	public String vacancyid;
	
	/*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}    
    public List<SelectOption> selectSkillGroupList {get; set;}
    /*************End Add skill parsing param********************************************************/
    
    /*************Generate Indeed Posting Country option********************************************/
    public String pickListFieldsString; //Picklist Json String
    public List<SelectOption> indeedjobcountrySelectOption {get; set;}
    public List<SelectOption> indeedJobTypeSelectOption {get;set;}
    public List<SelectOption> indeedSponsordAmountOption {get;set;}
    public List<SelectOption> indeedScreeningQuestionOption {get; set;}

    //Init Error Message for customer
    public Integer msgcode{get;set;}
    public PeopleCloudErrorInfo errormesg; 
    public Boolean msgSignalhasmessageIndeed{get ;set;} //show error message or not
    
    
    
    
	public IndeedPostingController(ApexPages.StandardController con) {
	    isEdit = false;
		isClone = false;
		isIndeedAccountEnable = false;
		
	    pickListFieldsString = LinkedInPostUtil.getPickListFieldMapJsonString();
		namespacePrefix = PeopleCloudHelper.getPackageNamespace();
	
	    action = ApexPages.currentPage().getParameters().get('action');
	    if(action!=null&&action!=''&&action.equalsignorecase('edit')){
	    	isEdit = true;
	    }
        
        if(action!=null&&action!=''&&action.equalsignorecase('clone')){
	    	isClone = true;
	    }
	    //vacancyid = ApexPages.currentPage().getParameters().get('vid');
	    
	    //Init skill parsing param. Add by Jack on 23/05/2013
   		enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
        enableSkillGroupList = new List<Skill_Group__c>();
        enableSkillGroupList = SkillGroups.getSkillGroups();
        defaultskillgroups = new List<String>();
        //system.debug('enableskillparsing ='+ enableskillparsing);
        //end skill parsing param.
        
		//the advertisement Id
		String adId = con.getId();
		//get the ad template details
		Set<String> ExcludeFieldsForJobPosting = new Set<String>();//Fields exclude for job posting
		adTemplate = DaoAdvertisement.getAdvertisementById(adId, ExcludeFieldsForJobPosting);

		if(adTemplate.Salary_Description__c == null || adTemplate.Salary_Description__c == '') {
			adTemplate.Salary_Description__c = '$'+adTemplate.Salary_Min__c + ' - $'+adTemplate.Salary_Max__c;
		}

		if(adTemplate.Skill_Group_Criteria__c!=null&&adTemplate.Skill_Group_Criteria__c!=''){
			String defaultskillgroupstring = adTemplate.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
			defaultskillgroups = defaultskillgroupstring.split(',');
		}
		if(adTemplate.Country__c == null || adTemplate.Country__c == ''){
			adTemplate.Country__c = LinkedInPostUtil.getUserLinkedInCounry(pickListFieldsString);
		}
	}
	
	public void initSelectOption(){
	    isIndeedAccountEnable = IndeedPostUtil.checkIndeedAccountEnable();

	    if(isIndeedAccountEnable) {


		    LinkedInPostUtil tempLinkedInPostUtil = new LinkedInPostUtil();
	        tempLinkedInPostUtil.getLinkedInPostUtil(pickListFieldsString);
	        indeedjobcountrySelectOption = tempLinkedInPostUtil.linkedinjobcountrySelectOption;
	        indeedJobTypeSelectOption = tempLinkedInPostUtil.linkedinJobTypeSelectOption;
	        indeedSponsordAmountOption = IndeedPostUtil.GetIndeedSponsoredSelect();
	        indeedScreeningQuestionOption = IndeedPostUtil.getIndeedScreeingQuestionSelect(UserInformationCon.getUserDetails.Indeed_Account__c);

	        selectSkillGroupList = new List<SelectOption>();
			selectSkillGroupList = JobBoardUtils.getSelectSkillGroupList();
		}else {
			customerErrorMessage(PeopleCloudErrorInfo.INDEED_JOBBOARD_ACCOUT);
		} 
	}
	
	

	public PageReference savead(){
        
        system.debug('isEdit ='+isEdit);
	    
        
        if(isEdit){
			return updateAd();
		}
		
		
		indeedAd = adTemplate.clone(false, true);
		if(!isEdit) {
			indeedAd.Status__c = 'Active';
			indeedAd.Job_Posting_Status__c = 'Ready To Post';
		}
		indeedAd.WebSite__c = 'Indeed';
		indeedAd.Advertisement_Account__c = UserInformationCon.getUserDetails.Indeed_Account__c;
		//indeedAd.Vacancy__c = vacancyid;
		indeedAd.RecordTypeId = JobBoardUtils.getRecordId('Indeed');
		if(indeedAd.Indeed_Question__c != null && indeedAd.Indeed_Question__c != '') {
			String urlEndpoint = Endpoint.getcpewsEndpoint() + '/IndeedQuestionForm/questions/' + UserInfo.getOrganizationId() + '/'  +indeedAd.Indeed_Question__c;
			indeedAd.Indeed_Question_URL__c = urlEndpoint;
		}
		//Add Skill Group to linkedIn Ad
		if(enableskillparsing){
			String tempstring = JobBoardUtils.removeListTrim(defaultskillgroups);
	        indeedAd.Skill_Group_Criteria__c = LinkedInPostUtil.removeMultipicklistSquare(tempstring);
	    }
	    try{
	    	if(!Test.isRunningTest()){
	    		checkFLS();
	    	}
			insert indeedAd;
			IndeedAdXMLWriter indeedAdXML = new IndeedAdXMLWriter();
			indeedAdXML.setAd(indeedAd);
			indeedAd.JobXML__c = indeedAdXML.writeXML();
			if(!Test.isRunningTest()){
				checkFLS();
			}
			update indeedAd;
			//String successreturnurl = URL.getSalesforceBaseUrl().toExternalForm() +'/'+ indeedAd.id;
			String extraerror = '! Please click<a onclick="goToRec(\''+indeedAd.id+'\')" style="font-size:12px;">Here</a>to check this ad';
			customerErrorMessage(PeopleCloudErrorInfo.SAVE_AD_SUCCESS, extraerror);
		}catch (Exception e) {
			customerErrorMessage(PeopleCloudErrorInfo.SAVE_AD_FAILED);
		}
		return null;
	}

	//Update LinkedIn Ad
	public PageReference updateAd(){
		system.debug('---------------updatead');
		
		
		
		indeedad = adTemplate;
		if(enableskillparsing){			
			String tempstring = JobBoardUtils.removeListTrim(defaultskillgroups);
	        indeedad.Skill_Group_Criteria__c = LinkedInPostUtil.removeMultipicklistSquare(tempstring);
	    }
	    /*
        if(indeedad.Advertisement_Account__c == null ){
        	String indeedA = UserInformationCon.getUserDetails.Indeed_Account__c;
        	if(indeedA != null ){
        		indeedad.Advertisement_Account__c = indeedA;
        	}
            
        }*/
        
		try{
			if(!Test.isRunningTest()){
				checkFLS();
			}
			update indeedad;
			IndeedAdXMLWriter indeedAdXML = new IndeedAdXMLWriter();
			indeedAdXML.setAd(indeedAd);
			indeedAd.JobXML__c = indeedAdXML.writeXML();
			if(!Test.isRunningTest()){
				checkFLS();
			}
			update indeedAd;
			customerErrorMessage(PeopleCloudErrorInfo.SAVE_AD_SUCCESS);
		}catch(Exception e){
			customerErrorMessage(PeopleCloudErrorInfo.SAVE_AD_FAILED);
		}
		return null;
	}

	public PageReference postad() {
		
	    system.debug('isEdit ='+isEdit);
	    
	   
		
		if(isEdit) {
			indeedad = adTemplate;
			indeedAd.Status__c = 'Active';
			indeedAd.Job_Posting_Status__c = 'Posted Successfully';
			indeedAd.Placement_Date__c = system.Today();
			indeedAd.Ad_Closing_Date__c = system.Today() + 119;
			system.debug('Job_Posting_Status__c ='+ indeedAd.Job_Posting_Status__c);
			if(enableskillparsing){			
				String tempstring = JobBoardUtils.removeListTrim(defaultskillgroups);
	        	indeedad.Skill_Group_Criteria__c = LinkedInPostUtil.removeMultipicklistSquare(tempstring);								
			}

			if(indeedAd.Indeed_Question__c != null && indeedAd.Indeed_Question__c != '') {
				String urlEndpoint = Endpoint.getcpewsEndpoint() + '/IndeedQuestionForm/questions/' + UserInfo.getOrganizationId() + '/' +indeedAd.Indeed_Question__c;
				indeedAd.Indeed_Question_URL__c = urlEndpoint;
			}
			if(!Test.isRunningTest()){
				checkFLS();
			}
			update indeedAd;
		} else {
			indeedAd = adTemplate.clone(false, true);
			indeedAd.Status__c = 'Active';
			indeedAd.Job_Posting_Status__c = 'Posted Successfully';
			indeedAd.WebSite__c = 'Indeed';
			indeedAd.Advertisement_Account__c = UserInformationCon.getUserDetails.Indeed_Account__c;
			//indeedAd.Vacancy__c = vacancyid;
			indeedAd.RecordTypeId = JobBoardUtils.getRecordId('Indeed');
			indeedAd.Placement_Date__c = system.Today();
			indeedAd.Ad_Closing_Date__c = system.Today() + 119;

			if(enableskillparsing){			
				String tempstring = JobBoardUtils.removeListTrim(defaultskillgroups);
	        	indeedad.Skill_Group_Criteria__c = LinkedInPostUtil.removeMultipicklistSquare(tempstring);
			}

			if(indeedAd.Indeed_Question__c != null && indeedAd.Indeed_Question__c != '') {
				String urlEndpoint = Endpoint.getcpewsEndpoint() + '/IndeedQuestionForm/questions/' + UserInfo.getOrganizationId() + '/' +indeedAd.Indeed_Question__c;
				indeedAd.Indeed_Question_URL__c = urlEndpoint;
			}
			checkFLS();
			insert indeedAd;
		}
		
		//system.debug('Ad in controller + ' + indeedAd.Advertisement_Account__c);
		IndeedAdXMLWriter indeedAdXML = new IndeedAdXMLWriter();
		indeedAdXML.setAd(indeedAd);
		indeedAd.JobXML__c = indeedAdXML.writeXML();
		checkFLS();
		update indeedAd;
		//String successreturnurl = URL.getSalesforceBaseUrl().toExternalForm() +'/'+ indeedAd.id;
		String extraerror = '! Please click<a onclick="goToRec(\''+indeedAd.id+'\')" style="font-size:12px;">Here</a>to check this ad';
		customerErrorMessage(PeopleCloudErrorInfo.INDEED_JOB_POST_SUCCESS , extraerror);
		return null;
	}

	public void customerErrorMessage(Integer msgcode){
   		errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        	msgSignalhasmessageIndeed = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
            	ApexPages.addMessage(newMessage);
            }               
    }
    
    public void customerErrorMessage(Integer msgcode, String extraError){
   		errormesg = new PeopleCloudErrorInfo(msgcode,extraError,true);                                     
        	msgSignalhasmessageIndeed = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
            	ApexPages.addMessage(newMessage);
            }               
    }
    
    public PageReference customerErrorMessage(){
    	system.debug('msgcode ='+ msgcode);
   		errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        	msgSignalhasmessageIndeed = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
            	ApexPages.addMessage(newMessage);
            } 
        return null;              
    }

    private void checkFLS(){
      List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
		    Advertisement__c.Ad_Closing_Date__c,
		    Advertisement__c.Placement_Date__c,
		    Advertisement__c.Advertisement_Account__c,
		    Advertisement__c.Indeed_Question_URL__c,
		    Advertisement__c.Vacancy__c,
		    Advertisement__c.Skill_Group_Criteria__c,
		    Advertisement__c.Job_Type__c,
		    Advertisement__c.Job_Title__c,
		    Advertisement__c.JXT_Short_Description__c,
		    Advertisement__c.Bullet_1__c,
		    Advertisement__c.Bullet_2__c,
		    Advertisement__c.Bullet_3__c,
		    Advertisement__c.Job_Content__c,
		    Advertisement__c.Job_Contact_Name__c,
		    Advertisement__c.Company_Name__c,
		    Advertisement__c.Nearest_Transport__c,
		    Advertisement__c.Residency_Required__c,
		    Advertisement__c.IsQualificationsRecognised__c,
		    Advertisement__c.Location_Hide__c,
		    Advertisement__c.TemplateCode__c,
		    Advertisement__c.AdvertiserJobTemplateLogoCode__c,
		    Advertisement__c.ClassificationCode__c,
		    Advertisement__c.SubClassificationCode__c,
		    Advertisement__c.Classification2Code__c,
		    Advertisement__c.SubClassification2Code__c,
		    Advertisement__c.WorkTypeCode__c,
		    Advertisement__c.SectorCode__c,
		    Advertisement__c.Street_Adress__c,
		    Advertisement__c.Search_Tags__c,
		    Advertisement__c.CountryCode__c,
		    Advertisement__c.LocationCode__c,
		    Advertisement__c.AreaCode__c,
		    Advertisement__c.Salary_Type__c,
		    Advertisement__c.SeekSalaryMin__c,
		    Advertisement__c.SeekSalaryMax__c,
		    Advertisement__c.Salary_description__c,
		    Advertisement__c.No_salary_information__c,
		    Advertisement__c.Has_Referral_Fee__c,
		    Advertisement__c.Referral_Fee__c,
		    Advertisement__c.Terms_And_Conditions_Url__c,
		    Advertisement__c.Status__c,
		    Advertisement__c.Job_Posting_Status__c,
		    Advertisement__c.JobXML__c,
		    Advertisement__c.Prefer_Email_Address__c,
		    Advertisement__c.RecordTypeId,
		    Advertisement__c.Website__C
      };

      fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
      fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    } 
}