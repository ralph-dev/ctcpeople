/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * 
 * This test class is created for testing CandidateDispatcher
 */
@isTest
private class TestCandidateDispatcher {

	/**
		Test for scenario where inserted candidates don't exceed the limit on number 
		of candidates a candidate pool account can link to.
		
		'Default Name Of Candidate Pool' in PCAppSwitch should be set to empty
	*/

    static {
        TriggerHelper.disableAllTrigger();
    }

    static testMethod void candPoolContactUnderLimit() {
    	
    	
    	TriggerHelper.UpdateAccountField=false;
    	
    	Map<Id, SObject> insertedRecs = new Map<Id,SObject>();
    	RecordType candPoolRT = DaoRecordType.candidatePoolRT;
    	RecordType candRT = DaoRecordType.indCandidateRT;
    	RecordType conCandRT = DaoRecordType.corpCandidateRT;
    	
        Account candPool = new Account(Name='Candidate Pool Test',RecordTypeId=candPoolRT.Id);
        Account randomAcc = new Account(Name='Company 1');
        
        insert candPool;
        insert randomAcc;
        insertedRecs.put(candPool.id,candPool);
        insertedRecs.put(randomAcc.id,randomAcc);
        CandidateDispatcher cd=new CandidateDispatcher(candRT.id);
        
        List<Contact> conList = new List<Contact>(); 
        for(Integer i=1;i<=cd.getContactLimit();i++){
       		Contact cand = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand1@testcanddispatcher.ctc.test',Candidate_From_Web__c=true);
        	conList.add(cand);
        }
        
        // Client contact
        Contact cand = new Contact(RecordTypeId=conCandRT.Id,LastName='candidate corp cand',Email='Cand1@testcanddispatcher.ctc.test',Candidate_From_Web__c=true, AccountId=randomAcc.Id);
        // Candidate from specific company
        Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate randomAcc',Email='Cand1@testcanddispatcher.ctc.test',Candidate_From_Web__c=true, AccountId=randomAcc.Id);
        
        conList.add(cand);
        conList.add(cand1);
        insert conList;
        Test.startTest();
        cd.dispatchCand(conList);
        for(Integer i=0;i<cd.getContactLimit();i++){
        	Contact con = conList.get(i);
        	system.debug('linked account info, should be candidate pool:'+insertedRecs.get(con.AccountId));
        	system.debug('candidate pool info:'+insertedRecs.get(candPool.Id));
        	system.debug('candidate account id:'+con.AccountId);
       		system.assertEquals(con.AccountId, candPool.Id);
        }
        // A contact should not be assigned under candidate pool if it is corp candidate
        Contact con = conList.get(conList.size()-2);        
        system.assertEquals(DaoAccount.getAccsById(con.AccountId).Name, DaoAccount.getAccsById(randomAcc.Id).Name);
        // A candidate should not be assigned under candidate pool if it has been assigned under a non-candidate-pool account
        con = conList.get(conList.size()-1);
        system.debug('non-candidate-pool cand:'+con);     
        system.assertEquals(DaoAccount.getAccsById(con.AccountId).Name, DaoAccount.getAccsById(randomAcc.Id).Name);
        
        Test.stopTest();
    	
    	
    }
    
	/**
		Test for scenario where inserted candidates exceed the limit on number 
		of candidates a candidate pool account can link to.
		
		'Default Name Of Candidate Pool' in PCAppSwitch should be set to empty
	*/
    static testMethod void candPoolContactExceedLimit() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	TriggerHelper.UpdateAccountField=false;
    	
    	Map<Id, SObject> insertedRecs = new Map<Id,SObject>();
    	RecordType candPoolRT = DaoRecordType.candidatePoolRT;
    	RecordType candRT = DaoRecordType.indCandidateRT;
    	RecordType conCandRT = DaoRecordType.corpCandidateRT;
    	
        Account candPool = new Account(Name='Candidate Pool Test',RecordTypeId=candPoolRT.Id);
        Account randomAcc = new Account(Name='Company 1');
        
        insert candPool;
        insert randomAcc;
        insertedRecs.put(candPool.id,candPool);
        insertedRecs.put(randomAcc.id,randomAcc);
        CandidateDispatcher cd=new CandidateDispatcher(candRT.id);
        
        List<Contact> conList = new List<Contact>();
        Integer noOfContacAdded =  cd.getContactLimit()+1;
        
        // Create fist batch of candidates, these candidates should be linked to 'Candidate Pool Test'
        for(Integer i=1;i<=noOfContacAdded;i++){
       		Contact cand = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand1@testcanddispatcher.ctc.test',Candidate_From_Web__c=true);
        	conList.add(cand);
        }        
        Contact cand = new Contact(RecordTypeId=conCandRT.Id,LastName='candidate 1',Email='Cand1@testcanddispatcher.ctc.test',Candidate_From_Web__c=true, AccountId=randomAcc.Id);
        conList.add(cand);
        insert conList;
        
        // Create fist batch of candidates, these candidates should be linked to new candidate pool
        List<Contact> secondBatchCon = new List<Contact>();
        for(Integer i=1;i<=noOfContacAdded;i++){
       		Contact tmp = new Contact(RecordTypeId=candRT.Id,LastName='candidate 2',Email='Cand1@testcanddispatcher.ctc.test',Candidate_From_Web__c=true);
        	secondBatchCon.add(tmp);
        }
        insert secondBatchCon;
        
        Test.startTest();
        // Run CandidateDispatcher and examine the first batch of records
        // they should be linked to 'Candidate Pool Test'
        cd.dispatchCand(conList);
        for(Integer i=0;i<noOfContacAdded;i++){
        	Contact con = conList.get(i);
        	system.debug('linked account info, should be candidate pool:'+insertedRecs.get(con.AccountId));
        	system.debug('candidate pool info:'+insertedRecs.get(candPool.Id));
       		system.assertEquals(con.AccountId, candPool.Id);
        }
        Contact conLastOne = conList.get(conList.size()-1);        
        system.assertEquals(DaoAccount.getAccsById(conLastOne.AccountId).Name, DaoAccount.getAccsById(randomAcc.Id).Name);
        
        // Run CandidateDispatcher and examine the second batch of records
        // they should be linked to a new candidate pool.
        cd.dispatchCand(secondBatchCon);
        for(Integer i=0;i<noOfContacAdded;i++){
        	Contact con = secondBatchCon.get(i);
        	system.debug('linked account info, should be candidate pool:'+insertedRecs.get(con.AccountId));
        	system.debug('candidate pool info:'+insertedRecs.get(candPool.Id));
       		system.assertNotEquals(con.AccountId, candPool.Id);
        }
        // New candidate pool should be named 'Candidate Pool' as the default candidate pool name
        // in PCAppSwitch custom setting has been set as empty.
        system.assertEquals('Candidate Pool',DaoAccount.getAccsById(secondBatchCon.get(0).AccountId).Name);
        Test.stopTest();
    	}
    
    }
    
	/**
		Test for scenario where inserted candidates exceed the limit on number 
		of candidates a candidate pool account can link to. And default name
		for candidate pool has been setup.
		
		'Default Name Of Candidate Pool' in PCAppSwitch should be set to empty
	*/
    static testMethod void candPoolContactExceedLimitAndDefaultNameSetup() {
    	
    	String defaultNameForTest = 'Test default name';
    	TriggerHelper.UpdateAccountField=false;
    	PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
    	cs.Default_Name_Of_Candidate_Pool__c = defaultNameForTest;
    	insert cs;
    	
    	
    	Map<Id, SObject> insertedRecs = new Map<Id,SObject>();
    	RecordType candPoolRT = DaoRecordType.candidatePoolRT;
    	RecordType candRT = DaoRecordType.indCandidateRT;
    	RecordType conCandRT = DaoRecordType.corpCandidateRT;
    	
        Account candPool = new Account(Name='Candidate Pool Test',RecordTypeId=candPoolRT.Id);
        Account randomAcc = new Account(Name='Company 1');
        
        insert candPool;
        insert randomAcc;
        insertedRecs.put(candPool.id,candPool);
        insertedRecs.put(randomAcc.id,randomAcc);
        CandidateDispatcher cd=new CandidateDispatcher(candRT.id);
        
        List<Contact> conList = new List<Contact>();
        Integer noOfContacAdded =  cd.getContactLimit()+1;
        
        // Create fist batch of candidates, these candidates should be linked to 'Candidate Pool Test'
        for(Integer i=1;i<=noOfContacAdded;i++){
       		Contact cand = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand1@testcanddispatcher.ctc.test',Candidate_From_Web__c=true);
        	conList.add(cand);
        }        
        Contact cand = new Contact(RecordTypeId=conCandRT.Id,LastName='candidate 1',Email='Cand1@testcanddispatcher.ctc.test',Candidate_From_Web__c=true, AccountId=randomAcc.Id);
        conList.add(cand);
        insert conList;
        
        // Create fist batch of candidates, these candidates should be linked to new candidate pool
        List<Contact> secondBatchCon = new List<Contact>();
        for(Integer i=1;i<=noOfContacAdded;i++){
       		Contact tmp = new Contact(RecordTypeId=candRT.Id,LastName='candidate 2',Email='Cand1@testcanddispatcher.ctc.test',Candidate_From_Web__c=true);
        	secondBatchCon.add(tmp);
        }
        insert secondBatchCon;
        
        Test.startTest();
        // Run CandidateDispatcher and examine the first batch of records
        // they should be linked to 'Candidate Pool Test'
        cd.dispatchCand(conList);
        for(Integer i=0;i<noOfContacAdded;i++){
        	Contact con = conList.get(i);
        	system.debug('linked account info, should be candidate pool:'+insertedRecs.get(con.AccountId));
        	system.debug('candidate pool info:'+insertedRecs.get(candPool.Id));
       		system.assertEquals(con.AccountId, candPool.Id);
        }
        Contact conLastOne = conList.get(conList.size()-1);        
        system.assertEquals(DaoAccount.getAccsById(conLastOne.AccountId).Name, DaoAccount.getAccsById(randomAcc.Id).Name);
        
        // Run CandidateDispatcher and examine the second batch of records
        // they should be linked to a new candidate pool.
        cd.dispatchCand(secondBatchCon);
        for(Integer i=0;i<noOfContacAdded;i++){
        	Contact con = secondBatchCon.get(i);
        	system.debug('linked account info, should be candidate pool:'+insertedRecs.get(con.AccountId));
        	system.debug('candidate pool info:'+insertedRecs.get(candPool.Id));
       		system.assertNotEquals(con.AccountId, candPool.Id);
        }
        // New candidate pool should be named 'Candidate Pool' as the default candidate pool name
        // in PCAppSwitch custom setting has been set as empty.
        system.assertEquals(defaultNameForTest,DaoAccount.getAccsById(secondBatchCon.get(0).AccountId).Name);
        Test.stopTest();
    	
    	
    
    } 
    
    /**
    	Test when existing envrionment doesn't have candidate pool, and inserted contacts only
    	contain non-candidate type record, CandidateDispatcher shouldn't create any candidate pool. 
    */
    static testMethod void candPoolNoCandidatesNeedToAssignCandidatePool() {
    	
    	String defaultNameForTest = 'Test default name';
    	TriggerHelper.UpdateAccountField=false;
    	PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
    	cs.Default_Name_Of_Candidate_Pool__c = defaultNameForTest;
    	cs.Protect_Candidate_Pool__c = false;
    	insert cs;
    	
    	
    	List<Contact> candidates2Dispatch = new List<Contact>();
    	Account randomAcc = new Account(Name='Company 1');
    	insert randomAcc;
    	
    	RecordType conCandRT = DaoRecordType.corpCandidateRT;
    	RecordType candRT = DaoRecordType.indCandidateRT;
    	Contact contact = new Contact(RecordTypeId=conCandRT.Id,LastName='contact 1',Email='contact1@testcanddispatcher.ctc.test',AccountId=randomAcc.Id);
    	candidates2Dispatch.add(contact);
    	insert candidates2Dispatch;
    	CandidateDispatcher cd=new CandidateDispatcher(candRT.id);
    	 
    	
    	Test.startTest();
    	RecordType candidatePool = DaoRecordType.candidatePoolRT;
    	List<Account> candidatePools = [select Id,Name from Account where RecordTypeId = :candidatePool.Id];
    	if(candidatePools.size()>0){
    		
    		delete candidatePools;
    	}
    		
    	cd.dispatchCand(candidates2Dispatch);
    	
    	Integer noOfCandidatePools = [select count() from Account where RecordTypeId = :candidatePool.Id];
    	system.assert(noOfCandidatePools == 0);
    	Test.stopTest();
    	
    	
    }
}