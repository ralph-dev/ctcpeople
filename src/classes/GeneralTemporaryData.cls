public with sharing class GeneralTemporaryData {
	
	//General Temporary Data For Screen Candidate
	public static Temporary_Data__c generalTemporaryDataForScreenCandidate(List<String> contactList, Map<String, boolean> checkthisrecords){
		Temporary_Data__c returnresult = new Temporary_Data__c();
		String startTimeStamp = ''+Datetime.now().getTime();
		String tempName = 'TD_SC_'+UserInfo.getUserId()+'_'+startTimeStamp;
		String dataContent = JSON.serialize(contactList);
		String secondaryDataContent = JSON.serialize(checkthisrecords);
		try{
			DaoTemporaryData daoTempData = new DaoTemporaryData();
			returnresult = daoTempData.saveScreenCandidateTemporaryData(tempName , dataContent, secondaryDataContent);
		}catch(Exception e){
			returnresult = null;
		}		
		return returnresult;
	}
}