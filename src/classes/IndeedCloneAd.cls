public with sharing class IndeedCloneAd {
	private ApexPages.StandardController stdCon;
	public String adtemplateid; //Tempory ad id
	//Init Error Message for customer
    public PeopleCloudErrorInfo errormesg; 
    public Boolean msgSignalhasmessage{get ;set;} //show error message or not
    
	public IndeedCloneAd(ApexPages.StandardController stdController){
		stdCon = stdController;
		Advertisement__c adTemplate = (Advertisement__c) stdController.getRecord();
		adtemplateid = adTemplate.id;
	}
	
	//Clone ad
	public PageReference cloneAd(){ 
		String whereClause = 'id =\''+adtemplateid +'\'';
        String ExcludeFieldsString = 'CreatedDate, CreatedById, OwnerId,'+ PeopleCloudHelper.getPackageNamespace()+'Application_URL__c'+','+PeopleCloudHelper.getPackageNamespace()+'Placement_Date__c';//Not Colon Field list string
        Set<String> ExcludeFieldsSet = new Set<String>();
        ExcludeFieldsSet = GetfieldsMap.getExcludeFieldsForJobPosting(ExcludeFieldsString);//fetch the Exclude Field set
        //system.debug('ExcludeFieldsSet ='+ ExcludeFieldsSet);
        
        /**
		* already did checkread in GetfieldsMap.getCreatableFieldsSOQL method
		*/
        String queryString = GetfieldsMap.getCreatableFieldsSOQL(PeopleCloudHelper.getPackageNamespace()+'Advertisement__c', ExcludeFieldsSet, whereClause);
        try{
        	Advertisement__c ad = database.query(queryString);
        	//system.debug('ad =' + ad);
	        Advertisement__c newAd = ad.clone(false, true);
	        newAd.Status__c = 'Active';
	        newAd.Job_Posting_Status__c = 'Clone to Post';
          checkFLS();
	        insert newAd;
	        PageReference editPage = Page.IndeedPost;
	        editPage.getParameters().put('id', newAd.id);
	        editpage.getParameters().put('action','clone');//The action is about clone ad not update
	        //system.debug('editpage = '+ editpage.getUrl());
	        return editPage;
        }catch(Exception e){
        	customerErrorMessage(PeopleCloudErrorInfo.CLONE_AD_ERROR); 
            return null;
        }
	}
	
	//Cancel clone ad
	public PageReference cancelClone(){
    	return stdCon.view();
    }
    
    public void customerErrorMessage(Integer msgcode){
   		errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        	msgSignalhasmessage = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
            	ApexPages.addMessage(newMessage);
            }               
    }

    private void checkFLS(){
      List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
          Advertisement__c.Company_Description__c,
          Advertisement__c.Country__c,
          Advertisement__c.Industry__c,
            Advertisement__c.Application_URL__c,
            Advertisement__c.ListingID__c,
            Advertisement__c.Ad_Closing_Date__c,
            Advertisement__c.Placement_Date__c,
            Advertisement__c.Advertisement_Account__c,
            Advertisement__c.Indeed_Question_URL__c,
            Advertisement__c.Vacancy__c,
            Advertisement__c.Skill_Group_Criteria__c,
            Advertisement__c.Job_Type__c,
            Advertisement__c.Job_Title__c,
            Advertisement__c.JXT_Short_Description__c,
            Advertisement__c.Bullet_1__c,
            Advertisement__c.Bullet_2__c,
            Advertisement__c.Bullet_3__c,
            Advertisement__c.Job_Content__c,
            Advertisement__c.Job_Contact_Name__c,
            Advertisement__c.Company_Name__c,
            Advertisement__c.Nearest_Transport__c,
            Advertisement__c.Residency_Required__c,
            Advertisement__c.IsQualificationsRecognised__c,
            Advertisement__c.Location_Hide__c,
            Advertisement__c.TemplateCode__c,
            Advertisement__c.AdvertiserJobTemplateLogoCode__c,
            Advertisement__c.ClassificationCode__c,
            Advertisement__c.SubClassificationCode__c,
            Advertisement__c.Classification2Code__c,
            Advertisement__c.SubClassification2Code__c,
            Advertisement__c.WorkTypeCode__c,
            Advertisement__c.SectorCode__c,
            Advertisement__c.Street_Adress__c,
            Advertisement__c.Search_Tags__c,
            Advertisement__c.CountryCode__c,
            Advertisement__c.LocationCode__c,
            Advertisement__c.AreaCode__c,
            Advertisement__c.Salary_Type__c,
            Advertisement__c.SeekSalaryMin__c,
            Advertisement__c.SeekSalaryMax__c,
            Advertisement__c.Salary_description__c,
            Advertisement__c.No_salary_information__c,
            Advertisement__c.Has_Referral_Fee__c,
            Advertisement__c.Referral_Fee__c,
            Advertisement__c.Terms_And_Conditions_Url__c,
            Advertisement__c.Status__c,
            Advertisement__c.Job_Posting_Status__c,
            Advertisement__c.JobXML__c,
            Advertisement__c.Prefer_Email_Address__c,
            Advertisement__c.RecordTypeId,
            Advertisement__c.Website__C
      };

      fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);

    } 
}