@isTest
private with sharing class TaskExtensionTest {

    private static testMethod void getTop5TasksByContactIdTest() {
        
        
        Account acc = TestDataFactoryRoster.createAccount();
        
        Contact con = TestDataFactoryRoster.createContact();
        
        Profile p = [SELECT Id FROM Profile].get(0);
        
        User u = DataTestFactory.createUser();
        u.profileId = p.Id;
        insert u;
        
        
        System.runAs(DummyRecordCreator.platformUser) {
        List<Task> tList = new List<Task>();
        for(integer i=0; i<100; i++) {
            Task t = new Task();
            t.whoId = con.Id;
            t.OwnerId = u.Id;
            t.subject = 'test subject' + i;
            tList.add(t);
        }
        insert tList;

        List<Task> tTop5 = TaskExtension.getTop5TasksByContactId(con.Id);
        system.assertEquals(tTop5.size(),5);
        system.assert(TaskExtension.apiDescriptor().size()>0);
         }
        
    }
    
    private static testMethod void getSkillGroupsTest() {
        DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
        
        System.runAs(DummyRecordCreator.platformUser) {
        	
	        List<SkillGroups.selectedSkillGroup> selectedSkillGroupS = TaskExtension.getSkillGroups();
	        system.assert(selectedSkillGroupS.size()>0);
	    }
    }
    
    private static testMethod void getDocTypePickListTest() {
        
        DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
        
        System.runAs(DummyRecordCreator.platformUser) {
        	
            List<SelectOption> docTypeList = TaskExtension.getDocTypePickList();
            system.assert(docTypeList.size()>0);
        }
    }
    
    private static testMethod void getTaskInfoTest() {
        
        DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
        ActivityDummyRecordCreator dummyCreator = new ActivityDummyRecordCreator();
        Task dummyTask = dummyCreator.generateOneActivityDummyRecord(rs);
        System.runAs(DummyRecordCreator.platformUser) {
            
	        
            Task t = TaskExtension.getTaskInfo(dummyTask.Id);
            system.assert(t!=null);
        }
    }
    
    // private static testMethod void getActivityDocsTest() {
        
    //     DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
    //     ActivityDummyRecordCreator dummyCreator = new ActivityDummyRecordCreator();
    //     Task dummyTask = dummyCreator.generateOneActivityDummyRecord(rs);
    //     List<ContentDocumentLink> files = FileDummyRecordCreator.createFilesForActivity(dummyTask);
    //     System.runAs(DummyRecordCreator.platformUser) {
            
    //         List<ActivityDoc> activityDocs = TaskExtension.getActivityDocs(dummyTask.Id);
    //         system.assert(activityDocs.size()>0);
    //     }
    // }

}