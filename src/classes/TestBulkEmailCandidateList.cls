@isTest
private class TestBulkEmailCandidateList {
    
    public static testMethod void testExtractObjectFieldsFromTemplate(){
        Map<String, Set<String>> relatedObjectFields = BulkEmailCandidateList.extractObjectFieldsFromTemplate('{!Contact.Name} {!Contact.zzz}{!Account.xxx},,,{!Placement__c.Name}');
        
        System.assert(relatedObjectFields.get('CONTACT') != null );
        System.assertEquals(2, relatedObjectFields.get('CONTACT').size() );
        System.assertEquals(1, relatedObjectFields.get('ACCOUNT').size() );
        System.assertEquals(1, relatedObjectFields.get('PLACEMENT__C').size() );
        
        BulkEmailCandidateList be = new BulkEmailCandidateList();
        Map<String,String> params = new Map<String,String>();
        params.put('Contact','12345');
        params.put('Placement__c','99999');
        be.setInitParams(params);
        be.setRelatedObjectFields(relatedObjectFields);
        String xml = be.createRelatedObjectsXmlContent();
        System.debug(xml);
        System.assert(xml.contains('99999'));
        System.assert(xml.contains('12345'));
        System.assert(xml.contains('Name'));
        
    }
    public static testmethod void testTestBulkEmailCandidateList(){
        System.runAs (DummyRecordCreator.platformUser) {
        	Test.startTest();
		    String docid  = TestBulkEmailCandidateList.tempXMLdocument();   
		    String setUpDocId = TestBulkEmailCandidateList.tempSetupPageDoc(); 
		     
		    Test.setCurrentPage(Page.bulkEmailCandidateListPage);
		    ApexPages.currentPage().getParameters().put('APSCandidateXMLDocument', docid);
		    ApexPages.currentPage().getParameters().put('APSSearchCriteriaSetUpDocument', setUpDocId);
		    ApexPages.currentPage().getParameters().put('sourcepage','test');
		    BulkEmailCandidateList thisBulkEmailCandidateList = new BulkEmailCandidateList();
		    
		    thisBulkEmailCandidateList.init();
		    system.assert(thisBulkEmailCandidateList.ContactShowupList!=null);
		    system.assert(thisBulkEmailCandidateList.TotalBulkEmailConRowRec.size()>0);
		    thisBulkEmailCandidateList.nextPageOfcands();
		    thisBulkEmailCandidateList.prevPageOfCands();
		    
		    PageReference r1 = thisBulkEmailCandidateList.editEmailTemplate();
		    system.assert(r1 != null);
		    thisBulkEmailCandidateList.createEmailPlayload();
		    system.assert(thisBulkEmailCandidateList.candidateListXMLDocumentId != '');
		    
		    
		    
		    system.assert(thisBulkEmailCandidateList.returnurl != null);
		    
		    EmailTemplate emailTemplateWithAttachments = TestBulkEmailCandidateList.createDummyEmailTemplateWithAttachments();
		    thisBulkEmailCandidateList.selectedTempId = emailTemplateWithAttachments.id;
		    PageReference r3 = thisBulkEmailCandidateList.previewTemp();
		    system.assert(thisBulkEmailCandidateList.htmlTemplateContent != null);
		    
		    PageReference r4 = thisBulkEmailCandidateList.createDocuments();
		    system.assert(thisBulkEmailCandidateList.sendOutTemp != null); 
		    
		    PageReference r5 = thisBulkEmailCandidateList.sendBulkEmail();  
		    system.assert(thisBulkEmailCandidateList.msgSignalhasmessage == true); 
		    
		    Test.stopTest();
        }    
    }
    
    // This is a test method to test send bulk email button on Contact list view. 
    static testmethod void testSBECandListForContactListView() {
        // Create data
        System.runAs (DummyRecordCreator.platformUser) {
            Test.setCurrentPage(Page.bulkEmailCandidateListPage);
            Map<String,String> params = ApexPages.currentPage().getParameters();
            params.put('returnurl' , 'returnURL');
    		params.put('sourcepage', 'peopleListView');
            List<Contact> selectedCon = new List<Contact>();
            RecordType candRT = DaoRecordType.indCandidateRT;
            Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand1@test.ctc.test');
            Contact cand2 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 2',Email='Cand2@test.ctc.test');
            Contact cand3 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 3',Email='Cand3@test.ctc.test');
            selectedCon.add(cand1);
            selectedCon.add(cand2);
            selectedCon.add(cand3);
            Test.startTest();
            insert selectedCon;
            Placement__c vacancy = new Placement__c(Name='Test Vacancy');
            insert vacancy;
            List<String> candIdList = new List<String>();
            candIdList.add(cand1.Id);
            candIdList.add(cand2.Id);
            candIdList.add(cand3.Id);
            List<Placement_Candidate__c> cmList = DataTestFactory.createCandidateManagementListForSameVacancy((String)vacancy.Id, candIdList);
            String docId = tempXMLdocument2();
            Integer index = 0;
            if(selectedCon != null){
    			for(Contact c:selectedCon){
    				String argKey='arg'+index;
    				params.put(argKey,c.Id);
    				index++;
    			}
    		}
            BulkEmailCandidateList bulkEmailCandList = new BulkEmailCandidateList();
            bulkEmailCandList.init();
            Test.stopTest();
            system.assert(bulkEmailCandList.ContactShowupList!=null);
            system.assert(bulkEmailCandList.TotalBulkEmailConRowRec.size()==3);
            system.assertEquals('<<<<<Back to List of People', bulkEmailCandList.commandlinkvalue);
        }
    }
    
    public static String tempXMLdocument2(){
        String xmlstring = '<?xml version="1.0" encoding="utf-8" ?><pages><results><page>';
        String page = '';
        List<Contact> tempContact = DataTestFactory.createCands();
        for(Contact con : tempContact){
            if(page !='' && page!=null){
                page =  page + ',\'' + con.id + '\'';
            }
            else{
                page = '\'' + con.id + '\'';
            }
        }
        xmlstring += page+'</page></results></pages>';
        String orgId = UserInfo.getOrganizationId();
        Document d = new Document(Type ='xml', body= Blob.valueof(xmlstring), DeveloperName='ClicktoCloud_Xml_Contact_Test'+orgId, name='ClicktoCloud_Xml_Contact_'+orgId, FolderId=DaoFolder.getFolderByDevName('CTC_Admin_Folder').Id);
        insert d;
        return d.id;
    }
    
    public static String tempXMLdocument(){
        String xmlstring = '<?xml version="1.0" encoding="utf-8" ?><pages><results><page>';
        String page = '';
        List<Contact> tempContact = DataTestFactory.createCands();
        for(Contact con : tempContact){
            if(page !='' && page!=null){
                page =  page + ',\'' + con.id + '\'';
            }
            else{
                page = '\'' + con.id + '\'';
            }
        }
        xmlstring += page+'</page></results></pages>';
        Document tempdoc = new Document();
        tempdoc.Name = 'test.xml';
        tempdoc.Body = Blob.valueOf(xmlstring);  
        tempdoc.FolderId = userinfo.getUserId();    
        insert tempdoc;
        return tempdoc.id;
    }
    
    public static String tempSetupPageDoc(){
        String xmlstring = '<?xml version="1.0" encoding="utf-8" ?><fileroot><columns><column><label>First Name</label><obj>EMPTY_STRING</obj><api>FirstName</api><type>STRING(40)</type><sortable>true</sortable></column></columns></fileroot>';
        Document tempSetdoc = new Document();
        tempSetdoc.Name = 'testSet.xml';
        tempSetdoc.Body = Blob.valueOf(xmlstring);  
        tempSetdoc.FolderId = userinfo.getUserId();     
        insert tempSetdoc;
        return tempSetdoc.id;
    }  
    
    
    
    // test codes for editEmailTemplate functions
    public static testmethod void testEditEmailTemplateFunction(){
        System.runAs (DummyRecordCreator.platformUser) {
        	Test.startTest();
	        // test initialization
	        Test.setCurrentPage(Page.editEmailTemplate);
	        BulkEmailCandidateList becl = new BulkEmailCandidateList();
	        BulkEmailCandidateList becl2 = new BulkEmailCandidateList();
	        becl.initEditEmailTemplateFunction();
	        becl2.initEditEmailTemplateFunction();
	        
	        // test preview email template (without attachments)
	        EmailTemplate emailTemplate = TestBulkEmailCandidateList.createDummyEmailTemplate();
	        becl.selectedTempId = emailTemplate.Id;
	        PageReference pr4PreviewTemp = becl.previewTemp();
	        system.assert(pr4PreviewTemp == null);
	        
	        
	        // test preview email template (with attachments)
	        EmailTemplate emailTemplateWithAttachments = TestBulkEmailCandidateList.createDummyEmailTemplateWithAttachments();
	        becl2.selectedTempId = emailTemplateWithAttachments.Id;
	        PageReference pr4PreviewTempWithAttachments = becl2.previewTemp();
	        system.assert(pr4PreviewTempWithAttachments == null);
	        system.assert(!becl2.isAttachmentsFromEmailTemplateEmpty);
	        system.assert(!becl2.isAttachmentsListEmpty);
	        
	        
	        // test add local document/attachment
	        String newDocId = TestBulkEmailCandidateList.createDummyDoc();
	        becl.docsId = newDocId;
	        becl.addlocaldocs();
	        system.assert(becl.optInlocalDocList != null);
	        system.assert(!becl.isLocalDocListEmpty);
	        
	        // test get all attachment IDs
	        List<String> tmpAttachmentIds = new List<String>();
	        tmpAttachmentIds = becl.getAllAttachmentsId();
	        //system.assert(tmpAttachmentIds.size()>0);
	        
	        // test get all document IDs
	        List<String> tmpDocumentIds = new List<String>();
	        tmpDocumentIds = becl.getAllDocumentsId();
	        system.assert(tmpDocumentIds.size()>0);
	        
	        // test get total size
	        Integer tmpTotalSize = becl.getTotalSize();
	        system.assert(tmpTotalSize>0);
	        
	        
	        // test remove documents/attachments
	        becl.dellocalDocId = (becl.docsId).substring((becl.docsId).indexof('=')+1,(becl.docsId).indexof(';'));
	        becl.removeDummyDoc();
	        system.assert(becl.optInlocalDocList.size() == 0);
	        
	        
	        // test going back to advanced people search page
	        becl.returnurl = 'test';
	        //PageReference tmpPage = becl.backToPreviouspage();
	        system.assert(becl.returnurl != null);
	        
	        // test going back to advanced people search page
	        PageReference tmpPage2 = becl.gobackBulkEmailCandidateList();
	        system.assert(tmpPage2 != null);   
	        
	        // test reset email template
	        PageReference tmpPage3 = becl.resetEmailContent();
	        Test.stopTest();
        }
       
    }

    // create dummy email template 
    private static EmailTemplate createDummyEmailTemplate(){
    	EmailTemplate e = new EmailTemplate (developerName = 'test', isactive=true, Body = 'test template', TemplateType= 'Text', Name = 'test', TemplateStyle='none' , FolderId=UserInfo.getUserId());
        insert e;
        EmailTemplate result =  [Select TimesUsed, TemplateType, TemplateStyle, 
                                SystemModstamp, Subject, OwnerId, NamespacePrefix, 
                                Name, Markup, LastUsedDate, LastModifiedDate, 
                                LastModifiedById, IsActive, Id, HtmlValue, 
                                FolderId, Encoding, DeveloperName, Description, 
                                CreatedDate, CreatedById, BrandTemplateId, Body, 
                                ApiVersion From EmailTemplate where (TemplateStyle='none' and TemplateType='text') limit 1];
        return result;
    }
    
    // create dummy document  
    private static String createDummyDoc(){
        Document tempdoc = new Document();
        tempdoc.Name = 'test.doc';
        tempdoc.Body = Blob.valueOf('test');
        tempdoc.FolderId = userinfo.getUserId();
        insert tempdoc;
        MultiAttachmentController newDummydoc = new MultiAttachmentController();
        newDummydoc.myfiles = new List<document>();
        newDummyDoc.myfiles.add(tempdoc);
        newDummyDoc.createDummyDoc();   
        
        return newDummyDoc.optInlocalDocString;
    }
    
    //create dummy email template with attachments
    private static EmailTemplate createDummyEmailTemplateWithAttachments(){
    	EmailTemplate e = new EmailTemplate (developerName = 'test_with_attachment', isactive=true, Body = 'test template', TemplateType= 'Text', Name = 'test', TemplateStyle='none' , FolderId=UserInfo.getUserId());
        insert e;
        
        Attachment a = new Attachment(name='test', body=Blob.valueof('test'), parentId=e.Id);
        insert a;

        EmailTemplate emailTemplate =  [Select TimesUsed, TemplateType, TemplateStyle, 
                                        SystemModstamp, Subject, OwnerId, NamespacePrefix, 
                                        Name, Markup, LastUsedDate, LastModifiedDate, 
                                        LastModifiedById, IsActive, Id, HtmlValue, 
                                        FolderId, Encoding, DeveloperName, Description, 
                                        CreatedDate, CreatedById, BrandTemplateId, Body, 
                                        ApiVersion From EmailTemplate where id =: e.Id];
        return emailTemplate;
    }
    
    
}