@isTest
private class RosterHandlerTest {
	final static String ROSTER_STATUS_FILLED = 'Filled';
	final static String ROSTER_STATUS_PENDING = 'Pending';
	
    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // TO DO: implement unit test
        Account acc = TestDataFactoryRoster.createAccount();
    	Contact con = TestDataFactoryRoster.createContact();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        Days_Unavailable__c roster = TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_FILLED);
        
       	RosterHandler rHandler = new RosterHandler();
       	rHandler.bulkBefore();
       	rHandler.bulkAfter();
        rHandler.beforeInsert(roster);
        rHandler.beforeUpdate(roster, roster);
        rHandler.beforeDelete(roster);
        rHandler.afterInsert(roster);
        rHandler.afterUpdate(roster, roster);
        rHandler.afterDelete(roster);
        rHandler.afterUndelete(roster);
        try {
       	    rHandler.andFinally();
        } catch (Exception e) {
            
        }
        system.assertNotEquals(null, [select id from Shift__c].size() );
    	}
    }
}