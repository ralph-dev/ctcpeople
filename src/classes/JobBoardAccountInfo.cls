/**
* This is the DTO class for job board account and all account related objects
* This DTO is used for frontend to display account selection
*/

public class JobBoardAccountInfo {

    public List<JobBoardAccount> accounts;

    public JobBoardAccountInfo(List<JobBoardAccount> jobBoardAccounts) {
        accounts = jobBoardAccounts;
    }

    public class JobBoardAccount {
        public String id;
        public String name;
        public Boolean isDefault;
        public List<String> availableTypes;
        public List<AccountInfo> templates;
        public List<AccountInfo> logos;
        public List<AccountInfo> screens;
        public JobBoardAccount(String accountId, String accountName, Boolean accountIsDefault) {
            id = accountId;
            name = accountName;
            isDefault = accountIsDefault;
        }
    }

    public class AccountInfo {
        public String sfId;
        public String id;
        public String name;
        public Boolean isDefault;
        public AccountInfo(String recordId, String infoId, String infoName, Boolean infoIsDefault) {
            sfId = recordId;
            id = infoId;
            name = infoName;
            isDefault = infoIsDefault;
        }
    }
}