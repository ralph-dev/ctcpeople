/**
 * This class is the DTO of JXT ad request for web service
 * 
 * Author: Lorena
 * Date: 7/12/2016
 * */
public with sharing class JXTAd {
    
    public String username;
    public String password;
    public String advertiserId;
    
    public String orgId;
    public String referenceNo;
    public String jobBoard;
    public String jobBoardSfApiName;
    public String namespace ;
    public String postingStatus;
    
    public String jobContent;
    
    public JXTAd() {}

}