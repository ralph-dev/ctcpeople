/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestUserInformationCon {

    static testMethod void testGetUserInfo() {
		User user = PeopleCloudHelper.createPlatformUser();
		system.runAs(user){	
			UserInformationCon thecon = new UserInformationCon();
			thecon.getUserDetail(); 
			System.assert(0<thecon.checkquota(1));
			System.assert(0<thecon.checkquota(2));
			System.assert(0<thecon.checkquota(3));
			System.assert(0==thecon.checkquota(0));
			User tempUser = UserInformationCon.getUserDetails;
			System.assertEquals(tempUser.id ,user.id);
			String companyName = UserInformationCon.CompanyName();
			system.assert(companyName!='');
		}
    }

	static testMethod void testCheckRemainingQuota() {
		User u = DummyRecordCreator.platformUser;
		System.runAs(u) {
			u.Seek_Usage__c = String.valueOf(10);
			u.Monthly_Quota_Seek__c = String.valueOf(20);
			u.MyCareer_Usage__c = null;
			u.Monthly_Quota_MyCareer__c = String.valueOf(30);
			u.CareerOne_Usage__c = null;
			u.Monthly_Quota_CareerOne__c = null;

			// Test seek
			System.assertEquals(10, UserInformationCon.checkRemainingQuota('Seek', u));
			// Test for MyCareer
			System.assertEquals(30, UserInformationCon.checkRemainingQuota('MyCareer', u));
			// Test for CareerOne
			System.assertEquals(0, UserInformationCon.checkRemainingQuota('CareerOne', u));
		}
	}
}