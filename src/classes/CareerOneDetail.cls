public with sharing class CareerOneDetail {
    public Advertisement__c job;
    public Advertisement__c newJob;
    public String jobrefcode;
    //get json from safesforce document object.
    public String catOccJson{get; set;}
    public String searchAreasJson{get; set;}
    public String c1industryJSon{get; set;}
    
    public String iid = UserInfo.getOrganizationId().subString(0, 15);
    //public String iid = UserInfo.getOrganizationId();
    public String saveInfo{get; set;}
    public String bucketname ;
    public boolean haserror {get;set;}
    public User u;
    
    //CareerOne Account Detail from StyleCategory
    private String C1Username;
    private String C1CompanyXCode;

    private transient String C1Password;
    private Boolean C1PremiumPackage;
    public Boolean PremiumPackageCheckBox {get; set;} //add by jack for Premium Package
    
    public boolean hasSaved {get;set;}
    public String jobContentC1{get; set;}
    //UPDATES FOR CKEDTOR
    public String onlineFooterC1{get; set;}
    
    public String remining_Quota{get;set;}
    public String numofads{get;set;}
    
    Integer remainingQuota;
    
    public static final String ERR_INVALID_PHY_ADDRESS = 'Your input address is invalid.';
    public static final String CON_SAVE_AD = 'Your job ad has been saved!';
    public static final String CON_POST_AD = 'Your job ad has been post!';
    
    
    //Get Available Template
    public String selectedTemplate{get;set;}
    public List<SelectOption> templateOptions = new List<SelectOption>();
    Map<String,String> template_map = new Map<String,String>();
    public List<SelectOption> getTemplateOptions(){
        return templateOptions;
    }
    
    //Get Available Style
    public String selectedStyle {get;set;}
    public List<SelectOption> styleOptions = new List<SelectOption>();
    Map<Id,StyleCategory__c> style_map = new Map<Id,StyleCategory__c>();

    public List<SelectOption> getStyleOptions(){
        return styleOptions;
    }    
    
    /*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}    
    /*************End Add skill parsing param********************************************************/
    
    //CareerOne Error message
    public PeopleCloudErrorInfo errormesg; 
    public Boolean msgSignalhasmessageC1{get ;set;} //show error message or not
    
    public CareerOneDetail(ApexPages.StandardController stdController){
    	//Init skill parsing param. Add by Jack on 23/05/2013
        enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
        enableSkillGroupList = new List<Skill_Group__c>();
        enableSkillGroupList = SkillGroups.getSkillGroups();
        defaultskillgroups = new List<String>();
        //end skill parsing param.
       job = (Advertisement__c) stdController.getRecord();
       if(job.Skill_Group_Criteria__c!=null&&job.Skill_Group_Criteria__c!=''){
        	String defaultskillgroupstring = job.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
	        defaultskillgroups = defaultskillgroupstring.split(',');
        }
    } 
    
    public void init(){
        hasSaved = false;
        haserror = false;
        PremiumPackageCheckBox = false; //After the Career can use standard and premium together. Set the C1PremiumPackage to PremiumPackageCheckBox
        String errstr = '';
        jobrefcode = '';
        boolean has_websiteacc = true;
        
		UserInformationCon usercls = new UserInformationCon();
        u = usercls.getUserDetail();
         
        if(!Test.isRunningTest()) {
            if(u.AmazonS3_Folder__c != null){
                bucketname = u.AmazonS3_Folder__c.trim();
                }
            else{
                haserror = true;
                errstr = 'Sorry. Your Amazon Folder is not valid for job-posting! Please contact your System Administrator!';              
            }
        }
        
        
        //get CareeerOne Account detail from StyleCategory Object
        //modified by andy for security review II
        //StyleCategory__c[] websiteacc = [select id,Advertiser_Password__c,Advertiser_Name__c,Advertiser_Uploadcode__c,CareerOne_Premium_Package__c from StyleCategory__c where WebSite__c='CareerOne' and Account_Active__c = true and RecordType.DeveloperName like '%WebSite_Admin_Record%' limit 1];
        
        ProtectedAccount pa = new StyleCategorySelector().getCareerOneProtectedAccount();
        if(pa != null){
        	
            C1CompanyXCode =(String) pa.getAccountFieldValue('Advertiser_Uploadcode__c');// ((StyleCategory__c) pa.account).Advertiser_Uploadcode__c;
            C1PremiumPackage = (Boolean) pa.getAccountFieldValue('CareerOne_Premium_Package__c');// ((StyleCategory__c) pa.account).CareerOne_Premium_Package__c;
            
            //modified by andy for security review II
            //C1Username = websiteacc[0].Advertiser_Name__c;
            //C1Password = websiteacc[0].Advertiser_Password__c;
            
            C1Username = pa.getUsername();
            C1Password = pa.getPassword();
           
            
                
            if(C1Username == null || C1CompanyXCode == null || C1Password == null 
                    || C1Username == '' || C1CompanyXCode == '' || C1Password == ''){
                    haserror = true;
                    has_websiteacc = false;
                    //System.debug('account is null: \n' + C1Username +'\n' +C1CompanyXCode +'\n'+ C1Password );
                } 
        }else{   
        	haserror = true;
            has_websiteacc = false;
  
        }
        
        
        if(haserror){
            customerErrorMessage(PeopleCloudErrorInfo.CAREERONE_ACCOUNT_FAILED);
        }else{//Init the dropdown list option
        	/**
        	* modified by andy for security review II
        	*/
        	CommonSelector selector = new CommonSelector(Document.sObjectType);
            //Document doc = [Select d.Id, d.Body From Document d where Name='careerone_cat_occ_json' limit 1];
            Document doc =(Document) selector.getOne('Id, Body','Name=\'careerone_cat_occ_json\'',1);
            catOccJson = doc.Body.toString();
            
            //Document doc3 = [Select d.Id, d.Body From Document d where Name='c1searcharea_json' limit 1];
            Document doc3 = (Document) selector.getOne('Id, Body','Name=\'c1searcharea_json\'',1);
            searchAreasJson = doc3.Body.toString();
            
           // Document doc2 = [Select d.Id, d.Body From Document d where Name='c1industry_json' limit 1];
            Document doc2 = (Document) selector.getOne('Id, Body','Name=\'c1industry_json\'',1);
            c1industryJson = doc2.Body.toString();
            getStyles(); 
        }
    }    

    public void getStyles(){
        //**** get Ad. Templates      
        StyleCategory__c[] templates = CTCService.getOptions(1,'careerone'); 
        if(templates != null){
            for(StyleCategory__c ts : templates){
                templateOptions.add(new SelectOption(ts.Id, ts.Name));
                template_map.put(ts.Id,ts.Name);
            }
        }
        //**** populate application form style selection
        StyleCategory__c[] styles = CTCService.getOptions(2,null); 
        if(styles != null){          
            for(StyleCategory__c sc : styles){
                styleOptions.add(new SelectOption(sc.Id, sc.Name));
                style_map.put(sc.Id, sc);
            }     
        }   
    }
    
    /*
    	save and post recent job ad.
    	It's an action will be invoke by page actionFunction tag
    */
    public PageReference saveAndPost(){
    	save();
    	if(hasErrMsgs){
    		customerErrorMessage(PeopleCloudErrorInfo.CAREERONE_VALIDATE_FAILED);
			return null;
		}
		
		post();
		return null;    		
    }
    
    public PageReference save(){
        try{
            newJob = job.clone(false, true);
            //newJob.RecordTypeId = [Select Id, Name From RecordType where DeveloperName = 'CareerOne_com_Advertisement'].Id;
            newJob.RecordTypeId = DaoRecordType.careerOneAdRT.Id;
            
            if(newJob.Template__c != null){
                newJob.Template_Name__c = template_map.get(newJob.Template__c);
            }
            if(newJob.Application_Form_Style__c != null){
                newJob.Application_Form_Style_Name__c = style_map.get(newJob.Application_Form_Style__c).Name;
            }
            
            if(job.Physical_Address__c == null || job.Physical_Address__c.split(', ').size() !=3){
            	job.Physical_Address__c.addError(CareerOneDetail.ERR_INVALID_PHY_ADDRESS);
            	customerErrorMessage(PeopleCloudErrorInfo.CAREERONE_VALIDATE_FAILED);
            	return null;
            }
            
            String[] phyAddress = job.Physical_Address__c.split(', ');
            
            newJob.zipcode__c = phyAddress[2];
            newJob.City_C1__c = phyAddress[0];
            newJob.State__c = phyAddress[1];
            //newJob.Job_Content__c = jobContentC1;
            //UPDATES FOR CKEDTOR
            //newJob.Online_Footer__c = onlineFooterC1;
            
            
            newJob.Status__c = 'Ready to Post';
            newJob.website__c='CareerOne';
            newJob.JobXML__c = 'Only saved! User has not posted yet.'; //** for admin trouble shooting
            
            //Add the premium packe by Jack on 12 Oct 2012
            if(C1PremiumPackage == true){
                newJob.Job_Bolding__c = true;
                newjob.CareerOne_Autorefresh__c = true;
                }
             else{
             	newJob.Job_Bolding__c = false;
                newjob.CareerOne_Autorefresh__c = false;
            }
            //end the premium packe by Jack on 12 Oct 2012
            
            //insert skill group ext id
		    if(enableskillparsing){
		    	String tempString  = String.valueOf(defaultskillgroups);
		        tempString = tempString.replaceAll('\\(', '');
		        tempString = tempString.replaceAll('\\)', '');
		        tempString = tempString.replace(' ','');
		        newjob.Skill_Group_Criteria__c = tempString;
		    }
		    //end insert skill group ext id
            //check FLS
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Advertisement__c.RecordTypeId,
                Advertisement__c.Template_Name__c,
                Advertisement__c.Application_Form_Style_Name__c,
                Advertisement__c.Physical_Address__c,
                Advertisement__c.zipcode__c,
                Advertisement__c.City_C1__c,
                Advertisement__c.State__c,
                Advertisement__c.Job_Content__c,
                Advertisement__c.Status__c,
                Advertisement__c.WebSite__c,
                Advertisement__c.JobXML__c,
                Advertisement__c.Job_Bolding__c,
                Advertisement__c.CareerOne_Autorefresh__c,
                Advertisement__c.Skill_Group_Criteria__c
            };
            fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
            insert newJob;
            jobrefcode = String.valueOf(newJob.Id).substring(0,15) + ':' + iid;
            hasSaved = true;
            saveInfo = 'Saved Locally Successfully';
            
            //Change by Jack and add new error message
            EC2Callout(String.valueOf(newJob.Id).substring(0,15),iid,this.newJob.Job_Title__c, this.newJob.Application_Form_Style__c , bucketname);
            String jobTitle = newJob.Job_Title__c == null ? '' :  newJob.Job_Title__c;
            String physicalAddress = newJob.Physical_Address__c == null ? '' : newJob.Physical_Address__c;
            String strMsg = 'Job Ad "' + jobTitle.escapeHtml4() + '" with location "'
	            				+ physicalAddress.escapeHtml4() +'" has been saved';
	        customerErrorMessage(PeopleCloudErrorInfo.CAREERONE_SAVE_AD,strMsg);
        	/*EC2Callout(String.valueOf(newJob.Id),iid,this.newJob.Job_Title__c, 
        	this.newJob.Application_Form_Style__c , bucketname);*/
        }catch(system.Exception e){
        	customerErrorMessage(PeopleCloudErrorInfo.CAREERONE_VALIDATE_FAILED);
            String err_msg = 'System Exception for record creation! log@clicktocloud.com<br/>';
            err_msg += 'Detail: ' + e.getMessage() + '('+e.getTypeName() +')';
            err_msg += 'JobRefCode:' + jobrefcode + '<br/>';
            notificationsendout(err_msg,'systemexception','CareerOneDetail', 'save');        
        }
        return null; 
    }
    
    public pageReference post(){
         C1PostingUtil  util = new C1PostingUtil(C1Username,C1CompanyXCode,C1Password);
         String soapXml = util.constructSoap(this.newJob,false);
         if(soapXml == null)
         {
            saveInfo = 'Sorry. Posted Unsuccessfully. Please contact your system admin.';
            if(!Test.isRunningTest()) {
                return null;    
            }
         }
         else if(soapXml.contains('ERROR Total Ad')) //**** over quota
         {
         	String[] xml_results = soapXml.split(':');
            remining_Quota = xml_results[2];
            numofads = xml_results[1];
            if(numofads != '1'){
                saveInfo = 'Sorry. Posted Unsuccessfully. Saved locally.<br/>';
                saveInfo += '<b>You have no enough quota to post ad now. Please contact your admin.</b>'; 
            }
            else{
                saveInfo = 'Sorry. You have no enough quota to post this Ad. now. Ad. is just saved in system.';
            }
            return null;
         }
         String aid = String.valueOf(newJob.id).subString(0,15);      
         //String aid = String.valueOf(newJob.id);         
         post2C1(soapXml, iid, aid);
         try
         {
             newJob.JobXML__c = soapXml;
             newJob.Status__c = 'Active';
             //check FLS
             List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Advertisement__c.JobXML__c,
                Advertisement__c.Status__c
             };
             fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
             update this.newJob;
             saveInfo = 'This Ad has been posted Successfully. If you like to post more ads here, please change Search Area, Classification or Occupation and press \'Save & Post\' button. ';
             //******* usage count *********
             /**
             * modified by andy for security review II
             */
             //User thisuser = [select Id,CareerOne_Usage__c from User where Id=:UserInfo.getUserId()];
             User thisuser = (User) new CommonSelector(User.sObjectType).setParam('userId', UserInfo.getUserId())
             			.getOne('Id,CareerOne_Usage__c', 'Id=:userId');
             			
             integer myusage = (thisuser.CareerOne_Usage__c == null || thisuser.CareerOne_Usage__c == '')? 0 : Integer.valueOf(thisuser.CareerOne_Usage__c);
             thisuser.CareerOne_Usage__c = String.valueOf(myusage + 1);
             fflib_SecurityUtils.checkFieldIsUpdateable(User.SObjectType, User.CareerOne_Usage__c);
             update thisuser;
             String jobtitle = newJob.Job_Title__c == null ? '' : newJob.Job_Title__c;
             String physicalAddress = newJob.Physical_Address__c == null ? '' : newJob.Physical_Address__c;
             String strMsg = 'Job Ad "' + jobtitle.escapeHtml4() + '" with location "'
           				+ physicalAddress.escapeHtml4() +'" has been post';
         	 customerErrorMessage(PeopleCloudErrorInfo.CAREERONE_SAVE_AD_POST_AD,strMsg);
         }
         catch(system.Exception e)
         {
         	String err_msg = 'System Exception for post! log@clicktocloud.com<br/>';
            err_msg += 'Detail: ' + e.getMessage() + '('+e.getTypeName() +')';
            err_msg += 'JobRefCode:' + jobrefcode + '<br/>';
            notificationsendout(err_msg,'systemexception','CareerOneDetail', 'Post');
         }
         return null;
     }
     
    @future(callout=true)
    static void post2C1(String soapXml, String orgid, String newJobId){
        String jobreferencecode = orgid + ':'+ newJobId;
        CTCWS.PeopleCloudWSDaoPort port = new CTCWS.PeopleCloudWSDaoPort();
        //CTCWS4Test.PeopleCloudWSDaoPort port = new CTCWS4Test.PeopleCloudWSDaoPort();
        port.timeout_x = 60000;
        
        String result;
        try{
            result = port.post2C1(soapXml);
        }
        catch(system.Exception e)
        {
            String err_msg = 'System Exception for future callout! log@clicktocloud.com<br/>';
            err_msg += 'Detail: ' + e.getMessage() + '('+e.getTypeName() +')';
            err_msg += '<br/> JobRefCode: ' + jobreferencecode;
            notificationsendout(err_msg,'systemexception','CareerOneDetail', 'post2C1');
        }
        ResultProcess(result, newJobId);
    }
    
    //insert the application form record to EC2 DB
    @future(callout=true)
    static void EC2Callout(String AdId, String instanceid, String jobtitle, String selectedStyle, String bucketname){
        Boolean insert_success = false;
        String jobcode = instanceid +':'+ AdId;
        CTCservice theservice = new CTCservice();
        try{
            insert_success = theservice.theCallout(AdId, instanceid, jobtitle, selectedStyle, bucketname,false,'careerone');
            if(!insert_success){            	 
            	String err_msg = 'System Exception for Create Record in EC2!<br/>'; 
            	err_msg += '<br/> JobRefCode: ' + jobcode;           	
	            notificationsendout(err_msg,'systemexception','CareerOneDetail', 'save');   
            }            
        }
        catch(system.Exception e)
        { 
            String err_msg = 'System Exception for future callout (Insert Style to EC2)! log@clicktocloud.com<br/>';
            err_msg += 'Detail: ' + e.getMessage() + '('+e.getTypeName() +')';
            err_msg += '<br/> JobRefCode: ' + jobcode;
            notificationsendout(err_msg,'systemexception','CareerOneDetail', 'EC2Callout');
        }        
    }

    public static void ResultProcess(String result, String aId){
        xmldom theXMLDom;
        Map<String,Set<String>> jobs = new Map<String,Set<String>>();
        
        String[] postingids = new String[]{};
        String thestatus;
        String postingId;
        String theref;
        try{
        	/**
        	* modified by andy for security review II
        	*/
            //Advertisement__c a = [select Id, Online_Job_Id__c, Placement_Date__c,
            //                        Job_Posting_Status__c, Status__c,Ad_Closing_Date__c  
            //                    from Advertisement__c where Id =:aId];
                                
            Advertisement__c a = (Advertisement__c)new CommonSelector(Advertisement__c.sObjectType)
            							.setParam('aId',aId)
            							.getOne('Id, Online_Job_Id__c, Placement_Date__c, Job_Posting_Status__c, Status__c,Ad_Closing_Date__c ',
            							'Id =:aId');
            if(result != null)
            {
                theXMLDom = new xmldom(result);
                if(theXMLDom != null)
                {
                    List<xmldom.Element> jobresponses = theXMLDom.getElementsByTagName('JobResponse');
                    if(jobresponses != null)
                    {
                        for(xmldom.Element r : jobresponses)
                        {
                            if(r.hasChildNodes())
                            {
                                theref = r.getElementsByTagName('JobReference')[0].getAttribute('jobRefCode');
                                thestatus = r.getElementsByTagName('status')[0].getElementsByTagName('ReturnCode')[0].nodeValue;
                                if(thestatus != null)
                                {
                                    if(thestatus == '0') //** success
                                    {
                                        List<xmldom.Element> postings = r.getElementsByTagName('JobPostingResponse');
                                        for(xmldom.Element p : postings)
                                        {
                                            postingId = p.getAttribute('postingId');
                                            postingids.add(postingId);
                                        }
                                    }
                                }
                                else
                                {
                                                                
                                }
                            }
                        }//for
                    }
                }
            }
            for(Integer i=0 ; i<postingids.size(); i++ )
            {
                if(i ==0)
                {
                    a.Online_Job_Id__c = postingids[i];
                }
                else
                {
                a.Online_Job_Id__c += ';' + postingids[i] ;
                }
            }
            if(thestatus == '0'){
                 //a.Job_Posting_Status__c='Posted Successfully';
                 a.Job_Posting_Status__c='active';
                 a.Status__c = 'Active';
                 a.Placement_Date__c = system.today();
                 a.Ad_Closing_Date__c = system.today().addDays(29);
            }
            else
            {
                String err_msg = 'CareerOne Job Post Unsuccessfully! log@clicktocloud.com<br/>';
                err_msg += 'Result XML: ' + result;
                err_msg += '<br/> advertisement Id: ' + aId;
                notificationsendout(err_msg,'codefail','CareerOneDetail', 'ResultProcess');
            }
            //check FLS
            List<Schema.SObjectField> adFieldList = new List<Schema.SObjectField>{
                Advertisement__c.Online_Job_Id__c,
                Advertisement__c.Job_Posting_Status__c,
                Advertisement__c.Status__c,
                Advertisement__c.Placement_Date__c,
                Advertisement__c.Ad_Closing_Date__c
            };
            fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, adFieldList); 
            update a;
            

        }
        catch(system.Exception e)
        {
            NotificationClass.notifyErr2Dev('CareerOneDetail/ResultProcess', e);
        }

    }
    public static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){
    
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());
    
    
    }
    
    public boolean hasErrMsgs{
    	get{
    		List<ApexPages.Message> msgList = ApexPages.getMessages();
    		for(ApexPages.Message sglMsg:msgList){
    			if(sglMsg.getSeverity() == ApexPages.Severity.ERROR || sglMsg.getSeverity() == ApexPages.Severity.FATAL){
    				haserror = true;
    				return true; }
    		} 
    		haserror = false;
    		return false;
    		}
    }
    
    //insert the skill group picklist value option
    public List<SelectOption> getSelectSkillGroupList(){
	  	List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
	  	List<SelectOption> displaySelectOption = new List<SelectOption>();
	  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
	  		displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
	  	}
	  	return displaySelectOption;
	}
    //Show the error message in the page
    public void customerErrorMessage(Integer msgcode){
        errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
                msgSignalhasmessageC1 = errormesg.returnresult;
                for(ApexPages.Message newMessage:errormesg.errorMessage){
                        ApexPages.addMessage(newMessage);
                }               
    }  
    
    public void customerErrorMessage(Integer msgcode, String extraError){
   		errormesg = new PeopleCloudErrorInfo(msgcode,extraError,true);                                     
        	msgSignalhasmessageC1 = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
            	ApexPages.addMessage(newMessage);
            }               
    }

}