/*
 * Add a page to redirect the application quesiton by recordTypeId
 * @param Id and recordTypeID
 * @return redirect to SeekScreenField page or IndeedQuestionSetup page
 * @author Jack ZHOU
 * @date 17 Mar 2017 
 */
public with sharing class ApplicationQuestionSetController {

	private final sObject applicationQuestion;
    private final Id sobjectId;
    private final RecordType sobjectReocrdType;
    private final String recordTypeId;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public ApplicationQuestionSetController(ApexPages.StandardController stdController) {
        this.applicationQuestion = (sObject)stdController.getRecord();
        sobjectId = ((Application_Question__c)applicationQuestion).Application_Question_Set__c;
        recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
    }

    public PageReference redirectQuestionPage() {
        PageReference redURL;
        if(recordTypeId == DaoRecordType.SeekScreenFieldRT.Id) {
            redURL = Page.SeekScreenFieldNewEdit;
        }else {
            redURL =Page.IndeedQuestionSetup;
        }
        Map<String, String> params = ApexPages.currentPage().getParameters();
        for (String paramKey : params.keySet()) {
            redURL.getParameters().put(paramKey, params.get(paramKey));
        }
        //redURL.getParameters().put('retURL', ApexPages.currentPage().getParameters().get('retURL'));
        //redURL.getParameters().put('sfdc.override'，'1')；
        redURL.setRedirect(true);

        return redURL;
    }
}