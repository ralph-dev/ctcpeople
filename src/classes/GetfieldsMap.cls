/***********************************************
*
*Fetch Object fields according Objectname
*Author by Alvin on 1 Jul 2013
*
/***********************************************/

public class GetfieldsMap {
	//Fetch Object fields according Objectname
    public static Map<string, SObjectField> getFieldMap(String objectn){
        Map<string, SObjectField> objectfields = new Map<string, SObjectField>();
        try{
	        SObjectType objToken = Schema.getGlobalDescribe().get(objectn.toLowerCase()); 
	        DescribeSObjectResult objDef = objToken.getDescribe();          
	        objectfields = objDef.fields.getMap();
        }
        catch(Exception e){
        	objectfields = new Map<string, SObjectField>();
        }
        return objectfields;
    }
    
    //General SOQL according the object name and whereclause
    public static string getCreatableFieldsSOQL(String objectName, Set<String> ExcludeFieldsForJobPosting, String whereClause){
    	String result = '';
    	list<string> selectFields = new list<string>();
    	Map<string, SObjectField> tempfields = getFieldMap(objectName);
    	//system.debug('ExcludeFieldsForJobPosting ='+ ExcludeFieldsForJobPosting);
    		if(tempfields!=null && tempfields.size()>0){
    			for (Schema.SObjectField ft : tempfields.values()){ // loop through all field tokens (ft)
		        	Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
		            if(fd.isCreateable() && !ExcludeFieldsForJobPosting.contains(fd.getName())){
		            	selectFields.add(fd.getName());
		            }
            	}
            	
            	if (!selectFields.isEmpty()){
		            for (string s:selectFields){
		                result += s + ',';
		            }
		            if (result.endsWith(',')){
		            	result = result.substring(0,result.lastIndexOf(','));
		            }
		            
		            CommonSelector.checkRead(objectName, result);
		            
		            if(whereClause!= null && whereClause != ''){
		    			result = 'SELECT ' + result + ' FROM ' + objectName + ' WHERE ' + whereClause;
		    		}
			    	else{
			    		result = 'SELECT ' + result + ' FROM ' + objectName;
			    	}
		        }
		    	else{
		    		return null;
		    	}
		    }
		    else{//no object fields can be found
		    	return null;
		    }
		return result;
    }
    
    //Exclude the Fields according the tempstring
    public static Set<String> getExcludeFieldsForJobPosting(String tempString){
    	Set<String> ExcludeFieldsForJobPostingSet = new Set<String>();
    	List<String> ExcludeFieldsForJobPosting = new List<String>();
    	tempString = tempString.trim();
    	ExcludeFieldsForJobPosting = tempString.split(',');
    	for(String setString: ExcludeFieldsForJobPosting){
    		setString = setString.trim();//remove the left and right space
    		ExcludeFieldsForJobPostingSet.add(setString);
    	}
    	return ExcludeFieldsForJobPostingSet;
    }
}