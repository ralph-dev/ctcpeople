/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class UnsecuredCredentialRESTTest {

/**
	private static testMethod void testGetCrendentials() {
		
		System.runAs(DummyRecordCreator.platformUser) {
			Advertisement_Configuration__c ac = new Advertisement_Configuration__c();
		    ac.name = 'indeed';
		    ac.Indeed_API_Token__c = 'dddd';
		    ac.recordtypeId = DaoRecordType.IndeedAdRT.Id;
		    insert ac;
		    
		    ac = new Advertisement_Configuration__c();
		    ac.name = 'jxt';
		    ac.UserName__c = 'username';
		    ac.Password__c = 'password';
		    ac.recordtypeId = DaoRecordType.jxtAccountRT.Id;
		    insert ac;
		    
		    StyleCategory__c sc = new StyleCategory__c();
		    sc.name = 'AstutePayrollAccount';
		    sc.AstutePayroll_api_username__c = 'username';
		    sc.AstutePayroll_api_password__c = 'password';
		    Id rtId = [Select Id, Name from RecordType where DeveloperName = 'AstutePayrollAccount'].Id;
		    sc.RecordTypeId = rtId;
		    
		    insert sc;
		    
		    
		   
		    sc = new StyleCategory__c();
		    sc.name = 'JXTNZAccount';
		    sc.Jobx_Username__c = 'username';
		    sc.Jobx_Password__c = 'password';
		    sc.WebSite__c = 'JXT_NZ';
		    sc.Advertiser_Id__c='1';
	
		    insert sc;
		    
		    sc = new StyleCategory__c();
		    sc.name = 'CareerOne';
		    sc.Advertiser_Name__c = 'username';
		    sc.Advertiser_Password__c = 'password';
		    sc.WebSite__c = 'CareerOne';
		    sc.Account_Active__c = true;
		    
		    sc.recordTypeId = DaoRecordType.jobBoardAccRT.Id;
	
		    insert sc;
		    
		    
		    OAuth_Service__c os = new OAuth_Service__c();
		    os.name = 'OAuth_Service__c';
		    os.Access_Token_URL__c='ddd';
		    os.Consumer_Key__c = 'key';
		    os.Consumer_Secret__c = 'secret';
		    os.Active__c = true;
		    
		    insert os;
		    
		    OAuth_Token__c ot = new OAuth_Token__c();
		    ot.name = 'OAuth_Token__c';
		    ot.secret__c = 'secret';
		    ot.token__c = 'token';
		    ot.OAuth_Service__c = os.Id;
		    
		    insert ot;
		    
		    
	
		    UnsecuredCredentialREST.CredentialList cl = UnsecuredCredentialREST.getCrendentials();
		    
		    System.assertEquals(8, cl.getCredentialList().size());
			
		}
	    
	    

	}
	
	*/

}