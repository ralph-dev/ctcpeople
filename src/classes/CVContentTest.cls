@isTest
private class CVContentTest {

    static testMethod void testGetterSetter() {
    	System.runAs(DummyRecordCreator.platformUser) {
        CVContent c= new CVContent();
        
        c.setCvFilename('xxxxx.doc');
        System.assertEquals('xxxxx.doc', c.getCvFilename());
        
        c.setContent(null);
        
         System.assertEquals(null, c.getContent());
    	}
    }
}