public with sharing class ViewDocExt {
	//Init Error Message for customer
    public PeopleCloudErrorInfo errormesg; 
    public Boolean msgSignalhasmessage{get ;set;} //show error message or not

	public Web_Document__c wd {get;set;}
	S3AdminController S3Con = new S3AdminController();
	public String bucket;
	public String objkey;
		
	public ViewDocExt(ApexPages.StandardController controller){
		wd = (Web_Document__c)controller.getRecord();
	}
	public PageReference init(){
		try{
			if(wd != null){
				CommonSelector.checkRead(Web_Document__c.SObjectType,'Id,ObjectKey__c,S3_Folder__c,Name');
				wd = [select Id,ObjectKey__c,S3_Folder__c,Name from Web_Document__c where Id =:wd.Id];
				if(wd.ObjectKey__c != null && wd.S3_Folder__c != null){
					bucket = wd.S3_Folder__c;
					objkey = wd.ObjectKey__c;
				}
			}
		}
		catch(exception e){
			system.debug(e);
		}
		
		try{
			if(bucket!= null && objkey!=null){				
				S3Con.bucketToList = bucket;
				S3Con.initS3();
				//system.debug(S3Con.as3.key);				
			}
		}
		catch(exception e){
			customerErrorMessage(PeopleCloudErrorInfo.S3_ACCOUNT_ERROR);
		}
		return downloadDoc();
	}
	
	public PageReference downloadDoc(){
		try{
			if(objkey == null || objkey == ''){
				return null;
			}
			PageReference pageref = S3Con.getDirectory(objkey);
			pageref.setRedirect(true);
			//system.debug(pageref);
			return pageref;
		}
		catch(exception e){
			customerErrorMessage(PeopleCloudErrorInfo.DOWNLOAD_S3_DOCUMENT_FAILED);
		}
		return null;
	}

	private void customerErrorMessage(Integer msgcode){
        errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        msgSignalhasmessage = errormesg.returnresult;
        for(ApexPages.Message newMessage:errormesg.errorMessage){
            ApexPages.addMessage(newMessage);
        }               
    }
}