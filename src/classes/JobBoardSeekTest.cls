/**
 * This is the test class for JobBoardSeek
 *
 * This test class only includes test method for seek specific method
 * For test of generic method, please check JobBoardPostingControllerTest
 *
 * Created by: Lina Wei
 * Created on: 31/03/2017
 *
 */

@isTest
private class JobBoardSeekTest {

    public static Advertisement__c ad;
    public static Advertisement_Configuration__c account;
    public static Advertisement_Configuration_Detail__c template;
    public static User u;
    public static String orgId;
    public static AdvertisementSelector selector;

    static {
        u = DummyRecordCreator.platformUser;
        System.runAs(u) {
            Placement__c vacancy = DataTestFactory.createSingleVacancy();
            ad = AdvertisementDummyRecordCreator.generateSeekAdDummyRecord();
            ad.Vacancy__c = vacancy.Id;
            insert ad;
            account = AdConfigDummyRecordCreator.createDummySeekAccount();
            template = AdConfigDetailDummyRecordCreator.createActiveTemplateWithoutInsert(account);
            template.Template_Items__c = 'BULLET1;BULLET2;BULLET3';
            insert template;
            orgId = UserInfo.getOrganizationId().substring(0, 15);
            selector = new AdvertisementSelector();
        }
    }

    static testMethod void testPostAd() {
        u.Seek_Usage__c = String.valueOf(0);
        u.Monthly_Quota_Seek__c = String.valueOf(1);
        u.Seek_Account__c = account.Id;
        update u;

        System.runAs(u) {
            // enable skill parsing
            PCAppSwitch__c cs = new PCAppSwitch__c();
            cs.Enable_Daxtra_Skill_Parsing__c = true;
            insert cs;
            // set skill groups
            List<Skill_Group__c> skillGroups = DataTestFactory.createSkillGroup();
            // set skill group
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.ad.Skill_Group_Criteria__c = skillGroups.get(0).Id + ',' + skillGroups.get(1).Id;

            jobBoard.postInit();
            System.assertEquals('post', jobBoard.actionType);
            System.assertEquals(true, jobBoard.enableskillparsing);
            System.assertEquals(new List<String>{skillGroups.get(0).Id, skillGroups.get(1).Id}, jobBoard.defaultskillgroups);

            jobBoard.ad.Advertisement_Account__c = account.Id;
            jobBoard.ad.isStandOut__c = false;
            jobBoard.ad.Reference_No__c = 'ReferenceNo';
            jobBoard.jobContentTemp = 'This is test content';
            jobBoard.jobOnlineFooterTemp = 'This is footer [%User Name%] [%Vacancy Referece No%]';
            jobBoard.selectedTemplate = template.Id;

            jobBoard.assignTemplateItems();
            jobBoard.ad.Bullet1_Item__c = 'BULLET1';
            jobBoard.ad.Bullet2_Item__c = 'BULLET2';
            jobBoard.ad.Bullet3_Item__c = 'BULLET3';

            jobBoard.postAd();

            Advertisement__c newAd = selector.getAdForSeekClone(jobBoard.newAd.Id);
            System.assertEquals(null, newAd.AdvertiserJobTemplateLogoCode__c);
            System.assertEquals(null, newAd.Logo_Name__c);
            System.assertEquals(null, newAd.Search_Bullet_Point_1__c);
            System.assertEquals(null, newAd.Search_Bullet_Point_2__c);
            System.assertEquals(null, newAd.Search_Bullet_Point_3__c);
            System.assertEquals('BULLET1', newAd.Bullet1_Item__c);
            System.assertEquals('BULLET2', newAd.Bullet2_Item__c);
            System.assertEquals('BULLET3', newAd.Bullet3_Item__c);
            System.assertEquals('Seek', newAd.WebSite__c);
            System.assertEquals('This is test content', newAd.Job_Content__c);
            System.assertEquals('This is footer ' + u.LastName + ' ' + 'ReferenceNo', newAd.Online_Footer__c);
            System.assertEquals('Active', newAd.Status__c);
            System.assertEquals('In Queue', newAd.Job_Posting_Status__c);

            User userAfter = new UserSelector().getCurrentUser();
            System.assertEquals(String.valueOf(1), userAfter.Seek_Usage__c);
            System.assertEquals(false, jobBoard.remaining_quota_enough);
        }
    }

    static testMethod void testPostAdWithoutQuota() {
        System.runAs(u) {
            User userAfter = new UserSelector().getCurrentUser();
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.postInit();
            jobBoard.ad.Advertisement_Account__c = account.Id;
            jobBoard.postAd();
            System.assertEquals(false, jobBoard.remaining_quota_enough);
            System.assert(jobBoard.msgAfterSubmit.contains('0 quota remaining'));
        }
    }

    static testMethod void testCheckEmailServiceSettingWhenDisabled() {
        System.runAs(u) {
            // no email service enabled
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.postInit();
            System.assertEquals(false, jobBoard.enableEmailService);
            System.assertEquals(null, jobBoard.ad.Prefer_Email_Address__c);
        }
    }

    static testMethod void testCheckEmailServiceSettingWhenEnabled() {
        u.Job_Posting_Email__c = 'test@test.com';
        update u;

        System.runAs(u) {
            // enable email service
            JobPostingSettings__c setting = new JobPostingSettings__c();
            setting.Posting_Seek_Ad_with_User_Email__c = true;
            insert setting;

            // for post action
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.postInit();
            System.assertEquals(true, jobBoard.enableEmailService);
            System.assertEquals('test@test.com', jobBoard.ad.Prefer_Email_Address__c);

            // for clone to post action without prefer email address
            jobBoard = new JobBoardSeek(ad);
            jobBoard.ad.Prefer_Email_Address__c = null;
            jobBoard.ad.Status__c = 'Clone to Post';
            jobBoard.updateInit();
            System.assertEquals(true, jobBoard.enableEmailService);
            System.assertEquals(null, jobBoard.ad.Prefer_Email_Address__c);

            // for clone to post action
            jobBoard = new JobBoardSeek(ad);
            jobBoard.ad.Prefer_Email_Address__c = 'prefer@test.com';
            jobBoard.ad.Status__c = 'Clone to Post';
            jobBoard.updateInit();
            System.assertEquals(true, jobBoard.enableEmailService);
            System.assertEquals('prefer@test.com', jobBoard.ad.Prefer_Email_Address__c);
        }
    }

    static testMethod void testInitOtherVariable() {
        System.runAs(u) {
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.ad.Search_Bullet_Point_1__c = null;
            jobBoard.ad.Search_Bullet_Point_2__c = null;
            jobBoard.ad.Search_Bullet_Point_3__c = null;
            jobBoard.ad.Bullet_1__c = 'bullet1';
            jobBoard.ad.Bullet_2__c = 'bullet2';
            jobBoard.ad.Bullet_3__c = 'bullet3';
            jobBoard.postInit();
            System.assertEquals('bullet1', jobBoard.ad.Search_Bullet_Point_1__c);
            System.assertEquals('bullet2', jobBoard.ad.Search_Bullet_Point_2__c);
            System.assertEquals('bullet3', jobBoard.ad.Search_Bullet_Point_3__c);
        }
    }

    static testMethod void testRepostAd() {
        u.Seek_Usage__c = String.valueOf(0);
        u.Monthly_Quota_Seek__c = String.valueOf(1);
        u.Seek_Account__c = account.Id;
        update u;
        System.runAs(u) {
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.ad.Job_Posting_Status__c = 'Posting Failed';
            jobBoard.updateInit();
            System.assertEquals('repost', jobBoard.actionType);

            jobBoard.ad.isStandOut__c = true;
            jobBoard.ad.Advertisement_Account__c = account.Id;
            jobBoard.ad.Bullet1_Item__c = 'BULLET1';
            jobBoard.ad.Bullet2_Item__c = 'BULLET2';
            jobBoard.ad.Bullet3_Item__c = 'BULLET3';
            jobBoard.assignTemplateItems();
            jobBoard.updateAd();
            Advertisement__c newAd = selector.getAdForSeekClone(jobBoard.ad.Id);
            System.assertEquals('12345', newAd.AdvertiserJobTemplateLogoCode__c);
            System.assertEquals('Point1', newAd.Search_Bullet_Point_1__c);
            System.assertEquals('Point2', newAd.Search_Bullet_Point_2__c);
            System.assertEquals('Point3', newAd.Search_Bullet_Point_3__c);
            System.assertEquals(null, newAd.Bullet1_Item__c);
            System.assertEquals(null, newAd.Bullet2_Item__c);
            System.assertEquals(null, newAd.Bullet3_Item__c);
            System.assertEquals('Seek', newAd.WebSite__c);
            System.assertEquals('Active', newAd.Status__c);
            System.assertEquals('In Queue', newAd.Job_Posting_Status__c);

            User userAfter = new UserSelector().getCurrentUser();
            system.debug(userAfter);
            System.assertEquals(String.valueOf(1), userAfter.Seek_Usage__c);
        }
    }

    static testMethod void testCloneAd() {
        System.runAs(u) {
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.u.Seek_Usage__c = String.valueOf(10);
            jobBoard.u.Monthly_Quota_Seek__c = String.valueOf(20);
            jobBoard.u.Seek_Account__c = account.Id;
            jobBoard.cloneInit();
            System.assertEquals('cloneToPost', jobBoard.actionType);
            PageReference returnPage = jobBoard.cloneAd();
            System.assert(returnPage.getUrl().contains('seekedit?id=' + jobBoard.newAd.Id));
            Advertisement__c newAd = selector.getAdvertisementById(jobBoard.newAd.Id);
            System.assertEquals('Clone to Post', newAd.Status__c);
            System.assertEquals(null, newAd.Job_Posting_Status__c);
            System.assertEquals(null, newAd.Placement_Date__c);
            System.assertEquals(null, newAd.Online_Ad__c);
        }
    }

    static testMethod void testCloneAdWithoutQuota() {
        System.runAs(u) {
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard = new JobBoardSeek(ad);
            jobBoard.cloneInit();
            System.assertEquals(false, jobBoard.show_page);
            for(ApexPages.Message msg : ApexPages.getMessages()) {
                System.assert(msg.getSummary().contains('no more quota'));
            }
        }
    }

    static testMethod void testCloneAdWithoutActiveAccount() {
        System.runAs(u) {
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.u.Seek_Usage__c = String.valueOf(10);
            jobBoard.u.Monthly_Quota_Seek__c = String.valueOf(20);
            jobBoard.cloneInit();
            System.assertEquals(false, jobBoard.show_page);
            for (ApexPages.Message msg : ApexPages.getMessages()) {
                System.assert(msg.getSummary().contains('no active seek account'));
            }
        }
    }

    static testMethod void testCloneToPostAd() {
        u.Seek_Usage__c = String.valueOf(0);
        u.Monthly_Quota_Seek__c = String.valueOf(1);
        update u;

        System.runAs(u) {
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.ad.Status__c = 'Clone to Post';
            jobBoard.updateInit();
            System.assertEquals('cloneToPost', jobBoard.actionType);

            jobBoard.ad.Advertisement_Account__c = account.Id;
            jobBoard.updateAd();

            Advertisement__c newAd = selector.getAdForSeekClone(jobBoard.ad.Id);
            System.assertEquals('Seek', newAd.WebSite__c);
            System.assertEquals('Active', newAd.Status__c);
            System.assertEquals('In Queue', newAd.Job_Posting_Status__c);

            User userAfter = new UserSelector().getCurrentUser();
            System.assertEquals(String.valueOf(1), userAfter.Seek_Usage__c);
            System.assertEquals(false, jobBoard.remaining_quota_enough);
        }
    }

    static testMethod void testUpdateAd() {
        u.Seek_Usage__c = String.valueOf(0);
        u.Monthly_Quota_Seek__c = String.valueOf(1);
        u.Seek_Account__c = account.Id;
        update u;

        System.runAs(u) {
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.ad.Status__c = 'Active';
            jobBoard.ad.Job_Posting_Status__c = 'Posted Successfully';
            jobBoard.ad.Advertisement_Account__c = account.Id;
            jobBoard.updateInit();
            jobBoard.updateAd();

            Advertisement__c newAd = selector.getAdForSeekClone(jobBoard.ad.Id);
            System.assertEquals('Seek', newAd.WebSite__c);
            System.assertEquals('Active', newAd.Status__c);
            System.assertEquals('Posted Successfully', newAd.Job_Posting_Status__c);
            System.assertEquals('Sending update request ...', newAd.Ad_Posting_Status_Description__c);

            User userAfter = new UserSelector().getCurrentUser();
            System.assertEquals(String.valueOf(0), userAfter.Seek_Usage__c);
        }
    }

    static testMethod void testUpdateAdWithWrongStatus() {
        System.runAs(u) {
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.updateInit();
            System.assertEquals('update', jobBoard.actionType);
            System.assertEquals(false, jobBoard.show_page);
            for (ApexPages.Message msg : ApexPages.getMessages()) {
                System.assert(msg.getSummary().contains('Ad Status is not correct'));
            }
        }
    }

    static testMethod void testUpdateAdWithoutAccountActive() {
        System.runAs(u) {
            Advertisement_Configuration__c inactiveAccount = AdConfigDummyRecordCreator.createInactiveSeekAccount();
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.ad.Status__c = 'Active';
            jobBoard.ad.Job_Posting_Status__c = 'Posted Successfully';
            jobBoard.ad.Advertisement_Account__c = inactiveAccount.Id;
            jobBoard.u.Seek_Account__c = inactiveAccount.Id;
            jobBoard.updateInit();
            System.assertEquals(false, jobBoard.show_page);
            for (ApexPages.Message msg : ApexPages.getMessages()) {
                System.assert(msg.getSummary().contains('seek account is not active'));
            }
        }
    }

    static testMethod void testUpdateAdWithoutAccountAssigned() {
        System.runAs(u) {
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.ad.Status__c = 'Active';
            jobBoard.ad.Job_Posting_Status__c = 'Posted Successfully';
            jobBoard.ad.Advertisement_Account__c = account.Id;
            jobBoard.updateInit();
            System.assertEquals(false, jobBoard.show_page);
            for (ApexPages.Message msg : ApexPages.getMessages()) {
                System.assert(msg.getSummary().contains('do not have a Seek account assigned to you'));
            }
        }
    }

    static testMethod void testArchiveAd() {
        System.runAs(u) {
            // Test for archive ad with wrong status
            JobBoard jobBoard = new JobBoardSeek(ad);
            jobBoard.archiveInit();
            System.assertEquals('archive', jobBoard.actionType);
            System.assertEquals(false, jobBoard.show_page);

            jobBoard = new JobBoardSeek(ad);
            jobBoard.ad.Status__c = 'Active';
            jobBoard.ad.Job_Posting_Status__c = 'Posted Successfully';
            jobBoard.archiveInit();
            jobBoard.archiveAd();
            Advertisement__c adArchived = selector.getAdvertisementById(ad.Id);
            System.assertEquals('Archived', adArchived.Status__c);
            System.assertEquals('Sending archive request ...', adArchived.Ad_Posting_Status_Description__c);
        }
    }


    static testMethod void testGetWebServiceEndpoint() {
        JobBoard jobBoard = new JobBoardSeek();
        System.assertEquals('https://cpews.clicktocloud.com', jobBoard.getWebServiceEndpoint());
    }

    static testMethod void testGetPostingEndpoint() {
        JobBoard jobBoard = new JobBoardSeek();
        jobBoard.actionType = 'post';
        System.assertEquals('https://cpews.clicktocloud.com/SeekAdPostWeb/job/postJobAd', jobBoard.getPostingEndpoint());
        jobBoard.actionType = 'repost';
        System.assertEquals('https://cpews.clicktocloud.com/SeekAdPostWeb/job/postJobAd', jobBoard.getPostingEndpoint());
        jobBoard.actionType = 'cloneToPost';
        System.assertEquals('https://cpews.clicktocloud.com/SeekAdPostWeb/job/postJobAd', jobBoard.getPostingEndpoint());
        jobBoard.actionType = 'update';
        System.assertEquals('https://cpews.clicktocloud.com/SeekAdPostWeb/job/updateJobAd', jobBoard.getPostingEndpoint());
        jobBoard.actionType = 'archive';
        System.assertEquals('https://cpews.clicktocloud.com/SeekAdPostWeb/job/archiveJobAd', jobBoard.getPostingEndpoint());
    }

    static testMethod void testGetHeaders() {
        JobBoard jobBoard = new JobBoardSeek();
        System.assertEquals('application/json', jobBoard.getHeaders().get('Content-Type'));
    }

    static testMethod void testGenerateRequestBody() {
        JobBoard jobBoard = new JobBoardSeek(ad);
        jobBoard.ad.Job_Posting_Status__c = 'In Queue';
        jobBoard.ad.JobXML__c = 'Test Content';
        jobBoard.selectedAccount = account;

        // for posting ad
        jobBoard.actionType = 'post';
        JobPostingRequest request = (JobPostingRequest) JSON.deserialize(jobBoard.generateRequestBody(ad), JobPostingRequest.Class);
        System.assertEquals('Seek', request.jobBoard);
        System.assertEquals('In Queue', request.postingStatus);
        System.assertEquals(orgId + ':' + ad.Id, request.referenceNo);
        System.assertEquals('Test Content', request.jobContent);
        System.assertEquals(account.Advertisement_Account__c, request.advertiserId);
        System.assertEquals(null, request.externalAdId);

        // for reposting ad
        jobBoard.actionType = 'repost';
        request = (JobPostingRequest) JSON.deserialize(jobBoard.generateRequestBody(ad), JobPostingRequest.Class);
        System.assertEquals('Seek', request.jobBoard);
        System.assertEquals('In Queue', request.postingStatus);
        System.assertEquals(orgId + ':' + ad.Id, request.referenceNo);
        System.assertEquals('Test Content', request.jobContent);
        System.assertEquals(account.Advertisement_Account__c, request.advertiserId);
        System.assertEquals(null, request.externalAdId);

        // for clone and post ad
        jobBoard.actionType = 'cloneToPost';
        request = (JobPostingRequest) JSON.deserialize(jobBoard.generateRequestBody(ad), JobPostingRequest.Class);
        System.assertEquals('Seek', request.jobBoard);
        System.assertEquals('In Queue', request.postingStatus);
        System.assertEquals(orgId + ':' + ad.Id, request.referenceNo);
        System.assertEquals('Test Content', request.jobContent);
        System.assertEquals(account.Advertisement_Account__c, request.advertiserId);
        System.assertEquals(null, request.externalAdId);

        ad.Online_Job_Id__c = 'EXTERNALID';

        // for updating ad
        jobBoard.actionType = 'update';
        request = (JobPostingRequest) JSON.deserialize(jobBoard.generateRequestBody(ad), JobPostingRequest.Class);
        System.assertEquals('Seek', request.jobBoard);
        System.assertEquals('In Queue', request.postingStatus);
        System.assertEquals(orgId + ':' + ad.Id, request.referenceNo);
        System.assertEquals('Test Content', request.jobContent);
        System.assertEquals(account.Advertisement_Account__c, request.advertiserId);
        System.assertEquals('EXTERNALID', request.externalAdId);

        // for archiving ad
        jobBoard.actionType = 'archive';
        request = (JobPostingRequest) JSON.deserialize(jobBoard.generateRequestBody(ad), JobPostingRequest.Class);
        System.assertEquals('Seek', request.jobBoard);
        System.assertEquals('In Queue', request.postingStatus);
        System.assertEquals(orgId + ':' + ad.Id, request.referenceNo);
        System.assertEquals(null, request.jobContent);
        System.assertEquals(null, request.advertiserId);
        System.assertEquals('EXTERNALID', request.externalAdId);
    }


    static testMethod void testGetPickListDefaultList() {
        JobBoard jobBoard = new JobBoardSeek();
        System.assert(jobBoard.getPickListDefaultList().contains('/SeekAdPostWeb/default/list'));
    }

    static testMethod void testGetAccountsInfo() {
        System.runAs(u) {
            JobBoard jobBoard = new JobBoardSeek(ad);
            // Test if no account assigned
            System.assertEquals(null, jobBoard.getAccountsInfo());

            // Test with account and related info
            Advertisement_Configuration__c account1 = AdConfigDummyRecordCreator.createDummySeekAccountWithoutInsert('Selective Account');
            Advertisement_Configuration__c account2 = AdConfigDummyRecordCreator.createDummySeekAccountWithoutInsert('Mandatory Account');
            account2.Account_Type__c = 'Mandatory';
            Advertisement_Configuration__c account3 = AdConfigDummyRecordCreator.createDummySeekAccountWithoutInsert('Never Account');
            account3.Account_Type__c = 'Never';
            List<Advertisement_Configuration__c> accounts = new List<Advertisement_Configuration__c>{account1, account2, account3};
            insert accounts;

            jobBoard.u.Seek_Account__c = account1.Id + ';' + account2.Id + ';' + account3.Id;
            Advertisement_Configuration_Detail__c template1 = AdConfigDetailDummyRecordCreator.createActiveTemplateWithoutInsert(account1);
            Advertisement_Configuration_Detail__c template2 = AdConfigDetailDummyRecordCreator.createActiveTemplateWithoutInsert(account1);
            template2.Default__c = true;
            Advertisement_Configuration_Detail__c logo1 = AdConfigDetailDummyRecordCreator.createActiveLogoWithoutInsert(account1);
            logo1.Default__c = true;
            Advertisement_Configuration_Detail__c logo2 = AdConfigDetailDummyRecordCreator.createActiveLogoWithoutInsert(account1);
            List<Advertisement_Configuration_Detail__c> adConfigDetails =
                    new List<Advertisement_Configuration_Detail__c> {template1, template2, logo1, logo2};
            insert adConfigDetails;
            Application_Question_Set__c screen1 = ApplicationQuestionSetDummyRecordCreator.createActiveSeekScreen(account1);
            Application_Question_Set__c screen2 = ApplicationQuestionSetDummyRecordCreator.createActiveSeekScreen(account1);
            Advertisement_Configuration_Detail__c template3 = AdConfigDetailDummyRecordCreator.createInactiveTemplate(account2);

            // Set default seek account as account 3
            jobBoard.u.Default_Seek_Account__c = account3.Id;

            // Generate account information
            String accountJson = jobBoard.getAccountsInfo();
            JobBoardAccountInfo accountInfo = (JobBoardAccountInfo)JSON.deserialize(accountJson, JobBoardAccountInfo.Class);
            System.assertEquals(3, accountInfo.accounts.size());

            JobBoardAccountInfo.JobBoardAccount accountInfo1;
            JobBoardAccountInfo.JobBoardAccount accountInfo2;
            JobBoardAccountInfo.JobBoardAccount accountInfo3;

            for (JobBoardAccountInfo.JobBoardAccount jobBoardAccount : accountInfo.accounts) {
                if (jobBoardAccount.id.equals(account1.Id)) {
                    accountInfo1 = jobBoardAccount;
                } else if (jobBoardAccount.id.equals(account2.Id)) {
                    accountInfo2 = jobBoardAccount;
                } else {
                    accountInfo3 = jobBoardAccount;
                }
            }

            // account 1
            System.assertEquals(account1.Id, accountInfo1.id);
            System.assertEquals(account1.Name, accountInfo1.name);
            System.assertEquals(false, accountInfo1.isDefault);
            System.assertEquals(new List<String>{'Classic', 'Stand Out'}, accountInfo1.availableTypes);

            System.assertEquals(2, accountInfo1.templates.size());
            // account 1 template 1
            System.assertEquals(template1.Id, accountInfo1.templates.get(0).sfId);
            System.assertEquals(template1.ID_API_Name__c, accountInfo1.templates.get(0).id);
            System.assertEquals(template1.Name, accountInfo1.templates.get(0).name);
            System.assertEquals(false, accountInfo1.templates.get(0).isDefault);
            // account 1 template 2
            System.assertEquals(template2.Id, accountInfo1.templates.get(1).sfId);
            System.assertEquals(template2.ID_API_Name__c, accountInfo1.templates.get(1).id);
            System.assertEquals(template2.Name, accountInfo1.templates.get(1).name);
            System.assertEquals(true, accountInfo1.templates.get(1).isDefault);

            System.assertEquals(2, accountInfo1.logos.size());
            // account 1 logo 1
            System.assertEquals(logo1.ID_API_Name__c, accountInfo1.logos.get(0).id);
            System.assertEquals(logo1.Name, accountInfo1.logos.get(0).name);
            System.assertEquals(true, accountInfo1.logos.get(0).isDefault);
            // account 1 logo 2
            System.assertEquals(logo2.ID_API_Name__c, accountInfo1.logos.get(1).id);
            System.assertEquals(logo2.Name, accountInfo1.logos.get(1).name);
            System.assertEquals(false, accountInfo1.logos.get(1).isDefault);

            System.assertEquals(2, accountInfo1.screens.size());
            // account 1 screen 1
            System.assertEquals(screen1.ID_API_Name__c, accountInfo1.screens.get(0).id);
            System.assertEquals(screen1.Name, accountInfo1.screens.get(0).name);
            System.assertEquals(false, accountInfo1.screens.get(0).isDefault);
            // account 1 screen 2
            System.assertEquals(screen2.ID_API_Name__c, accountInfo1.screens.get(1).id);
            System.assertEquals(screen2.Name, accountInfo1.screens.get(1).name);
            System.assertEquals(false, accountInfo1.screens.get(1).isDefault);

            // account 2
            System.assertEquals(account2.Id, accountInfo2.id);
            System.assertEquals(account2.Name, accountInfo2.name);
            System.assertEquals(false, accountInfo2.isDefault);
            System.assertEquals(new List<String>{'Stand Out'}, accountInfo2.availableTypes);
            System.assertEquals(null, accountInfo2.templates);
            System.assertEquals(null, accountInfo2.logos);
            System.assertEquals(null, accountInfo2.screens);

            // account 3
            System.assertEquals(account3.Id, accountInfo3.id);
            System.assertEquals(account3.Name, accountInfo3.name);
            System.assertEquals(true, accountInfo3.isDefault);
            System.assertEquals(new List<String>{'Classic'}, accountInfo3.availableTypes);

        }
    }
}