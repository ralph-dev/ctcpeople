public with sharing class UpdateReminingTrigger {
	public static void UpdateRemingTrigger(List<Placement_Candidate__c> contacts, List<Placement_Candidate__c> oldcontacts, boolean isupdate){
		String triggerName = 'UpdateRemining';
    	boolean flag = false;  
	    try{    
	        TriggerDetail thetriggerdetail = new TriggerDetail();
	        thetriggerdetail.getTriggerDetails();
	        system.debug('active = '+thetriggerdetail.IsActive(triggerName));
	        if(thetriggerdetail.IsActive(triggerName)) {
	        	Placement_Candidate__c insertcontact = new Placement_Candidate__c();
	        	insertcontact = getContact(contacts);
	        	
	            Placement_Candidate__c oldcontact  = new Placement_Candidate__c();
	            oldcontact = getContact(oldcontacts);
	            
	            if(isupdate){
	            	if(insertcontact.Status__c =='Placed' && insertcontact.Candidate_Status__c != oldcontact.Candidate_Status__c){
		            	flag = true;
		            }
		        }
		        if(!isupdate){
		        	if(insertcontact.Status__c =='Placed'){
		        		flag = true;
					}
		       	}
		        if(flag){
					CommonSelector.checkRead(Placement__c.SObjectType,'Id,With_Candidate_Placed__c,Stage__c');
		        	Placement__c p = [select Id,With_Candidate_Placed__c,Stage__c from Placement__c where Id=: insertcontact.Placement__c];
		            if(p != null){
		            	p.With_Candidate_Placed__c = true;
		            	fflib_SecurityUtils.checkFieldIsUpdateable(Placement__c.SObjectType, Placement__c.With_Candidate_Placed__c);
		                update p;
		            }
					CommonSelector.checkRead(Placement_Candidate__c.SObjectType,'Id, Show_Alert__c');
		            Placement_Candidate__c pc = [select Id, Show_Alert__c from Placement_Candidate__c where Id =: insertcontact.Id];
		            if(pc != null){
		            	 pc.Show_Alert__c = true;
		            	 fflib_SecurityUtils.checkFieldIsUpdateable(Placement_Candidate__c.SObjectType, Placement_Candidate__c.Show_Alert__c);
		                 update pc;
		            }
		        }
	        }
	    }
	    catch(Exception e){
        	system.debug(e);
    	}
    	
	}
	
	private static Placement_Candidate__c getContact(List<Placement_Candidate__c> contacts){
		Placement_Candidate__c returnContact = new Placement_Candidate__c();
		if(contacts != null && contacts.size()>0){
			for(Placement_Candidate__c con: contacts){
				returnContact = con;
			}
		}
		return returnContact;
	}
}