public with sharing class WebSitePost {
    public Advertisement__c ad;
    public String code{set; get
    	{	
    		if(check){
    			return code;
    		}
    		else{
    			return '';
    		}
    	
    	}}
    /**
    	Required_Other_Map is used to contain(fieldname, 'required website_required') for none text or none multipicklist
    	Or{fieldname,'website_required'} for required multipicklist type Or {fieldname,''} for not required or text.
    	
     **/
    public Map<String,String> Required_Other_Map{set;get;}
    
    /**
    	These maps is used to judge required block according to their text type and required or not.
    **/
    public Map<String, Boolean> Multi_Map{set;get;}
    public Map<String, Boolean> Textarea_Map{set;get;}
    public Map<String, Boolean> NoneRequiredTextarea_Map{set;get;}
    public Map<String, Boolean> Other_Map{set;get;}
    public Map<String, Integer> Map_recordtextareaforvalidation{set;get;}
    public Boolean check{set;get;}
    
    Advertisement__c newAd;
    
    /*
    	Used for the loop in javascript in websitepostjs.js to run the validation code in Textarea required.
    */
    public Integer Number_text{set;get;}
    
    /*
    	Used for the loop in javascript in websitepostjs.js to run the validation code in Textarea required.
    */
    public Integer Number_Multi{set;get;}
    public Map<String,Integer> textdivid{set;get;}
    public String multipicklist{get;set;}
   	public List<Schema.FieldSetMember> field_list{get;set;}
	
    public Boolean checkmultipost=false;
    public Boolean Getcheckmultipost(){
        return checkmultipost;
    }
    
   
    public IEC2WebService iec2ws{get; set;}
    
    /*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}    
    /*************End Add skill parsing param********************************************************/
    public WebSitePost(){}
    public WebSitePost(ApexPages.StandardController stdController){
    	checkmultipost=PCAppSwitch__c.getinstance().Multi_job_postings__c;
        String objectname=PCAppSwitch__c.getinstance().Object_Name__c;
        String fieldsetname=PCAppSwitch__c.getinstance().Field_Set_Name__c;
    	
    	//Init skill parsing param. Add by Jack on 23/05/2013
        enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
        enableSkillGroupList = new List<Skill_Group__c>();
        enableSkillGroupList = SkillGroups.getSkillGroups();
        defaultskillgroups = new List<String>();
        //end skill parsing param.
         
        ad = (Advertisement__c) stdcontroller.getRecord();
        if(ad.Skill_Group_Criteria__c!=null&&ad.Skill_Group_Criteria__c!=''){
        	String defaultskillgroupstring = ad.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
	        defaultskillgroups = defaultskillgroupstring.split(',');
        }
        iec2ws = new EC2WebServiceImpl();        
        Multi_Map =new Map<String, Boolean>();
    	Textarea_Map=new Map<String, Boolean>();
    	Required_Other_Map=new Map<String,String>();
   		NoneRequiredTextarea_Map=new Map<String, Boolean>();
    	Other_Map=new Map<String, Boolean>();
    	Map_recordtextareaforvalidation=new Map<String, Integer>();
        field_list=new List<Schema.FieldSetMember>();
        CustomFieldList cfl=new CustomFieldList(objectname,fieldsetname);
        check=true;
        Number_text=0;
  		Number_Multi=0;
  		
        System.debug('website post 1');
     	
       	field_list=cfl.getCustomFieldList();
      	
      	
        for(Schema.FieldSetMember fieldmember: field_list){
        	
        	Other_Map.put(fieldmember.getfieldpath(), false);
	       	Multi_Map.put(fieldmember.getfieldpath(), false);
	  		Textarea_Map.put(fieldmember.getfieldpath(), false);
	  		NoneRequiredTextarea_Map.put(fieldmember.getfieldpath(), false);
	  		
        	if(fieldmember.getrequired()){
	        	if(fieldmember.gettype().name()=='MULTIPICKLIST'){
	        		Required_Other_Map.put(fieldmember.getfieldpath(),'website_required');
	        		Multi_Map.put(fieldmember.getfieldpath(), true);
	        		
	        		//Number_Multi++;
	        		
	        	}else{
	        		if(fieldmember.gettype().name()=='TEXTAREA'){
	        			Required_Other_Map.put(fieldmember.getfieldpath(),'');
	        			Textarea_Map.put(fieldmember.getfieldpath(), true);
	        			Map_recordtextareaforvalidation.put(fieldmember.getfieldpath(),Number_text);
	        			Number_text++;
	        			
	        		}else{
	        			Required_Other_Map.put(fieldmember.getfieldpath(),'required website_required');
	        			Other_Map.put(fieldmember.getfieldpath(), true);
	        		}
	        	}
        	}else{
        			if(fieldmember.gettype().name()=='TEXTAREA'){
        				NoneRequiredTextarea_Map.put(fieldmember.getfieldpath(), true);
        			}else{
        				Other_Map.put(fieldmember.getfieldpath(), true);
        			}
	        		Required_Other_Map.put(fieldmember.getfieldpath(),'');
	        	}
        }
    }
    public PageReference website_save(){
    	try{
	        newAd = ad.clone(false, true);
	        newAd.website__c = 'WebSite';
	        newAd.RecordTypeId = JobBoardUtils.getRecordId('WebSite');
	        
	        //insert skill group ext id
	        if(enableskillparsing){
	        	String tempString  = String.valueOf(defaultskillgroups);
	        	tempString = tempString.replaceAll('\\(', '');
	        	tempString = tempString.replaceAll('\\)', '');
	        	tempString = tempString.replace(' ','');
	        	newAd.Skill_Group_Criteria__c = tempString;
	        }
	        //end insert skill group ext id
	        
	      	String temp = '';
	        if(newAd.online_footer__c != null && newAd.online_footer__c != 'null'){
	            temp = newAd.online_footer__c.replaceAll('\\[\\%User Name\\%\\]',UserInfo.getName());
	            if(newAd.Reference_No__c!=null){
	                temp = temp.replaceAll('\\[\\%Vacancy Referece No\\%\\]',newAd.Reference_No__c );
	            }
	        }
	        newAd.Online_Footer__c = temp;
	        //system.debug('footer = '+temp);
            checkFLS();
	        insert newAd;
	        return null;
    	}catch(Exception e){
        	return null;
    	}
    }
    
     public List<SelectOption> getStyleOptions(){
        return JobBoardUtils.getStyleOptions('website');
    }
    
    public PageReference insertDB(){
        CommonSelector.checkRead(User.SObjectType,'AmazonS3_Folder__c');
        User currentUser = [select AmazonS3_Folder__c from User where Id=:UserInfo.getUserId()];
        String jobRefCode=UserInfo.getOrganizationId().subString(0, 15)+':'+newAd.id;
        CommonSelector.checkRead(StyleCategory__c.SObjectType,'Id, Header_EC2_File__c, Header_File_Type__c,Div_Html_EC2_File__c, Div_Html_File_Type__c,Footer_EC2_File__c, Footer_File_Type__c');
        StyleCategory__c sc = [select Id, Header_EC2_File__c, Header_File_Type__c,
            Div_Html_EC2_File__c, Div_Html_File_Type__c, 
             Footer_EC2_File__c, Footer_File_Type__c  
                from StyleCategory__c where id = :newAd.Application_Form_Style__c limit 1];
        boolean result = iec2ws.insertDetailTable(jobRefCode, UserInfo.getOrganizationId().subString(0, 15),
             newAd.id, currentUser.AmazonS3_Folder__c, JobBoardUtils.blankValue(sc.Header_EC2_File__c), 
              JobBoardUtils.blankValue(sc.Header_File_Type__c), sc.Div_Html_EC2_File__c,
                sc.Div_Html_File_Type__c,  JobBoardUtils.blankValue(sc.Footer_EC2_File__c), 
                 JobBoardUtils.blankValue(sc.Footer_File_Type__c), '', 'website', 
                     JobBoardUtils.blankValue(newAd.Job_Title__c));
        //system.debug('result = '+result);
        if(result){
            newAd.Application_URL__c=Endpoint.getcpewsEndpoint()+'/peoplecloud/GetApplicationForm?jobRefCode='+jobRefCode+'&website=website';
            newAd.Status__c = 'Active';
            newAd.Job_Posting_Status__c = 'Posted Successfully';
            checkFLS();
            update newAd;
        }else{
            CareerOneDetail.notificationsendout('orgid='+Userinfo.getOrganizationId()+' userid='+UserInfo.getUserId()+' jxt cannot post detail information to EC2 database', 'ec2 callout', 'jxtnzpost', 'feedXml()');
            return Page.peoplecloudErrorPage;
        } 
        return null;
    }
    
    //Add Skill Parsing function for create or update ad template
	public List<String> getDefaultSkillGroups(List<Skill_Group__c> tempskillgrouplist){
	  	List<String> defaultSkillGroups = new List<String>();
	  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
	  		if(tempskillgroup.Default__c){
	  			defaultSkillGroups.add(tempskillgroup.Skill_Group_External_Id__c);
	  		}
	  	}
	  	return defaultSkillGroups;
    }
  
    //insert the skill group picklist value option
    public List<SelectOption> getSelectSkillGroupList(){
	  	List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
	  	List<SelectOption> displaySelectOption = new List<SelectOption>();
	  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
	  		displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
	  	}
	  	return displaySelectOption;
	}

    private void checkFLS(){
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Advertisement__c.Website__c,
            Advertisement__c.RecordTypeId,
            Advertisement__c.Job_Content__c,
            Advertisement__c.Online_Footer__c,
            Advertisement__c.Skill_Group_Criteria__c,
            Advertisement__c.Reference_No__c,
            Advertisement__c.Application_Form_Style__c,
            Advertisement__c.Job_Title__c,
            Advertisement__c.Application_URL__c,
            Advertisement__c.Status__c,
            Advertisement__c.Job_Posting_Status__c
        };
        fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
        fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    }
}