/**
 * This is the test class for SeekJobPostingUtils
 *
 * Created by: Lina Wei
 * Created on: 30/03/2017
 *
 */
@isTest
private class SeekJobPostingUtilsTest {

    static testMethod void testGenerateJobContent() {
        System.runAs(DummyRecordCreator.platformUser) {
            Advertisement_Configuration__c account = AdConfigDummyRecordCreator.createDummySeekAccount();
            Advertisement__c ad = AdvertisementDummyRecordCreator.generateSeekAdDummyRecord();
            Placement__c vacancy = DataTestFactory.createSingleVacancy();
            ad.Vacancy__c = vacancy.Id;
            ad.IsStandout__c = true;
            ad.Bullet1_Item__c = 'BULLET1';
            ad.Bullet2_Item__c = 'BULLET2';
            ad.Bullet3_Item__c = 'BULLET3';
            insert ad;
            Map<String, String> itemFieldMap = new Map<String, String>{
                    'BULLET1' => 'Bullet1_Item__c',
                    'BULLET2' => 'Bullet2_Item__c',
                    'BULLET3' => 'Bullet3_Item__c'
            };

            String refNo = UserInfo.getOrganizationId().substring(0, 15) + ':' + String.valueOf(ad.Id);
            String jobContentString = SeekJobPostingUtils.generateJobContent(ad, account,refNo, itemFieldMap);
            SeekJobContent content = (SeekJobContent) JSON.deserialize(jobContentString, SeekJobContent.Class);
            System.assertEquals('132456', content.thirdParties.advertiserId);
            System.assertEquals('StandOut', content.advertisementType);
            System.assertEquals('Job Title', content.jobTitle);
            System.assertEquals('Online Search Title', content.searchJobTitle);
            System.assertEquals('Sydney', content.location.id);
            System.assertEquals('CBD', content.location.areaId);
            System.assertEquals('SoftwareDeveloper', content.subclassificationId);
            System.assertEquals('FullTime', content.workType);
            System.assertEquals('Annual', content.salary.type);
            System.assertEquals('100000', content.salary.minimum);
            System.assertEquals('200000', content.salary.maximum);
            System.assertEquals('$100000 - $200000', content.salary.details);
            System.assertEquals('Online Summary', content.jobSummary);
            System.assertEquals('Job Content<p>Online Footer</p>', content.advertisementDetails);
            System.assertEquals('Test', content.contact.name);
            System.assertEquals('12345678', content.contact.phone);
            System.assertEquals('test@test.com', content.contact.email);
            System.assertEquals('https://youtu.be/testVideo', content.video.url);
            System.assertEquals('Above', content.video.position);
            System.assertEquals('test@test.com', content.applicationEmail);
            System.assertEquals(null, content.applicationFormUrl);
            System.assertEquals(null, content.endApplicationUrl);
            System.assertEquals('12345', content.screenId);
            System.assertEquals(refNo, content.jobReference);
            System.assertEquals(null, content.agentJobReference);
            System.assertEquals('12345', content.template.id);
            System.assertEquals(3, content.template.items.size());
            System.assertEquals('12345', content.standout.logoId);
            System.assertEquals(new List<String>{'Point1', 'Point2', 'Point3'}, content.standout.bullets);
            System.assertEquals('lnPlatform', content.recruiter.fullName);
            System.assertEquals('adminplatformuser@clicktocloud.com', content.recruiter.email);
            System.assertEquals(null, content.recruiter.teamName);
            System.assertEquals(new List<String>{'ResidentsOnly', 'Graduate'}, content.additionalProperties);
            System.assertEquals(refNo, content.creationId);
        }
    }

    static testMethod void testGenerateJobContentWithDifferentCondition() {
        System.runAs(DummyRecordCreator.platformUser) {
            Advertisement_Configuration__c account = AdConfigDummyRecordCreator.createDummySeekAccount();
            Advertisement__c ad = AdvertisementDummyRecordCreator.generateSeekAdDummyRecord();
            Placement__c vacancy = DataTestFactory.createSingleVacancy();
            ad.Vacancy__c = vacancy.Id;
            ad.IsStandout__c = false;
            ad.Online_Footer__c = '';
            ad.Prefer_Email_Address__c = null;
            ad.Residency_Required__c = false;
            ad.SeekMarketSegment__c = '';
            insert ad;
            String refNo = UserInfo.getOrganizationId().substring(0, 15) + ':' + String.valueOf(ad.Id);
            String jobContentString = SeekJobPostingUtils.generateJobContent(ad, account,refNo, null);
            SeekJobContent content = (SeekJobContent) JSON.deserialize(jobContentString, SeekJobContent.Class);
            System.assertEquals('Job Content', content.advertisementDetails);
            System.assertEquals(null, content.applicationEmail);
            System.assertEquals(null, content.standout);
            System.assertEquals(new List<String>(), content.additionalProperties);
        }
    }
}