@isTest
private class TestGlobalUtility {

    static testMethod void myUnitTest() { 
    	System.runAs(DummyRecordCreator.platformUser) {
        Boolean ABNValid1=
            GlobalUtility.ValidateABN(DataTestFactory.ABNContainsLetter);
        System.assert(!ABNValid1);
        
        Boolean ABNValid2=
            GlobalUtility.ValidateABN(DataTestFactory.ABNLengthNotEqualToEvelen);
        System.assert(!ABNValid2);
        
        Boolean ABNValid3=
            GlobalUtility.ValidateABN(DataTestFactory.ABNWeightingSumModEightNineReminderNotEqualZero);
        System.assert(!ABNValid3);
        
        Boolean ABNValid4=
            GlobalUtility.ValidateABN(DataTestFactory.ABNNull);
        System.assert(!ABNValid4);
        
        Boolean ABNValid5=
            GlobalUtility.ValidateABN(DataTestFactory.ABNStartWithZero);
        
        System.assert(!ABNValid5);
        Boolean ABNValid6=
            GlobalUtility.ValidateABN(DataTestFactory.ABNValid);
        System.assert(ABNValid6); 
    	}
        
    }
}