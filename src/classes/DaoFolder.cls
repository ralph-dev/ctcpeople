public with sharing class DaoFolder extends CommonSelector{
	
	public DaoFolder(){
		super(Folder.sObjectType);
	}
	
	private static DaoFolder dao = new DaoFolder();
	private static String defaultFields = 'Type, NamespacePrefix, Name, IsReadonly, Id, DeveloperName, AccessType';
	
	public static List<Folder> getPublicFoldersByType(String fType){
		List<Folder> result;
    	try{  
    		dao.checkRead(defaultFields);
			result = [Select Type, NamespacePrefix, Name, 
					IsReadonly, Id, DeveloperName, AccessType 
					From Folder where AccessType='Public' and Type=:fType];
    	}catch(Exception e){
    		result = new List<Folder>();
    	}
    	return result;
	}
	
	public static Folder getFolderByDevName(String devName){
		Folder result;
		try{
			dao.checkRead(defaultFields);
			result = [Select Type, NamespacePrefix, Name, 
					IsReadonly, Id, DeveloperName, AccessType 
					From Folder where DeveloperName=:devName limit 1];
		}catch(Exception e){
			result = null;
		}
		return result;
	}	
	
	//Add by Jack
	public static List<Folder> getFoldersByType(String fType){
		List<Folder> result;
    	try{  
    		dao.checkRead(defaultFields);
			result = [Select Type, NamespacePrefix, Name, 
					IsReadonly, Id, DeveloperName, AccessType 
					From Folder where Type=:fType];
    	}catch(Exception e){
    		result = new List<Folder>();
    	}
    	return result;
	}
	
	public static Folder getFolderByTypeAndId(String ftype, Id folderid){
		Folder result;
		try{
			dao.checkRead(defaultFields);
			result = [Select Type, NamespacePrefix, Name, 
					IsReadonly, Id, DeveloperName, AccessType 
					From Folder where Type=:fType and Id=:folderid limit 1];
		}catch(Exception e){
			result = null;
		}
		return result;
	}
	
	//Sort folder by Name, Author by Jack on 17/05/2013
	public static List<Folder> getFolderByIds(Set<Id> emailfolderids){
		List<Folder> tempemailfoderids = new List<Folder>();
		try{
			dao.checkRead(defaultFields);
			tempemailfoderids = [Select Type, NamespacePrefix, Name, 
					IsReadonly, Id, DeveloperName, AccessType 
					From Folder where Id in:emailfolderids order By Name ASC];
		}
		catch(Exception e){
			tempemailfoderids = new List<Folder>();
		}
		return tempemailfoderids;
	}
}