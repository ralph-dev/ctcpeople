@isTest
private class AdvertisementSelectorTest {


    static {
        TriggerHelper.disableAllTrigger();
    }

    static DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
    static AdvertisementDummyRecordCreator adCreator = new AdvertisementDummyRecordCreator();
    static Advertisement__c adv = adCreator.generateOneAdvertisementDummyRecord(rs);
    static AdvertisementSelector selector = new AdvertisementSelector();

    static testMethod void testGetVacIdByAdId() {
        System.runAs(DummyRecordCreator.platformUser) {
            Advertisement__c advBySelect = selector.getVacIdByAdId(adv.Id);
            System.assertNotEquals(null, advBySelect.Vacancy__c );
            System.assertNotEquals(null, advBySelect.Vacancy__r.ownerId );
        }
    }

    static testMethod void testGetSObjectFieldListForSeekClone(){
        System.runAs(DummyRecordCreator.platformUser) {
            Advertisement__c advBySelect = selector.getAdForSeekClone(adv.Id);
            System.assertNotEquals(null, advBySelect.Vacancy__c );
            try {
                System.debug(advBySelect.OwnerId);
            } catch (Exception e) {
                    System.Assert(e.getMessage().contains('without querying the requested field: '+ PeopleCloudHelper.getPackageNamespace() +'Advertisement__c.OwnerId'));
            }
            try {
                System.debug(advBySelect.CreatedDate);
            } catch (Exception e) {
                System.Assert(e.getMessage().contains('without querying the requested field: '+ PeopleCloudHelper.getPackageNamespace() +'Advertisement__c.CreatedDate'));
            }
            try {
                System.debug(advBySelect.CreatedById);
            } catch (Exception e) {
                System.Assert(e.getMessage().contains('without querying the requested field: '+ PeopleCloudHelper.getPackageNamespace() +'Advertisement__c.CreatedById'));
            }
            try {
                System.debug(advBySelect.Placement_Date__c);
            } catch (Exception e) {
                System.Assert(e.getMessage().contains('without querying the requested field: '+ PeopleCloudHelper.getPackageNamespace() +'Advertisement__c.'+ PeopleCloudHelper.getPackageNamespace() +'Placement_Date__c'));
            }
        }
    }

    static testMethod void testGetAdvertisementById() {
        System.runAs(DummyRecordCreator.platformUser) {
            Advertisement__c ad = selector.getAdvertisementById(adv.Id);
            System.assertEquals(adv.Id, ad.Id);
        }
    }

}