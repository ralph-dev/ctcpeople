public with sharing class DaoAdvertisement extends CommonSelector{
	
	public DaoAdvertisement(){
		super(Advertisement__c.sObjectType);
	}
	
	//Get advertisement by Id
	public static Advertisement__c getAdvertisementById(String advertisementid, Set<String> ExcludeFieldsForJobPosting){
		Advertisement__c result = new Advertisement__c();
		String searchURL = '';
		String whereClause = '';
		
		try{
			whereClause = 'id = \''+ advertisementid+ '\'';
			
			/**
			* already did checkread in GetfieldsMap.getCreatableFieldsSOQL method
			*/
			searchURL = GetfieldsMap.getCreatableFieldsSOQL(PeopleCloudHelper.getPackageNamespace()+'Advertisement__c', ExcludeFieldsForJobPosting, whereClause);
			//system.debug('searchURL ='+ searchURL);
			if(searchURL!=null && searchURL!= '')
				result = database.query(searchURL);
		}catch(Exception e){
			result = new Advertisement__c();
		}
		//system.debug('result ='+ result);
		return result;
	}
}