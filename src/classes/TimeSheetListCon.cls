/**************************************
 *                                    *
 * Deprecated Class, 02/05/2017 Ralph *
 *                                    *
 **************************************/

/*
	list on candidate portal to view their own existing timesheets
 	only the vacanies they applied (having vcw) can be displayed in the list.

*/

public with sharing class TimeSheetListCon{
	/*User user ;
	List<Placement_Candidate__c> vcws;
	public List<Placement__c> alljobs{get;set;}
	public List<ApplicationHistory> Apps{get;set;}
	Map<Id,ApplicationHistory> map_apps = new Map<Id,ApplicationHistory>();
	public String selectedVid {get;set;} 
	public String contactId ; // in user detail page 
	public Set<String> Progresses = new Set<String>(); 

	public TimeSheetListCon(){
    	//ts = new List<TimeSheetClass.TimeSheetRow>();
    	Apps = new List<ApplicationHistory>();
    	alljobs = new List<Placement__c>();
    	UserInformationCon usercls = new UserInformationCon();
		user = usercls.getUserDetail();
    	contactId = user.ContactId;
		getProgressValues(); 
	}   
	
	public void getProgressValues(){	
		try{
			String Progress_Str = '';
			if(Test.isRunningtest()){
				Progress_Str = 'Placed';
			}else{
			 	Progress_Str = TimeSheetInfo__c.getInstance().Candidate_Progress__c;
			}
			if(Progress_Str != null){
				List<String> temp = Progress_Str.split(';');
				//system.debug('temp =' + temp);
				Progresses.addAll(temp == null || temp.size() <=0 ? new List<String>{'Placed'} : temp); 
				//system.debug('Progresses =' + Progresses);
			}
		}
		catch(system.exception e){	
			Progresses = new Set<String>{'Placed'};
		}
	}
	
	public void init(){
		try{
			if(contactId != null ){
	            Set<Id> aids = new Set<Id>();
	            Set<Id> vids = new Set<Id>();
            	try{
            		CommonSelector.checkRead(Placement_Candidate__c.sObjectType, 'Placement__c, Placement__r.Name,Online_Ad__c');
                	for (Placement_Candidate__c[] cs : [select Placement__c, Placement__r.Name,Online_Ad__c
                									 from Placement_Candidate__c 
                                                     where Candidate__c =: contactId and Candidate_Status__c in: Progresses
                                                     order by LastModifiedDate DESC limit 1000] ){
	                    for(Placement_Candidate__c vcw : cs){
	                        vids.add(vcw.Placement__c);
	                        aids.add(vcw.Online_Ad__c);
	                    }                    
	                }
	            }
	            catch(system.exception e)
	            {
	            // ** list error : too much vacancy
	            }
        
	        	//*** get all vacancy information Ad Title. And Company Information
	        	//** if both approver fields are blank, the vacancy can not be displayed in portal 
	        	CommonSelector.checkRead(Placement__c.sObjectType , 'Id,Online_Job_Title__c');
	        	CommonSelector.checkRead(Advertisement__c.sObjectType, '  Id,Job_Title__c');
	        	for(Placement__c p : [select Id,Online_Job_Title__c,
        						 (select Id,Job_Title__c from Advertisements__r 
        						  where Id in: aids
        						  order by Id DESC) 
        						  from Placement__c where Id in: vids
        						  and (TimeSheet_Approver__c <> null or Backup_Approver__c <> null or Internal_Approver__c <> null)
        						  ]){
        	
	        		if(p.Advertisements__r != null)
	        		{
        			
	        			if(p.Advertisements__r.size() > 0 )
	        			{
	        				map_apps.put(p.Id,new ApplicationHistory(p,null,null,p.Advertisements__r[0].Job_Title__c,p.Id));
	        			}
	        			else
		        		{
		        			if(p.Online_Job_Title__c != null)
		        			{map_apps.put(p.Id,new ApplicationHistory(p,null,null,p.Online_Job_Title__c,p.Id));}
		        		}
	        		}
	        		else
	        		{
	        			if(p.Online_Job_Title__c != null)
	        			{
	        			map_apps.put(p.Id,new ApplicationHistory(p,null,null,p.Online_Job_Title__c,p.Id));
	        			}
	        		}
	        	}
	        	if(map_apps.size()>0)
	        	{
	        	
	        		Apps = map_apps.values();
	        	}
	        }
	    }
	    catch(system.exception e)
	    {
	        system.debug(e);
	    }
		
	
	}
	public pageReference viewTimeSheet(){
	    String theid = ApexPages.currentPage().getParameters().get('id');
	    if(theid != null && theid != '')
	    {
	        PageReference pageRef = new PageReference('/apex/TimeSheet?id='+theid);
	       
	        pageRef.setRedirect(true);
	        return pageRef;
	        
	    }
	    return null;
	
	
	}
	public pageReference newTimeSheet(){
		// open a timesheet window
		try{
			Id tsid = TimeSheetClass.CreateNewTimeSheet(selectedVid, contactId, 'In Progress');
			PageReference pageRef = new PageReference('/apex/TimeSheet?id='+tsid+'&isnew=yes');
		    pageRef.setRedirect(true);
		    return pageRef; 
	    }
	    catch(system.exception e)
	    {
	    }
	    return null;
	
	}
	public void deleteTimeSheet(){
		try{
			String tid = ApexPages.currentPage().getParameters().get('tid');
			String v_id = ApexPages.currentPage().getParameters().get('jobid');
			if(tid != null && v_id != null)
			{
				CommonSelector.checkRead(TimeSheet__c.sObjectType, 'Id');
				TimeSheet__c ts = [select Id from TimeSheet__c where Id =: tid];
				fflib_SecurityUtils.checkObjectIsDeletable(ts.getSObjectType());
				Database.DeleteResult DR_Dels = Database.delete(ts);
				if(DR_Dels.isSuccess())
				{
					ApplicationHistory theah = map_apps.get(v_id);
					if(theah != null)
					{
						List<TimeSheet__c> tss = theah.theTSs;
						Integer i = 0;
						for(TimeSheet__c t : tss)
						{
							
							if(t.Id == tid)
							{
								tss.remove(i);
								break;
							}
							i++;
						}
						theah.theTSs= tss;
						map_apps.put(v_id, theah);
						Apps = map_apps.values();
					}
				}
			
			}
		}
		catch(system.exception e)
		{
		}
	
	}
	public void LoadTimeSheets(){
		String vid = ApexPages.currentPage().getParameters().get('vid');
		ApplicationHistory theAH ;
		List<TimeSheet__c> ts = new List<TimeSheet__c>(); 
	    if(vid != null && vid != '' && map_apps != null)
	    {
	         	theAH = map_apps.get(vid);	
	         	
	         	CommonSelector.checkRead(TimeSheet__c.sObjectType, 'Id,Name,TimeSheet_Status__c,Vacancy__r.Name, Start_Date__c,End_Date__c,Start_Time__c, End_Time__c, Vacancy__c, CreatedDate');
	         	for(TimeSheet__c t : [select Id,Name,TimeSheet_Status__c,Vacancy__r.Name, Start_Date__c,End_Date__c,
	                  Start_Time__c, End_Time__c, Vacancy__c, CreatedDate
	                  from TimeSheet__c 
	                  where Vacancy__c =: vid and Candidate__c =: contactId
	                  order by LastModifiedDate DESC
	                  limit 1000])
	            {
	                ts.add(t);
	            
	            }
	            if(ts.size()>0 && theAH != null)
	            {
	            	theAH.TheTSs =ts;
	            	map_apps.put(vid,theAH);
	            	Apps = map_apps.values();
	            }
	            else
	            {
	            	if(ts.size() == 0 && theAH != null)
	            	{
	            		ts = null;
	            		theAH.TheTSs =ts;
		            	map_apps.put(vid,theAH);
		            	Apps = map_apps.values();
	            	}
	            }
	            
	    }
	    //system.debug('the list ='+ Apps);
	
	}
	//************
	public class ApplicationHistory{
		public Placement__c theJob {get;set;}
		public Placement_Candidate__c theVCW{get;set;} //always the latest one, candidate may apply one job more than 1 time.
		public List<TimeSheet__c> theTSs{get;set;} 
		public String AdTitle{get;set;}//display on the list
		public String VId {get;set;}
		public ApplicationHistory(Placement__c p, Placement_Candidate__c pc, List<TimeSheet__c> ts, String t, String vid ){
			this.theJob = p;
			this.theVCW = pc;
			this.theTSs = ts;
			this.AdTitle = t;
			this.VId = vid;
		}
		
	}*/
}