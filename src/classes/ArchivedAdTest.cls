@isTest
public class ArchivedAdTest{	
	private static testmethod void testArchivedAd(){
		System.runAs(DummyRecordCreator.platformUser) {
			TriggerHelper.disableAllTrigger();
			Advertisement__c ad = createJXTAd();

			ArchivedAd aa = new ArchivedAd(new ApexPages.StandardController(ad));
			aa.archive();
			system.assertEquals(aa.ad.Status__c, 'Archived');

			ad.Status__c= 'Active'; 
			ad.Job_Posting_Status__c='In Queue';
			update ad;
			ArchivedAd aa2 = new ArchivedAd(new ApexPages.StandardController(ad));
			aa2.archive();
			system.assertEquals(aa2.ad.Status__c, 'Recalled');

			ad.Status__c= 'Recalled';
			update ad;
			ArchivedAd aa3 = new ArchivedAd(new ApexPages.StandardController(ad));
			aa3.archive();
			system.assertEquals(aa3.ad.Status__c, 'Active');


			PageReference cancelachieve = aa3.cancelArchive();
			system.assert(cancelachieve != null);
		}
		
	}

	public static Advertisement__c createJXTAd(){

		
		Placement__c v = new Placement__c();
		insert v;
		Advertisement__c ad = new Advertisement__c();
		ad.RecordTypeId = DaoRecordType.JXTAdRT.Id;
		ad.WebSite__c='a';	 ad.Location__c='a'; 	ad.SearchArea_Label_String__c='a';
		ad.Classification__c='a'; 	ad.Vacancy__c=v.id; 	ad.SubClassification2__c='a';
		ad.Classification2__c='a';	ad.SubClassification_Seek_Label__c='a';	 ad.WorkType__c='a';
		ad.Reference_No__c='a';		ad.Job_Title__c='a';		ad.Company_Name__c='a';
		ad.Template__c='a';			ad.Bullet_1__c='a';		
		ad.Bullet_2__c='a';		ad.Bullet_3__c='a';			ad.Job_Type__c='a';
		ad.JXT_Short_Description__c='a';	ad.jxt_sectors__c='a';		ad.Hot_Job__c=true;
		ad.Residency_Required__c=true;		ad.Search_Tags__c='a';		ad.Street_Adress__c='a';
		ad.Nearest_Transport__c='a';		ad.Job_Contact_Name__c='a';		ad.Job_Contact_Phone__c='12345';
		ad.Location_Hide__c =false;			ad.Salary_Type__c='a';			ad.SeekSalaryMin__c='123';
		ad.SeekSalaryMax__c='2353'; 		ad.Salary_Description__c='a';	ad.No_salary_information__c=false;
		ad.Job_Content__c='a';			ad.Has_Referral_Fee__c='a';	ad.Referral_Fee__c=500; ad.Status__c='Active';
		ad.Terms_And_Conditions_Url__c=Endpoint.getPeoplecloudWebSite()+'';
		ad.JXTNZ_Location__c='a,b,c,d,e,f';
		ad.Industry__c='abc';
		insert ad;
		return ad;
	}


}