@isTest
private class DisplayColumnTest {

	private static testMethod void test() {
		System.runAs(DummyRecordCreator.platformUser) {
	    DisplayColumn d = new DisplayColumn();
        system.assertEquals(null, d.ColumnName);
        system.assertEquals(null, d.Field_api_name);
        system.assertEquals(null, d.Order);
        system.assertEquals(null, d.fromasc);
        system.assertEquals(null, d.defaultorder);
        system.assertEquals(null, d.fieldtype);
        system.assertEquals(null, d.referenceFieldName);
        system.assertEquals(null, d.isCustomField);
        system.assertEquals(null, d.sortable);
		}
	}

}