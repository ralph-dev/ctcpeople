/**
 * This is the class to call Job Posting Web Service
 *
 * Created by: Lina Wei
 * Created on: 03/03/2017
 */

public with sharing class JobPostingWebService {

    @future(callout=true)
    public static void sendRequest(String endpoint, List<String> bodyList, Map<String, String> headers){
        for (String body : bodyList) {
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            //System.debug('Request Body: ' + body);
            req.setEndpoint(endpoint);
            req.setMethod('POST');
            for (String name : headers.keySet()) {
                req.setHeader(name, headers.get(name));
            }
            req.setTimeout(120000);
            req.setBody(body);
            HttpResponse res = new HttpResponse();
            if (!Test.isRunningTest()) {
                res = h.send(req);
            } else {
                res.setStatusCode(200);
                res.setBody('Successfully');
            }
            Integer responsecode = res.getStatusCode();
        }
    }
}