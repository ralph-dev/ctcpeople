@isTest
private class CandidateDTOTest {
	
	@isTest static void candidateDTOTest() {
		Contact dummycon = TestDataFactoryRoster.createContact();
		candidateDTO conDTO = new candidateDTO();
        conDTO.setCandidate(dummycon);
        conDTO.setRadiusRanking(1);
        conDTO.setCandidateId(dummycon.id);
        conDTO.setIsSelect(true);
        conDTO.setIsPlaced(true);
        conDTO.setEventStatus('Pending');
        conDTO.setAvailablityRanking(1);
        conDTO.setNumberOfShifts(1);
        conDTO.setNumberOfRoster(1);
        conDTO.setAvailabilityPercentage(0.5);
        conDTO.setCreateRosteringStatus('Success');
        Integer returnResult = conDTO.compareTo(conDTO);

        System.assertNotEquals(conDTO.getCandidateId(),'');
        System.assertNotEquals(conDTO.getUnavailableBinaryInteger(),null);
        System.assertNotEquals(conDTO.getAvailableBinaryInteger(),null);
        System.assertEquals(conDTO.getNumberOfShifts(),1);
        System.assertEquals(conDTO.getNumberOfRoster(),1);
        System.assertEquals(conDTO.getAvailabilityPercentage(),0.5);
        System.assertNotEquals(conDTO.getCandidate(),null);
        System.assertEquals(conDTO.getRadiusRanking(),1);
        System.assertEquals(conDTO.getAvailablityRanking(),1);
        System.assertEquals(conDTO.getIsSelect(),true);
        System.assertEquals(conDTO.getEventStatus(),'Pending');
        System.assertEquals(conDTO.getIsPlaced(),true);
        System.assertEquals(conDTO.getCreateRosteringStatus(),'Success');
        System.assertEquals(returnResult,0);
	}
}