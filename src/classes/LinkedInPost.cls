/**
 * CPE-1082: Updated signout function to solve the CSRF issue, 13/09/2017, added by Zoe
 * 
 **/
global with sharing class LinkedInPost {
	public Advertisement__c linkedInad{get; set;}
	public Advertisement__c adTemplate{get ;set;}
	public Boolean isTabMode{get;set;}
	public Boolean isEdit{get; set;}
	public Boolean isClone{get; set;}
	public String action;
	public List<SelectOption> styleCategorySelectOption {get; set;} //get application form picklist value
	public Boolean styleCategoryexist; //check the style category exist
	public List<SelectOption> selectSkillGroupList {get; set;}
	
	/*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}    
    /*************End Add skill parsing param********************************************************/
    
    /*************Generate LinkedIn Posting Select option********************************************/
    public List<SelectOption> linkedinjobcountrySelectOption {get; set;}
	//public List<SelectOption> linkedinJobPostalCodeSelectOption {get; set;}
	public List<SelectOption> linkedinJobFunctionSelectOption {get; set;}
	public List<SelectOption> linkedinJobIndustrySelectOption {get; set;}
	public List<SelectOption> linkedinJobExperienceLevelSelectOption {get; set;}
	public List<SelectOption> linkedinJobTypeSelectOption {get; set;}
	public List<SelectOption> linkedinPostingRoleSelectOption {get; set;}
	public boolean seletoptioncorrect{get;set;}
	/*************END Generate LinkedIn Posting Select option******************************************/
	
	public static String linkedInPostURL = 'https://api.linkedin.com/v1/jobs';
	public Boolean updateadvertisement = false;
	public static Map<String, LinkedinCountryCode> linkedincountryvalue;
	public Boolean linkedInAccountEnable{get; set;}  //check LinkedIn Account detail current or not
	
	public String adtemplateid; //Tempory ad id
	public Integer msgcode{get;set;}
	public String vacancyid;
	
	public User currentlyuser;  //Current user detail
	//Init Error Message for customer
    public PeopleCloudErrorInfo errormesg; 
    public Boolean msgSignalhasmessageLinkedin{get ;set;} //show error message or not
    
    public Boolean LinkedInValidation {get; set;} //Check the linkedInValdate
    public Boolean linkedinoauthEnable {get; set;}
    public String linkedInAuthPageUrl{get; set;}
    public String linkedInPostalcode {get;set;}   //Add the postal code to URL
    public List<String> linkedInJobFunction {get; set;}  //Store Linkedin Job function
    public List<String> linkedInJobIndustry {get; set;}  //Store LInkedin Job Industry
    public String retUrl;
    
	public String pickListFieldsString;

	/**
	* UPDATES FOR CKEDITOR 
	*
	public String jobContent{get; set;}
	public String skillsExperience{get; set;}
	public String companyDescription{get; set;}
	*/
	
	
	public LinkedInPost(ApexPages.StandardController stdController){
		pickListFieldsString = LinkedInPostUtil.getPickListFieldMapJsonString();
		isTabMode = true;
		isEdit = false;
		isClone = false;
		styleCategoryexist = false;
		linkedInAccountEnable = false;
		//Init skill parsing param. Add by Jack on 23/05/2013
   		enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
        enableSkillGroupList = new List<Skill_Group__c>();
        enableSkillGroupList = SkillGroups.getSkillGroups();
        defaultskillgroups = new List<String>();
        //end skill parsing param.
        
        //Get User detail 
        UserInformationCon usercls = new UserInformationCon();
		currentlyuser = usercls.getUserDetail();
		
        //init LinkedIn Multi picklist field value
        linkedInJobIndustry = new List<String>();
        linkedinJobFunction = new List<String>();
        //end LinkedIn Multi picklist field value
        
        retUrl = EncodingUtil.urlEncode(ApexPages.currentPage().getURL(),'UTF-8'); //Init retUrl
        //Get ad template according the id
        Set<String> ExcludeFieldsForJobPosting = new Set<String>();//Fields exclude for job posting
        adTemplate = new Advertisement__c();
        
        adtemplateid = ApexPages.currentPage().getParameters().get('id');
        action = ApexPages.currentPage().getParameters().get('action');
        vacancyid = ApexPages.currentPage().getParameters().get('vid');

        adTemplate = (Advertisement__c) stdController.getRecord();
        adTemplate = DaoAdvertisement.getAdvertisementById(adTemplate.id, ExcludeFieldsForJobPosting);
        //system.debug('adTemplate2 ='+ adTemplate);
        if(vacancyid == null || vacancyid == ''){ //set vacancy id
        	vacancyid = adTemplate.Vacancy__c;
        }else{
        	isTabMode = false;
        }
        
	}
	public void init(){
		linkedInAccountEnable = LinkedInPostUtil.checklinkedInAccountEnable();  //Check LinkedIn Account Enable
        LinkedInPostUtil templpu = new LinkedInPostUtil();
        linkedinoauthEnable = templpu.signinlinkedinoauth();                    //Check Customer Oauth Service enable
       	
        system.debug('linkedInAccountEnable ='+ linkedInAccountEnable+'linkedinoauthEnable='+ linkedinoauthEnable);
        system.debug('adTemplate ='+ adTemplate.id);
        if(linkedInAccountEnable && linkedinoauthEnable){
        	if(action!=null && action.equalsIgnoreCase('edit')){
        		isTabMode = false;
	       		isEdit = true; //Job Posting status is Posted Successfully, Ready To Post, Posting Failed or Clone to Post can be edit;
	        	if(adTemplate.Job_Posting_Status__c!=null && adTemplate.Status__c!= null && adTemplate.Status__c == 'active' && (adTemplate.Job_Posting_Status__c.equals('Posted Successfully') || adTemplate.Job_Posting_Status__c.equals('Ready To Post') ||
	        			adTemplate.Job_Posting_Status__c.contains('Posting Failed') || adTemplate.Job_Posting_Status__c.equals('Clone to Post'))){
	        		
		        }else{
		        	linkedInAccountEnable = false;
		        	customerErrorMessage(PeopleCloudErrorInfo.LINKEDIN_UPDATE_FAILED);
		        }
		    }
		    if(action!=null && action.equalsIgnoreCase('clone')){
		    	isTabMode = false;
	       		isEdit = true; //Job Posting status is Posted Successfully, Ready To Post, Posting Failed or Clone to Post can be edit;
	        	isClone = true;
	        	if(adTemplate.Job_Posting_Status__c.equals('Clone to Post')){
	        		
		        }else{
		        	linkedInAccountEnable = false;
		        	customerErrorMessage(PeopleCloudErrorInfo.LINKEDIN_UPDATE_FAILED);
		        }
		    }
		    if(adtemplateid != null && adtemplateid != ''){
		        if(adTemplate.Skill_Group_Criteria__c!=null&&adTemplate.Skill_Group_Criteria__c!=''){
			    	String defaultskillgroupstring = adTemplate.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
				    defaultskillgroups = defaultskillgroupstring.split(',');
			    }
			   	if(adTemplate.Country__c == null || adTemplate.Country__c == ''){
			   		adTemplate.Country__c = LinkedInPostUtil.getUserLinkedInCounry(pickListFieldsString);
			   	}
			   	linkedInPostalcode = getlinkedinCountryCodeByFiedlString(adTemplate.Country__c, 'linkedincountry', pickListFieldsString).code;//set default country code is au
			   	if(adTemplate.Job_Function__c!= null && adTemplate.Job_Function__c!= '')
			   		linkedInJobFunction = adTemplate.Job_Function__c.split(',');
			   	if(adTemplate.Industry__c!= null && adTemplate.Industry__c!= '')
			   		linkedInJobIndustry = adTemplate.Industry__c.split(',');
			}else{
		    	if(adTemplate.Country__c == null || adTemplate.Country__c == ''){
			   		adTemplate.Country__c = LinkedInPostUtil.getUserLinkedInCounry(pickListFieldsString);
			   	}
			   	linkedInPostalcode = getlinkedinCountryCodeByFiedlString(adTemplate.Country__c, 'linkedincountry' ,pickListFieldsString).code;//set default country code is au    	
		    }
		    
		    if(adTemplate.Company_Description__c == null || adTemplate.Company_Description__c == ''){//Set default company description
				adTemplate.Company_Description__c = currentlyuser.Company_Description__c;
				system.debug('Company_Description__c ='+ adTemplate.Company_Description__c);
		    }
		    //Get application form picklist value
		    styleCategorySelectOption = new List<SelectOption>();
		    styleCategorySelectOption = JobBoardUtils.getStyleApplicationFormSelectOption();
		    if(styleCategorySelectOption!= null && styleCategorySelectOption.size()>0){
		    	styleCategoryexist = true;
		    }        
		    selectSkillGroupList = new List<SelectOption>();
		    selectSkillGroupList = JobBoardUtils.getSelectSkillGroupList();
		        
		    initSelectOption();//Init the select option
        }else{
        	if(!linkedInAccountEnable){
        		customerErrorMessage(PeopleCloudErrorInfo.LINKEDIN_JOBBOARD_ACCOUT);
        	}
        	if(!linkedinoauthEnable){
        		linkedInAuthPageUrl = templpu.linkedInAuthPageUrl;
        		//system.debug('linkedInAuthPageUrl ='+ linkedInAuthPageUrl);
        		customerErrorMessage(templpu.linkedInAuthPageResponsecode);                                   
		    }
        }
	}
	//Generate SelectOption according the JSON string from Ec2 Server
	public void initSelectOption(){
		linkedincountryvalue = new Map<String, LinkedinCountryCode>();
		LinkedInPostUtil tempLinkedInPostUtil = new LinkedInPostUtil();
        tempLinkedInPostUtil.getLinkedInPostUtil(pickListFieldsString);
        linkedinjobcountrySelectOption = (tempLinkedInPostUtil.linkedinjobcountrySelectOption);
		linkedinJobFunctionSelectOption = tempLinkedInPostUtil.linkedinJobFunctionSelectOption;
		linkedinJobIndustrySelectOption = tempLinkedInPostUtil.linkedinJobIndustrySelectOption;
		linkedinJobExperienceLevelSelectOption = tempLinkedInPostUtil.linkedinJobExperienceLevelSelectOption;
		linkedinJobTypeSelectOption = tempLinkedInPostUtil.linkedinJobTypeSelectOption;
		linkedinPostingRoleSelectOption = tempLinkedInPostUtil.linkedinPostingRoleSelectOption;
		seletoptioncorrect = tempLinkedInPostUtil.seletoptioncorrect;
		linkedincountryvalue = tempLinkedInPostUtil.linkedincountryvalue;
		//system.debug('linkedincountryvalue='+ linkedincountryvalue);
	}
	
	/**
	* UPDATES FOR CKEDITOR 
	
	private void assignRichTextContent(Advertisement__c ad){
		ad.Job_Content__c = jobContent;
		ad.Skills_Experience__c = skillsExperience;
		ad.Company_Description__c = companyDescription;
		
	}
	*/
	
	//Save ad as template
	public PageReference saveadastemplate(){
		/**
		* UPDATES FOR CKEDITOR 
		assignRichTextContent(adTemplate);
		*/
		
		linkedInad = adTemplate.clone(false, true);
		linkedInad.Status__c = '';
		linkedInad.Job_Posting_Status__c = '';
		linkedInad.LinkedIn_Account__c = currentlyuser.LinkedIn_Account__c;
		linkedInad.Vacancy__c = vacancyid;
		linkedInad.RecordTypeId = JobBoardUtils.getRecordId('Template_Record');
		//Add Skill Group to linkedIn Ad
		if(enableskillparsing){
			String tempstring = JobBoardUtils.removeListTrim(defaultskillgroups);
	        linkedInad.Skill_Group_Criteria__c = LinkedInPostUtil.removeMultipicklistSquare(tempstring);
	    }
	    if(linkedInJobFunction != null)
			linkedInad.Job_Function__c = JobBoardUtils.removeListTrim(linkedInJobFunction); //Add the job function value to function field
		if(linkedInJobIndustry != null)
			linkedInad.Industry__c= JobBoardUtils.removeListTrim(linkedInJobIndustry);     //Add the job industry value to industry field
		try{
			if(!Test.isRunningTest()){
				checkFLS();
			}
			
			insert linkedInad;
			customerErrorMessage(PeopleCloudErrorInfo.SAVE_AD_SUCCESS);
		}catch(Exception e){
			customerErrorMessage(PeopleCloudErrorInfo.SAVE_AD_FAILED);
		}
		return null;
	}
	
	//Save advertisement
	public PageReference saveAd(){
		/**
		* UPDATES FOR CKEDITOR 
		assignRichTextContent(adTemplate);
		*/
		
		linkedInad = adTemplate.clone(false, true);
		linkedInad.Status__c = 'active';
		linkedInad.Job_Posting_Status__c = 'Ready To Post';
		linkedInad.WebSite__c = 'LinkedIn';
		linkedInad.LinkedIn_Account__c = currentlyuser.LinkedIn_Account__c;
		linkedInad.Vacancy__c = vacancyid;
		linkedInad.RecordTypeId = JobBoardUtils.getRecordId('LinkedIn');
		//Add Skill Group to linkedIn Ad
		if(enableskillparsing){
			String tempstring = JobBoardUtils.removeListTrim(defaultskillgroups);
	        linkedInad.Skill_Group_Criteria__c = LinkedInPostUtil.removeMultipicklistSquare(tempstring);
	    }
	    if(linkedInJobFunction != null)
			linkedInad.Job_Function__c = JobBoardUtils.removeListTrim(linkedInJobFunction); //Add the job function value to function field
		if(linkedInJobIndustry != null)
			linkedInad.Industry__c= JobBoardUtils.removeListTrim(linkedInJobIndustry);     //Add the job industry value to industry field
		try{
			if(!Test.isRunningTest()){
				checkFLS();
			}
			
			insert linkedInad;
			//String successreturnurl = URL.getSalesforceBaseUrl().toExternalForm() +'/'+ linkedInad.id;
			String extraerror = '! Please click<a onclick="goToRec(\''+linkedInad.id+'\')" style="font-size:12px;">Here</a>to check this ad';
			customerErrorMessage(PeopleCloudErrorInfo.SAVE_AD_SUCCESS, extraerror);
		}catch(Exception e){
			customerErrorMessage(PeopleCloudErrorInfo.SAVE_AD_FAILED);
		}
		system.debug('-----------------insert'+linkedInad.id);
		return null;
	}

	//Update LinkedIn Ad
	public PageReference updateAd(){
		system.debug('---------------updatead');
		
		/**
		* UPDATES FOR CKEDITOR 
		assignRichTextContent(adTemplate);
		*/
		
		linkedInad = adTemplate;
		if(enableskillparsing){			
			String tempstring = JobBoardUtils.removeListTrim(defaultskillgroups);
	        linkedInad.Skill_Group_Criteria__c = LinkedInPostUtil.removeMultipicklistSquare(tempstring);
	    }
	    if(linkedInJobFunction != null)
			linkedInad.Job_Function__c = JobBoardUtils.removeListTrim(linkedInJobFunction); //Add the job function value to function field
	    if(linkedInJobIndustry != null)
			linkedInad.Industry__c= JobBoardUtils.removeListTrim(linkedInJobIndustry);      //Add the job industry value to industry field
		try{
			checkFLS();
			update linkedInad;
			customerErrorMessage(PeopleCloudErrorInfo.SAVE_AD_SUCCESS);
		}catch(Exception e){
			customerErrorMessage(PeopleCloudErrorInfo.SAVE_AD_FAILED);
		}
		return null;
	}
	
	//Post LinkedIn Ad
	public pageReference postad(){
		system.debug('----------------------postad');
		Boolean isEditJustEdit = false;
		String posturl = linkedInPostURL;
		String method = 'POST';
		if(isEdit){
			if( adTemplate.Job_Posting_Status__c.equals('Posted Successfully')){
				isEditJustEdit = true;
				posturl = linkedInPostURL + '/partner-job-id='+userInfo.getOrganizationId().subString(0, 15)+':'+String.valueOf(linkedInad.id).subString(0, 15) ;
				method = 'PUT';
			}
		}
		
		
		String requestbody = LinkedInGenerateXMLFeed.generateXMLFeed(linkedInad.id,isEditJustEdit);
		 
		Integer postLinked = LinkedInPostExecute.fetchLinkedInPostExecute(requestbody, method, posturl);
		Set<String> ExcludeFieldsForJobPosting = new Set<String>();
		Advertisement__c tempad = DaoAdvertisement.getAdvertisementById(linkedInad.id, ExcludeFieldsForJobPosting);
		if(postLinked == 201 || postLinked == 200){
			tempad.Placement_Date__c = system.today();
			tempad.Job_Posting_Status__c = 'Posted Successfully';
			//String successreturnurl = URL.getSalesforceBaseUrl().toExternalForm() +'/'+ linkedInad.id;
			String extraerror = '! Please click<a onclick="goToRec(\''+linkedInad.id+'\')" style="font-size:12px;">Here</a>to check this ad';
			customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAP_JOB_POST_SUCCESS, extraerror);
		}else{
			if(postLinked == 400){
				tempad.Job_Posting_Status__c = 'Posting Failed (BAD REQUEST)'; //change the status, status contains the more details
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_BAD_REQUEST);
			}else if(postLinked == 401){
				tempad.Job_Posting_Status__c = 'Posting Failed (UNAUTHORIZED)'; 
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_UNAUTHORIZED);
			}else if(postLinked == 402){
				tempad.Job_Posting_Status__c = 'Posting Failed (PAYMENT REQUIRED)'; 
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_PAYMENT_REQUIRED);
			}else if(postLinked == 403){
				tempad.Job_Posting_Status__c = 'Posting Failed (POSTAL CODE FAILED)';
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDIN_POSTAL_CODE_FAILED);
			}else if(postLinked == 404){
				tempad.Job_Posting_Status__c = 'Posting Failed (NO JOB AD FOUND)';
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_NO_JOB_AD_FOUND);
			}else if(postLinked == 409){
				tempad.Job_Posting_Status__c = 'Posting Failed (DUPLICATE JOB)';
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_DUPLICATE_JOB);
			}else {
				tempad.Job_Posting_Status__c = 'Posting Failed (OTHER REASON)';
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAP_JOB_POST_FAILED);
			}
		}
		if(!Test.isRunningTest()){
			checkFLS();
		}
		
		update tempad;
		return null;
	}
	
	//Insert dynamic application form
	@RemoteAction
	public static Integer insertEc2(String linkedinadid){
		Set<String> ExcludeFieldsForJobPosting = new Set<String>();//Fields exclude for job posting
		Advertisement__c linkedInad = DaoAdvertisement.getAdvertisementById(linkedinadid, ExcludeFieldsForJobPosting);
		Integer responsecode;
        User tempuser = UserInformationCon.getUserDetails;
		String bucketname = tempuser.AmazonS3_Folder__c;  //get bucket name
		IEC2WebService iec2ws = new EC2WebServiceImpl();
        
        String jobRefCode=UserInfo.getOrganizationId().subString(0, 15)+':'+String.valueOf(linkedInad.id).substring(0, 15);  //generate job reference code
        
        CommonSelector.checkRead(StyleCategory__c.sObjectType,
        	'Id, Header_EC2_File__c, Header_File_Type__c,  '
            +'Div_Html_EC2_File__c, Div_Html_File_Type__c, '
            +'Footer_EC2_File__c, Footer_File_Type__c  ');
        StyleCategory__c sc = [select Id, Header_EC2_File__c, Header_File_Type__c,   //get application form style
            Div_Html_EC2_File__c, Div_Html_File_Type__c, 
             Footer_EC2_File__c, Footer_File_Type__c  
                from StyleCategory__c where id = :linkedInad.Application_Form_Style__c limit 1];
		
		if(linkedInad.Application_URL__c == null || linkedInad.Application_URL__c == ''){						  //if application url not exist, insert again.
	        boolean result = iec2ws.insertDetailTable(jobRefCode, UserInfo.getOrganizationId().subString(0, 15),  //generate the dynamic application form at ec2 server
	            String.valueOf(linkedInad.id).substring(0, 15), bucketname, JobBoardUtils.blankValue(sc.Header_EC2_File__c), 
	              JobBoardUtils.blankValue(sc.Header_File_Type__c), sc.Div_Html_EC2_File__c,
	                sc.Div_Html_File_Type__c,  JobBoardUtils.blankValue(sc.Footer_EC2_File__c), 
	                 JobBoardUtils.blankValue(sc.Footer_File_Type__c), '', 'linkedin', 
	                     JobBoardUtils.blankValue(linkedInad.Job_Title__c));
	        if(result){
	            linkedInad.Status__c = 'Active';
	            linkedInad.Application_URL__c = Endpoint.getcpewsEndpoint()+'/peoplecloud/GetApplicationForm?jobRefCode='+jobRefCode+'&website=linkedin';  //generate the url
	            responsecode = PeopleCloudErrorInfo.INSERT_APPLICATIONFORM_SUCCESS;
	        }else{
	        	linkedInad.Status__c = 'Not Valid';
	        	responsecode = PeopleCloudErrorInfo.INSERT_APPLICATIONFORM_FAILED;
	        }
        }else{
        	//Integer result = iec2ws.updateDetailTable(                                   //generate the dynamic application form at ec2 server; aid, hname, htype, dname, dtype, fname, ftype,iid
         //   String.valueOf(linkedInad.id).substring(0, 15), JobBoardUtils.blankValue(sc.Header_EC2_File__c), 
         //     JobBoardUtils.blankValue(sc.Header_File_Type__c), sc.Div_Html_EC2_File__c,
         //       sc.Div_Html_File_Type__c,  JobBoardUtils.blankValue(sc.Footer_EC2_File__c), 
         //        JobBoardUtils.blankValue(sc.Footer_File_Type__c), UserInfo.getOrganizationId().subString(0, 15));
        	Integer result = iec2ws.updateDetailTable(                                   //generate the dynamic application form at ec2 server; aid, hname, htype, dname, dtype, fname, ftype,iid
            String.valueOf(linkedInad.id).substring(0, 15), JobBoardUtils.blankValue(sc.Header_EC2_File__c), 
              JobBoardUtils.blankValue(sc.Header_File_Type__c), sc.Div_Html_EC2_File__c,
                sc.Div_Html_File_Type__c,  JobBoardUtils.blankValue(sc.Footer_EC2_File__c), 
                 JobBoardUtils.blankValue(sc.Footer_File_Type__c), UserInfo.getOrganizationId().subString(0, 15), JobBoardUtils.blankValue(linkedInad.Job_Title__c));
            //system.debug('result ='+ result);
			if(result == 1){
	            responsecode = PeopleCloudErrorInfo.INSERT_APPLICATIONFORM_SUCCESS;
	        }else{
	        	responsecode = PeopleCloudErrorInfo.INSERT_APPLICATIONFORM_FAILED;
	        }
        }
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
        	Advertisement__c.Status__c,
        	Advertisement__c.Application_URL__c
        }; 
        fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType,fieldList);
        update linkedInad;
        return responsecode;
    }
    
    public static LinkedinCountryCode getlinkedinCountryCodeByFiedlString(String countrycode, String mapkey, String pickListFieldsString){
    	Map<String, LinkedinCountryCode> linkedinCountryCode = new Map<String, LinkedinCountryCode>();
    	LinkedinCountryCode templinkedincountrycode = new LinkedinCountryCode();
    	linkedinCountryCode = LinkedInPostUtil.getlinkedinCountryCode(pickListFieldsString, mapkey);
    	templinkedincountrycode = linkedinCountryCode.get(countrycode);
    	system.debug('templinkedincountrycode='+ templinkedincountrycode);
    	return templinkedincountrycode;
    }
    
    
    @RemoteAction
    public static LinkedinCountryCode getlinkedinCountryCode(String countrycode, String mapkey){
    	String pickListFieldsString = LinkedInPostUtil.getPickListFieldMapJsonString();
    	Map<String, LinkedinCountryCode> linkedinCountryCode = new Map<String, LinkedinCountryCode>();
    	LinkedinCountryCode templinkedincountrycode = new LinkedinCountryCode();
    	linkedinCountryCode = LinkedInPostUtil.getlinkedinCountryCode(pickListFieldsString, mapkey);
    	templinkedincountrycode = linkedinCountryCode.get(countrycode);
    	//system.debug('templinkedincountrycode='+ templinkedincountrycode);
    	return templinkedincountrycode;
    }
    
    @RemoteAction
    public static boolean LinkedinSaveCompanyDescriptionAsDefault(String CompanyDescription){
    	Boolean updateSuccess = false;
    	UserInformationCon usercls = new UserInformationCon();
		User currentlyuser = usercls.getUserDetail();
    	currentlyuser.Company_Description__c = CompanyDescription;
    	try{
    		fflib_SecurityUtils.checkFieldIsUpdateable(User.SObjectType, User.Company_Description__c);
    		update currentlyuser;
    		updateSuccess = true;
    	}
    	catch(Exception e){
    		updateSuccess = false;
    	}
    	return updateSuccess;
    }
    
    public PageReference signout(){  //Sign out the LinkedIn Oauth service
        String oauthurl = 'https://api.linkedin.com/uas/oauth/invalidateToken'; //Token Invalidation
        String method = 'GET';
        String requestbody = '';
      	Integer postLinked = LinkedInPostExecute.fetchLinkedInPostExecute(requestbody, method, oauthurl);
        
        if(!Test.isRunningTest()){
        	OAuth_Service__c service = (OAuth_Service__c) new CommonSelector(OAuth_Service__c.sObjectType)
        		.addSubselect(OAuth_Token__c.sObjectType,'id','Owner__c=: userId')
        		.setParam('userId',UserInfo.getUserId() )
        		.getOne('id','name = \'linkedin\' and Active__c=true');
        		
	        //OAuth_Service__c service = [SELECT id,(select id FROM tokens__r WHERE Owner__c=:UserInfo.getUserId() ) //Delete the Token Record
	        //                  FROM OAuth_Service__c WHERE name = 'linkedin'];
	        
	        
	         try{
            	OAuth_Token__c delT = service.tokens__r;
	            //
	            if(delT != null){
	                CredentialSettingService.deleteProtectedAccount(delT);
	                
	            }
	        }catch(Exception e){
	         	System.debug(e);   
	        }
        }
        String linkedInAuthPageUrl = Page.AuthPage.getURL()+'?retUrl='+ URL.getSalesforceBaseUrl().toExternalForm() + retUrl;
        PageReference returnPage= new PageReference(linkedInAuthPageUrl);
        
         
        // Redirect to the previous page with login to LinkedIn link (JobBoards page)
        // CPE-1082, 13/09/2017, added by Zoe
        String newReturnUrl = returnPage.getParameters().get('retUrl');
       	return new PageReference(newReturnUrl);
    }
    
    public void customerErrorMessage(Integer msgcode){
   		errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        	msgSignalhasmessageLinkedin = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
            	ApexPages.addMessage(newMessage);
            }               
    }
    
    public void customerErrorMessage(Integer msgcode, String extraError){
   		errormesg = new PeopleCloudErrorInfo(msgcode,extraError,true);                                     
        	msgSignalhasmessageLinkedin = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
            	ApexPages.addMessage(newMessage);
            }               
    }
    
    public PageReference customerErrorMessage(){
    	system.debug('msgcode ='+ msgcode);
   		errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        	msgSignalhasmessageLinkedin = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
            	ApexPages.addMessage(newMessage);
            } 
        return null;              
    }

    private void checkFLS(){
      List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
      		Advertisement__c.Company_Description__c,
      		Advertisement__c.Country__c,
      		Advertisement__c.Industry__c,
            Advertisement__c.Application_URL__c,
            Advertisement__c.ListingID__c,
            Advertisement__c.Ad_Closing_Date__c,
            Advertisement__c.Placement_Date__c,
            Advertisement__c.Advertisement_Account__c,
            Advertisement__c.Indeed_Question_URL__c,
            Advertisement__c.Vacancy__c,
            Advertisement__c.Skill_Group_Criteria__c,
            Advertisement__c.Job_Type__c,
            Advertisement__c.Job_Title__c,
            Advertisement__c.JXT_Short_Description__c,
            Advertisement__c.Bullet_1__c,
            Advertisement__c.Bullet_2__c,
            Advertisement__c.Bullet_3__c,
            Advertisement__c.Job_Content__c,
            Advertisement__c.Job_Contact_Name__c,
            Advertisement__c.Company_Name__c,
            Advertisement__c.Nearest_Transport__c,
            Advertisement__c.Residency_Required__c,
            Advertisement__c.IsQualificationsRecognised__c,
            Advertisement__c.Location_Hide__c,
            Advertisement__c.TemplateCode__c,
            Advertisement__c.AdvertiserJobTemplateLogoCode__c,
            Advertisement__c.ClassificationCode__c,
            Advertisement__c.SubClassificationCode__c,
            Advertisement__c.Classification2Code__c,
            Advertisement__c.SubClassification2Code__c,
            Advertisement__c.WorkTypeCode__c,
            Advertisement__c.SectorCode__c,
            Advertisement__c.Street_Adress__c,
            Advertisement__c.Search_Tags__c,
            Advertisement__c.CountryCode__c,
            Advertisement__c.LocationCode__c,
            Advertisement__c.AreaCode__c,
            Advertisement__c.Salary_Type__c,
            Advertisement__c.SeekSalaryMin__c,
            Advertisement__c.SeekSalaryMax__c,
            Advertisement__c.Salary_description__c,
            Advertisement__c.No_salary_information__c,
            Advertisement__c.Has_Referral_Fee__c,
            Advertisement__c.Referral_Fee__c,
            Advertisement__c.Terms_And_Conditions_Url__c,
            Advertisement__c.Status__c,
            Advertisement__c.Job_Posting_Status__c,
            Advertisement__c.JobXML__c,
            Advertisement__c.Prefer_Email_Address__c,
            Advertisement__c.RecordTypeId,
            Advertisement__c.Website__C
      };

      fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
      fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    } 
}