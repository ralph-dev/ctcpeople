@isTest
private class ClientExtensionGlobalTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		Account acc = TestDataFactoryRoster.createAccount();
       
	       //test method searchClient(String keyword)
	       String keyword = '';
	       List<Account> accListFetchedByKeyword = ClientExtensionGlobal.searchClient(keyword);
	       System.assertNotEquals(accListFetchedByKeyword,null);
	       
	       //test method getClient(String Id)
	       Account accFetchedById = ClientExtensionGlobal.getClient(acc.Id);
	       System.assertNotEquals(accFetchedById,null);
    	}
       
    }
}