@isTest
private class TestAvailabilityRequestHandler{

	// test creating a new avl record
    static testMethod void testCreateNewAvlRec(){
    	System.runAs(DummyRecordCreator.platformUser) {
        AvailabilityRequestHandler arh = new AvailabilityRequestHandler();	
    	Contact candidate = createTestCandidate();
    	String candidateId = candidate.Id;	
    	String startDate = '2013-12-01';
    	String endDate = '2013-12-15';
    	String eventStatus = 'unavailable';
    	createTestAvlRecords(startDate, endDate, eventStatus, candidateId);	// create a dummy in the first place

    	String newAvlJsonString ='[{"title":"available","startDate":"2013-12-01","endDate":"2013-12-05","eventStatus":"available","candidateId":"' + candidateId + '"}]';
    	String displayRangeJsonString ='[{"startDate":"2013-11-01","endDate":"2013-12-31","candidateId":"' + candidateId + '"}]';
    	boolean dupCheck = true;
    	
		String result = AvailabilityRequestHandler.createAvlRecords(newAvlJsonString, displayRangeJsonString,dupCheck);	
    	//System.debug('---------------- result:' + result);
    	System.assert(result.contains('success'));
    	}
    }
    
   	// test creating a new avl record with 'tentative'
    static testMethod void testCreateNewAvlRecWithTentativeType(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate();
    	String candidateId = candidate.Id;	
    	String startDate = '2013-12-01';
    	String endDate = '2013-12-15';
    	String eventStatus = 'unavailable';
    	createTestAvlRecords(startDate, endDate, eventStatus, candidateId);	// create a dummy in the first place

    	String newAvlJsonString ='[{"title":"tentative","startDate":"2013-12-01","endDate":"2013-12-05","eventStatus":"tentative","candidateId":"' + candidateId + '"}]';
    	String displayRangeJsonString ='[{"startDate":"2013-11-01","endDate":"2013-12-31","candidateId":"' + candidateId + '"}]';
    	boolean dupCheck = true;
    	
		String result = AvailabilityRequestHandler.createAvlRecords(newAvlJsonString, displayRangeJsonString,dupCheck);	
    	//System.debug('---------------- result:' + result);
    	System.assert(result.contains('success'));
    	}
    }

    // test creating a duplicate avl record
    static testMethod void testCreateDuplicateNewAvlRec(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate();
   		String candidateId = candidate.Id;	
    	String startDate = '2013-09-01';
    	String endDate = '2013-09-15';
    	String eventStatus = 'available';
    	createTestAvlRecords(startDate, endDate, eventStatus, candidateId);	// create a dummy in the first place
    	
    	
    	String newAvlJsonString ='[{"title":"available","startDate":"2013-09-01","endDate":"2013-09-05","eventStatus":"available","candidateId":"' + candidateId + '"}]';
    	String displayRangeJsonString ='[{"startDate":"2013-09-01","endDate":"2013-10-31","candidateId":"' + candidateId + '"}]';
    	boolean dupCheck = true;
    	
    	String result = AvailabilityRequestHandler.createAvlRecords(newAvlJsonString, displayRangeJsonString,dupCheck);	
    	System.assert(result.contains('duplicate Available'));
    	}
    }
    
    
    // test updating an existing avl record
    static testMethod void testUpdateAvlRec(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 	
    	String candidateId = candidate.Id;
    	String startDate = '2013-09-01';
    	String endDate = '2013-09-15';
    	String eventStatus = 'available';
    	String testJsonString ='[{"title":"Unavailable","startDate":"2013-09-02","endDate":"2013-09-15","eventStatus":"unavailable","candidateId":"' + candidateId + '"}]';
    	String displayRangeJsonString ='[{"startDate":"2013-11-01","endDate":"2013-12-31","candidateId":"' + candidateId + '"}]';
    	boolean dupCheck = false;
    	Days_Unavailable__c originalAvl = createTestAvlRecords(startDate,endDate,eventStatus,candidateId);
    	String result =  AvailabilityRequestHandler.updateAvlRecords(originalAvl.Id+'', testJsonString,  displayRangeJsonString, dupCheck);	
    	System.assert(result.contains('success'));
    	}
    }
    
    // test updating an existing avl record to tentative
    static testMethod void testUpdateAvlRec2TentativeType(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 	
    	String candidateId = candidate.Id;
    	String startDate = '2013-09-01';
    	String endDate = '2013-09-15';
    	String eventStatus = 'available';
    	String updateJsonString ='[{"title":"tentative","startDate":"2013-09-02","endDate":"2013-09-15","eventStatus":"tentative","candidateId":"' + candidateId + '"}]';
    	String displayRangeJsonString ='[{"startDate":"2013-11-01","endDate":"2013-12-31","candidateId":"' + candidateId + '"}]';
    	boolean dupCheck = false;
    	
    	Days_Unavailable__c originalAvl = createTestAvlRecords(startDate,endDate,eventStatus,candidateId);
    	String result =  AvailabilityRequestHandler.updateAvlRecords(originalAvl.Id+'', updateJsonString,  displayRangeJsonString, dupCheck);	
    	System.assert(result.contains('success'));
    	}
    }
    
    
    
    // test updating an avl record by creating a duplicate record
    static testMethod void testUpdateDuplicateRecord(){
    
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 	
   		String candidateId = candidate.Id;	
    	String startDate = '2013-09-01';
    	String endDate = '2013-09-15';
    	String eventStatus = 'available';
    	createTestAvlRecords(startDate, endDate, eventStatus, candidateId);	// create a dummy in the first place	
    	
    	
    	String startDate2 = '2013-10-01';
    	String endDate2 = '2013-10-15';
    	Days_Unavailable__c originalAvl = createTestAvlRecords(startDate2, endDate2, eventStatus, candidateId);	// create the 2nd dummy in the first place	
    	
    	String avlJsonString ='[{"title":"available","startDate":"2013-09-01","endDate":"2013-09-05","eventStatus":"available","candidateId":"' + candidateId + '"}]';
    	String displayRangeJsonString ='[{"startDate":"2013-09-01","endDate":"2013-10-31","candidateId":"' + candidateId + '"}]';
    	boolean dupCheck = true;
    	String result =  AvailabilityRequestHandler.updateAvlRecords(originalAvl.Id+'', avlJsonString,  displayRangeJsonString, dupCheck);	
    	System.assert(result.contains('duplicate Available'));
    	}
    }
    
    // test deleting an existing avl record
    static testMethod void testDeleteAvlRec(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 		
    	String candidateId = candidate.Id;	
    	String startDate = '2013-09-01';
    	String endDate = '2013-09-15';
    	String eventStatus = 'available';
    	Days_Unavailable__c originalAvl = createTestAvlRecords(startDate,endDate,eventStatus,candidateId);
    	String displayRangeJsonString ='[{"startDate":"2013-11-01","endDate":"2013-12-31","candidateId":"' + candidateId + '"}]';
    	
    	String result = AvailabilityRequestHandler.deleteAvlRecord(originalAvl.Id, displayRangeJsonString);
    	System.assert(result.contains('success'));
    	}
    }
    
    // test deleting a record that does not exist 
    static testMethod void testDeleteAvlRecThatDoesntExist(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 		
    	String candidateId = candidate.Id;	
		String avlId = 'a01E000000LZHk9';	// this is a dummy Id
    	String displayRangeJsonString ='[{"startDate":"2013-11-01","endDate":"2013-12-31","candidateId":"' + candidateId + '"}]';
    	
    	String result = AvailabilityRequestHandler.deleteAvlRecord(avlId, displayRangeJsonString);
    	System.assert(result.contains('The record does not exist'));
    	}
    }
    
    
    // test displaying avl records given a time frame
    static testMethod void testDisplayRecords(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 
    	String displayRangeJsonString ='[{"startDate":"2013-11-01","endDate":"2013-12-31","candidateId":"' + candidate.Id + '"}]';	
    	String result  = AvailabilityRequestHandler.getAvlRecords4Display(displayRangeJsonString);
    	System.assert(result.contains('success'));
    	}
    }

    
    // helper method
    // to create a dummy avl record given start date, end date, event status and candidate Id 
    private static Days_Unavailable__c createTestAvlRecords(String startDate, String endDate, String eventStatus, String contactId){
    	Days_Unavailable__c	avl2Create = new Days_Unavailable__c();
    	avl2Create.Start_Date__c = Date.valueOf(startDate);
    	avl2Create.End_Date__c = Date.valueOf(endDate);
    	avl2Create.Event_Status__c = eventStatus;
    	avl2Create.Contact__c = contactId;
    	insert avl2Create;
    	return avl2Create;	
    }
        
    // update an existing avl record
    private static String updateAvlRec(Contact candidate){
    	String candidateId = candidate.Id;
    	String startDate = '2013-09-01';
    	String endDate = '2013-09-15';
    	String eventStatus = 'available';
    	String testJsonString ='[{"title":"Unavailable","startDate":"2013-09-02","endDate":"2013-09-15","eventStatus":"unavailable","candidateId":"' + candidateId + '"}]';
    	String displayRangeJsonString ='[{"startDate":"2013-11-01","endDate":"2013-12-31","candidateId":"' + candidateId + '"}]';
    	boolean dupCheck = false;
    	
    	Days_Unavailable__c originalAvl = createTestAvlRecords(startDate,endDate,eventStatus,candidateId);
    	system.assertNotEquals(null, originalAvl);
    	return AvailabilityRequestHandler.updateAvlRecords(originalAvl.Id+'', testJsonString,  displayRangeJsonString, dupCheck);		
    }
    
    // delete an existing avl record
    // private static String deleteAvlRec(Contact candidate){
    // 	String candidateId = candidate.Id;	
    // 	String startDate = '2013-09-01';
    // 	String endDate = '2013-09-15';
    // 	String eventStatus = 'available';
    // 	Days_Unavailable__c originalAvl = createTestAvlRecords(startDate,endDate,eventStatus,candidateId);
    // 	String displayRangeJsonString ='[{"startDate":"2013-11-01","endDate":"2013-12-31","candidateId":"' + candidateId + '"}]';
    // 	insert originalAvl;
    	
    // 	return AvailabilityRequestHandler.deleteAvlRecord(originalAvl.Id, displayRangeJsonString);
    // }
    
    // helper method
    // to create a dummy contact record
    private static Contact createTestCandidate(){
        Contact candidate = new Contact(LastName='candidate 1',email='Cand1@test.ctc.test');
    	insert candidate;
    	return candidate;
    }
    
    

}