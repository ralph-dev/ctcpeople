/**
    * This is the JXT email to candidate DTO
    * 
    * @author: Lorena
    * @createdDate: 6/10/2016
    * @lastModify: 
    */
public with sharing class JXTContent {
	
	public String fromAddress;
	public String firstName;
    public String lastName;
    public String emailAddress;
    public String adId;
    public String vacId;
    public String vacOwnerId;
    public String skillGroupString;
    //public String mobileNumber;
    //public String status;
    //public Date applicationReceivedDate;
    //public String source;
	public List<CVContent> files;
	
	public JXTContent() {
	    
	}
	
	public JXTContent(String fromAddress, String firstName, String lastName, String emailAddress, String adId, String vacId, String vacOwnerId, String skillGroupString, List<CVContent> files ) {
	    this.fromAddress = fromAddress;
	    this.firstName = firstName;
	    this.lastName = lastName;
	    this.emailAddress = emailAddress;
	    this.adId = adId;
	    this.vacId = vacId;
	    this.vacOwnerId = vacOwnerId;
	    //this.mobileNumber = mobileNumber;
	    //this.status = status;
	    //this.applicationReceivedDate = applicationReceivedDate;
	    //this.source = source;
	    this.skillGroupString = skillGroupString;
	    this.files = new List<CVContent>();
	    this.files.addAll(files);
	}


    
}