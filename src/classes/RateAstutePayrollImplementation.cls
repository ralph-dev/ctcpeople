public with sharing class RateAstutePayrollImplementation {
      /**
		To add rates to Astute Payroll

		***/
    public void addRates(list<Rate__c> newRecords){
        CTCPeopleSettingHelperServices ctcPeopleSettingHelperServ=new CTCPeopleSettingHelperServices();
        if(!ctcPeopleSettingHelperServ.hasApAccount()){
    		return;
    	}
        list<Rate__c> ratesNeedSync=new list<Rate__c>();
        for(Rate__c rate : newRecords){
            /***Easy to add work rule to control****/
            if(rate.Astute_Payroll_Upload_Status__c!='On Hold'){
                continue;
            }
            if(rate.PRID__c==null || rate.PRID__c==0){
                ratesNeedSync.add(rate);
            }
    	}
        if(ratesNeedSync.size()>0){
            callAddRatesAPI(ratesNeedSync);
        }
        
    }
    @future(callout=true)
	public static void userAddRateChange(String messages, String endpointStr, String session){
        HttpResponse res=new HttpResponse();
       	res= AstutePayrollFutureJobs.pushHandler(messages,endpointStr,session);
   		updateAddRateResults(res);
	}
	public void callAddRatesAPI(list<Rate__c> rates){
        /**
			Convert rates to JSON message
			***/
		String messages=userAddRateChangesMessagea(rates);
        /** 
			call future job to send the message to server
		
		**/ 
		String endPointStr=Endpoint.getAstuteEndpoint(); 
        endpointStr=endPointStr+'/AstutePayroll/addedRates';
        if(!Test.isRunningTest()){
        	userAddRateChange(messages,endpointStr,AstutePayrollUtils.GetloginSession());
        }else{
            
        }

    }
    public static void updateAddRateResults(HttpResponse res){
        if(res.getStatusCode()==200){
            String bodyStr=res.getBody();
            if(bodyStr!=null){
            	list<Object> objMaps=(list<Object>)JSON.deserializeUntyped(bodyStr);
                list<Rate__c> rates=new list<Rate__c>();
                for(Object obj: objMaps){
                    map<String,Object> objMap=(map<String,Object>)obj;
                    String sfId=(String)objMap.get('salesforceId');
                    Integer prid=(Integer)objMap.get('PRID');
                    String errorMessage=(String)objMap.get('errorMessage');
                    Rate__c rate=new Rate__c();
                    rate.Id=sfId;
                    if(prid!=0){
                    	rate.PRID__c=prid;
                        rate.Astute_Payroll_Upload_Status__c='Success';
                        rate.Astute_Payroll_Error_Message__c='-';
                        rate.Is_Api__c=true;
                    }else{
                        rate.Astute_Payroll_Upload_Status__c='Failed';
                    	rate.Astute_Payroll_Error_Message__c=errorMessage;
                        rate.Is_Api__c=true;
                    }
                    rates.add(rate);
                }
                //check FLS
                List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                        Rate__c.PRID__c,
                        Rate__c.Astute_Payroll_Upload_Status__c,
                        Rate__c.Astute_Payroll_Error_Message__c,
                        Rate__c.Is_Api__c
                };
                fflib_SecurityUtils.checkUpdate(Rate__c.SObjectType, fieldList);
                update rates;
            }
        }
    }
    /***
		Delete rate changes
	**/
    public void deleteRates(list<Rate__c> newRecords){
         CTCPeopleSettingHelperServices ctcPeopleSettingHelperServ=new CTCPeopleSettingHelperServices();
        //Need to add one more status to skip it
        ctcPeopleSettingHelperServ.hasApAccount() ;
        if(!ctcPeopleSettingHelperServ.hasApAccount() || ctcPeopleSettingHelperServ.getDisableDeleteRates()){
    		return;
    	}
    	 //System.debug('The disable Delete Rates '+ctcPeopleSettingHelperServ.getDisableDeleteRates());
    	list<Rate__c> ratesNeedSync=new list<Rate__c>();
        for(Rate__c rate : newRecords){
            /***Easy to delete work rule to control****/
            if(rate.PRID__c!=null && rate.PRID__c!=0){
                ratesNeedSync.add(rate);
            }
    	}
        if(ratesNeedSync.size()>0){
            callDeleteRatesAPI(ratesNeedSync);
        }
    }
    public void callDeleteRatesAPI(list<Rate__c> rates){
        /**
			Convert rates to JSON message
			***/
		String messages=userRemoveRateChangesMessagea(rates);
         /** 
			call future job to send the message to server
		
		**/ 
		String endPointStr=Endpoint.getAstuteEndpoint(); 
        endpointStr=endPointStr+'/AstutePayroll/deletedRates';
        if(!Test.isRunningTest()){
        	 AstutePayrollFutureJobs.pushToServer(messages,endpointStr,AstutePayrollUtils.GetloginSession());
        }else{
            
        }
        
    } 
  
    public String userRemoveRateChangesMessagea(list<Rate__c> rates){
		RateServices rateServ=new RateServices();
		list<map<String,Object>> objSTR=rateServ.convertRatesToRateChange(rates);
		map<String,Object> messagesJson=new map<String,Object>();
		messagesJson.put('userRemoveRateChanges',objSTR);
		String messages=JSON.serialize(messagesJson);
		return messages;
	}
    
    public String userAddRateChangesMessagea(list<Rate__c> rates){
		RateServices rateServ=new RateServices();
		list<map<String,Object>> objSTR=rateServ.convertRatesToRateChange(rates);
		map<String,Object> messagesJson=new map<String,Object>();
		messagesJson.put('userAddRateChanges',objSTR);
		String messages=JSON.serialize(messagesJson);
		return messages;
	}
}