/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class UserAstutePayrollImplementationTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	list<User> users=new list<User>();
    	User userRec=new User();
    	userRec.firstName='firstName';
    	userRec.lastName='Test name';
    	userRec.Email='Test@example.com';
    	users.add(userRec);
    	map<Id,User> userOwnerMap=new map<Id,User>();
    	userOwnerMap.put(userRec.Id,userRec);
    	UserAstutePayrollImplementation.astutePayrollSetCriterial(users, userOwnerMap);
        
        System.assertEquals('On Hold', userRec.Astute_Payroll_Upload_Status__c );
    	}
    }
    static testMethod void myUnitTest2() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	list<User> users=new list<User>();
    	User userRec=new User();
    	userRec.firstName='firstName';
    	userRec.lastName='Test name';
    	userRec.Email='Test@example.com';
    	users.add(userRec);
    	UserAstutePayrollImplementation uAPImp=new UserAstutePayrollImplementation();
    	uAPImp.astutePayrollTrigger(users);
        
        System.assertEquals(false, userRec.Is_Pushing_To_AP__c);
        
        uAPImp.pushToAstutePayroll(users);
    	}
        
        
    }
   /* static testMethod void myUnitTest3() {
    	list<User> users=new list<User>();
    	User userRec=new User();
    	userRec.firstName='firstName';
    	userRec.lastName='Test name';
    	userRec.Email='Test@example.com';
    	users.add(userRec);
    	UserAstutePayrollImplementation uAPImp=new UserAstutePayrollImplementation();
    	uAPImp.pushToAstutePayroll(users);
        
        System.assert(true);
    }*/
}