public class DaoRecordType {
	
	
    public static RecordType corpCandidateRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'Corporate_Candidate'];
        }
    }

    public static RecordType corpContactRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'Business_Contact' or DeveloperName= 'Corporate_Contact'];
        }
    }

    public static RecordType indCandidateRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'Independent_Candidate'];
        }
    }

    /**
        Return all Contact record types that are candidate type.
        In a company, they may have more than one candidate type: client candidate, independent candidate.
    */
    public static List<RecordType> candidateRTs{
        get{
            List<RecordType> result = new List<RecordType>();
            List<RecordType> rtList=[Select Id, Name,Description From RecordType where SobjectType='Contact' and isActive=true];
            for(RecordType rt:rtList){
                if(rt.Name.toLowerCase().contains('candidate')){
                    result.add(rt);
                }else if(rt.Description !=null && rt.Description.toLowerCase().contains('recordtype_candidate')){
                    result.add(rt);
                }
            }
            return result;
        }
    }
    
   
    
     public static List<RecordType> contactRTs{
        get{
            List<RecordType> result = new List<RecordType>();
            List<RecordType> rtList=[Select Id, Name,Description From RecordType where SobjectType='Contact' and isActive=true];
            for(RecordType rt:rtList){
                if(rt.Name.toLowerCase().contains('corporate') || rt.Name.toLowerCase().contains('contact')){
                    result.add(rt);
                }
            }
            return result;
        }
    }
    /**
        Return all Contact record types other than candidate type.
        In a company, they may have more than one contact record types: client contact, hiring manager.
    */
    public static List<RecordType> nonCandidateRTs{
        get{
            List<RecordType> result = new List<RecordType>();
            List<RecordType> candRTList=candidateRTs;
            List<RecordType> contactRTList=[Select Id, Name,Description From RecordType where SobjectType='Contact' and isActive=true];
            Map<Id,SObject> candRTMap = new Map<Id,SObject>();
            for(RecordType candRT :candRTList){
                candRTMap.put(candRT.Id,candRT);
            }
            for(RecordType conRT:contactRTList){
                if(candRTMap.get(conRT.Id) == null){
                    result.add(conRT);
                }
            }
            return result;
        }
    }
    
    
	
	 /**
        Return all Contact record types other than candidate type.
        In a company, they may have more than one contact record types: client contact, hiring manager.
    */
    public static List<String> nonCandidateRTIds{
        get{
            List<String> result = new List<String>();
            List<RecordType> candRTList=candidateRTs;
            List<RecordType> contactRTList=[Select Id, Name,Description From RecordType where SobjectType='Contact' and isActive=true];
            Map<Id,SObject> candRTMap = new Map<Id,SObject>();
            for(RecordType candRT :candRTList){
                candRTMap.put(candRT.Id,candRT);
            }
            for(RecordType conRT:contactRTList){
                if(candRTMap.get(conRT.Id) == null){
                    result.add(conRT.id);
                }
            }
            return result;
        }
    }
    
    //Deprecated method don't use. Will be delete in version 2.9
    public static List<RecordType> corpContactRTs{
        get{
            List<RecordType> result = new List<RecordType>();
            List<RecordType> candRTList=candidateRTs;
            List<RecordType> contactRTList=[Select Id, Name,Description From RecordType where isActive=true];
            Map<Id,SObject> candRTMap = new Map<Id,SObject>();
            for(RecordType candRT :candRTList){
                candRTMap.put(candRT.Id,candRT);
            }
            for(RecordType conRT:contactRTList){
                if(candRTMap.get(conRT.Id) == null){
                    result.add(conRT);
                }
            }
            return result;
        }
    }

    public static RecordType xeroOauthRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'Xero_OAuth'];
        }
    } 
    public static RecordType linkedinOauthRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'LinkedIn_OAuth'];
        }
    }


    public static RecordType candidatePoolRT{
        get{
            return [Select Id, Name, developername, Namespaceprefix From RecordType where developerName='Candidate_Pool'];
        }
    }

    public static RecordType contractVacRT{
        get{
            return [Select Id, Name, developername, Namespaceprefix From RecordType where developerName='Contract_Vacancy'];
        }
    }
    
    public static RecordType ASSaveSearchRT{//Save search criteria for Advanced search
        get{
            return [Select Id, Name, developername, Namespaceprefix From RecordType where developerName='Advanced_Search_Criteria'];
        }
    }
    
    public static RecordType GoogleSaveSearchRT{//Save search criteria for Advanced search
        get{
            return [Select Id, Name, developername, Namespaceprefix From RecordType where developerName='Google_Search_Criteria'];
        }
    }
    public static RecordType temporaryDataRT{
    	get{
    		if(temporaryDataRT == null){
    			temporaryDataRT = [Select Id, Name, developername, Namespaceprefix From RecordType where developerName='Screen_Candidate'];
    			return temporaryDataRT;
            }else{
                return temporaryDataRT; 
             }
        }
    }

    public static RecordType googleSearchUserPerformacne{
        get{
            return [Select Id, Name from RecordType where DeveloperName = 'Google_Search'];
        }
    }

    public static RecordType ScreenCandidateTempDataRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'Screen_Candidate'];
        }
    }
    /**
     * Get Default Skill Group from CTC People Setting Helper Object
     */
    public static RecordType DefaultSkillGroupRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'Default_Skill_Group'];
        }
    }


    /********** These record type are for Advertisement Object ***********/

    public static RecordType IndeedRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'Indeed'];
        }
    }

    public static RecordType JXTAdRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'JXT'];
        }
    }

    public static RecordType linkedInAdRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'LinkedIn'];
        }
    }

    public static RecordType seekAdRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'Seek_com_Advertisement'];
        }
    }

    public static RecordType myCareerAdRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'MyCareer_com_Advertisement'];
        }
    }

    public static RecordType careerOneAdRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'CareerOne_com_Advertisement'];
        }
    }

    public static RecordType templateAdRT {
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'Template_Record'];
        }
    }

    /************************************************************************************/


    /********** These record type are for Job Board Config ***********/

    public static RecordType seekaccountOldRT{
        get{
            String obj = PeopleCloudHelper.getPackageNamespace() + 'StyleCategory__c';
            return [Select Id, Name from RecordType where SobjectType =: obj  and DeveloperName = 'Seek_Account'];
        }
    }

    public static RecordType seekAccountRT{
        get{
            String obj = PeopleCloudHelper.getPackageNamespace() + 'Advertisement_Configuration__c';
            return [Select Id, Name from RecordType where SobjectType =: obj and DeveloperName = 'Seek_Account'];
        }
    }

    public static RecordType linkedInaccountRT{
        get{
            return [Select Id, Name from RecordType where DeveloperName = 'LinkedIn_Account'];
        }
    }

    public static RecordType IndeedAdRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'Indeed_Account'];
        }
    }

    public static RecordType jxtAccountRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'JXT_Account'];
        }
    }
    
    public static RecordType careerOneAccountRT{
    	get{
    		return [select Id from recordType where DeveloperName ='WebSite_Admin_Record'];
   
    	}
     }

    public static RecordType jobBoardAccRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'WebSite_Admin_Record'];
        }
    }

    // don't know if anywhere used this, if no problem with deploy, can be deleted for next release
//    public static RecordType defaultseekaccoutRT{
//        get{
//            return [Select Id, Name from RecordType where DeveloperName = 'WebSite_Admin_Record'];
//        }
//    }

    public static RecordType adTemplateRT{
        get{
            return [Select Id, Name from RecordType where DeveloperName = 'templateStyle'];
        }
    }

    public static RecordType ApplicationFormStyleRT{
        get{
            return [Select Id, Name from RecordType where DeveloperName = 'ApplicationFormStyle'];
        }
    }

    /************************************************************************************/


    /********** These record type are for advertisement config detail object ***********/

    public static RecordType seekScreenRT{
        get{
            return [Select Id, Name from RecordType where DeveloperName = 'Seek_Screen'];
        }
    }

    public static RecordType adConfigLogoRT{
        get{
            return [Select Id, Name from RecordType where DeveloperName = 'Logo'];
        }
    }

    public static RecordType adConfigTemplateRT{
        get{
            return [Select Id, Name from RecordType where DeveloperName = 'Template'];
        }
    }

    /**
     * Get Seek Screen Field RecordType from CTC People Setting Helper Object
     */
    public static RecordType SeekScreenFieldRT{
        get{
            return [Select Id, Name From RecordType where DeveloperName = 'Seek_Screen_Field'];
        }
    }
    /************************************************************************************/
    
    public static RecordType scheduleJobRT{
        get{
            return [select Id,Name from RecordType where developername='schedule_job'];
        }
    }
    
    
    public static RecordType AstutePayrollAccountRT{
        get{
            return [select id from RecordType where developername='AstutePayrollAccount'];
        }
    }
    
    
    public static List<RecordType> getRecordTypesIn(String[] rTDeveloperNames){
    	return [select id, developername from RecordType where developername in: rTDeveloperNames];
    }

    public static List<RecordType> getRecordTypesInList(List<String> rTDeveloperNames){
        return [select id, name, developername from RecordType where developername in: rTDeveloperNames];
    }
    
     public static List<RecordType> getRecordTypesIn(Set<String> rTDeveloperNames){
    	return [select id, developername from RecordType where developername in: rTDeveloperNames];
    }
    
    public static List<RecordType> getAllRecordTypes( ){
    	return [select id, name,developername from RecordType ];
    }
     
    public static List<RecordType> getRecordTypesByDeveloperandObjectName(String scDeveloperName, String sobjectTypeName){
    		String queryString = 'select Id, Name, developerName from RecordType where developerName=:scDeveloperName and SobjectType =:sobjectTypeName ';
            
            
            return Database.query(queryString);
    }
    
     public static List<RecordType> getActiveRecordTypesByObjectType(String SelectedObject){
    	return [select id,name from RecordType where SobjectType=: SelectedObject and IsActive = true];
    }
    
    public static RecordType getRecordTypesByObjectType(String SelectedObject){
    	return [select id,name from RecordType where SobjectType=: SelectedObject limit 1];
    }
    
    public static RecordType getRecordTypeByDeveloperName(String devname){
    	return [select Id,name from RecordType where DeveloperName=: devname  limit 1];
    }
    

    public static List<RecordType> getRecordTypesByGivenObjectType(String SelectedObject){
        return [select id,name,developerName from RecordType where SobjectType=: SelectedObject];
    }
    
    public static RecordType getRecordTypeById(Id rtid){
        return [select Name From RecordType where id =: rtid];
    }
    

}