/**
*
* Careerone, Trademe, and JXT_NZ account info are saved as records of StyleCategory
* JXZ account info are saved as records of AdvertisementConfiguration
*
* All the account credentials of Careerone, JXT, JXT_NZ are saved as records of Credential Setting
*
* Modified by Zoe on 17/07/2017
*
*/
public with sharing class JobBoardAccountClass{

    public boolean has_c1 = false, has_trademe = false, has_jxt = false, has_jxt_nz = false, has_indeed = false, has_linkedin = false;

    public List<SelectOption> websites {get;set;}
    public String selected_website {get;set;}
    public Boolean showNew {get;set;}

    public List<StyleCategory__c> AllAccSC{get; set;}
    public LiSt<Advertisement_Configuration__c> AllAccADC{get;set;}
    public OAuth_Service__c linkedinOAuthSerivce {get;set;} // Linkedin  OAuthService

    // To save credentials of different account in custom setting
    public ProtectedCredential careerOneAcc;
    public ProtectedCredential jxtAcc;
    public ProtectedCredential jxtNZAcc;
    public ProtectedCredential indeedAcc;
    public ProtectedCredential linkedinAcc;

    // To save the values to create new accounts
    private transient String String1;
    private transient String String2;
    private transient String String3;
    private transient String String4;
    private transient String String5;
    private transient String String6;
    private transient String String7;
    public boolean seekApplicationForm{get; set;}

    // To store usernames
    public String careerOneUName {get;set;}
    public String jxtnzUName{get;set;}
    public String jxtUName {get;set;}

    // To store new passwords, tokens, secrets, and keys
    private transient String careerOnePwd;
    private transient String jobxPwd;
    private transient String linkedInCS;
    private transient String linkedInCK;
    private transient String jxtPwd;
    private transient String indeedToken;
    
    // To save RecordTypeId of different account
    private Id scRTId;
    private Id indeedRTId;
    private Id jxtRTId;
    private Id linkedinRTId;
    private Id linkedinOAuthServiceRTId;
    
    private Map<String,String> ids;
    
    // To store error messages
    public String err{get;set;}

    public JobBoardAccountClass(){

        AllAccSC = new List<StyleCategory__c>();
        AllAccADC = new List<Advertisement_Configuration__c>();
        linkedinOAuthSerivce = new OAuth_Service__c();
        ids = new Map<String,String>();
		
        careerOneAcc = new ProtectedCredential();
        jxtAcc = new ProtectedCredential();
        jxtNZAcc = new ProtectedCredential();
        indeedAcc = new ProtectedCredential();
        linkedinAcc = new ProtectedCredential();

        scRTId = DaoRecordType.jobBoardAccRT.Id;
        jxtRTId = DaoRecordType.jxtAccountRT.Id;
        indeedRTId = DaoRecordType.IndeedAdRT.Id;
        linkedinRTId = DaoRecordType.linkedInaccountRT.Id;
        linkedinOAuthServiceRTId = DaoRecordType.linkedinOauthRT.Id;

        // If there are account info records, retrieve them
        toDisplayRecords();
    }


    public pageReference updateAcc(){
        err = '';
        if(checkMultiple()){
            if(AllAccSC != null && !AllAccSC.isEmpty()){
                checkUpdatableOrInsertableSC();
                update AllAccSC;
            }
            if(AllAccADC != null && !AllAccADC.isEmpty()){
                checkUpdatableOrInsertableADC();
                update AllAccADC;
            }
            if(linkedinOAuthSerivce != null){
                checkUpdatableOrInsertableOAuthService();
                update linkedinOAuthSerivce;
            }
            if(careerOneAcc != null){
                if(String.isNotEmpty(careerOnePwd)&& !careerOnePwd.equals(careerOneAcc.password)){
                    careerOneAcc.password = careerOnePwd;
                }
                if(String.isNotEmpty(careerOneUName)&& !careerOneUName.equals(careerOneAcc.username)){
                    careerOneAcc.username = careerOneUName;
                }
                CredentialSettingService.updateCareerOneCredential(careerOneAcc.name,careerOneAcc.username,careerOneAcc.password);
            }else{
                if(String.isNotEmpty(careerOnePwd) && String.isNotEmpty(careerOneUName)){
                    CredentialSettingService.upsertCareerOneCredential(ids.get(JobBoardUtils.CAREERONE),careerOneUName,careerOnePwd);
                }
            }
            if(jxtAcc != null){
                if(String.isNotEmpty(jxtPwd) && !jxtPwd.equals(jxtAcc.password)){
                    jxtAcc.password = jxtPwd;
                }
                if(String.isNotEmpty(jxtUName) && !jxtUName.equals(jxtAcc.username)){
                    jxtAcc.username = jxtUName;
                }
                CredentialSettingService.updateJXTCredential(jxtAcc.name,jxtAcc.username,jxtAcc.password);
            }else{
                if(String.isNotEmpty(jxtPwd) && String.isNotEmpty(jxtUName)){
                    CredentialSettingService.upsertJXTCredential(ids.get(JobBoardUtils.JXT),jxtUName,jxtPwd);
                }
            }
            if(jxtNZAcc != null){
                if(String.isNotEmpty(jobxPwd)&& !jobxPwd.equals(jxtNZAcc.password)){
                    jxtNZAcc.password = jobxPwd;
                }
                if(String.isNotEmpty(jxtnzUName) && !jxtnzUName.equals(jxtNZAcc.username)){
                    jxtNZAcc.username = jxtnzUName;
                }
                CredentialSettingService.updateJXTNZCredential(jxtNZAcc.name,jxtNZAcc.username,jxtNZAcc.password);
            }else{
                if(String.isNotEmpty(jobxPwd)&&String.isNotEmpty(jxtnzUName)){
                    CredentialSettingService.upsertJXTNZCredential(ids.get(JobBoardUtils.JXT_NZ),jxtnzUName,jobxPwd);
                }
            }
            if(indeedAcc !=null){
                if(String.isNotEmpty(indeedToken)&& !indeedToken.equals(indeedAcc.token)){
                    indeedAcc.token = indeedToken;
                }
                CredentialSettingService.updateIndeedCredential(indeedAcc.name,indeedAcc.token);
            }else{
                if(String.isNotEmpty(indeedToken)){
                    CredentialSettingService.upsertIndeedCredential(ids.get(JobBoardUtils.INDEED),indeedToken);
                }
            }
            if(linkedinAcc != null){
                if(String.isNotEmpty(linkedInCS)&& !linkedInCS.equals(linkedinAcc.secret)){
                    linkedinAcc.secret = linkedInCS;
                }
                if(String.isNotEmpty(linkedInCK)&& !linkedInCK.equals(linkedinAcc.key)){
                    linkedinAcc.key = linkedInCK;
                }
                CredentialSettingService.updateLinkedInOAuthService(linkedinAcc.name, linkedinAcc.key, linkedinAcc.secret);
            }else{
                if(String.isNotEmpty(linkedInCS)&&String.isNotEmpty(linkedInCK)){
                    CredentialSettingService.upsertLinkedInOAuthService(ids.get(JobBoardUtils.LINKEDIN), linkedInCK, linkedInCS);
                }
            }
            err = 'Update successfully!';
        }

        return null;
    }


    public List<SelectOption> getOptions(){

        err = '';
        List<SelectOption> sites = new List<SelectOption>();
        sites.add(new SelectOption('','--Please Select--'));
        if(!has_c1){
            sites.add(new SelectOption(JobBoardUtils.CAREERONE,JobBoardUtils.CAREERONE));
        }
        if(!has_indeed){
            sites.add(new SelectOption(JobBoardUtils.INDEED,JobBoardUtils.INDEED));
        }
        if(!has_jxt){
            sites.add(new SelectOption(JobBoardUtils.JXT,JobBoardUtils.JXT));
        }
        if(!has_jxt_nz){
            sites.add(new SelectOption(JobBoardUtils.JXT_NZ,JobBoardUtils.JXT_NZ));
        }
        if(!has_linkedin){
            sites.add(new SelectOption(JobBoardUtils.LINKEDIN,JobBoardUtils.LINKEDIN));
        }
        if(!has_trademe){
            sites.add(new SelectOption(JobBoardUtils.TRADEME,JobBoardUtils.TRADEME));
        }

        return sites;
    }
    public pageReference checkAcc(){
        err ='';
        if(AllAccSC!=null){
            for(StyleCategory__c  s: AllAccSC) {
                if(s.WebSite__c != null){
                    if (s.WebSite__c == JobBoardUtils.CAREERONE) {
                        has_c1 = true;
                    }else if (s.WebSite__c == JobBoardUtils.TRADEME) {
                        has_trademe = true;
                    }else if (s.WebSite__c == JobBoardUtils.JXT_NZ) {
                        has_jxt_nz = true;
                    }else if(s.WebSite__c == JobBoardUtils.LINKEDIN){
                        has_linkedin = true;
                    }
                }
            }
        }
        if(AllAccADC!=null) {
            for (Advertisement_Configuration__c ac: AllAccADC) {
                if(ac.RecordTypeId != null){
                    if (ac.RecordTypeId == jxtRTId) {
                        has_jxt = true;
                    } else if (ac.RecordTypeId == indeedRTId) {
                        has_indeed = true;
                    }
                }
            }
        }
        if(has_c1 && has_trademe && has_jxt && has_jxt_nz && has_indeed && has_linkedin){
            err = 'There are existing accounts for CareerOne,Indeed,JXT,JXT_NZ, LinkedIn and Trademe. Please activate and update them if you like to start job posting.';
            showNew = false;
            return null;
        }
        showNew = true;
        websites = getOptions();
        return null;
    }
    public pageReference displayDetail(){
        return null;

    }


    public pageReference createnew(){
        err = '';
        if(selected_website == null){
            return null;
        }
        try{
            StyleCategory__c newaccsc = new StyleCategory__c();
            Advertisement_Configuration__c newaccadc= new Advertisement_Configuration__c();

            if(selected_website == JobBoardUtils.CAREERONE){
                newaccsc.Account_Active__c = true;
                careerOneAcc.username = String1;
                newaccsc.Advertiser_Uploadcode__c = String2;
                careerOneAcc.password = String3;
                newaccsc.CareerOne_Premium_Package__c = seekApplicationForm;
                newaccsc.Name = JobBoardUtils.CAREERONEACCOUNT;
                newaccsc = insertSC(newaccsc,scRTId);
                CredentialSettingService.upsertCareerOneCredential(newaccsc.Id, careerOneAcc.username,careerOneAcc.password);
            }
            if(selected_website == JobBoardUtils.TRADEME){
                newaccsc.Account_Active__c = true;
                newaccsc.CompanyCode__c = String1 ; // client id
                newaccsc.ProviderCode__c = String2;
                newaccsc.OfficeCodes__c = String3;
                newaccsc.Name = JobBoardUtils.TRADEMEACCOUNT;
                newaccsc = insertSC(newaccsc,scRTId);
            }
            if(selected_website == JobBoardUtils.LINKEDIN){
                newaccsc.Account_Active__c = true;
                newaccsc.Advertiser_Id__c = String1;
                newaccsc.LinkedIn_Partner_ID__c = String2;
                newaccsc.Name = JobBoardUtils.LINKEDINACCOUNT;
                newaccsc = insertSC(newaccsc,linkedinRTId);
                if(linkedinAcc==null){
                    linkedinAcc = new ProtectedCredential();
                }
                linkedinAcc.key = String3;
                linkedinAcc.secret = String4;
                if(linkedinOAuthSerivce == null){
                    linkedinOAuthSerivce = new OAuth_Service__c();
                }
                linkedinOAuthSerivce.Active__c = true;
                linkedinOAuthSerivce.Name = JobBoardUtils.LINKEDIN;
                linkedinOAuthSerivce.Access_Token_URL__c = String5;
                linkedinOAuthSerivce.Authorization_URL__c = String6;
                linkedinOAuthSerivce.Request_Token_URL__c = String7;
                upsertOAuthServie(linkedinOAuthServiceRTId);
                CredentialSettingService.upsertLinkedInOAuthService(linkedinOAuthSerivce.Id, linkedinAcc.key,linkedinAcc.secret);
            }
            if(selected_website == JobBoardUtils.JXT){
                newaccadc.Active__c = true;
                newaccadc.Advertisement_Account__c = String1;
                jxtAcc.username = String2;
                jxtAcc.password = String3;
                newaccadc.Name = JobBoardUtils.JXTACCOUNT;
                newaccadc = insertADC(newaccadc,jxtRTId);
                CredentialSettingService.upsertJXTCredential(newaccadc.Id,jxtAcc.username,jxtAcc.password);
            }
            if(selected_website == JobBoardUtils.JXT_NZ){
                newaccsc.Account_Active__c = true;
                newaccsc.Advertiser_Id__c = String1;
                newaccsc.White_labels__c = String2;
                jxtNZAcc.username = String3;
                jxtNZAcc.password = String4;
                newaccsc.Name = JobBoardUtils.JXT_NZACCOUNT;
                newaccsc = insertSC(newaccsc,scRTId);
                CredentialSettingService.upsertJXTNZCredential(newaccsc.Id,jxtNZAcc.username,jxtNZAcc.password);
            }
            if(selected_website == JobBoardUtils.INDEED){
                newaccadc.Active__c = true;
                newaccadc.Advertisement_Account__c = String1;
                newaccadc.Indeed_Apply_Job_URL__c = String2;
                indeedAcc.token = String3;
                newaccadc.Name = JobBoardUtils.INDEEDACCOUNT;
                newaccadc = insertADC(newaccadc,indeedRTId);
                CredentialSettingService.upsertIndeedCredential(newaccadc.Id,indeedAcc.token);
            }

            toDisplayRecords();
            checkAcc();
            getOptions();

            selected_website = null;
            seekApplicationForm = false;

        }
        catch(Exception e){
            system.debug(e);
        }

        return null;
    }



    private void toDisplayRecords(){
        err = '';
        getRecords();
        checkMultiple();
        if(AllAccSC!=null) {
            // All the records of credential setting
            for (StyleCategory__c sc: AllAccSC) {
                if(sc.WebSite__c!=null){
                    ids.put(sc.WebSite__c,sc.Id);
                    if (sc.WebSite__c == JobBoardUtils.CAREERONE) {
                        careerOneAcc = CredentialSettingService.getCredential(sc.Id);
                        if(careerOneAcc!=null) {
                            careerOneUName = careerOneAcc.username;
                        }
                    } else if (sc.WebSite__c == JobBoardUtils.JXT_NZ) {
                        jxtNZAcc = CredentialSettingService.getCredential(sc.Id);
                        if(jxtNZAcc!=null) {
                            jxtnzUName = jxtNZAcc.username;
                        }
                    }
                }
            }
        }
        if(AllAccADC!=null) {
            for (Advertisement_Configuration__c ac : AllAccADC) {
                if(ac.RecordTypeId != null){
                    if(ac.WebSite__c!=null){
                        ids.put(ac.WebSite__c,ac.Id);
                    }
                    if (ac.RecordTypeId == jxtRTId) {
                        jxtAcc = CredentialSettingService.getCredential(ac.Id);
                        if(jxtAcc!=null) {
                            jxtUName = jxtAcc.username;
                        }
                    }
                    if (ac.RecordTypeId == indeedRTId) {
                        indeedAcc = CredentialSettingService.getCredential(ac.Id);
                    }
                }
            }
        }
        if(linkedinOAuthSerivce !=null){
            ids.put(JobBoardUtils.LINKEDIN,linkedinOAuthSerivce.Id);
            linkedinAcc = CredentialSettingService.getCredential(linkedinOAuthSerivce.Id);
        }

    }

    private void checkUpdatableOrInsertableADC(){
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Advertisement_Configuration__c.RecordTypeId,
                Advertisement_Configuration__c.Name,
                Advertisement_Configuration__c.Active__c,
                Advertisement_Configuration__c.Advertisement_Account__c,
                Advertisement_Configuration__c.Indeed_Apply_Job_URL__c
        };

        fflib_SecurityUtils.checkInsert(Advertisement_Configuration__c.SObjectType, fieldList);
        fflib_SecurityUtils.checkUpdate(Advertisement_Configuration__c.SObjectType, fieldList);
    }

    private void checkUpdatableOrInsertableSC(){

        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                StyleCategory__c.Account_Active__c,
                StyleCategory__c.Advertiser_Id__c,
                StyleCategory__c.Advertiser_Uploadcode__c,
                StyleCategory__c.CareerOne_Premium_Package__c,
                StyleCategory__c.CompanyCode__c,
                StyleCategory__c.Name,
                StyleCategory__c.OfficeCodes__c,
                StyleCategory__c.ProviderCode__c,
                StyleCategory__c.RecordTypeId,
                StyleCategory__c.White_labels__c,
                StyleCategory__c.WebSite__c,
                StyleCategory__c.LinkedIn_Partner_ID__c
        };

        fflib_SecurityUtils.checkInsert(StyleCategory__c.SObjectType, fieldList);
        fflib_SecurityUtils.checkUpdate(StyleCategory__c.SObjectType, fieldList);
    }
    
    private void checkUpdatableOrInsertableOAuthService(){
         List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                OAuth_Service__c.Active__c,
                OAuth_Service__c.Access_Token_URL__c,
                OAuth_Service__c.Authorization_URL__c,
                OAuth_Service__c.Name,
                OAuth_Service__c.Request_Token_URL__c
        };

        fflib_SecurityUtils.checkInsert(OAuth_Service__c.SObjectType, fieldList);
        fflib_SecurityUtils.checkUpdate(OAuth_Service__c.SObjectType, fieldList);
        
    }


    private StyleCategory__c insertSC(StyleCategory__c scAcc, Id recordTypeId){
        scAcc.WebSite__c = selected_website;
        scAcc.RecordTypeId = recordTypeId;
        checkUpdatableOrInsertableSC();
        insert scAcc;
        return scAcc;
    }

    private Advertisement_Configuration__c insertADC(Advertisement_Configuration__c adcAcc, Id recordTypeId){
        adcAcc.RecordTypeId = recordTypeId;
        checkUpdatableOrInsertableADC();
        insert adcAcc;
        return adcAcc;

    }
    
    private void upsertOAuthServie(Id recordTypeId){

       linkedinOAuthSerivce.RecordTypeId = recordTypeId;
       checkUpdatableOrInsertableOAuthService();
       upsert linkedinOAuthSerivce;
    }
    
    public String getcareerOnePwd(){
        return null;
    }
    public String getjobxPwd(){
        return null;
    }
    public String getlinkedInCS(){
        return null;
    }
    public String getlinkedInCK(){
        return null;
    }
    public String getjxtPwd(){
        return null;
    }
    public String getindeedToken(){
        return null;
    }
    
    public void setcareerOnePwd(String careerOnePwd){
        this.careerOnePwd = careerOnePwd;
    }

    public void setjobxPwd(String jobxPwd){
        this.jobxPwd = jobxPwd;
    }
    
    public void setlinkedInCk(String linkedInCK){
        this.linkedInCK = linkedInCK;
    }
    
    public void setlinkedInCS(String linkedInCS){
        this.linkedInCS = linkedInCS;
    }
    
    public void setjxtPwd(String jxtPwd){
        this.jxtPwd = jxtPwd;
    }
    
    public void setindeedToken(String indeedToken){
        this.indeedToken = indeedToken;
    }

    private Boolean checkMultiple(){
        err = '';
         // One website only has one account, this is to check if this criterion is satisfied

        integer count3 = 0; //careerone
        integer count4 = 0; //trademe
        integer count5 = 0; //jxt
        integer count6 = 0; //jxt_nz
        integer count7 = 0; //Indeed
        integer count8 = 0; //Linkedin
        if(AllAccSC!=null) {
            for (StyleCategory__c s: AllAccSC) {
                if(s.WebSite__c != null){
                    if (s.WebSite__c == JobBoardUtils.CAREERONE) {
                        count3 = count3 + 1;
                    }
                    if (s.WebSite__c == JobBoardUtils.TRADEME) {
                        count4 = count4 + 1;
                    }
                    if (s.WebSite__c == JobBoardUtils.JXT_NZ) {
                        count6 = count6 + 1;
                    }
                    if (s.WebSite__c == JobBoardUtils.LINKEDIN) {
                        count8 = count8 + 1;
                    }
                }
            }
        }
        if(AllAccADC!=null) {
            for (Advertisement_Configuration__c ac:AllAccADC) {
                if(ac.WebSite__c != null){
                    if (ac.WebSite__c == JobBoardUtils.JXT) {
                        count5 = count5 + 1;
                    }
                    if (ac.WebSite__c == JobBoardUtils.INDEED) {
                        count7 = count7 + 1;
                    }
                }
            }
        }
		
        if(count3 > 1){
            err = 'You have more than one CareerOne account.';
        }
        if (count4 > 1){
            err = err + '\n You have more than one TradeMe account.';
        }
        if(count5 > 1){
            err = err + '\n You have more than one JXT account.';
        }
        if(count6 > 1){
            err = err + '\n You have more than one JXT_NZ account.';
        }
        if(count7>1){
            err = err + '\n You have more than one Indeed account.';
        }
        if(count8>1){
            err = err + '\n You have more than one Linkedin account.';
        }
        
         if(count3<=1 && count4<=1 && count5 <=1 && count6<=1 && count7 <=1 && count8 <=1){
             return true;
         }else{
             return false;
         }
        
    }
    
    private void getRecords(){
         //all the records of StyleCategory with WebSite_Admin_Record record type
        if(scRTId!=null) {
            AllAccSC = new StyleCategorySelector().getStyleCategoriesByRecordTypeId(scRTId);
        }
        if(AllAccSC != null) {
            if(linkedinRTId !=null) {
                List<StyleCategory__c> temp = new StyleCategorySelector().getStyleCategoriesByRecordTypeId(linkedinRTId);
                if (temp != null) {
                    AllAccSC.addAll(temp);
                }
            }
        }else{
            if(linkedinRTId!=null) {
                AllAccSC = new StyleCategorySelector().getStyleCategoriesByRecordTypeId(linkedinRTId);
            }
        }

        // all the records of AdvertisementConfiguration with RecordType JXT_Account
        if(jxtRTId!=null) {
            AllAccADC = new AdvertisementConfigurationSelector().getAccountByRecordTypeId(jxtRTId);
        }
        if(indeedRTId!=null) {
            if(AllAccADC!=null) {
                List<Advertisement_Configuration__c> tempAC = new AdvertisementConfigurationSelector().getAccountByRecordTypeId(indeedRTId);
                if(tempAC!=null) {
                    AllAccADC.addAll(tempAC);
                }
            }else{
                AllAccADC =  new AdvertisementConfigurationSelector().getAccountByRecordTypeId(indeedRTId);
            }
        }


        if(linkedinOAuthServiceRTId!=null) {
            linkedinOAuthSerivce = new OAuthServiceSelector().getOAuthServiceByRecordTypeId(linkedinOAuthServiceRTId);
        }
    }
    
    
    public String getString1(){
        return null;
    }
    public String getString2(){
        return null;
    }
    public String getString3(){
        return null;
    }
    public String getString4(){
        return null;
    }
    public String getString5(){
        return null;
    }
    public String getString6(){
        return null;
    }
    public String getString7(){
        return null;
    }
    
    public void setString1(String String1){
        this.String1 = String1;
    }
    public void setString2(String String2){
        this.String2 = String2;
    }
    public void setString3(String String3){
        this.String3 = String3;
    }
    public void setString4(String String4){
        this.String4 = String4;
    }
    public void setString5(String String5){
        this.String5 = String5;
    }
    public void setString6(String String6){
        this.String6 = String6;
    }
    public void setString7(String String7){
        this.String7 = String7;
    }
}