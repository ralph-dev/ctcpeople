global with sharing class AstutePayrollFutureJobs {
	@future(callout=true)
	public static void pushToServer(String messages, String endpointStr,String session){
		String messageStr=messages;
		if(messageStr!=null)
			messageStr=EncodingUtil.URLEncode(messageStr, 'UTF-8');
        HttpResponse res=new HttpResponse();
		pushHandler(messageStr,endpointStr,session);
		
	}
	public static HttpResponse pushHandler(String messages, String endpointStr,String session){
		String bodyString='jsonStr='+messages+'&namespace='+PeopleCloudHelper.getPackageNamespace();
		 Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setBody(bodyString);
        req.setHeader('orgId', UserInfo.getOrganizationId());
        req.setHeader('userId', UserInfo.getUserId());
        //commented by Ralph 11-08-2017
        //req.setHeader('session', session);            
        req.setEndpoint(endpointStr);       
        String urlStr=URL.getSalesforceBaseUrl().getHost()+'/services/Soap/u/29.0/';
        req.setHeader('surl', urlStr);
        req.setTimeout(120000); 
        HttpResponse res=new HttpResponse();
         if(Test.isRunningTest()){
        	res.setBody('Success');
        	res.setStatusCode(200);
        }else{
        	res=h.send(req);
        }  
        return res;
	}
	/***
		Synchronize invoice to Astute Payroll
							-Alvin Zhou
	
	
	*****/
	@future(callout=true)
	public static void pushBrandNewInvoiceToAP(list<String> invoiceIds,String session){
		if(invoiceIds.size()==0 || test.isRunningTest()){
			return;
		}
		
		allocateBrandNewInvoiceForAP(invoiceIds,session);
	}
	public static HttpResponse allocateBrandNewInvoiceForAP(list<String> invoiceIds,String session){
		list<AstutePayrollRecordsDTO> aPRecordsDTOs=new list<AstutePayrollRecordsDTO>();
		InvoiceItemServices invoiceItemserv=new InvoiceItemServices();
		list<map<String,Object>> objSTR=new list<map<String,Object>>();
		objSTR=invoiceItemserv.convertInvoiceItemToInvocieItemByIds(invoiceIds);
		map<String,Object> messagesJson=new map<String,Object>();
		messagesJson.put('allrecords',objSTR);
		String messages=JSON.serialize(messagesJson);
		/** 
			Generate the json message and send to AP
		
		**/ 
		String endPointStr=Endpoint.getAstuteEndpoint(); 
        endpointStr=endPointStr+'/AstutePayroll/brandnewInvoice';
        if(messages!=null)
        	messages=EncodingUtil.UrlEncode(messages,'UTF-8');
        String bodyString='jsonStr='+messages+'&namespace='+PeopleCloudHelper.getPackageNamespace();
		Http h = new Http(); 
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setBody(bodyString);
        req.setHeader('orgId', UserInfo.getOrganizationId());
        req.setHeader('userId', UserInfo.getUserId());
        //commented by Ralph, 11-08-2017
        //req.setHeader('session', session);              
        req.setEndpoint(endpointStr);       
        String urlStr=URL.getSalesforceBaseUrl().getHost()+'/services/Soap/u/29.0/';
        req.setHeader('surl', urlStr);
        req.setTimeout(120000); 
        HttpResponse res=new HttpResponse();
        if(Test.isRunningTest()){ 
        	res.setBody('Success');
        	res.setStatusCode(200);
        }else{
        	res=h.send(req);
        }    
		return res;	
		
	}
	/***
		Synchronize placements and other related records to Astute Payroll
							-Alvin Zhou
	
	
	*****/
	@future(callout=true)
	public static void pusthToAPFirstTime(list<String> placementIds,String session){
		if(placementIds.size()==0 || test.isRunningTest())
			return;
		firstTimePush(placementIds,session);
	}
	public static HttpResponse firstTimePush(list<String> placementIds,String session){
		PlacementServices placementServ=new PlacementServices();
		list<map<String,Object>> objSTR=new list<map<String,Object>>();
		objSTR=placementServ.fetchAllRecords(placementIds);
		map<String,Object> messagesJson=new map<String,Object>();
		messagesJson.put('allrecords',objSTR);
		String messages=JSON.serialize(messagesJson);
		/** 
			call future job to send the message to server
		
		**/ 
		String endPointStr=Endpoint.getAstuteEndpoint(); 
        endpointStr=endPointStr+'/AstutePayroll/allrecords';
        if(messages!=null)
        	messages=EncodingUtil.UrlEncode(messages,'UTF-8');
        String bodyString='jsonStr='+messages+'&namespace='+PeopleCloudHelper.getPackageNamespace();
		 Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setBody(bodyString);
         req.setHeader('orgId', UserInfo.getOrganizationId());
        req.setHeader('userId', UserInfo.getUserId());
        //commented by Ralph, 11-08-2017
        //req.setHeader('session', session);            
        req.setEndpoint(endpointStr);       
        String urlStr=URL.getSalesforceBaseUrl().getHost()+'/services/Soap/u/29.0/';
        req.setHeader('surl', urlStr);
        req.setTimeout(120000); 
        HttpResponse res=new HttpResponse();
        if(Test.isRunningTest()){
        	res.setBody('Success');
        	res.setStatusCode(200);
        }else{
        	res=h.send(req);
        }
        return res;
		
	}
	/***
		Synchronize client records to Astute Payroll
							-Alvin Zhou
	
	
	*****/
	@future(callout=true)
	public static void syncClientBillerFuture(list<String> accountIds, String session){
		if(accountIds.size()==0 || test.isRunningTest()){
			return;
		}
		AccountAstutePayrollImplementation.syncClientBiller(accountIds,session);				
	}
	
	
	/***
		Synchronize biller contacts records to Astute Payroll
	
	
	******/
	@future(callout=true)
	public static void syncContactBillerContactFuture(list<String> contactIds, String session){
		if(contactIds.size()==0 || test.isRunningTest()){
			return;
		}
		ContactAstutePayrollImplementation.syncBillerContacts(contactIds,session);				
	}
	
	/***
		Synchronize approvers records to Astute Payroll
	
	
	******/
	@future(callout=true)
	public static void syncContactApproverFuture(list<String> contactIds, String session){
		if(contactIds.size()==0 || test.isRunningTest()){
			return;
		}
		ContactAstutePayrollImplementation.syncApproverContacts(contactIds,session);				
	}
	
	/***
		Synchronize employee records to Astute Payroll
	
	
	******/
	@future(callout=true)
	public static void syncContactEmployeeFuture(list<String> contactIds, String session){
		if(contactIds.size()==0 || test.isRunningTest()){
			return;
		}
		ContactAstutePayrollImplementation.syncEmployeeContacts(contactIds,session);				
	}
	
	/***
		Synchronize workplace records to Astute Payroll
	*******/
	@future(callout=true)
	public static void syncWorkplacesFuture(list<String> workplaceIds, String session){
		if(workplaceIds.size()==0 || test.isRunningTest()){
			return;
		}
		WorkplaceAstutePayrollImplementation.syncWorkplaces(workplaceIds,session);
		
	}
	/***
		Synchronize placement records to Astute Payroll
	****/
	@future(callout=true)
	public static void syncPlacementsFuture(list<String> placementIds, String session){
		if(placementIds.size()==0 || test.isRunningTest()){
			return;
		}
		PlacementAstutePayrollImplementation.syncPlacements(placementIds, session);
	}
	
	/***
		Synchronize invoice line items to Astute Payroll
	****/
	@future(callout=true)
	public static void syncInvoiceLineItemsFuture(list<String> invoiceLineItemsIds, String session){
		if(invoiceLineItemsIds.size()==0 || test.isRunningTest()){
			return;
		}
		InvoiceItemAstutePayrollImplementation.syncInvoiceLineItems(invoiceLineItemsIds,session);
		
		
	}
	

}