@isTest
private class CTCAdminControllerTest{
	@isTest
	static void ctcAdminControllerTest(){
        system.runAs(DummyRecordCreator.platformUser) {
            User currentUser = UserInformationCon.getUserDetails;
            currentUser.View_CTC_Admin__c = true;
            update currentUser;

    		CTCAdminController adminController = new CTCAdminController();
        	// Mock up a successful authorization process
        	Test.startTest();
        
        	adminController.init();
        	Test.stopTest();
        	// If users is authorized to view admin page, then they should
        	// get '0' as a result of authorization.

            system.assertEquals(adminController.is_allowed, true);
        }
	}
}