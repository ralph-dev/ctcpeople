public with sharing class APMockApiCalls {
	public static HttpResponse webServiceExpectedResponse(){
		HttpResponse httpResponse=new HttpResponse();
		httpResponse.setStatusCode(200);
		httpResponse.setStatus('OK');
		httpResponse.setBody('messages');
		return httpResponse;
	}
	@isTest
	private static void testWebservice(){
		HttpResponse webServicesTest=new HttpResponse();
		webServicesTest=APMockApiCalls.webServiceExpectedResponse();
		System.assertEquals(webServicesTest.getStatusCode(),200);
	}

}