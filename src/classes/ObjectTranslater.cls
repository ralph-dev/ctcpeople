/**
    Used to populate the field options in one dimensional criteria list 

**/
public with sharing class ObjectTranslater {
    //Used to retrieve option_field_list from the component in upper level.
    public SelectOption[] options_field_List{set;get;}
    public String objectn;
    public Integer maxcriterianumber;
    public boolean Appear_component{set;get;}
    public String textvalues{set;get;}
    public List<String> apinamelist{set;get;}
    public List<String> typelist{set;get;}
    public boolean firstset{set;get;}
    public SObject con{set;get;} 
    public String fieldtoobject{set;get;}   
    public List<Integer> componentcounter{set;get;}
    
    public void Setobjectn(String objectname){
        if(firstset){
            getFieldMap(objectname);
            objectn=objectname;
            firstset=false;
        }
        
    }
    public String getobjectn(){
        return objectn;
    }
    public void setmaxcriterianumber(Integer numbercriteria){    	
    	maxcriterianumber=numbercriteria;
    	if(componentcounter.size()==0){
	    	for(Integer i=5;i<maxcriterianumber;i++){
	        	componentcounter.add(i+1);
	        }
    	}
        
    }
    public Integer getmaxcriterianumber(){
    	return maxcriterianumber;
    }
    
    public ObjectTranslater(){
    	componentcounter=new List<Integer>(); 
    	Appear_component=false;
    	maxcriterianumber=0;        
        options_field_List=new List<SelectOption>();
        apinamelist=new List<String>();
        typelist=new List<String>();
        firstset=true;        
        textvalues=''; 
        fieldtoobject='';       
        options_field_List.add(new SelectOption('','--SELECT Field--'));
   
    }
    
    
    
    //Translater the object name into Schema SelectOption list
    public void getFieldMap(String objectn){
    	List<String> optionvalues=new List<String>();
        //Transfer the object name to Sobject
        SObjectType objToken = Schema.getGlobalDescribe().get(objectn); 
        DescribeSObjectResult objDef = objToken.getDescribe();     
        con=objToken.newSObject();
        
        Map<String,String> map_type=new Map<String,String>();  
        Map<string, SObjectField> fields_all = GetfieldsMap.getFieldMap(objectn);       
        for (Schema.SObjectField f : fields_all.values()){
        	//System.debug('##### '+f);
        	DescribeFieldResult fdr;
        	try{
        	 fdr= f.getDescribe();
        	if(fdr.getName()!='LastReferencedDate'){	        
	            String apiname=fdr.getName();
	            String labelname=fdr.getLabel();
	            //The selectOption will not take the TEXTAREA type field.
	            if(fdr.gettype().name()!='TEXTAREA'){
	            	String optionvalue=labelname+'};'+apiname;
	            	optionvalues.add(optionvalue);
	           		if(fdr.gettype().name()=='REFERENCE'){
	           			String referenceobjectname='';
	           			
	           			referenceobjectname=fdr.getReferenceTo()[0].getDescribe().getname();
	           			//System.debug('##### objectname'+referenceobjectname+' the apiname is '+apiname);
	           			String fieldtoobjectvalues=apiname+'},'+referenceobjectname;
	           			fieldtoobject=fieldtoobjectvalues+'};'+fieldtoobject;
	           			
	           		}
	           
	                                 
	                apinamelist.add(apiname);	                
	                typelist.add(fdr.gettype().name()   );
	                if(fdr.getType().name().contains('PICKLIST')){
	                    textvalues=textvalues+fdr.getName();                    
	                    for(Schema.PicklistEntry entity: fdr.getPicklistValues()){                      
	                            textvalues=textvalues+'},'+entity.getValue();
	                                                
	                    }
	                    textvalues=textvalues+'};';
	                    
	                }
           		}
        	  
            }        
        	}catch(Exception e){
        		continue;
        	}            
            
        }
        
       optionvalues.sort();
       for(String va: optionvalues){
	       	try{
	       		String[] nameapilabel=va.split('};');
	       		String namelabel=nameapilabel[0];
	       		String nameapi=nameapilabel[1];
	       		options_field_List.add(new SelectOption(nameapi,namelabel));
	       	}catch(Exception ex){
	       		continue;
	       	}  
       }
    
    }
    
    
    
}