public with sharing class OauthTokenAdminPageController {
  public String appId{get;set;}
  public String consumerKey{get;set;}
  public String secretKey{get;set;}
  public String instanceURL{get;set;}
  public String oauthToken{get;set;}
  public String refreshToken{get;set;}
  public String redirectPage{set;get;}
  public  OauthTokenAdminPageController(){
    redirectPage=ApexPages.currentPage().getParameters().get('redirectUrl');
    if(!Test.isRunningTest()){
      if(redirectPage.startsWith('/')){
      redirectPage = redirectPage.replaceFirst('/+','');
      }
    }   
  }
  
  public PageReference save(){
    consumerKey = consumerKey.trim();
    secretKey = secretKey.trim();
    
    /*
    if(appId == null || appId == '' ||
        consumerKey == null || consumerKey == '' || 
        secretKey == null || secretKey == '' ||
        instanceURL == null || instanceURL == '' ||
        oauthToken == null || oauthToken == '' ||
        refreshToken == null || refreshToken == ''){
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: OAuth information can not be empty.');
      ApexPages.addMessage(myMsg);
      return;
    }
    */
    
    SFOauthToken__c sfOauthToken = new SFOauthToken__c();
    sfOauthToken.Name__c= 'oAuthToken';
    sfOauthToken.name='oAuthToken';
    if(appId != null && appId != ''){
      sfOauthToken.App_Id__c = appId;
    }
    if(consumerKey != null && consumerKey != ''){
      sfOauthToken.ConsumerKey__c = consumerKey;
    }
    if(secretKey != null && secretKey != ''){
      sfOauthToken.SecretKey__c = secretKey;
    }
    if(instanceURL != null && instanceURL != ''){
      sfOauthToken.InstanceURL__c = instanceURL;
    }
    if(oauthToken != null && oauthToken != ''){
      sfOauthToken.OAuthToken__c = oauthToken;
    }
    if(refreshToken != null && refreshToken != ''){
      sfOauthToken.RefreshToken__c = refreshToken;
    }
    //Use the upsert function in SfOauthTokenSelector
    List<SFOauthToken__c> sfOauthTokenList = new List<SFOauthToken__c>(); //Bulkify issue
    sfOauthTokenList.add(sfOauthToken);
    Boolean isSuccess=SFOauthTokenSelector.upsertOauthToken(sfOauthTokenList);
    if(isSuccess){
      string message='OAuth information has been updated successfully.';
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, message));
    }else{
      string message='OAuth information failed to be updated.';
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
      return null;
    }
   
    if(redirectPage!=null){
      return new PageReference('/'+redirectPage);
    }else{
      return null;
    }
  }
}