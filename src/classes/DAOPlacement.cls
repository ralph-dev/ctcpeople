public class DAOPlacement extends CommonSelector{
	
	public DAOPlacement(){
		super(Placement__c.sObjectType);
	}
	
	private static DAOPlacement dao = new DAOPlacement();

	public static Placement__c getVById(String Id){
		dao.checkRead('Id,Name,Internal_Approver__c');
		return [select Id,Name,Internal_Approver__c from Placement__c where Id = :Id];
	}


	static testmethod void testGetVById(){
		System.runAs(DummyRecordCreator.platformUser) {
			Placement__c v = new Placement__c(Name='Pleacement For Test');
	 		insert v;
	 		Placement__c chkv=DAOPlacement.getVById(v.Id);
	 		system.assertEquals(v.Name,chkv.Name);
		}
	}
}