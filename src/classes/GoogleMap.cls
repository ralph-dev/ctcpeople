public with sharing class GoogleMap {
    private static String vacancyAddress;

    @AuraEnabled
    public static List<Placement_Candidate__c> findAll(Id recId){
        List<Placement_Candidate__c> cms = new List<Placement_Candidate__c >();
        cms = [Select Id, Name, Candidate__r.Name, Candidate__r.MailingAddress from Placement_Candidate__c where Placement__c =: recId];
        //system.debug('cms:'+cms);
        return cms ; 
    }

    @AuraEnabled
    public static List<Placement_Candidate__c> searchCM(Id recId, String searchTerm){
         List<Placement_Candidate__c> cms = new List<Placement_Candidate__c>();
        try{
            String query = '%'+searchTerm+'%';
            query = String.escapeSingleQuotes(query);
            System.debug('query:'+query);
           
            cms = [select Id, Name, Candidate__r.Name, Candidate__r.MailingAddress from Placement_Candidate__c where Placement__c = :recId
                    And (Candidate__r.Name like :query Or Name like :query)];
            System.debug('cms:'+cms);
        }catch (Exception e){
            System.debug(e);
        }
      
        return cms;
    }
    
    @AuraEnabled
    public static String getGeoCode(Id recId, String objName){
        String address = '';
        //Determin the object Type
        System.debug('objName:'+objName);
        if(objName == 'Placement__c'){
            address = getVacancyAddress(recId);
            System.debug('vacancyAddress 1:'+vacancyAddress);
        }else if(objName == 'Contact'){
            address = getContactAddress(recId);
            //System.debug('address:'+address);
        }else{
            
        }
        //Get GeoLocation by the address
        if(address != null){  
            return sendRequest(address);
        }else{
            return null;
        }
    }
    
    @AuraEnabled
    public static String getDirections(Id conId, String vacAddress){
        return sendRequest(conId, vacAddress);
    }
    
    private static String getVacancyAddress(Id recId){
        System.debug('recId:'+recId);
        Placement__c vac =  [Select Id, Vacancy_Address__c from Placement__c where Id=:recId];
        String address = vac.Vacancy_Address__c;
        address = EncodingUtil.urlEncode(address, 'UTF-8');
        vacancyAddress = address;
        return address;
    }
    
    private static String getContactAddress(Id recId){
        Contact con = [Select Id, MailingAddress from Contact Where Id=:recId];
        String address ='';
        if(con.MailingAddress != null){
            address += con.MailingAddress.getStreet()!=null ? con.MailingAddress.getStreet() : '';
            address += con.MailingAddress.getCity()!=null ? con.MailingAddress.getCity() : '';
            address += con.MailingAddress.getState()!=null ? con.MailingAddress.getState() : '';
            address += con.MailingAddress.getPostalCode()!=null ? con.MailingAddress.getPostalCode() : '';
        }
        //System.debug('address before encode:'+ address);
        address = EncodingUtil.urlEncode(address, 'UTF-8');
        return address;
    }
    
    private static String sendRequest(String address){
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndPoint('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key=AIzaSyBipgfOaf9MRXNe3bBdJXXMEaybFj6lPYo');
        req.setHeader('Host', 'maps.googleapis.com');
        Http http = new Http();
        HttpResponse res = http.send(req);
        //System.debug('res body:'+res.getBody());
        return res.getBody();
    }
    
    private static String sendRequest(Id conId, String vacAddress){
        String origin = getContactAddress(conId);
        //String destination = getVacancyAddress(vacId);
        System.debug('vacAddress:'+vacAddress);
        String destination = EncodingUtil.urlEncode(vacAddress, 'UTF-8');
        System.debug('destination:'+destination);
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('https://maps.googleapis.com/maps/api/directions/json?origin='+origin+'&destination='
                        +destination+'&key=AIzaSyBipgfOaf9MRXNe3bBdJXXMEaybFj6lPYo');
        req.setHeader('Host', 'maps.googleapis.com');
        Http http = new Http();
        HttpResponse res = http.send(req);
        return res.getBody();
    }
    
    
    private static Integer getDistance(Object dist1, Object dist2){
        return 1;
    }
    
    
}