/**
* modified by andy for security review II
*/

public with sharing class AccountServices {
	/***
		Convert Account Info to Biller.
					---Alvin
	
	***/
	public list<map<String,Object>> convertAccountToBiller(list<Account> accounts){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> accountBillerMap=ctcPeopleHelperService.getAccountBillerMap('Biller_Mapping');
		list<map<String,Object>> billerInfos=new list<map<String,Object>>();	
		for(Account record : accounts){
			billerInfos.add(ctcPeopleHelperService.convertToAPFormat(record, accountBillerMap)); 
		}
		return 	billerInfos;				
	}
	
	/**
		Convert Account to ID_JSON map
					--Alvin
	
	***/
	public map<String,Object> convertAccountToBillerMap(list<Account> accounts){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> accountBillerMap=ctcPeopleHelperService.getAccountBillerMap('Biller_Mapping');
		map<String,Object> billerInfoMap=new map<String,Object>();	
		for(Account record : accounts){
			billerInfoMap.put(record.Id,ctcPeopleHelperService.convertToAPFormat(record, accountBillerMap)); 
		}
		return billerInfoMap;
	}
	/**
		Convert Account ids to Accounts
	
	**/
	public list<Account> retrieveAccountsById(list<String> accountIds){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		String queryStr=ctcPeopleHelperService.formQueryFromMap(ctcPeopleHelperService.getAccountBillerMap('Biller_Mapping'));
		
		list<Account> accounts=new list<Account>();
		
		//queryStr='select '+queryStr+' from Account where id in: accountIds';
		//System.debug('The query str is '+queryStr);
		//accounts=database.query(queryStr);
		accounts = new AccountSelector().setParam('accountIds', accountIds).get(queryStr, 'id in: accountIds');
		
		return accounts;
	}
	
	
	

}