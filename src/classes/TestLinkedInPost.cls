@isTest
private class TestLinkedInPost {
	
	static testmethod void testgetLinkedInPost(){
		system.runAs(DummyRecordCreator.platformUser) {
			Advertisement__c testad = TestLinkedInCloneAd.dummyAd();
			Test.setCurrentPage(Page.LinkedInPost); 
			//ApexPages.currentpage().getParameters().put('id','test');
			ApexPages.Standardcontroller testStdCon = new ApexPages.Standardcontroller(testad);
			LinkedInPost testLinkedInPost = new LinkedInPost(testStdCon);
			PageReference testsaveadastemplate = testLinkedInPost.saveadastemplate();
			system.assert(testsaveadastemplate == null);
			PageReference testsavead = testLinkedInPost.saveAd();
			system.assert(testsavead == null);
			PageReference testupdatead = testLinkedInPost.updateAd();
			system.assert(testupdatead == null);
			PageReference testpostad = testLinkedInPost.postad();
			system.assert(testpostad == null);
			Boolean testUpdateCompanyDescription = LinkedInPost.LinkedinSaveCompanyDescriptionAsDefault('test');
			system.assert(testUpdateCompanyDescription);
			PageReference testsignout = testLinkedInPost.signout();
			system.assertNotEquals(null,testsignout);
		}
	}
	
	static testmethod void testupdateLinkedInPost(){
		system.runAs(DummyRecordCreator.platformUser) {
			Advertisement__c testad = TestLinkedInCloneAd.dummyAd();
			testad.Job_Posting_Status__c = 'Posted Successfully';
			testad.Status__c = 'Active';
			update testad;
			Test.setCurrentPage(Page.LinkedInPost); 
			ApexPages.currentpage().getParameters().put('id',testad.id);
			ApexPages.currentpage().getParameters().put('action','edit');
			ApexPages.Standardcontroller testStdCon = new ApexPages.Standardcontroller(testad);
			LinkedInPost testLinkedInPost = new LinkedInPost(testStdCon);
			PageReference testupdatead = testLinkedInPost.updateAd();
			system.assert(testupdatead == null);
		}
	}
	
	static testMethod void test() {
		system.runAs(DummyRecordCreator.platformUser) {
		    Advertisement__c testad = TestLinkedInCloneAd.dummyAd();
			testad.Job_Posting_Status__c = 'Posted Successfully';
			testad.Status__c = 'Active';
			update testad;
			Test.setCurrentPage(Page.LinkedInPost); 
			ApexPages.currentpage().getParameters().put('id',testad.id);
			ApexPages.currentpage().getParameters().put('action','edit');
		    LinkedInPost c = new LinkedInPost(new ApexPages.Standardcontroller(testad));
		    c.initSelectOption();
		    
		    c.init();
		    try{
		        system.assertNotEquals(null, LinkedInPost.insertEc2('aaaFsghsafkdsGafjk1231413a'));
		    } catch (Exception e) {
		    }
		    
		    system.assertEquals(null, LinkedInPost.getlinkedinCountryCode('AU', 'aaaFsghsafkdsGafjk1231413a'));
		    system.assertEquals(null, c.customerErrorMessage());
		}
	}
}