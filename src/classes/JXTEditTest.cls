@isTest
private class JXTEditTest {
    
    // Added by Lina
    static testmethod void testEmailServiceToEmailServiceToApplicationForm() {
    	System.runAs(DummyRecordCreator.platformUser) {
		    //create data
		    StyleCategory__c sc = new StyleCategory__c();
			sc = StyleCategoryDummyRecordCreator.setJobboardStyleCategory();
		    Placement__c v = new Placement__c();
			insert v;
			Advertisement__c ad = AdvertisementDummyRecordCreator.generateJXTAdDummyRecord();
			ad.Vacancy__c=v.id; 
			insert ad;
			DummyRecordCreator.createSkillGroup();
	
			

		    JobPostingSettings__c setting = new JobPostingSettings__c();
		    setting.SetupOwnerId = UserInfo.getUserId();
		    setting.Enable_JXT_Email_Service__c = true;
		    setting.Default_Email_Service_Address__c = 'test@test.com';
            insert setting;
		
		    JXTPost jp = new JXTPost(new ApexPages.StandardController(ad));
		    jp.saveAd();
		    jp.feedXml();
            
            // update with email service enabled
            JXTEdit je = new JXTEdit(new ApexPages.StandardController(jp.newAd));
            system.assertEquals('test@test.com', je.ad.Prefer_Email_Address__c);
            je.applicationEmail = 'applicationEmail@test.com';
            system.assertEquals(je.getReferralItems().size(),2);  
		    List<SelectOption> JXTgetTemplateOptions = je.getTemplateOptions();
		    system.assert(JXTgetTemplateOptions.size()>0);
		    List<SelectOption> JXTgetSelectSkillGroupList = je.getSelectSkillGroupList();
		    system.assert(JXTgetSelectSkillGroupList.size()>0);
            je.saveAd();
            je.feedXml();
            system.assertEquals('active', je.ad.Job_Posting_Status__c);
	        system.assertEquals('Active', je.ad.Status__c);
            
            // update again with email service diabled (change to application form)
		    setting.Enable_JXT_Email_Service__c = false;
            update setting;            
            
            JXTEdit je2 = new JXTEdit(new ApexPages.StandardController(je.ad));
            system.assertEquals('applicationEmail@test.com', je2.ad.Prefer_Email_Address__c);
            List<SelectOption> JXTgetStyleOptions = je2.getStyleOptions();
            system.assert(JXTgetStyleOptions.size()>0);
            je2.ad.Application_Form_Style__c = sc.Id;
            je2.saveAd();
            je2.feedXml();
	        system.assertEquals('Active', je2.ad.Status__c);
		}
	}

    // Added by Lina
    static testmethod void testApplicationFormToApplicationFormToEmailService() {
    	System.runAs(DummyRecordCreator.platformUser) {
		    //create data
		    StyleCategory__c sc = new StyleCategory__c();
			sc = StyleCategoryDummyRecordCreator.setJobboardStyleCategory();
		    Placement__c v = new Placement__c();
			insert v;
			Advertisement__c ad = AdvertisementDummyRecordCreator.generateJXTAdDummyRecord();
			ad.Vacancy__c=v.id; 
			ad.Skill_Group_Criteria__c = 'G00001;G00002';
			insert ad;

		
			PCAppSwitch__c pcSwitch = new PCAppSwitch__c();
            pcSwitch.Enable_Daxtra_Skill_Parsing__c = true;
            insert pcSwitch;
            
		    JobPostingSettings__c setting = new JobPostingSettings__c();
		    setting.SetupOwnerId = UserInfo.getUserId();
		    setting.Enable_JXT_Email_Service__c = false;
		    setting.Default_Email_Service_Address__c = 'test@test.com';
            insert setting;            
            
		    JXTPost jp = new JXTPost(new ApexPages.StandardController(ad));
		    jp.ad.Application_Form_Style__c = sc.Id;
		    jp.saveAd();
		    jp.feedXml();
		    
		    jp.newAd.Status__c = 'Clone to Post';
		    update jp.newAd;
		    
            // update with email service diabled (application form)
            JXTEdit je = new JXTEdit(new ApexPages.StandardController(jp.newAd));
            system.assertEquals(je.getReferralItems().size(),2);  
		    List<SelectOption> JXTgetTemplateOptions = je.getTemplateOptions();
		    system.assert(JXTgetTemplateOptions.size()>0);
            List<SelectOption> JXTgetStyleOptions = je.getStyleOptions();
            system.assert(JXTgetStyleOptions.size()>0);		    
            je.saveAd();
            je.feedXml();
		    system.assertEquals('In Queue', je.ad.Job_Posting_Status__c);
	        system.assertEquals('Active', je.ad.Status__c);

            // update again with email service enabled (change to email service)
            je.ad.Status__c = 'Archived';
		    update je.ad;
            
		    setting.Enable_JXT_Email_Service__c = true;
            update setting;
            
            JXTEdit je2 = new JXTEdit(new ApexPages.StandardController(je.ad));
            system.assertEquals('', je2.ad.Prefer_Email_Address__c);
            system.assertEquals('test@test.com', je2.applicationEmail);
            je2.saveAd();
            je2.feedXml();            
            system.assertEquals('In Queue', je2.ad.Job_Posting_Status__c);
	        system.assertEquals('Archived', je2.ad.Status__c);
		}
	}
	
	static testmethod void testOtherMethod() {
		System.runAs(DummyRecordCreator.platformUser) {
	    //Test Some Other function
        system.assertNotEquals(null, JXTEdit.customerErrorMessage(1));
		}
	}

}