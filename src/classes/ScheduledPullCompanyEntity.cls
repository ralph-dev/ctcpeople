global class ScheduledPullCompanyEntity implements Schedulable {
    global void execute(SchedulableContext sc) {
        actScheduledJobs();
    }
    @future(callout=true)
    public static void actScheduledJobs(){
        APSynchronizeServices apSynServices=new APSynchronizeServices();
        apSynServices.synchroniseCompanyEntity();
    }
}