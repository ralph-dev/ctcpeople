public class CTCSettingUploadDocumentServices {
    private list<SelectOption>  docTypeOptions=new list<SelectOption>();
    private map<String,list<Schema.FieldSetMember>> typeFieldMap=new map<String,list<Schema.FieldSetMember>>();
    public list<SelectOption>  getDocTypeOptions(){
        return docTypeOptions;
    }
    public map<String,list<Schema.FieldSetMember>> getTypeFieldMap(){
        return typeFieldMap;
    }
    public list<Schema.FieldSetMember>  setupOptionsForDocTypes(String srcObj){
        list<SelectOption> listOptions=retrieveSelectOptionsForDocTypes(srcObj);
        //System.debug('The list options is '+listOptions);
        map<String,Schema.FieldSet> webDocFieldMap=getWebDocCriteriaFields();
        //System.debug('The webdoc fielMap is '+webDocFieldMap);
        
        list<Schema.FieldSetMember> allFields=new list<Schema.FieldSetMember>();
        String nsPrefix=PeopleCloudHelper.getPackageNamespace();
        String defaultSetName=nsPrefix+'default_upload_documents';
        Schema.FieldSet defaultFS = webDocFieldMap.get(defaultSetName);
        List<Schema.FieldSetMember> defaultFielda = defaultFS.getFields();        
        allFields.addAll(defaultFielda);
        
        
        for(SelectOption option: listOptions){
            String fieldSetName=convertCharToApiName(option.getValue());
            fieldSetName=fieldSetName.toLowerCase(); 
             typeFieldMap.put(option.getValue(),defaultFielda);
            if(webDocFieldMap.keyset().contains(fieldSetName)){
                System.debug('Get into the list option');
                Schema.FieldSet fs = webDocFieldMap.get(fieldSetName);
                List<Schema.FieldSetMember> fielda = new list<Schema.FieldSetMember>();
                fielda.addAll(defaultFielda);
                fielda.addAll(fs.getFields());
                typeFieldMap.put(option.getValue(),fielda);
                allFields.addAll(fielda);
            }
        }
        System.debug('The all fields size is '+allFields.size());
        set<String> duplicateCatset=new set<String>();
        Integer index=0;
        while(index<allFields.size()){
            Schema.FieldSetMember singleFieldSetMe=allFields.get(index);
            //System.debug('The specific field is '+ singleFieldSetMe.getFieldPath());
            if(duplicateCatset.contains(singleFieldSetMe.getFieldPath())){
                //System.debug('The fieldName is '+singleFieldSetMe.getFieldPath());
                allFields.remove(index);
                continue;
            }
            duplicateCatset.add(singleFieldSetMe.getFieldPath());
            index++;
            
        }
        System.debug('The size  of all feilds after filter is '+allFields.size());
        return allFields;
    }
    public String convertCharToApiName(string fieldName){
        //regular expressions to be used for replacing
        string specialCharPatterns = '[^\\w]+';
        string multiUnderscorePattern = '_+';
        //replace special chars with underscores, and multiple underscores with one
        fieldName = fieldName.replaceAll(specialCharPatterns,'_').replaceAll(multiUnderscorePattern,'_');
        //remove leading underscores
        fieldName = fieldName.left(1) == '_' ? fieldName.substring(1) : fieldName;
        //remove trailing underscores
        fieldName = fieldName.right(1) == '_' ? fieldName.substring(0,fieldName.length()-1) : fieldName;
        return fieldName;
    }
    //Retreive picklist values for Web Document for different src object.
    private list<SelectOption> retrieveSelectOptionsForDocTypes(String srcObj){
        String resumeAndFilesDocTypeMappingRT='Resume_And_Files_Doc_Type_Mapping';
         
        CTCPeopleSettingHelperSelector 
            cTCPeopleSettingHelper=new CTCPeopleSettingHelperSelector();

         list<CTCPeopleSettingHelper__c> resumeAndFilesDocTypeList =
            cTCPeopleSettingHelper.fetchResAndFilesDocTypeListByRTAndObjName(
                resumeAndFilesDocTypeMappingRT,
                srcObj);
        if(resumeAndFilesDocTypeList.size()>0)
        {
            String docTypeMultiPicklistString = 
                resumeAndFilesDocTypeList.get(0).Resume_And_Files_Document_Type__c;
            if(docTypeMultiPicklistString!=null){
                String[] docTypeList = docTypeMultiPicklistString.split(';');
                for(String docType: docTypeList)
                {
                   SelectOption singleSelectOption=new SelectOption(docType,docType);
                   docTypeOptions.add(singleSelectOption);
                }
           }
        }
        //If there is no standard map, will retreive the whole picklist options.
        if(docTypeOptions.size()==0){
            Schema.DescribeFieldResult fieldWebDoc =
                 Web_Document__c.Document_Type__c.getDescribe();
            List<Schema.PicklistEntry> fieldWebDocOptions = fieldWebDoc.getPicklistValues();
            
            for( Schema.PicklistEntry singlepickEntity : fieldWebDocOptions)
            {
                docTypeOptions.add(new SelectOption(singlepickEntity.getLabel(), singlepickEntity.getValue()));
            }  
        }
        
       return  docTypeOptions;     
    }
    //Retrieve fieldsets according to documentTypes
     public map<String,Schema.FieldSet> getWebDocCriteriaFields(){
        String jsonString='';
        CustomFieldList cfl=new CustomFieldList();
        map<String,Schema.FieldSet> finalFieldsMap=new map<String,Schema.FieldSet>();
        finalFieldsMap=cfl.getCustomFieldSetMap('Web_Document__c');      
         //System.debug('The final fieldMap is '+finalFieldsMap);
        return finalFieldsMap;
    } 
    

}