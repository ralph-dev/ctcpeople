public class IndeedPostUtil {
    //Check the LinkedIn Validation
    public static Boolean checkIndeedAccountEnable(){
        Boolean indeedAccountEnable = true;
        
        String IndeedAccount = UserInformationCon.getUserDetails.Indeed_Account__c;
        
        if(IndeedAccount == null || IndeedAccount == ''){
            indeedAccountEnable = false;
        }else{

            Advertisement_Configuration__c currentIndeedaccount = new AdvertisementConfigurationSelector().getIndeedAccountById(IndeedAccount);
            if(currentIndeedaccount == null){
                indeedAccountEnable = false;
            }
        } 
        return indeedAccountEnable;
    }

    public static List<SelectOption> GetIndeedSponsoredSelect() {
        List<SelectOption> indeedSponsoredOption = new List<SelectOption>();
        indeedSponsoredOption.add(new SelectOption('0', 'Not a sponsored Indeed Ad'));
        indeedSponsoredOption.add(new SelectOption('100', '$100 -  6 times as many (approximate)'));
        indeedSponsoredOption.add(new SelectOption('200', '$200 - 10 times as many (approximate)'));
        indeedSponsoredOption.add(new SelectOption('300', '$300 - 14 times as many (approximate)'));
        return indeedSponsoredOption;
    }

    public static List<SelectOption> getIndeedScreeingQuestionSelect(String indeedAccount) {
        List<SelectOption> indeedScreeingQuestionOption = new List<SelectOption>();
        String queryStr = 'select id, name from Application_Question_Set__c where Advertisement_Account__c=: indeedAccount';
        
        CommonSelector.checkRead(Application_Question_Set__c.sObjectType,'id, name');
        List<Application_Question_Set__c> questionSet = Database.query(queryStr);
        indeedScreeingQuestionOption.add(new SelectOption('','--None--'));
        if(questionSet!=null && questionSet.size()>0){
            for(Application_Question_Set__c  question : questionSet) {
                indeedScreeingQuestionOption.add(new SelectOption(question.id,question.name));
            }
        }
        return indeedScreeingQuestionOption;
    }
}