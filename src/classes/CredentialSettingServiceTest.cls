@isTest
private class CredentialSettingServiceTest {

	public static testMethod void testInsertIndeedCredential() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        
	        ProtectedCredential pc = CredentialSettingService.insertIndeedCredential('test indeed', 'test indeed token');
	    
    	    System.assertNotEquals(null, pc, 'failed to insertIndeedCredential ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test indeed' ,pc.name,  'name error');
    	    System.assertEquals('test indeed token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_INDEED ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test indeed');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test indeed' ,pc.name,  'name error');
    	    System.assertEquals('test indeed token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_INDEED ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
    	    
    	    
    	    Credential_Setting__c cs = Credential_Setting__c.getInstance('test indeed');
    	    System.assertNotEquals(null, cs, 'failed to insertIndeedCredential ');
    	    
    	    System.debug('Custom setting Id : ' + cs.Id);
    	    System.assertNotEquals(null,cs.Id,  'Id error');
    	    System.assertEquals('test indeed' ,cs.Name,  'name error');
    	    System.assertEquals('test indeed token',cs.Token__c,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,cs.Type__c,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_INDEED ,cs.Provider__c,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,cs.Object_Name__c,  'object name error');
	    }
 

	}
	
	public static testMethod void testUpdateIndeedCredential() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        
	        CredentialSettingService.insertIndeedCredential('test indeed', 'test indeed token');
	    
    	    ProtectedCredential pc = CredentialSettingService.updateIndeedCredential('test indeed', 'updated indeed token');
    	    
    	    System.assertNotEquals(null, pc, 'failed to insertIndeedCredential ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test indeed' ,pc.name,  'name error');
    	    System.assertEquals('updated indeed token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_INDEED ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test indeed');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test indeed' ,pc.name,  'name error');
    	    System.assertEquals('updated indeed token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_INDEED ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
    	    
    	    
    	    Credential_Setting__c cs = Credential_Setting__c.getInstance('test indeed');
    	    System.assertNotEquals(null, cs, 'failed to insertIndeedCredential ');
    	    
    	    System.debug('Custom setting Id : ' + cs.Id);
    	    System.assertNotEquals(null,cs.Id,  'Id error');
    	    System.assertEquals('test indeed' ,cs.Name,  'name error');
    	    System.assertEquals('updated indeed token',cs.Token__c,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,cs.Type__c,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_INDEED ,cs.Provider__c,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,cs.Object_Name__c,  'object name error');
	        
	    }
	    
	    

	}
	
	public static testMethod void testUpsertIndeedCredential() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        
	        ProtectedCredential pc = CredentialSettingService.upsertIndeedCredential('test indeed', 'test indeed token');
	    
    	    System.assertNotEquals(null, pc, 'failed to insertIndeedCredential ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test indeed' ,pc.name,  'name error');
    	    System.assertEquals('test indeed token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_INDEED ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test indeed');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test indeed' ,pc.name,  'name error');
    	    System.assertEquals('test indeed token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_INDEED ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.upsertIndeedCredential('test indeed', 'updated indeed token');
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test indeed' ,pc.name,  'name error');
    	    System.assertEquals('updated indeed token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_INDEED ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test indeed');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test indeed' ,pc.name,  'name error');
    	    System.assertEquals('updated indeed token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_INDEED ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
	        
	    }
	    
	    

	}
	
	
	
	public static testMethod void testInsertandUpdateJXTCredential() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        ProtectedCredential pc = CredentialSettingService.insertJXTCredential('test JXT', 'test JXT username','test JXT password');
	    
    	    System.assertNotEquals(null, pc, 'failed to insertJXTCredential ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test JXT' ,pc.name,  'name error');
    	    System.assertEquals('test JXT username',pc.username,  'username error ');
    	    System.assertEquals('test JXT password',pc.password,  'password error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_JXT ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test JXT');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test JXT' ,pc.name,  'name error');
    	    System.assertEquals('test JXT username',pc.username,  'username error ');
    	    System.assertEquals('test JXT password',pc.password,  'password error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_JXT ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.updateJXTCredential('test JXT', 'updated JXT username','updated JXT password');
    	    
    	    System.assertEquals('test JXT' ,pc.name,  'name error');
    	    System.assertEquals('updated JXT username',pc.username,  'username error ');
    	    System.assertEquals('updated JXT password',pc.password,  'password error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_JXT ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
	        
	    }
	    
	    
	    

	}
	
	public static testMethod void testUpsertJXTCredential() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        ProtectedCredential pc = CredentialSettingService.upsertJXTCredential('test JXT', 'test JXT username','test JXT password');
	    
    	    System.assertNotEquals(null, pc, 'failed to upsertJXTCredential ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test JXT' ,pc.name,  'name error');
    	    System.assertEquals('test JXT username',pc.username,  'username error ');
    	    System.assertEquals('test JXT password',pc.password,  'password error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_JXT ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test JXT');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test JXT' ,pc.name,  'name error');
    	    System.assertEquals('test JXT username',pc.username,  'username error ');
    	    System.assertEquals('test JXT password',pc.password,  'password error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_JXT ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.upsertJXTCredential('test JXT', 'updated JXT username','updated JXT password');
    	    
    	    System.assertEquals('test JXT' ,pc.name,  'name error');
    	    System.assertEquals('updated JXT username',pc.username,  'username error ');
    	    System.assertEquals('updated JXT password',pc.password,  'password error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_JXT ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG ,pc.relatedTo,  'object name error');
	        
	    }
	    
	    
	    

	}
	
	

    public static testMethod void testInsertandUpdateCareerOneCredential() {
        
        System.runAs(DummyRecordCreator.platformUser) {
            ProtectedCredential pc = CredentialSettingService.insertCareerOneCredential('test C1', 'username','test C1 password');
	    
    	    System.assertNotEquals(null, pc, 'failed to insertCareerOneCredential ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test C1' ,pc.name,  'name error');
    	    
    	    System.assertEquals('username',pc.username,  'username error ');
    	    System.assertEquals('test C1 password',pc.password,  'password error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_CAREERONE ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_STYLECATEGORY ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test C1');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test C1' ,pc.name,  'name error');
    	    
    	    System.assertEquals('username',pc.username,  'username error ');
    	    System.assertEquals('test C1 password',pc.password,  'password error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_CAREERONE ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_STYLECATEGORY ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.updateCareerOneCredential('test C1', 'updated username','updated C1 password');
    	    
    	    System.assertEquals('test C1' ,pc.name,  'name error');
    	    
    	    System.assertEquals('updated username',pc.username,  'username error ');
    	    System.assertEquals('updated C1 password',pc.password,  'password error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_CAREERONE ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_STYLECATEGORY ,pc.relatedTo,  'object name error');
            
            
        }
	    
	    
	    

	}
	
	public static testMethod void testUpsertCareerOneCredential() {
	    
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        ProtectedCredential pc = CredentialSettingService.upsertCareerOneCredential('test C1', 'username','test C1 password');
	    
    	    System.assertNotEquals(null, pc, 'failed to upsertCareerOneCredential ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test C1' ,pc.name,  'name error');
    	    
    	    System.assertEquals('username',pc.username,  'username error ');
    	    System.assertEquals('test C1 password',pc.password,  'password error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_CAREERONE ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_STYLECATEGORY ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test C1');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test C1' ,pc.name,  'name error');
    	    
    	    System.assertEquals('username',pc.username,  'username error ');
    	    System.assertEquals('test C1 password',pc.password,  'password error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_CAREERONE ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_STYLECATEGORY ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.upsertCareerOneCredential('test C1','updated username', 'updated C1 password');
    	    
    	    System.assertEquals('test C1' ,pc.name,  'name error');
    	    
    	    System.assertEquals('updated username',pc.username,  'username error ');
    	    System.assertEquals('updated C1 password',pc.password,  'password error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_CAREERONE ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_STYLECATEGORY ,pc.relatedTo,  'object name error');
	        
	    }
	    
	    
	    

	}
	

    public static testMethod void testInsertandUpdateAstutePayrollCredential() {
        
        System.runAs(DummyRecordCreator.platformUser) {
            ProtectedCredential pc = CredentialSettingService.insertAstutePayrollCredential('test AP', 'test AP username', 'test AP password', 'test AP key');
	    
    	    System.assertNotEquals(null, pc, 'failed to insertAstutePayrollCredential ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test AP' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test AP username',pc.username,  'username error ');
    	    System.assertEquals('test AP password',pc.password,  'password error ');
    	    System.assertEquals('test AP key',pc.key,  'key error ');
    	    System.assertEquals(ProtectedCredential.TYPE_PAYROLL ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_ASTUTE ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_STYLECATEGORY ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test AP');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test AP' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test AP username',pc.username,  'username error ');
    	    System.assertEquals('test AP password',pc.password,  'password error ');
    	    System.assertEquals('test AP key',pc.key,  'key error ');
    	    System.assertEquals(ProtectedCredential.TYPE_PAYROLL ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_ASTUTE ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_STYLECATEGORY ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.updateAstutePayrollCredential('test AP', 'updated AP username', 'updated AP password', 'updated AP key');
    	    
    	    System.assertEquals('test AP' ,pc.name,  'name error');
    	    
    	    System.assertEquals('updated AP username',pc.username,  'username error ');
    	    System.assertEquals('updated AP password',pc.password,  'password error ');
    	    System.assertEquals('updated AP key',pc.key,  'key error ');
    	    System.assertEquals(ProtectedCredential.TYPE_PAYROLL ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_ASTUTE ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_STYLECATEGORY ,pc.relatedTo,  'object name error');
            
            
        }
	    
	    
	    

	}
	
	public static testMethod void testUpsertAstutePayrollCredential() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        
	        ProtectedCredential pc = CredentialSettingService.upsertAstutePayrollCredential('test AP', 'test AP username', 'test AP password', 'test AP key');
	    
    	    System.assertNotEquals(null, pc, 'failed to upsertAstutePayrollCredential ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test AP' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test AP username',pc.username,  'username error ');
    	    System.assertEquals('test AP password',pc.password,  'password error ');
    	    System.assertEquals('test AP key',pc.key,  'key error ');
    	    System.assertEquals(ProtectedCredential.TYPE_PAYROLL ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_ASTUTE ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_STYLECATEGORY ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test AP');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test AP' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test AP username',pc.username,  'username error ');
    	    System.assertEquals('test AP password',pc.password,  'password error ');
    	    System.assertEquals('test AP key',pc.key,  'key error ');
    	    System.assertEquals(ProtectedCredential.TYPE_PAYROLL ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_ASTUTE ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_STYLECATEGORY ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.upsertAstutePayrollCredential('test AP', 'updated AP username', 'updated AP password', 'updated AP key');
    	    
    	    System.assertEquals('test AP' ,pc.name,  'name error');
    	    
    	    System.assertEquals('updated AP username',pc.username,  'username error ');
    	    System.assertEquals('updated AP password',pc.password,  'password error ');
    	    System.assertEquals('updated AP key',pc.key,  'key error ');
    	    System.assertEquals(ProtectedCredential.TYPE_PAYROLL ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_ASTUTE ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_STYLECATEGORY ,pc.relatedTo,  'object name error');
	        
	    }
	    
	    
	    

	}
	
	public static testMethod void testInsertandUpdateLinkedInOAuthService() {
	    
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        
	        ProtectedCredential pc = CredentialSettingService.insertLinkedInOAuthService('test', 'test key', 'test secret');
	    
    	    System.assertNotEquals(null, pc, 'failed to insertLinkedInOAuthService ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test key',pc.key,  'key error ');
    	    System.assertEquals('test secret',pc.secret,  'secret error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_LINKEDIN ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_OAUTHSERVICE ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test key',pc.key,  'key error ');
    	    System.assertEquals('test secret',pc.secret,  'secret error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_LINKEDIN ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_OAUTHSERVICE ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.updateLinkedInOAuthService('test', 'updated key', 'updated secret');
    	    
    	    System.assertEquals('test' ,pc.name,  'name error');
    	    
    	    System.assertEquals('updated key',pc.key,  'key error ');
    	    System.assertEquals('updated secret',pc.secret,  'secret error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_LINKEDIN ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_OAUTHSERVICE ,pc.relatedTo,  'object name error');
	    }
	    
	    
	    

	}
	
	
	public static testMethod void testUpsertLinkedInOAuthService() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        
	        ProtectedCredential pc = CredentialSettingService.upsertLinkedInOAuthService('test', 'test key', 'test secret');
	    
    	    System.assertNotEquals(null, pc, 'failed to upsertLinkedInOAuthService ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test key',pc.key,  'key error ');
    	    System.assertEquals('test secret',pc.secret,  'secret error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_LINKEDIN ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_OAUTHSERVICE ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test key',pc.key,  'key error ');
    	    System.assertEquals('test secret',pc.secret,  'secret error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_LINKEDIN ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_OAUTHSERVICE ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.upsertLinkedInOAuthService('test', 'updated key', 'updated secret');
    	    
    	    System.assertEquals('test' ,pc.name,  'name error');
    	    
    	    System.assertEquals('updated key',pc.key,  'key error ');
    	    System.assertEquals('updated secret',pc.secret,  'secret error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_LINKEDIN ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_OAUTHSERVICE ,pc.relatedTo,  'object name error');
	        
	    }
	    
	    
	    

	}
	
	public static testMethod void testInsertandUpdateLinkedInOAuthToken() {
	    
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        ProtectedCredential pc = CredentialSettingService.insertLinkedInOAuthToken('test', 'test secret', 'test token');
	    
    	    System.assertNotEquals(null, pc, 'failed to insertLinkedInOAuthToken ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test secret',pc.secret,  'secret error ');
    	    System.assertEquals('test token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_LINKEDIN ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_OAUTHTOKEN ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test secret',pc.secret,  'secret error ');
    	    System.assertEquals('test token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_LINKEDIN ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_OAUTHTOKEN ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.updateLinkedInOAuthToken('test', 'test secret', 'test token');
    	    
    	    System.assertEquals('test' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test secret',pc.secret,  'secret error ');
    	    System.assertEquals('test token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_LINKEDIN ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_OAUTHTOKEN ,pc.relatedTo,  'object name error');
	        
	    }
	    
	    
	    

	}
	
	public static testMethod void testUpsertLinkedInOAuthToken() {
	    
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        
	        ProtectedCredential pc = CredentialSettingService.upsertLinkedInOAuthToken('test', 'test secret', 'test token');
	    
    	    System.assertNotEquals(null, pc, 'failed to upsertLinkedInOAuthToken ');
    	    
    	    System.debug('Custom setting Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test secret',pc.secret,  'secret error ');
    	    System.assertEquals('test token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_LINKEDIN ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_OAUTHTOKEN ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.getCredential('test');
    	    System.debug('Credentials Id : ' + pc.id);
    	    System.assertNotEquals(null,pc.id,  'Id error');
    	    System.assertEquals('test' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test secret',pc.secret,  'secret error ');
    	    System.assertEquals('test token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_LINKEDIN ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_OAUTHTOKEN ,pc.relatedTo,  'object name error');
    	    
    	    pc = CredentialSettingService.upsertLinkedInOAuthToken('test', 'test secret', 'test token');
    	    
    	    System.assertEquals('test' ,pc.name,  'name error');
    	    
    	    System.assertEquals('test secret',pc.secret,  'secret error ');
    	    System.assertEquals('test token',pc.token,  'token error ');
    	    System.assertEquals(ProtectedCredential.TYPE_ADVERTISEMENT ,pc.type,  'type error');
    	    System.assertEquals(ProtectedCredential.PROVIDER_LINKEDIN ,pc.provider,  'provider error');
    	    System.assertEquals(ProtectedCredential.RELATED_TO_OAUTHTOKEN ,pc.relatedTo,  'object name error');
	        
	    }
	    
	    
	    

	}

    

}