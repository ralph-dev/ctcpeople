/*
	Author by Jack
	Generate Email List under Folder name
*/

public with sharing class SendResumeEmailTemplate {
	
	public List<EmailTemplate> emailTempList{get;set;}
	public List<SelectOption> tempOptions{get; set;}
	public Map<Id,EmailTemplate> tempMap {get; set;}

	//Init the Email Template Tree View data
	public WrapperFolder newWrapFolder{get; set;}
	public List<SendResumeEmailTem> newSendResTem{get; set;}
	public String selTemplateId{get; set;}
			
	public void initEmailTemplate(){
		try{
			tempMap = new Map<Id,EmailTemplate>();
			tempOptions = new List<SelectOption>();
			emailTempList = DaoEmailTemplate.getAllSupportedTemplates();			
			newWrapFolder = null;
			newSendResTem = new List<SendResumeEmailTem>();
			set<Id> emailTemFolIds = new set<Id>{};
			for(EmailTemplate emailTmp:emailTempList){
	            if(!emailTmp.Name.contains('QSRTEMP_') && !emailTmp.Name.contains('BE_CL_')){
	                tempOptions.add(new SelectOption(emailTmp.Id,emailTmp.Name));
	                //tempMap.put(emailTmp.Id,emailTmp);
	                emailTemFolIds.add(emailTmp.folderid);	
	            }
	        }
	        
	        //Sort the Email folder by Name. Add by Jack on 17/06/2013
	        //system.debug('emailTemFolIds =' + emailTemFolIds);
	        //Put the User Id to WrapFolder
	        if(emailTemFolIds.contains(UserInfo.getUserId())){
	        	newWrapFolder = new WrapperFolder(UserInfo.getUserId());
				newSendResTem.add(new SendResumeEmailTem(newWrapFolder));
	        }
	        //Put the other email folder to the list
	        List<Folder> emailtemplatefolders = DaoFolder.getFolderByIds(emailTemFolIds);
	        //system.debug('emailtemplatefolders =' + emailtemplatefolders );
	        for(Folder newid : emailtemplatefolders){
	        //for(Id newid : emailTemFolIds){
				newWrapFolder = new WrapperFolder(newid.id);
				//system.debug('newWrapFolder =' + newWrapFolder );
				newSendResTem.add(new SendResumeEmailTem(newWrapFolder));
				//system.debug('newSendResTem =' + newSendResTem );
			}
			
			//Put the Org Id to WrapFolder
	        if(emailTemFolIds.contains(UserInfo.getOrganizationId())){
	        	newWrapFolder = new WrapperFolder(UserInfo.getOrganizationId());
				newSendResTem.add(new SendResumeEmailTem(newWrapFolder));
	        }
		}
		catch(Exception e){
			 NotificationClass.notifyErr2Dev('SendResumeEmailTemplate/initEmailTemplate', e);
		}
		
	}
	
	static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){//Send error message
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());    
    }	
}