public with sharing class PlacementServices {
	/***
		Convert Placement Info to Employee.
					---Alvin
	
	***/
	public list<map<String,Object>> convertPlacementToEmployee(list<Placement_Candidate__c> placements){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> employMap=ctcPeopleHelperService.getPlacementEmployeeMap('Placement_Mapping');
		list<map<String,Object>> employeeInfos=new list<map<String,Object>>();	
         list<String> candidateIds=new list<String>();
        for(Placement_Candidate__c plac: placements){
            candidateIds.add(plac.Candidate__c );
        }
        ContactServices contactServ=new ContactServices();
        map<String,Object> candidates=contactServ.convertCandidateToEmployeeMap(contactServ.retrieveCandidateToEmployeesByIds(candidateIds));
        //System.debug('The candidates is '+candidates);
		for(Placement_Candidate__c record : placements){
			map<String,Object> singleObject=ctcPeopleHelperService.convertToAPFormat(record, employMap);
             for(String key: ((map<String,Object>) candidates.get(record.Candidate__c)).keySet()){
				singleObject.put(key,((map<String,Object>) candidates.get(record.Candidate__c)).get(key));
			}
			singleObject.put('USERTYPE','employee');
			employeeInfos.add(singleObject);
		}
		return 	employeeInfos;				
	}
	
	public map<String,String> fetchCanPlacementIDMap(list<String> candIds){
		PlacementSelector pSelector=new PlacementSelector();
		list<Placement_Candidate__c> placements=pSelector.getPlacedCMFromCandidates(candIds);
		map<String,String> candPlacementIDMap=new map<String,String>();
        //System.debug('candidate id is '+candIds);
		for(Placement_Candidate__c placement: placements){
			candPlacementIDMap.put(placement.candidate__c,placement.Id);	
		}
		
		return candPlacementIDMap;
	}
	
	public map<String,Object> convertPlacementToEmployeeMap(list<Placement_Candidate__c> candidateManagements){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		map<String,String> employMap=ctcPeopleHelperService.getPlacementEmployeeMap('Placement_Mapping');
		map<String,Object> employeeInfos=new map<String,Object>();	
		for(Placement_Candidate__c record : candidateManagements){
			map<String,Object> singleObject=ctcPeopleHelperService.convertToAPFormat(record, employMap);
			singleObject.put('USERTYPE','employee');           
			employeeInfos.put(record.Id,singleObject);
		}
		//System.debug('The candidate managements is '+employeeInfos);
		return 	employeeInfos;
		
	}
	/**
		Convert placementIds to placements
	
	**/
	public list<Placement_Candidate__c> retrievePlacementsByIds(list<String> placementIds){
		CTCPeopleSettingHelperServices ctcPeopleHelperService=new CTCPeopleSettingHelperServices();
		String queryStr=ctcPeopleHelperService.formQueryFromMap(ctcPeopleHelperService.getPlacementEmployeeMap('Placement_Mapping'));
		
		CommonSelector.checkRead(Placement_Candidate__c.sObjectType, queryStr);
		
		queryStr='select '+queryStr+' from Placement_Candidate__c where id in: placementIds';
		list<Placement_Candidate__c> placements=new list<Placement_Candidate__c>();

		placements=database.query(queryStr);
		return placements;
	}
	/******
		This fetch all records service is a bulk fetch records service that will retrieve all the related
		records for the placed candidate manangements.
	*********/
	public list<map<String,Object>> fetchAllRecords(list<String> placementIds){
		list<Placement_Candidate__c> candidateManagements=retrievePlacementsByIds(placementIds);
		
		CommonSelector.checkRead(Placement_Candidate__c.sObjectType,
			'id, Second_Approver__c, Approver__c ,Work_place__c,Placement__r.Company__c ,placement__r.biller_contact__c, candidate__c');
		list<Placement_Candidate__c> cms=[select id, Second_Approver__c, Approver__c ,Work_place__c,Placement__r.Company__c ,placement__r.biller_contact__c, candidate__c from Placement_Candidate__c where id in :placementIds ];
		list<String> accountIds=new list<String>();
		list<String> billingContactIds=new list<String>();
		list<String> approverIds=new list<String>();
		list<String> candidateIds=new list<String>();
		list<String> workplaceIds=new list<String>();
		list<AstutePayrollRecordsDTO> apRecordDTOs=new list<AstutePayrollRecordsDTO>();
		for(Placement_Candidate__c cm : cms){
			String accountId=cm.Placement__r.Company__c;
			String billingContactId=cm.placement__r.biller_contact__c;
			String firstApproverId=cm.Approver__c;
			String secondApproverId=cm.Second_Approver__c;
			String workplaceId=cm.Work_place__c;
			String candidateId=cm.candidate__c;
			String placementId=cm.Id;
			AstutePayrollRecordsDTO apRecordDTO=new AstutePayrollRecordsDTO();
			apRecordDTO.accountId=accountId;
			apRecordDTO.billingContactId=billingContactId;
			apRecordDTO.firstApproverId=firstApproverId;
			apRecordDTO.secondApproverId=secondApproverId;
			apRecordDTO.candidateId=candidateId;
			apRecordDTO.placementId=placementId;
			apRecordDTO.workplaceId=workplaceId;
			apRecordDTOs.add(apRecordDTO);
			if(accountId!=null){
				accountIds.add(accountId);
			}
			if(billingContactId!=null){
				billingContactIds.add(billingContactId);
			}
			if(firstApproverId!=null){
				approverIds.add(firstApproverId);
			}
			if(secondApproverId!=null){
				approverIds.add(secondApproverId);
			}
			if(candidateId!=null){
				candidateIds.add(candidateId);
			}
			if(workplaceId!=null){
				workplaceIds.add(workplaceId);
			}
		}
		AccountServices accountServ=new AccountServices();
		ContactServices contactServ=new ContactServices();
		WorkplaceServices workplaceServ=new WorkplaceServices();
        map<String,Object> billers=accountServ.convertAccountToBillerMap(accountServ.retrieveAccountsById(accountIds));
        map<String,Object> accountBillingContacts=contactServ.convertContactToBillerContactMap(contactServ.retrieveBillerContactsByIds(billingContactIds));
        map<String,Object> approvers=contactServ.convertContactToApproverMap(contactServ.retrieveApproverContactsByIds(approverIds));
        map<String,Object> candidates=contactServ.convertCandidateToEmployeeMap(contactServ.retrieveCandidateToEmployeesByIds(candidateIds));
		map<String,Object> workplaces=workplaceServ.convertWorkplaceToBillerWorkplaceMap(workplaceServ.retrieveWorkplaceByIds(workplaceIds));
		map<String,Object> placements=convertPlacementToEmployeeMap(candidateManagements);
		list<map<String,Object>> requestData=new list<map<String,Object>>(); 
		for(AstutePayrollRecordsDTO apRecordsDTO: apRecordDTOs){
			apRecordsDTO.setBiller(billers);
			apRecordsDTO.setBillingContact(accountbillingContacts);
			apRecordsDTO.setFirstApprover(approvers);
			apRecordsDTO.setSecondApprover(approvers);
			apRecordsDTO.setWorkplace(workplaces);
			apRecordsDTO.setEmployee(placements, candidates);
			requestData.add(apRecordsDTO.getRequestMap());
		}
		//System.debug('The final data is '+requestData);
		return requestData;
	}
    
    /***
		Fix for biller.

		**/
    public void fixBiller(list<String> accountIds){
        PlacementSelector placementSel=new PlacementSelector();
        list<Placement_Candidate__c> placements= placementSel.getPlacedCMFromAccounts(accountIds);
        list<String> placementIds=new list<String>();
        for(Placement_Candidate__c placement: placements){
            placementIds.add(placement.Id);
        }
        AstutePayrollFutureJobs.pusthToAPFirstTime(placementIds,userinfo.getSessionId());
    }
     /***
		Fix for biller contact.

		**/
    public void fixBillerContact(list<String> contactIds){
        PlacementSelector placementSel=new PlacementSelector();
        list<Placement_Candidate__c> placements= placementSel.getPlacedCMFromBillingContact(contactIds);
        list<String> placementIds=new list<String>();
        for(Placement_Candidate__c placement: placements){
            placementIds.add(placement.Id);
        }
        if(placementIds.size()>0){
        	AstutePayrollFutureJobs.pusthToAPFirstTime(placementIds,userinfo.getSessionId());
        }
    }
     /***
		Fix for approver.
		**/
    public void fixApprover(list<String> approverIds){
        PlacementSelector placementSel=new PlacementSelector();
        //System.debug('the approver id is '+approverIds);
        list<Placement_Candidate__c> placements= placementSel.getPlacedCMFromApprover(approverIds);
        list<String> placementIds=new list<String>();
        for(Placement_Candidate__c placement: placements){
            placementIds.add(placement.Id);
        }
        System.debug('Placement size is '+placementIds.size());
        AstutePayrollFutureJobs.pusthToAPFirstTime(placementIds,userinfo.getSessionId());
    }
    
     /***
		Fix for workplaces.
		**/
    public void fixWorkplaces(list<String> workplaceIds){
        PlacementSelector placementSel=new PlacementSelector();
        list<Placement_Candidate__c> placements= placementSel.getPlacedCMFromWorkplaces(workplaceIds);
        list<String> placementIds=new list<String>();
        for(Placement_Candidate__c placement: placements){
            placementIds.add(placement.Id);
        }
        AstutePayrollFutureJobs.pusthToAPFirstTime(placementIds,userinfo.getSessionId());
    }

}