/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is de
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployedployed
 * to a production organization to confirm correctness, ensure code
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class WebDocumentTriggerSelectorTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	S3Utils.testFlag = true;
		Web_Document__c webdocument = new Web_Document__c(name='web001', Type__c='Resume');
		Contact con = new Contact(LastName='name001',email='aabb@aabb.com');
		insert webdocument;
        
        system.debug(webdocument.id);
        
		webdocument.name = 'test001';
		update webdocument;
        
        Web_Document__c test = [select name from Web_Document__c where Id =: webdocument.Id];
        
        System.assertEquals(webdocument.name, test.name);
        
		list<String> condIds=new list<String>();
		condIds.add(con.Id);
		
		System.assertEquals(0, WebDocumentTriggerSelector.getContactAttachment(condIds).size());
    	}
    	
    }
    
    static testMethod void testGetCandResumeMap() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Disable Web Document Trigger
        TriggerHelper.DisableTriggerOnWebDocument = True;
        
        // Create data
        Contact con1 = new Contact(LastName='test1',email='test1@test.com');
        Contact con2 = new Contact(LastName='test2',email='test2@test.com');
        Contact con3 = new Contact(LastName='test3',email='test3@test.com');
        Contact con4 = new Contact(LastName='test4',email='test4@test.com');
        insert con1;
        insert con2;
        insert con3;
        insert con4;
        List<Id> conIdList = new List<Id>{con1.Id, con2.Id, con3.Id, con4.Id};
        
        Web_Document__c doc21 = new Web_Document__c(name='web21', Document_Related_To__c=con2.Id, Document_Type__c='Resume');
        insert doc21;
        
        Web_Document__c doc31 = new Web_Document__c(name='web31', Document_Related_To__c=con3.Id, Document_Type__c='Resume');
        insert doc31;
        sleep(1500);
        Web_Document__c doc32 = new Web_Document__c(name='web32', Document_Related_To__c=con3.Id, Document_Type__c='Resume');
        insert doc32;
        
        Web_Document__c doc41 = new Web_Document__c(name='web41', Document_Related_To__c=con4.Id, Document_Type__c='Resume');
        insert doc41;
        Web_Document__c doc42 = new Web_Document__c(name='web42', Document_Related_To__c=con4.Id, Document_Type__c='Resume');
        insert doc42;
        sleep(1500);
        Web_Document__c doc43 = new Web_Document__c(name='web43', Document_Related_To__c=con4.Id, Document_Type__c='Resume');
        insert doc43;
        
        List<Id> docList = new List<Id>{doc21.Id, doc32.Id, doc41.Id};

        Map<Id, Web_Document__c> candResumeMap = WebDocumentTriggerSelector.getCandResumeMap(conIdList, docList);
        System.assertEquals(null, candResumeMap.get(con1.Id));
        System.assertEquals(null, candResumeMap.get(con2.Id));
        System.assertEquals(doc31.Id, candResumeMap.get(con3.Id).Id);
        System.assertEquals(doc43.Id, candResumeMap.get(con4.Id).Id);
    	}
    }

    static testMethod void testGetCandResumeMap2() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Disable Web Document Trigger
        TriggerHelper.DisableTriggerOnWebDocument = True;
        
        // Create data
        Contact con1 = new Contact(LastName='test1',email='test1@test.com');
        Contact con2 = new Contact(LastName='test2',email='test2@test.com');
        Contact con3 = new Contact(LastName='test3',email='test3@test.com');
        Contact con4 = new Contact(LastName='test4',email='test4@test.com');
        insert con1;
        insert con2;
        insert con3;
        insert con4;
        List<Id> conIdList = new List<Id>{con1.Id, con2.Id, con3.Id, con4.Id};

        Web_Document__c doc11 = new Web_Document__c(name='web11', Document_Related_To__c=con1.Id, Document_Type__c='Resume');
        insert doc11;
        Web_Document__c doc12 = new Web_Document__c(name='web12', Document_Related_To__c=con1.Id, Document_Type__c='Resume');
        insert doc12;
        Web_Document__c doc21 = new Web_Document__c(name='web21', Document_Related_To__c=con2.Id, Document_Type__c='Resume');
        insert doc21;
        Web_Document__c doc22 = new Web_Document__c(name='web22', Document_Related_To__c=con2.Id, Document_Type__c='Resume');
        insert doc22;
        sleep(1500);
        Web_Document__c doc23 = new Web_Document__c(name='web23', Document_Related_To__c=con2.Id, Document_Type__c='Resume');
        insert doc23;
        sleep(1500);
        Web_Document__c doc24 = new Web_Document__c(name='web24', Document_Related_To__c=con2.Id, Document_Type__c='Resume');
        insert doc24;
        Web_Document__c doc31 = new Web_Document__c(name='web31', Document_Related_To__c=con3.Id, Document_Type__c='Resume');
        insert doc31;
        sleep(1500);
        Web_Document__c doc32 = new Web_Document__c(name='web32', Document_Related_To__c=con3.Id, Document_Type__c='Resume');
        insert doc32;
        Web_Document__c doc33 = new Web_Document__c(name='web33', Document_Related_To__c=con3.Id, Document_Type__c='Resume');
        insert doc33;
        
        List<Id> docList = new List<Id>{doc31.Id, doc12.Id, doc24.Id, doc22.Id};
        system.debug('im here');

        Map<Id, Web_Document__c> candResumeMap = WebDocumentTriggerSelector.getCandResumeMap(conIdList, docList);
        //system.debug('candResumeMap = ' + candResumeMap);
        System.assertEquals(doc11.Id, candResumeMap.get(con1.Id).Id);
        System.assertEquals(doc23.Id, candResumeMap.get(con2.Id).Id);
        System.assertEquals(doc33.Id, candResumeMap.get(con3.Id).Id);
    	}
    }    
    
    
    // Allow thread to sleep for 1s 
    // @param sleepTime = 1000
    public static void sleep(Integer sleepTime) {
        Long startTime = DateTime.now().getTime();
        Long finishTime = DateTime.now().getTime();
        while ((finishTime - startTime) < sleepTime) {
            //sleep for 9s
            finishTime = DateTime.now().getTime();
        }
    }    
}