/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CTCPeopleSettingHelperServicesTest {
	  static testMethod void myUnitTest1() {
	  	System.runAs(DummyRecordCreator.platformUser) {
	  	DataTestFactory.createCTCPeopleSettings();
	  	Sobject record=DataTestFactory.createAccounts().get(0);
	  	map<String,String> mappings = new map<String,String>();
	  	
	  	List<recordtype> rtList =DaoRecordType.getAllRecordTypes() ;//[select id, developername from recordtype];
	  	for(recordtype rt : rtList){
	  	    mappings.put(rt.Id, rt.developername);
	  	}
	  	CTCPeopleSettingHelperServices ctcHepler=new CTCPeopleSettingHelperServices();
	  	String query = ctcHepler.formQueryFromMap( mappings);
	  	Boolean hasA = ctcHepler.hasApAccount();
	  	String s = ctcHepler.getEndpoint();
	  	map<String,String> accBillerMap = ctcHepler.getAccountBillerMap(rtList[0].developername);
	  	ctcHepler.pullCompanyEntities();
	  	ctcHepler.pullRuleGroups();
	  	ctcHepler.pullRateCards();
	  	ctcHepler.pullOccupLibs();
	  	ctcHepler.convertToAPFormat(record, ctcHepler.getAccountBillerMap());
        ctcHepler.getWorkplaceMap();
        ctcHepler.getRateChangeMap();
        ctcHepler.getUserOwnerMap('Owner_Mapping');
	  	ctcHepler.getinvoiceItemMap('Invoice_Line_Item_Mapping');
	  	ctcHepler.getContactBillerContactMap('Biller_Contacts_Mapping');
        ctcHepler.getPlacementEmployeeMap('Placement_Mapping');
        ctcHepler.getContactEmployeeMap('Employee Mapping');
		ctcHepler.getContactApproverMap('Approver_Mapping');
        System.assert(record!=null);
	  	}
	  }
   
}