/*
	@description: Candidate skill object selector
*/

public with sharing class CandidateSkillSelector extends CommonSelector{
	final static String CANDIDATE_SKILL_BASIC_QUERY = 'Select Candidate__c, Skill__c from Candidate_Skill__c';
	
	public CandidateSkillSelector() {
		super(Candidate_Skill__c.sObjectType);
	}

	//Generate skill query string by skillsMap
	private String generateSkillSearchQueryString(Map<String, String> skillsMap) {
		String skillSearchQueryString = '';
		String skillSearchCriteria = '';
		for(String skillId : skillsMap.keySet()) {
		    //if(skillSearchCriteria.length() < 19000) {
			    skillSearchCriteria += skillSearchCriteria!='' ? ' or Skill__c = \''+skillId+'\'' : ' Skill__c = \''+skillId+'\'';
		    //}
		}
		
		//skillsMap cannot be null
		if(skillSearchCriteria!=''){
			skillSearchQueryString = CANDIDATE_SKILL_BASIC_QUERY + ' where (' + skillSearchCriteria + ') ';
		}
		return skillSearchQueryString;
	}

	/*
		@author: Jack ZHOU
		@input: skills map and contacts id set
		@output: candidate skill list
		@description: get candidate skills by candidates
	*/
	public List<Candidate_Skill__c> getCandidateSkillByCandidates(Map<String, String> skillsMap, Set<String> contactsId) {
		List<Candidate_Skill__c> candidateSkillList = new List<Candidate_Skill__c> ();
		String queryString = generateSkillSearchQueryString(skillsMap) + 'and Candidate__c in: contactsId' + ' LIMIT 40000';
		checkRead('Candidate__c, Skill__c');
		candidateSkillList = Database.query(queryString);
		return candidateSkillList;
	}
	
	public List<Candidate_Skill__c> getCandidateSkillByCandidates(Map<String, String> skillsMap, Boolean verifiedSkills, Set<String> contactsId) {
		List<Candidate_Skill__c> candidateSkillList = new List<Candidate_Skill__c> ();
		String queryString;
		Set<String> skillsIdSet = skillsMap.keySet();
		if(verifiedSkills ) {
		    if(contactsId!=null && contactsId.size()>0 ) {
		        queryString = CANDIDATE_SKILL_BASIC_QUERY + ' WHERE Skill__c IN: skillsIdSet AND Verified__c = true' + ' AND Candidate__c in: contactsId' + ' LIMIT 40000';
		    } else {
		        queryString = CANDIDATE_SKILL_BASIC_QUERY + ' WHERE Skill__c IN: skillsIdSet AND Verified__c = true' + ' LIMIT 40000';
		    }
		    
		} else {
		    if(contactsId!=null && contactsId.size()>0 ) {
		        queryString = CANDIDATE_SKILL_BASIC_QUERY + ' WHERE Skill__c IN: skillsIdSet AND Candidate__c in: contactsId' + ' LIMIT 40000';
		    } else {
		        queryString = CANDIDATE_SKILL_BASIC_QUERY + ' WHERE Skill__c IN: skillsIdSet' + ' LIMIT 40000';
		    }
		    
		}
		checkRead('Candidate__c, Skill__c');
		candidateSkillList = Database.query(queryString);
		return candidateSkillList;
	}
}