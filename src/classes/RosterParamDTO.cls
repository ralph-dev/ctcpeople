public with sharing class RosterParamDTO {
	private String id;
	private String status;
	private String rosterType;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRosterType() {
		return rosterType;
	}
	public void setRosterType(String rosterType) {
		this.rosterType = rosterType;
	}
}