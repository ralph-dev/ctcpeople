@isTest(seeAllData = true)
public class CareerOneDetailTest {
    public static testMethod void myUnitTest(){  
    	
    	System.runAs(DummyRecordCreator.platformUser) {
    		PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
	        cs.Enable_Daxtra_Skill_Parsing__c = true; 
	        upsert cs;
	        
	        StyleCategory__c styleCate = new StyleCategory__c();
	       	styleCate.name = 'test1';
	        styleCate.WebSite__c='CareerOne';
	        styleCate.Account_Active__c = true;
	        styleCate.RecordTypeId = DaoRecordType.careerOneAccountRT.Id;
	        styleCate.Advertiser_Uploadcode__c = 'test';
	       
	      
	        //commented by andy for security review II
	        //styleCate.Advertiser_Name__c = '24802279';
	        //styleCate.Advertiser_Password__c = 'test';
	        
	       // styleCate.CareerOne_Premium_Package__c = true;
	        //insert styleCate;
	        
	        //added by andy for security review II
	        StyleCategorySelector selector = new StyleCategorySelector();
	        Boolean s = selector.insertCareerOneProtectedAccount(styleCate,'24802279', 'test');
	        System.assertEquals(true, s);
	       
	        
	        StyleCategory__c styleCate2 = new StyleCategory__c();
	        //styleCate2.WebSite__c='CareerOne';
	        styleCate2.RecordTypeId = DaoRecordType.ApplicationFormStyleRT.Id;
	        styleCate2.name = 'test2';
	        styleCate2.Active__c = true; 
	        
	        selector.doInsert(styleCate2);
	        //insert styleCate2; 
	        StyleCategory__c styleCate3 = new StyleCategory__c();
	        styleCate3.name = 'test3';
	        styleCate3.templateActive__c = true; 
	        styleCate3.templateC1__c = 'test';
	        styleCate3.WebSite__c='CareerOne';
	        styleCate3.RecordTypeId = DaoRecordType.adTemplateRT.Id;// [select Id from recordType where DeveloperName ='templateStyle'].Id;
	        
	        selector.doInsert(styleCate3);
	        //insert styleCate3; 
	        StyleCategory__c styleCate4 = new StyleCategory__c();
	        //styleCate4.WebSite__c='CareerOne';
	        styleCate4.RecordTypeId = DaoRecordType.ApplicationFormStyleRT.Id;// [select Id from recordType where DeveloperName ='ApplicationFormStyle'].Id;
	        styleCate4.name = 'test4';
	        styleCate4.Active__c = true; 
	        
	         selector.doInsert(styleCate4);
	        //insert styleCate4; 
	        
	        Advertisement__c adv = DataTestFactory.createAdvertisement();
	        adv.Skill_Group_Criteria__c = 'IT';
	        adv.Template_Name__c = 'test';
	        adv.Template__c = styleCate.Id;
	        adv.Application_Form_Style__c = styleCate2.Id;
	        adv.Application_Form_Style_Name__c = 'test';
	        adv.Physical_Address__c = 'sydney, NSW, 2000';
	        
	        CommonSelector.quickUpdate(adv);
	       // update adv;
	        Skill_Group__c skillGroup = TestDataFactoryRoster.createSkillGroup(); 
	        skillGroup.Enabled__c = true;
	        
	        CommonSelector.quickUpdate(skillGroup);
	        //update skillGroup;
	        
	      
	        ApexPages.StandardController controller = new ApexPages.StandardController(adv); 
	        CareerOneDetail thecontroller = new CareerOneDetail(controller);
	        system.assertNotEquals(thecontroller.defaultskillgroups.size(),0);
	        thecontroller.init();
	        system.assert(!thecontroller.haserror);
	        thecontroller.getStyles();
	        system.assertNotEquals(thecontroller.templateOptions.size(),0);
	        system.assertNotEquals(thecontroller.styleOptions.size(),0);
	        PageReference p1 = thecontroller.saveAndPost();
	        PageReference p2 = thecontroller.save();
	        pageReference p3 = thecontroller.post();
	        CareerOneDetail.ResultProcess('', adv.Id);
	        List<SelectOption> so =  thecontroller.getSelectSkillGroupList();
	        thecontroller.customerErrorMessage(200);
	      
	        system.assertEquals(null, thecontroller.post());
    	}
    	
        
    }

}