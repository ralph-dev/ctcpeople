@isTest
private class ViewDocExtTest
{
	@isTest
	static void viewDocExtTest(){
		Contact c = new Contact(LastName='Okga');
        insert c;
        Web_Document__c wd = new Web_Document__c(Document_Related_To__c =c.Id, ObjectKey__c ='suzancv.txt',S3_Folder__c ='ctc');
        insert wd;
        test.startTest();
        ApexPages.StandardController con = new ApexPages.StandardController(wd);
        ViewDocExt v = new ViewDocExt(con);
		PageReference tempdownload = v.init();
		system.assert(v.bucket!= null);
		system.assert(v.objkey!= null);
		system.assert(tempdownload == null);
		test.stopTest();
	}
}