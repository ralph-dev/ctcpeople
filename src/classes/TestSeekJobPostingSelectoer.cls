@isTest
public with sharing class TestSeekJobPostingSelectoer {
	static testmethod void testgetJobPostingCustomerSetting() {
		System.runAs(DummyRecordCreator.platformUser) {
		JobPostingSettings__c testCustomSetting = CreateCustomerSettingTestData.createCustomerSettingTestData();
		JobPostingSettings__c resultCustomSetting = SeekJobPostingSelector.getJobPostingCustomerSetting(); 
		system.assert(testCustomSetting.Posting_Seek_Ad_with_User_Email__c = resultCustomSetting.Posting_Seek_Ad_with_User_Email__c);
		String userEmail = '';
		userEmail = SeekJobPostingSelector.getUserPreferEmailAddress(); 
		system.assert(userEmail!= null);
		}
	}
}