/*
 *  Description: This is a controller to save OAuth detail generated back to custom settings
 *  Author: Kevin Wu
 *  Date: 22.04.2015
 */
public with sharing class OAuthCallbackController {
    private String refreshToken;
    private String accessToken;
    private String signature;
    private String returnUrl;
    private String appId;

    // Constructor
    public OAuthCallbackController() {
        this.refreshToken = ApexPages.currentPage().getParameters().get('refresh_token');
        this.accessToken = ApexPages.currentPage().getParameters().get('access_token');
        this.signature = ApexPages.currentPage().getParameters().get('signature');
        this.returnUrl = ApexPages.currentPage().getParameters().get('return_url');
        this.appId = ApexPages.currentPage().getParameters().get('app_id');

        //system.debug('refreshToken='+refreshToken);
        //system.debug('accessToken='+accessToken);
        //system.debug('signature='+signature);
        //system.debug('returnUrl='+returnUrl);
        //system.debug('appId='+appId);
    }

    // redirect users back to return url
    public PageReference redirect(){
        if(!verify()){
            PageReference pageRef = new PageReference('/apex/OAuthCallbackError');
            return pageRef;
        }

        saveOAuthDetail();
        PageReference pageRef = new PageReference(returnUrl);
        return pageRef;
    }

    // save OAuth detail
    public void saveOAuthDetail(){
        SFOauthToken__c sfOauthToken = SFOauthTokenSelector.querySFOauthToken('oAuthToken');
        List<sfOauthToken__c> sfOauthTokenList = new List<sfOauthToken__c>();
        sfOauthTokenList.add(sfOauthToken);
        saveSFOAuth(sfOauthTokenList);
    }

    // save SF web connector OAuth detail back to custom settings
    @TestVisible
    private void saveSFOAuth(List<SFOauthToken__c> sfOauthTokenList){
        List<SFOauthToken__c> sfotListToUpdate = new List<SFOauthToken__c>();
        for(SFOauthToken__c sfot : sfOauthTokenList){
            sfot.OAuthToken__c = accessToken;
            sfot.RefreshToken__c = refreshToken;
            sfotListToUpdate.add(sfot);
        }
        //check FLS
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            SFOauthToken__c.OAuthToken__c,
            SFOauthToken__c.RefreshToken__c 
        };
        fflib_SecurityUtils.checkInsert(SFOauthToken__c.SObjectType, fieldList);
        fflib_SecurityUtils.checkUpdate(SFOauthToken__c.SObjectType, fieldList);
        upsert sfotListToUpdate;
    }

    // verify the parameters 
    private Boolean verify(){
        Boolean verified = true;
        if(refreshToken == null || 
            accessToken == null ||
            returnUrl == null ||
            appId == null){
            verified = false;
        }

        return verified;
    }

}