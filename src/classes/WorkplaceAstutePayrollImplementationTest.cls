/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class WorkplaceAstutePayrollImplementationTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	list<Workplace__c> places=DataTestFactory.createWorkplaces();
    	WorkplaceAstutePayrollImplementation workplaceIMP=new WorkplaceAstutePayrollImplementation();
    
    	map<ID,Workplace__c> placeMaps=new map<ID,Workplace__c>();
    	list<String> workplaceIds=new list<String>();
    	
    	for(Integer i=0;i< places.size();i++){
    		Workplace__c place=places.get(i);
    		placeMaps.put(place.Id, place);
    		workplaceIds.add(place.Id);
    		
    		if(Math.mod(i, 2)==0){
    			place.Is_Pushing_To_AP__c=true;
    		}else{
    			place.Is_Fixing_Data_For_AP__c=true;
    		}
    	}
    	WorkplaceAstutePayrollImplementation.astutePayrollTrigger(places);
    	WorkplaceAstutePayrollImplementation.syncWorkplaces(workplaceIds, AstutePayrollUtils.GetloginSession());
    	System.assertEquals(places.get(0).Astute_Payroll_Upload_Status__c, 'On Hold');
    	}
    }
}