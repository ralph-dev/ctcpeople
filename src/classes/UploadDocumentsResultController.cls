public with sharing class UploadDocumentsResultController{
	public String srcId {get;set;}		// id of the record that used upload document function
	public String srcObj {get;set;}		// source object
	public String ids {get;set;}        // ids of successful documents
	public String errorCode {get;set;}		// error code to retrieve error message
	public String errorId {get;set;}		// an indentifier to check the log
	public String returnLinkValue{get;set;}		// value/wording of the return link
	public boolean hasError {get;set;}		// indicate whether error occurs
	public String message {get;set;}		// message as upload result
	public String message2ndLine {get;set;}		// the 2nd line of the message
	public String resultTitle {get;set;}	// title of the page block 
	public String lookupField;
	
	public UploadDocumentsResultController(){
		srcId = ApexPages.currentPage().getParameters().get('srcId');		
		srcObj = ApexPages.currentPage().getParameters().get('srcObj');
		ids = ApexPages.currentPage().getParameters().get('ids');
		errorCode = ApexPages.currentPage().getParameters().get('errorCode');
		errorId = ApexPages.currentPage().getParameters().get('errorId');
		lookupField=ApexPages.currentPage().getParameters().get('lookupField');
		setDisplayMessage(errorCode, errorId);

		returnLinkValue = getLinkValueBySObjType(srcObj);
		//System.debug('returnLinkValue:' + returnLinkValue);	
			
	}

	//Back to previous page 
	public PageReference returnToPreviousPage(){
	 	system.debug('return srcId = '+ srcId);
        PageReference previousPage = new PageReference('/'+ srcId);
        previousPage.setRedirect(true);
        return previousPage;
    }
    
    public PageReference returnToUploadDocumentsPage(){
    	PageReference uploadDocumentsPage = new PageReference('/apex/UploadDocuments?srcId='+ srcId + '&srcObj=' + srcObj + '&lookupField=' + lookupField);
    	uploadDocumentsPage.setRedirect(true);
    	return uploadDocumentsPage;
    }
    
	private String getLinkValueBySObjType(String sobjName){
		return 'Back To '+sobjName+' Page';
	}
	
	private void setDisplayMessage(String errorCode, String errorId){
		if(errorCode == '00000'){
			resultTitle = 'Upload successfully';
			message =  'All documents have been uploaded successfully.';	
			message2ndLine = 'Please click here to upload more documents.';
			
		}else if(errorCode == '10006'){
			resultTitle = 'Error';
			message = 	'Session expired. Please upload your documents again. (Error Id:' + errorId + ')';
			message2ndLine = 'Please click here to upload documents again.';
			
		}else if(errorCode == '10007'){
			resultTitle = 'Error';
			message = 	'Resume failed to upload as there is an issue with Amozon S3. (Error Id:' + errorId + ')';
			message2ndLine = 'Please click here to upload documents again.';
			
		}else if(errorCode == '10008'){
			resultTitle = 'Error';
			message = 	'Fail to create new web documents in Salesforce database. (Error Id:' + errorId + ')';
			message2ndLine = 'Please click here to upload documents again.';
			
		}else if(errorCode == '10009'){
			resultTitle = 'Error';
			message = 	'Login details in corporate instance is incorrect. (Error Id:' + errorId + ')';
			message2ndLine = 'Please contact support@clicktocloud.com.';
		}else{
			resultTitle = 'Error';
			message = 'Unexpected system error. (Error Id:' + errorId + ')';
			message2ndLine = 'Please contact support@clicktocloud.com.';	
		}	
			

	}


}