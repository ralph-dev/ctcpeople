public with sharing class PlacementCandidateService extends SObjectCRUDService{

	private DescribeSObjectResult currentObject;
	private Map<String, Schema.SObjectField> currentObjectFields;

	public PlacementCandidateService() {
		super(Placement_Candidate__c.sObjectType);
	}

	public Map<String, Placement_Candidate__c> createCMByVancancyAndCandidates(List<candidateDTO> canDTOs, String vacancyId) {
		Set<String> conIdList = new Set<String>();
		Map<String, Placement_Candidate__c> cms = new Map<String, Placement_Candidate__c>();

		for(candidateDTO canDTO : canDTOs) {
			Placement_Candidate__c cm = new Placement_Candidate__c();
			conIdList.add(canDTO.getCandidate().Id);
			cm.Candidate__c = canDTO.getCandidate().Id;
			cm.Placement__c = vacancyId;
			cm.Status__c = 'New';
			cms.put(canDTO.getCandidate().Id, cm);
		}

		Map<String, Placement_Candidate__c> candidateCMMap = new Map<String, Placement_Candidate__c>();

		PlacementCandidateSelector pCandidateSelector = new PlacementCandidateSelector();
		candidateCMMap = pCandidateSelector.getCandidateMangementByVacancyIdandContacts(vacancyId, conIdList);

		cms.keySet().removeAll(candidateCMMap.keySet());

		List<Placement_Candidate__c> cmList = new List<Placement_Candidate__c>();
		cmList = cms.values();

		cms.putALL(candidateCMMap);
		checkFLS();
		insert cmList;
		return cms;
	}

	public override List<SObject> createRecords(List<Map<String, Object>> sobjs) {
		List<Placement_Candidate__c> cms = (List<Placement_Candidate__c>)constructSObjectsForCRUD(sobjs, SObjectCRUDService.OperationType.CREATE);
		try{
			return createRecords(cms);
		}catch(exception e){
			throw e;
		}
	}

	public List<Placement_Candidate__c> createRecords(List<Id> contactIds, List<Id> vacancyIds) {
		List<Placement_Candidate__c> cms = new List<Placement_Candidate__c>();

		for(Id vacancyId : vacancyIds) {
			for(Id contactId: contactIds) {
				Placement_Candidate__c cm = new Placement_Candidate__c();
				cm.Candidate__c = contactId;
				cm.Placement__c = vacancyId;
				cms.add(cm);
			}
		}

		if(!cms.isEmpty()) {
			Savepoint sp = Database.setSavepoint();
			try{
				checkFLS();
				upsert cms;
			}catch(Exception e) {
				Database.rollback(sp);
 				throw e;
			}
		}
		return cms;
	}

	public override List<SObject> updateRecords(List<Map<String, Object>> sobjs) {
		List<Placement_Candidate__c> cms = (List<Placement_Candidate__c>)constructSObjectsForCRUD(sobjs, SObjectCRUDService.OperationType.MODIFY);
		try{
			return updateRecords(cms);
		}catch(exception e){
			throw e;
		}
	}

	public String getAllCMFields(){
		// exclude Id in fieldsList
		// exclude Star_Rating__c in the fieldsList
		Set<String> excludedField = new Set<String>();
		excludedField.add(Placement_Candidate__c.Id.getDescribe().getName());
		excludedField.add(Placement_Candidate__c.Star_Rating__c.getDescribe().getName());
        
        // Change the Column name of Rating__c
		Set<String> changeNameField = new Set<String>{Placement_Candidate__c.Rating__c.getDescribe().getName()};

		currentObject = Schema.getGlobalDescribe().get(Placement_Candidate__c.SObjectType.getDescribe().getName()).getdescribe();
		currentObjectFields = currentObject.fields.getmap();

		List<PeopleSearchDTO.FieldColumn> output = new List<PeopleSearchDTO.FieldColumn>();
		if(currentObjectFields != null){
			for(Schema.SObjectField currentField : currentObjectFields.values()){
				if(!excludedField.contains(currentField.getDescribe().getName())){
					if(changeNameField.contains(currentField.getDescribe().getName())){
						PeopleSearchDTO.FieldColumn temp = new PeopleSearchDTO.FieldColumn(currentField);
						temp.ColumnName = 'Star Rating';
						output.add(temp);
					}else{
						output.add(new PeopleSearchDTO.FieldColumn(currentField));
					}
				}else{
					system.debug(LoggingLevel.FINE, '\n\nExcluded field: ' +currentField.getDescribe().getName()+ '\n\n');
				}
			}
            output.sort();
		}
		return JSON.serialize(output); 
	}
	
	private void checkFLS(){
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			Placement_Candidate__c.Candidate__c,
			Placement_Candidate__c.Placement__c,
			Placement_Candidate__c.Status__c
		};
		fflib_SecurityUtils.checkInsert(Placement_Candidate__c.SObjectType, fieldList);
	}
}