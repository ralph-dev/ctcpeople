@isTest
private class CVEmailParserTest {

   	private static Messaging.InboundEmail email;
    private static CVEmailParser parser; 
  
    private static void createFixture(){
       email = new Messaging.InboundEmail();
       	
		email.plainTextBody = 'test content';
		email.subject = 'andys CV  - hello world';
		email.fromAddress = 'andy@anydomain.com';
		Messaging.InboundEmail.BinaryAttachment attach = new Messaging.InboundEmail.BinaryAttachment();
		attach.fileName = 'andy-latest-CV-resume.doc';
		attach.body = null;
		email.binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[]{attach};
	   
    }
    
    
    static{
    	System.runAs(DummyRecordCreator.platformUser) {
        createFixture( );
        
        parser = new CVEmailParser(email);
        
    	}
    }
    
    static testMethod void testParse(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	CVContent content = parser.parse();
    	System.assertEquals('andy-latest-CV-resume.doc',content.getCvFilename());
    	parser = new CVEmailParser();
    	JXTContent c = parser.parseJxtEmail();
    	}
    }
}