@isTest
private class ShiftSelectorTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        
        //System.debug('shift'+shift);
        
        List<String> shiftIdList = new List<String>();
        shiftIdList.add(shift.Id);
        
        ShiftSelector selector = new ShiftSelector();
        
        //test method fetchShiftListByVacId(vacancyId)
        List<Shift__c> shiftList1 = selector.fetchOpenShiftListByVacId(vac.Id);
        System.assertEquals(shiftList1.size(),0);
        //System.debug('shiftList1'+shiftList1);
        
        //test method fetchShiftListByShiftId(shiftId)
        List<Shift__c> shiftList2 = selector.fetchShiftListByShiftId(shift.Id);
        System.assertNotEquals(shiftList2,null);
        
        //test method fetchOpenShiftListByShiftIdList(shiftIdList)
        List<Shift__c> shiftList3 = selector.fetchOpenShiftListByShiftIdList(shiftIdList);
        System.assertEquals(shiftList3.size(),0);
        
        //test method fetchOpenShiftListByAccountId(shiftIdList)
        List<Shift__c> shiftList4 = selector.fetchOpenShiftListByAccountId(acc.Id);
        System.assertNotEquals(shiftList4,null);
        
        //test method fetchShiftListByShiftIdList(shiftIdList)
        List<Shift__c> shiftList5 = selector.fetchShiftListByShiftIdList(shiftIdList);
        System.assertNotEquals(shiftList5,null);
    	}
    }
}