public with sharing class AstutePayrollAction {
    public String erroMessage='';
    public String CheckActiveUserJson(list<Placement_Candidate__c> previousUploadedCms,StyleCategory__c apAccount){
        //Generate JSON String
        
        String userGetJsonStr='';
        if(apAccount!=null){
        	
        	/**
        	* modified by andy for security review II 
        	*/
        	ProtectedCredential pc = CredentialSettingService.getCredential(apAccount.Id);
        	if(pc != null){
        		userGetJsonStr='\'userget\':{\'api_username\':'+'\''+pc.username+'\','+
            		'\'api_password\':\''+pc.password+'\',\'api_key\':\''+pc.key+'\'}';
        	}
            
        }
        String employeesStr='{\'users\':{';
        String employeeliststr='';
        for(Placement_Candidate__c cm: previousUploadedCms){    
            String employeeJsonStr='\'usersave\':{\'remoteid\':\''+cm.candidate__c+'\',\'job_code\':\''+cm.id+'\'}';
            employeeliststr=employeeliststr+','+employeeJsonStr;
        }
        employeeliststr=employeeliststr.substring(1);
        employeesStr=employeesStr+employeeliststr+','+userGetJsonStr+'}}';
        String jsonUrl=EncodingUtil.urlEncode(employeesStr, 'UTF-8');
        return jsonUrl;       
    }
    public void addCustomMaps(map<String,String> BillerFieldsMap,
        map<String,String> BillerContactFieldsMap,map<String,String> EmployeeFieldsMap
        ,map<String,String> PlacementFieldsMap,map<String,String> approverFieldsMap,List<StyleCategory__c> apAccounts){
            
        StyleCategory__c apAccount = apAccounts[0];
        //If there is no extra maps, we use default maps
        list<RecordType> APRecordTypes=new list<RecordType>();
        set<String> rTDeveloperNames=new set<String>();
        rTDeveloperNames.add('Biller_Contacts_Mapping');
        rTDeveloperNames.add('Biller_Mapping');
        rTDeveloperNames.add('Placement_Mapping');
        rTDeveloperNames.add('Employee_Mapping');
        rTDeveloperNames.add('Approver_Mapping');
        
        /**
        * modified by andy for security review II
        */
        
        APRecordTypes=DaoRecordType.getRecordTypesIn(rTDeveloperNames);//[select id, developername from RecordType where developername in: rTDeveloperNames];
       
        map<String,String> recordTypeMap=new map<String,String>();
        for(RecordType recordTypeunit: APRecordTypes){
            recordTypeMap.put(recordTypeunit.developername,recordTypeunit.id);
        }
        list<CTCPeopleSettingHelper__c> astutePayrollFieldsMapList=new list<CTCPeopleSettingHelper__c>();
        set<String> duplicateCatcherBiller=new set<String>();
        set<String> duplicateCatcherBillerContact=new set<String>();
        set<String> duplicateCatcherEmployee=new set<String>(); 
        set<String> duplicateCatcherApprover=new set<String>(); 
        /**
        * modified by andy for security review II
        */
        astutePayrollFieldsMapList = new CTCPeopleSettingHelperSelector().setParam('accountId','apAccount.id')
        					.get('id, recordtypeid, name, AP_Biller_Contact_Fields__c,AP_Biller_Fields__c,AP_Approver_Fields__c, AP_Employee_Fields__c',
        						'CTCPeopleSetting__c=: accountId');
        //astutePayrollFieldsMapList=[select id, recordtypeid, name, AP_Biller_Contact_Fields__c,
        //    AP_Biller_Fields__c,AP_Approver_Fields__c, AP_Employee_Fields__c from CTCPeopleSettingHelper__c where CTCPeopleSetting__c=: apAccount.id];
        
        for(CTCPeopleSettingHelper__c astutePayrollUniRecord: astutePayrollFieldsMapList){
            if(astutePayrollUniRecord.RecordTypeId==recordTypeMap.get('Biller_Mapping')){
                if(!duplicateCatcherBiller.contains(astutePayrollUniRecord.AP_Biller_Fields__c)){
                    BillerFieldsMap.put(astutePayrollUniRecord.AP_Biller_Fields__c,astutePayrollUniRecord.name.toLowerCase());  
                    duplicateCatcherBiller.add(astutePayrollUniRecord.AP_Biller_Fields__c);
                }else{
                    erroMessage='Duplicate customized field mapping in Astute Payroll Biller.';
                }
                                
            }
            if(astutePayrollUniRecord.RecordTypeId==recordTypeMap.get('Approver_Mapping')){
                if(!duplicateCatcherApprover.contains(astutePayrollUniRecord.AP_Approver_Fields__c)){
                    approverFieldsMap.put(astutePayrollUniRecord.AP_Approver_Fields__c,astutePayrollUniRecord.name.toLowerCase());  
                    duplicateCatcherBiller.add(astutePayrollUniRecord.AP_Approver_Fields__c);
                }else{
                    erroMessage='Duplicate customized field mapping in Astute Payroll Approver.';
                }
                
            }
            if(astutePayrollUniRecord.RecordTypeId==recordTypeMap.get('Biller_Contacts_Mapping')){
                if(!duplicateCatcherBillerContact.contains(astutePayrollUniRecord.AP_Biller_Contact_Fields__c)){
                    BillerContactFieldsMap.put(astutePayrollUniRecord.AP_Biller_Contact_Fields__c, astutePayrollUniRecord.name.toLowerCase());
                    duplicateCatcherBillerContact.add(astutePayrollUniRecord.AP_Biller_Contact_Fields__c);
                    
                }else{
                    erroMessage='Duplicate customized field mapping in Astute Payroll Biller Contact.';
                }
            }
            if(astutePayrollUniRecord.RecordTypeId==recordTypeMap.get('Employee_Mapping')){
                if(!duplicateCatcherEmployee.contains(astutePayrollUniRecord.AP_Employee_Fields__c)){
                    EmployeeFieldsMap.put(astutePayrollUniRecord.AP_Employee_Fields__c,astutePayrollUniRecord.name.toLowerCase() );
                    duplicateCatcherEmployee.add(astutePayrollUniRecord.AP_Employee_Fields__c);
                }else{
                    erroMessage='Duplicate customized field mapping in Astute Payroll Employee.';
                }
            }
            if(astutePayrollUniRecord.RecordTypeId==recordTypeMap.get('Placement_Mapping')){
                if(!duplicateCatcherEmployee.contains(astutePayrollUniRecord.AP_Employee_Fields__c)){
                    PlacementFieldsMap.put(astutePayrollUniRecord.AP_Employee_Fields__c, astutePayrollUniRecord.name.toLowerCase());
                    duplicateCatcherEmployee.add(astutePayrollUniRecord.AP_Employee_Fields__c);
                }else{
                    erroMessage='Duplicate customized field mapping in Astute Payroll Placement.';
                }
            }
        }
    }
    public String WriteJsonStrs(String idStr){
        //Initiate
        Contact cand=new Contact();
        Contact billerContact=new Contact();
        Account biller=new Account();
        Placement_Candidate__c cm=new Placement_Candidate__c();
        Contact approver=new Contact();
        String jsonStr ;
        String jsonUrl='';
        
        //FetchFieldMaps
        map<String,String> BillerFieldsMap=new map<String,String>();
        map<String,String> BillerContactFieldsMap=new map<String,String>();
        map<String,String> EmployeeFieldsMap=new map<String,String>();
        map<String,String> PlacementFieldsMap=new map<String,String>();
        map<String,String> approverFieldsMap=new map<String,String>();
        AstutePayrollUtils.BillerMap(BillerFieldsMap);
        AstutePayrollUtils.BillerContactMap(BillerContactFieldsMap);
        AstutePayrollUtils.EmployeeMap(EmployeeFieldsMap);
        AstutePayrollUtils.PlacementMap(PlacementFieldsMap);
        AstutePayrollUtils.ApproverMap(approverFieldsMap);
        
        //Fetch AstutePayroll Account;
        StyleCategory__c[] apAccount = new List<StyleCategory__c>();
        try{
            apAccount.add(AstutePayrollUtils.GetAstutePayrollAccount());
        }catch(QueryException ex){
            
            erroMessage='Please set up your Astute Payroll account.';
        }
        if(!apAccount.isEmpty()){
            addCustomMaps(BillerFieldsMap,BillerContactFieldsMap,EmployeeFieldsMap
                    ,PlacementFieldsMap,approverFieldsMap,apAccount);
            
            //Generate Query String
            String billerQuery='';          
            String billerContactQuery='';           
            String employeeQuery='';
            String approverQuery='';
            String approverId='';
            //String PlacementQuery='select '+ AstutePayrollUtils.ReturnQuery(PlacementFieldsMap)+',Approver__c ,Placement__r.Company__c ,placement__r.biller_contact__c, candidate__c from Placement_Candidate__c where id =:idStr';
            try{
            	/**
            	* modified by andy for security review II
            	*/
            	//cm = (Placement_Candidate__c)Database.query(PlacementQuery);
            	cm = (Placement_Candidate__c) new CommonSelector(Placement_Candidate__c.sObjectType)
            			.setParam('idStr',idStr)
            			.getOne(
            				AstutePayrollUtils.ReturnQuery(PlacementFieldsMap)+',Approver__c ,Placement__r.Company__c ,placement__r.biller_contact__c, candidate__c',
            				
            				'id =:idStr'
            			);
            }catch(QueryException ex){
                cm=null;
                erroMessage='Please check Astute Payroll field mappings for Placements object.';
            }
            
            //Query
            if(erroMessage==''){
                //billerQuery='select '+AstutePayrollUtils.ReturnQuery(BillerFieldsMap)+' from Account where id = \''+cm.Placement__r.Company__c+'\'';
                //employeeQuery='select '+AstutePayrollUtils.ReturnQuery(EmployeeFieldsMap)+' from Contact where id =\''+ cm.candidate__c+'\'';
                //billerContactQuery='select '+AstutePayrollUtils.ReturnQuery(BillerContactFieldsMap)+' from Contact where id =\''+ cm.placement__r.biller_contact__c+'\'';
                approverId=cm.Approver__c;
                //approverQuery='select '+AstutePayrollUtils.ReturnQuery(approverFieldsMap) +' from Contact where id =: approverId';
                try{
                	/**
	            	* modified by andy for security review II
	            	*/
	            	biller = (Account) new CommonSelector(Account.sObjectType)
	            				.getOne(
	            					AstutePayrollUtils.ReturnQuery(BillerFieldsMap),
	            					'id = \''+cm.Placement__r.Company__c+'\'');
                    //biller=(Account)Database.query(billerQuery);
                }catch(QueryException ex){
                    biller=null;
                    if(cm.Placement__r.Company__c!=null){
                        erroMessage='Please check Astute Payroll field mappings for Account object.';
                    }               
                }
                try{
                	/**
	            	* modified by andy for security review II
	            	*/
                	approver = (Contact) new CommonSelector(Contact.sObjectType)
                				.setParam('approverId',approverId)
	            				.getOne(
	            					AstutePayrollUtils.ReturnQuery(approverFieldsMap),
	            					'id =: approverId');
                   // approver=(Contact)Database.query(approverQuery);
                    
                }catch(QueryException ex){
                    approver=null;
                    if(cm.Approver__c!=null){
                        erroMessage='Please check Astute Payroll field mappings for Contact Approver.';
                    }               
                }
                try{
                	billerContact = (Contact) new CommonSelector(Contact.sObjectType)
	            				.getOne(
	            					AstutePayrollUtils.ReturnQuery(BillerContactFieldsMap),
	            					'id =\''+ cm.placement__r.biller_contact__c+'\'');
                    //billerContact=(Contact)Database.query(billerContactQuery);
                    
                }catch(QueryException ex){
                    billerContact=null;
                    if(cm.placement__r.biller_contact__c!=null){
                        erroMessage='Please check Astute Payroll field mappings for Contact object.';
                        
                    }
                }
                
                try{
                	cand = (Contact) new CommonSelector(Contact.sObjectType)
	            				.getOne(
	            					AstutePayrollUtils.ReturnQuery(EmployeeFieldsMap),
	            					'id =\''+ cm.candidate__c+'\'');
                	
                //cand=(Contact)Database.query(employeeQuery);
                }catch(QueryException ex){
                    cand=null;
                    erroMessage='Please check Astute Payroll field mappings for Candidate object.';
                }
            }
            //Generate JSON String
            if(erroMessage==''){
                String userGetJsonStr='';
                if(!apAccount.isEmpty() && CredentialSettingService.getCredential(apAccount[0].Id) != null){
                	/**
                	* modified by andy for security review II
                	*/
                	ProtectedCredential pc = CredentialSettingService.getCredential(apAccount[0].Id);
                	
                    userGetJsonStr='\'userget\':{\'api_username\':'+'\''+pc.username+'\','+
                    '\'api_password\':\''+pc.password+'\',\'api_key\':\''+pc.key+'\'}';
                }           
                String billerJsonStr='';
                if(biller!=null){
                    billerJsonStr='\'billersave\':{'+AstutePayrollUtils.WriteRequest(BillerFieldsMap, biller)+'},';
                }
                String billerContactJsonStr='';
                if(billerContact!=null){
                    billerContactJsonStr='\'billercontactssave\':{'+AstutePayrollUtils.WriteRequest(BillerContactFieldsMap, billerContact)+'},';
                }
                String approverContactJsonStr='';
                if(approver!=null){
                    approverContactJsonStr='{'+AstutePayrollUtils.WriteRequest(approverFieldsMap,approver)+', \'USERTYPE\':\'Approver\'}';
                }
                String employeeJsonStr='{'+AstutePayrollUtils.WriteRequest(EmployeeFieldsMap, cand)+', \'USERTYPE\':\'employee\','+AstutePayrollUtils.WriteRequest(PlacementFieldsMap, cm)+'}';
                String usersaveJsonStr='';
                if(approverContactJsonStr!=''){
                    usersaveJsonStr='\'usersave\':['+approverContactJsonStr+','+employeeJsonStr+']';
                }else{
                    usersaveJsonStr='\'usersave\':'+employeeJsonStr;
                }
                jsonStr='{\'users\':{'+billerJsonStr+billerContactJsonStr+usersaveJsonStr+','+userGetJsonStr+'}}';       
                jsonUrl=EncodingUtil.urlEncode(jsonStr, 'UTF-8');
            }
        }
        return jsonUrl;
    }
    public HttpResponse AstutePayrollCheckActiveUsers(String jsonstr){
         Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setBody(jsonstr);
        //commented by Ralph
        //req.setHeader('session', AstutePayrollUtils.GetloginSession());
        //added by Ralph 11-08-2017
        req.setHeader('orgId', UserInfo.getOrganizationId());        
        req.setHeader('userId', UserInfo.getUserId());
        String endPointStr=Endpoint.getAstuteEndpoint();
        req.setEndpoint(endPointStr+'/AstutePayroll/draftcatcher');
        String urlStr=URL.getSalesforceBaseUrl().getHost()+'/services/Soap/u/29.0/';
        req.setHeader('surl', urlStr);
        req.setTimeout(120000); 
        HttpResponse res=new HttpResponse();
        if(!Test.isRunningTest()){
            res=h.send(req);    
        }else{
            res=APMockApiCalls.webServiceExpectedResponse();
        }   
        return res;
    }
    public HttpResponse AstutePayrollUpload(String idStr){
        //Send Request
        String jsonUrl=WriteJsonStrs(idStr);
        //System.debug('The whole json string is '+jsonUrl);
        HttpResponse res=new HttpResponse();
        if(erroMessage==''){
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setBody(jsonUrl);
            //commented by Ralph
            //req.setHeader('session', AstutePayrollUtils.GetloginSession());
            //added by Ralph 11-08-2017
       		req.setHeader('orgId', UserInfo.getOrganizationId());        
       		req.setHeader('userId', UserInfo.getUserId());
            String endPointStr=Endpoint.getAstuteEndpoint();
            req.setEndpoint(endPointStr+'/AstutePayroll/allrecords');
            String urlStr=URL.getSalesforceBaseUrl().getHost()+'/services/Soap/u/21.0/';
            req.setHeader('surl', urlStr);
            req.setTimeout(120000); 
            if(!Test.isRunningTest()){
                res=h.send(req);    
            }else{
                res=APMockApiCalls.webServiceExpectedResponse();
            }
        }else{
            res.setStatusCode(500);
            res.setStatus('No Astute Payroll Account Error');
            res.setBody(erroMessage);
        }
        
        return res;
    }
}