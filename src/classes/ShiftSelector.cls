public with sharing class ShiftSelector extends CommonSelector{
	
	public ShiftSelector(){
		super(Shift__c.SObjectType);
	}

	final static String SHIFT_QUERY_BASE1 = 'SELECT Id, Name, Account__c, Account__r.Name, Start_Date__c,Start_Time__c,'
											+'End_Time__c, End_Date__c, Location__c, Shift_Type__c,Vacancy_Name_For_Display_On_Wizard__c,'
											+'Vacancy__c, Vacancy__r.Name, No_of_Position_Requried__c,No_of_Position_Placed__c,'
											+'No_of_Position_Open__c, Weekly_Recurrence__c,Recurrence_End_Date__c,Recurrence_Weekdays__c,'
											+'Shift_Status__c,Rate__c From Shift__c';
											
	final static String SHIFT_STATUS_OPEN = 'Open';
	
	/**
	*	fetch open shift list by vacancy id
	*   start date should be after today OR start date before today, end date after today
	*	OR start date before today, recurrence end date after today
	*
	**/										
	public List<Shift__c> fetchOpenShiftListByVacId(String vacId){
		String query = 	SHIFT_QUERY_BASE1 + ' WHERE Vacancy__c =: vacId '
						+'AND Shift_Status__c =: SHIFT_STATUS_OPEN AND (Start_Date__c >= Today '
						+'OR End_Date__c >=Today OR Recurrence_End_Date__c >=Today)';
		checkRead('Id, Name, Account__c, Account__r.Name, Start_Date__c,Start_Time__c,'
				+'End_Time__c, End_Date__c, Location__c, Shift_Type__c,Vacancy_Name_For_Display_On_Wizard__c,'
				+'Vacancy__c, Vacancy__r.Name, No_of_Position_Requried__c,No_of_Position_Placed__c,'
				+'No_of_Position_Open__c, Weekly_Recurrence__c,Recurrence_End_Date__c,Recurrence_Weekdays__c,'
				+'Shift_Status__c,Rate__c');
		List<Shift__c> shiftList = Database.query(query);
		return shiftList;
	}
	
	/**
	*	fetch open shift list by list of shift id
	*	start date should be after today OR start date before today, end date after today
	*	OR start date before today, recurrence end date after today
	**/	
	public List<Shift__c> fetchOpenShiftListByShiftIdList(List<String> shiftIdList){
		String query = SHIFT_QUERY_BASE1 + ' WHERE Id in: shiftIdList '
						+'AND Shift_Status__c=:SHIFT_STATUS_OPEN AND (Start_Date__c >= Today '
						+'OR End_Date__c >=Today OR Recurrence_End_Date__c >=Today)';
		checkRead('Id, Name, Account__c, Account__r.Name, Start_Date__c,Start_Time__c,'
				+'End_Time__c, End_Date__c, Location__c, Shift_Type__c,Vacancy_Name_For_Display_On_Wizard__c,'
				+'Vacancy__c, Vacancy__r.Name, No_of_Position_Requried__c,No_of_Position_Placed__c,'
				+'No_of_Position_Open__c, Weekly_Recurrence__c,Recurrence_End_Date__c,Recurrence_Weekdays__c,'
				+'Shift_Status__c,Rate__c');
		List<Shift__c> shiftList =  Database.query(query);
		return shiftList;
	}
	
	/**
	*	fetch shift list by shift id
	*
	**/	
	public List<Shift__c> fetchShiftListByShiftId(String shiftId){
		String query = SHIFT_QUERY_BASE1 + ' WHERE Id =: shiftId';
		checkRead('Id, Name, Account__c, Account__r.Name, Start_Date__c,Start_Time__c,'
				+'End_Time__c, End_Date__c, Location__c, Shift_Type__c,Vacancy_Name_For_Display_On_Wizard__c,'
				+'Vacancy__c, Vacancy__r.Name, No_of_Position_Requried__c,No_of_Position_Placed__c,'
				+'No_of_Position_Open__c, Weekly_Recurrence__c,Recurrence_End_Date__c,Recurrence_Weekdays__c,'
				+'Shift_Status__c,Rate__c');
		List<Shift__c> shiftList =  Database.query(query);
		return shiftList;
	}
	
	/**
	*	fetch shift list by shift id list
	*
	**/	
	public List<Shift__c> fetchShiftListByShiftIdList(List<String> shiftIdList){
		String query = SHIFT_QUERY_BASE1 + ' WHERE Id in: shiftIdList';
		checkRead('Id, Name, Account__c, Account__r.Name, Start_Date__c,Start_Time__c,'
				+'End_Time__c, End_Date__c, Location__c, Shift_Type__c,Vacancy_Name_For_Display_On_Wizard__c,'
				+'Vacancy__c, Vacancy__r.Name, No_of_Position_Requried__c,No_of_Position_Placed__c,'
				+'No_of_Position_Open__c, Weekly_Recurrence__c,Recurrence_End_Date__c,Recurrence_Weekdays__c,'
				+'Shift_Status__c,Rate__c');
		List<Shift__c> shiftList =  Database.query(query);
		return shiftList;
	}
	
	/**
	*	fetch open shift list by account id
	*
	**/	
	public List<Shift__c> fetchOpenShiftListByAccountId(String accId){
		String query = SHIFT_QUERY_BASE1 + ' WHERE Account__c =: accId AND Shift_Status__c =: SHIFT_STATUS_OPEN';
		checkRead('Id, Name, Account__c, Account__r.Name, Start_Date__c,Start_Time__c,'
				+'End_Time__c, End_Date__c, Location__c, Shift_Type__c,Vacancy_Name_For_Display_On_Wizard__c,'
				+'Vacancy__c, Vacancy__r.Name, No_of_Position_Requried__c,No_of_Position_Placed__c,'
				+'No_of_Position_Open__c, Weekly_Recurrence__c,Recurrence_End_Date__c,Recurrence_Weekdays__c,'
				+'Shift_Status__c,Rate__c');
		List<Shift__c> shiftList = Database.query(query);
		return shiftList;
	}
}