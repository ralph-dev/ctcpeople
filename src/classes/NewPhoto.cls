public with sharing class NewPhoto {
    public String iid{get; set;}
    public String bucketname{get; set;}
    public Photo__c newPhoto{get; set;}
    
    //added by Andy Yang
    public String serviceEndPoint {get;set;}    // service end point
    
    public NewPhoto(ApexPages.StandardController stdController){
        iid = UserInfo.getOrganizationId().substring(0,15);
        CommonSelector.checkRead(User.sObjectType,'AmazonS3_Folder__c');
        User u = [select AmazonS3_Folder__c from User where Id=:UserInfo.getUserId() limit 1];
        bucketname = u.AmazonS3_Folder__c;
        
        serviceEndPoint = getServiceEndpoint();
    }
    
     private String getServiceEndpoint(){
        return Endpoint.getcpewsEndpoint();
    }
}