@isTest
private class RosterExtensionGlobalTest {
	final static String ROSTER_STATUS_PENDING = 'Pending';
	final static String ROSTER_TYPE_CANDIDATE = 'Candidate';
	final static String ROSTER_TYPE_CLIENT = 'Client';
	
	@isTest
    static void retrieveRosterListByIdTest() {
        // TO DO: implement unit test
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
    	List<Days_Unavailable__c> rosters = 
    		TestDataFactoryRoster.createRostersForSplitRosterFunction(con, shift, acc,ROSTER_STATUS_PENDING);
    	System.debug('roster id: '+rosters[0].id);
    	 	
        RosterParamDTO  rParamDTO = TestDataFactoryRoster.createRosterParamDTO(acc,ROSTER_STATUS_PENDING,ROSTER_TYPE_CLIENT);
        String rParamDTOJson = JSON.serialize(rParamDTO);
       
       	List<Days_Unavailable__c> rostersRetrievedById = RosterExtensionGlobal.retrieveRosterListById(rParamDTOJson);
       	System.assertNotEquals(rostersRetrievedById.size(),0); 
    }
    
    @isTest
    static void retrieveRosterDBListByIdTest(){
    	Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
    	List<Days_Unavailable__c> rosters = 
    		TestDataFactoryRoster.createRostersForSplitRosterFunction(con, shift, acc,ROSTER_STATUS_PENDING);
    	RosterParamDTO  rParamDTO = TestDataFactoryRoster.createRosterParamDTO(acc,ROSTER_STATUS_PENDING,ROSTER_TYPE_CLIENT);
        String rParamDTOJson = JSON.serialize(rParamDTO);
        
    	List<Days_Unavailable__c> rosterDBRetrievedById = RosterExtensionGlobal.retrieveRosterDBListById(rParamDTOJson);
       	System.assertNotEquals(rosterDBRetrievedById.size(),0); 
    }
   	
   	@isTest
    static void retrieveRosterListByIdAndStatusTest(){
    	Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
    	List<Days_Unavailable__c> rosters = 
    		TestDataFactoryRoster.createRostersForSplitRosterFunction(con, shift, acc,ROSTER_STATUS_PENDING);
    	RosterParamDTO  rParamDTO = TestDataFactoryRoster.createRosterParamDTO(acc,ROSTER_STATUS_PENDING,ROSTER_TYPE_CLIENT);
        String rParamDTOJson = JSON.serialize(rParamDTO);
        
    	List<Days_Unavailable__c> rostersRetrievedById = RosterExtensionGlobal.retrieveRosterListByIdAndStatus(rParamDTOJson);
       	System.assertNotEquals(rostersRetrievedById.size(),0); 
    }
    
    @isTest
    static void updateRosterDBTest(){
    	Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        Days_Unavailable__c roster = 
        	TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_PENDING);
        RosterDTO rDTO = 
        	TestDataFactoryRoster.createRosterDTO(acc, con, shift, roster,ROSTER_STATUS_PENDING);
        String rDTOJson = JSON.serialize(rDTO);
        
   		List<Days_Unavailable__c> rostersUpdated = RosterExtensionGlobal.updateRosterDB(rDTOJson);
   		System.assertNotEquals(rostersUpdated.size(),0);
        
    }
    
    @isTest
    static void retrieveRosterListByRosterIdTest(){
    	Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
    	List<Days_Unavailable__c> rosters = 
    		TestDataFactoryRoster.createRostersForSplitRosterFunction(con, shift, acc,ROSTER_STATUS_PENDING);
        List<Days_Unavailable__c> rosterList = RosterExtensionGlobal.retrieveRosterListByRosterId(rosters[0].Id);
        System.assertNotEquals(rosterList.size(),0);
    }
    
    @isTest
    static void retrieveUnavailDatesForCandidateTest(){
    	List<String> unavailDates = RosterExtensionGlobal.retrieveUnavailDatesForCandidate('abc');
    	System.assertEquals(unavailDates.size(),0);
    }
    
    @isTest
    static void retrieveOpenVacanciesMapForClientTest(){
    	Account acc = TestDataFactoryRoster.createAccount();
       	Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
       	List<Shift__c> shiftList = TestDataFactoryRoster.createShiftListForSplitShiftFunction(acc,vac);
       	//System.debug(shiftList);
       	      	
       	Map<String,List<ShiftDTO>> openVacancies = RosterExtensionGlobal.retrieveOpenVacanciesMapForClient(acc.Id);
       	System.assertNotEquals(openVacancies,null);
    }
    
    @isTest
    static void retrieveOpenVacanciesForClientTest(){
    	Account acc = TestDataFactoryRoster.createAccount();
       	Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
       	List<Shift__c> shiftList = TestDataFactoryRoster.createShiftListForSplitShiftFunction(acc,vac);
       	//System.debug(shiftList);
       	      	
       	List<ShiftDTO> openVacancies = RosterExtensionGlobal.retrieveOpenVacanciesForClient(acc.Id);
       	System.assertNotEquals(openVacancies.size(),0);
    }
    
    @isTest
    static void retrieveShiftDBByShiftIdTest(){
    	Account acc = TestDataFactoryRoster.createAccount();
       	Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
       	List<Shift__c> shifts = TestDataFactoryRoster.createShiftListForSplitShiftFunction(acc,vac);

       	ShiftDTO sDTO = RosterExtensionGlobal.retrieveShiftDBByShiftId(shifts[0].Id);
       	
       	System.assertNotEquals(sDTO,null);
    }
    
    @isTest
    static void retrieveVacancyNameIdMapFromShiftsTest(){
    	Account acc = TestDataFactoryRoster.createAccount();
       	Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
       	List<Shift__c> shifts = TestDataFactoryRoster.createShiftListForSplitShiftFunction(acc,vac);

       	Map<String,ShiftDTO> vacancyIdNameMap = RosterExtensionGlobal.retrieveVacancyNameIdMapFromShifts(acc.Id);
       	
       	System.assertNotEquals(vacancyIdNameMap,null);
    }
    
    @isTest
    static void deleteRosterByIdTest(){
    	Boolean isDeleted = RosterExtensionGlobal.deleteRosterById('abc');
    	System.assertNotEquals(isDeleted,true);
    }
    
    @isTest
    static void describeRosterTest(){
    	 Map<String,List<Schema.PicklistEntry>> resultMap = RosterExtensionGlobal.describeRoster();
    	 System.assertNotEquals(resultMap,null);
    }
    
    static testMethod void testConstruct() {
    	System.runAs(DummyRecordCreator.platformUser) {
        RosterExtensionGlobal rg = new RosterExtensionGlobal('test');
        system.assertNotEquals(null, rg);
    	}
    }
    
    static testMethod void testRetrieveOpenVacanciesStoredInDB() {
    	System.runAs(DummyRecordCreator.platformUser) {
        system.assertEquals(null, RosterExtensionGlobal.retrieveOpenVacanciesStoredInDB('12345'));
    	}
    }
    
    static testMethod void testEventStatus() {
    	System.runAs(DummyRecordCreator.platformUser) {
        system.assertNotEquals(null, RosterExtensionGlobal.EventStatus());
    	}
    }
}