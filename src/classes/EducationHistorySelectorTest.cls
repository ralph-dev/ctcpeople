@isTest
public class EducationHistorySelectorTest {

	static testmethod void testGetContactEducationHistory(){
		TriggerHelper.disableAllTrigger();
	 	EducationDummyRecordCreator edCreator = new EducationDummyRecordCreator();
	    List<Id> conIds = edCreator.generateDummyRecord();
	    
		system.runAs(DummyRecordCreator.platformUser) {
			EducationHistorySelector edSel = new EducationHistorySelector();
			List<Education_History__c> edList = edSel.getContactEducationHistory(conIds);
			system.assertNotEquals(null, edList);
		}
	}
}