@isTest
public with sharing class TestLinkedInCloneAd {
	public testmethod static void testLinkedInCloneAd(){
		System.runAs(DummyRecordCreator.platformUser) {
		Advertisement__c testad = dummyAd();
		ApexPages.StandardController stdController =new ApexPages.StandardController(testad);
		LinkedInCloneAd testLinkedInCloneAd = new LinkedInCloneAd(stdController);
		PageReference testpagereference = testLinkedInCloneAd.cloneAd();
		system.assert(testpagereference.getUrl().contains('linkedinpost'));
		PageReference testcancelpagereference = testLinkedInCloneAd.cancelClone();
		system.assert(testcancelpagereference.getUrl()!=null);
		testLinkedInCloneAd.customerErrorMessage(PeopleCloudErrorInfo.CLONE_AD_ERROR);
		system.assert(apexPages.getMessages().size()>0);
		}
	}
	
	public testmethod static void testLinkedIngenerateXML(){
		System.runAs(DummyRecordCreator.platformUser) {
		Advertisement__c testad = dummyAd();
		boolean isedit = false;
		String result = LinkedInGenerateXMLFeed.generateXMLFeed(testad.id, isEdit);
		system.assert(result != null);
		result = LinkedInGenerateXMLFeed.generateRenewXMLFeed(testad);
		system.assert(result!= null);
		}
	}
	
	//Init the dummy LinkedIn ad
	public static Advertisement__c dummyAd(){
		
		StyleCategory__c tempLinkedInAccount  = dummyLinkedInAccount();
		List<Advertisement__c> listdummyads = new List<Advertisement__c>();
		Advertisement__c dummyad = new Advertisement__c();
		String linkedInrecordtypeid = (DaoRecordType.linkedInAdRT).id;
		dummyad.LinkedIn_Account__c = tempLinkedInAccount.id;
		dummyad.recordtypeid = linkedInrecordtypeid;
		dummyad.Job_Title__c = 'test';
		dummyad.Status__c = 'Active';
		dummyad.Placement_Date__c = System.today().addDays(-29);
		dummyad.Job_Function__c = 'Administrative';
		dummyad.Industry__c = 'Accounting';
		dummyad.Job_Type_Text__c = 'Full-time';
		dummyad.Experience_Level__c = 'Executive';
		dummyad.Poster_Role__c = 'Hiring Manager';
		insert dummyad;
		return dummyad;
	}
	
	public static StyleCategory__c dummyLinkedInAccount(){
		StyleCategory__c dummyLinkedInAccount  = new StyleCategory__c();
		dummyLinkedInAccount.recordtypeid = (DaoRecordType.linkedInaccountRT).id;
		dummyLinkedInAccount.name = 'linkedInDummyAccount';
		dummyLinkedInAccount.Advertiser_Id__c = '1234&';
		dummyLinkedInAccount.LinkedIn_Partner_ID__c = '46578-';
		dummyLinkedInAccount.Account_Active__c = true;
		insert dummyLinkedInAccount;
		return dummyLinkedInAccount;
	}
}