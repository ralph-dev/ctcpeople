@isTest
private class DaoEmailTemplateTest {

	private static testMethod void test() {
		System.runAs(DummyRecordCreator.platformUser) {
        system.assertNotEquals(null, DaoEmailTemplate.getAllSupportedTemplates());
        system.assertEquals(null, DaoEmailTemplate.getTemplateById('a049000000u5iOJ'));
        system.assertNotEquals(null, DaoEmailTemplate.getTemplateLikeDevName('test'));
        system.assertNotEquals(null, DaoEmailTemplate.getAllActiveTemplatesById('a049000000u5iOJ'));
        system.assertNotEquals(null, DaoEmailTemplate.getAllActiveTemplatesWithLimitedFieldsById('a049000000u5iOJ'));
        system.assertNotEquals(null, DaoEmailTemplate.getInitTemplatesByName('test'));
        system.assertNotEquals(null, DaoEmailTemplate.getOldTemplateLikeDevName('test'));
		}
	}

}