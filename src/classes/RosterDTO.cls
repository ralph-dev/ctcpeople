public with sharing class RosterDTO {
	private String 	startDate;
	private String 	startTime;
	private String 	endDate;
	private String 	endTime;
	private String 	location;
	private String 	candidateId;
	private String 	clientId;
	private String 	cmId;
	private String 	shiftId;
	private String 	shiftType;
	private String 	status;
	private Boolean weeklyRecurrence;
	private String  recurEndDate;
	private String  weekdaysDisplay;
	private String  id;
	private String  Rate;
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCandidateId() {
		return candidateId;
	}
	public void setCandidateId(String candidateId) {
		this.candidateId = candidateId;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getCmId() {
		return cmId;
	}
	public void setCmId(String cmId) {
		this.cmId = cmId;
	}
	public String getShiftId() {
		return shiftId;
	}
	public void setShiftId(String shiftId) {
		this.shiftId = shiftId;
	}
	public String getShiftType() {
		return shiftType;
	}
	public void setShiftType(String shiftType) {
		this.shiftType = shiftType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getWeeklyRecurrence() {
		return weeklyRecurrence;
	}
	public void setWeeklyRecurrence(Boolean weeklyRecurrence) {
		this.weeklyRecurrence = weeklyRecurrence;
	}
	public String getRecurEndDate() {
		return recurEndDate;
	}
	public void setRecurEndDate(String recurEndDate) {
		this.recurEndDate = recurEndDate;
	}
	public String getWeekdaysDisplay() {
		return weekdaysDisplay;
	}
	public void setWeekdaysDisplay(String weekdaysDisplay) {
		this.weekdaysDisplay = weekdaysDisplay;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRate() {
		return Rate;
	}
	public void setRate(String rate) {
		Rate = rate;
	}
}