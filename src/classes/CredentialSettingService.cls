public class CredentialSettingService {
    
   /**
    * insert, update, upsert indeed credentials
    */
    
    //insert
   public static ProtectedCredential insertIndeedCredential(String name,String apiToken){

        return insertIndeedCredential(name, apiToken,ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG);
   }
   
   public static ProtectedCredential insertIndeedCredential(String name,String apiToken,String objectName){
        if(name == null || apiToken == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type = ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_INDEED;
    
        c.name = name;
        c.token = apiToken ;
        c.relatedTo = objectName;
        
        return insertCredential(c);
   }
   
   //update
   public static ProtectedCredential updateIndeedCredential(String name,String apiToken){
        return updateIndeedCredential(name, apiToken,ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG);
   }
   
   public static ProtectedCredential updateIndeedCredential(String name,String apiToken,String objectName){
        if(name == null || apiToken == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type = ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_INDEED;
    
        c.name = name;
        c.token = apiToken ;
        c.relatedTo = objectName;
        
        return updateCredential(c);
   }
   
   
   //upsert
   public static ProtectedCredential upsertIndeedCredential(String name,String apiToken){
        return upsertIndeedCredential(name, apiToken,ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG);
   }
   
   public static ProtectedCredential upsertIndeedCredential(String name,String apiToken,String objectName){
        if(name == null || apiToken == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type = ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_INDEED;
    
        c.name = name;
        c.token = apiToken ;
        c.relatedTo = objectName;
        
        return upsertCredential(c);
   }
   
   /**
    * insert, update, upsert JXT and JXT NZ credentials
    */
   public static ProtectedCredential insertJXTCredential(String name,String username,String password){
        return insertJXTCredential( name, username, password, ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG);
   }
   
   public static ProtectedCredential insertJXTNZCredential(String name,String username,String password){
        return insertJXTCredential( name, username, password, ProtectedCredential.RELATED_TO_STYLECATEGORY);
   }
   
   public static ProtectedCredential insertJXTCredential(String name,String username,String password,String objectName){
        if(name == null || username == null || password == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_JXT;
    
        c.name = name;
        c.username = username ;
        c.password = password;
        c.relatedTo = objectName;
        
        return insertCredential(c);
   }
   
   public static ProtectedCredential updateJXTCredential(String name,String username,String password){
        return updateJXTCredential( name, username, password, ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG);
   }
   
   public static ProtectedCredential updateJXTNZCredential(String name,String username,String password){
        return updateJXTCredential( name, username, password, ProtectedCredential.RELATED_TO_STYLECATEGORY);
   }
   
   public static ProtectedCredential updateJXTCredential(String name,String username,String password,String objectName){
        if(name == null || username == null || password == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_JXT;
    
        c.name = name;
        c.username = username ;
        c.password = password;
        c.relatedTo = objectName;
        
        
        return updateCredential(c);
   }
   
   public static ProtectedCredential upsertJXTCredential(String name,String username,String password){
        return upsertJXTCredential( name, username, password, ProtectedCredential.RELATED_TO_ADVERTISEMENTCONFIG);
   }
   
    public static ProtectedCredential upsertJXTNZCredential(String name,String username,String password){
        return upsertJXTCredential( name, username, password, ProtectedCredential.RELATED_TO_STYLECATEGORY);
   }
   
   public static ProtectedCredential upsertJXTCredential(String name,String username,String password,String objectName){
        if(name == null || username == null || password == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_JXT;
    
        c.name = name;
        c.username = username ;
        c.password = password;
        c.relatedTo = objectName;
        
        return upsertCredential(c);
   }
   
   /**
    * insert, update, upsert CareerOne credentials
    */
   public static ProtectedCredential insertCareerOneCredential(String name,String username, String password){
        return insertCareerOneCredential(name,username, password,ProtectedCredential.RELATED_TO_STYLECATEGORY);
   }
   
   public static ProtectedCredential insertCareerOneCredential(String name,String username, String password,String objectName){
        if(name == null ||  password == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_CAREERONE;
    
        c.name = name;
        c.username = username;
        c.password = password;
        c.relatedTo = objectName;
        
        return insertCredential(c);
   }
   
   public static ProtectedCredential updateCareerOneCredential(String name,String username, String password){
        return updateCareerOneCredential( name,username, password,ProtectedCredential.RELATED_TO_STYLECATEGORY);
   }
   public static ProtectedCredential updateCareerOneCredential(String name,String username,String password,String objectName){
        if(name == null ||  password == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_CAREERONE;
    
        c.name = name;
        c.username = username;
        c.password = password;
        c.relatedTo = objectName;
        
        return updateCredential(c);
   }
   
   
   public static ProtectedCredential upsertCareerOneCredential(String name,String username, String password){
        return upsertCareerOneCredential(name, username, password,ProtectedCredential.RELATED_TO_STYLECATEGORY);
   }
   
   public static ProtectedCredential upsertCareerOneCredential(String name,String username, String password,String objectName){
        if(name == null || password == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_CAREERONE;
    
        c.name = name;
        c.username = username;
        c.password = password;
        c.relatedTo = objectName;
        
        return upsertCredential(c);
   }
   
   /**
    * insert, update, upsert AstutePayroll credentials
    */
    //insert
   public static ProtectedCredential insertAstutePayrollCredential(String name,String username,String password,String key){
        return insertAstutePayrollCredential( name, username, password, key,ProtectedCredential.RELATED_TO_STYLECATEGORY);
   }
   
   public static ProtectedCredential insertAstutePayrollCredential(String name,String username,String password,String key,String objectName){
        if(name == null ||  password == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_PAYROLL;
        c.provider = ProtectedCredential.PROVIDER_ASTUTE;
    
        c.name = name;
        c.username = username;
        c.password = password;
        c.key = key;
        c.relatedTo = objectName;
        
        return insertCredential(c);
   }
   
   //update
   public static ProtectedCredential updateAstutePayrollCredential(String name,String username,String password,String key){
        return updateAstutePayrollCredential( name, username, password, key,ProtectedCredential.RELATED_TO_STYLECATEGORY);
   }
   
   public static ProtectedCredential updateAstutePayrollCredential(String name,String username,String password,String key,String objectName){
        if(name == null ||  password == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_PAYROLL;
        c.provider = ProtectedCredential.PROVIDER_ASTUTE;
    
        c.name = name;
        c.username = username;
        c.password = password;
        c.key = key;
        c.relatedTo = objectName;
        
        return updateCredential(c);
   }
   
   //upsert
   public static ProtectedCredential upsertAstutePayrollCredential(String name,String username,String password,String key){
        return upsertAstutePayrollCredential( name, username, password, key,ProtectedCredential.RELATED_TO_STYLECATEGORY);
   }
   
   public static ProtectedCredential upsertAstutePayrollCredential(String name,String username,String password,String key,String objectName){
        if(name == null || password == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_PAYROLL;
        c.provider = ProtectedCredential.PROVIDER_ASTUTE;
    
        c.name = name;
        c.username = username;
        c.password = password;
        c.key = key;
        c.relatedTo = objectName;
        
        
        return upsertCredential(c);
   }
   
   /**
    * insert, update, upsert LinkedIn OAuth Service
    */
    //insert
   public static ProtectedCredential insertLinkedInOAuthService(String name,String consumerKey,String comsumerSecret){
        return insertLinkedInOAuthService(name,consumerKey,comsumerSecret,ProtectedCredential.RELATED_TO_OAUTHSERVICE);
   }
   
   public static ProtectedCredential insertLinkedInOAuthService(String name,String consumerKey,String comsumerSecret,String objectName){
        if(name == null ||  consumerKey == null || comsumerSecret == null ){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_LINKEDIN;
    
        c.name = name;
        c.key = consumerKey ;
        c.secret = comsumerSecret;
        c.relatedTo = objectName;
        
        return insertCredential(c);
   }
   
   //update
   public static ProtectedCredential updateLinkedInOAuthService(String name,String consumerKey,String comsumerSecret){
        return updateLinkedInOAuthService(name,consumerKey,comsumerSecret,ProtectedCredential.RELATED_TO_OAUTHSERVICE);
   }
   
   public static ProtectedCredential updateLinkedInOAuthService(String name,String consumerKey,String comsumerSecret,String objectName){
        if(name == null ||  consumerKey == null || comsumerSecret == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_LINKEDIN;
        
        c.name = name;
        c.key = consumerKey ;
        c.secret = comsumerSecret;
        c.relatedTo = objectName;
        
        return updateCredential(c);
   }
   
   //upsert
   public static ProtectedCredential upsertLinkedInOAuthService(String name,String consumerKey,String comsumerSecret){
        return upsertLinkedInOAuthService(name,consumerKey,comsumerSecret,ProtectedCredential.RELATED_TO_OAUTHSERVICE);
   }
   
   public static ProtectedCredential upsertLinkedInOAuthService(String name,String consumerKey,String comsumerSecret,String objectName){
        if(name == null || consumerKey == null || comsumerSecret == null){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_LINKEDIN;
    
        c.name = name;
        c.key = consumerKey ;
        c.secret = comsumerSecret;
        c.relatedTo = objectName;
        
        return upsertCredential(c);
   }
   
    /**
    * insert, update, upsert LinkedIn OAuth Token
    */
    //insert
   public static ProtectedCredential insertLinkedInOAuthToken(String name,String secret,String token){
        return insertLinkedInOAuthToken(name,secret,token,ProtectedCredential.RELATED_TO_OAUTHTOKEN);
   }
   
   public static ProtectedCredential insertLinkedInOAuthToken(String name,String secret,String token,String objectName){
        if(name == null ||  secret == null || token == null ){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_LINKEDIN;
    
        c.name = name;
        c.secret = secret ;
        c.token = token;
        c.relatedTo = objectName;
        
        return insertCredential(c);
   }
   
   //update
   public static ProtectedCredential updateLinkedInOAuthToken(String name,String secret,String token){
        return updateLinkedInOAuthToken(name,secret,token,ProtectedCredential.RELATED_TO_OAUTHTOKEN);
   }
   
   public static ProtectedCredential updateLinkedInOAuthToken(String name,String secret,String token,String objectName){
        if(name == null ||  secret == null || token == null ){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_LINKEDIN;
        
        c.name = name;
        c.secret = secret ;
        c.token = token;
        c.relatedTo = objectName;
        
        return updateCredential(c);
   }
   
   //upsert
   public static ProtectedCredential upsertLinkedInOAuthToken(String name,String secret,String token){
        return upsertLinkedInOAuthToken(name,secret,token,ProtectedCredential.RELATED_TO_OAUTHTOKEN);
   }
   
   public static ProtectedCredential upsertLinkedInOAuthToken(String name,String secret,String token,String objectName){
        if(name == null ||  secret == null || token == null ){
           return null;
        }
        ProtectedCredential c = new ProtectedCredential();
        c.type=ProtectedCredential.TYPE_ADVERTISEMENT;
        c.provider = ProtectedCredential.PROVIDER_LINKEDIN;
    
        c.name = name;
        c.secret = secret ;
        c.token = token;
        c.relatedTo = objectName;
        
        return upsertCredential(c);
   }
   
   public static ProtectedCredential getLinkedInOAuthTokenByToken(String token){
   	
   		if(token == null){
   			return null;
   		}
   	
   		List<ProtectedCredential> pcs = getRelatedCredentials(ProtectedCredential.RELATED_TO_OAUTHTOKEN);
   		for(ProtectedCredential pc : pcs){
   			if(token.equals( pc.token)){
   				return pc;
   			}
   		}
   		
   		return null;
   	
   }
   
   
   public static List<ProtectedCredential> getRelatedCredentials(String relatedTo){
   		List<ProtectedCredential> pcs = new List<ProtectedCredential>();
   		if(relatedTo == null){
   			return pcs;
   		}
   		
        List<Credential_Setting__c> css = Credential_Setting__c.getall().values();
        if(css != null){
        	for(Credential_Setting__c cs : css){
        		if(relatedTo.equals(cs.Object_Name__c)){
        			pcs.add(buildCredential(cs));
        		}
        	}          
        }
        
        return pcs;
    }
    
   /**
    * get protected credentials by name
    */
    public static ProtectedCredential getCredential(String name){
    	
    	
        Credential_Setting__c cs = getCredentialSetting(name);
        if(cs != null){
            return buildCredential(cs);
        }
        
        return null;
    }
    
    /**
    * insert protected credentials 
    */
    public static ProtectedCredential insertCredential(ProtectedCredential pc){
        
        Credential_Setting__c cs = buildCredentialSetting(pc);
        if(cs != null){
            return assignCredential( pc, insertCredentialSetting(cs));
        }
        
        return null;
    }
    
    /**
    * update protected credentials 
    */
     public static ProtectedCredential updateCredential(ProtectedCredential pc){
        if(pc == null || pc.name == null){
            return null;
        }
        
        Credential_Setting__c cs = getCredentialSetting(pc.name);
        
        cs = assignCredentialSetting(cs, pc);
        
        if(cs != null){
            
            return assignCredential( pc,updateCredentialSetting(cs));
        }
        
        return null;
    }
    
    /**
    * upsert protected credentials 
    */
     public static ProtectedCredential upsertCredential(ProtectedCredential pc){
        if(pc == null || pc.name == null){
            return null;
        }
        
        
        Credential_Setting__c cs = getCredentialSetting(pc.name);
        
        if(cs == null){
            return insertCredential(pc);
        }else{
            cs = assignCredentialSetting(cs, pc);
        
            if(cs != null){
                
                return assignCredential( pc,updateCredentialSetting(cs));
            }
            
            return null;
        }
        
        
    }
   
    /**
    * delete protected credential
    */
    public static ProtectedCredential deleteCredential(ProtectedCredential pc){
        if(pc == null || pc.name == null){
            return null;
        }
        
      
        
        return deleteCredential(pc.name);
    }
    
    /**
    * delete protected credential by name
    */
    public static ProtectedCredential deleteCredential(String name){
        if(name == null){
            return null;
        }
        
       
        
        Credential_Setting__c cs = getCredentialSetting(name);
        if(cs != null){
            return buildCredential( deleteCredentialSetting(cs));
        }
        
        return null;
    }
    





    /**
     * Basic functionalities accessing Credential_Setting__c record, including:
     * get 
     * insert 
     * update
     */
    public static Credential_Setting__c getCredentialSetting(String name){
    	 if(name == null){
            return null;
        }
        
        name = convertTo15Id( name);
        
        return Credential_Setting__c.getInstance(name);
      
    }
    
    public static Credential_Setting__c insertCredentialSetting(Credential_Setting__c cs){
        
        if(cs == null){
            return null;
        }
        
        try{
            insert cs;
        }catch(Exception e){
        	System.debug(e);
            return null;
        }
        
        return cs;
        
    }
    
    public static Credential_Setting__c deleteCredentialSetting(Credential_Setting__c cs){
        
        
         if(cs == null){
            return null;
        }
        
        try{
            delete cs;
        }catch(Exception e){
        	System.debug(e);
            return null;
        }
        
        return cs;
        
    }
    
   public static Credential_Setting__c updateCredentialSetting(Credential_Setting__c cs){
        
        if(cs == null){
            return null;
        }
        
        try{
            update cs;
        }catch(Exception e){
        	System.debug(e);
            return null;
        }
        
        return cs;
        
    }
    
    
    /**
     * build a protected credential record by Credential_Setting__c record
     */
    public static ProtectedCredential buildCredential(Credential_Setting__c cs){
        
        if(cs == null)
            return null;
        
        ProtectedCredential pc = new ProtectedCredential();
        return assignCredential(pc, cs);
        
    }
    
    /**
     * assign Credential_Setting__c values to ProtectedCredential
     */
     public static ProtectedCredential assignCredential(ProtectedCredential pc ,Credential_Setting__c cs){
        
        if(pc == null || cs == null )
            return null;
            
        pc.id = cs.Id;
        pc.name = cs.Name;
        pc.provider = cs.Provider__c;
        pc.type = cs.Type__c;
        pc.password = cs.Password__c;
        pc.username = cs.Username__c;
        pc.key = cs.Key__c;
        pc.secret = cs.Secret__c;
        pc.token = cs.Token__c;
        pc.relatedTo = cs.Object_Name__c;
        
        
       return pc;
        
    }
    
     /**
     * build a Credential_Setting__c record by protected credential object
     */
     public static Credential_Setting__c buildCredentialSetting(ProtectedCredential pc){
        
        if(pc == null )
            return null;
        
        Credential_Setting__c cs = new Credential_Setting__c();
        return assignCredentialSetting(cs, pc);
        
    }
    
    /**
     * assign ProtectedCredential values to Credential_Setting__c
     */
    public static Credential_Setting__c assignCredentialSetting(Credential_Setting__c cs, ProtectedCredential pc){
        
        if(cs == null || pc == null)
            return null;
            
        pc.name = convertTo15Id(pc.name);
        
        cs.Provider__c = pc.provider;
        cs.Type__c = pc.type;
        cs.Object_Name__c = pc.relatedTo;
        cs.Name = pc.name;
        cs.Password__c = pc.password ;
        cs.Username__c = pc.username ;
        cs.Key__c = pc.key;
        cs.Secret__c = pc.secret;
        cs.Token__c = pc.token ;
        
        
       return cs;
        
    }
    
    public static Boolean insertProtectedAccount(ProtectedAccount protectedAccount){
		if(protectedAccount == null || protectedAccount.account == null ){
			return false;
		}
		return insertProtectedAccount( protectedAccount.account, protectedAccount.credential);
		
	}
    
    public static Boolean insertProtectedAccount(SObject account, ProtectedCredential pc){
		if(account == null || pc == null ){
			
			return false;
			
		}
		
		try{
			
			CommonSelector selector = new CommonSelector(account.getSObjectType());
			
			selector.doInsert( account);
			
			CredentialSettingService.upsertCredential(pc);
			
		}catch(Exception e){
			System.debug(e);
			return false;
		}
		
		return true;
	}
	
	public static Boolean updateProtectedAccount(ProtectedAccount protectedAccount){
		if(protectedAccount == null || protectedAccount.account == null || protectedAccount.fieldNames == null ){
			return false;
		}
		return updateProtectedAccount( protectedAccount.account, protectedAccount.credential,protectedAccount.fieldNames);
		
	}
    
    public static Boolean updateProtectedAccount(SObject account, ProtectedCredential pc, String fieldNames){
		if(account == null || pc == null ){
			
			return false;
			
		}
		
		try{
			
			CommonSelector selector = new CommonSelector(account.getSObjectType());
			
			selector.doUpdate( account, fieldNames);
			
			CredentialSettingService.upsertCredential(pc);
			
		}catch(Exception e){
			System.debug(e);
			return false;
		}
		
		return true;
	}
	
	
    
    public static Boolean deleteProtectedAccount(SObject account){
		if(account == null  ){
			
			return false;
			
		}
		
		try{
			
			CredentialSettingService.deleteCredential(account.Id);
			
			CommonSelector selector = new CommonSelector(account.getSObjectType());
			
			selector.doDelete( account);
			
		
		}catch(Exception e){
			System.debug(e);
			return false;
		}
		
		return true;
	}
	
	
	public static String convertTo15Id(String id){
		
		if(id != null && id.length() > 15){
			id =  id.substring(0,15);
		}
		
		return id;
		
	}

}