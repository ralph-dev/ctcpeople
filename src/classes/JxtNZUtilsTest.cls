@isTest
private class JxtNZUtilsTest
{
	static testmethod void testGetClassifications(){
		System.runAs(DummyRecordCreator.platformUser) {
		system.assertNotEquals(JxtNZUtils.getClassifications().size(), null);
		}
	} 
	
	static testmethod void testJxtNZUtil(){
		System.runAs(DummyRecordCreator.platformUser) {
		StyleCategoryDummyRecordCreator.createStyleCategoryDummyRecord();
		StyleCategory__c sc = new StyleCategory__c();
		sc.Name='a';
		insert sc;
		Placement__c v = new Placement__c();
		insert v;
		Advertisement__c ad = new Advertisement__c();
		ad.WebSite__c='a';	 ad.Location__c='a'; 	ad.SearchArea_Label_String__c='a';
		ad.Classification__c='a'; 	ad.Vacancy__c=v.id; 	ad.SubClassification2__c='a';
		ad.Classification2__c='a';	ad.SubClassification_Seek_Label__c='a';	 ad.WorkType__c='a';
		ad.Reference_No__c='a';		ad.Job_Title__c='a';		ad.Company_Name__c='a';
		ad.Template__c='a';		ad.Application_Form_Style__c=sc.id;	ad.Bullet_1__c='a';		
		ad.Bullet_2__c='a';		ad.Bullet_3__c='a';			ad.Job_Type__c='a';
		ad.JXT_Short_Description__c='a';	ad.jxt_sectors__c='a';		ad.Hot_Job__c=true;
		ad.Residency_Required__c=true;		ad.Search_Tags__c='a';		ad.Street_Adress__c='a';
		ad.Nearest_Transport__c='a';		ad.Job_Contact_Name__c='a';		ad.Job_Contact_Phone__c='12345';
		ad.Location_Hide__c =false;			ad.Salary_Type__c='a';			ad.SeekSalaryMin__c='123';
		ad.SeekSalaryMax__c='2353'; 		ad.Salary_Description__c='a';	ad.No_salary_information__c=false;
		ad.Job_Content__c='a';			ad.Has_Referral_Fee__c='a';	ad.Referral_Fee__c=500;
		ad.Terms_And_Conditions_Url__c=Endpoint.getPeoplecloudWebSite()+'';
		ad.JXTNZ_Location__c='a,b,c,d,e,f';
		ad.Industry__c='abc';
		insert ad;
		system.assert(JxtNZUtils.getFeed(ad)!= '');
		system.assertEquals(JxtNZUtils.transferBooleanToNumber(true),'1');
		system.assertEquals(JxtNZUtils.transferBooleanToNumber(false),'0');
		JxtNZUtils.getWriteLabels();
		system.assert(JxtNZUtils.getReferralItems().size()>0);
		}
	}
}