public with sharing class AvailabilityCompareUtils {
	private static final Date MONDAY = Date.newInstance(2015, 1, 5);

	public AvailabilityCompareUtils() {
		
	}

	public Map<Integer, Integer> generateShiftAndAvailToMap(Date sDate, Date eDate, String type, Map<String, String> shiftBinaryMap,  
																	Map<Integer, Integer> currentShiftBinaryMap){
		Map<String,Integer> shiftMatrixes = AvailabilityCompare.setupShiftMatrix();
		Map<String,Integer> operatorMatrixes = AvailabilityCompare.setupOperatorMatrix();

		Datetime sDatetime = WizardUtils.convertDateToDateTime(sDate);
		String sDateWeekDay = sDatetime.format('EEE');
		Integer sDateWeek = Math.ceil(MONDAY.daysBetween(sDatetime.date())/7).intValue();//Integer.valueOf(sDatetime.formatGmt('w'));
			
		Datetime eDatetime = WizardUtils.convertDateToDateTime(eDate);
		String eDateWeekDay = eDatetime.format('EEE');
		Integer eDateWeek =  Math.ceil(MONDAY.daysBetween(eDatetime.date())/7).intValue();//Integer.valueOf(eDatetime.formatGmt('w'));
				
		//get the matrix binary value	
		Integer shiftMatrixBinary = 0; 
		Integer operatorMatrixBinary = 0;  //get the operator binary value
		Integer shiftBinaryInteger = 0;
		String shiftTypeBinaryKey = '';

		if(shiftBinaryMap.containsKey(type)) {
			shiftTypeBinaryKey = shiftBinaryMap.get(type);
		}else {
			shiftTypeBinaryKey = shiftBinaryMap.get('on call');
		}
		shiftMatrixBinary = shiftMatrixes.get(shiftTypeBinaryKey);     
		//caculate the shift binary value

		if(sDate == eDate) {
			operatorMatrixBinary = operatorMatrixes.get(sDateWeekDay);
			system.debug('operatorMatrixBinary =' + operatorMatrixBinary);
			shiftBinaryInteger = shiftMatrixBinary&operatorMatrixBinary;
			system.debug('shiftBinaryInteger =' + shiftBinaryInteger);
			if(currentShiftBinaryMap.containsKey(sDateWeek)) {
				shiftBinaryInteger = shiftBinaryInteger | currentShiftBinaryMap.get(sDateWeek);
			}
			currentShiftBinaryMap.put(sDateWeek, shiftBinaryInteger);
		}else{
			if(sDateWeek == eDateWeek) {
				for(Datetime i = sDatetime ; i < WizardUtils.nextDT(eDatetime) ; i=WizardUtils.nextDT(i)){
					String iWeekday = i.format('EEE');
					system.debug('iWeekday ='+ iWeekday);
					operatorMatrixBinary = operatorMatrixes.get(iWeekday);
					shiftBinaryInteger = shiftMatrixBinary&operatorMatrixBinary;
					system.debug('shiftBinaryInteger =' + shiftBinaryInteger);
					if(currentShiftBinaryMap.containsKey(sDateWeek)) {
						shiftBinaryInteger = shiftBinaryInteger | currentShiftBinaryMap.get(sDateWeek);
					}
					currentShiftBinaryMap.put(sDateWeek, shiftBinaryInteger);
				}
					
			}else{
				String iStartWeekday = '';
				String iEndWeekday = '';
				Datetime iStartDatetime = sDatetime ;
				Datetime iEndDatetime = eDatetime ;
				
				do {
					iStartWeekday = iStartDatetime.format('EEE');
					system.debug('iStartWeekday='+iStartWeekday);
					operatorMatrixBinary = operatorMatrixes.get(iStartWeekday);
					system.debug('operatorMatrixBinary =' + operatorMatrixBinary);
					system.debug('shiftMatrixBinary =' + shiftMatrixBinary);
					shiftBinaryInteger = shiftMatrixBinary&operatorMatrixBinary;
					if(currentShiftBinaryMap.containsKey(sDateWeek)) {
						shiftBinaryInteger = shiftBinaryInteger | currentShiftBinaryMap.get(sDateWeek);
					}
					currentShiftBinaryMap.put(sDateWeek, shiftBinaryInteger);
					iStartDatetime=WizardUtils.nextDT(iStartDatetime);
					system.debug('iStartDatetime ='+iStartDatetime);
				}while(iStartWeekday != 'Sat');
				
				do {
					iEndWeekday = iEndDatetime.format('EEE');
					system.debug('iEndWeekday='+iEndWeekday);
					operatorMatrixBinary = operatorMatrixes.get(iEndWeekday);
					shiftBinaryInteger = shiftMatrixBinary&operatorMatrixBinary;
					system.debug('operatorMatrixBinary =' + operatorMatrixBinary);
					system.debug('shiftMatrixBinary =' + shiftMatrixBinary);
					if(currentShiftBinaryMap.containsKey(eDateWeek)) {
						shiftBinaryInteger = shiftBinaryInteger | currentShiftBinaryMap.get(eDateWeek);
					}
					currentShiftBinaryMap.put(eDateWeek, shiftBinaryInteger);
					iEndDatetime=iEndDatetime.addDays(-1);
					system.debug('iEndDatetime ='+iEndDatetime);
				}while(iEndWeekday != 'Sun');
				
				if(eDateWeek-sDateWeek>1){
					for(Integer i = sDateWeek+1 ; i< eDateWeek; i++) {
						currentShiftBinaryMap.put(i,shiftMatrixBinary);
					}
				}
			}
		}
		system.debug('currentShiftBinaryMap ='+currentShiftBinaryMap);
		return currentShiftBinaryMap;
	}

	public Map<String, candidateDTO> compareCandidateAvailAndShifts(Map<Integer, Integer> shiftWeekMap, Map<String, candidateDTO> candidateDTOMap) {
		
		Integer weekShiftBinaryNumber = 0;
		for(Integer numberofWeek : shiftWeekMap.keySet()) {
			Integer weekShiftBinary = shiftWeekMap.get(numberofWeek);
			weekShiftBinaryNumber += calculateBinaryHammingWeight(weekShiftBinary);

			for(String candidateId : candidateDTOMap.keySet()) {
				Integer weekRosterBinaryNumber = 0;
				Integer availabilityWeekBinary = 268435455;
				Integer unavailabilityWeekBinary = 0;

				candidateDTO cDTO = candidateDTOMap.get(candidateId);
				
				if(cDTO.getNumberOfRoster()!=null)
					weekRosterBinaryNumber = cDTO.getNumberOfRoster();
				
				Map<Integer, Integer> availabilityWeekMap = cDTO.getAvailableBinaryInteger();
				Map<Integer, Integer> unavailabilityWeekMap = cDTO.getUnavailableBinaryInteger();

				if(availabilityWeekMap.get(numberofWeek)!=null)
					availabilityWeekBinary = availabilityWeekMap.get(numberofWeek);
				if(unavailabilityWeekMap.get(numberofWeek)!=null)		
					unavailabilityWeekBinary = unavailabilityWeekMap.get(numberofWeek);
				
				Integer reverseUnavailabilityWeekBinary = 268435455 - unavailabilityWeekBinary;

				availabilityWeekBinary = availabilityWeekBinary&reverseUnavailabilityWeekBinary;
				Integer weekShiftCompareAvail = weekShiftBinary&availabilityWeekBinary;
				weekRosterBinaryNumber += calculateBinaryHammingWeight(weekShiftCompareAvail);
				decimal availabilityPercentage = (Decimal.valueOf(weekRosterBinaryNumber)).divide(weekShiftBinaryNumber,3);

				cDTO.setNumberOfShifts(weekShiftBinaryNumber);
				cDTO.setNumberOfRoster(weekRosterBinaryNumber);
				cDTO.setAvailabilityPercentage(availabilityPercentage);
				Integer availablityRanking = Integer.valueOf(Math.floor(availabilityPercentage*5));
				cDTO.setAvailablityRanking(availablityRanking);

				candidateDTOMap.put(candidateId , cDTO);
			}
			
		}

		return candidateDTOMap;
	}

	private Integer calculateBinaryHammingWeight(Integer recordBinary) {
		Integer numberOfDays = 0;
		recordBinary = recordBinary - ((recordBinary >> 1) & 1431655765);
		recordBinary = (recordBinary & 858993459) + ((recordBinary >> 2) & 858993459);
		numberOfDays = (((recordBinary + (recordBinary >> 4)) & 252645135) * 16843009) >> 24;
		return numberOfDays;
	}
}