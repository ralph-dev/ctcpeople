/**
 * Seletor for Skill Object
 * 
 * Created by: Lina
 * Created on: 09/08/2016
 * 
 */ 
 
public with sharing class SkillSelector extends CommonSelector{

    public SkillSelector(){
        super(Skill__c.SObjectType);
    }

    
    /**
     * Get all skills and skill groups
     * 
     * @return List<Skill__c>     all skills in the instance
     *
    **/ 
    public static List<Skill__c> getAllSkills() { 
        List<Skill__c> skillList = new List<Skill__c>();
        String query = 'select Skill_Group__c, Skill_Group__r.Name, Name, Id from Skill__c '+                                   
                       'where Skill_Group__r.Enabled__c=true limit 50000';          
        try {
            CommonSelector.checkRead(Skill__c.SObjectType,'Skill_Group__c, Skill_Group__r.Name, Name, Id');
            skillList = Database.query(query);
        }catch(Exception e) {
            skillList = new List<Skill__c>();
        }
        return skillList;
    }
    
    /**
     * Get all skills for a specific skill group
     * 
     * @param skillGroupId          Skill Group Id
     * @return List<Skill__c>       All skills for a skill group
     * 
    **/ 
    public static List<Skill__c> getSkillsForSkillGroup(String skillGroupId) {
        List<Skill__c> skillList = new List<Skill__c>();
        String query = 'select Skill_Group__c, Skill_Group__r.Name, Name, Id, Ext_Id__c from Skill__c '+                                   
                        'where Skill_Group__c =: skillGroupId and Skill_Group__r.Enabled__c=true order by Name limit 50000'; 
        try {
            CommonSelector.checkRead(Skill__c.SObjectType,'Skill_Group__c, Skill_Group__r.Name, Name, Id, Ext_Id__c');
            skillList = Database.query(query);
        }catch(Exception e) {
            skillList = new List<Skill__c>();
        }
        return skillList;
    }

}