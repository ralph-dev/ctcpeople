public with sharing class CTCAdminController{
  public String result {get;set;}
    public boolean is_allowed {get;set;}
    public void init(){
      is_allowed = false;
      UserInformationCon usercls = new UserInformationCon();
		  User Current_User = usercls.getUserDetail();
      if(Current_User != null){
        if(Current_User.View_CTC_Admin__c == false){
          is_allowed = false;
        }
        else{
          is_allowed = true;
        }
    }
  }
}