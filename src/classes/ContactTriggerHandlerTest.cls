@isTest
private class ContactTriggerHandlerTest {
	
	public static void init(){
		SQSCredential__c sqsc = new SQSCredential__c(Name='Daxtra Feed SQS');
        sqsc.URL__c = 'https://sqs.ap-southeast-2.amazonaws.com/729402844354/CandidateFeedRequest';
        sqsc.Access_Key__c = '123';
        sqsc.Secret_Key__c = '123';
        upsert sqsc;
	}

	public testmethod static void testAfterInsertContact(){
		System.runAs(DummyRecordCreator.platformUser) {
		init();
		Set<Id> vsSet = new Set<Id>();
		List<Placement__c> vs = new List<Placement__c>();
		Placement__c v1 = new Placement__c(Name='V1');
		Placement__c v2 = new Placement__c(Name='V2');
		vs.add(v1); vs.add(v2);
		insert vs;
		vsSet.add(v1.id); vsSet.add(v2.id);
		
		Set<Id> adsSet = new Set<Id>();		
		list<Advertisement__c> ads = new List<Advertisement__c>();
		Advertisement__c ad1 = new Advertisement__c(Vacancy__c=v1.id);
		Advertisement__c ad2 = new Advertisement__c(Vacancy__c=v2.id);
		ads.add(ad1); ads.add(ad2);
		insert ads;
		adsSet.add(ad1.id); adsSet.add(ad2.id);
		
		Set<Id> contactsSet = new Set<Id>();
		List<Contact> cons = new List<Contact>();
		Contact con1 = new Contact(LastName='san11', job_reference__c='abcde:'+ad1.id);
		Contact con2 = new Contact(LastName='san12', job_reference__c='aaa:'+ad2.id);
		Contact con3 = new Contact(LastName='san13');
		cons.add(con1);
		cons.add(con2);
		cons.add(con3);
		insert cons;
		contactsSet.add(con1.id);
		contactsSet.add(con2.id);
		
		Set<Id> contactSet = new Set<Id>();
		for(Contact con:cons){
			contactSet.add(con.Id);
		}
		Placement_Candidate__c[] pcs = [select Id, Name, Candidate__c, Placement__r.Id, Placement__r.Name ,
			 Online_Ad__r.Id, Online_Ad__r.Name from Placement_Candidate__c where Candidate__c in :contactSet];
		
		for(Placement_Candidate__c pc:pcs){
			//system.debug('pc name = '+pc.Name + ' vancancy name = '+pc.Placement__r.Name+' advertisement name = '+pc.Online_Ad__r.Name);
			system.assert(vsSet.contains(pc.Placement__r.Id));
			system.assert(adsSet.contains(pc.Online_Ad__r.Id));
			system.assert(contactSet.contains(pc.Candidate__c));
		}  
		ContactTriggerHandler.afterInsert(cons);
		}
	}  
	
	public testmethod static void testAfterDelContact(){
		System.runAs(DummyRecordCreator.platformUser) {
		init();
		S3Utils.testFlag=true;
		Contact con1 = new Contact(Lastname='zhangsan');
		insert con1; 
		Web_Document__c webdocument5 = new Web_Document__c(name='web005', Document_Related_To__c=con1.Id, ObjectKey__c='test001', S3_Folder__c='test0001');
		Web_Document__c webdocument6 = new Web_Document__c(name='web006', Document_Related_To__c=con1.Id, ObjectKey__c='test003', S3_Folder__c='test0004');
		insert webdocument5; 
		insert webdocument6;
		delete con1;
		undelete con1;
		Set<Id> contactsSet = new Set<Id>();
		contactsSet.add(con1.Id);
		ContactTriggerHandler.UndeleteDocs(contactsSet);

		List<Web_Document__c> webdocumentAfterDelete = [select id from Web_Document__c where Document_Related_To__c =: con1.Id];
		system.assertEquals(webdocumentAfterDelete.size(),0);
		
		}
		
		
	}

}