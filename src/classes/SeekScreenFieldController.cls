public with sharing class SeekScreenFieldController {
  
  FieldsClass ObjectFields; 
  List<FieldsClass.AvailableField> allFields = new List<FieldsClass.AvailableField>{};
 
  public SeekScreenFieldController() {
    ObjectFields = new FieldsClass('Contact');
    allFields = ObjectFields.getAvailableFields();
  }
  
  public List<FieldsClass.AvailableField> getAllfields(){    
    return allFields ;
  }
}