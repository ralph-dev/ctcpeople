//Only can use for sort by date
global class ContactWrapper implements Comparable {
    public Contact con;
    public String fieldName;
    
    public ContactWrapper(Contact c, String fn) {
        con = c;
        fieldName = fn;
    }
    
    global Integer compareTo(Object compareTo) {
        //Cast object to ContactWrapperByCreatedDate
        ContactWrapper compareToContact = (ContactWrapper) compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer ret = 0;
        if ((DateTime)con.get(fieldName) > (DateTime)compareToContact.con.get(fieldName)) {
            // Set return value to a positive value.
            ret = 1;
        } else if ((DateTime)con.get(fieldName) < (DateTime)compareToContact.con.get(fieldName)) {
            // Set return value to a negative value.
            ret = -1;
        }
        
        return ret;
    }
}