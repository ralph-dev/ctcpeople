/*
    *Author Jack Zhou
    *The class for generate the activity document and send to EC2 server
*/

public with sharing class ActivityDoc {
	//added by andy yang
	public String userId;
	//
    public String orgId;
    public String activityId;
    public String docType {get; set;}
    public Map<String,String> whoMap=new Map<String,String>();
    public Map<String,String> whatMap=new Map<String,String>();
    public String attachmentId {get; set;}
    public String attachmentName {get; set;}
    public List<String> skillGroupIds; 
    public Boolean isSelected {get; set;}
    public String amazonS3Folder;
    public String nameSpace;
    public String enableSkillParsing;
    public Boolean isEmailService;
    public String resourceType {get; set;}
    public String resourceName {get; set;}
    public String resourceId {get; set;}

}