/**
Update JXT or JXTNZ advertisement.
According to incoming ad id, retrieve ad info then determine
which job board this ad belongs to. After that, it will redirect
user to corresponding ad edit page to let user update the content
of this advertisement. 

Update detail by Jack on 12/09/2014
The status is not Active, Clone to Post, Recalled or Not Valid not allowed to update the Ad
If the Job_Posting_Status__c is In Queue not allow to update ad
**/
public with sharing class UpdateSelect {
	public String id{get; set;}
	public Advertisement__c ad {get; set;}
	public UpdateSelect(ApexPages.StandardController stdController){
		id = ApexPages.currentPage().getParameters().get('id');
		System.debug('id='+id);
		CommonSelector.checkRead(Advertisement__c.SObjectType,'id, WebSite__c, Status__c, Job_Posting_Status__c');
		ad = [select id, WebSite__c, Status__c, Job_Posting_Status__c from Advertisement__c where id=:id];
	}
	
	public PageReference selectPage(){
		
		PageReference newPage = null; //Add ad status is null return to error page. Changed by Jack on22/09/2014
		if(ad.Status__c == null || (ad.Status__c!='Active' && ad.Status__c!='Clone to Post' && ad.Status__c!='Recalled' && !ad.Status__c.equalsignorecase('not valid'))){	// "ad.Status__c!='Recalled'" was added by Kevin on 25.06.2012
																																				// !ad.Status__c.equalsignorecase('not valid') was added by Jack on 12/09/2014
			newPage = Page.peopleCloudErrorInfo;
			newPage.getParameters().put('code','10004');
			newPage.setRedirect(true);
			return newPage;	
		}
		
		if(ad.Job_Posting_Status__c == 'In Queue'){
			newPage = Page.peopleCloudErrorInfo;
			newPage.getParameters().put('code','10005');
			newPage.setRedirect(true);
			return newPage;	
		}
		
		System.debug('website = '+ad.webSite__c);
		if(ad.webSite__c.toLowerCase()=='seek'){
			newPage = Page.seekEdit;
		}else if(ad.webSite__c.toLowerCase()=='careerone'){
			newPage = Page.careeronePageEdit;
		}else if(ad.webSite__c.toLowerCase()=='trademe'){
			newPage = Page.trademeEdit;
		}else if(ad.webSite__c.toLowerCase()=='jxt'){
			newPage = Page.jxtEdit;
		}else if(ad.webSite__c.toLowerCase()=='jxt_nz'){
			newPage = Page.jxtNZEdit;
		}else if(ad.webSite__c.toLowerCase()=='website'){
			newPage = Page.websiteEdit;
		}else{
			// impossible
		}
		newPage.getParameters().put('id', id);
		newPage.setRedirect(true);
		return newPage;
	}
}