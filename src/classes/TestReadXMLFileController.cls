@isTest
private class TestReadXMLFileController {

 //   static {
 //       Contact dummyContact = new Contact();
 //       dummyContact.Email = 'test@test.com';
 //       dummyContact.LastName = 'Test';
 //       insert dummyContact;
        
 //       Folder ctcFolder = DaoFolder.getFolderByDevName('CTC_Admin_Folder');
 //       String dummyResultContactString = '<?xml version="1.0"?><!--file body starts here--><fileroot><object objectname="Contact"><field><label>Email</label><type>EMAIL</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_Email" value=""/></div>]]></component><apiname>Email</apiname><location>left</location><sequence>0</sequence><filterable>true</filterable></field><field><label>Last Name</label><type>STRING(80)</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_LastName" value=""/></div>]]></component><apiname>LastName</apiname><location>left</location><sequence>1</sequence><filterable>true</filterable></field><field><label>Title</label><type>STRING(80)</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_Title" value=""/></div>]]></component><apiname>Title</apiname><location>left</location><sequence>2</sequence><filterable>true</filterable></field><field><label>Full Name</label><type>STRING(121)</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_Name" value=""/></div>]]></component><apiname>Name</apiname><location>left</location><sequence>3</sequence><filterable>true</filterable></field><field><label>Mailing Country</label><type>STRING(40)</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_MailingCountry" value=""/></div>]]></component><apiname>MailingCountry</apiname><location>right</location><sequence>0</sequence><filterable>true</filterable></field><field><label>Mobile Phone</label><type>PHONE</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_MobilePhone" value=""/></div>]]></component><apiname>MobilePhone</apiname><location>right</location><sequence>1</sequence><filterable>true</filterable></field><field><label>First Name</label><type>STRING(40)</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_FirstName" value=""/></div>]]></component><apiname>FirstName</apiname><location>right</location><sequence>2</sequence><filterable>true</filterable></field></object><columns><column><label>Created Date</label><obj>EMPTY_STRING</obj><api>CreatedDate</api><type>DATETIME</type><sortable>true</sortable></column></columns></fileroot>';
 //       Document ctcContact;
 //       String contactfileName = 'ClicktoCloud_Xml_Contact';
 //       try {
 //           ctcContact = [select Id,DeveloperName, Body 
 //                           from Document where Type ='xml' and DeveloperName= :contactfileName 
 //                           and FolderId = : ctcFolder.Id];
 //       } catch (Exception e) {
 //           ctcContact = new Document();
 //           ctcContact.Name = 'ClicktoCloud_Xml(Contact)';
 //           ctcContact.FolderId = UserInfo.getUserId();
 //           ctcContact.Body = Blob.valueOf(dummyResultContactString);
 //           ctcContact.Type = 'xml';
 //           ctcContact.DeveloperName = contactfileName;
 //           insert ctcContact;
 //       }
        
 //       String contactRealName = contactfileName + '_' + UserInfo.getOrganizationId();
 //       List<Document> contactDocs = [select Id,Name,DeveloperName, Body 
 //                   from Document where Type ='xml' and DeveloperName like: contactRealName + '%'
 //                   and Name =: contactRealName and FolderId = : ctcFolder.Id];
 //       if (contactDocs.size() == 0) {
 //           Document doc = new Document();
 //           doc.Name = contactRealName;
 //           doc.FolderId = ctcFolder.Id;
 //           doc.Type = 'xml';
 //           doc.DeveloperName = contactRealName + '_' + Datetime.now().getTime();
 //           doc.Body = ctcContact.Body;
 //           insert doc;
 //       }
        
 //       String dummyAccountXmlString = '<?xml version="1.0"?><fileroot><object objectname="Account"><field><label>Account Name</label><type>STRING(255)</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_Name" value=""/></div>]]></component><apiname>Name</apiname><location>left</location><sequence>0</sequence><filterable>true</filterable></field><field><label>Website</label><type>URL</type><component><![CDATA[<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_Website" value=""/></div>]]></component><apiname>Website</apiname><location>left</location><sequence>1</sequence><filterable>true</filterable></field><field><label>SLA Expiration Date</label><type>DATE</type><component><![CDATA[<div><SELECT  name="CTC_XML_COMPONENT" id="CTC_PeopleCloud1__SLAExpirationDate__c" ><OPTION VALUE="1" >Today</OPTION><OPTION VALUE="7">Last 7 Days</OPTION><OPTION VALUE="14">Last 14 Days</OPTION><OPTION VALUE="30">Last 30 Days</OPTION><OPTION VALUE="60">Last 60 Days</OPTION><OPTION VALUE="90">Last 90 Days</OPTION></SELECT></div>]]></component><apiname>PeopleCloud1__SLAExpirationDate__c</apiname><location>left</location><sequence>2</sequence><filterable>true</filterable></field><field><label>Type</label><type>PICKLIST</type><component><![CDATA[<div><SELECT  name="CTC_XML_COMPONENT" SIZE="1" id="CTC_PeopleCloud1__Type__c" ><OPTION VALUE="Prospect" >Prospect<OPTION VALUE="Customer" >Customer<OPTION VALUE="Internal Account" >Internal Account</SELECT></div>]]></component><apiname>PeopleCloud1__Type__c</apiname><location>right</location><sequence>0</sequence><filterable>true</filterable></field></object><columns><column><label>Owner ID</label><obj>EMPTY_STRING</obj><api>OwnerId</api><type>REFERENCE</type><sortable>true</sortable></column></columns></fileroot>';
 //       Document ctcAccount;
 //       String fileName = 'ClicktoCloud_Xml_Account';
 //       try {
 //           ctcAccount = [select Id,DeveloperName, Body 
 //                           from Document where Type ='xml' and DeveloperName= :fileName 
 //                           and FolderId = : ctcFolder.Id];
 //       } catch (Exception e) {
 //           ctcAccount = new Document();
 //           ctcAccount.Name = 'ClicktoCloud_Xml(Account)';
 //           ctcAccount.FolderId = UserInfo.getUserId();
 //           ctcAccount.Body = Blob.valueOf(dummyAccountXmlString);
 //           ctcAccount.Type = 'xml';
 //           ctcAccount.DeveloperName = fileName;
 //           insert ctcAccount;
 //       }
        
 //       String realName = fileName + '_' + UserInfo.getOrganizationId();
 //       List<Document> docs = [select Id,Name,DeveloperName, Body 
 //                   from Document where Type ='xml' and DeveloperName like: realName + '%'
 //                   and Name =: realName and FolderId = : ctcFolder.Id];
 //       if (docs.size() == 0) {
 //           Document doc = new Document();
 //           doc.Name = realName;
 //           doc.FolderId = ctcFolder.Id;
 //           doc.Type = 'xml';
 //           doc.DeveloperName = realName + '_' + Datetime.now().getTime();
 //           doc.Body = ctcAccount.Body;
 //           insert doc;
 //       }
    
 //   }
    
	//public static testmethod void testAccountcontroller() {  
	//	System.runAs(DummyRecordCreator.platformUser) {
 //   	Account account = new account(Name='test');
 //   	insert account;
 //   	Contact contact = new contact(FirstName='test',LastName='test', email='a@test.com' , phone = '111111111');
 //   	insert contact;
 //   	ApexPages.currentPage().getParameters().put('id', contact.id);
 //   	ReadXMLFileController thecontroller1 = new ReadXMLFileController();
 //       thecontroller1.getDocBody();        
 //       thecontroller1.initAccountSearch();
 //       system.assert(thecontroller1.gobackAPSSearchCriteria == null);
 //       system.assert(thecontroller1.tempcomponmentlist.size() == 0);
 //       system.assert(thecontroller1.tempid != null);
 //       thecontroller1.DoQuery();
 //       if(thecontroller1.total_num >0 && thecontroller1.total_num<10000){
 //       	system.assert(thecontroller1.is_Show_DisplayBtn  == true);
 //       	system.assert(thecontroller1.tempSetController != null);
 //       }
 //       else{
 //       	system.assert(thecontroller1.is_Show_DisplayBtn  == false);
 //       }
 //       thecontroller1.displayresult();
 //       system.assert(thecontroller1.getSelectableRecords().size()>0);
 //       system.assert(thecontroller1.SetController.getHasPrevious() == false);
 //       thecontroller1.previous();
 //       system.assert(thecontroller1.getSelectableRecords().size()>0);
 //       thecontroller1.next();
 //       if(thecontroller1.SetController.getHasNext())
 //       system.assert(thecontroller1.getSelectableRecords().size()>0);
 //       PageReference pageRef = thecontroller1.BacktoContact();
 //       system.assertNotEquals(null, pageRef);
	//	}
 //   }
    
 //  static testmethod void testthiscontroller() {  
 //  	System.runAs(DummyRecordCreator.platformUser) {
 //       System.debug('testthiscontroller');
 //       Contact contact = new contact(FirstName='test',LastName='test', email='a@test.com' , phone = '111111111');
 //   	insert contact;
 //       Placement__c p = new Placement__c(Name='test',RecordTypeId = DaoRecordType.contractVacRT.Id);
 //       insert p;
 //       ApexPages.currentPage().getParameters().put('id',p.id);
 //       ApexPages.currentPage().getParameters().put('istab','0');  
 //       ApexPages.currentPage().getParameters().put('OrderField','FirstName');               
 //       ReadXMLFileController thecontroller = new ReadXMLFileController();        
 //       //thecontroller.getDocBody();
 //       thecontroller.initContactSearch();
 //       //system.assert(thecontroller.tempid != null);
 //       system.assert(thecontroller.SelectedObject == 'Contact');
 //       system.assert(thecontroller.showDayRangeSearch == true );
 //       thecontroller.DoQuery();
 //       system.debug('thecontroller.total_num='+ thecontroller.total_num);
 //       if(thecontroller.total_num >0 && thecontroller.total_num<10000){
 //       	system.assert(thecontroller.is_Show_DisplayBtn  == true);
 //       	system.assert(thecontroller.tempSetController != null);
 //       }
 //       else{
 //       	system.assert(thecontroller.is_Show_DisplayBtn  == false);
 //       }
 //       thecontroller.displayresult();
 //       system.assert(thecontroller.getSelectableRecords().size()>0);
 //       system.assert(thecontroller.SetController.getHasPrevious() == false);
 //       thecontroller.previous();
 //       system.assert(thecontroller.getSelectableRecords().size()>0);
 //       thecontroller.next();
 //       if(thecontroller.SetController.getHasNext())
 //       system.assert(thecontroller.getSelectableRecords().size()>0);        
 //       thecontroller.DoQuery2();
 //       thecontroller.getIsBeyondtheNum();
 //       thecontroller.getNo_confirm();
 //       thecontroller.sortResult();
 //       thecontroller.SortFields();
 //       //thecontroller.SortFieldsConfirm();
 //       thecontroller.SortFieldsCancel();
 //       thecontroller.AddActivity();
 //       thecontroller.BacktoContact();
 //       for(ReadXmlFileController.SelectRecord c : thecontroller.getSelectableRecords()){
 //       	c.selected = true;
 //       }
 //       thecontroller.apply();
 //       thecontroller.BulkEmail();
 //       system.assert(thecontroller.APSSearchCriteria!= '');
 //       thecontroller.resetSearchCriteria();
 //       testSelectRecord();
 //       PageReference ref = thecontroller.BacktoPlacement();
 //       system.assertNotEquals(null, ref);
 //       thecontroller.apply();
 //       //thecontroller.doapply();
 //  	}
 //   }
    
 //   public static testmethod void testplacementcontroller() {  
 //   	System.runAs(DummyRecordCreator.platformUser) {
 //   	Account account = new account(Name='test');
 //   	insert account;
 //   	Contact contact = new contact(FirstName='test',LastName='test', email='a@test.com' , phone = '111111111');
 //   	insert contact;
 //   	component nc = new component('Last Name','<div><input  name="CTC_XML_COMPONENT" type="text" id="CTC_LastName" value=""/></div>','STRING','LastName','component0','0','0','');
 //   	ApexPages.currentPage().getParameters().put('id', contact.id);
 //   	ReadXMLFileController theplacementcontroller = new ReadXMLFileController(); 
 //   	theplacementcontroller.initPlacementSearch(); 
 //   	system.assertEquals('Placement__c', theplacementcontroller.SelectedObject);
 //   	//Boolean isBeyondtheNum = theplacementcontroller.getIsBeyondtheNum();
 //   	//Boolean no_confirm = theplacementcontroller.getNo_confirm();
 //   	//PageReference p = theplacementcontroller.resetSearchCriteria();
 //   	//p = theplacementcontroller.DoQuery2();
 //   	}
 //   }
 //  static testmethod void testSelectRecord(){
 //  	System.runAs(DummyRecordCreator.platformUser) {
 //  	System.debug('testSelectRecord');
 //   	Account a=new Account(Name='abctest',AccountNumber='cbc',AnnualRevenue=1234,Sic='asdg',NumberOfEmployees=100,Description='asf');
 //   	insert a;
 //   	SObject obj = [select Name,AccountNumber,AnnualRevenue,Sic,NumberOfEmployees, Description from Account where Id=:a.id limit 1];
 //   	system.debug('obj ='+ obj);
 //   	List<String> fieldNameList = new String[]{'Name','AccountNumber','NumberOfEmployees','AnnualRevenue','Sic','Description'};
 //   	ReadXmlFileController.SelectRecord rec = new ReadXmlFileController.SelectRecord(obj,fieldNameList);
 //   	system.assert(rec.RecordId != null);
 //   	system.assert(rec.Value_1 != null);
 //   	system.assert(rec.Value_2 != null);
 //   	system.assert(rec.Value_3 != null);
 //   	system.assert(rec.Value_4 != null);
 //   	system.assert(rec.Value_5 != null);
 //   	system.assert(rec.Value_6 != null);
 //   	system.assert(rec.TheObject !=null);
    	
 //   	//system.assert(rec.Selected);
 //  	}
 //   }  
}