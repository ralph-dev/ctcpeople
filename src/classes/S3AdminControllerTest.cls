@isTest
public class S3AdminControllerTest {
    private static testMethod void s3AdminControllerTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
	    	S3TestData.setTestData();			
		    S3AdminController thecontroller = new S3AdminController();
		    thecontroller.pageinit();
			system.assert(thecontroller.getkey() == null);
		    system.assert(thecontroller.getsecret() == null);
			system.assert(thecontroller.BucketNames.size()>0);
			system.assert(thecontroller.users2.size()>0);
			thecontroller.bucketToList = 'test';
			PageReference pagelistBucket = thecontroller.listBucket();
			system.assert(pagelistBucket == null);
			thecontroller.bucketNameToCreate = 'test';
			PageReference pagelistlistBucket = thecontroller.createBucket();
			system.assert(pagelistlistBucket == null);
			thecontroller.bucketNameToDelete = 'test';
			PageReference pagedeleteBucket = thecontroller.deleteBucket();
			system.assert(pagedeleteBucket == null);
			User currentuser = DaoUsers.getActiveUser(userinfo.getuserid());
			currentuser.AmazonS3_Folder__c = 'test';
			update currentuser;	
			PageReference pageupdateusers = thecontroller.updateusers();	
			system.assert(pageupdateusers == null);
			PageReference pageupdateCredential = thecontroller.updateCredential();
			system.assert(pageupdateCredential == null);
			PageReference pageredirectToS3Key = thecontroller.redirectToS3Key();
			system.assert(pageredirectToS3Key != null);
			PageReference pagegetDirectory =  thecontroller.getDirectory('test');
			thecontroller.refresh();
			system.assert(pagegetDirectory != null);
    	}
	}
}