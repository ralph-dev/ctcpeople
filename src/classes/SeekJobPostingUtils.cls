/**
 * This is the Utils class for Seek Job Posting
 *
 * Created by: Lina Wei
 * Created on: 3/03/2017
 */

public with sharing class SeekJobPostingUtils {

    /**
     * This method is to generate SeekJobContent(Object will send to Seek) from a ad record
     *
     * @param: ad               Advertisement__c record
     * @param: account          Seek Account used by the ad record
     * @param: refNo            a stirng in the format of "15DidigtsOrgId:adId"
     * return JSON string of SeekJobContent object
     */
    public static String generateJobContent(Advertisement__c ad, Advertisement_Configuration__c account,
            String refNo, Map<String, String> templateItemsMap) {
        SeekJobContent adContent = new SeekJobContent();

        /********* Set third party *********/
        SeekJobContent.ThirdParties thirdParties = new SeekJobContent.ThirdParties(account.Advertisement_Account__c);
        adContent.thirdParties = thirdParties;
        /***********************************/

        adContent.advertisementType = (ad.isStandOut__c == true)? 'StandOut' : 'Classic';
        adContent.jobTitle = ad.Job_Title__c;
        adContent.searchJobTitle = ad.Online_Search_Title__c;

        /********* Set location *********/
        SeekJobContent.Location location = new SeekJobContent.Location(ad.RealLocation_Seek__c, ad.RealArea_Seek__c);
        adContent.location = location;
        /***********************************/

        adContent.subclassificationId = ad.SeekSubclassification__c;
        adContent.workType = ad.WorkType__c;

        /********* Set Salary *********/
        SeekJobContent.Salary salary = new SeekJobContent.Salary(
                ad.SeekSalaryType__c, ad.SeekSalaryMin__c, ad.SeekSalaryMax__c, ad.SeekSalaryText__c);
        adContent.salary = salary;
        /***********************************/

        adContent.jobSummary = ad.Online_Summary__c;
        String footer = (ad.Online_Footer__c == null) ? '' :ad.Online_Footer__c;
        adContent.advertisementDetails = ad.Job_Content__c;
        if (String.isNotEmpty(ad.Online_Footer__c)) {
            adContent.advertisementDetails += '<p>' + ad.Online_Footer__c + '</p>';
        }

        /********* Set Contact *********/
        SeekJobContent.SeekAdContact contact = new SeekJobContent.SeekAdContact(
                ad.Job_Contact_Name__c, ad.Job_Contact_Phone__c, ad.Job_Contact_Email__c);
        adContent.contact = contact;
        /***********************************/

        /********* Set Video *********/
        if (String.isNotEmpty(ad.Video_ID__c) && String.isNotEmpty(ad.Video_Position__c)) {
            String url = 'https://youtu.be/' + ad.Video_ID__c;
            SeekJobContent.Video video = new SeekJobContent.Video(url, ad.Video_Position__c);
            adContent.video = video;
        }
        /***********************************/

        adContent.applicationEmail = ad.Prefer_Email_Address__c;
        if (account.Seek_Application_Export_Enabled__c) {
            adContent.screenId = ad.SeekScreen__c;
        }
        adContent.jobReference = refNo;

        /********* Set Template *********/
        SeekJobContent.Template template = new SeekJobContent.Template(ad.TemplateCode__c);
        if (templateItemsMap != null && templateItemsMap.size() > 0) {
            template.items = new List<SeekJobContent.Item>();
            for (String item : templateItemsMap.keySet()) {
                template.items.add(new SeekJobContent.Item(item, String.valueOf(ad.get(templateItemsMap.get(item)))));
            }
        }
        adContent.template = template;
        /***********************************/

        /********* Set Standout *********/
        if (ad.isStandOut__c) {
            SeekJobContent.Standout standout = new SeekJobContent.Standout(ad.AdvertiserJobTemplateLogoCode__c);
            List<String> bullets = new List<String>{ad.Search_Bullet_Point_1__c, ad.Search_Bullet_Point_2__c, ad.Search_Bullet_Point_3__c};
            standout.bullets = bullets;
            adContent.standout = standout;
        }
        /***********************************/

        /********* Set recruiter *********/
        // Get vacancy owner's name and email
        SeekJobContent.Recruiter recruiter = new SeekJobContent.Recruiter();
        VacancySelector vacancySel = new VacancySelector();
        Placement__c vacancy = vacancySel.fetchVacancyOwner(ad.Vacancy__c);
        recruiter.fullName = vacancy.Owner.Name;
        recruiter.email = vacancy.Owner.Email;
        adContent.recruiter = recruiter;
        /***********************************/

        /********* Set additionalProperties *********/
        List<String> additions = new List<String>();
        if (ad.Residency_Required__c) {
            additions.add('ResidentsOnly');
        }
        if (String.isNotEmpty(ad.SeekMarketSegment__c) && ad.SeekMarketSegment__c.equalsIgnoreCase('Graduate')) {
            additions.add('Graduate');
        }
        adContent.additionalProperties = additions;
        /***********************************/

        adContent.creationId = refNo;
        /********* Finish create SeekJobContent *********/
        
        String jsonString = JSON.serialize(adContent);
		while(!jsonString.isAsciiPrintable()){
            String sub = findChar(jsonString);
            jsonString = jsonString.replaceAll(sub, '');
       	}
        return jsonString;
    }

    private static String findChar(String str){
        str = str.escapeUnicode();
        Integer start = str.indexOf('\\u');
        String sub = str.substring(start, start+6);
        return sub;      
    }
}