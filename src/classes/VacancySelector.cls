public with sharing class VacancySelector extends CommonSelector{
									
	final static String VACANCY_QUERY_BASE_1='SELECT Id, RecordTypeId, RecordType.Name, Name,Company__c, Company__r.Name,'
			+'Company__r.ShippingStreet, Company__r.ShippingCity,Company__r.ShippingPostalCode,'
			+'Company__r.ShippingCountry, Company__r.ShippingLatitude,Company__r.ShippingLongitude,'
			+'Company__r.ShippingState, Start_Date__c, End_Date__c, Candidate_Charge_Rate__c, Category__c,'
			+'Specialty__c, Seniority__c, Rate_Type__c,Stage__c FROM Placement__c';
	
	//private DynamicSelector dynamicSel;

	public VacancySelector() {
		super(Placement__c.SObjectType);
		//dynamicSel = new DynamicSelector(Placement__c.SObjectType.getDescribe().getName(), false, true, true);
	}

	private List<String> getVacancyFieldsForPeopleSearch(){
		return new List<String>{
			'id',
			'RecordTypeId',
			'RecordType.Name',
			'Name',
			'Start_Date__c',
			'End_Date__c',
			'Candidate_Charge_Rate__c',
			'Category__c',
			'Specialty__c',
			'Seniority__c',
			'Rate_Type__c',
			'Stage__c',
			'Company__c'
		};
	}

	private List<String> getCompanyFields(){
		return new List<String>{
			'Company__r.Name',
			'Company__r.ShippingStreet',
			'Company__r.ShippingCity',
			'Company__r.ShippingPostalCode',
			'Company__r.ShippingCountry',
			'Company__r.ShippingLatitude',
			'Company__r.ShippingLongitude',
			'Company__r.ShippingState'
		};
	}
	//query vacancy base on vacancy id
	public list<Placement__c> fetchVacancyListById(String id){
		list<Placement__c> vacList = new list<Placement__c>();
		try{
			String condition =  'Id =:id';
			/*
			fflib_QueryFactory qf = newQueryFactory();
			qf.selectFields(getVacancyFieldsForPeopleSearch());
			qf.selectFields(getCompanyFields());
			qf.setCondition(condition);
			qf.getOrderings().clear();
			String soqlStr = qf.toSOQL();
			//System.debug(soqlStr);
			vacList = Database.query(soqlStr);
			*/
			vacList = simpleQuery()
				.selectFields(getVacancyFieldsForPeopleSearch())
				.selectFields(getCompanyFields())
				.setCondition(condition)
				.setParam('id',id)
				.get();
		}catch(Exception e){
			System.debug('Cannot get vacancy details:'+e);
		}
		
		return vacList;
	}

	public List<Placement__c> getVacancyListAndRelatedCandidateById(String Id) {
		String condition = 'Id =: id';
		SimpleQueryFactory qf = simpleQuery()
			.selectFields(getSObjectFieldList())
			.addSubselect(Placement_Candidate__c.SObjectType,'Candidate__c,Candidate__r.Name,Shortlist__c','')
			.setCondition(condition);

		//fflib_QueryFactory qfSub = qf.subselectQuery(Placement_Candidate__c.SObjectType,true);
		//qfSub.selectFields(new List<String>{'Candidate__c','Candidate__r.Name','Shortlist__c'});

		//qf.setCondition(condition);
		String soqlStr = qf.toSOQL();
		//system.debug('soqlStr ='+ soqlStr);
		List<Placement__c> vacancies = Database.query(soqlStr);
		return vacancies;
	}

	/*
     *  Set object as Placement__c
     */
	/*private Schema.SObjectType getSObjectType(){
		return Placement__c.SObjectType;
	}*/
	public override  LIST<Schema.SObjectField> getSObjectFieldList(){
		return new List<Schema.SObjectField>{
			Placement__c.Id,
			Placement__c.Name
		};
	}

	public LIST<String> getUserFieldList() {
		return new List<String> {
				'Owner.Id',
				'Owner.Name',
				'Owner.Email'
		};
	}

	//query vacancy base on vacancy id list
	public list<Placement__c> fetchVacancyListByIdList(List<String> idList){
		list<Placement__c> vacList = new list<Placement__c>();
		String query = VACANCY_QUERY_BASE_1 + ' WHERE Id in: idList';
		checkRead('Id, RecordTypeId, RecordType.Name, Name,Company__c, Company__r.Name,'
				+'Company__r.ShippingStreet, Company__r.ShippingCity,Company__r.ShippingPostalCode,'
				+'Company__r.ShippingCountry, Company__r.ShippingLatitude,Company__r.ShippingLongitude,'
				+'Company__r.ShippingState, Start_Date__c, End_Date__c, Candidate_Charge_Rate__c, Category__c,'
				+'Specialty__c, Seniority__c, Rate_Type__c,Stage__c');
		vacList = Database.query(query);
		return vacList;
	}

	//query vacancy base on parent company id and name keyword
	public list<Placement__c> fetchVacancyListBySearchCriteria(VacancySearchCriteria vSearchCriteria){
		list<Placement__c> vacList = new list<Placement__c>();
		String likeArg = '%' + vSearchCriteria.getVacancyEntered() + '%';
		//System.debug('VacancySelector likeArg: '+likeArg);
		String companyId = vSearchCriteria.getCompanyId();
		System.debug('VacancySelector companyId: '+companyId);
		String query;
		if(companyId != '' && companyId != null){
			query = VACANCY_QUERY_BASE_1 + ' WHERE Name LIKE : likeArg AND Company__c =: companyId';
		}else{
			query = VACANCY_QUERY_BASE_1 + ' WHERE Name LIKE : likeArg';
		}
		checkRead('Id, RecordTypeId, RecordType.Name, Name,Company__c, Company__r.Name,'
				+'Company__r.ShippingStreet, Company__r.ShippingCity,Company__r.ShippingPostalCode,'
				+'Company__r.ShippingCountry, Company__r.ShippingLatitude,Company__r.ShippingLongitude,'
				+'Company__r.ShippingState, Start_Date__c, End_Date__c, Candidate_Charge_Rate__c, Category__c,'
				+'Specialty__c, Seniority__c, Rate_Type__c,Stage__c');
		vacList = Database.query(query);
		return vacList;
	}

	public list<Placement__c> fetchVacancyListByCompanyId(String companyId){
		list<Placement__c> vacList = new list<Placement__c>();
		String query = VACANCY_QUERY_BASE_1 + ' WHERE Company__c =:companyId';
		checkRead('Id, RecordTypeId, RecordType.Name, Name,Company__c, Company__r.Name,'
				+'Company__r.ShippingStreet, Company__r.ShippingCity,Company__r.ShippingPostalCode,'
				+'Company__r.ShippingCountry, Company__r.ShippingLatitude,Company__r.ShippingLongitude,'
				+'Company__r.ShippingState, Start_Date__c, End_Date__c, Candidate_Charge_Rate__c, Category__c,'
				+'Specialty__c, Seniority__c, Rate_Type__c,Stage__c');
		vacList = Database.query(query);
		return vacList;
	}

	public List<Placement__c> fetchVacnacyListByVacancyName(String vacancyName) {
		list<Placement__c> vacList = new list<Placement__c>();
		String likeArg = '%' + vacancyName + '%';
		String query = VACANCY_QUERY_BASE_1 + ' WHERE Name LIKE : likeArg';
		checkRead('Id, RecordTypeId, RecordType.Name, Name,Company__c, Company__r.Name,'
				+'Company__r.ShippingStreet, Company__r.ShippingCity,Company__r.ShippingPostalCode,'
				+'Company__r.ShippingCountry, Company__r.ShippingLatitude,Company__r.ShippingLongitude,'
				+'Company__r.ShippingState, Start_Date__c, End_Date__c, Candidate_Charge_Rate__c, Category__c,'
				+'Specialty__c, Seniority__c, Rate_Type__c,Stage__c');
		vacList = Database.query(query);
		return vacList;
	}

	private List<Schema.SObjectField> getFieldsForVacancySearch(){
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>();
		fieldList = getSObjectType().getDescribe().fields.getMap().values();
		return fieldList;
	}

	public List<Placement__c> fetchVacancyListBySearch(String query){
		SimpleQueryFactory qf = simpleQuery();
		String condition = '';
		String queryEsc = String.escapeSingleQuotes(query);
		qf.selectFields(getSObjectFieldList());
		qf.selectFields(getFieldsForVacancySearch());
		qf.setLimit(3000);
		if(String.isNotBlank(queryEsc)){
			condition += 'Name like \'%'+ queryEsc +'%\' ';
		}
		qf.setCondition(condition);
		qf.getOrderings().clear();
		qf.addOrdering('LastModifiedDate', fflib_QueryFactory.SortOrder.DESCENDING);
		String soqlStr = qf.toSOQL();
		List<Placement__c> vacancies  = Database.query(soqlStr);
		return vacancies;
	}

	/**
	 * This method is to retrieve a vacancy owner's name and email address
	 * @param vacancyId
	 * @return Placement__c
	 * Added by Lina Wei on 08/03/2017
	 */
	public Placement__c fetchVacancyOwner(String vacancyId) {
		try {
			SimpleQueryFactory qf = simpleQuery()
					.selectFields(getUserFieldList())
					.setCondition('Id =: vacancyId');
			List<Placement__c> vacList = Database.query(qf.toSOQL());
			return vacList.get(0);
		} catch (System.Exception e){
			System.debug('VacancySelector_fetchVacancyOwner: ' + e);
			return null;
		}
	}
}