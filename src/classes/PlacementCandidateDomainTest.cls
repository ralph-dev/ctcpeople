@isTest
private class PlacementCandidateDomainTest
{
	@isTest
	static void createPlacementCandidates() {
		System.runAs(DummyRecordCreator.platformUser) {
		Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();

        candidateDTO conDTO = new candidateDTO();
        conDTO.setCandidate(con);
        conDTO.setRadiusRanking(1);

        List<candidateDTO> candidateDTOs = new List<candidateDTO>{conDTO};
        PlacementCandidateDomain pCandidateDomain = new PlacementCandidateDomain();

        Map<String, Placement_Candidate__c> cmMap = pCandidateDomain.createPlacementCandidates(candidateDTOs, vac.Id);
        system.assertEquals(cmMap.size(),1);
		}
	}
	
	static testMethod void testConstruct() {
		System.runAs(DummyRecordCreator.platformUser) {
	    list<Placement_Candidate__c> cms = DataTestFactory.createCandidateManagements();
	    
	    PlacementCandidateDomain d1 = new PlacementCandidateDomain(cms);
	    PlacementCandidateDomain d2 = new PlacementCandidateDomain(cms.get(0));
	    
	    system.assertNotEquals(null, d1);
	    system.assertNotEquals(null, d2);
		}
	}
}