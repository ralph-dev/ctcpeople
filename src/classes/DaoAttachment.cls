/**
* Author: Kevin Wu
* 
* A Data access object class that used to encapsulate the data operations for Attachment object
*/

public with sharing class DaoAttachment extends CommonSelector{
	
	
	public DaoAttachment(){
		super(Attachment.sObjectType);
	}
	
	private static DaoAttachment dao = new DaoAttachment();

    public static List<Attachment> getAttachmentsByEmailTemplateId(String Id){
        List<Attachment> attachments;
        try{
        	dao.checkRead('ParentId, Name, Id, BodyLength ');
            attachments = [Select ParentId, Name, Id, BodyLength From Attachment where ParentId=:Id]; 
        }catch(Exception ex){
            attachments = new List<Attachment>();    
        }
       
        return attachments;
    }
    
    public static List<Id> getAttachmentIdsByEmailTemplateId(String emailTemplateId){
    	List<Id> attachmentsId;
    	//attachmentsId = [Select ParentId, Name, Id From Attachment where ParentId=:Id].Id; 
    	return attachmentsId;
    }
	
	public static List<Attachment> getAttachmentsDetailByAttachment(List<Attachment> previsouAttachments){
		List<Attachment> attachments;
		try{
			dao.checkRead('ParentId, Name, Id, BodyLength,Body ');
            attachments = [Select ParentId, Name, Id, BodyLength, Body From Attachment where Id in: previsouAttachments]; 
        }catch(Exception ex){
            attachments = new List<Attachment>();    
        }
       
        return attachments;
	}
}