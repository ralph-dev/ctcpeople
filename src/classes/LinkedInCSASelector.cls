/** 
 * A selector class to return the current LinkedIn CSA Setting
 * Created by: Lina
 * 
 */
public class LinkedInCSASelector extends CommonSelector{
	
	private static LinkedInCSASelector selector = new LinkedInCSASelector();
	public LinkedInCSASelector(){
		super(LinkedIn_CSA__c.sObjectType);
	}

    public static LinkedIn_CSA__c getLinkedInCSA() {
    	selector.checkRead('API_Key__c, Customer_Id__c');
        List<LinkedIn_CSA__c> csaList = [SELECT API_Key__c, Customer_Id__c From LinkedIn_CSA__c LIMIT 1];//LinkedIn_CSA__c.getall().values();
        if (csaList != null) {
            for (LinkedIn_CSA__c csa : csaList) {
                return csa;
            }
        } 
        return null;
    }

}