@isTest
private class TestInnerClass {
	public static testmethod void testDocRowRec(){
		System.runAs(DummyRecordCreator.platformUser) {
        Contact con = new Contact(LastName='AAA');
		insert con;
		ConRowRec  newcon = new ConRowRec(con);
		DocRowRec newdoc = new DocRowRec(new Web_Document__c(),newcon,0);
		DocRowRec newdoc1 = new DocRowRec(new Document(),newcon);
		DocRowRec newdoc2 = new DocRowRec(new Document(),con,userinfo.getUserId());
		DocRowRec newdoc3 = new DocRowRec(new Document(),userinfo.getUserId());
		system.assert(newdoc2.isSelected);
		}
    }
    
     public static testmethod void testConRowRec(){
     	System.runAs(DummyRecordCreator.platformUser) {
    	Account acc = new Account(Name = 'test');
    	insert acc;
        Contact con = new Contact(LastName='AAA');
		insert con;
		ConRowRec newcon = new ConRowRec(con);
		ConRowRec newcon1 = new ConRowRec(con,acc,0,'a@test.com','a@test.com' ,'a@test.com');
		system.assertEquals(newcon1.addemails, 'a@test.com');
     	}
    }
    
    public static testmethod void testWrapperFolder(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Id newid = UserInfo.getUserId();
    	WrapperFolder newWrappserFolder = new WrapperFolder(newid);
    	system.assertEquals(newWrappserFolder.folderName,  'My Personal Email Templates');
    	Id newid1 = UserInfo.getOrganizationId();
    	WrapperFolder newWrappserFolder1 = new WrapperFolder(newid1);
    	/*Folder newFolder = new Folder(type='Email', Name='test', Accesstype='public');
    	insert newFolder;
    	WrapperFolder newWrappserFolder2 = new WrapperFolder(newFolder.id);*/
    	}
    }
    
    public static testmethod void testSendResumeEmailTemplate(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	SendResumeEmailTemplate newSendResumeEmailTemplate = new SendResumeEmailTemplate();
        newSendResumeEmailTemplate.initEmailTemplate();
        List<SendResumeEmailTem> newSendResTem = newSendResumeEmailTemplate.newSendResTem;
        Map<Id,EmailTemplate> tempMap = newSendResumeEmailTemplate.tempMap;
        system.assert(newSendResTem != null);
        system.assert(tempMap != null);
    	}
        
    }
    
    public static testmethod void testGetContactJson(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	String searchcre = 'cli';    	
        String contactJson = SendResumeController.searchConAndAccSR(searchcre);       
        system.assert(contactJson != null);
        String searchcre1 = '';
        String contactJson1 = SendResumeController.searchConAndAccSR(searchcre1);       
        system.assert(contactJson1 != null);
    	}
    }
}