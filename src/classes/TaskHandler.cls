public with sharing class TaskHandler implements ITrigger {
	
    private Set<Id> tIds = new Set<Id>();
	private Map<Id, Task> conTaskMap = new Map<Id, Task>();
    private Map<Id, Task> conLastTaskMap = new Map<Id, Task>();

    public TaskHandler(){}

    public void bulkBefore(){}

  
    public void bulkAfter(){
        List<Task> taskList = new List<Task>();
        List<Id> conIds = new List<Id>();
        if(Trigger.isDelete){
             taskList = (List<Task>)trigger.old;
        }else{
             taskList = (List<Task>)trigger.new;
        }
        
        for(Task t:taskList){
            conIds.add(t.WhoId);
            if(t.Subject != 'left message' && t.Status == 'Completed'){
                tIds.add(t.Id);
            }
        }
        conLastTaskMap = TaskTriggerService.getConLastTaskMap(conIds);
    }


    public void beforeInsert(SObject so){}

    public void beforeUpdate(SObject oldSo, SObject so){}


    public void beforeDelete(SObject so){}

  
    public void afterInsert(SObject so){
        Task newTask = (Task)so;
        if(tIds.contains(newTask.Id)){
            Task latestTask = conLastTaskMap.get(newTask.WhoId);
            if(latestTask!=null){
                if(latestTask.ActivityDate <= newTask.ActivityDate){
                    conTaskMap.put(newTask.WhoId, newTask);
                }
            }else{
                conTaskMap.put(newTask.WhoId, newTask);
            }
            
        }
    }


    public void afterUpdate(SObject oldSo, SObject so){
        Task t = (Task)so;
        Task latestTask = conLastTaskMap.get(t.WhoId);
        if(tIds.contains(t.Id)){
            if(latestTask.ActivityDate <= t.ActivityDate){
               conTaskMap.put(t.WhoId, t);
            } 
        }else{
            if(latestTask == null){
                conTaskMap.put(t.WhoId, null);
            }else{
                conTaskMap.put(t.WhoId, latestTask);
            }
        }
    }

    public void afterDelete(SObject so){
        Task delTask = (Task)so;
        if(tIds.contains(delTask.Id)){
            conTaskMap.put(delTask.WhoId, conLastTaskMap.get(delTask.WhoId));
        }
    }
    

    public void afterUndelete(SObject so){}
    
    
    public void andFinally(){
        TaskTriggerService.updateContactLastActivityDate(conTaskMap);
    }
}