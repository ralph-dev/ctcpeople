@isTest
private class CTCPeopleSettingHelperSelectorTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	DataTestFactory.createCTCPeopleSettings();
    	String apAccountId=DataTestFactory.createStyleCategory().Id;
    	CTCPeopleSettingHelperSelector ctcSetting=new CTCPeopleSettingHelperSelector();
    	list<String> recordTypeValues=new list<String>();
			recordTypeValues.add('Approver_Mapping');
			recordTypeValues.add('Biller_Contacts_Mapping');
			recordTypeValues.add('Biller_Mapping');
			recordTypeValues.add('Company_Entity');
			recordTypeValues.add('Owner_Mapping');
			recordTypeValues.add('Placement_Mapping');
			recordTypeValues.add('Employee_Mapping');
			recordTypeValues.add('Rate_Card');
			recordTypeValues.add('Rate_Change');
			recordTypeValues.add('Rule_Group');
			recordTypeValues.add('LinkedIn_CSA_Registration');
			RecordTypeSelector recordSelector=new RecordTypeSelector();
			list<RecordType> recordTypes=
				recordSelector.fetchRecordTypesByName(recordTypeValues);
			list<String> rTIdValues=new list<String>();
			for(RecordType rTRecord: recordTypes){
				rTIdValues.add(rTRecord.Id);
			}
    	list<CTCPeopleSettingHelper__c> hepers=ctcSetting.fetchCTCPeopleHelperSettingByRT(rTIdValues.get(0));
    	system.assertEquals(hepers.size(),1);
    	list<CTCPeopleSettingHelper__c> hepers2=ctcSetting.fetchCTCPeopleHelperSettingByRT(rTIdValues.get(0),apAccountId);
    	system.assertEquals(hepers2.size(),1);
    	
    	DataTestFactory.createResumeAndFilesDocTypeMap();
    	String recordTypeName = 'Resume_And_Files_Doc_Type_Mapping';
    	String objName = 'Test';
    	list<CTCPeopleSettingHelper__c> helpers3 = ctcSetting.fetchResAndFilesDocTypeListByRTAndObjName(recordTypeName, objName);
    	System.assertEquals(helpers3.size(), 1);
    	}
    	
    }
}