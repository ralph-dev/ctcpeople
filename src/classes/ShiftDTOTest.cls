@isTest
private class ShiftDTOTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        ShiftDTO sDTO = TestDataFactoryRoster.createShiftDTO(acc,vac,shift);
        
        System.assertNotEquals(sDTO.getStartDate(),'');
        System.assertNotEquals(sDTO.getsTime(),'');
        System.assertNotEquals(sDTO.getEndDate(),'');
        System.assertNotEquals(sDTO.geteTime(),'');
        System.assertNotEquals(sDTO.getsRequiredPositionNumber(),'');
        System.assertNotEquals(sDTO.getsPlacedPositionNumber(),'');
        System.assertNotEquals(sDTO.getsVacId(),'');
        System.assertNotEquals(sDTO.getsCompanyId(),'');
        System.assertNotEquals(sDTO.getsType(),'');
        System.assertNotEquals(sDTO.getsId(),'');
        System.assertNotEquals(sDTO.getsWeeklyRecur(),false);
        System.assertNotEquals(sDTO.getsRecurEndDate(),'');
        System.assertNotEquals(sDTO.getsWeekdaysDisplay(),'');
        System.assertNotEquals(sDTO.getsShiftStatus(),'');
        System.assertNotEquals(sDTO.getsVacancyName(),'');
    	}
    }
}