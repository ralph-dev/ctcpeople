/**
A class to controll triggers of People Cloud
only two triggers are controlled by this class.

All setting info will be saved in a document file with name started with TriggerAdmin

**/
public with sharing class TriggerAdminController{
    TriggerDetail theTriggerDetails = new TriggerDetail();
    String develperName = 'Trigger_Admin';  
    public List<TriggerDetail.DetailRecord> detailrecords {get;set;}
    public String info{get;set;}
    public void init(){
    	try{
    		theTriggerDetails.getTriggerDetails();
            detailrecords = theTriggerDetails.getDetails();
        }
        catch(Exception ex){
        	System.debug(ex);
         //  ApexPages.addMessages(ex);
         }
	}
    public void updateTriggers(){
    	 writeXMLfile();
   	}
    public void writeXMLfile(){
    	try{
	    	Document dd;
	        dd = DaoDocument.getCTCConfigFileByDevName(develperName);
	        XmlStreamWriter w = new XmlStreamWriter();
	        w.writeStartDocument(null, '1.0');
	        w.writeComment('file body starts here');
	        w.writeStartElement(null, 'fileroot', null);
	        for(TriggerDetail.DetailRecord dr: detailrecords)   {
	            w.writeStartElement(null, 'trigger' , null);
	            w.writeStartElement(null, 'apiname', null);    
	            w.writeCharacters(dr.apiName);
	            w.writeEndElement(); // end server
	            w.writeStartElement(null, 'targetObject', null);    
	            w.writeCharacters(dr.targetObject);
	            w.writeEndElement(); // end key
	            w.writeStartElement(null, 'status', null);    
	            if(dr.Active)
	               {w.writeCharacters('Active');}
	            else
	               {w.writeCharacters('InActive');}
	            w.writeEndElement(); // end key
	            w.writeStartElement(null, 'accountid', null);    
	            w.writeCharacters(dr.AccountId);
	            w.writeEndElement(); // end accountId
	            w.writeStartElement(null, 'recordtypeid', null);    
	            w.writeCharacters(dr.ConRecordTypeId);
	            w.writeEndElement(); // end recordtypeid
	            w.writeStartElement(null,'note', null);            
	            w.writeCData(dr.Note);            
	            w.writeEndElement(); // end component
	            w.writeEndElement(); // end trigger
	         }
	         w.writeEndElement(); // end fileroot
	         String xmlOutput = w.getXmlString();
	         dd.Body = Blob.valueOf(xmlOutput);
	         fflib_SecurityUtils.checkFieldIsUpdateable(Document.SObjectType, Document.Body);
	         update dd;
	         info = 'Updated Successfully!';
	    }
	    catch(Exception e)
	    {
	        info = 'Updated Unsuccessfully! Please contact support@clicktocloud.com or log an issue from Dream Cloud. ';
	    }
	}

	public static testMethod void test1(){
		System.runAs(DummyRecordCreator.platformUser) {
	    TriggerAdminController thecontroller = new TriggerAdminController();
	    thecontroller.init();
	    system.assert(thecontroller.detailrecords.size()>0);
	    thecontroller.updateTriggers();
		system.assertEquals(thecontroller.info, 'Updated Successfully!');
		}
	}
}