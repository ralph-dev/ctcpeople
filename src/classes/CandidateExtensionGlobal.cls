public with sharing class CandidateExtensionGlobal {
	public CandidateExtensionGlobal(Object obj){}
	
	@RemoteAction
	public static Contact getCandidate(String Id){
		ContactSelector conSelector = new ContactSelector();
		List<Contact> conList = conSelector.fetchContactInfoById(Id);
		if(conList == null || conList.size() == 0){
			return null;
		}
		return conList[0];
	}
	
	@RemoteAction
	public static List<Contact> searchCandidate(String nameKeyword){
		ContactSelector cSelector = new ContactSelector();
		List<Contact> conList = cSelector.fetchContactListByNameKeyword(nameKeyword);
		if(conList == null || conList.size() == 0){
			return null;
		}
		return conList;
	}
}