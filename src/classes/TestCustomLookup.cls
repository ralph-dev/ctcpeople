@isTest
private class TestCustomLookup {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // TO DO: implement unit test
        Account acc=new Account();
        acc.name='TestAccount for scheduled test';
        insert acc;
        RecordType candRT = DaoRecordType.indCandidateRT;
        List<Contact> cands=new List<Contact>();
        for(Integer i=0;i<200;i++){
            Contact cand = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1'+i,Email='Cand1'+i+'@testcanddispatcher.ctc.test',Accountid=acc.id);
            cands.add(cand);
        }
        insert cands;
        Apexpages.currentPage().getParameters().put('oN','Contact');
        Apexpages.currentPage().getParameters().put('fN','Id');
         Apexpages.currentPage().getParameters().put('idrecorder','3');
        CustomLookup custo=new CustomLookup();        
        System.assertEquals(custo.currentpage, 1);
        custo.onNext();
        System.assertEquals(custo.currentpage, 2);
        custo.onPrevious();        
        custo.searchByName='cand1';
        custo.onSearch(); 
        custo.onCheck();
        System.assertEquals(custo.currentpage, 1);
        
        
    	}
        
        
        
    }
}