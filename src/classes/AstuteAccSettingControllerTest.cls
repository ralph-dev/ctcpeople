@isTest
public class AstuteAccSettingControllerTest {
    @testSetup static void setup(){
        StyleCategory__c sc = new StyleCategory__c(Name='test astute');
        insert sc;
    }
    
    @isTest
    static void testAstuteAccSettingController(){
        system.runAs(DummyRecordCreator.platformUser){
            StyleCategory__c sc = [Select Id From StyleCategory__c Where Name = 'test astute']; 
            ApexPages.StandardController sctrl = new ApexPages.StandardController(sc);
            AstuteAccSettingController aasc = new AstuteAccSettingController(sctrl);
            
            aasc.userName = 'test';
       		aasc.setPassword('123');
            aasc.setApiKey('123');
            aasc.save();
            System.assertEquals(null, aasc.getpassword());
            PageReference pageRef = aasc.cancel();
            String scID = sc.Id;
            System.assert(pageRef.getUrl().contains(scID.substring(0, 15)));
        }
    }
    
}