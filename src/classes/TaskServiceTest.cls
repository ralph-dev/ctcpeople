@isTest
private class TaskServiceTest
{
	static{
        TriggerHelper.disableAllTrigger();
    }
    
	@isTest
	static void createAndDeleteRecordsTest() {
		DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
		
		System.runAs(DummyRecordCreator.platformUser) {
		String clientId = rs.clients[0].Id;

		List<Map<String, Object>> tasksMap = new List<Map<String, Object>>();
		Map<String, Object> taskMap = new Map<String, Object>();

		DateTime dateTimeNow = dateTime.now();
		Decimal unixTime = dateTimeNow.getTime()/1000;

		taskMap.put('Subject','Test Subject');
		taskMap.put('WhatId ' , clientId);
		taskMap.put('Status' ,'Completed');
		taskMap.put('ActivityDate' , unixTime);
		taskMap.put('OwnerId' ,UserInfo.getUserId());
		taskMap.put('Type' ,'call');
		taskMap.put('Priority','Normal');
		tasksMap.add(taskMap);
		TaskService tService = new TaskService();
		List<Task> newTasks = tService.createRecords(tasksMap);

		system.assertEquals(newTasks.size(),1);

		List<Map<String,Object>> deleteMap = new List<Map<String,Object>>();
		Map<String, Object> deleteTaskMap = new Map<String, Object>{'Id'=>newTasks[0].Id};
		system.debug('newTasks ='+ newTasks);
		deleteMap.add(deleteTaskMap);
		List<Id> tasksId = tService.deleteTaskRecords(deleteMap);
		system.assertEquals(tasksId[0], newTasks[0].id);
		}
	}
}