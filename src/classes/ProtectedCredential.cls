global with sharing class ProtectedCredential{
        public static final String PROVIDER_LINKEDIN = 'linkedin';
    	public static final String PROVIDER_INDEED = 'indeed';
    	public static final String PROVIDER_JXT = 'JXT';
    	public static final String PROVIDER_CAREERONE = 'careerone';
    	public static final String PROVIDER_ASTUTE = 'astute';
    	public static final String PROVIDER_AMAZON = 'amazon';
    	
    	public static final String CREDENTIAL_AD_LINKEDIN = 'linkedin'; 
    	public static final String CREDENTIAL_AD_INDEED = 'indeed'; 
    	public static final String CREDENTIAL_AD_JXT_NZ = 'JXT_NZ'; 
    	public static final String CREDENTIAL_AD_CAREERONE = 'careerone'; 
    	public static final String CREDENTIAL_PAYROLL_ASTUTE = 'astutepayroll'; 
    	public static final String CREDENTIAL_SQS_AMAZON = 'amazonsqs'; 
    	
    	
    	public static final String TYPE_ADVERTISEMENT = 'advertisement'; 
    	public static final String TYPE_PAYROLL = 'payroll'; 
    	public static final String TYPE_SQS = 'sqs'; 
    	
    	public static final String RELATED_TO_STYLECATEGORY = 'StyleCategory__c'; 
    	public static final String RELATED_TO_ADVERTISEMENTCONFIG = 'Advertisement_Configuration__c'; 
    	public static final String RELATED_TO_OAUTHSERVICE = 'OAuth_Service__c';
    	public static final String RELATED_TO_OAUTHTOKEN = 'OAuth_Token__c';
        
        
        public String id;
        public String name;
        public String relatedTo;
    	public String type;//advertisement 
    	public String provider;
    	public String username {get;set;}
    	public String password{get;set;}
    	public String token{get;set;}
    	public String key{get;set;}
    	public String secret{get;set;}
    	
        
    }