@isTest
private class TestTriggerOnWorkplace {

    static testMethod void testTriggerOnWorkplace() {
    	System.runAs(DummyRecordCreator.platformUser) {
         list<Workplace__c> workplaces=DataTestFactory.createWorkplaces();
          list<String> workplaceIds=new list<String>();
         for(Workplace__c wKplace: workplaces){
             wKplace.Workplace_Id__c=5;
             wKplace.Is_Pushing_To_AP__c=true;
             workplaceIds.add(wKplace.Id);
         }
         update workplaces;
        
         list<Workplace__c> wKPlacesUpdated=new list<Workplace__c>();
         wKPlacesUpdated=[select id, Astute_Payroll_Upload_Status__c from workplace__c where id in: workplaceIds];
         for(Workplace__c wKplace: wKPlacesUpdated){
          System.assertEquals(wKplace.Astute_Payroll_Upload_Status__c , 'On Hold');
         
         }
    	}
        
    }
}