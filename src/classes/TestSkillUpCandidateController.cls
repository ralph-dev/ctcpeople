@isTest
private class TestSkillUpCandidateController {
    
    static testMethod void testGoBackButton(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	SkillUpCandidateController suc = new SkillUpCandidateController();
    	
    	
    	PageReference test = null;
    	test = suc.goBackToPreviousPage();	
    	
    	System.assertNotEquals(null, test);
    	}
    
    }
    
    static testMethod void testRetrieveSkillGroupsJSON() {
    	System.runAs(DummyRecordCreator.platformUser) {
        //SkillUpCandidateController.retrieveSkillGroupsJSON();
        System.assertEquals(false, String.isEmpty(SkillUpCandidateController.retrieveSkillGroupsJSON()));
    	}
    }
    
    static testMethod void testRetrieveSkillsJSON(){
    	System.runAs(DummyRecordCreator.platformUser) {
        RecordType candRT=[select Id from RecordType where SobjectType = 'Contact' limit 1];
        Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1');
        insert cand1;
        
        System.assertEquals(1, [select count() from Contact where Id =: cand1.Id]);
        
        //SkillUpCandidateController.retrieveSkillsJSON(cand1.id);
        
        System.assertEquals(false, String.isEmpty(SkillUpCandidateController.retrieveSkillsJSON(cand1.id)));
    	}
        
    }
    
    static testMethod void testInsertNewSkills(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	RecordType candRT=[select Id from RecordType where SobjectType = 'Contact' limit 1];
        Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1');
        insert cand1;
    	
    	Skill_Group__c skillgroup = new Skill_Group__c(Name='skillgroup');
    	insert skillgroup;
    	    	
    	Skill__c skill1 = new Skill__c(Name='skill1', Skill_Group__c=skillgroup.Id, Ext_Id__c='ext1');
    	Skill__c skill2 = new Skill__c(Name='skill2', Skill_Group__c=skillgroup.Id, Ext_Id__c='ext2');
    	insert skill1;
    	insert skill2;
    	
    	List<String> skillIdList = new List<String>();
    	skillIdList.add(skill1.id);
    	skillIdList.add(skill2.id);
    	
    	SkillUpCandidateController.insertNewSkillsForCandidate(cand1.Id, skillIdList);
    	
    	List<Candidate_Skill__c> skillListFromCandidate = new List<Candidate_Skill__c>();
    	skillListFromCandidate = [select Skill__c from Candidate_Skill__c where Candidate__c =: cand1.Id and Skill__c IN :skillIdList];
    	
    	system.assertEquals(skillListFromCandidate.size(),skillIdList.size());
    	}
    	
    }
    

}