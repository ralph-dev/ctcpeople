@isTest
private class VacancyExtensionTest
{
	@isTest
	static void testVacancyExtension()
	{
		Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        VacancySearchCriteria searchCriteria = TestDataFactoryRoster.createVacancySearchCriteria(acc);
        String searchCriteriaStr = JSON.serialize(searchCriteria);
        VacancyDTO vacDTO =  TestDataFactoryRoster.createVacancyDTO(acc,vac);
        String vacDTOJson = JSON.serialize(vacDTO);
        
        System.runAs(DummyRecordCreator.platformUser) {
	        //test VacancyExtension getVacancy(vacancyId) method
	        Placement__c vac1 = VacancyExtension.getVacancy(vac.Id);
	        System.assertNotEquals(vac1,null);
	        
	        //test VacancyExtension searchVacancy(searchCriteria) method
	        String vacDTOListStr = VacancyExtension.searchVacancy(searchCriteriaStr);
	        System.assertNotEquals(vacDTOListStr,'');
	        
	        //test VacancyExtension upsertVacancy(String vacDTOJson) method
	        String vacDTOListDisplay = VacancyExtension.upsertVacancy(vacDTOJson);
	        System.assertNotEquals(vacDTOListDisplay,'');
	        
	        //test VacancyExtension describeVacancy() method
	        Map<String,List<Schema.PicklistEntry>> describeResult = VacancyExtension.describeVacancy();
	        System.assertNotEquals(describeResult,null);
	        
	        //test VacancyExtension fetchRecordTypeInfo() method
	        String recordTypeInfo = VacancyExtension.fetchRecordTypeInfo();
	        System.assertNotEquals(recordTypeInfo,'');
	
	        Placement__c vac2 = VacancyExtension.getVacancyAndRelatedCandidates(vac.Id);
	        System.assertNotEquals(vac1,null);
	
	        Map<String, RemoteAPIDescriptor.RemoteAction> maps = VacancyExtension.apiDescriptor();
	        System.assert(maps.size()>1);
        }
	}
}