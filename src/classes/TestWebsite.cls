@isTest
public with sharing class TestWebsite {

	static {
		System.runAs(DummyRecordCreator.platformUser) {
        List<Skill_Group__c> skillGroups = new List<Skill_Group__c>();
        skillGroups.add(new Skill_Group__c(Name='Education/Quals', Enabled__c=true, Default__c=false, Skill_Group_External_Id__c='G000001'));
        skillGroups.add(new Skill_Group__c(Name='Industry', Enabled__c=true, Default__c=false, Skill_Group_External_Id__c='G000002'));
        skillGroups.add(new Skill_Group__c(Name='Position', Enabled__c=true, Default__c=false, Skill_Group_External_Id__c='G000003'));
        skillGroups.add(new Skill_Group__c(Name='Analysis / Consulting Skill', Enabled__c=true, Default__c=false, Skill_Group_External_Id__c='G000004'));
        skillGroups.add(new Skill_Group__c(Name='Business Services /  NON IT Skill', Enabled__c=true, Default__c=false, Skill_Group_External_Id__c='G000005'));
        skillGroups.add(new Skill_Group__c(Name='Candidate Hot List Skill', Enabled__c=true, Default__c=false, Skill_Group_External_Id__c='G000006'));
        skillGroups.add(new Skill_Group__c(Name='Development Services Skill', Enabled__c=true, Default__c=false, Skill_Group_External_Id__c='G000007'));
        skillGroups.add(new Skill_Group__c(Name='Infrastructure Services Skill', Enabled__c=true, Default__c=false, Skill_Group_External_Id__c='G000008'));
        skillGroups.add(new Skill_Group__c(Name='Project Services Skill', Enabled__c=true, Default__c=false, Skill_Group_External_Id__c='G000009'));
        skillGroups.add(new Skill_Group__c(Name='QA testing Skill', Enabled__c=true, Default__c=true, Skill_Group_External_Id__c='G000010'));
        insert skillGroups;
        
        //RecordType type = new RecordType(Name = 'ApplicationFormStyle', DeveloperName ='ApplicationFormStyle');
        RecordType type = [select Id from RecordType where DeveloperName = 'ApplicationFormStyle'];
        //insert type;
        
        List<StyleCategory__c> styleCategories = new List<StyleCategory__c>();
        styleCategories.add(new StyleCategory__c(Name='new test', Active__c = true, RecordTypeId = type.Id));
        styleCategories.add(new StyleCategory__c(Name='test', Active__c = true, RecordTypeId = type.Id));
        insert styleCategories;
        
        PCAppSwitch__c pcSwitch = new PCAppSwitch__c();
        pcSwitch.Enable_Daxtra_Skill_Parsing__c = true;
        insert pcSwitch;
		}
    }

	public testMethod static void testWebsiteEdit(){
		System.runAs(DummyRecordCreator.platformUser) {
		PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
        pcSwitch.Enable_Daxtra_Skill_Parsing__c = true;
        update pcSwitch;
        
		StyleCategory__c sc = new StyleCategory__c();
		sc.Name='a';
		insert sc;
		Placement__c v = new Placement__c();
		insert v;
		Advertisement__c ad = new Advertisement__c();
		ad.WebSite__c='a';	 ad.Location__c='a'; 	ad.SearchArea_Label_String__c='a';
		ad.Classification__c='a'; 	ad.Vacancy__c=v.id; 	ad.SubClassification2__c='a';
		ad.Classification2__c='a';	ad.SubClassification_Seek_Label__c='a';	 ad.WorkType__c='a';
		ad.Reference_No__c='a';		ad.Job_Title__c='a';		ad.Company_Name__c='a';
		ad.Template__c='a';		ad.Application_Form_Style__c=sc.id;	ad.Bullet_1__c='a';		
		ad.Bullet_2__c='a';		ad.Bullet_3__c='a';			ad.Job_Type__c='a';
		ad.JXT_Short_Description__c='a';	ad.jxt_sectors__c='a';		ad.Hot_Job__c=true;
		ad.Residency_Required__c=true;		ad.Search_Tags__c='a';		ad.Street_Adress__c='a';
		ad.Nearest_Transport__c='a';		ad.Job_Contact_Name__c='a';		ad.Job_Contact_Phone__c='12345';
		ad.Location_Hide__c =false;			ad.Salary_Type__c='a';			ad.SeekSalaryMin__c='123';
		ad.SeekSalaryMax__c='2353'; 		ad.Salary_Description__c='a';	ad.No_salary_information__c=false;
		ad.Job_Content__c='a';			ad.Has_Referral_Fee__c='a';	ad.Referral_Fee__c=500;
		ad.Terms_And_Conditions_Url__c=Endpoint.getPeoplecloudWebSite()+'';
		ad.online_footer__c = 'If you are interest, pleaes apply now!';
		ad.Reference_No__c = '100285A';
		ad.Industry__c='abc';
		insert ad;
		WebSiteEdit website = new WebSiteEdit(new ApexPages.StandardController(ad));
		system.assert(website.enableSkillGroupList.size()>0);
		website.iec2ws = new EC2WebServiceTestImpl();
		List<SelectOption> getStyleOptions = website.getStyleOptions();
		system.assert(getStyleOptions.size()>0);
		PageReference website_save = website.website_save();
		system.assert(website_save!= null);
		
		List<SelectOption> skillGroupSelectOption = website.getSelectSkillGroupList();
		system.assertEquals(skillGroupSelectOption.size(),10);
		}
	}
	
	public testMethod static void testWebsitePost(){
		System.runAs(DummyRecordCreator.platformUser) {
		PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
        pcSwitch.Enable_Daxtra_Skill_Parsing__c = true;
        update pcSwitch;
        
		StyleCategory__c sc = new StyleCategory__c();
		sc.Name='a';
		insert sc;
		Placement__c v = new Placement__c();
		insert v;
		Advertisement__c ad = new Advertisement__c();
		ad.WebSite__c='a';	 ad.Location__c='a'; 	ad.SearchArea_Label_String__c='a';
		ad.Classification__c='a'; 	ad.Vacancy__c=v.id; 	ad.SubClassification2__c='a';
		ad.Classification2__c='a';	ad.SubClassification_Seek_Label__c='a';	 ad.WorkType__c='a';
		ad.Reference_No__c='a';		ad.Job_Title__c='a';		ad.Company_Name__c='a';
		ad.Template__c='a';		ad.Application_Form_Style__c=sc.id;	ad.Bullet_1__c='a';		
		ad.Bullet_2__c='a';		ad.Bullet_3__c='a';			ad.Job_Type__c='a';
		ad.JXT_Short_Description__c='a';	ad.jxt_sectors__c='a';		ad.Hot_Job__c=true;
		ad.Residency_Required__c=true;		ad.Search_Tags__c='a';		ad.Street_Adress__c='a';
		ad.Nearest_Transport__c='a';		ad.Job_Contact_Name__c='a';		ad.Job_Contact_Phone__c='12345';
		ad.Location_Hide__c =false;			ad.Salary_Type__c='a';			ad.SeekSalaryMin__c='123';
		ad.SeekSalaryMax__c='2353'; 		ad.Salary_Description__c='a';	ad.No_salary_information__c=false;
		ad.Job_Content__c='a';			ad.Has_Referral_Fee__c='a';	ad.Referral_Fee__c=500;
		ad.Terms_And_Conditions_Url__c=Endpoint.getPeoplecloudWebSite()+'';
		ad.online_footer__c = 'If you are interest, pleaes apply now!';
		ad.Reference_No__c = '100285A';
		ad.Industry__c='abc';
		insert ad;
		WebSitePost website = new WebSitePost(new ApexPages.StandardController(ad));
		system.assert(website.enableSkillGroupList.size()>0);
		website.iec2ws = new EC2WebServiceTestImpl();
		List<SelectOption> getStyleOptions = website.getStyleOptions();
		system.assert(getStyleOptions.size()>0);
		List<SelectOption> getSelectSkillGroupList = website.getSelectSkillGroupList();	
		system.assert(getSelectSkillGroupList.size()>0);
		PageReference website_save = website.website_save();
		system.assert(website_save == null);
		PageReference insertDBpage =  website.insertDB();
		system.assert(insertDBpage == null);
		}
	}
	
	private testMethod static void testGetDefaultSkillGroups() {
		System.runAs(DummyRecordCreator.platformUser) {
	    WebSitePost wSitePost = new WebSitePost();
	    List<Skill_Group__c> skillGroups = new List<Skill_Group__c>();
	    skillGroups = [Select Id, Name, Enabled__c, Default__c, Skill_Group_External_Id__c from Skill_Group__c];
	    List<String> defaultSkillGroups = wSitePost.getDefaultSkillGroups(skillGroups);
	    system.assertEquals(defaultSkillGroups.size(),1);
		}
	}
}