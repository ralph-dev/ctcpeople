public with sharing class VacancyExtension {
	public VacancyExtension(Object obj){}
    
	@RemoteAction
    public static Placement__c getVacancy(String vacancyId){
    	VacancySelector vacSelector = new VacancySelector();
		list<Placement__c> vacList = vacSelector.fetchVacancyListById(vacancyId);
		if(vacList.size() == 0 || vacList == null){
			return null;
		}
		//System.debug('VacancyExtensionGlobal getVacancy vacList: '+JSON.serialize(vacList.get(0)));
		return vacList.get(0);
    }
    
    @RemoteAction
    public static Placement__c getVacancyAndRelatedCandidates(String vacancyId) {
        VacancySelector vacSelector = new VacancySelector();
        list<Placement__c> vacList = vacSelector.getVacancyListAndRelatedCandidateById(vacancyId);
        if(vacList.size() == 0 || vacList == null){
            return null;
        }
        return vacList.get(0);
    }
    
    @RemoteAction
    public static String searchVacancy(String searchCriteria){ 
    	VacancySearchCriteria vSearchCriteria = (VacancySearchCriteria)JSON.deserialize(searchCriteria,VacancySearchCriteria.class);
    	VacancySelector vacSelector = new VacancySelector();
		List<Placement__c> vacList = vacSelector.fetchVacancyListBySearchCriteria(vSearchCriteria);
		List<VacancyDTO> vacDTOList = VacancyHelper.convertVacancyListToVacancyDTO(vacList);
		return JSON.serialize(vacDTOList);
    }
    
    @RemoteAction
    public static List<Placement__c> searchVacancyByName(String vacancyName) {
        return new VacancySelector().fetchVacnacyListByVacancyName(vacancyName);
    }
    
    /*
    *	upsert vacancy
    */
    @RemoteAction
    public static String upsertVacancy(String vacDTOJson){
    	List<VacancyDTO>  vacDTOList = VacancyHelper.deserializeVacancyDTOJson(vacDTOJson);
    	
    	VacancyService vService = new VacancyService();
    	List<Placement__c> vacList = vService.upsertVacancyFromWizard(vacDTOList);
    	
    	if(vacList.size() == 0 || vacList == null){
			return null;
		}
		
		List<VacancyDTO> vacDTOListDisplay = VacancyHelper.convertVacancyListToVacancyDTO(vacList);
		
    	return JSON.serialize(vacDTOListDisplay.get(0));
    }
    
    /**
    *	Get the picklist values of Specialty__c, Seniority__c, Category__c
    *	Create a new Map by using  Specialty as key to avoid namespace problem in the front end
    *
    **/
    @RemoteAction
     public static Map<String,List<Schema.PicklistEntry>> describeVacancy(){
     	//System.debug('VacancyExtensionGlobal describeVacancy : ' + DescribeHelper.getDescribeFieldResult(Placement__c.sObjectType.getDescribe().fields.getMap().values()));
        Map<String, Schema.DescribeFieldResult> resultMap = DescribeHelper.getDescribeFieldResult(Placement__c.sObjectType.getDescribe().fields.getMap().values());
    	Map<String,List<Schema.PicklistEntry>> describeResultMap = new Map<String,List<Schema.PicklistEntry>>();
        describeResultMap.put('Specialty',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Specialty__c').picklistValues);
        describeResultMap.put('Seniority',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Seniority__c').picklistValues);
        describeResultMap.put('Category',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Category__c').picklistValues);
        describeResultMap.put('RateType',resultMap.get(PeopleCloudHelper.getPackageNamespace()+'Rate_Type__c').picklistValues);
        return describeResultMap;
    }
    
    /**
    *	Get the record type of Placement__c
    *
    **/
    @RemoteAction
    public static String fetchRecordTypeInfo(){
  		Schema.DescribeSObjectResult R = Placement__c.SObjectType.getDescribe();
  		List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
        return JSON.serialize(RT);
    }
    
    /**
     *  Provide additional information on the remote action API
     * */
    @RemoteAction
    public static Map<String, RemoteAPIDescriptor.RemoteAction> apiDescriptor(){
        RemoteAPIDescriptor descriptor = new RemoteAPIDescriptor();
        descriptor.description('CRUD & describe remote action for Search.');
        
	    descriptor.action('getVacancy').param(String.class);
        descriptor.action('searchVacancy').param(String.class);
        descriptor.action('upsertVacancy').param(String.class);
        descriptor.action('searchVacancyByName').param(String.class);
        descriptor.action('getVacancyAndRelatedCandidates').param(String.class);
        descriptor.action('describeVacancy');
        descriptor.action('fetchRecordTypeInfo');
        return descriptor.paramTypesMap;
    } 
}