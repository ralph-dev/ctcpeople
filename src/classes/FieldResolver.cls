public abstract with sharing class FieldResolver {
	public abstract List<String> getDisplayFieldsInStringList(String id);
	public abstract List<Schema.DescribeFieldResult> getDisplayFields(String id);
}