/*
 *  Description: This is a controller to initialise authentication for applications
 *  Author: Kevin Wu
 *  Date: 21.04.2015
 */
public with sharing class AppsAuthController {
  public Boolean sfAppAuth{get;set;}
  public Boolean sfConsumerAuth{set;get;}
  public AppsAuthController() {
      sfAppAuth = false;
    sfConsumerAuth=true;
  }

  // authenticate SF web connector app
  public PageReference authSFWebConnectorApp(){
    String authUrl = AuthService.authSFWebConnectorApp();
    //system.debug('Web Connector authUrl=' + authUrl);
    return new PageReference(authUrl);
  }

  //Fill in oauth info
  public PageReference fillInConsumerInfo(){
    PageReference currentPage=ApexPages.CurrentPage();
    String appsAuthUrl=currentPage.getURL();
    PageReference consumerInfoPage=Page.OauthTokenAdminPage;
    consumerInfoPage.getParameters().put('redirectUrl',appsAuthUrl);
    return consumerInfoPage;
  }

  public PageReference reset(){
    // Remove the current settings.
    SFOauthToken__c sfOauthToken = SFOauthTokenSelector.querySFOauthToken('oAuthToken');
    delete sfOauthToken;
    // Redirect page to fill the consumer info.
    return fillInConsumerInfo();
  }
  
  public void checkSFAuth(){
    SFOauthToken__c sfOauthToken = SFOauthTokenSelector.querySFOauthToken('oAuthToken');
    if(sfOauthToken.ConsumerKey__c!=null &&
            sfOauthToken.SecretKey__c!=null)
          sfConsumerAuth=false;  
    if(sfOauthToken.OAuthToken__c!=null &&
      sfOauthToken.RefreshToken__c!=null){
      checkSFRefreshToken();
    }
  }
  
  private void checkSFRefreshToken(){
  //Check if the access token is active or not 
    SFOauthTokenService sfOauthService = new SFOauthTokenService();
      HttpResponse returnHttpRes = sfOauthService.getAccessToken();
      if(returnHttpRes.getStatusCode() == 200) {
      	//Commented by Ralph 14/09/2017. Don't allow DML operation when page is loaded.
        //SFOauthTokenService sfOauthTokenService = new SFOauthTokenService();
        //if(sfOauthTokenService.saveTokentoCustomSetting(returnHttpRes.getBody()))
          sfAppAuth = true;  
      }
  }
  
  // check any existing OAuth details in custom settings
  public void checkAppsAuth(){
    checkSFAuth();
  }
}