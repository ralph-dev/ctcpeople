@isTest
private class ShiftServiceTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
       Account acc = TestDataFactoryRoster.createAccount();
       Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
       Shift__c shift = TestDataFactoryRoster.createShift(vac);
       ShiftDTO sDTO = TestDataFactoryRoster.createShiftDTO(acc,vac,shift);  
       List<ShiftDTO> sDTOList = new List<ShiftDTO>();
       sDTOList.add(sDTO);
       
       ShiftService service = new ShiftService();
       
       //test method ShiftService getDisplayShifts(List<Shift__c> inputShiftList)
       List<Shift__c> inputShiftList = TestDataFactoryRoster.createShiftListForSplitShiftFunction(acc,vac);
       List<Shift__c> shiftsForDisplay = service.getDisplayShifts(inputShiftList);
       System.assertNotEquals(shiftsForDisplay.size(),0);
       
       //test method ShiftService upsertShiftsFromWizard(List<ShiftDTO>)
       List<Shift__c> shiftCreated = service.upsertShiftsFromWizard(sDTOList);
       System.assertEquals(shiftCreated.size(),0);
       
       //test method ShiftService updateShiftStoredInDB(String shiftJson>)
       List<Shift__c> shiftUpdated = service.updateShiftStoredInDB(sDTOList);
       System.assertEquals(shiftUpdated.size(),0);
       
       //test method ShiftService deleteShiftById(String shiftId>)
       Boolean isSuccessDeleted = service.deleteShiftById('abc');
       System.assertNotEquals(isSuccessDeleted,true);
       
       //test method ShiftService groupOpenShiftsForClient(String clientId)
       Map<String,List<ShiftDTO>> vacancyShiftDTOsMap = service.groupOpenShiftsForClient(acc.Id);
       System.assertNotEquals(vacancyShiftDTOsMap,null);
       
       //test method ShiftService getOpenShiftsForClient(String clientId)
       List<ShiftDTO> openShiftDTOListForClient = service.getOpenShiftsForClient(acc.Id);
       System.assertNotEquals(openShiftDTOListForClient, null);
       
       //test method ShiftService getOpenShiftsStoredInDatabase(String clientId)
       List<ShiftDTO> openShiftDTOListForClientStoredInDB = service.getOpenShiftsStoredInDatabase(acc.Id);
       System.assertNotEquals(openShiftDTOListForClientStoredInDB, null);
       
       //test method ShiftService getShiftDBByShiftId(String shiftId)
       List<ShiftDTO> shiftDTOList = service.getShiftDBByShiftId(shift.Id);
       System.assertNotEquals(shiftDTOList, null);
    	}
    }
}