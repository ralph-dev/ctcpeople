@isTest
private class JXTContentTest {

    static testMethod void testJXTContent() {
       
        JXTContent c= new JXTContent();
        
        List<CVContent> files = new List<CVContent>();
        
        System.runAs(DummyRecordCreator.platformUser) {
            c= new JXTContent('test@test.com', 'test firstName', 'test lastName', 'test@emailAddress.com', 'test adId', 'test vacId', 'test vacOwnerId', '', files);
            
            System.assertEquals('test firstName', c.firstName);
        }
    }
}