public with sharing class BulkEmailConRowRec {
    public Contact beConRowRec{get;set;}
    public Boolean selected{get;set;}
    public String ContactID{get;set;}
    
    public BulkEmailConRowRec(Contact c, Boolean sel){
        beConRowRec = c;
        selected = sel;
    }
    
    public BulkEmailConRowRec(String cid, Boolean sel){
        ContactID = cid;
        selected = sel;
    }     
}