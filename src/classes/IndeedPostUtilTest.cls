@isTest
private class IndeedPostUtilTest
{
	static testMethod void testCheckIndeedAccountEnable(){
		system.runAs(DummyRecordCreator.platformUser) {
			Advertisement_Configuration__c tempIndeedAccount  = AdConfigDummyRecordCreator.createDummyIndeedAccount();
			User userRec=[select id,Indeed_Account__c from User where id =: userinfo.getUserId()];
			userRec.Indeed_Account__c=tempIndeedAccount.Id;
			update userRec;
			System.assertEquals(IndeedPostUtil.checkIndeedAccountEnable(),true);
		}
	}  
	@isTest
	public static void testindeedInPost(){
		system.runAs(DummyRecordCreator.platformUser) {
			Advertisement__c testad = PeopleAdMapperTest.createTestAd();
			Test.setCurrentPage(Page.IndeedPost);
			ApexPages.Standardcontroller testStdCon = new ApexPages.Standardcontroller(testad);
			IndeedPostingController testIndeedPost = new IndeedPostingController(testStdCon);
			
			PageReference testsavead = testIndeedPost.saveAd();
			system.assert(testsavead == null);
			PageReference testupdatead = testIndeedPost.updateAd();
			system.assert(testupdatead == null);
		}
	}

	@isTest
	public static void testGetIndeedSponsoredSelect() {
		system.runAs(DummyRecordCreator.platformUser) {
			List<SelectOption> sponsoredSelectOption = IndeedPostUtil.GetIndeedSponsoredSelect();
			system.assert(sponsoredSelectOption.size()==4);
			Advertisement_Configuration__c tempIndeedAccount  = AdConfigDummyRecordCreator.createDummyIndeedAccount();
			List<SelectOption> IndeedScreeingQuestion = IndeedPostUtil.getIndeedScreeingQuestionSelect(tempIndeedAccount.Id);
			system.assert(IndeedScreeingQuestion.size()==1);
		}
	}
}