/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class IndeedQuestionsControllerTest2 {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	String indeedApplicationSetId=IndeedApplicationQuestionSelectorTest.createData();
    	String jsonStr='[{"sequence":1,"question":"Mobile Phone","name":"Mobile Phone","isRequired":false,"conditionOption":{"label":"Mrs.","value":"Mrs."},"conditionField":{"label":"Salutation","value":"Salutation"},"apiname":"MobilePhone","$$hashKey":"004"},{"sequence":3,"question":"Last Name","name":"Last Name","isRequired":true,"conditionOption":{"label":null,"value":null},"conditionField":{"label":null,"value":null},"apiname":"LastName","$$hashKey":"006"},{"sequence":4,"question":"Other Country","name":"Other Country","isRequired":false,"conditionOption":{"label":"Dr.","value":"Dr."},"conditionField":{"label":"Salutation","value":"Salutation"},"apiname":"OtherCountry","$$hashKey":"008"},{"sequence":5,"question":"Email","name":"Email","isRequired":true,"conditionOption":{"label":null,"value":null},"conditionField":{"label":null,"value":null},"apiname":"Email","$$hashKey":"00A"},{"sequence":6,"question":"Email Bounced Date","name":"Email Bounced Date","isRequired":false,"conditionOption":{"label":null,"value":null},"conditionField":{"label":null,"value":null},"apiname":"EmailBouncedDate","$$hashKey":"00C"},{"name":"jobApplication Id","apiname":"jobApplication_Id__c","question":"jobApplication Id","$$hashKey":"00G","isRequired":true,"conditionField":{"label":"Please choose control field","value":""},"conditionOption":{"label":"Please choose option for control field","value":""},"sequence":2}]';
    	IndeedQuestionsController.saveRecords(jsonStr,indeedApplicationSetId);
    	list<Application_Question__c> applicationQuestionRecs=new list<Application_Question__c>();
    	applicationQuestionRecs=[select id from Application_Question__c where Application_Question_Set__c=:  indeedApplicationSetId];
    	System.assertEquals(applicationQuestionRecs.size(),6);
    	}
    	
    }
}