@isTest
private class StyleCategoryExtensionTest {

	private static testMethod void testStyleCategoryExtension() {
		System.runAs(DummyRecordCreator.platformUser) {
        List<Map<String, Object>> sobjs = new List<Map<String, Object>>();
		Map<String, Object> sobj = new Map<String, Object>();
		Id saveSearchRecordTypeId = DaoRecordType.GoogleSaveSearchRT.Id;
		
		sobj.put('ownerId', userInfo.getUserId());
		sobj.put('RecordTypeId', saveSearchRecordTypeId);
		sobj.put('Name' , 'testStyleCategoryRecord');
		sobj.put('Active__c', true);
		sobj.put('templateActive__c', true);
		
		sobjs.add(sobj);
		
	    List<StyleCategory__c> sCategory = StyleCategoryExtension.getSaveSearchRecordsByUser();
		system.assertEquals(sCategory.size(), 0);
		
		sCategory = StyleCategoryExtension.createRecords(sobjs);
		system.assert(sCategory.size()>0);
		
		List<Map<String, Object>> sobjsUpdate = new List<Map<String, Object>>();
		Map<String, Object> sobjUpdate = new Map<String, Object>();
		sobjUpdate.put('Id', sCategory[0].Id);
		sobjsUpdate.add(sobjUpdate);
		sCategory = StyleCategoryExtension.updateRecords(sobjsUpdate);
		system.assert(sCategory.size()>0);
		
		StyleCategoryExtension.deleteRecords(sobjsUpdate);
		sCategory = StyleCategoryExtension.getSaveSearchRecordsByUser();
		system.assertEquals(sCategory.size(), 0);
		
		StyleCategoryExtension.apiDescriptor();
		}
	}

}