/*** Controller ***/
// Project: PeopleCloud
// Used for: Search Query Construction
// Search Admin App: in development
// Other involved: apex class: ReadXMLFileController 

  
public class QueryClass {
    String SearchObject;
    String QueryString;
    String[] QueryFields = new String[]{};
    String[] QueryWhere = new String[]{};

	//generate operator list
    public static List<SelectOption> sloOprStr = new List<SelectOption>{new SelectOption('0', '--None--'), new SelectOption('1', 'contains'), new SelectOption('2', 'starts with'),
        new SelectOption('3', 'ends with'), new SelectOption('4', 'equals'), new SelectOption('5', 'not equal to')};
    public static List<SelectOption> sloOprBoo = new List<SelectOption>{new SelectOption('0', '--None--'),new SelectOption('1', 'equals'), new SelectOption('2', 'not equal to')};
    public static List<SelectOption> sloOprInt = new List<SelectOption>{new SelectOption('0', '--None--'),new SelectOption('1', 'equals'), 
        new SelectOption('2', 'not equal to'),new SelectOption('3', 'less than'), new SelectOption('4', 'greater than'),
        new SelectOption('5', 'less or equal'),new SelectOption('6', 'greater or equal')};
    public static List<SelectOption> sloOprMulti = new List<SelectOption>{new SelectOption('0', '--None--'),new SelectOption('1', 'equals'), new SelectOption('2', 'not equal to'),
        new SelectOption('3', 'includes'), new SelectOption('4', 'excludes')};
    public static List<SelectOption> sloOprArea = new List<SelectOption>{ new SelectOption('1', 'contains')};
    public static List<SelectOption> sloOprDate = new List<SelectOption>{ new SelectOption('0', '--None--'),new SelectOption('1', 'within')};

    public QueryClass(){    
        SearchObject = 'Contact';
        //DefineList();
    }

    public QueryClass(String o){    
        SearchObject = o;    
    }
  
    public static List<SelectOption> getOperatorType(String disType) {
            List<SelectOption> result;
            disType = disType.toLowerCase();
            if(disType == 'picklist' || disType == 'combobox' || disType == 'reference' || 
                disType == 'boolean' || disType == 'id') {
                result = sloOprBoo;
            }
            else if (disType == 'currency' || disType == 'integer' || disType == 'double' || 
                disType == 'percent' ) {
                result = sloOprInt;
            }
            else if (disType == 'multipicklist') {
                result = sloOprMulti;
            }
            else if (disType == 'string'  || disType == 'phone' ||
                disType == 'url' || disType == 'email') {
                result = sloOprStr;
            }
            else if (disType == 'textarea')
            {   result = sloOprArea; }
            else if (disType == 'date' || disType == 'datetime')
            {
                result = sloOprDate;
                
            }
            return result;
   }
    public String getQueryString(){
        QueryString = '';
        if(SearchObject != null){
     /*   if(QueryFields != null ){
            if(QueryFields.size()>0)
            {   QueryString = 'Select ';
                
                for(Integer i=0; i<QueryFields.size(); i++ ){
                    if(i == QueryFields.size()-1 ){
                        QueryString = QueryString + QueryFields[i];}
                    else
                        {QueryString = QueryString + QueryFields[i] + ',';}
                    
                }
                QueryString = QueryString + ' from ' + SearchObject;
      */
                if (QueryWhere != null)
                {
                    if(QueryWhere.size()>0)
                    {
                        QueryString = ' where '; 
                        for(Integer j=0; j<QueryWhere.size(); j++ ){
                            if(j == 0 ){
                                QueryString = QueryString + QueryWhere[j];}
                            else
                                {QueryString = QueryString + ' And ' + QueryWhere[j] ;}
                        }
                        
                    }
                }
                //system.assertEquals(QueryString,null);
                return QueryString;  
            }
       // }
       // }
        return null;
    }
    
    public void ClearQueryWhere(){    
        QueryWhere.clear();    
    }
    
    //add the query string to QueryWhere list
    public void ConstructQuery(String FieldApi, String FieldType, String filter, String InputValue){
        FieldType = FieldType.toLowerCase();
        
        QueryFields.add(FieldApi);
        //** please see the DefineList()
        if (FieldType  == 'string'  || FieldType == 'phone' ||
                FieldType == 'url' || FieldType  == 'email') {
                if (filter == '1') {
                    QueryWhere.add(SearchObject + '.' + FieldApi + ' LIKE \'%' + InputValue + '%\'');
                }
                else if (filter == '2') {
                    QueryWhere.add(SearchObject + '.' + FieldApi + ' LIKE \'' + InputValue + '%\'');
                }
                else if (filter == '3') {
                    QueryWhere.add(SearchObject + '.' + FieldApi + ' LIKE \'%' + InputValue + '\'');
                }
                else if (filter == '4') {
                    QueryWhere.add(SearchObject + '.' + FieldApi + ' = \'' + InputValue + '\'');
                }
                else if (filter == '5') {
                    QueryWhere.add( SearchObject + '.' + FieldApi + ' <> \'' + InputValue + '\'');
                }
         }
         else if (FieldType == 'textarea')
         {
             if (filter == '1') {
                    QueryWhere.add(SearchObject + '.' + FieldApi + ' LIKE \'%' + InputValue + '%\'');
                }
         }
         else if (FieldType  == 'boolean') {
         	if(InputValue != ''){
                if (InputValue.trim().toLowerCase() == 'yes')
                {  InputValue = 'true';  }
                if (InputValue.trim().toLowerCase() == 'no')
                {  InputValue = 'false';  }
                
                if (filter == '1') {
                    QueryWhere.add( SearchObject + '.' + FieldApi + ' = ' + InputValue);   
                }
                else if (filter == '2') {
                    QueryWhere.add(SearchObject + '.' + FieldApi + ' <> ' + InputValue);  
                }
         	}
         }
         else if (FieldType  == 'picklist' || FieldType == 'combobox'  ||
                     FieldType  == 'id') {
                if (filter == '1') {
                    QueryWhere.add(SearchObject + '.' + FieldApi + ' = \'' + InputValue + '\'');  
                }
                else if (filter == '2') {
                    QueryWhere.add(SearchObject + '.' + FieldApi + ' <> \'' + InputValue + '\''); 
                }
         }
         else if (FieldType == 'reference' ) {
                if(InputValue != null)
                {   String[] temp_inputs ;
                    temp_inputs = InputValue.split(':',2);
                    if(temp_inputs != null){
                        if(temp_inputs.size()==2 )
                        {
                            QueryWhere.add(temp_inputs[0] + ' = \'' + temp_inputs[1] + '\'');  
                        
                        }
                        else{

                        // error 
                        }
                    
                    }

                
                }
                
         }
        else if (FieldType == 'date' || FieldType == 'datetime'){
            	if(InputValue != ''){
	                if(filter == '1')
	                {   String datestring;
	                    
	                    if(FieldType == 'datetime')
	                    {   datestring = getTheDateString(InputValue, true);}
	                    else
	                    {
	                        datestring = getTheDateString(InputValue, false);
	                    }
	                    if(datestring != null)
	                    {
	                        QueryWhere.add(SearchObject + '.' + FieldApi + ' >= ' + datestring);
	                    }  
	                }
                }
            }
        else if (FieldType == 'currency' || FieldType == 'integer' || FieldType == 'double' || 
                     FieldType == 'percent' ) {
               /* if(FieldType == 'datetime')
                {InputValue = ChangeDateFormat(InputValue, true, true);}
                if(FieldType == 'date')
                {InputValue =  ChangeDateFormat(InputValue, false, true);}
                */
                if(InputValue != ''){                
	                if (filter == '1') {
	                    QueryWhere.add(SearchObject + '.' + FieldApi + ' = ' + InputValue);   
	                } 
	                else if (filter == '2') {
	                    QueryWhere.add(SearchObject + '.' + FieldApi + ' <> ' + InputValue);  
	                }
	
	                else if (filter == '3') {
	                    QueryWhere.add(SearchObject + '.' + FieldApi + ' < ' + InputValue);   
	                } 
	                else if (filter == '4') {
	                    QueryWhere.add(SearchObject + '.' + FieldApi + ' > ' + InputValue);   
	                } 
	                else if (filter == '5') {
	                    QueryWhere.add(SearchObject + '.' + FieldApi + ' <= ' + InputValue);  
	                }  
	                else if (filter == '6') {
	                    QueryWhere.add(SearchObject + '.' + FieldApi + ' >= ' + InputValue);  
	                }
                }   
            }    
        else if (FieldType == 'multipicklist') {
                if (filter == '1') {
                    String[] tempvalues = InputValue.split(',');
                    String tempinput ='';
                    for(Integer i=0; i<tempvalues.size(); i++)
                    {
                        if(i == tempvalues.size()-1)
                        {tempinput = tempinput  +  tempvalues[i] ;}
                        else
                        {tempinput = tempinput +  tempvalues[i] + ';' ;}
                    
                    }
                    QueryWhere.add(SearchObject + '.' + FieldApi + ' = \'' + tempinput + '\'');  
                }
                else if (filter == '2') {
                    String[] tempvalues = InputValue.split(',');
                    String tempinput ='';
                    for(Integer i=0; i<tempvalues.size(); i++)
                    {
                        if(i == tempvalues.size()-1)
                        {tempinput = tempinput  +  tempvalues[i] ;}
                        else
                        {tempinput = tempinput +  tempvalues[i] + ';' ;}
                    
                    }
                    QueryWhere.add(SearchObject + '.' + FieldApi + ' <> \'' + tempinput + '\''); 
                }
                else if (filter == '3') {
                    String[] tempvalues = InputValue.split(',');
                    String tempinput ='';
                    for(Integer i=0; i<tempvalues.size(); i++)
                    {
                        if(i == tempvalues.size()-1)
                        {tempinput = tempinput + '\'' +  tempvalues[i] + '\'';}
                        else
                        {tempinput = tempinput + '\''  + tempvalues[i] + '\',';}
                    
                    }
                    QueryWhere.add(SearchObject + '.' + FieldApi + ' includes (' + tempinput + ')'); 
                }
                else if (filter == '4') {
                    String[] tempvalues = InputValue.split(',');
                    String tempinput = '';
                    for(Integer i=0; i<tempvalues.size(); i++)
                    {
                        if(i == tempvalues.size()-1)
                        {tempinput = tempinput + '\'' +  tempvalues[i] + '\'';}
                        else
                        {tempinput = tempinput + '\''  + tempvalues[i] + '\',';}
                    
                    }
                    QueryWhere.add(SearchObject + '.' + FieldApi + ' excludes (' + tempinput + ')'); 
                }
            }
    
        //system.debug('QueryWhere=' + QueryWhere);
    }
public static String getTheDateString(String days, boolean isDatetime){
    // return the last day in days
    Integer lastdays = Integer.valueOf(days);
    Date now = Date.today();
    Integer theoldestday = now.day() - lastdays + 1; 
    Date finaldate ;
    
    if( theoldestday > 0 ) // means in same month
    {
        finaldate = Date.newInstance(now.year(),now.month(), theoldestday);
    
    }
    else
    {
        Date firstday = Date.newInstance(now.year(), 1, 1); 
        integer i;
        integer extradays;
        Integer daysdiff;
        integer temp =0;
        if( firstday.addDays(lastdays) > now)  // previous year needed
        {
            extradays = 0;
            for(integer j = now.month()-1 ; j>0 ; j--)
            {
                extradays = extradays + Date.daysInMonth(now.year(), j);
            }
            daysdiff = lastdays - (now.day() + extradays);
            i = 12;
            daysdiff = daysdiff - Date.daysInMonth(now.year()-1, i);
            while (daysdiff > 0)
            {
                temp = daysdiff;
                i = i - 1; 
                daysdiff = daysdiff - Date.daysInMonth(now.year()-1, i);
            }
            finaldate = Date.newInstance(now.year() - 1,i, Date.daysInMonth(now.year()-1, i) - temp + 1);
        }
        else
        {
            i = 1;
            extradays = Date.daysInMonth(now.year(),now.month() - i );
            daysdiff = lastdays - ( now.day() + extradays);
            
            while(daysdiff > 0){
                i = i + 1 ;
                temp = daysdiff;
                extradays = extradays +  Date.daysInMonth(now.year(),now.month() - i );
                daysdiff = lastdays - ( now.day() + extradays);
                
            }
            finaldate = Date.newInstance(now.year(),now.month() - i, temp);
        
        }
    
    }
    String datestring;
    if(finaldate != null)
    {
        
        datestring = String.valueOf(finaldate);
    }
    else
    {
        datestring = String.valueOf(now);
    }
    if(isDatetime)
    {
        datestring = datestring + 'T23:59:59Z';
    }
    return datestring;
}
/*public static boolean CheckDate(String inputDate) {
        boolean result = false;
        
        result = Pattern.Matches('([1-9]|[1-2][0-9]|3[0-1])[/]([1-9]|1[0-2])[/](19|20)[0-9]{2}',inputDate);
        
        if (result) {
            inputDate = ChangeDateFormat(inputDate, false, false);
            string[] temp;
            temp = inputDate.split('-');
            if (temp[1] == '04' ||temp[1] == '06' ||temp[1] == '09' ||temp[1] == '11') {
                //if max date 30
                if (integer.valueOf(temp[2]) > 30){
                    result = false;
                }
            }
            else if (temp[1] == '02') {
                //if month feb
                if (date.isLeapYear(integer.valueOf(temp[0]))) {
                    //if leap year
                    if (integer.valueOf(temp[2]) > 29) {
                        result = false;
                    }
                }
                else {
                    if (integer.valueOf(temp[2]) > 28) {
                        result = false;
                    }
                }
            }
        }
        
        return result;
    }
   public static string ChangeDateFormat(String inputDate, Boolean isDateTime, Boolean isForQuery) {
        string[] temp;
            
        inputDate = inputDate.trim().replace('\'', '').replace('/','-');
        temp = inputDate.trim().split('-');
        if(temp[0].length() == 1) {
            temp[0] = '0' + temp[0];
        }
        if(temp[1].length() == 1) {
            temp[1] = '0' + temp[1];
        }
        inputDate = temp[2] + '-' + temp[1] + '-' + temp[0];
        if (isDateTime) {
            if (isForQuery) {
                inputDate = inputDate + 'T00:00:00Z';
            }
            else {
                inputDate = inputDate + ' 00:00:00';
            }
        }

        return inputDate;
    }
    
    */
    public static Integer inputValidation(String fieldtype,String inputvalue){
	    fieldtype= fieldtype.toLowerCase().trim();
    	if(inputvalue!= ''){
	        if(fieldtype== 'boolean') {
	            if(inputvalue.trim().toLowerCase() == 'yes' || inputvalue.trim().toLowerCase() == 'no' || inputvalue.trim().toLowerCase() == '') {
	                return 0;
	            }
	            else {
	                return 1;  
	            }
	         }                
	        if(fieldtype== 'currency' || fieldtype== 'integer' || 
	                            fieldtype== 'double' || fieldtype== 'percent') {
	            if(inputvalue == ''){ //add by jack for inputvalue is null
	        	try {            	
	                double temp = double.valueOf(inputvalue.trim());
	                return 0;
	                }
	            catch (exception e) {
	                                //ShowMessageBox('Filter criteria row on 1, Invalid number for this field');
	                return 2;
	                }
	            }
	        } 
    	}
       /* if(fieldtype== 'date' || fieldtype == 'datetime') {
            //if(disType == DisplayType.Date) {
                if (CheckDate(inputvalue.trim())) {                                  
                                    return 0;
                }
                else {
                    //DateTime d = System.now();
                    //ShowMessageBox('Filter criteria row on 1, Invalid date (Valid date format ' + d.day() + '/' + d.month() + '/' + d.year() + ')');
                    return 3;
                }
            // }
             
        }*/
        return 0;
    }
    static testmethod void testthiscontroller() {  
    	System.runAs(DummyRecordCreator.platformUser) {
        QueryClass thecontroller = new QueryClass();
        List<SelectOption> options = new List<SelectOption>();
        options = QueryClass.getOperatorType('multipicklist');
        system.assert(options.size()>0);
        options = QueryClass.getOperatorType('integer');
        system.assert(options.size()>0);
        options = QueryClass.getOperatorType('email');
        system.assert(options.size()>0);
        options = QueryClass.getOperatorType('datetime');
        system.assert(options.size()>0);
        options = QueryClass.getOperatorType('picklist');
        system.assert(options.size()>0);
        
        thecontroller.ClearQueryWhere();
        system.assert(thecontroller.QueryWhere.size() == 0);
        
        thecontroller.ConstructQuery('FirstName', 'String', '1', 'suzan');
        thecontroller.ConstructQuery('phone', 'phone', '2', '1111111111');
        thecontroller.ConstructQuery('URL', 'url', '3', '1111111111');
        thecontroller.ConstructQuery('Email', 'Email', '4', 'suzan@clicktocloud.com');
        thecontroller.ConstructQuery('FirstName', 'String', '1', 'suzan');
        thecontroller.ConstructQuery('LeadSource', 'Picklist', '1', 'Web');
        thecontroller.ConstructQuery('Birthdate', 'Date', '1', '3');
        thecontroller.ConstructQuery('Candidate_From_Web__c', 'boolean', '1', 'true');
        thecontroller.ConstructQuery('Candidate_From_Web__c', 'boolean', '2', 'true');
        thecontroller.ConstructQuery('Age', 'currency', '1', '50');
        thecontroller.ConstructQuery('Age', 'currency', '2', '50');
        thecontroller.ConstructQuery('Age', 'currency', '3', '50');
        thecontroller.ConstructQuery('Age', 'currency', '4', '50');
        thecontroller.ConstructQuery('Age', 'currency', '5', '50');
        thecontroller.ConstructQuery('Age', 'currency', '6', '50');
        thecontroller.ConstructQuery('Gender__c', 'multipicklist', '1', 'male,female');
        thecontroller.ConstructQuery('Gender__c', 'multipicklist', '2', 'male,female');
        thecontroller.ConstructQuery('Gender__c', 'multipicklist', '3', 'male,female');
        thecontroller.ConstructQuery('Gender__c', 'multipicklist', '4', 'male,female');
        system.assert(thecontroller.QueryWhere.size() > 0);
        system.assert(thecontroller.getQueryString()!='');
        system.assert(QueryClass.getTheDateString('30',true)!='');
        
        system.assert(QueryClass.inputValidation('boolean','yes')==0);
        system.assert(QueryClass.inputValidation('boolean','haha')==1);
        system.assert(QueryClass.inputValidation('currency','30')==0);
    	}
    }
}