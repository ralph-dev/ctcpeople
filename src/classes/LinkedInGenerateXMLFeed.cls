/*
	Author by Jack on 16 Apr 2014
	This class is for LinkedIn Generate XML Feed
    generateXMLFeed is for Job Posting and Job Updating function
	generateRenewXMLFeed is for Job Renew function
*/

public with sharing class LinkedInGenerateXMLFeed {
	public static String generateXMLFeed(String adid, Boolean isEdit){
		Advertisement__c tempad = new Advertisement__c();
		CommonSelector.checkRead(Advertisement__c.sObjectType,
			'id, Job_Title__c, Country__c, LinkedIn_Postal_code__c, Job_content__c, Location__c,'
			+'Skills_Experience__c, Company_Description__c, Job_Function__c,Industry__c,'
			+'Job_Type_Text__c, Experience_Level__c, Show_Poster__c, Poster_Role__c, Salary_Description__c, Has_Referral_Fee__c,'
			+'Application_URL__c, LinkedIn_Account__r.Advertiser_Id__c, LinkedIn_Account__r.LinkedIn_Partner_ID__c');
			
		tempad = [select id, Job_Title__c, Country__c, LinkedIn_Postal_code__c, Job_content__c, Location__c,
					Skills_Experience__c, Company_Description__c, Job_Function__c,Industry__c,
					Job_Type_Text__c, Experience_Level__c, Show_Poster__c, Poster_Role__c, Salary_Description__c, Has_Referral_Fee__c,
					Application_URL__c, LinkedIn_Account__r.Advertiser_Id__c, LinkedIn_Account__r.LinkedIn_Partner_ID__c from Advertisement__c where id=: adid];
		
		String pickListFieldsString = LinkedInPostUtil.getPickListFieldMapJsonString();
		XMLDom.Element job = new XMLDom.Element('job'); 
		
		//Partner job id and contract id can not be update
		if(!isEdit){
			XMLDom.Element partnerjobid = new XMLDom.Element('partner-job-id');
	        partnerjobid.nodeValue = UserInfo.getOrganizationId().subString(0, 15)+':'+String.valueOf(tempad.Id).subString(0, 15);
	        job.childNodes.add(partnerjobid);
	        
	        XMLDom.Element contractid = new XMLDom.Element('contract-id');
	        contractid.nodeValue = (tempad.LinkedIn_Account__r.LinkedIn_Partner_ID__c).escapeHtml4();
	        job.childNodes.add(contractid);
		}
		
        XMLDom.Element company = new XMLDom.Element('company');
        job.childNodes.add(company);
        
        XMLDom.Element companyname = new XMLDom.Element('name');
        //companyname.nodeValue = JobBoardUtils.wrappedWithCDATA((UserInformationCon.CompanyName()).escapeHtml4());
        companyname.nodeValue = JobBoardUtils.wrappedWithCDATA(UserInformationCon.CompanyName());
        company.childNodes.add(companyname);
        
        XMLDom.Element companyid = new XMLDom.Element('id');
        companyid.nodeValue = tempad.LinkedIn_Account__r.Advertiser_Id__c;
        company.childNodes.add(companyid);
        
        XMLDom.Element companydescription = new XMLDom.Element('description');
        if(tempad.Company_Description__c != null){
        	tempad.Company_Description__c = JobBoardUtils.removebreakcharacteratrichtextarea(tempad.Company_Description__c);
        	companydescription.nodeValue = JobBoardUtils.wrappedWithCDATA((tempad.Company_Description__c).unescapeHtml4());
        }
        company.childNodes.add(companydescription); 
        
        XMLDom.Element position = new XMLDom.Element('position');
        job.childNodes.add(position); 
        
       	XMLDom.Element jobtitle = new XMLDom.Element('title');
        if(tempad.Job_Title__c != null){
        	//jobtitle.nodeValue = JobBoardUtils.wrappedWithCDATA((tempad.Job_Title__c).escapeHtml4());
        	jobtitle.nodeValue = JobBoardUtils.wrappedWithCDATA(tempad.Job_Title__c);
        }
        position.childNodes.add(jobtitle); 
        
        XMLDom.Element jobdescription = new XMLDom.Element('description');
        if(tempad.Job_content__c != null){
        	tempad.Job_content__c = JobBoardUtils.removebreakcharacteratrichtextarea(tempad.Job_content__c);
        	jobdescription.nodeValue = JobBoardUtils.wrappedWithCDATA((tempad.Job_content__c).unescapeHtml4());
        }
        position.childNodes.add(jobdescription); 
        
        XMLDom.Element joblocation = new XMLDom.Element('location');
        position.childNodes.add(joblocation); 
		
		XMLDom.Element locationcountry = new XMLDom.Element('country');
        joblocation.childNodes.add(locationcountry); 
		
		XMLDom.Element locationcode = new XMLDom.Element('code');
		if(tempad.Country__c!=null && tempad.Country__c!=''){
			String countrycode = (LinkedInPost.getlinkedinCountryCodeByFiedlString(tempad.Country__c,'linkedincountry' ,pickListFieldsString)).code;
			locationcode.nodeValue = countrycode;
		}
		locationcountry.childNodes.add(locationcode);
		
		XMLDom.Element locationpostalcode = new XMLDom.Element('postal-code');
		if(tempad.LinkedIn_Postal_code__c!=null && tempad.LinkedIn_Postal_code__c!=''){
			locationpostalcode.nodeValue = tempad.LinkedIn_Postal_code__c;
		}
        joblocation.childNodes.add(locationpostalcode); 
        
        //Change by Jack on 16 Sep 2014. According the info from LinkedIn. If the option field is empty, the tag should be omitted.
		if(tempad.Location__c!=null && tempad.Location__c!=''){
			XMLDom.Element locationname = new XMLDom.Element('name');
			locationname.nodeValue = JobBoardUtils.wrappedWithCDATA(tempad.Location__c);
			joblocation.childNodes.add(locationname);
		}
        
        XMLDom.Element jobfunctions = new XMLDom.Element('job-functions');
        position.childNodes.add(jobfunctions); 
		
		if(tempad.Job_Function__c != null){
			List<String> jobfunctionlist = (tempad.Job_Function__c).split(',');//multi job function values
			if(jobfunctionlist!=null && jobfunctionlist.size()>0){
				for(String function: jobfunctionlist){
					XMLDom.Element jobfunction = new XMLDom.Element('job-function');
					jobfunctions.childNodes.add(jobfunction);
					
					XMLDom.Element jobfunctioncode = new XMLDom.Element('code');
					jobfunctioncode.nodeValue = (LinkedInPost.getlinkedinCountryCodeByFiedlString(function.trim(),'linkedinJobFunction',pickListFieldsString)).code;
					jobfunction.childNodes.add(jobfunctioncode);
				}
			}
		}
		XMLDom.Element industries = new XMLDom.Element('industries');
        position.childNodes.add(industries); 
		
		if(tempad.Industry__c != null){
			List<String> industrielist = (tempad.Industry__c).split(',');//Multi industries values
			if(industrielist!=null && industrielist.size()>0){
				for(String ind: industrielist){
					XMLDom.Element industry = new XMLDom.Element('industry');
					industries.childNodes.add(industry);
					
					XMLDom.Element industrycode = new XMLDom.Element('code');
					industrycode.nodeValue = (LinkedInPost.getlinkedinCountryCodeByFiedlString(ind.trim(),'linkedinJobIndustry',pickListFieldsString)).code;
					industry.childNodes.add(industrycode);
				}
			}
		}
		XMLDom.Element jobtype = new XMLDom.Element('job-type');
        position.childNodes.add(jobtype); 
		
		XMLDom.Element jobtypecode = new XMLDom.Element('code');
		jobtypecode.nodeValue = (LinkedInPost.getlinkedinCountryCodeByFiedlString(tempad.Job_Type_Text__c,'linkedinJobType',pickListFieldsString)).code;
		jobtype.childNodes.add(jobtypecode);
		
		XMLDom.Element experiencelevel = new XMLDom.Element('experience-level');
        position.childNodes.add(experiencelevel); 
		
		XMLDom.Element experiencecode = new XMLDom.Element('code');
		experiencecode.nodeValue = (LinkedInPost.getlinkedinCountryCodeByFiedlString(tempad.Experience_Level__c,'linkedinJobExperienceLevel', pickListFieldsString)).code;
		experiencelevel.childNodes.add(experiencecode);
		
		//Change by Jack on 16 Sep 2014. According the info from LinkedIn. If the option field is empty, the tag should be omitted.
		if(tempad.Skills_Experience__c!= null){
			XMLDom.Element skillsandexperience = new XMLDom.Element('skills-and-experience');
			tempad.Skills_Experience__c = JobBoardUtils.removebreakcharacteratrichtextarea(tempad.Skills_Experience__c);
			skillsandexperience.nodeValue = JobBoardUtils.wrappedWithCDATA((tempad.Skills_Experience__c).unescapeHtml4());
			position.childNodes.add(skillsandexperience); 
		}
		
		//Change by Jack on 16 Sep 2014. According the info from LinkedIn. If the option field is empty, the tag should be omitted.
		if(tempad.Salary_Description__c != null){
			XMLDom.Element salary = new XMLDom.Element('salary');
			tempad.Salary_Description__c = JobBoardUtils.removebreakcharacteratrichtextarea(tempad.Salary_Description__c);
        	salary.nodeValue = JobBoardUtils.wrappedWithCDATA((tempad.Salary_Description__c).unescapeHtml4());
        	job.childNodes.add(salary); 
        }
        
        //Change by Jack on 16 Sep 2014. According the info from LinkedIn. If the option field is empty, the tag should be omitted.
		if(tempad.Has_Referral_Fee__c != null){
			XMLDom.Element referral = new XMLDom.Element('referral-bonus');
        	referral.nodeValue = JobBoardUtils.wrappedWithCDATA((tempad.Has_Referral_Fee__c).unescapeHtml4());
        	job.childNodes.add(referral); 
        }
        
        XMLDom.Element poster = new XMLDom.Element('poster');
        job.childNodes.add(poster); 
		
		XMLDom.Element posterdisplay = new XMLDom.Element('display');
		posterdisplay.nodeValue = String.valueof(tempad.Show_Poster__c);
		poster.childNodes.add(posterdisplay);
		
		//Poster email address can not be update
		if(!isEdit){
			XMLDom.Element posteremailaddress = new XMLDom.Element('email-address');
			if(UserInformationCon.getUserDetails.Job_Posting_Email__c == null || UserInformationCon.getUserDetails.Job_Posting_Email__c == ''){
				UserInformationCon.getUserDetails.Job_Posting_Email__c = UserInformationCon.getUserDetails.Email;
			}
			posteremailaddress.nodeValue = UserInformationCon.getUserDetails.Job_Posting_Email__c;
			poster.childNodes.add(posteremailaddress);
		}
		
		XMLDom.Element posterrole = new XMLDom.Element('role');
		poster.childNodes.add(posterrole);
		
		XMLDom.Element postercode = new XMLDom.Element('code');
		postercode.nodeValue = (LinkedInPost.getlinkedinCountryCodeByFiedlString(tempad.Poster_Role__c,'linkedinPostingRole', pickListFieldsString)).code;
		posterrole.childNodes.add(postercode);
		
		XMLDom.Element howtoapply = new XMLDom.Element('how-to-apply');
        job.childNodes.add(howtoapply); 
		
		XMLDom.Element applicationurl = new XMLDom.Element('application-url');
		String applicationFromURL = JobBoardUtils.removeAmpersand(tempad.Application_URL__c); 
		applicationurl.nodeValue = JobBoardUtils.wrappedWithCDATA(applicationFromURL);
		howtoapply.childNodes.add(applicationurl);
		
		return job.toXmlString();
	}
	
	//Generate renew XML feed
	public static String generateRenewXMLFeed(Advertisement__c ad){
		XMLDom.Element job = new XMLDom.Element('job'); 
		
		XMLDom.Element contractid = new XMLDom.Element('contract-id');
        contractid.nodeValue = ad.LinkedIn_Account__r.LinkedIn_Partner_ID__c;
        job.childNodes.add(contractid);
        
        XMLDom.Element renewalad = new XMLDom.Element('renewal');
        job.childNodes.add(renewalad);
        
        return job.toXmlString();
	}
}