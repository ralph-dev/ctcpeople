@isTest
private with sharing class JXTEmailToCandidateTest {
    
    static JXTEmailToCandidate service;
    static DummyRecordCreator.DefaultDummyRecordSet rs;
	
	static Advertisement__c adv;
	static JXTContent content;
 	static Messaging.InboundEmail sampleEmail;
 	static List<Messaging.InboundEmail.BinaryAttachment> binaryAtts;
	
	
	static{
		rs = DummyRecordCreator.createDefaultDummyRecordSet();
	    System.runAs(DummyRecordCreator.platformUser) {
	    
	    //create dummy advertisement for dummy vacancy[0]
	    AdvertisementDummyRecordCreator adCreator = new AdvertisementDummyRecordCreator();
	    adv = adCreator.generateOneAdvertisementDummyRecord(rs);
	    
	    binaryAtts = EmailDummyRecordCreator.createBinaryAttachment(new Map<String, String>{'Resume' => 'test content', 'CoverLetter' => 'test content'} ); 
	    
	    sampleEmail = EmailDummyRecordCreator.createEmail('www.jxt.solutions test', 
	    'Dear AYCG,\nThis is to inform you that \nFirstname:testCanFirstName1\nLastname:testCanLastName1\nhas sent you a job application for job\nJob Title: Bryn Mawr, PA 19010, USA - job for delta T\nReference Number: 00D28000001ddc5:'+ adv.Id +'\nSource: www.jxt.solutions\nEmail Address: 1test@test.com\nFrom The Team', 
        'test@test.com', binaryAtts);
	    
	    service = new JXTEmailToCandidate();
	    }
	   
	}
	
	static testMethod void testJXTEmailToCandidate() {
	    System.runAs(DummyRecordCreator.platformUser) {
		    CTCPeopleSettingHelper__c ctc = new CTCPeopleSettingHelper__c(Name='test', Default_Skill_Groups__c='["G001"]', recordTypeId=DaoRecordType.DefaultSkillGroupRT.Id );
	        insert ctc;
	        PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
	        cs.Enable_Daxtra_Skill_Parsing__c=true;
	        upsert cs; 
		    
		   
	        
    	    Messaging.InboundEmailResult result = service.handleInboundEmail( sampleEmail, new Messaging.InboundEnvelope());
            System.assertEquals(true, result.success);
	    }
	    
	}
    
    static testMethod void testJXTEmailToCandidateFailed() {
        
        System.runAs(DummyRecordCreator.platformUser) {
	        sampleEmail = EmailDummyRecordCreator.createEmail('www.jxt.solutions test', 
		    'Dear AYCG,\nThis is to inform you that \nFirstname:rick\nLastname:mare\nhas sent you a job application for job\nJob Title: Bryn Mawr, PA 19010, USA - job for delta T\nReference Number: 00D28000001ddc5:testAd\nSource: www.jxt.solutions\nEmail Address: rickx@jxt.com.au\nFrom The Team', 
	        'test@test.com', binaryAtts);
	        
    	    Messaging.InboundEmailResult result = service.handleInboundEmail( sampleEmail, new Messaging.InboundEnvelope());
            System.assertEquals(true, result.success);
	    }
	}

}