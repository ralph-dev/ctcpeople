@isTest
private class CandidateParserFactoryTest {

     static Map<String , Messaging.InboundEmail> emails  = new Map<String, Messaging.InboundEmail>();
   
   static{
   	System.runAs(DummyRecordCreator.platformUser) {
   		Messaging.InboundEmail mail = new Messaging.InboundEmail();
		mail.plainTextBody = 'test content';
		mail.subject = 'andys resume';
		mail.fromAddress = 'andy@anydomain.com';
	    emails.put('email 1', mail);
	    
	    mail = new Messaging.InboundEmail();
		mail.plainTextBody = 'test content';
		mail.subject = 'andys coverletter';
		mail.fromAddress = 'andy@anydomain.com';
	    emails.put('email 2', mail);
	    
	     mail = new Messaging.InboundEmail();
		mail.plainTextBody = 'test content';
		mail.subject = 'andys resume - hello world';
		mail.fromAddress = 'andy@anydomain.com';
		Messaging.InboundEmail.BinaryAttachment attach = new Messaging.InboundEmail.BinaryAttachment();
		attach.fileName = 'andy-resume.pdf';
		attach.body = null;
		mail.binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[]{attach};
	    emails.put('email 3', mail);
	    
	    mail = new Messaging.InboundEmail();
		mail.plainTextBody = 'test content';
		mail.subject = 'andys CV  - hello world';
		mail.fromAddress = 'andy@anydomain.com';
		attach = new Messaging.InboundEmail.BinaryAttachment();
		attach.fileName = 'andy-latest-CV-resume.doc';
		attach.body = null;
		mail.binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[]{attach};
	    emails.put('email 4', mail);
	    
	    mail = new Messaging.InboundEmail();
		mail.plainTextBody = 'test content';
		mail.subject = 'www.jxt.solutions  - hello world';
		mail.fromAddress = 'andy@anydomain.com';
		attach = new Messaging.InboundEmail.BinaryAttachment();
		attach.fileName = 'andy-latest-CV-resume.doc';
		attach.body = null;
		mail.binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[]{attach};
	    emails.put('email 5', mail);
        
        mail = new Messaging.InboundEmail();
		mail.plainTextBody = 'test content';
		mail.subject = 'andys application  - hello world';
		mail.fromAddress = 'andy@anydomain.com';
		attach = new Messaging.InboundEmail.BinaryAttachment();
		attach.fileName = 'andy-latest-CV-resume.doc';
		attach.body = null;
		mail.binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[]{attach};
	    emails.put('email 6', mail);
   	}
       	
	    
   }

   static testMethod void testGetParserName(){
   	System.runAs(DummyRecordCreator.platformUser) {
   		System.assertEquals('' , CandidateParserFactory.getParserName( emails.get('email 1') ));
		System.assertEquals('', CandidateParserFactory.getParserName( emails.get('email 2') ) );
		System.assertEquals('CVEmailParser', CandidateParserFactory.getParserName( emails.get('email 3') ) );
		System.assertEquals('CVEmailParser', CandidateParserFactory.getParserName( emails.get('email 4') ) );
		System.assertEquals('JXTEmailParser', CandidateParserFactory.getParserName( emails.get('email 5') ) );
        System.assertEquals('CVEmailParser', CandidateParserFactory.getParserName( emails.get('email 6') ) );
   	}
		
		
	}
	
	static testMethod void testCreateParser( ){
		System.runAs(DummyRecordCreator.platformUser) {
			System.assert(CandidateParserFactory.createParser('CVEmailParser') instanceof CVEmailParser);
		    System.assert(CandidateParserFactory.createParser('NotExistingParser') == null);
		    System.assert(CandidateParserFactory.createParser('JXTEmailParser') instanceof JXTEmailParser);
		}
	    
	}
	
	static testMethod void testGetParser(){
		System.runAs(DummyRecordCreator.platformUser) {
			System.assert(CandidateParserFactory.getParser(emails.get('email 1')) == null);
		    System.assert(CandidateParserFactory.getParser(emails.get('email 2')) == null);
		    System.assert(CandidateParserFactory.getParser(emails.get('email 3'))  instanceof CVEmailParser);
		    System.assert(CandidateParserFactory.getParser(emails.get('email 4'))  instanceof CVEmailParser);
		    System.assert(CandidateParserFactory.getParser(emails.get('email 5'))  instanceof JXTEmailParser);
            System.assert(CandidateParserFactory.getParser(emails.get('email 6'))  instanceof CVEmailParser);
		}
	    
	}
	

}