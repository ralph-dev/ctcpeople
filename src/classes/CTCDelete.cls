public with sharing class CTCDelete {
	//Delete the Old documents and Old Email template       
    public static void delOldDummyDocument(String delTempNam){
        //system.debug('delTempNam='+ delTempNam);
        List<Document> tempdocs=DaoDocument.getOldSendResumeDummyDoc(UserInfo.getUserId(),delTempNam);
        if(tempdocs != null && tempdocs.size()>0){
	        try{
                fflib_SecurityUtils.checkObjectIsDeletable(Document.SObjectType);
	            delete tempdocs;
	        }catch(Exception e){
	            NotificationClass.notifyErr2Dev('Error key:Send Resume encounter error when deleting old the dummy docs', e);
	        }
        }        
    }
    
    @future
    public static void delOldTemplate(String delTempNam){
        //
        List<EmailTemplate> temp=DaoEmailTemplate.getOldTemplateLikeDevName(delTempNam);
        if(temp != null && temp.size()>0){
        try{
            fflib_SecurityUtils.checkObjectIsDeletable(EmailTemplate.SObjectType);
            delete temp;
	        }
	        catch(Exception e){
	            NotificationClass.notifyErr2Dev('Error key: Send Resume encounter error when deleting old the custom template', e);
	        }
        }        
    } 
    
    @future
    public static void delOldStyleCategory(){
        String delTempNam='Temp_ASrecord_'+UserInfo.getUserId()+'_';
        List<StyleCategory__c> temp=DaoStyleCategory.getOldStyleCategoryLikeDevName(delTempNam);
        if(temp != null && temp.size()>0){
        try{
            fflib_SecurityUtils.checkObjectIsDeletable(StyleCategory__c.SObjectType);
            delete temp;
	        }
	   	catch(Exception e){
	        NotificationClass.notifyErr2Dev('Error key: APS encounter error when deleting old the StyleCategory Record', e);
	        }
        }        
    }      
}