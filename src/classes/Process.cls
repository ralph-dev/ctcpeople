public interface Process {
    Process handle();
    Process handle(Object context);
    String getProcessName();
    PageReference getProcessPage();
    Process rollBack();
}