@isTest
private class CandidateSkillSelectorTest
{
	@isTest
	static void testGetCandidateSkillByCandidates() {
		Contact dummycon = TestDataFactoryRoster.createContact();     //create a contact
		Skill__c skill = TestDataFactoryRoster.createSkill();
		Candidate_Skill__c cSkill = TestDataFactoryRoster.createCandidateSkill(dummycon, skill); //create candidate skill
		
		Map<String, String> skillsMap = new Map<String, String>();
		skillsMap.put(skill.Id, 'And');

		Set<String> contactsId = new Set<String>();
		contactsId.add(dummycon.Id);

		List<Candidate_Skill__c> cSkillList = new List<Candidate_Skill__c>();

		CandidateSkillSelector cSkillSelector = new CandidateSkillSelector();
		cSkillList = cSkillSelector.getCandidateSkillByCandidates(skillsMap, contactsId);
		system.assertEquals(cSkillList.size(),1);
	}
	
	@isTest
	//test candidate skill for specific candidate but no verified skill needed
	static void testGetCandidateSkillByCandidatesWithQuery1() {
		Contact dummycon = TestDataFactoryRoster.createContact();     //create a contact
		Skill__c skill = TestDataFactoryRoster.createSkill();
		Candidate_Skill__c cSkill = TestDataFactoryRoster.createCandidateSkill(dummycon, skill); //create candidate skill
		
		Map<String, String> skillsMap = new Map<String, String>();
		skillsMap.put(skill.Id, 'And');

		Set<String> contactsId = new Set<String>();
		contactsId.add(dummycon.Id);

		List<Candidate_Skill__c> cSkillList = new List<Candidate_Skill__c>();

		CandidateSkillSelector cSkillSelector = new CandidateSkillSelector();
		cSkillList = cSkillSelector.getCandidateSkillByCandidates(skillsMap, false, contactsId);
		system.assertEquals(cSkillList.size(),1);
	}
	
	
	@isTest
	//test candidate skill without specific candidate but no verified skill needed
	static void testGetCandidateSkillByCandidatesWithQuery2() {
		Contact dummycon = TestDataFactoryRoster.createContact();     //create a contact
		Skill__c skill = TestDataFactoryRoster.createSkill();
		Candidate_Skill__c cSkill = TestDataFactoryRoster.createCandidateSkill(dummycon, skill); //create candidate skill
		
		Map<String, String> skillsMap = new Map<String, String>();
		skillsMap.put(skill.Id, 'And');

		Set<String> contactsId = new Set<String>();
		//contactsId.add(dummycon.Id);

		List<Candidate_Skill__c> cSkillList = new List<Candidate_Skill__c>();

		CandidateSkillSelector cSkillSelector = new CandidateSkillSelector();
		cSkillList = cSkillSelector.getCandidateSkillByCandidates(skillsMap, false, contactsId);
		system.assertEquals(cSkillList.size(),1);
	}
	
	@isTest
	//test candidate skill for specific candidate but verified skill needed
	static void testGetCandidateSkillByCandidatesWithQuery3() {
		Contact dummycon = TestDataFactoryRoster.createContact();     //create a contact
		Skill__c skill = TestDataFactoryRoster.createSkill();
		Candidate_Skill__c cSkill = TestDataFactoryRoster.createCandidateSkill(dummycon, skill); //create candidate skill
		
		Map<String, String> skillsMap = new Map<String, String>();
		skillsMap.put(skill.Id, 'And');

		Set<String> contactsId = new Set<String>();
		contactsId.add(dummycon.Id);

		List<Candidate_Skill__c> cSkillList = new List<Candidate_Skill__c>();

		CandidateSkillSelector cSkillSelector = new CandidateSkillSelector();
		cSkillList = cSkillSelector.getCandidateSkillByCandidates(skillsMap, true, contactsId);
		system.assertEquals(cSkillList.size(),0);
	}
	
	@isTest
	//test candidate skill without specific candidate but verified skill needed
	static void testGetCandidateSkillByCandidatesWithQuery4() {
		Contact dummycon = TestDataFactoryRoster.createContact();     //create a contact
		Skill__c skill = TestDataFactoryRoster.createSkill();
		Candidate_Skill__c cSkill = TestDataFactoryRoster.createCandidateSkill(dummycon, skill); //create candidate skill
		
		Map<String, String> skillsMap = new Map<String, String>();
		skillsMap.put(skill.Id, 'And');

		Set<String> contactsId = new Set<String>();
		//contactsId.add(dummycon.Id);

		List<Candidate_Skill__c> cSkillList = new List<Candidate_Skill__c>();

		CandidateSkillSelector cSkillSelector = new CandidateSkillSelector();
		cSkillList = cSkillSelector.getCandidateSkillByCandidates(skillsMap, true, contactsId);
		system.assertEquals(cSkillList.size(),0);
	}
	
	
	
}