@isTest
private class AuthServiceTest {
    
    @isTest
    static void test_authSFWebConnectorApp() {
        SFOauthToken__c token = new SFOauthToken__c();
        token.name = 'oAuthToken';
        token.App_Id__c = 'CTC_Connected_App';
        insert token;
        String url = AuthService.authSFWebConnectorApp();
        system.assert(url!=null);
    }
    
}