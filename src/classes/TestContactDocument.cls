@isTest
private class TestContactDocument
{
    private static testMethod void testContactDocument() {
    	System.runAs(DummyRecordCreator.platformUser) {
        List<Contact> con = new List<Contact>();
        con = createTestAccount();
        
        PCAppSwitch__c cs=PCAppSwitch__c.getInstance();
        cs.Enable_Create_Sample_Resume_for_Contact__c = true;
        insert cs;
        
        update con;
        // createTestDoc(con);
        
        List<Attachment> att = [select id from Attachment where parentId =: con];
        system.assert(att.size() == 171);
    	}
    }
    
    private static testmethod void TestContactDocumentOperator() {
    	System.runAs(DummyRecordCreator.platformUser) {
        List<Contact> con = new List<Contact>();
        con = createTestAccount();
        Map<String, Web_Document__c> webMap = ContactDocumentSelector.getContactExistingDocument(con);
        ContactDocumentOperator contactDocOperator = new ContactDocumentOperator();
        List<ActivityDoc> activityDocList = contactDocOperator.getActivityDocList(con , webMap);
        system.assertEquals(activityDocList.size() , 171); 
        ContactDocumentServices.UploaddocumentsByEc2('jsonString');
    	}
    }
    

    
    private static testmethod void TestContactDocumentSelector() {
    	System.runAs(DummyRecordCreator.platformUser) {
        List<Contact> con = new List<Contact>();
        con = createTestAccount();
        Map<String, Web_Document__c> webMap = ContactDocumentSelector.getContactExistingDocument(con);
        system.assert(webMap.size() == 29);
    	}
    }
    private static List<Contact> createTestAccount() {
    	
        Account acc = new Account(Name='comp1');
        insert acc;
        
        List<String> contactRecordIdSet = new List<String>();
        contactRecordIdSet = new List<String>(DaoRecordType.nonCandidateRTIds);
        
        List<Contact> conlist = new List<Contact>();
        
        for(integer i=0; i<200; i++){
            Contact con = new Contact();
            con.LastName='con1';
            con.RecordTypeId = contactRecordIdSet.get(0);
            conlist.add(con);
        }
        insert conlist;
        createTestDoc(conlist);
        return conlist;
    }

    private static List<Web_Document__c> createTestDoc(List<Contact> conList) {
        List<Web_Document__c> wdList = new List<Web_Document__c>();
        for(integer i=0; i<200; i++){
            if(math.mod(i, 7) == 0) {
                User currentUser = UserInformationCon.getUserDetails;
                Web_Document__c webDoc = new Web_Document__c();
                webDoc.Document_Related_To__c  = conList[i].id;
                webDoc.Document_Type__c = 'Resume';
                webDoc.S3_Folder__c = currentUser.AmazonS3_Folder__c;
                wdList.add(webDoc);
            }
        }
        insert wdList;
        return wdList;
    }
}