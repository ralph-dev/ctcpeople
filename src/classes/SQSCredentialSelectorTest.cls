/**
 * This the test class for SQSCredentialSelector
 * Created by: Lina
 * Created date: 19/05/2016
 * */

@isTest
public class SQSCredentialSelectorTest {
    
    private static SQSCredentialSelector selector = new SQSCredentialSelector();
    
    
    public static void createDummySQSCredential(){
    	SQSCredential__c sqs = new SQSCredential__c();
        sqs.Name = 'Daxtra Feed SQS';
        sqs.Access_Key__c = 'TestAccessKey';
        sqs.Secret_Key__c = 'TestSecretKey';
        sqs.URL__c = 'https://www.test.com/test/123456'; 
        insert sqs;
    }
    
    static testmethod void testGetSQSCredential() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Create data
        createDummySQSCredential();
        
        SQSCredential__c sqsSelected = selector.getSQSCredential('Daxtra Feed SQS');
        System.assertEquals('https://www.test.com/test/123456', sqsSelected.URL__c);
        System.assertEquals('TestAccessKey', sqsSelected.Access_Key__c);
        System.assertEquals('TestSecretKey', sqsSelected.Secret_Key__c);
    	}
    }
    
    static testmethod void testNull() {
    	System.runAs(DummyRecordCreator.platformUser) {
        SQSCredential__c sqsSelected = selector.getSQSCredential('Daxtra Feed SQS');
        System.assertEquals(null, sqsSelected);
    	}
    }
    
    
    static testmethod void testMultisqs() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Create data
        SQSCredential__c sqs = new SQSCredential__c();
        sqs.Name = 'Daxtra Feed SQS';
        sqs.Access_Key__c = 'TestAccessKey1';
        sqs.Secret_Key__c = 'TestSecretKey1';
        sqs.URL__c = 'https://www.test.com/test1/123456'; 
        insert sqs;        
        SQSCredential__c sqs2 = new SQSCredential__c();
        sqs2.Name = 'Daxtra Feed SQS';
        sqs2.Access_Key__c = 'TestAccessKey2';
        sqs2.Secret_Key__c = 'TestSecretKey2';
        sqs2.URL__c = 'https://www.test.com/test2/123456'; 
        insert sqs2;
        
        SQSCredential__c sqsSelected = selector.getSQSCredential('Daxtra Feed SQS');
        System.assertEquals('https://www.test.com/test1/123456', sqsSelected.URL__c);
        System.assertEquals('TestAccessKey1', sqsSelected.Access_Key__c);
        System.assertEquals('TestSecretKey1', sqsSelected.Secret_Key__c);
    	}
    }
}