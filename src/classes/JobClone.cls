/**
Advertisement clone function for
JXT, jobx_nz, TradeMe, and website.
**/
public with sharing class JobClone {
     public String aid{get; set;}
     public Advertisement__c ad{get; set;}
     public Advertisement__c newAd{get; set;}
     public User u;
     private ApexPages.StandardController stdCon;
     public JobClone(ApexPages.StandardController stdController){
        stdCon = stdController;
        aid = ApexPages.currentPage().getParameters().get('id');
        
        CommonSelector.checkRead(Advertisement__c.sObjectType,
        	'RecordTypeId, zipcode__c, '
                +'years_of_work_experience__c, years_of_work_experience_Label__c,  '
                +'white_label__c, website_Location__c, suburb__c, '
                +'photos__c, legally_work__c, jxt_sectors__c, '
                +'isStandOut__c, Work_Status__c, Work_Status_Label__c, '
                +'WorkType__c, WorkType_Seek_Label__c, WorkType_Label__c, '
                +'WebSite__c, Video_ID__c, Vacancy__c, Terms_And_Conditions_Url__c, '
                +'Template__c, Template_Name__c, SubClassification_Seek_Label__c, SubClassification2__c, '
                +'Street_Adress__c, Status__c, State__c, State_Label__c, Short_Description__c,'
                +'SeekSubclassification__c, SeekSpecialisation__c, SeekSkill__c, SeekSalaryType__c, '
                +'SeekSalaryText__c, SeekSalaryMin__c, SeekSalaryMax__c, SeekMarketSegment__c,  '
                +'SeekLocation__c, SeekFunction__c, SeekClassification__c, SeekArea__c, Sectors__c, '
                +'Search_Tags__c, Search_Bullet_Point_3__c, Search_Bullet_Point_2__c, Search_Bullet_Point_1__c, '
                +'Search_Areas__c, SearchArea_String__c, SearchArea_Label_String__c, '
                +'Salary_Type__c, Salary_Modifier__c, Salary_Min__c, Salary_Max__c, Salary_Description__c,'
                +'Residency_Required__c, Requires_Work_Permit__c, Referral_Fee__c, Reference_No__c, RealLocation_Seek__c,'
                +'RealArea_Seek__c, Qualification__c, Provider_Code__c, Priority_Start_Date__c, Priority_Listings__c, '
                +'Priority_Listing__c, Preferred_Application_Mode__c, Postcode__c, Postal_Address__c,'
                +'Physical_Address__c, Phone2_Prefix__c, Phone2_Number__c, Phone1_Prefix__c, Phone1_Number__c, '
                +'Permanent_or_ContractLabel__c, Permanent_or_Contact__c, Pay_Type__c, Pay_Type_Label__c, Options__c,'
                +'Online_Summary__c, Online_Search_Title__c, Online_Job_Id__c, Online_Footer__c, '
                +'OnlineFooter__c, Office_Code__c, Occupation__c, Occ_String__c, '
                +'Occ_Label_String__c, No_salary_information__c, Nearest_Transport__c, '
                +'Location__c, Location_Seek_Label__c, Location_Hide__c, Listing_Title__c, ListingID__c, '
                +'Link_to_Ad__c, Job_Type__c, Job_Title__c, Job_Posting_Status_txt__c, Job_Posting_Status__c, '
                +'Job_Content__c, Job_Content_Rich__c, Job_Contact_Phone__c, Job_Contact_Name__c, Job_Bolding__c, '
                +'JobXML__c, JXT_Short_Description__c, JXTNZ_Location__c, Is_Pro_rata__c,'
                +'Is_Package__c, Is_OTE__c, Industry__c, Industry_String__c, Industry_Label_String__c, '
                +'Hot_Job__c, Hide_Company_Name__c, Has_Referral_Fee__c, Function_Seek_Label__c, Featured__c,'
                +'Enhanced_Listing__c, Employer_Name__c, Email__c, Education_Level__c, Education_Label_Level__c,'
                +'Currency_Type__c, Country__c, Contact_Name__c, CompensationType__c, Company_Name__c, '
                +'Classification__c, Classification_Seek_Label__c, Classification2__c, City__c, City_C1__c, '
                +'Area_Seek_Label__c,  Application_Form_Style__c, Application_Form_Style_Name__c ,Skill_Group_Criteria__c, '
                +'IsQualificationsRecognised__c, TemplateCode__c, AdvertiserJobTemplateLogoCode__c, ClassificationCode__c, '
                +'Classification2Code__c, SubClassificationCode__c, SubClassification2Code__c, WorkTypeCode__c, SectorCode__c, '
                +'CountryCode__c, LocationCode__c, AreaCode__c, Bullet_1__c, Bullet_2__c, Bullet_3__c, Prefer_Email_Address__c,UnitNumber__c, StreetNumber__c,StreetName__c   ');
        ad = [Select p.RecordTypeId, zipcode__c, 
                years_of_work_experience__c, years_of_work_experience_Label__c,  
                white_label__c, website_Location__c, suburb__c, 
                photos__c, legally_work__c, jxt_sectors__c, 
                isStandOut__c, Work_Status__c, Work_Status_Label__c, 
                WorkType__c, WorkType_Seek_Label__c, WorkType_Label__c, 
                WebSite__c, Video_ID__c, Vacancy__c, Terms_And_Conditions_Url__c, 
                Template__c, Template_Name__c, SubClassification_Seek_Label__c, SubClassification2__c, 
                Street_Adress__c, Status__c, State__c, State_Label__c, Short_Description__c,
                SeekSubclassification__c, SeekSpecialisation__c, SeekSkill__c, SeekSalaryType__c, 
                SeekSalaryText__c, SeekSalaryMin__c, SeekSalaryMax__c, SeekMarketSegment__c,  
                SeekLocation__c, SeekFunction__c, SeekClassification__c, SeekArea__c, Sectors__c, 
                Search_Tags__c, Search_Bullet_Point_3__c, Search_Bullet_Point_2__c, Search_Bullet_Point_1__c, 
                Search_Areas__c, SearchArea_String__c, SearchArea_Label_String__c, 
                Salary_Type__c, Salary_Modifier__c, Salary_Min__c, Salary_Max__c, Salary_Description__c,
                Residency_Required__c, Requires_Work_Permit__c, Referral_Fee__c, Reference_No__c, RealLocation_Seek__c,
                RealArea_Seek__c, Qualification__c, Provider_Code__c, Priority_Start_Date__c, Priority_Listings__c, 
                Priority_Listing__c, Preferred_Application_Mode__c, Postcode__c, Postal_Address__c,
                Physical_Address__c, Phone2_Prefix__c, Phone2_Number__c, Phone1_Prefix__c, Phone1_Number__c, 
                Permanent_or_ContractLabel__c, Permanent_or_Contact__c, Pay_Type__c, Pay_Type_Label__c, Options__c,
                Online_Summary__c, Online_Search_Title__c, Online_Job_Id__c, Online_Footer__c, 
                OnlineFooter__c, Office_Code__c, Occupation__c, Occ_String__c, 
                Occ_Label_String__c, No_salary_information__c, Nearest_Transport__c, 
                Location__c, Location_Seek_Label__c, Location_Hide__c, Listing_Title__c, ListingID__c, 
                Link_to_Ad__c, Job_Type__c, Job_Title__c, Job_Posting_Status_txt__c, Job_Posting_Status__c, 
                Job_Content__c, Job_Content_Rich__c, Job_Contact_Phone__c, Job_Contact_Name__c, Job_Bolding__c, 
                JobXML__c, JXT_Short_Description__c, JXTNZ_Location__c, Is_Pro_rata__c,
                Is_Package__c, Is_OTE__c, Industry__c, Industry_String__c, Industry_Label_String__c, 
                Hot_Job__c, Hide_Company_Name__c, Has_Referral_Fee__c, Function_Seek_Label__c, Featured__c,
                Enhanced_Listing__c, Employer_Name__c, Email__c, Education_Level__c, Education_Label_Level__c,
                Currency_Type__c, Country__c, Contact_Name__c, CompensationType__c, Company_Name__c, 
                Classification__c, Classification_Seek_Label__c, Classification2__c, City__c, City_C1__c, 
                Area_Seek_Label__c,  Application_Form_Style__c, Application_Form_Style_Name__c ,Skill_Group_Criteria__c, 
                IsQualificationsRecognised__c, TemplateCode__c, AdvertiserJobTemplateLogoCode__c, ClassificationCode__c, 
                Classification2Code__c, SubClassificationCode__c, SubClassification2Code__c, WorkTypeCode__c, SectorCode__c, 
                CountryCode__c, LocationCode__c, AreaCode__c, Bullet_1__c, Bullet_2__c, Bullet_3__c, Prefer_Email_Address__c,UnitNumber__c, StreetNumber__c,StreetName__c   
                From Advertisement__c p
                where id=:aid];
        
     }
     public PageReference cloneAd(){
        Advertisement__c newAd = ad.clone(false, true);
        try{
            newAd.Status__c = 'Clone to Post';
            //if(ad.WebSite__c=='Trademe'){
            if(ad.WebSite__c=='Trademe' || ad.WebSite__c=='JXT' || ad.WebSite__c == 'JXT_NZ'){
                newAd.Job_Posting_Status__c = '';
            }else{
                newAd.Job_Posting_Status__c = 'In Queue';
            }
            //checkFLS(); 
            //insert newAd;
            CommonSelector.quickInsert(newAd);
            //system.debug('newAd id = '+newAd.id);
        }catch(DMLException e){
            system.debug(e);
        }
        
        String iid = UserInfo.getOrganizationId().subString(0, 15); //Org id
        String rid = String.valueOf(newAd.id);                      //Ad id
        
        PageReference newPage = null;
        if(ad.WebSite__c=='Seek'){
            rid = rid.subString(0,15);
            newPage = Page.seekEdit;
        }else if(ad.WebSite__c=='CareerOne'){
            rid = rid.subString(0,15);
            newPage = Page.CareerOnePageEdit;
        }else if(ad.WebSite__c=='Trademe'){
            rid = rid.subString(0,15);
            newAd.ListingID__c = newAd.Id;
            String jobRefCode=UserInfo.getOrganizationId().subString(0, 15)+':'+newAd.Id;
            newAd.Application_URL__c = Endpoint.getcpewsEndpoint()+'/peoplecloud/GetApplicationForm?jobRefCode='+jobRefCode+'&website=trademe';
            newAd.Placement_Date__c = System.today();
            //checkFLS();
            //update newAd;
            CommonSelector.quickUpdate(newAd, 'ListingID__c,Application_URL__c,Placement_Date__c');
            newPage = Page.trademeEdit;
        }else if(ad.WebSite__c=='JXT'){
            newPage = page.jxtEdit; 
        }else if(ad.WebSite__c=='JXT_NZ'){
            newPage = page.jxtNZEdit;
        }else if(ad.WebSite__c=='Website'){  
            newPage = page.websiteEdit;
        }
        
        String jobRefCode = iid+':'+rid;
        CommonSelector.checkRead(User.sObjectType, 'Id, AmazonS3_Folder__c');
        u = [select Id, AmazonS3_Folder__c from User where id=:UserInfo.getUserId()];
        //insertDetailTable(jobRefCode, iid, rid, u.AmazonS3_Folder__c, '', '', '', '', '','', '',newAd.WebSite__c, newAd.Job_Title__c); //Don't insert DB at clone page. Change by Jack on 22/09/2014
        
        newPage.getParameters().put('id', rid);
        newPage.setRedirect(true);
        return newPage;
     }
     
    public PageReference cancelClone(){
        return stdCon.view();
    }
    
    private void checkFLS(){
      List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Advertisement__c.Application_URL__c,
            Advertisement__c.ListingID__c,
            Advertisement__c.Ad_Closing_Date__c,
            Advertisement__c.Placement_Date__c,
            Advertisement__c.Advertisement_Account__c,
            Advertisement__c.Indeed_Question_URL__c,
            Advertisement__c.Vacancy__c,
            Advertisement__c.Skill_Group_Criteria__c,
            Advertisement__c.Job_Type__c,
            Advertisement__c.Job_Title__c,
            Advertisement__c.JXT_Short_Description__c,
            Advertisement__c.Bullet_1__c,
            Advertisement__c.Bullet_2__c,
            Advertisement__c.Bullet_3__c,
            Advertisement__c.Job_Content__c,
            Advertisement__c.Job_Contact_Name__c,
            Advertisement__c.Company_Name__c,
            Advertisement__c.Nearest_Transport__c,
            Advertisement__c.Residency_Required__c,
            Advertisement__c.IsQualificationsRecognised__c,
            Advertisement__c.Location_Hide__c,
            Advertisement__c.TemplateCode__c,
            Advertisement__c.AdvertiserJobTemplateLogoCode__c,
            Advertisement__c.ClassificationCode__c,
            Advertisement__c.SubClassificationCode__c,
            Advertisement__c.Classification2Code__c,
            Advertisement__c.SubClassification2Code__c,
            Advertisement__c.WorkTypeCode__c,
            Advertisement__c.SectorCode__c,
            Advertisement__c.Street_Adress__c,
            Advertisement__c.Search_Tags__c,
            Advertisement__c.CountryCode__c,
            Advertisement__c.LocationCode__c,
            Advertisement__c.AreaCode__c,
            Advertisement__c.Salary_Type__c,
            Advertisement__c.SeekSalaryMin__c,
            Advertisement__c.SeekSalaryMax__c,
            Advertisement__c.Salary_description__c,
            Advertisement__c.No_salary_information__c,
            Advertisement__c.Has_Referral_Fee__c,
            Advertisement__c.Referral_Fee__c,
            Advertisement__c.Terms_And_Conditions_Url__c,
            Advertisement__c.Status__c,
            Advertisement__c.Job_Posting_Status__c,
            Advertisement__c.JobXML__c,
            Advertisement__c.Prefer_Email_Address__c,
            Advertisement__c.RecordTypeId,
            Advertisement__c.Website__C
      };

      fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
      fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    } 

      
    /*
    	not in use since 22/09/2014. Changed by Jack
    */ 
    // @future(callout=true)
    // public static void insertDetailTable(String jobRefCode, String iid, String aid, String bucketname, String hname, String htype,
    //     String dname, String dtype, String fname, String ftype, String divisionName, String webSite, string jobTitle){
    //      CTCWS.PeopleCloudWSDaoPort port = new CTCWS.PeopleCloudWSDaoPort();
    //      port.timeout_x = 60000;
    //      Advertisement__c newJob = [select id from Advertisement__c where id=:aid];
    //      try{
    //         boolean result =  port.insertJobPostingDetail(jobRefCode, iid, aid, bucketname, hname, htype, dname, dtype, fname, ftype, divisionName, webSite, jobTitle);
    //      	if(result != true){
    //      		newJob.Status__c='Not Valid';
    //      		newJob.Job_Posting_Status__c = 'Insert/Update URL Failed';
    //         	update newJob;
    //      	}
    //      }catch(System.Exception e){
    //         newJob.Status__c=('Not Valid');
    //         newJob.Job_Posting_Status__c = 'Insert/Update URL Failed';
    //         update newJob;
    //      }
    // }
}