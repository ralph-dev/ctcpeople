@isTest
private class TestUploadDocumentsController{
    
    public static String getTestCandidateId(){
    	RecordType candRT=DaoRecordType.indCandidateRT; 
    	Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',email='Cand1@test.ctc.test');
    	return cand1.Id;
    }
    
    
    public static testmethod void test(){
    	System.runAs(DummyRecordCreator.platformUser) {
        User userToUpdate = new User();
        userToUpdate.Id = Userinfo.getUserId();
        userToUpdate.AmazonS3_Folder__c = 'testBucket';
        update userToUpdate;

    	String candidateId = getTestCandidateId();
    	Test.setCurrentPage(Page.uploadDocuments);
    	ApexPages.currentPage().getParameters().put('srcId', candidateId);
    	ApexPages.currentPage().getParameters().put('lookupField', 'Document_Related_To__c');
    	
    	ApexPages.currentPage().getParameters().put('srcObj', 'dummy');
    	UploadDocumentsController udcNoSource = new UploadDocumentsController();   
    	
        ApexPages.currentPage().getParameters().put('srcObj', 'Vacancy');
    	UploadDocumentsController udcVacancy = new UploadDocumentsController();    	
    	
    	ApexPages.currentPage().getParameters().put('srcObj', 'Contact');
    	UploadDocumentsController udcContact = new UploadDocumentsController();    	
    	
    	ApexPages.currentPage().getParameters().put('srcObj', 'Client');
    	UploadDocumentsController udcClient = new UploadDocumentsController();    	
    	
    	ApexPages.currentPage().getParameters().put('srcObj', 'candidate');  	
    	UploadDocumentsController udc = new UploadDocumentsController(); 
    	
    	ApexPages.currentPage().getParameters().put('srcId', UserInfo.getUserId());
    	ApexPages.currentPage().getParameters().put('srcObj', 'User');
    	ApexPages.currentPage().getParameters().put('lookupField', 'User__c');
    	ApexPages.currentPage().getParameters().put('srcPage', 'SendResume');
    	UploadDocumentsController udcUser = new UploadDocumentsController();
    	
    	System.assert(udc.serviceEndPoint != null);
    	System.assert(udc.orgId != null);
    	System.assert(udc.bucketName != null);
    	System.assert(udc.nsPrefix != null);
    	System.assert(udc.sessionId != null);
    	System.assert(udc.srcId == candidateId);
    	System.assert(udc.srcObj == 'candidate');
    	System.assert(udc.retURLNotEncoded != null);
    	System.assert(udc.returnLinkValue != null);
    	//System.assert(udc.fieldSetName != null);    	
    	
    	System.assert(true, udcUser.retURLNotEncoded.contains('SRUploadDocumentsResult'));
    	udc.returnToPreviousPage();
    	}
    	
    }

}