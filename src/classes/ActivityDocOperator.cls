/**
* modified by andy for security review II
*/
public with sharing class ActivityDocOperator {
	
	public static boolean updateActivityStatus(String status, String error, String taskId) {
		CommonSelector selector = new CommonSelector(Task.sObjectType);
		Task taskToUpdate =(Task) selector
					.setParam('taskId',taskId)
					.getOne('id, Upload_Documents_Status__c, Upload_Documents_Discription__c',
						'Id =: taskId');
		//[select id, Upload_Documents_Status__c, Upload_Documents_Discription__c from Task where Id =: taskId];
		try{
			taskToUpdate.Upload_Documents_Status__c = status;
			taskToUpdate.Upload_Documents_Discription__c = error;
			
			return selector.doUpdate( taskToUpdate) ;
			
		}catch(Exception e) {
			return false;
		}
	}
}