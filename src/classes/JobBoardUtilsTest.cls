/**
 * This is the test class for JobBoardUtils
 *
 * Created by: Lina
 * Created on: 27/03/2017
 */
@isTest
private class JobBoardUtilsTest {

	static testMethod void testGetRecordId() {
		System.runAs(DummyRecordCreator.platformUser) {
		Id appFormRecordTypeId = JobBoardUtils.getRecordId('ApplicationFormStyle');
		System.assertEquals(DaoRecordType.ApplicationFormStyleRT.Id, appFormRecordTypeId);
		}
	}

	static testMethod void testStringModificationMethod() {
		System.runAs(DummyRecordCreator.platformUser) {
		String str = 'test';
		System.assertEquals('<![CDATA[test]]>', JobBoardUtils.wrappedWithCDATA(str));
		str = 'test\n';
		System.assertEquals('test', JobBoardUtils.removebreakcharacteratrichtextarea(str));

		str = null;
		System.assertEquals('', JobBoardUtils.blankValue(str));
		str = 'test';
		System.assertEquals('test', JobBoardUtils.blankValue(str));

		System.assertEquals(false, JobBoardUtils.isBlank(str));
		str = 'null';
		System.assertEquals(true, JobBoardUtils.isBlank(str));

		System.assertEquals('null', JobBoardUtils.removeAmpersand(str));
		str = null;
		System.assertEquals('', JobBoardUtils.removeAmpersand(str));
		str = '&test';
		System.assertEquals('&amp;test', JobBoardUtils.removeAmpersand(str));

		str = '';
		System.assertEquals('FALSE', JobBoardUtils.booleanValue(str));
		str = 'TRUE';
		System.assertEquals('TRUE', JobBoardUtils.booleanValue(str));
		}
	}

	static testMethod void testRemoveListTrim() {
		System.runAs(DummyRecordCreator.platformUser) {
		List<String> strings = new List<String>{'This  ', ' is ', 'a', 'te st'};
		System.debug(JobBoardUtils.removeListTrim(strings));
		System.assertEquals('This,is,a,te st', JobBoardUtils.removeListTrim(strings));
		}
	}

	static testMethod void testGetTemplateOptions() {
		System.runAs(DummyRecordCreator.platformUser) {
			Test.startTest();
			StyleCategoryDummyRecordCreator.setJobboardStyleCategory();
			List<SelectOption> options = JobBoardUtils.getTemplateOptions('jxt');
			System.assertEquals(2, options.size());
			options = JobBoardUtils.getTemplateOptions('jxt_nz');
			System.assertEquals(2, options.size());
			Test.stopTest();
		}
	}

	static testMethod void testGetStyleOptions() {
		System.runAs(DummyRecordCreator.platformUser) {
			Test.startTest();
			StyleCategoryDummyRecordCreator.setJobboardStyleCategory();
			List<SelectOption> options = JobBoardUtils.getStyleOptions('');
			System.assertEquals(1, options.size());
			Test.stopTest();
		}
	}

	static testMethod void testCheckUserHasActiveJobBoardAccount() {
		System.runAs(DummyRecordCreator.platformUser) {
			Test.startTest();
			Advertisement_Configuration__c seekAccount = AdConfigDummyRecordCreator.createDummySeekAccount();
			String str = String.valueOf(seekAccount.Id);
			System.assertEquals(true, JobBoardUtils.checkUserHasActiveJobBoardAccount(str, DaoRecordType.seekAccountRT.Id));
			System.assertEquals(true, JobBoardUtils.checkUserHasActiveJobBoardAccount(DaoRecordType.seekAccountRT.Id));
			System.assertEquals(false, JobBoardUtils.checkUserHasActiveJobBoardAccount(DaoRecordType.jxtAccountRT.Id));
			Test.stopTest();
		}
	}  

	static testMethod void testOtherMethod() {
		System.runAs(DummyRecordCreator.platformUser) {
		System.assertEquals(true, JobBoardUtils.insertDetailTable('jobRefCode', 'iid', 'aid', 'bucketname', 'hname', 'htype',
				'dname', 'dtype', 'fname', 'ftype', 'divisionName', 'webSite', 'jobTitle'));
		System.assertEquals(1, JobBoardUtils.updateDetailTable('aid', 'hname', 'htype', 'dname', 'dtype', 'fname', 'ftype', 'iid'));
		JobBoardUtils.JobBoardAvailableCheck('test');
		JobBoardUtils.available('test');
		JobBoardUtils.whiteLabelAvailable('test');
		JobBoardUtils.trademeAvailable();
		}
	}

	/************************************** Old priviliage method ***********************************************/

	static testMethod void testGetStyleApplicationFormSelectOption() {
		System.runAs(DummyRecordCreator.platformUser) {
			Test.startTest();
			List<SelectOption> options = JobBoardUtils.getStyleApplicationFormSelectOption();
			System.assertEquals(1, options.size());
			StyleCategoryDummyRecordCreator.setJobboardStyleCategory();
			options = JobBoardUtils.getStyleApplicationFormSelectOption();
			System.assertEquals(2, options.size());
			Test.stopTest();
		}
	}

	static testMethod void testGetSelectSkillGroupList() {
		DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
		Skill_Group__c dsg = DummyRecordCreator.createDefaultSkillGroup();
		System.runAs(DummyRecordCreator.platformUser) {
			
		
			List<SelectOption> options = JobBoardUtils.getSelectSkillGroupList();
			system.assert(options.size()>0);
		}
	}

}