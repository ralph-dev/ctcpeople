public with sharing class MessageAlertController{
    //** this controller is used in vacancy candidate workflow
    public boolean updatesuccessful = false;
    public void ok(){
       String objName = ApexPages.currentPage().getParameters().get('obj');
       String rid = ApexPages.currentPage().getParameters().get('recordid');
       //system.debug('obj = ' + objName);
       system.debug('rid  = ' + rid );
       if(objName !=null && rid != null)
        {
            if(objName.toLowerCase() == 'candidate')
            {
                
                CommonSelector.checkRead(Placement_Candidate__c.sObjectType,'Id, Show_Alert__c ');
                Placement_Candidate__c pc = [select Id, Show_Alert__c from Placement_Candidate__c where Id =: rid];
                pc.Show_Alert__c = false;
                fflib_SecurityUtils.checkFieldIsUpdateable(Placement_Candidate__c.SObjectType, Placement_Candidate__c.Show_Alert__c);
                update pc;
                updatesuccessful = true;
            }
            
            /*if(objName.toLowerCase() == 'vacancy')
            {
                Placement__c p = [select Id, With_Candidate_Placed__c from Placement__c where Id=:rid];
                p.With_Candidate_Placed__c = false;
                update p;
            
            }*/
        }
    }
public static testMethod void testthiscontroller(){
	System.runAs(DummyRecordCreator.platformUser) {
    MessageAlertController thecontroller = new MessageAlertController();
    ApexPages.currentPage().getParameters().put('obj','candidate');
    Account a = new Account(Name='tets');
    insert a;
    Placement__c p = new Placement__c(Name='test', Company__c = a.Id);
    insert p;
    Contact c = new Contact(LastName='test');
    insert c;
    Placement_Candidate__c pc = new Placement_Candidate__c(Show_Alert__c = true, Placement__c = p.Id, Candidate__c = c.Id);
    insert pc;
    ApexPages.currentPage().getParameters().put('recordid',pc.Id);
    thecontroller.ok();
    system.assertEquals(thecontroller.updatesuccessful, true);
    }
	}




}