public with sharing class careerOnePageEdit {
    public Advertisement__c job;
    public String catOccJson{get; set;}
    public String searchAreasJson{get; set;}
    public String c1industryJSon{get; set;}
    public String iid = UserInfo.getOrganizationId().subString(0, 15);
    //public String iid = UserInfo.getOrganizationId();
    public String bucketname ;
    public User u;
    
    private String C1Username;
    private String C1CompanyXCode;
    private transient String C1Password;
    public Boolean PremiumPackageCheckBox {get; set;} //add by jack for Premium Package
    
    public boolean haserror {get;set;}
    public String[] thepostings {get;set;}
    //public String jobContentC1{get; set;}
     //UPDATES FOR CKEDTOR
    //public String onlineFooterC1{get; set;}
    public String saveInfo{get;set;}
    public Integer remainingQuota{get;set;}
    public boolean has_online_id{get;set;} 
    public boolean runout_quota{get;set;}
    public boolean is_post{get;set;}
    public boolean is_test;
    public Integer posted_ad_num{get;set;}
    public List<String> old_locations = new List<String>{};
    public List<String> old_classifiction = new List<String>{};
    public Set<String> old_postings = new Set<String>{};
	public String oldPhyAddress{get;set;} 	// store and display previously input physical address
	
    public boolean has_websiteacc = true;
    public boolean valid_userinfo = true;
    UserInformationCon usercls = new UserInformationCon();
    
    /*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}    
    /*************End Add skill parsing param********************************************************/
    
   /* public void checkquota(){
    
       u = [select Id, Phone, Email, AmazonS3_Folder__c, JobBoards__c, CareerOne_Usage__c,
           Monthly_Quota_CareerOne__c from User where Id=:UserInfo.getUserId()];
       integer career1_count = (u.CareerOne_Usage__c == null || u.CareerOne_Usage__c == '' )? 0 : Integer.valueOf(u.CareerOne_Usage__c);
       integer career1_quota = (u.Monthly_Quota_CareerOne__c == null || u.Monthly_Quota_CareerOne__c == '' )? 0 : Integer.valueOf(u.Monthly_Quota_CareerOne__c);
       remainingQuota = career1_quota - career1_count;
    }*/
    public careerOnePageEdit(ApexPages.StandardController stdController){
       haserror = false;
       runout_quota = false;
       has_online_id = false;
       PremiumPackageCheckBox = false; //After the Career can use standard and premium together. Set the C1PremiumPackage to PremiumPackageCheckBox
   	   is_test = false;
       posted_ad_num =0;
       
       u = usercls.getUserDetail();
	   remainingQuota = usercls.checkquota(3);
       
       if(remainingQuota <=0)
       {
           saveInfo = 'Your current CareerOne quota is 0.';
       }
       
       //Init skill parsing param. Add by Jack on 23/05/2013
       enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
       enableSkillGroupList = new List<Skill_Group__c>();
       enableSkillGroupList = SkillGroups.getSkillGroups();
       defaultskillgroups = new List<String>();
       //end skill parsing param.
        
       this.job = (Advertisement__c) stdController.getRecord();
       
       if(job.Skill_Group_Criteria__c!=null&&job.Skill_Group_Criteria__c!=''){
        	String defaultskillgroupstring = job.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
	        defaultskillgroups = defaultskillgroupstring.split(',');
        }
       //initialize previously input physical address
       String cityStr = '';
       String stateStr ='';
       if(job.City_C1__c != null)
       		cityStr = job.City_C1__c;
       if(job.State__c != null)
       		stateStr = job.State__c;
       oldPhyAddress = cityStr+' '+stateStr;
       
       // system.debug(this.job.Status__c);
       if(this.job.SearchArea_String__c != null && this.job.Cat_String__c !=null)
       {
            old_locations = this.job.SearchArea_String__c.split('\\|\\|\\|'); //** when status = active. that means ads posted before. 
            old_classifiction = this.job.Cat_String__c.split('\\|\\|\\|');
            if(old_locations.size() != old_classifiction.size())
            {
                haserror = true;
            }
            for(integer k = 0; k < old_locations.size(); k++) //** construct old postings' key
            {
                old_postings.add(old_locations[k]+':'+old_classifiction[k]);
            }
       }
       else
       {
            haserror = true;
       }
       
       if(this.job.Online_Job_Id__c != null)
        {
            if(this.job.Online_Job_Id__c.length()>0)
            {
                has_online_id = true;
            }
       }
       if(this.job.Status__c != null)
        {
            if(this.job.Status__c.equalsIgnoreCase('Deleted') || this.job.Status__c.equalsIgnoreCase('Expired'))
            {
	             haserror = true;
	             ApexPages.Message error = new ApexPages.Message(ApexPages.severity.INFO,'This Ad. has been deleted/Expired from website. You are not allowed to update and repost it!');              
	             ApexPages.addMessage(error);  
            }
            //*** quota check ***
            if(this.job.Status__c.equalsIgnoreCase('Ready to Post')) // ** never post before
            {
                if(remainingQuota <=0)
                {
                    runout_quota = true; //** if false, the post button display. 
                }
                old_postings = new Set<String>{}; // ** never posted so never has old postings even location and category saved before.
            }
       }
       else
       {
            haserror = true;
       }
    } 
   
    public void init(){
        if(!haserror)
        {
            String errstr = '';
            boolean has_websiteacc = true;
            boolean valid_userinfo = true;
            
            if(u.AmazonS3_Folder__c != null){
            	bucketname = u.AmazonS3_Folder__c.trim();
            }
            else{
                valid_userinfo = false;
            }
            //commented by andy for security review II
            //StyleCategory__c[] websiteacc = [select id,Advertiser_Password__c,Advertiser_Name__c,Advertiser_Uploadcode__c from StyleCategory__c where WebSite__c='CareerOne' and Account_Active__c = true and RecordType.DeveloperName like '%WebSite_Admin_Record%' limit 1];
            ProtectedAccount pa = new StyleCategorySelector().getCareerOneProtectedAccount();
            
            if(pa == null)
            {
               has_websiteacc = false;
            }
            else
            {
                if(pa.account != null && pa.credential != null)
                {
                	C1CompanyXCode = (String) pa.getAccountFieldValue('Advertiser_Uploadcode__c');
                    C1Username = pa.getUsername();
                    C1Password = pa.getPassword();
                    
                    if(C1Username == null || C1CompanyXCode == null || C1Password == null 
                        || C1Username == '' || C1CompanyXCode == '' || C1Password == '')
                    {
                    	has_websiteacc = false;
                    }
                }
                else
                {  
                    has_websiteacc = false;
           		}
            }
            if(!has_websiteacc )
            {
               	errstr =  'Information: Your company account is not available for job posting to CareerOne. Please contact your system administrator.';
                ApexPages.Message error = new ApexPages.Message(ApexPages.severity.INFO,errstr);              
                ApexPages.addMessage(error);  
                haserror = true;
            }
            if(!valid_userinfo )
            {
               
                errstr =  'Sorry. Your user information is not valid for job-posting! Please contact system administrator.';         
                ApexPages.Message error = new ApexPages.Message(ApexPages.severity.INFO,errstr);              
                ApexPages.addMessage(error);  
                haserror = true;
            }
            if(!is_test)
            {
                haserror = CTCAccessClass.CTCAccessChecking('careerone');
                if(haserror)
                {
                	ApexPages.Message error = new ApexPages.Message(ApexPages.severity.WARNING,'Currently you have no privilege to update this CareerOne Ad.!');              
                    ApexPages.addMessage(error); 
                }
                
                
            }
            if(!haserror)
            {
                thepostings = new String[]{};
                if(this.job.Online_Job_Id__c != null && this.job.Online_Job_Id__c != '')
                {
                    thepostings = this.job.Online_Job_Id__c.split(';');
                    posted_ad_num = thepostings.size();
                }
                else
                {
                    posted_ad_num =0;
                }

				CommonSelector selector = new CommonSelector(Document.sObjectType);
                //Document doc = [Select d.Id, d.Body From Document d where Name='careerone_cat_occ_json' limit 1];
                Document doc = (Document)selector.getOne('Id, Body', 'Name=\'careerone_cat_occ_json\'',1);
                catOccJson = doc.Body.toString();
                
                //Document doc3 = [Select d.Id, d.Body From Document d where Name='c1searcharea_json' limit 1];
                Document doc3 = (Document)selector.getOne('Id, Body', 'Name=\'c1searcharea_json\'',1);
                searchAreasJson = doc3.Body.toString();
                
                //Document doc2 = [Select d.Id, d.Body From Document d where Name='c1industry_json' limit 1];
                Document doc2 = (Document)selector.getOne('Id, Body', 'Name=\'c1industry_json\'',1);
                c1industryJson = doc2.Body.toString();
                getStyles();
                
            }
        }
    }
    
    public String selectedTemplate{get;set;}
    public List<SelectOption> templateOptions = new List<SelectOption>();
    Map<String,String> template_map = new Map<String,String>();
    public List<SelectOption> getTemplateOptions(){
        return templateOptions;
    }
    public String selectedStyle {get;set;}
    public List<SelectOption> styleOptions = new List<SelectOption>();
    Map<Id,StyleCategory__c> style_map = new Map<Id,StyleCategory__c>();

    public List<SelectOption> getStyleOptions(){
        return styleOptions;
    }

    public void getStyles(){
        //**** get Ad. Templates      
        StyleCategory__c[] templates = CTCService.getOptions(1,'careerone'); 
        if(templates != null){
            for(StyleCategory__c ts : templates){
                templateOptions.add(new SelectOption(ts.Id, ts.Name));
                template_map.put(ts.Id,ts.Name);
            }
        }
        //**** populate application form style selection
        StyleCategory__c[] styles = CTCService.getOptions(2,null); 
        if(styles != null){          
            for(StyleCategory__c sc : styles){
                styleOptions.add(new SelectOption(sc.Id, sc.Name));
                style_map.put(sc.Id, sc);
            }     
        }   
        
    }

    public PageReference post(){

        boolean xml_has_err = false;
        
        Map<String,String> paras = ApexPages.currentPage().getParameters();
        Set<String> keys=paras.keySet();
        //for(String k:keys){
        //	system.debug('para '+k+': '+paras.get(k));
        //}
		//job.Job_Content__c = paras.get('dupC1JobContent');        
        //system.debug('job content from job object =' + job.Job_Content__c);
        //system.debug('job content from javascript =' + jobContentC1);
        
        //UPDATES FOR CKEDTOR
        //job.Job_Content__c = jobContentC1;
        //job.Online_Footer__c = onlineFooterC1;
        
        /* check then split job ad physical location */
        if(job.Physical_Address__c == null || job.Physical_Address__c.split(', ').size() !=3){
        	job.Physical_Address__c.addError(CareerOneDetail.ERR_INVALID_PHY_ADDRESS);
          	return ApexPages.currentpage();
        }
        String[] phyAddress = job.Physical_Address__c.split(', ');
        job.zipcode__c = phyAddress[2];
        job.City_C1__c = phyAddress[0];
        job.State__c = phyAddress[1];
       
       	//system.debug('job content =' + jobContentC1);
        C1PostingUtil  util = new C1PostingUtil(C1Username,C1CompanyXCode,C1Password);
        String soapXml = util.constructSoap(this.job,true);
       // system.debug('this.job =' + this.job);
        //system.debug('req soapXml  =' + soapXml );
        if(soapXml == null)
        {
            saveInfo = 'Sorry. Save and Posted Unsuccessfully. <br/> Please contact your system admin.';
            if(!is_test)
            {
            return null;
            }
        }
        
        if(this.job.Template__c != null)
        {
            this.job.Template_Name__c = template_map.get(this.job.Template__c);
        }
        if(this.job.Application_Form_Style__c != null)
        {
            this.job.Application_Form_Style_Name__c = style_map.get(this.job.Application_Form_Style__c).Name;
        }
        this.job.JobXML__c = soapXml;

        List<String> new_postings = new List<String>{};
        List<String> new_locs = new List<String>{};
        List<String> new_cats = new List<String>{};
        if(this.job.SearchArea_String__c != null && this.job.Cat_String__c != null)
        {
            new_locs = this.job.SearchArea_String__c.split('\\|\\|\\|');
            new_cats = this.job.Cat_String__c.split('\\|\\|\\|');
            if(new_locs.size() != new_cats.size())
            {
                xml_has_err = true;
            }
            else
            {
                for(integer nk=0; nk<new_cats.size(); nk++)
                {
                    new_postings.add(new_locs[nk]+':'+ new_cats[nk]);
                }
            }
        }
        else
        {
            xml_has_err = true;
            this.job.JobXML__c = 'JobXml is invalid';
        }
        
        //insert skill group ext id
		if(enableskillparsing){
			String tempString  = String.valueOf(defaultskillgroups);
		    tempString = tempString.replaceAll('\\(', '');
		    tempString = tempString.replaceAll('\\)', '');
		    tempString = tempString.replace(' ','');
		    job.Skill_Group_Criteria__c = tempString;
		}
		    //end insert skill group ext id
        if(!Test.isRunningTest()){
            checkFLS(); 
        }    
           
        update this.job;
        if(xml_has_err)
        {
            saveInfo = 'Sorry. Posted Unsuccessfully. <br/> Please contact your system admin.';
            return null;
        }
        integer newnum =0; //** to count the new location = new postings
        for(String s: new_postings)
        {
            if(!old_postings.contains(s))
            {
                newnum = newnum + 1;
                old_postings.add(s);
            }
        }
        if(is_post)
        {
            //checkquota(); //** check again why, because sometimes user has 1 quota but he open 2 careerone edit page. we can not only do check on loading page.
            remainingQuota = usercls.checkquota(3);
            if(remainingQuota < newnum && newnum >0)
            {
                
                saveInfo = 'Sorry. Posted Unsuccessfully. <br/>';
                saveInfo += 'You have no enough quota to post ad(s).';    
                
               
                return null;
            }
        }
        String aid = String.valueOf(this.job.id).substring(0,15);
        //String aid = String.valueOf(this.job.id);         
        try{
            if(Test.isRunningTest()){

            }else{
                if(!is_test)
                {
                    EC2Callout(aid,iid,this.job.Job_Title__c, this.job.Application_Form_Style__c , bucketname);
                }
                if(is_post && !is_test)
                {
                 postEdit(soapXml, iid, aid, 'edit');
                }
            }                   
            
        }catch(Exception e){
            
            system.debug('Post method Error: ' + e);
            String err_msg = 'System Exception!<br/>';
            err_msg += 'Detail: ' + e.getMessage() + '('+e.getTypeName() +')';
            err_msg += '<br/> advertisement Id: ' + aid +'</br>';
            notificationsendout(err_msg,'systemexception','careerOnePageEdit', 'Post');
        
        }
        system.debug('is_post='+is_post);
        if(is_post)
        {
            this.job.Status__c ='Active';
        }
        checkFLS();
        update this.job;
        //********************** usage count ***************************
        //** is_post = true, means not just save record, means quota enough to post
        //** newnum > 0 , no matter what status = active or ready to post
        
        if(newnum > 0 && is_post)
        {
         u.CareerOne_Usage__c = String.valueOf(Integer.valueOf(u.CareerOne_Usage__c) + newnum);
         fflib_SecurityUtils.checkFieldIsUpdateable(User.SObjectType, User.CareerOne_Usage__c);
         update u;
         
        }
        //***************************************************************

		/* return a post success message to user then redirect user to record detail page*/ 
        String jobTitle = job.Job_Title__c;       
        String strMsg = 'Job Ad "' + jobTitle.escapeHtml4()+ '" with location "'
        			+ job.Physical_Address__c +'" has been post';
        Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.CONFIRM,CareerOneDetail.CON_POST_AD,strMsg));
        return new ApexPages.StandardController(this.job).view();
    }
    
    @future(callout=true)
    public static void postEdit(String soapXml, String orgid, String jobId, String whichaction){
        CTCWS.PeopleCloudWSDaoPort port = new CTCWS.PeopleCloudWSDaoPort();
        //CTCWS4Test.PeopleCloudWSDaoPort port = new CTCWS4Test.PeopleCloudWSDaoPort();
        port.timeout_x = 60000;
        //System.debug('req soapXml = '+soapXml);
        String result;
        result = port.post2C1(soapXml);
        if(whichaction == 'deletebyrefcode')
        {
            ResultProcessForDel(result,jobId);
        }
        else if(whichaction == 'edit')
        {
        
            ResultProcessForEdit(result,jobId);
        }
        //system.debug('call out response =' + result);
        //ResultProcess(result, jobId);
    }
    public static void ResultProcessForEdit(String result, String aId){
        xmldom theXMLDom;
        Map<String,Set<String>> jobs = new Map<String,Set<String>>();
        
        String[] postingids = new String[]{};
        String thestatus;
        String postingId;
        String theref;
        String errorinfo;
        try{
            
            Advertisement__c a = (Advertisement__c) new CommonSelector(Advertisement__c.sObjectType)
            								.setParam('Id',aId)
            								.getOne('Id, Online_Job_Id__c, Placement_Date__c,Job_Posting_Status__c ','Id =:aId');
            
            //[select Id, Online_Job_Id__c, Placement_Date__c,Job_Posting_Status__c 
            //                    from Advertisement__c where Id =:aId];
            
            if(result != null)
            {
                theXMLDom = new xmldom(result);
                if(theXMLDom != null)
                {
                    List<xmldom.Element> jobresponses = theXMLDom.getElementsByTagName('JobResponse');
                    if(jobresponses != null)
                    {
                        for(xmldom.Element r : jobresponses)
                        {
                            if(r.hasChildNodes())
                            {
                                theref = r.getElementsByTagName('JobReference')[0].getAttribute('jobRefCode');
                                thestatus = r.getElementsByTagName('status')[0].getElementsByTagName('ReturnCode')[0].nodeValue;
                                //system.debug(thestatus);
                                if(thestatus != null)
                                {
                                    if(thestatus == '0') //** success
                                    {
                                        List<xmldom.Element> postings = r.getElementsByTagName('JobPostingResponse');
                                        for(xmldom.Element p : postings)
                                        {
                                            
                                            postingId = p.getAttribute('postingId');
                                            postingids.add(postingId);
                                                    
                                        
                                        }
                                    }
                                    else
                                    {
                                        errorinfo = r.getElementsByTagName('status')[0].getElementsByTagName('Descriptions')[0].getElementsByTagName('Description')[0].nodeValue;
                                    
                                    }
                                }
                                else
                                {
                                
                                
                                }
                            }
                        }//for
                    }
                }
            }
            //system.debug('postingids = '+ postingids);
            
            String resultids = '';
            for(Integer i=0 ; i<postingids.size(); i++ )
            {
                if(i ==0)
                {
                    resultids  = postingids[i];
                }
                else
                {
                    resultids  += ';' + postingids[i] ;
                }
            }
            if(thestatus == '0'){
                 if(a.Online_Job_Id__c == null || a.Online_Job_Id__c == '')
                {
                    a.Job_Posting_Status__c = 'active';                
                    if(a.Placement_Date__c == null)
                    {
                         a.Placement_Date__c = system.today();
                         a.Ad_Closing_Date__c = system.today().addDays(29);
                    }
                }
                else
                {
                    a.Job_Posting_Status__c = 'active';
                }
                a.Online_Job_Id__c = resultids;
                a.Status__c = 'Active';
            }
            else{
            	a.Job_Posting_Status__c = 'updatefail';
                String err_msg = 'CareerOne Job Update Unsuccessfully! log@clicktocloud.com<br/>';
                err_msg += 'Result XML: ' + result;
                err_msg += '<br/> advertisement Id: ' + aid;
                notificationsendout(err_msg,'codefail','careerOnePageEdit', 'ResultProcessForEdit');
			}
            system.debug('job ids = ' + a.Online_Job_Id__c);
            //check FLS
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Advertisement__c.Placement_Date__c,
                Advertisement__c.Online_Job_Id__c,
                Advertisement__c.Status__c,
                Advertisement__c.Job_Posting_Status__c,
                Advertisement__c.Ad_Closing_Date__c
            };
            fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
            update a;
        }
        catch(system.Exception e)
        {
        
            system.debug('Update Process Error: ' + e);
            String err_msg = 'System Exception! log@clicktocloud.com<br/>';
            err_msg += 'Detail: ' + e.getMessage() + '('+e.getTypeName() +')';
            err_msg += '<br/> advertisement Id: ' + aid;
            notificationsendout(err_msg,'systemexception','careerOnePageEdit', 'ResultProcessForEdit');
        }
	}
    public static void ResultProcessForDel(String result, String aId){
        xmldom theXMLDom;
        Map<String,Set<String>> jobs = new Map<String,Set<String>>();
        
        String[] postingids = new String[]{};
        String thestatus;
        String postingId;
        String theref;
        String msg;
        String errorinfo ;
        try{
             Advertisement__c a = (Advertisement__c) new CommonSelector(Advertisement__c.sObjectType)
            								.setParam('Id',aId)
            								.getOne('Id, Online_Job_Id__c, Placement_Date__c,Job_Posting_Status__c ','Id =:aId');
           // Advertisement__c a = [select Id, Online_Job_Id__c, Placement_Date__c,Job_Posting_Status__c 
           //                     from Advertisement__c where Id =:aId];
            
            if(result != null)
            {
                theXMLDom = new xmldom(result);
                if(theXMLDom != null)
                {
                    List<xmldom.Element> jobresponses = theXMLDom.getElementsByTagName('DeleteResponse');
                    if(jobresponses != null)
                    {
                        
                        if(jobresponses.size() >0)
                        {
                            if(jobresponses[0].hasChildNodes())
                            {
                            
                                thestatus = jobresponses[0].getElementsByTagName('status')[0].getElementsByTagName('ReturnCode')[0].nodeValue;
                                if(thestatus != null)
                                {
                                    if(thestatus == '0') //** success
                                    {
                                        msg = 'Deleted Successfully!';
                                    }
                                    else
                                    {
                                    
                                        msg = 'Deleted Unsuccessfully!';
                                        //errorinfo = jobresponses[0].getElementsByTagName('status')[0].getElementsByTagName('Descriptions')[0].getElementsByTagName('Description')[0].nodeValue;
                                        //msg += ' Error: '+ errorinfo;
                                    }
                                }
                            }
                        
                        }
                        
                    }
                }
            }
            
            if(thestatus == '0')
            {
                    a.Online_Job_Id__c = null;
                    a.Status__c = 'Deleted';
                    a.Job_Posting_Status__c = 'deleted';
            }
            else
            {
                String err_msg = 'CareerOne Job Delete Unsuccessfully! log@clicktocloud.com<br/>';
                err_msg += 'Result XML: ' + result;
                err_msg += '<br/> advertisement Id: ' + aid;
                notificationsendout(err_msg,'codefail','careerOnePageEdit', 'ResultProcessForDel');
            }
            a.Job_Posting_Status__c = msg;
            
            system.debug('job ids = ' + a.Online_Job_Id__c);
            //check FLS
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Advertisement__c.Placement_Date__c,
                Advertisement__c.Online_Job_Id__c,
                Advertisement__c.Status__c,
                Advertisement__c.Job_Posting_Status__c,
                Advertisement__c.Ad_Closing_Date__c
            };
            fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
            update a;
        }
        catch(system.Exception e)
        {
        
            system.debug('Delete Process Error: ' + e);
            String err_msg = 'System Exception! log@clicktocloud.com<br/>';
            err_msg += 'Detail: ' + e.getMessage() + '('+e.getTypeName() +')';
            err_msg += '<br/> advertisement Id: ' + aid;
            notificationsendout(err_msg,'systemexception','careerOnePageEdit', 'ResultProcessForDel');
        
        }

    }
    public PageReference deleteJob(){
        String timeStamp = C1PostingUtil.getTimeStamp();
        String aid = String.valueOf(this.job.id).subString(0, 15);
        //String aid = String.valueOf(this.job.id);
        system.debug('aid = '+ aid);
        String jobRefCode = iid+':'+aid;
        String xmltodelete = '<?xml version="1.0" encoding="UTF-8"?>';
        xmltodelete += '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header>';
        xmltodelete += '<mh:MonsterHeader xmlns:mh="http://schemas.monster.com/MonsterHeader"><mh:MessageData>';
        xmltodelete += '<mh:MessageId>Company Jobs created on '+timeStamp+'</mh:MessageId><mh:Timestamp>'+timeStamp+'</mh:Timestamp></mh:MessageData>';
        xmltodelete += '</mh:MonsterHeader><wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/04/secext">';
        xmltodelete += '<wsse:UsernameToken><wsse:Username>'+C1Username+'</wsse:Username><wsse:Password>'+C1Password+'</wsse:Password></wsse:UsernameToken></wsse:Security>';
        xmltodelete += '</SOAP-ENV:Header><SOAP-ENV:Body><Monster:Delete xmlns:Monster="http://schemas.monster.com/Monster" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://schemas.monster.com/Monster http://schemas.monster.com/Current/xsd/Monster.xsd"><Monster:Target>JobPosting</Monster:Target><Monster:DeleteBy><Monster:Criteria>RecruiterName</Monster:Criteria>';
        xmltodelete += '<Monster:Value>'+C1Username+'</Monster:Value></Monster:DeleteBy><Monster:DeleteBy><Monster:Criteria>RefCode</Monster:Criteria><Monster:Value>'+jobRefCode+'</Monster:Value></Monster:DeleteBy></Monster:Delete></SOAP-ENV:Body></SOAP-ENV:Envelope>';
        if(!Test.isRunningTest()) {
            postEdit(xmltodelete,iid,aid, 'deletebyrefcode');
        }

        return new ApexPages.StandardController(this.job).view();
    }
 
    @future(callout=true)
    static void EC2Callout(String AdId, String instanceid, String jobtitle, 
                            String selectedStyle, String bucketname){
        
        CTCservice theservice = new CTCservice();
        theservice.theCallout(AdId, instanceid, jobtitle, selectedStyle, bucketname,true,'careerone');
        
    }
    static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());
    
    
    }
    
    public boolean hasErrMsgs{
    	get{
    		List<ApexPages.Message> msgList = ApexPages.getMessages();
    		for(ApexPages.Message sglMsg:msgList){
    			if(sglMsg.getSeverity() == ApexPages.Severity.ERROR || sglMsg.getSeverity() == ApexPages.Severity.FATAL)
    				return true; 
    		} 
    		return false;}
    }
    
    public boolean hasMsgs{
    	get{return ApexPages.getMessages()!=null && ApexPages.getMessages().size()>0;}
    }    
    
    //insert the skill group picklist value option
    public List<SelectOption> getSelectSkillGroupList(){
	  	List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
	  	List<SelectOption> displaySelectOption = new List<SelectOption>();
	  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
	  		displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
	  	}
	  	return displaySelectOption;
	}
    //Show the error message in the page

    private void checkFLS(){
      List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            Advertisement__c.Online_Job_Id__c,
            Advertisement__c.Company_Description__c,
            Advertisement__c.Country__c,
            Advertisement__c.Industry__c,
            Advertisement__c.Application_URL__c,
            Advertisement__c.ListingID__c,
            Advertisement__c.Ad_Closing_Date__c,
            Advertisement__c.Placement_Date__c,
            Advertisement__c.Advertisement_Account__c,
            Advertisement__c.Indeed_Question_URL__c,
            Advertisement__c.Vacancy__c,
            Advertisement__c.Skill_Group_Criteria__c,
            Advertisement__c.Job_Type__c,
            Advertisement__c.Job_Title__c,
            Advertisement__c.JXT_Short_Description__c,
            Advertisement__c.Bullet_1__c,
            Advertisement__c.Bullet_2__c,
            Advertisement__c.Bullet_3__c,
            Advertisement__c.Job_Content__c,
            Advertisement__c.Job_Contact_Name__c,
            Advertisement__c.Company_Name__c,
            Advertisement__c.Nearest_Transport__c,
            Advertisement__c.Residency_Required__c,
            Advertisement__c.IsQualificationsRecognised__c,
            Advertisement__c.Location_Hide__c,
            Advertisement__c.TemplateCode__c,
            Advertisement__c.AdvertiserJobTemplateLogoCode__c,
            Advertisement__c.ClassificationCode__c,
            Advertisement__c.SubClassificationCode__c,
            Advertisement__c.Classification2Code__c,
            Advertisement__c.SubClassification2Code__c,
            Advertisement__c.WorkTypeCode__c,
            Advertisement__c.SectorCode__c,
            Advertisement__c.Street_Adress__c,
            Advertisement__c.Search_Tags__c,
            Advertisement__c.CountryCode__c,
            Advertisement__c.LocationCode__c,
            Advertisement__c.AreaCode__c,
            Advertisement__c.Salary_Type__c,
            Advertisement__c.SeekSalaryMin__c,
            Advertisement__c.SeekSalaryMax__c,
            Advertisement__c.Salary_description__c,
            Advertisement__c.No_salary_information__c,
            Advertisement__c.Has_Referral_Fee__c,
            Advertisement__c.Referral_Fee__c,
            Advertisement__c.Terms_And_Conditions_Url__c,
            Advertisement__c.Status__c,
            Advertisement__c.Job_Posting_Status__c,
            Advertisement__c.JobXML__c,
            Advertisement__c.Prefer_Email_Address__c,
            Advertisement__c.RecordTypeId,
            Advertisement__c.Website__C
      };

      fflib_SecurityUtils.checkInsert(Advertisement__c.SObjectType, fieldList);
      fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
    } 
}