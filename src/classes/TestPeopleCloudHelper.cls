/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 *
 * @Author Raymond
 * This class is used to test PeopleCloudHelper class
 */
@isTest
private class TestPeopleCloudHelper {
	
	
    @testSetup static void setUpTestData() {
    	
    	
    	//User testUser = DummyRecordCreator.platformUser;
    	
    	
    	
    	DummyRecordCreator.DefaultDummyRecordSet dms = DummyRecordCreator.createDefaultDummyRecordSet();
    	
    	
        
        CandidateManagementDummyRecordCreator cmRecord = new CandidateManagementDummyRecordCreator();
        Advertisement__c ad = new AdvertisementDummyRecordCreator().generateOneAdvertisementDummyRecord(dms);
        
        List<Placement_Candidate__c> updateCMs = new List<Placement_Candidate__c>();
        List<Placement_Candidate__c> cms = cmRecord.generateCandidateManagementDummyRecord(dms);
        for(Placement_Candidate__c cm : cms) {
            cm.Online_Ad__c = ad.Id;
            updateCMs.add(cm);
        }
        update updateCMs;
        system.assertNotEquals(null, updateCMs);
    	
    }
    
  	public static testmethod void testsmsfunctionavailable(){
  	    
  		PCAppSwitch__c pcSwitch= PCAppSwitch__c.getOrgDefaults();
  		pcSwitch.Enable_Send_Bulk_SMS__c = true;
  		upsert pcSwitch;
  		
      	System.runAs(DummyRecordCreator.platformUser) {
      		Boolean smsavailable = PeopleCloudhelper.smsfunctionavailable();
      		system.assert(smsavailable);
  	    }
  	}
  
  	public static testmethod void testbulkemilavailable(){
      	
  		PCAppSwitch__c pcSwitch= PCAppSwitch__c.getOrgDefaults();
  		pcSwitch.Enable_Bulk_Email__c = true;
  		upsert pcSwitch;
  		Boolean bulkemailavailable = PeopleCloudhelper.bulkemilavailable();
  		system.assert(bulkemailavailable);
  		pcSwitch.Features__c = 'GoogleSearchOperator';
  		upsert pcSwitch;
  		
      	System.runAs(DummyRecordCreator.platformUser) {
      		bulkemailavailable = PeopleCloudhelper.featuresavailable('GoogleSearchOperator');
      		system.assert(bulkemailavailable);
  	    }
  	}
  	
  	static testMethod void testAddToOtherVacanciesAvailable() {
  		System.runAs(DummyRecordCreator.platformUser) {
  	    system.assertNotEquals(null, PeopleCloudHelper.addToOtherVacanciesAvailable());
  		}
  	}
  	
  	/*static testMethod void testConfigureSetting() {
  	    PeopleCloudHelper.configureSetting();
        List<Document> docs = [Select Name , DeveloperName from Document where DeveloperName = 'Trigger_Admin' and FolderId =: UserInfo.getUserId()];
        system.assertEquals(docs.size(), 1);
        system.assertEquals(docs[0].Name, 'TriggerAdminForTesting');
  	}*/
  	
  	static testMethod void testCreatePlatformUser() {
  		
  	    system.assertNotEquals(null, PeopleCloudHelper.createPlatformUser());
  		
  	}
  	
  	static testMethod void testCreateCandidatePool() {
  		System.runAs(DummyRecordCreator.platformUser) {
  	    system.assertNotEquals(null, PeopleCloudHelper.createCandidatePool());
  		}
  	}
  	
  	static testMethod void testAttributes() {
  		
  		PeopleCloudHelper.configureSetting();
  		
  		System.runAs(DummyRecordCreator.platformUser) {
  	    
  	    
  	    system.assertNotEquals(null, PeopleCloudHelper.ctcAdminFolder);
  	    //system.assertNotEquals(null, PeopleCloudHelper.triggerAdminConfigFile);
  	    
  	    PeopleCloudHelper.PeopleCloudTestDataSet ps = new PeopleCloudHelper.PeopleCloudTestDataSet();
  	    system.assertNotEquals(null, ps);
  	    
  	    PeopleCloudHelper.Application app = new PeopleCloudHelper.Application();
  	    system.assertNotEquals(null, app);
  	    
  	    PeopleCloudHelper.ApplicationList al = new PeopleCloudHelper.ApplicationList();
  	    system.assertNotEquals(null, al);
  	    system.assertNotEquals(null, al.getApplicationMap());
  		}
  	}
  	
  	static testMethod void testGetS3CredentialFromCustomSetting() {
  		
  	    system.assertNotEquals(null, PeopleCloudHelper.getS3CredentialFromCustomSetting());
  		
  	}
  	
  	static testMethod void testPrepareData() {
  		System.runAs(DummyRecordCreator.platformUser) {
  	    system.assertNotEquals(null, PeopleCloudHelper.prepareData());
  		}
  	}
  	
  	static testMethod void testGetMaxJobApplicationId() {
  		System.runAs(DummyRecordCreator.platformUser) {
  	    system.assertNotEquals(null, PeopleCloudHelper.getMaxJobApplicationId());
  		}
  	}
  	
  	static testMethod void testCandidateApplyJob() {
  		System.runAs(DummyRecordCreator.platformUser) {
        List<Advertisement__c> ads = [select id, Vacancy__c from Advertisement__c];
  	    system.assertNotEquals(null, PeopleCloudHelper.candidateApplyJob(ads[0], new Map<String,Object>(), null, true));
  		}
  	}
  	
  	static testMethod void testCandidatesApplyJob() {
  		System.runAs(DummyRecordCreator.platformUser) {
        List<Advertisement__c> ads = [select id, Vacancy__c from Advertisement__c];
  	    system.assertNotEquals(null, PeopleCloudHelper.candidatesApplyJob(1, ads[0], new Map<String,Object>(), new List<Skill__c>(), true));
  		}
  	}
  	
  	static testMethod void testDuplicateCandidatesApplyJob() {
  		System.runAs(DummyRecordCreator.platformUser) {
        List<Advertisement__c> ads = [select id, Vacancy__c from Advertisement__c];
  	    PeopleCloudHelper.Application app = PeopleCloudHelper.candidateApplyJob(ads[0], new Map<String,Object>(), null, true);
  	    system.assertNotEquals(null, PeopleCloudHelper.duplicateCandidatesApplyJob(1, app, ads[0], new Map<String,Object>(), new List<Skill__c>(), true, true));
  		}
  	}
}