public with sharing class ActivityDummyRecordCreator {
	private List<Task> tasks {get; set;}
	private Set<Id> taskIdSet {get; set;}
    private Map<Id, Task> taskMap {get; set;}

    public ActivityDummyRecordCreator() {
		tasks = new List<Task>();
		taskIdSet = new Set<Id>();
		taskMap = new Map<Id, Task>();
	}

	public List<Task> generateActivityDummyRecord(DummyRecordCreator.DefaultDummyRecordSet rs) {
		for(Integer i=0;i<rs.vacancies.size();i++) {
			Task ts = new Task(whoId=rs.candidates[i].Id , whatId=rs.vacancies[i].Id, OwnerId=DummyRecordCreator.platformUser.Id);
			tasks.add(ts);
		}
		checkFLS();
		insert tasks;
		return tasks;
	}


	// Generate only one advertisement 
	public Task generateOneActivityDummyRecord(DummyRecordCreator.DefaultDummyRecordSet rs) {
		Task t = new Task(whoId=rs.candidates[0].Id , whatId=rs.vacancies[0].Id, OwnerId=DummyRecordCreator.platformUser.Id);
		checkFLS();
		insert t;
		return t;
	}

	private void checkFLS(){
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			Task.WhoId,
			Task.WhatId,
			Task.OwnerId
		};
		fflib_SecurityUtils.checkInsert(Task.SObjectType, fieldList);
	}


}