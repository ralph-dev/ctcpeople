@isTest
public with sharing class DataTestFactory {
    public static Integer recordNumber=10;
    private static list<Account> accounts=new list<Account>();
    private static list<Contact> contacts=new list<Contact>();
    private static list<Placement__c> vacancies=new list<Placement__c>();
    private static list<Contact> cands=new list<Contact>();
    private static list<Placement_Candidate__c> candidateManagements=new list<Placement_Candidate__c>();
    private static list<CTCPeopleSettingHelper__c> ctcPeopleSettingHelpers=new list<CTCPeopleSettingHelper__c>();
    private static list<Workplace__c> places=new list<Workplace__c>();
    private static StyleCategory__c apAccount;
    private static list<Invoice_Line_Item__c> invoiceLineItems=new list<Invoice_Line_Item__c>();
    private static list<Rate__c> rates=new list<Rate__c>();
    public static String ABNContainsLetter='134539uj783';
    public static String ABNLengthNotEqualToEvelen='291209120931237';
    public static String ABNWeightingSumModEightNineReminderNotEqualZero='45 004 189 707';
    public static String ABNNull='';
    public static String ABNStartWithZero='05 004 189 707';
    public static String ABNValid='45 004 189 708';
    public static Skill__c SKILL = new Skill__c(Name='Java');
    public static List<Candidate_Skill__c> CANDIDATE_SKILL= new List<Candidate_Skill__c>();
    public static void upsertABNTriggerSwitchCSField(){
        PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();

        if(pcSwitch==null){

        pcSwitch=new PCAppSwitch__c();

        }

        pcSwitch.Turn_On_ABN_Validator_Switch__c = true;
        upsert pcSwitch;
    }
    public static List<Account> createAccountList(){
        List<Account> accInputList=new List<Account>();
        Account acc1=new Account();
        acc1.name='Test Account 1';
        acc1.ABN__c='45004189708';
        accInputList.add(acc1);

        Account acc2=new Account();
        acc2.name='Test Account 2';
        acc2.ABN__c='88143526096';
        accInputList.add(acc2);

        Account acc3=new Account();
        acc3.name='Test Account 3';
        acc3.ABN__c='45004189708a';
        accInputList.add(acc3);

        Account acc4=new Account();
        acc4.name='Test Account 4';
        acc4.ABN__c='';
        accInputList.add(acc4);
        
       
        insert accInputList;
        
      
        return accInputList;
    }
    public static list<Account> createAccounts(){
        createStyleCategory();
        if(accounts.size()==0){
            for(Integer i=0;i<recordNumber;i++){
                Account accountRec=new Account();
                accountRec.name='Test Name'+i;
                accountRec.ABN__c='83 914 571 673';
                accounts.add(accountRec);
            }
            
            
             CommonSelector.quickInsert( accounts);
        }
        return accounts;
    }
    public static list<Contact> createContacts(){
        createStyleCategory();
        if(contacts.size()==0){
            for(Integer i=0;i<recordNumber;i++){
                Contact contRec=new Contact();
                contRec.firstName='testFirstName';
                contRec.lastName='TestLastName'+i;
                contRec.Astute_Payroll_Upload_Status__c='On Hold';
                contRec.AccountId=createAccounts().get(i).Id;
                contacts.add(contRec);
            }
            
             CommonSelector.quickInsert( contacts);
        }
        return contacts;
    }
    public static list<Contact> createCands(){
        createStyleCategory();
        if(cands.size()==0){
            for(Integer i=0;i<recordNumber;i++){
                Contact contRec=new Contact();
                contRec.firstName='testFirstName';
                contRec.lastName='TestLastName'+i;
                contRec.email='TestEmail@email.com';
                contRec.MobilePhone = '040404040'+i;
                cands.add(contRec);
            }
            
            
            CommonSelector.quickInsert( cands );
        }
        return cands;
    }
    
    public static list<Invoice_Line_Item__c> createInvoiceLineItems(){
        createStyleCategory();
        if(invoiceLineItems.size()==0){
            for(Integer i=0;i<recordNumber;i++){
                Invoice_Line_Item__c invoiceLineItem=new Invoice_Line_Item__c();
                invoiceLineItem.Account__c=createAccounts().get(i).Id;
                invoiceLineItem.Billing_Contact__c=createContacts().get(i).Id;
                invoiceLineItem.Vacancy__c=createVacancies().get(i).Id;
                invoiceLineItem.Invoice_Item_Id__c=12;
                invoiceLineItems.add(invoiceLineItem);
            }
           
            
            CommonSelector.quickInsert( invoiceLineItems);
        }
        return invoiceLineItems;
    }
    public static list<Placement__c> createVacancies(){
        createStyleCategory();
        if(vacancies.size()==0){
            for(Integer i=0;i<recordNumber;i++){
                Placement__c vacancy=new Placement__c();
                vacancy.Biller_Contact__c=createContacts().get(i).ID;
                vacancy.Company__c=createAccounts().get(i).Id;
                vacancy.Vacancy_Contact__c=createContacts().get(i).Id;
                vacancy.Base_Salary__c=20;
                vacancies.add(vacancy);
            }
            
            
            CommonSelector.quickInsert( vacancies);
        }
        return vacancies;
    }
    public static list<Workplace__c> createWorkplaces(){
        createStyleCategory();
        if(places.size()==0){
            for(Integer i=0;i<recordNumber;i++){
                Workplace__c place=new Workplace__c();
                place.Workplace_Id__c=i;
                place.Name='office';
                place.Postcode__c='2000';
                place.Postcode__c='2000';
                place.State__c='NSW, AU';
                places.add(place);
            }
           
           
             CommonSelector.quickInsert( places);
            
        }
        return places;
        
    }
    public static StyleCategory__c createStyleCategory(){
        if(apAccount==null){
            apAccount=new StyleCategory__c(name='test');
            
            /**
            * modified by andy for security review II
            */
            //apAccount.Astute_Payroll_Api_Key__c='ApiKey';
            //apAccount.Astute_Payroll_Api_Password__c='password';
            //apAccount.AstutePayroll_api_username__c='userName';
            
            
            CommonSelector.quickInsert( apAccount);
            CredentialSettingService.insertAstutePayrollCredential(apAccount.Id,'userName','password','ApiKey');
        }
        return apAccount;
    }
    public static list<Placement_Candidate__c> createCandidateManagements(){
        createStyleCategory();
        if(candidateManagements.size()==0){
            for(Integer i=0;i<recordNumber;i++){
                Placement_Candidate__c candidateManagement=new Placement_Candidate__c();
                candidateManagement.candidate__c=createCands().get(i).Id;
                candidateManagement.Placement__c=createVacancies().get(i).Id;
                candidateManagement.Astute_Payroll_Upload_Status__c='On Hold';
                candidateManagement.Employee_User_Name__c='Test';
                candidateManagement.Approver__c=createContacts().get(i).Id;
                candidateManagement.Work_place__c=createWorkplaces().get(i).Id;
                candidateManagements.add(candidateManagement);
            }
           
           
             CommonSelector.quickInsert( candidateManagements);
        }
        return candidateManagements;
    }
    
    public static list<Rate__c> createRates(){
        createStyleCategory();
        if(rates.size()==0){
            for(Integer i=0;i<recordNumber;i++){
                Rate__c rate=new Rate__c();
                rate.Placement__c=createCandidateManagements().get(i).Id;
                rate.Start_date__c=Date.today();
                rate.End_Date__c=Date.today();
                rate.Pay_charge_rate__c=20;
                rate.Pay_oncosts__c=10;
                rate.Pay_Rate__c=4;
                rates.add(rate);
            }
           
            
             CommonSelector.quickInsert( rates);
            
        }
        return rates;
    }
    
    public static CTCPeopleSettingHelper__c createResumeAndFilesDocTypeMap(){
        list<String> rTDevNameList = new list<String>();
        rTDevNameList.add('Resume_And_Files_Doc_Type_Mapping');
        RecordTypeSelector recordSelector=new RecordTypeSelector();
        list<RecordType> rTList = recordSelector.fetchRecordTypesByName(rTDevNameList);
        CTCPeopleSettingHelper__c map1=new CTCPeopleSettingHelper__c();
        map1.RecordTypeId = rTList.get(0).id;
        map1.Name = 'Test';
        
        
        CommonSelector.quickInsert( map1 );
        return map1;
        
    }
    public static list<CTCPeopleSettingHelper__c> createCTCPeopleSettings(){
        createStyleCategory();
        if(ctcPeopleSettingHelpers.size()==0){
            list<String> recordTypeValues=new list<String>();
            recordTypeValues.add('Approver_Mapping');
            recordTypeValues.add('Biller_Contacts_Mapping');
            recordTypeValues.add('Biller_Mapping');
            recordTypeValues.add('Company_Entity');
            recordTypeValues.add('Owner_Mapping');
            recordTypeValues.add('Placement_Mapping');
            recordTypeValues.add('Employee_Mapping');
            recordTypeValues.add('Rate_Card');
            recordTypeValues.add('Rate_Change');
            recordTypeValues.add('Rule_Group');
            RecordTypeSelector recordSelector=new RecordTypeSelector();
            list<RecordType> recordTypes=
                recordSelector.fetchRecordTypesByName(recordTypeValues);
            map<String,String> rTMaps=new map<String,String>();
            list<String> rTIdValues=new list<String>();
            for(RecordType rTRecord: recordTypes){
                rTMaps.put(rTRecord.developerName,rTRecord.Id);
                rTIdValues.add(rTRecord.Id);
            }
            CTCPeopleSettingHelper__c ctcSettingApprover=new CTCPeopleSettingHelper__c();
            ctcSettingApprover.AP_Approver_Fields__c='gender';
            ctcSettingApprover.name='name';
            ctcSettingApprover.CTCPeopleSetting__c=createStyleCategory().ID;
            ctcSettingApprover.RecordTypeId=rTMaps.get('Approver_Mapping');
            ctcPeopleSettingHelpers.add(ctcSettingApprover);
            CTCPeopleSettingHelper__c ctcSettingBiller=new CTCPeopleSettingHelper__c();
            ctcSettingBiller.AP_Biller_Fields__c='abn';
            ctcSettingBiller.name='name';
            ctcSettingBiller.CTCPeopleSetting__c=createStyleCategory().ID;
            ctcSettingBiller.RecordTypeId=rTMaps.get('Biller_Mapping');
            ctcPeopleSettingHelpers.add(ctcSettingBiller);
            CTCPeopleSettingHelper__c ctcSettingBillerContact=new CTCPeopleSettingHelper__c();
            ctcSettingBillerContact.AP_Biller_Contact_Fields__c='email';
            ctcSettingBillerContact.name='email';
            ctcSettingBillerContact.CTCPeopleSetting__c=createStyleCategory().ID;
            ctcSettingBillerContact.RecordTypeId=rTMaps.get('Biller_Contacts_Mapping');
            ctcPeopleSettingHelpers.add(ctcSettingBillerContact);
            CTCPeopleSettingHelper__c ctcSettingOwner=new CTCPeopleSettingHelper__c();
            ctcSettingOwner.AP_Owner_Fields__c='email';
            ctcSettingOwner.name='email';
            ctcSettingOwner.CTCPeopleSetting__c=createStyleCategory().ID;
            ctcSettingOwner.RecordTypeId=rTMaps.get('Owner_Mapping');
            ctcPeopleSettingHelpers.add(ctcSettingOwner);
            CTCPeopleSettingHelper__c ctcSettingPlacement=new CTCPeopleSettingHelper__c();
            ctcSettingPlacement.AP_Employee_Fields__c='';
            ctcSettingPlacement.name='name';
            ctcSettingPlacement.CTCPeopleSetting__c=createStyleCategory().ID;
            ctcSettingPlacement.RecordTypeId=rTMaps.get('Placement_Mapping');
            ctcPeopleSettingHelpers.add(ctcSettingPlacement);
            CTCPeopleSettingHelper__c ctcSettingCand=new CTCPeopleSettingHelper__c();
            ctcSettingCand.AP_Employee_Fields__c='address_street';
            ctcSettingCand.name='email';
            ctcSettingCand.CTCPeopleSetting__c=createStyleCategory().ID;
            ctcSettingCand.RecordTypeId=rTMaps.get('Employee_Mapping');
            ctcPeopleSettingHelpers.add(ctcSettingCand);
            
           
            CommonSelector.quickInsert( ctcPeopleSettingHelpers);
            
        }
        return ctcPeopleSettingHelpers;
        
    }

    public static List<Candidate_Skill__c> createCandidatesSkill() {
        if(CANDIDATE_SKILL.size() == 0) {
            for(Integer i=0;i<recordNumber;i++){
                Candidate_Skill__c cSkill = new Candidate_Skill__c(Skill__c=SKILL.Id,Candidate__c=createCands().get(i).Id);
                CANDIDATE_SKILL.add(cSkill);
            }
        }
        return CANDIDATE_SKILL;
    }
    public static Advertisement__c createAdvertisement(){
        Advertisement__c adv = new Advertisement__c();
        List<Placement__c> vacList = createVacancies();
        adv.Vacancy__c = vacList[0].Id;
        
        CommonSelector.quickInsert( adv);
        return adv;
    }
    public static List<Skill_Group__c> createSkillGroup(){
        List<Skill_Group__c> sgList = new List<Skill_Group__c>();
        for(Integer i=0;i<recordNumber;i++){
            Skill_Group__c sg = new Skill_Group__c();
            sg.name = 'TestSkillGroup' + i;
            sg.Enabled__c = true;
            sg.Skill_Group_External_Id__c = '1' + i;
            sgList.add(sg);
        }
        
        CommonSelector.quickInsert( sgList);
        return sgList;
    }
    public static List<Skill__c> createSkills(Skill_Group__c sg){
        List<Skill__c> skillList = new List<Skill__c>();
        for(Integer i=0;i<recordNumber;i++){
            Skill__c sk = new Skill__c();
            sk.name = 'Test' + i;
            sk.Skill_Group__c = sg.Id;
            sk.Ext_Id__c = String.valueof(i);
            skillList.add(sk);
        }
       
        
        CommonSelector.quickInsert( skillList);
        return skillList;
    }
    public static User createUser(){
         User u = new User(Username='ctcProperty75869037476@clicktocloud.com',
                    LastName='ln',Email='admin@clicktocloud.com', Alias='ctcprpt1',
                    CommunityNickname='ctcProperty75869037476@clicktocloud.com',
                    TimeZoneSidKey='Australia/Sydney', LocaleSidKey='en_AU',
                    EmailEncodingKey='ISO-8859-1', LanguageLocaleKey='en_US',
                    AmazonS3_Folder__c= 'test', isActive=true);
         return u;
    }
    public static User getCurrentUser(){
        String UserId = UserInfo.getUserId();
        User u =  (User)new CommonSelector(User.sObjectType)
        			.setParam('UserId',UserId)
        			.getOne(
        				'AmazonS3_Folder__c,Seek_Account__c',
        				'Id =: UserId');//[select AmazonS3_Folder__c,Seek_Account__c from User where Id =: UserId];
        return u;
    }

    /**
     * 
     * The following test data were mainly used by PeopleCloud1.PlacementCandidateSelectorTest
     * Added by Lina
     * 
    **/ 
    public static Account createSingleAccount() {
        Account acc = new Account(Name='Test Account');
        //check FLS
       
        CommonSelector.quickInsert( acc );
        return acc;
    }
    
    public static Contact createSingleContact() {
        Contact con = new Contact(LastName='test',email='test@test.com');
        
        
        CommonSelector.quickInsert( con );
        return con;
    }
    
    public static List<Contact> createContactList(Integer num) {
        List<Contact> conList = new List<Contact>();
        for (Integer i = 0; i < num; i ++) {
            Contact con = new Contact(LastName='test'+i,email='test'+i+'@test.com');
            conList.add(con);
        }
        
        
        CommonSelector.quickInsert( conList );
        return conList;
    }
    
    public static List<String> createContactListIds(Integer num) {
        List<Contact> conList = createContactList(num);
        List<String> conIdList = new List<String>();
        for (Contact con : conList) {
            conIdList.add(con.Id);
        }
        return conIdList;
    }
    
    public static Placement__c createSingleVacancy() {
        Account acc = createSingleAccount();
        Placement__c vacancy = new Placement__c(Name='Test Vacancy', Company__c=acc.Id);
       
        CommonSelector.quickInsert( vacancy);
        return vacancy;
    }
    
    public static List<Placement__c> createVacancyList(Integer num) {
        Account acc = createSingleAccount();
        List<Placement__c> vacancyList = new List<Placement__c>();
        for (Integer i = 0; i < num; i ++) {
            Placement__c vacancy = new Placement__c(Name='Test Vacancy' + i, Company__c=acc.Id);
            vacancyList.add(vacancy);
        }
       
        CommonSelector.quickInsert( vacancyList);
        return vacancyList;
    }
    
    public static List<String> createVacancyListIds(Integer num) {
        List<Placement__c> vacancyList = createVacancyList(num);
        List<String> vacancyIdList = new List<String>();
        for (Placement__c vacancy : vacancyList) {
            vacancyIdList.add(vacancy.Id);
        }
        return vacancyIdList;
    }
    
    public static List<Placement_Candidate__c> createCandidateManagementListForSameVacancy(String vacancyId, List<String> contactIds) {
        List<Placement_Candidate__c> cmList = new List<Placement_Candidate__c>();
        for (String conId : contactIds) {
             Placement_Candidate__c cm = new Placement_Candidate__c(candidate__c=conId, Placement__c=vacancyId);
             cmList.add(cm);
        }
        
        CommonSelector.quickInsert( cmList );
        return cmList;
    }
    
    public static List<Placement_Candidate__c> createCandidateManagementListForSameCandidate(List<String> vacancyIds, String contactId) {
        List<Placement_Candidate__c> cmList = new List<Placement_Candidate__c>();
        for (String vacancyId : vacancyIds) {
             Placement_Candidate__c cm = new Placement_Candidate__c(candidate__c=contactId, Placement__c=vacancyId);
             cmList.add(cm);
        }
        
        CommonSelector.quickInsert( cmList);
        return cmList;
    }    


    /**
     * 
     * The following test data were mainly used by PeopleCloud1.TestSendResumeController
     * Added by Lina
     * 
    **/ 
	public static Contact createCorpContactWithAccount(){
	    Account acc = createSingleAccount();
		RecordType newConRecordType = DaoRecordType.corpContactRT;
		Contact con = new Contact(LastName='testCorpContact', RecordTypeId=newConRecordType.Id, AccountId=acc.Id);
        
		CommonSelector.quickInsert( con);
		return con;
	}
	
	public static Contact createCorpContactWithAccountAndEmail(){
	    Account acc = createSingleAccount();
		RecordType newConRecordType = DaoRecordType.corpContactRT;
		Contact con = new Contact(LastName='testCorpContactWithEmail', RecordTypeId=newConRecordType.Id, AccountId=acc.Id, Email='corpContact@test.com');	
		
        CommonSelector.quickInsert( con);
		return con;
	}	
	
	public static Contact CreateCandidate(){
		RecordType newCanRecordType = DaoRecordType.indCandidateRT;
		Contact can = new Contact(Lastname='testCandidate', RecordTypeId=newCanRecordType.Id);
        
		CommonSelector.quickInsert( can);
		return can;
	}
	
	public static Contact CreateCandidateWithAccount(){
	    Account acc = createSingleAccount();
		RecordType newCanRecordType = DaoRecordType.indCandidateRT;
		Contact can = new Contact(Lastname='testCandAcc', RecordTypeId=newCanRecordType.Id, AccountId=acc.Id, Email='test@test.com');
        
		CommonSelector.quickInsert( can );
		return can;
	}	
	
	public static Placement_Candidate__c createCandiateManangement() {
	    Placement__c vacancy = createSingleVacancy();
	    Contact con = createSingleContact();
	    Placement_Candidate__c cm = new Placement_Candidate__c(candidate__c=con.Id, Placement__c=vacancy.Id);
       
	    CommonSelector.quickInsert( cm );
	    return cm;
	}
	
    public static Document createSRDummyDocument(){
    	Document tempdoc = new Document();
    	tempdoc.Name = 'SendResumeDummyDocument';
    	tempdoc.developerName = 'SR_' + UserInfo.getUserId();
    	tempdoc.Body = Blob.valueOf('test');
    	tempdoc.FolderId = UserInfo.getUserId();
       
    	CommonSelector.quickInsert( tempdoc );
        return tempdoc;
    }
    
    public static Web_Document__c createWebDocument(Contact con) {
        Web_Document__c newdoc = new Web_Document__c();
    	newdoc.S3_Folder__c = 'test';
    	newdoc.ObjectKey__c = 'testObjKey';
    	newdoc.Document_Related_To__c = con.Id;
    	newdoc.Document_Type__c = 'Resume';
    	newdoc.Name = 'test.doc';
       
    	CommonSelector.quickInsert( newdoc);
    	return newdoc;
    }
    
    public static Web_Document__c createUserWebDocument() {
        Web_Document__c newdoc = new Web_Document__c();
    	newdoc.S3_Folder__c = 'test';
    	newdoc.ObjectKey__c = 'testObjKey';
    	newdoc.User__c = UserInfo.getUserId();
    	newdoc.Document_Type__c = 'Resume';
    	newdoc.Name = 'test.doc';
       
    	CommonSelector.quickInsert( newdoc);
    	return newdoc;
    }
    
    public static List<Web_Document__c> createResumeForUsr(String usrId) {
        List<Web_Document__c> resumeList = new List<Web_Document__c>();
        for(Integer i = 0; i < recordNumber; i ++) {
            String size = (512 * i) + '' ;
            size = Math.mod(i, 2) == 0 ? '5.6 MB' : size;
            size = Math.mod(i, 3) == 0 ? '15.7 KB' : size;
            size = Math.mod(i, 4) == 0 ? '550 bytes' : size;
            size = Math.mod(i, 5) == 0 ? '1.11 GB' : size;
           
            Web_Document__c resume = new Web_Document__c(Name='Test Resume'+i, User__c=usrId, File_Size__c = size);
            resumeList.add(resume);
        }
        
        CommonSelector.quickInsert( resumeList);
        return resumeList;
    }
    
    public static String getProfileId(String profileName) {
        Profile p =   (Profile) new CommonSelector(Profile.sObjectType)
        				.setParam('profileName',profileName)
        				.getOne('Id', 'name =: profileName ');//[select Id from Profile where name =: profileName].Id;
   		return p.Id;
    } 
}