/**
 * This is a selector class for Web_Document__c Object
 * 
 * Created by: Lina
 * Created on: 14/07/2016
 * 
 **/
 
public with sharing class WebDocumentSelector extends CommonSelector{
    
    final static String QUERY_STRING = 'SELECT Id, Name, File_Size__c, S3_Folder__c, ObjectKey__c, Default_Doc__c, CreatedDate, Document_Type__c, Document_Related_To__c, User__c FROM Web_Document__c';

    public WebDocumentSelector(){
        super(Web_Document__c.SObjectType);
    }

    /**
     * Select a list of Web_Document__c with Id specified
     * @param docIds
    **/   
    public static List<Web_Document__c> getWebDocsByIds(List<String> docIds) {
        String query = QUERY_STRING + ' WHERE Id in: docIds';
        CommonSelector.checkRead(Web_Document__c.SObjectType,'Id, Name, File_Size__c, S3_Folder__c, ObjectKey__c, Default_Doc__c, CreatedDate, Document_Type__c, Document_Related_To__c, User__c');
        List<Web_Document__c> result = Database.query(query);
        return result;
    }    

}