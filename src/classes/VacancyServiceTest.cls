@isTest
private class VacancyServiceTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        VacancyDTO vacDTO = TestDataFactoryRoster.createVacancyDTO(acc,vac);
        
        List<Placement__c> vacList = new List<Placement__c>();
        vacList.add(vac);
        
        List<VacancyDTO> vacDTOList = new List<VacancyDTO>();
        vacDTOList.add(vacDTO);
        
        VacancyService service = new VacancyService();
        
        //test method VacancyService.upsertVacancyFromWizard(List<VacancyDTO> vacDTOList)
        List<Placement__c> vacListUpserted = service.upsertVacancyFromWizard(vacDTOList);
        System.assertNotEquals(vacListUpserted.size(), 0);
    	}
    }
}