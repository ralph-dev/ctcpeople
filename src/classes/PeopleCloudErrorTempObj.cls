/********************************************************************
PeopleCloudErrorTempObj class contains the error message
Translate the error message from PeopleCloudErrorInfo to JSON string
And transfer this JSON string to JS by remote JS
Author by Jack, 07-02-2013
********************************************************************/

public class PeopleCloudErrorTempObj {
	public String errortype{get;set;}
	public String errorMessage{get;set;}
	public String errorCode{get;set;}
	public String recordid{get;set;}
	
}