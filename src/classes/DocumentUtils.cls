/*

	This class is for create document in SF.
	Created by Jack on 02 Oct 2013

*/
public with sharing class DocumentUtils {
	//Create Document at SF
	public static String CreateDocument(String docName, Blob docbody, String DeveloperName, String FolderId){
		Document tempdoc = new Document();
		try{			
			tempdoc.Name = docName;
			tempdoc.Body = docbody; 					
			tempdoc.DeveloperName = DeveloperName;
			tempdoc.FolderId = FolderId;
			//check FLS
			List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
				Document.Name,
				Document.Body,
				Document.DeveloperName,
				Document.FolderId
			};
			fflib_SecurityUtils.checkInsert(Document.SObjectType, fieldList);
			insert tempdoc;		
		}
		catch(Exception e){
			
		}
		return tempdoc.id;	    	
	}	
}