@isTest
private class RosterServiceTest {
	final static String ROSTER_STATUS_PENDING = 'Pending';
	
	@isTest
    static void getTotalDisplayRosterListTest() {
        // TO DO: implement unit test
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
    	List<Days_Unavailable__c> rosters = 
    		TestDataFactoryRoster.createRostersForSplitRosterFunction(con, shift, acc,ROSTER_STATUS_PENDING);
    	RosterService rService = new RosterService();
    	List<Days_Unavailable__c> rosterSplited = rService.getTotalDisplayRosterList(rosters);
    	System.assertNotEquals(rosterSplited,null);
    }
    
    @isTest
    static void updateRosterStoredInDBTest() {
        // TO DO: implement unit test
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        Days_Unavailable__c roster = 
        	TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_PENDING);
        RosterDTO rDTO = 
        	TestDataFactoryRoster.createRosterDTO(acc, con, shift, roster,ROSTER_STATUS_PENDING);
        	 
        List<RosterDTO> rDTOList = new List<RosterDTO>();
        rDTOList.add(rDTO);
        
        RosterService rService = new RosterService();
        List<Days_Unavailable__c> rosterReturned = rService.updateRosterStoredInDB(rDTOList);
        System.assertNotEquals(rosterReturned,null);
    }
    
    @isTest
    static void deleteRosterByIdTest() {
    	RosterService rService = new RosterService();
    	Boolean isSuccessDeleted = rService.deleteRosterById('abc');
    	System.assertNotEquals(isSuccessDeleted,true);
    	
    }
    
    @isTest
    static void getUnavailableDateStrListForCandidateTest(){
    	RosterService rService = new RosterService();
    	List<String> dateStrList = rService.getUnavailableDateStrListForCandidate('abc');
    	System.assertEquals(dateStrList.size(),0);
    }
    
    @isTest
    static void updateRosterWhenShiftCertianFieldsChangeTest(){
    	Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        Contact con = TestDataFactoryRoster.createContact();
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        Days_Unavailable__c roster = 
        	TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_PENDING);
        List<String> shiftIdList = new List<String>();
        shiftIdList.add(shift.Id);
        RosterService rService = new RosterService();
        rService.updateRosterWhenShiftCertianFieldsChange(shiftIdList);
        system.assert([select id from Shift__c].size()>0);
    }
}