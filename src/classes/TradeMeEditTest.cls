@isTest
private class TradeMeEditTest {
	public testmethod static void testPost(){
		System.runAs(DummyRecordCreator.platformUser) {
	    StyleCategory__c sc = DataTestFactory.createStyleCategory();
		sc.OfficeCodes__c = '2';
		sc.WebSite__c='Trademe';
		SC.Header_EC2_File__c = 'Test';
		sc.Header_File_Type__c = 'Test';
		sc.Div_Html_EC2_File__c = 'Test';
		sc.Div_Html_File_Type__c = 'Test';
		update sc;
    	List<Placement__c> vacList = DataTestFactory.createVacancies();
    	Advertisement__c ad = DataTestFactory.createAdvertisement();
    	ad.WebSite__c='a';	 ad.Location__c='a'; 	ad.SearchArea_Label_String__c='a';
		ad.Classification__c='a'; 	ad.Vacancy__c=vacList[0].id; 	ad.SubClassification2__c='a';
		ad.Classification2__c='a';	ad.SubClassification_Seek_Label__c='a';	 ad.WorkType__c='a';
		ad.Reference_No__c='a';		ad.Job_Title__c='a';		ad.Company_Name__c='a';
		ad.Template__c='a';		ad.Application_Form_Style__c=sc.id;	ad.Bullet_1__c='a';		
		ad.Bullet_2__c='a';		ad.Bullet_3__c='a';			ad.Job_Type__c='a';	
		ad.Residency_Required__c=true;		ad.Search_Tags__c='a';		ad.Street_Adress__c='a';
		ad.Nearest_Transport__c='a';		ad.Job_Contact_Name__c='a';		ad.Job_Contact_Phone__c='12345';
		ad.Location_Hide__c =false;			ad.Salary_Type__c='a';			ad.SeekSalaryMin__c='123';
		ad.SeekSalaryMax__c='2353'; 		ad.Salary_Description__c='a';	ad.No_salary_information__c=false;
		ad.Job_Content__c='a';			ad.Has_Referral_Fee__c='a';	ad.Referral_Fee__c=500;
		ad.Terms_And_Conditions_Url__c = Endpoint.getPeoplecloudWebSite()+'';
		ad.Status__c = 'Expired';
		ad.Skill_Group_Criteria__c = 'criteria';
		update ad;
    	TradeMeEdit trademe = new TradeMeEdit(new ApexPages.StandardController(ad));
    	trademe.iec2ws = new EC2WebServiceTestImpl();
    	trademe.getItems(); 
    	trademe.getStyleOptions();
    	trademe.getPhotoItems(); 
    	trademe.getSelectSkillGroupList();
    	trademe.post();
    	Advertisement__c trademeAd = new Advertisement__c();
    	trademeAd = [select Status__c from Advertisement__c where id =: ad.id];
    	System.assertEquals(trademeAd.Status__c,'Active');
    	System.assertEquals(trademe.show_page,false);
    	ad.Status__c = 'not valid';
    	update ad;
    	trademe = new TradeMeEdit(new ApexPages.StandardController(ad));
    	trademe.iec2ws = new EC2WebServiceTestImpl();
    	trademe.enableskillparsing = true;
    	trademe.post();
		}
    }
    
    private testmethod static void testCustomerErrorMessage(){
    	Integer errorCode = PeopleCloudErrorInfo.INSERT_OR_UPDATE_DYNAMIC_APPLICATION_FORM_FAILED;
    	Boolean returnResult = TradeMeEdit.customerErrorMessage(errorCode);
    	System.assert(returnResult);
    }
}