/*

	JXTNZ classification page.

*/

public with sharing class JxtnzClaAdmin {
	public List<XmlDom.Element> elementList;
	public String[] selectedClassification; 
	public Document doc;
	public String msg{get; set;}
	public XmlDom dom ;
	public XmlDom.Element classificationsDom;
	public JxtnzClaAdmin(){
		selectedClassification = new String[]{};
	}
	
	public void init(){
		String devName='jobx_classifications';
		doc = DaoDocument.getCTCConfigFileByDevName(devName);
		dom = new XmlDom(doc.body.toString());
		classificationsDom = dom.getElementByTagName('classifications');
		elementList = dom.getElementsByTagName('classification');
		for(XmlDom.Element e : elementList){
			if(e.attributes.get('display')=='true'){
				selectedClassification.add(e.nodeValue);
			}
		}
	}
	
	public List<SelectOption> getClassifications(){
		List<SelectOption> options = new List<SelectOption>();
		for(XmlDom.Element e:elementList){
			options.add(new SelectOption(e.nodeValue, e.nodeValue));
		}
		return options;
	}
	public PageReference save(){
		for(XmlDom.Element e:elementList){
			for(String s:selectedClassification){
				if(JobBoardUtils.removeAmpersand(e.nodeValue)==JobBoardUtils.removeAmpersand(s)){
					e.attributes.put('display', 'true');
					e.nodeValue = JobBoardUtils.removeAmpersand(e.nodeValue);
					break;
				}else{
					e.attributes.put('display', 'false');
					e.nodeValue = JobBoardUtils.removeAmpersand(e.nodeValue);
					//system.debug('e = '+e.nodeValue);
				}
			}
		}
		
		//system.debug('dom String = '+classificationsDom.toXmlString());
		doc.body = Blob.valueOf(classificationsDom.toXmlString());

		// Check FLS to ensure user have update and insert permission on all Document fields.
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			Document.Type,
			Document.Name,
			Document.FolderId,
			Document.Body,
			Document.DeveloperName
		};
		fflib_SecurityUtils.checkInsert(Document.SobjectType, fieldList);
		fflib_SecurityUtils.checkUpdate(Document.SobjectType, fieldList);
		upsert doc;

		msg = 'The Jobx classification are saved successfully';
		return null;
	}
	public String[] getSelectedClassification(){
		return selectedClassification;
	}
	public void setSelectedClassification(String[] selectedClassification){
		this.selectedClassification = selectedClassification;
	}
}