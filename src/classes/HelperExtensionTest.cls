@isTest
private class HelperExtensionTest {

	private static testMethod void testGetUserPermission() {
		
		/**
		* added by andy for setting SQS credential
		*/
		SQSCredentialSelectorTest.createDummySQSCredential();
		
	    PCAppSwitch__c sw = new PCAppSwitch__c(Enable_Daxtra_Skill_Parsing__c=true );
	    insert sw;
        DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
        Skill_Group__c dsg = DummyRecordCreator.createDefaultSkillGroup();
	        
	    System.runAs(DummyRecordCreator.platformUser) {
	        
	        UserPermission up = HelperExtension.getUserPermission();
	        system.assert(up.skillParsingEnabled);    
	    }
	}

	static testMethod void testSaveNumOfRecordsShowing() {
		System.runAs(DummyRecordCreator.platformUser) {
			Integer numOfRecords = HelperExtension.saveNumOfRecordsShowing(20);
			System.assertEquals(20, numOfRecords);
			User u = new UserSelector().getCurrentUser();
			System.assertEquals(20, Integer.valueOf(u.Number_of_Records_Showing__c));
		}
	}

	static testMethod void testEmptyMethod() {
		HelperExtension.apiDescriptor();
		System.assert(true);
	}

}