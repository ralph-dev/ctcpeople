@isTest
private class DaoFolderTest {

	private static testMethod void test() {
		System.runAs(DummyRecordCreator.platformUser) {
        system.assertNotEquals(null, DaoFolder.getPublicFoldersByType('txt'));
        system.assertNotEquals(null, DaoFolder.getFolderByDevName('CTC_Admin_Folder'));
        system.assertNotEquals(null, DaoFolder.getFoldersByType('txt'));
        system.assertEquals(null, DaoFolder.getFolderByTypeAndId('txt', 'a049000000u5iOJ'));
        system.assertNotEquals(null, DaoFolder.getFolderByIds(new Set<Id>{'a049000000u5iOJ'}));
		}
	}

}