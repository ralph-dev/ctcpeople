public with sharing class AdvertisementUtilities {
	public AdvertisementUtilities() {}

	//** for xml string, convert special characters.
	public static String ConvertString(String originalstr){
        String finalstr = '';
        if(originalstr == null){
            return finalstr;
        }
        originalstr = originalstr.replaceAll('&(?!amp)','&amp;'); 
        originalstr = originalstr.replaceAll('\'','&#39;');
        originalstr = originalstr.replaceAll('\"','&quot;');
        originalstr = originalstr.replaceAll('>','&gt;');
        originalstr = originalstr.replaceAll('<','&lt;');
        finalstr = originalstr;
        
        return finalstr;    
    }
}