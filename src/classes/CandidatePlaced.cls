/*
    Author: Jack
    Date: 14 Aug 2013
    
    This class is to excute CandidatePlaced Trigger 
*/
public with sharing class CandidatePlaced {
	public static void CandidatePlaced(List<Placement_Candidate__c> triggernewCM){		
		List<Placement__c> vacancy_updateList = new list<Placement__c>{}; //Get the update vacancy list
		List<String> VanID = new List<String>{};
        List<Placement__c> vacancylist = new List<Placement__c>();
        
        for(Placement_Candidate__c c:triggernewCM){//check the all of the CM
        	VanID.add(c.Placement__c);
        }
        system.debug('VanID ='+ VanID);
        if(VanID != null && VanID.size()>0){
        	CommonSelector.checkRead(Placement__c.sObjectType,'Id');
        	CommonSelector.checkRead(Placement_Candidate__c.sObjectType,'id, Status__c, Candidate_Status__c, Candidate__c, LastModifiedDate');
        	vacancylist = [select id,(select id, Status__c, Candidate_Status__c, Candidate__c, 
        					LastModifiedDate from Candidates__r) from Placement__c where ID IN: VanID]; //get the vacancay and all CM under the vacany
			
		vacancy_updateList = getPlacedCondition(vacancylist);
		//system.debug('vacancy_updateList ='+ vacancy_updateList);	
		if(vacancy_updateList.size()>0)//added by Jack on 02/04/2013 . If no vacancy in the list, don't need to update {
		fflib_SecurityUtils.checkFieldIsUpdateable(Placement__c.SObjectType, Placement__c.Placed_Candidate__c);           
        update vacancy_updateList;
			
		}
    }
	
	//Fetch the update vacancy list
	public static List<Placement__c> getPlacedCondition(List<Placement__c> vacancylist){
		//system.debug('vacancylist ='+vacancylist);
		String placedcancon = ''; //Fetch the Candidate Placed conditions from Custom Setting
		List<Placement__c> vacancy_update = new list<Placement__c>{};
		List<Placement_Candidate__c> tempCM = new List<Placement_Candidate__c>{};
		
        PCAppSwitch__c CS = PCAppSwitch__c.getInstance();
        placedcancon = CS.Placed_Candidate_Condition__c;
        
		if(vacancylist != null && vacancylist.size()>0){
			
			for(Placement__c van: vacancylist){//check each vacancy
                 	datetime vancancydate_com = datetime.newInstance(1990, 12, 1, 12, 30, 2);
                 	string status_value;
                 	string progress_value;
                 	for(Placement_Candidate__c vancan : van.Candidates__r){//check each candidate management under the vacancy
                    	if(vancan.Status__c == 'Placed' || vancan.Candidate_Status__c == 'Placed'){
							tempCM.add(vancan);
                    	}
                    	else if(placedcancon!=null && placedcancon != ''){                       
                        	for(string placedcan : placedcancon.split(';')){//check the status of the candidate management equal placed condition.
                            	if(placedcan.contains('|')){
                               		status_value = placedcan.substring(0,placedcan.lastIndexOf('|'));
                                	progress_value = placedcan.substring((placedcan.lastIndexOf('|')+1),placedcan.length());
                                }
                            	else{
	                                status_value = placedcan;   
	                                progress_value = null; 
                                }
                            	if(vancan.Status__c == status_value && vancan.Candidate_Status__c == progress_value){//if status under the condition,find out the candidate whose CM has the nearest modify date 
								tempCM.add(vancan);//put the vacancy into the list
                                }
                            } 
                        }
                        else{
		                 	continue;
		                 }                  
                 }
                 string vancancyid_com = '';
                 for(integer i = 0 ; i< tempCM.size() ; i++){//compare the candidate if more then 1, compare the lastmodifiedday
   					String vancancyid = tempCM[i].Candidate__c;
	                datetime vancancydate = tempCM[i].LastModifiedDate;
	                    
	                if(vancancydate > vancancydate_com){
	                	vancancydate_com = vancancydate;
	                    vancancyid_com = vancancyid;
	                 }
	             }
	             if(vancancyid_com != null && vancancyid_com != ''){
		         	van.Placed_Candidate__c  = vancancyid_com;
		            vacancy_update.add(van);		              
	             } 
	        	 tempCM.clear(); // added by Kevin to fix issue PC-425                 
            }
		}
		return vacancy_update;    
	}
}