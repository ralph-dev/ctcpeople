public with sharing class WebSiteEdit {
    public Advertisement__c ad{get; set;}
    public List<String> editableFields {set;get;}
    public Boolean checkmultipost=false;
    public Boolean Getcheckmultipost(){
        return checkmultipost;
    }
    public Integer textid{set;get;}
    public Map<String,Integer> textdivid{set;get;}
    public IEC2WebService iec2ws{get; set;}
   
    public Boolean check{set;get;}
     
    public List<Schema.FieldSetMember> field_list{get;set;}
    
    /**
    	Required_Other_Map is used to contain(fieldname, 'required website_required') for none text or none multipicklist
    	Or{fieldname,'website_required'} for required multipicklist type Or {fieldname,''} for not required or text.
    	
     **/
    public Map<String,String> Required_Other_Map{set;get;}
    
    /**
    	These maps is used to judge required block according to their text type and required or not.
    **/
    public Map<String, Boolean> Multi_Map{set;get;}
    public Map<String, Boolean> Textarea_Map{set;get;}
    public Map<String, Boolean> NoneRequiredTextarea_Map{set;get;}
    public Map<String, Boolean> Other_Map{set;get;}
    public Map<String, Integer> Map_recordtextareaforvalidation{set;get;}
    
    /*
    	Used for the loop in javascript in websitepostjs.js to run the validation code in Textarea required.
    */
    public Integer Number_text{set;get;}
    
    /*
    	Used for the loop in javascript in websitepostjs.js to run the validation code in Textarea required.
    */
    public Integer Number_Multi{set;get;}
    
    /*************Start Add skill parsing param, Author by Jack on 27/05/2013************************/
    public boolean enableskillparsing {get; set;} //check the skill parsing customer setting status
    public List<Skill_Group__c> enableSkillGroupList{get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public string skillparsingstyleclass {get; set;}    
    /*************End Add skill parsing param********************************************************/
    public WebSiteEdit(){}
    
    public WebSiteEdit(ApexPages.StandardController stdController){
    	String objectname=PCAppSwitch__c.getinstance().Object_Name__c;
        String fieldsetname=PCAppSwitch__c.getinstance().Field_Set_Name__c;               
        checkmultipost=PCAppSwitch__c.getinstance().Multi_job_postings__c;
        
        //Init skill parsing param. Add by Jack on 23/05/2013
	    enableskillparsing = SkillGroups.isSkillParsingEnabled();   //check the skill parsing is active 
	    enableSkillGroupList = new List<Skill_Group__c>();
	    enableSkillGroupList = SkillGroups.getSkillGroups();
	    defaultskillgroups = new List<String>();
	    //end skill parsing param.
	        
        editableFields=new List<String>();
        iec2ws = new EC2WebServiceImpl();     
        CustomFieldList cfl=new CustomFieldList(objectname,fieldsetname);
        field_list=new List<Schema.FieldSetMember>();
 		Multi_Map =new Map<String, Boolean>();
    	Textarea_Map=new Map<String, Boolean>();
    	Required_Other_Map=new Map<String,String>();
   		NoneRequiredTextarea_Map=new Map<String, Boolean>();
    	Other_Map=new Map<String, Boolean>();
    	Map_recordtextareaforvalidation=new Map<String, Integer>();		
        field_list=cfl.getCustomFieldList();
        check=true;
        Number_text=0;
  		Number_Multi=0;
    	
    	for(Schema.FieldSetMember fieldmember: field_list){
        	editableFields.add(fieldmember.getfieldpath());
        	
        	Other_Map.put(fieldmember.getfieldpath(), false);
	       	Multi_Map.put(fieldmember.getfieldpath(), false);
	  		Textarea_Map.put(fieldmember.getfieldpath(), false);
	  		NoneRequiredTextarea_Map.put(fieldmember.getfieldpath(), false);
	  		if(fieldmember.getrequired()){
	        	if(fieldmember.gettype().name()=='MULTIPICKLIST'){
	        		Required_Other_Map.put(fieldmember.getfieldpath(),'website_required');
	        		Multi_Map.put(fieldmember.getfieldpath(), true);
	        		
	        		//Number_Multi++;
	        		
	        	}else{
	        		if(fieldmember.gettype().name()=='TEXTAREA'){
	        			Required_Other_Map.put(fieldmember.getfieldpath(),'');
	        			Textarea_Map.put(fieldmember.getfieldpath(), true);
	        			Map_recordtextareaforvalidation.put(fieldmember.getfieldpath(),Number_text);
	        			Number_text++;
	        			
	        		}else{
	        			Required_Other_Map.put(fieldmember.getfieldpath(),'required website_required');
	        			Other_Map.put(fieldmember.getfieldpath(), true);
	        		}
	        	}
        	}else{
        			if(fieldmember.gettype().name()=='TEXTAREA'){
        				NoneRequiredTextarea_Map.put(fieldmember.getfieldpath(), true);
        			}else{
        				Other_Map.put(fieldmember.getfieldpath(), true);
        			}
	        		Required_Other_Map.put(fieldmember.getfieldpath(),'');
	        	}
        	
        }
    	
    	if(!test.isRunningTest()){
        	stdController.addFields(editableFields);
    	}
        ad = (Advertisement__c) stdController.getRecord();
        //fetch the existing picklist value
        if(ad.Skill_Group_Criteria__c!=null&&ad.Skill_Group_Criteria__c!=''){
        	String defaultskillgroupstring = ad.Skill_Group_Criteria__c; //if update ad template, the default skill group is the value of this advertisment record
	        defaultskillgroups = defaultskillgroupstring.split(',');
        }
    }
    
    public List<SelectOption> getStyleOptions(){
        return JobBoardUtils.getStyleOptions('website');
    }
    
	public PageReference website_save(){
		
		String temp = '';
		//insert skill group ext id
	    if(enableskillparsing){
	    	String tempString  = String.valueOf(defaultskillgroups);
	        tempString = tempString.replaceAll('\\(', '');
	        tempString = tempString.replaceAll('\\)', '');
	        tempString = tempString.replace(' ','');
	        ad.Skill_Group_Criteria__c = tempString;
	    }
	    //end insert skill group ext id
	    
        if(ad.online_footer__c != null && ad.online_footer__c != 'null'){
            temp = ad.online_footer__c.replaceAll('\\[\\%User Name\\%\\]',UserInfo.getName());
            if(ad.Reference_No__c!=null){
            	temp = temp.replaceAll('\\[\\%Vacancy Referece No\\%\\]',ad.Reference_No__c );
            }
        }
        ad.Online_Footer__c = temp;
		
		try{
			CommonSelector.checkRead(User.SObjectType,'AmazonS3_Folder__c');
			User currentUser = [select AmazonS3_Folder__c from User where Id=:UserInfo.getUserId()];
	    	CommonSelector.checkRead(StyleCategory__c.SObjectType,'Id, Header_EC2_File__c, Header_File_Type__c,Div_Html_EC2_File__c, Div_Html_File_Type__c,Footer_EC2_File__c, Footer_File_Type__c');
			StyleCategory__c sc = [select Id, Header_EC2_File__c, Header_File_Type__c,
	    	 	Div_Html_EC2_File__c, Div_Html_File_Type__c, 
	    	 	 Footer_EC2_File__c, Footer_File_Type__c  
	    	  		from StyleCategory__c where id = :ad.Application_Form_Style__c limit 1];
	    	  		
	    	  
			//iec2ws.updateDetailTable(ad.id, sc.Header_EC2_File__c, sc.Header_File_Type__c, 
			//	 sc.Div_Html_EC2_File__c, sc.Div_Html_File_Type__c, 
			//	 	sc.Footer_EC2_File__c, sc.Footer_File_Type__c, UserInfo.getOrganizationId().subString(0, 15));
            iec2ws.updateDetailTable(ad.id, sc.Header_EC2_File__c, sc.Header_File_Type__c, 
                 sc.Div_Html_EC2_File__c, sc.Div_Html_File_Type__c, 
                    sc.Footer_EC2_File__c, sc.Footer_File_Type__c, UserInfo.getOrganizationId().subString(0, 15), JobBoardUtils.blankValue(ad.Job_Title__c));
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Advertisement__c.Website__c,
                Advertisement__c.RecordTypeId,
                Advertisement__c.Job_Content__c,
                Advertisement__c.Online_Footer__c,
                Advertisement__c.Skill_Group_Criteria__c,
                Advertisement__c.Reference_No__c,
                Advertisement__c.Application_Form_Style__c,
                Advertisement__c.Job_Title__c,
                Advertisement__c.Application_URL__c,
                Advertisement__c.Status__c,
                Advertisement__c.Job_Posting_Status__c
            };
            fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
			update ad;
		}catch(System.Exception e){
			CareerOneDetail.notificationsendout('orgid='+Userinfo.getOrganizationId()+' userid='+UserInfo.getUserId()+' jxt cannot post detail information to EC2 database '+e.getMessage() + e, 'ec2 callout', 'jxtnzedit', 'feedXml()');
		}
		return new ApexPages.Standardcontroller(ad).view();
	}
	
	//insert the skill group picklist value option
    public List<SelectOption> getSelectSkillGroupList(){
	  	List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
	  	List<SelectOption> displaySelectOption = new List<SelectOption>();
	  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
	  		displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
	  	}
	  	return displaySelectOption;
	}
}