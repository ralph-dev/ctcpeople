public with sharing class StyleCategorySelector extends CommonSelector{
   //private String queryStr='select id, Astute_Payroll_Api_Key__c, Astute_Payroll_Api_Password__c,AstutePayroll_api_username__c from StyleCategory__c ';

	private static Set<String> excludedFields = new Set<String>{
		'Advertiser_Password__c',
		'AstutePayroll_api_key__c',
		'Astute_Payroll_Api_Key__c',
		'AstutePayroll_api_password__c',
		'Astute_Payroll_Api_Password__c',
		'AstutePayroll_api_username__c',
		'Jobx_Password__c',
		'Jobx_Username__c',
		'JXT_Password__c',
		'JXT_Username__c'};

   public StyleCategorySelector(){
      //enabled field access check
      super(StyleCategory__c.SObjectType);
   }




   public LIST<Schema.SObjectField> getAllSObjectFields(){
      List<Schema.SObjectField> fieldsList = new List<Schema.SObjectField>();
      
      for(Schema.SObjectField f : getSObjectType().getDescribe().fields.getMap().values()){
      	if(! excludedFields.contains(f.getDescribe().getName())){
      		fieldsList.add(f);
      	}
      	
      }
     
      return fieldsList;
   }




   public List<StyleCategory__c> getUserSaveSearchRecord() {
      Id userId = UserInfo.getUserId();
      Id saveSearchRecordTypeId = DaoRecordType.GoogleSaveSearchRT.Id;

      return Database.query(toSOQL(getAllSObjectFields(),'ownerId =: userId And RecordTypeId = : saveSearchRecordTypeId'));
      
      /**
      * commented by andy for security review II
      fflib_QueryFactory mainQF = newQueryFactory();
        mainQF.selectFields(getAllSObjectFields());
        mainQF.setCondition('ownerId =: userId And RecordTypeId =: saveSearchRecordTypeId');
        
        
   
      String soqlStr = mainQF.toSOQL();
        List<StyleCategory__c> result = Database.query(soqlStr);
        return result;
        */
   }

   public StyleCategory__c getStyleCatagoryById(String scId) {
       
       /**
       * commented by andy for security review II
       *
       fflib_QueryFactory qf = newQueryFactory();
       qf.setCondition('Id =: scId');
       qf.selectFields(getAllSObjectFields());
       String soqlStr = qf.toSOQL();
       List<StyleCategory__c> scs = Database.query(soqlStr);
       */

      //added by andy for security revieww II
      List<StyleCategory__c> scs = Database.query(toSOQL(getAllSObjectFields(), 'Id =: scId'));
      if(scs!=null && scs.size()>0) {
         return scs.get(0);
      } else {
         return null;
      }

   }

   public list<StyleCategory__c> getUserAstutePayrollAccount(String astutePayrollId){

      //added by andy for security revieww II
      return setParam('astutePayrollId',astutePayrollId).get('id,name', 'id =: astutePayrollId');
      
      /**
      * commented by andy for security review II
      list<StyleCategory__c> accountList=new list<StyleCategory__c>();
      String query=queryStr+ ' where id =: astutePayrollId';
      accountList=database.query(query);
      return accountList;
      */
   }

   public ProtectedAccount getUserAstutePayrollProtectedAccount(String astutePayrollId){
      StyleCategory__c[] account = getUserAstutePayrollAccount(astutePayrollId);
      if(account != null && account.size() > 0 ){
         ProtectedCredential pc = CredentialSettingService.getCredential(account[0].id);
         if(pc != null ){
            return new ProtectedAccount(account[0], pc);
         }
      }

      return null;
   }


   public ProtectedAccount getCareerOneProtectedAccount(){

      StyleCategory__c[] accounts = get('id, Account_Active__c,Advertiser_Uploadcode__c,CareerOne_Premium_Package__c,Website__c', 'WebSite__c=\'CareerOne\' and Account_Active__c = true and RecordType.DeveloperName like \'%WebSite_Admin_Record%\'' );
     
      if(accounts != null && accounts.size() > 0 ){
          for(StyleCategory__c a : accounts){
             ProtectedCredential pc = CredentialSettingService.getCredential(a.id);
            
             if(pc != null ){
                return new ProtectedAccount(a, pc);
             }
          }
         
      }

      return null;
   }


   public Boolean insertCareerOneProtectedAccount(StyleCategory__c account, String username, String password ){


      return doInsert(account) && CredentialSettingService.insertCareerOneCredential(account.id, username,password) != null;


   }

   public Boolean updateCareerOneProtectedAccount(StyleCategory__c account, String username, String password , String fieldNames) {


      return doUpdate(account, fieldNames) && CredentialSettingService.updateCareerOneCredential(account.id, username, password) != null;

   }
   /**
   *
   * Get a list of StyleCategory records by a given record type
   * @param recordTypeId
   * @return a list of StyleCategory records
   *
   * Added by Zoe on 17/07/2017
   *
   *
   */
      public List<StyleCategory__c> getStyleCategoriesByRecordTypeId(Id recordTypeId){

      //return get('Id,Name,Account_Active__c,Advertiser_Id__c,Advertiser_Uploadcode__c,CareerOne_Premium_Package__c,CompanyCode__c,ProviderCode__c,OfficeCodes__c,WebSite__c,White_labels__c','RecordTypeId =: \'recordTypeId\'');
      SimpleQueryFactory qf = simpleQuery()
      .selectFields(getAllSObjectFields())
      .setCondition('RecordTypeId =: recordTypeId');

      return Database.query(qf.toSOQL());
   }







}