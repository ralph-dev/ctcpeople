@isTest
private class VacancySelectorTest {

   static testMethod void myUnitTest() {
   	System.runAs(DummyRecordCreator.platformUser) {
      Account acc = TestDataFactoryRoster.createAccount();
      Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
      VacancySearchCriteria searchCriteria = TestDataFactoryRoster.createVacancySearchCriteria(acc);
      String query = 'Tes';

      List<String> idList = new List<String>();
      idList.add(vac.Id);

      VacancySelector selector = new VacancySelector();

      //test method VacancySelector.fetchVacancyListById(id)
      List<Placement__c> vacList1 = selector.fetchVacancyListById(vac.Id);
      System.assertNotEquals(vacList1.size(),0);

      //test method VacancySelector.fetchVacancyListByIdList(idList)
      List<Placement__c> vacList2 = selector.fetchVacancyListByIdList(idList);
      System.assertNotEquals(vacList2.size(),0);

      //test method VacancySelector.fetchVacancyListBySearchCriteria(searchCriteria)
      List<Placement__c> vacList3 = selector.fetchVacancyListBySearchCriteria(searchCriteria);
      System.assertNotEquals(vacList3.size(),0);

      //test method VacancySelector.fetchVacancyListBySearchCriteria(searchCriteria)
      List<Placement__c> vacList4 = selector.fetchVacancyListByCompanyId(acc.Id);
      System.assertNotEquals(vacList4.size(),0);

      List<Placement__c> vacList5 = selector.fetchVacancyListBySearch(query);
      System.assertNotEquals(0, vacList5.size());
   	}
   }

   static testmethod void testFetchVacancyOwner() {
   	
      VacancySelector selector = new VacancySelector();
      System.runas(DummyRecordCreator.platformUser) {
         Test.startTest();
         Account acc = TestDataFactoryRoster.createAccount();
         Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
         Placement__c vacAfter = selector.fetchVacancyOwner(vac.Id);
         Test.stopTest();
         System.assertEquals('lnPlatform', vacAfter.Owner.Name);
         System.assertEquals('adminplatformuser@clicktocloud.com', vacAfter.Owner.Email);
      }
   	
   }

   static testmethod void testFetchVacancyOwnerWithoutVacancy() {
   	System.runAs(DummyRecordCreator.platformUser) {
      VacancySelector selector = new VacancySelector();
      Placement__c vac = selector.fetchVacancyOwner('notexist');
      System.assertEquals(null, vac);
   	}
   }

}