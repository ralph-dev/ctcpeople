@isTest
private class CVEmailHandlerFactoryTest {

    static Map<String , Messaging.InboundEmail> emails  = new Map<String, Messaging.InboundEmail>();
   
   static{
   	System.runAs(DummyRecordCreator.platformUser) {
       	Messaging.InboundEmail mail = new Messaging.InboundEmail();
		mail.plainTextBody = 'test content';
		mail.subject = 'andys resume';
		mail.fromAddress = 'andy@anydomain.com';
	    emails.put('email 1', mail);
	    
	    mail = new Messaging.InboundEmail();
		mail.plainTextBody = 'test content';
		mail.subject = 'andys coverletter';
		mail.fromAddress = 'andy@anydomain.com';
	    emails.put('email 2', mail);
	    
	     mail = new Messaging.InboundEmail();
		mail.plainTextBody = 'test content';
		mail.subject = 'andys resume - hello world';
		mail.fromAddress = 'andy@anydomain.com';
		Messaging.InboundEmail.BinaryAttachment attach = new Messaging.InboundEmail.BinaryAttachment();
		attach.fileName = 'andy-resume.pdf';
		attach.body = null;
		mail.binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[]{attach};
	    emails.put('email 3', mail);
	    
	    mail = new Messaging.InboundEmail();
		mail.plainTextBody = 'test content';
		mail.subject = 'andys CV  - hello world';
		mail.fromAddress = 'andy@anydomain.com';
		attach = new Messaging.InboundEmail.BinaryAttachment();
		attach.fileName = 'andy-latest-CV-resume.doc';
		attach.body = null;
		mail.binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[]{attach};
	    emails.put('email 4', mail);
	    
	    mail = new Messaging.InboundEmail();
		mail.plainTextBody = 'test content';
		mail.subject = 'www.jxt.solutions  - hello world';
		mail.fromAddress = 'andy@anydomain.com';
		attach = new Messaging.InboundEmail.BinaryAttachment();
		attach.fileName = 'andy-latest-CV-resume.doc';
		attach.body = null;
		mail.binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[]{attach};
	    emails.put('email 5', mail);
   	}

	    
   }

	static testMethod void testBuild(){
		System.runAs(DummyRecordCreator.platformUser) {
		ICVEmailHandler handler = CVEmailHandlerFactory.build(emails.get('email 1'));
		System.assert(handler == null );
		 
		handler = CVEmailHandlerFactory.build(emails.get('email 3'));
	    System.assert(handler instanceof CandidateMaker);
	    
	    handler = CVEmailHandlerFactory.build(emails.get('email 5'));
	    System.assert(handler instanceof JXTCandidateMaker);
		}
	}
	
	
	private static testMethod void testGetCandidateMaker() {
	    System.runAs(DummyRecordCreator.platformUser) {
	    ICVEmailHandler handler = CVEmailHandlerFactory.getCandidateMaker(emails.get('email 1'));
	    System.assert(handler == null );
	    handler = CVEmailHandlerFactory.getCandidateMaker(emails.get('email 2'));
	    System.assert(handler == null);
	    
	    handler = CVEmailHandlerFactory.getCandidateMaker(emails.get('email 3'));
	    System.assert(handler instanceof CandidateMaker);
	    handler = CVEmailHandlerFactory.getCandidateMaker(emails.get('email 4'));
	    System.assert(handler instanceof CandidateMaker);
	    
	    handler = CVEmailHandlerFactory.getCandidateMaker(emails.get('email 5'));
	    System.assert(handler instanceof JXTCandidateMaker);
	  
        handler = CVEmailHandlerFactory.getCandidateMaker(emails.get('unknown'));
	    System.assert(handler == null);
	    
	    handler = CVEmailHandlerFactory.getCandidateMaker(null);
	    System.assert(handler == null);
	    }

	}
}