public with sharing class GlobalUtility {
    //check if abn is valid
    private enum ValidationType {ABN}
    static final List<Long> ABN_WEIGHTS = new List<Long>{10,1,3,5,7,9,11,13,15,17,19};

    public static Boolean ValidateABN(String abnString) {
        abnString = getNonBlankNumericStringWithoutWhitespace(abnString);
        if(abnString == null || abnString=='') {
            return false;
        }

        if(abnString.length() != 11) {
            return false;
        }

        if(abnString.substring(0,1) == '0') {
            return false;
        }

        String pos1Less1 = String.valueOf(Long.valueOf(abnString.substring(0,1))-1);
        String modifiedABN = String.valueOf(pos1Less1 + abnString.substring(1));
        Long abnWeightingSum = calcWeightingSum(modifiedABN);
        Long modEightyNineRemainder = Math.mod(abnWeightingSum, 89);
        if(modEightyNineRemainder != 0) {
            return false;
        }

        return true;
    }

    public static Long calcWeightingSum(String theNumString) {
        List<Long> weightList = ABN_WEIGHTS;
        Long weightingSum = 0;
        
        Integer startIndex = 0;
        Integer endIndex = 10;

        for(Integer i = startIndex; i <= endIndex; i++) {
            weightingSum += 
                ( Long.valueOf(theNumString.substring(i,i+1) ) * weightList[i]);
        }
        
        return weightingSum;
    }


    public static String getNonBlankNumericStringWithoutWhitespace(String theString) {
        if(String.isBlank(theString)) {
            return null;
        }

        theString = theString.deleteWhitespace();

        if(!theString.isNumeric()) {
            return null;
        }

        return theString;
    }
}