/**
 * Created by zoechen on 25/7/17.
 */

@isTest
public with sharing class ProtectedCredentialDummyRecordCreator {

    public static ProtectedCredential createCareerOneCredential(String name) {

        return CredentialSettingService.insertCareerOneCredential(name,'test','test');
    }

    public static ProtectedCredential createJXTNZCredential(String name){
        return CredentialSettingService.insertJXTNZCredential(name,'test','test');
    }

    public static ProtectedCredential createIndeedCredential(String name){
        return CredentialSettingService.insertIndeedCredential(name,'teset');
    }

    public static ProtectedCredential createLinkedInCredential(String name){
        return CredentialSettingService.insertLinkedInOAuthService(name,'testKey','testSecret');
    }

    public static ProtectedCredential createJXTCredential(String name){
        return CredentialSettingService.insertJXTCredential(name,'testacc','testpss');
    }

}