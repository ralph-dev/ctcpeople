/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 * This is a test for all account trigger
 */
@isTest
private class TestAccountTrigger {

    static testMethod void testPreventCandPoolFromChange() {
    	
    	RecordType candPoolRT = DaoRecordType.candidatePoolRT;
    	Account candPool = new Account(Name='Candidate Pool Test',RecordTypeId=candPoolRT.Id);
        Account randomAcc = new Account(Name='Company 1');
        insert candPool;
        insert randomAcc;
    	
    	// When protect candidate pool is enabled;
    	PCAppSwitch__c cs=PCAppSwitch__c.getInstance();
    	cs.Protect_Candidate_Pool__c=true;
    	insert cs;
    	
    	
    	// Check if protect candidate pool has been enabled successfully
    	system.assert(PCAppSwitch__c.getInstance().Protect_Candidate_Pool__c);
    	Boolean isDeleted = true;
    	try{
    		delete candPool;
    	}catch(Exception e){
    		isDeleted = false;
    	}
    	system.assert(!isDeleted);
    	
    	// Delete non-candidate-pool account
    	isDeleted = true;
    	try{
    		delete randomAcc;
    	}catch(Exception e){
    		isDeleted = false;
    	}
    	system.assert(isDeleted);
    	
    	// Disable protect candidate pool option
    	cs=PCAppSwitch__c.getInstance();
    	cs.Protect_Candidate_Pool__c=false;
    	update cs;
    	// Check if protect candidate pool has been disabled successfully
    	system.assert(!PCAppSwitch__c.getInstance().Protect_Candidate_Pool__c);
    	isDeleted = true;
    	try{
    		delete candPool;
    	}catch(Exception e){
    		isDeleted = false;
    	}
    	system.assert(isDeleted);
    	
    	
    }
}