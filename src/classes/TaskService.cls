/**
 * See https://github.com/financialforcedev/fflib-apex-common for more info
 *
 * Install library via
 *   https://githubsfdeploy.herokuapp.com/app/githubdeploy/financialforcedev/fflib-apex-common
 */

/**
 * Encapsulates all service layer logic for a given function or module in the application
 * 
 * For more guidelines and details see 
 *   https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Service_Layer
 *
 * @author Jack ZHOU
 * @purpose Task service
 **/
public with sharing class TaskService extends SObjectCRUDService {
	private DynamicSelector dynSel;

	public TaskService() {
		super(Task.sObjectType);
		dynSel = new DynamicSelector(Task.sObjectType.getDescribe().getName(),false,true,true);
	}
	
	/**
	*Create Task by Task JSON String
	*@param String
	*@return Task
	**/
	public override List<SObject> createRecords(List<Map<String, Object>> sobjs) {
		List<Task> tasks = (List<Task>)constructSObjectsForCRUD(sobjs, SObjectCRUDService.OperationType.CREATE);
        try{
			return createRecords(tasks);
		}catch(exception e){
			throw e;
		}

	}

	/**
	*Update Task by Task JSON String
	*@param String
	*@return Task
	**/
	public override List<SObject> updateRecords(List<Map<String, Object>> sobjs) {
		List<Task> tasks = (List<Task>)constructSObjectsForCRUD(sobjs, SObjectCRUDService.OperationType.MODIFY);
		try {
			return updateRecords(tasks);
		}catch(exception e) {
			throw e;
		}
	}

	/**
	*Create Task by Task JSON String
	*@param String
	*@return Task
	**/
	public List<Id> deleteTaskRecords(List<Map<String, Object>> sobjs) {
		try{
			return deleteRecords(sobjs);
		}
		catch(exception e){
			throw e;
		}
	}

}