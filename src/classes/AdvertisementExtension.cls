public with sharing class AdvertisementExtension {

	private final sObject advertisementObject;

    // The extension constructor initializes the private member
    // variable advertisementObject by using the getRecord method from the standard
    // controller.
    public AdvertisementExtension(ApexPages.StandardController stdController) {
        this.advertisementObject = (sObject)stdController.getRecord();
    }

    public AdvertisementExtension(Object obj) {}

    @RemoteAction
    public static String getJXTDefaults() {
        //return 'success';
        String JSONString = JxtUtils.getPickListFieldMapJsonString();
        return JSONString;
    }


    @RemoteAction
    public static String getPickListDefaultList(String recordTypeId) {
        JobBoard jobBoard = JobBoardFactory.getJobBoard(recordTypeId);
        return jobBoard.getPickListDefaultList();
    }

    @RemoteAction
    public static String getAccountInfo(String recordTypeId) {
        jobBoard jobBoard =JobBoardFactory.getJobBoard(recordTypeId);
        return jobBoard.getAccountsInfo();
    }

}