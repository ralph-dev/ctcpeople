@isTest
public class S3TestData{
	public static S3Credential__c setTestData() {
		S3Credential__c s3Credential = S3Credential__c.getOrgdefaults();
		s3Credential.Access_Key_ID__c = 'test';
		s3Credential.Secret_Access_Key__c = 'test';
		upsert s3Credential;
		return s3Credential;
	}
}