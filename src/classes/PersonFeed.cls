public with sharing class PersonFeed {
	public PersonFeed() {
		
	}
	public String orgId {get; set;}
	public String candidateId {get; set;}
	public String lastName {get; set;}
	public String resumeName {get; set;}
	public String resumeBucket {get; set;}
	public String action {get; set;}
}