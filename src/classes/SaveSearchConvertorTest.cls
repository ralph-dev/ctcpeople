/**
 * This is the test class for SaveSearchConvertor
 * This test class include tests for different field type
 * 
 * Created by: Lina
 * 
**/ 

@isTest
private class SaveSearchConvertorTest {
    
    static final String NAMESPACE = PeopleCloudHelper.getPackageNamespace();
    
    static testmethod void testCombinedAllType() {
        System.runAs(DummyRecordCreator.platformUser) {
        StyleCategory__c scGoogle = new StyleCategory__c();
        scGoogle.Name = 'GoogleSearch';
        scGoogle.ascomponents__c = '{"keywords":"","objsingle":{"LastName":{"label":"Last Name","operator":"equals","value":"a","encodevalue":"a"}, "Birthdate":{"label":"Birthdate","operator":"equals","value":"2016-06-05","encodevalue":"2016-06-05"},"' + NAMESPACE + 'Direct_Reports__c":{"label":"# Direct Reports","operator":"equals","value":"10","encodevalue":"10"}},"objmulti":{"Salutation":{"label":"Salutation","operator":"excludes","value":["Mr."],"encodevalue":["Mr."]},"RecordTypeId":{"label":"Record Type ID","operator":"Include","value":["01290000001606AAAQ","01290000001606FAAQ"],"encodevalue":["01290000001606AAAQ","01290000001606FAAQ"]},"' + NAMESPACE + 'Group__c":{"label":"Group","operator":"Include","value":["cat","abc & ok","cd, fine","abc + e"],"encodevalue":["cat","abc%2520%2526%2520ok","cd%252c%2520fine","abc%2520%252b%2520e"]}},"objselectedskills":"[]","operatorMap":"{}","availabilitycriteria":[],"availabilitysearchmode":""}';
        StyleCategory__c scPeople = new StyleCategory__c();
        scPeople.Name = 'PeopleSearch';
        scPeople.ascomponents__c = '{"criteria":[{"field":"HasOptedOutOfEmail","type":"BOOLEAN","operator":"eq","values":true}],"resumeKeywords":null,"availability":null,"skillsSearch":null}';
        List<StyleCategory__c> scList = SaveSearchConvertor.convertGoogleSearchToPeopleSearch(new List<StyleCategory__c>{scGoogle, scPeople});
        System.assertEquals(2, scList.size());
        for (StyleCategory__c sc : scList) {
            if (sc.Name == 'GoogleSearch') {
                String afterConvert = '{"skillsSearch":{"verifiedSkills":false,"query":""},"resumeKeywords":"","criteria":[{"values":"a","type":"STRING","operator":"eq","field":"LastName"},{"values":"{\\"toDate\\":\\"2016-06-05T00:00:00.000Z\\",\\"fromDate\\":\\"2016-06-05T00:00:00.000Z\\"}","type":"DATE","operator":"between","field":"Birthdate"},{"values":"10","type":"DOUBLE","operator":"eq","field":"' + NAMESPACE + 'Direct_Reports__c"},{"values":"[\\"Mr.\\"]","type":"PICKLIST","operator":"notIn","field":"Salutation"},{"values":"[\\"01290000001606AAAQ\\",\\"01290000001606FAAQ\\"]","type":"PICKLIST","operator":"in","field":"RecordTypeId"},{"values":"[\\"cat\\",\\"abc & ok\\",\\"cd, fine\\",\\"abc + e\\"]","type":"MULTIPICKLIST","operator":"inc","field":"' + NAMESPACE + 'Group__c"}],"availability":{"availabilitysearchmode":"","availabilitycriteria":[]}}';
                System.assertEquals(afterConvert, sc.ascomponents__c);
            } else {
                System.assertEquals('{"criteria":[{"field":"HasOptedOutOfEmail","type":"BOOLEAN","operator":"eq","values":true}],"resumeKeywords":null,"availability":null,"skillsSearch":null}', sc.ascomponents__c);
            }
        }
        }
    }
    
    static testmethod void testDateConversion() {
    	System.runAs(DummyRecordCreator.platformUser) {
        StyleCategory__c scGoogle = new StyleCategory__c();
        scGoogle.Name = 'GoogleSearch';
        scGoogle.ascomponents__c = '{"keywords":"","objsingle":{"Birthdate":{"label":"Birthdate","operator":"greaterorequal","value":"2016-06-05","encodevalue":"2016-06-05"}, "' + NAMESPACE + 'Date__c":{"label":"Date","operator":"lessorequal","value":"2016-06-05","encodevalue":"2016-06-05"}},"objmulti":{},"objselectedskills":"[]","operatorMap":"{}","availabilitycriteria":[],"availabilitysearchmode":""}';
        List<StyleCategory__c> scList = SaveSearchConvertor.convertGoogleSearchToPeopleSearch(new List<StyleCategory__c>{scGoogle});
        String toDateForGreater = JSON.serialize(WizardUtils.convertDateToDateTime(Date.valueOf('2050-12-31'))).replace('"', '');
        String fromDateForLess = JSON.serialize(WizardUtils.convertDateToDateTime(Date.valueOf('1900-1-1'))).replace('"', '');
        System.assertEquals(1, scList.size());
        for (StyleCategory__c sc : scList) {
            String afterConvert = '{"skillsSearch":{"verifiedSkills":false,"query":""},"resumeKeywords":"","criteria":[{"values":"{\\"toDate\\":\\"'+toDateForGreater+'\\",\\"fromDate\\":\\"2016-06-05T00:00:00.000Z\\"}","type":"DATE","operator":"between","field":"Birthdate"},{"values":"{\\"toDate\\":\\"2016-06-05T00:00:00.000Z\\",\\"fromDate\\":\\"'+fromDateForLess+'\\"}","type":"DATE","operator":"between","field":"' + NAMESPACE + 'Date__c"}],"availability":{"availabilitysearchmode":"","availabilitycriteria":[]}}';
            System.assertEquals(afterConvert, sc.ascomponents__c);
        }
    	}
    }
    
    static testmethod void testBooleanEqualConversion() {
    	System.runAs(DummyRecordCreator.platformUser) {
        StyleCategory__c scGoogle = new StyleCategory__c();
        scGoogle.Name = 'GoogleSearch';
        scGoogle.ascomponents__c = '{"keywords":"","objsingle":{"DoNotCall":{"lable":"Do Not Call", "operator":"equals", "value":"true", "encodedvalue":"true"}, "HasOptedOutOfEmail":{"lable":"Email Opt Out", "operator":"equals", "value":"False", "encodedvalue":"False"}},"objmulti":{},"objselectedskills":"[]","operatorMap":"{}","availabilitycriteria":[],"availabilitysearchmode":""}';
        List<StyleCategory__c> scList = SaveSearchConvertor.convertGoogleSearchToPeopleSearch(new List<StyleCategory__c>{scGoogle});
        System.assertEquals(1, scList.size());
        for (StyleCategory__c sc : scList) {
            String afterConvert = '{"skillsSearch":{"verifiedSkills":false,"query":""},"resumeKeywords":"","criteria":[{"values":"true","type":"BOOLEAN","operator":"eq","field":"DoNotCall"},{"values":"false","type":"BOOLEAN","operator":"eq","field":"HasOptedOutOfEmail"}],"availability":{"availabilitysearchmode":"","availabilitycriteria":[]}}'; 
            System.assertEquals(afterConvert, sc.ascomponents__c);
        }
    	}
    }    
    
    static testmethod void testBooleanNotEqualConversion() {
    	System.runAs(DummyRecordCreator.platformUser) {
        StyleCategory__c scGoogle = new StyleCategory__c();
        scGoogle.Name = 'GoogleSearch';
        scGoogle.ascomponents__c = '{"keywords":"","objsingle":{"DoNotCall":{"lable":"Do Not Call", "operator":"notequals", "value":"yes", "encodedvalue":"yes"}, "HasOptedOutOfEmail":{"lable":"Email Opt Out", "operator":"notequals", "value":"no", "encodedvalue":"no"}},"objmulti":{},"objselectedskills":"[]","operatorMap":"{}","availabilitycriteria":[],"availabilitysearchmode":""}';
        List<StyleCategory__c> scList = SaveSearchConvertor.convertGoogleSearchToPeopleSearch(new List<StyleCategory__c>{scGoogle});
        System.assertEquals(1, scList.size());
        for (StyleCategory__c sc : scList) {
            String afterConvert = '{"skillsSearch":{"verifiedSkills":false,"query":""},"resumeKeywords":"","criteria":[{"values":"false","type":"BOOLEAN","operator":"eq","field":"DoNotCall"},{"values":"true","type":"BOOLEAN","operator":"eq","field":"HasOptedOutOfEmail"}],"availability":{"availabilitysearchmode":"","availabilitycriteria":[]}}'; 
            System.assertEquals(afterConvert, sc.ascomponents__c);
        }
    	}
    }
    
    static testmethod void testURLEqualConversion() {
    	System.runAs(DummyRecordCreator.platformUser) {
        StyleCategory__c scGoogle = new StyleCategory__c();
        scGoogle.Name = 'GoogleSearch';
        scGoogle.ascomponents__c = '{"keywords":"","objsingle":{"' + NAMESPACE + 'Web_Site__c":{"lable":"Website", "operator":"equals", "value":"test", "encodedvalue":"test"}},"objmulti":{},"objselectedskills":"[]","operatorMap":"{}","availabilitycriteria":[],"availabilitysearchmode":""}';
        List<StyleCategory__c> scList = SaveSearchConvertor.convertGoogleSearchToPeopleSearch(new List<StyleCategory__c>{scGoogle});
        System.assertEquals(1, scList.size());
        for (StyleCategory__c sc : scList) {
            String afterConvert = '{"skillsSearch":{"verifiedSkills":false,"query":""},"resumeKeywords":"","criteria":[{"values":"test","type":"URL","operator":"like","field":"' + NAMESPACE + 'Web_Site__c"}],"availability":{"availabilitysearchmode":"","availabilitycriteria":[]}}'; 
            System.assertEquals(afterConvert, sc.ascomponents__c);
        }
    	}
    }
    
    static testmethod void testURLNotEqualConversion() {
    	System.runAs(DummyRecordCreator.platformUser) {
        StyleCategory__c scGoogle = new StyleCategory__c();
        scGoogle.Name = 'GoogleSearch';
        scGoogle.ascomponents__c = '{"keywords":"","objsingle":{"' + NAMESPACE + 'Web_Site__c":{"lable":"Website", "operator":"notequals", "value":"test", "encodedvalue":"test"}},"objmulti":{},"objselectedskills":"[]","operatorMap":"{}","availabilitycriteria":[],"availabilitysearchmode":""}';
        List<StyleCategory__c> scList = SaveSearchConvertor.convertGoogleSearchToPeopleSearch(new List<StyleCategory__c>{scGoogle});
        System.assertEquals(1, scList.size());
        for (StyleCategory__c sc : scList) {
            String afterConvert = '{"skillsSearch":{"verifiedSkills":false,"query":""},"resumeKeywords":"","criteria":[{"values":"test","type":"URL","operator":"notLike","field":"' + NAMESPACE + 'Web_Site__c"}],"availability":{"availabilitysearchmode":"","availabilitycriteria":[]}}'; 
            System.assertEquals(afterConvert, sc.ascomponents__c);
        }
    	}
    }
    
    static testmethod void testDecimalConversion() {
    	System.runAs(DummyRecordCreator.platformUser) {
        StyleCategory__c scGoogle = new StyleCategory__c();
        scGoogle.Name = 'GoogleSearch';
        scGoogle.ascomponents__c = '{"keywords":"","objsingle":{"' + NAMESPACE + 'Direct_Reports__c":{"lable":"# Direct Reports", "operator":"notequals", "value":"12.21", "encodedvalue":"12.21"}},"objmulti":{},"objselectedskills":"[]","operatorMap":"{}","availabilitycriteria":[],"availabilitysearchmode":""}';
        List<StyleCategory__c> scList = SaveSearchConvertor.convertGoogleSearchToPeopleSearch(new List<StyleCategory__c>{scGoogle});
        System.assertEquals(1, scList.size());
        for (StyleCategory__c sc : scList) {
            String afterConvert = '{"skillsSearch":{"verifiedSkills":false,"query":""},"resumeKeywords":"","criteria":[{"values":"12.21","type":"DOUBLE","operator":"ne","field":"' + NAMESPACE + 'Direct_Reports__c"}],"availability":{"availabilitysearchmode":"","availabilitycriteria":[]}}'; 
            System.assertEquals(afterConvert, sc.ascomponents__c);
        }
    	}
    }
    
    static testmethod void testException() {
    	System.runAs(DummyRecordCreator.platformUser) {
        StyleCategory__c scGoogle = new StyleCategory__c();
        scGoogle.Name = 'GoogleSearch';
        scGoogle.ascomponents__c = '{"keywords":"","objsingle":{"' + NAMESPACE + 'Direct_Reports__c":{"lable":"# Direct Reports", "operator":"notequals", "value":"test", "encodedvalue":"test"}},"objmulti":{},"objselectedskills":"[]","operatorMap":"{}","availabilitycriteria":[],"availabilitysearchmode":""}';
        List<StyleCategory__c> scList = SaveSearchConvertor.convertGoogleSearchToPeopleSearch(new List<StyleCategory__c>{scGoogle});
        System.assertEquals(1, scList.size());
        for (StyleCategory__c sc : scList) {
            String afterConvert = '{"skillsSearch":{"verifiedSkills":false,"query":""},"resumeKeywords":"","criteria":[],"availability":{"availabilitysearchmode":"","availabilitycriteria":[]}}'; 
            System.assertEquals(afterConvert, sc.ascomponents__c);
        }
    	}
    }    
    
    static testmethod void newPicklistConversion() {
    	System.runAs(DummyRecordCreator.platformUser) {
        StyleCategory__c scPeople = new StyleCategory__c();
        Id saveSearchRecordTypeId = DaoRecordType.GoogleSaveSearchRT.Id;
        scPeople.Name = 'PeopleSearch';
        scPeople.ascomponents__c = '{"criteria":[{"field":"Salutation","type":"PICKLIST","operator":"eq","values":"Mr."}],"resumeKeywords":null,"availability":null,"skillsSearch":null}';
        scPeople.RecordTypeId = saveSearchRecordTypeId;
        scPeople.Unique_Search_Record_Name__c = scPeople.Name + '_' + UserInfo.getUserId();
        scPeople.Active__c = true;
        scPeople.templateActive__c =true;
        insert scPeople;
        Test.setCreatedDate(scPeople.Id, DateTime.newInstance(2016,6,25));
        Test.startTest();
        scPeople = [SELECT Id, Name, ascomponents__c, CreatedDate FROM StyleCategory__c WHERE Id =: scPeople.Id limit 1];
        system.debug(scPeople.CreatedDate);
        List<StyleCategory__c> scList = SaveSearchConvertor.convertGoogleSearchToPeopleSearch(new List<StyleCategory__c>{scPeople});
        System.assertEquals(1, scList.size());
        for (StyleCategory__c sc : scList) {
            System.assertEquals('{"skillsSearch":null,"availability":null,"resumeKeywords":null,"criteria":[{"values":["Mr."],"operator":"eq","type":"PICKLIST","field":"Salutation"}]}', sc.ascomponents__c);
        } 
        Test.stopTest();
    	}
    }
    
    static testmethod void testConvertSkillSearch() {
    	System.runAs(DummyRecordCreator.platformUser) {
        StyleCategory__c scPeople = new StyleCategory__c();
        Id saveSearchRecordTypeId = DaoRecordType.GoogleSaveSearchRT.Id;
        scPeople.Name = 'PeopleSearch';
        scPeople.ascomponents__c = '{"criteria":[],"resumeKeywords":"a","availability":{"availabilitysearchmode":"","availabilitycriteria":[]},"skillsSearch":{"operatorMap":"{\\"a0Q28000000FGf2EAG\\":\\"and\\",\\"a0Q28000000FGgVEAW\\":\\"or\\",\\"a0Q28000000FGgpEAG\\":\\"and\\",\\"a0Q28000000FGggEAG\\":\\"not\\",\\"a0Q28000000FGhUEAW\\":\\"and\\",\\"a0Q28000000FGhzEAG\\":\\"and\\"}","objselectedskills":"[{\\"attributes\\":{\\"type\\":\\"Skill__c\\",\\"url\\":\\"/services/data/v37.0/sobjects/Skill__c/a0Q28000000FGf2EAG\\"},\\"Skill_Group__c\\":\\"a0O28000000DMjlEAG\\",\\"Name\\":\\"Cloud Computing\\",\\"Id\\":\\"a0Q28000000FGf2EAG\\",\\"Ext_Id__c\\":\\"S000753\\",\\"Skill_Group__r\\":{\\"attributes\\":{\\"type\\":\\"Skill_Group__c\\",\\"url\\":\\"/services/data/v37.0/sobjects/Skill_Group__c/a0O28000000DMjlEAG\\"},\\"Name\\":\\"Development Services Skill\\",\\"Id\\":\\"a0O28000000DMjlEAG\\"}},{\\"attributes\\":{\\"type\\":\\"Skill__c\\",\\"url\\":\\"/services/data/v37.0/sobjects/Skill__c/a0Q28000000FGgVEAW\\"},\\"Skill_Group__c\\":\\"a0O28000000DMjlEAG\\",\\"Name\\":\\"Photoshop\\",\\"Id\\":\\"a0Q28000000FGgVEAW\\",\\"Ext_Id__c\\":\\"S000844\\",\\"Skill_Group__r\\":{\\"attributes\\":{\\"type\\":\\"Skill_Group__c\\",\\"url\\":\\"/services/data/v37.0/sobjects/Skill_Group__c/a0O28000000DMjlEAG\\"},\\"Name\\":\\"Development Services Skill\\",\\"Id\\":\\"a0O28000000DMjlEAG\\"}},{\\"attributes\\":{\\"type\\":\\"Skill__c\\",\\"url\\":\\"/services/data/v37.0/sobjects/Skill__c/a0Q28000000FGgpEAG\\"},\\"Skill_Group__c\\":\\"a0O28000000DMjlEAG\\",\\"Name\\":\\"Sed\\",\\"Id\\":\\"a0Q28000000FGgpEAG\\",\\"Ext_Id__c\\":\\"S000864\\",\\"Skill_Group__r\\":{\\"attributes\\":{\\"type\\":\\"Skill_Group__c\\",\\"url\\":\\"/services/data/v37.0/sobjects/Skill_Group__c/a0O28000000DMjlEAG\\"},\\"Name\\":\\"Development Services Skill\\",\\"Id\\":\\"a0O28000000DMjlEAG\\"}},{\\"attributes\\":{\\"type\\":\\"Skill__c\\",\\"url\\":\\"/services/data/v37.0/sobjects/Skill__c/a0Q28000000FGggEAG\\"},\\"Skill_Group__c\\":\\"a0O28000000DMjlEAG\\",\\"Name\\":\\"Quantify\\",\\"Id\\":\\"a0Q28000000FGggEAG\\",\\"Ext_Id__c\\":\\"S000855\\",\\"Skill_Group__r\\":{\\"attributes\\":{\\"type\\":\\"Skill_Group__c\\",\\"url\\":\\"/services/data/v37.0/sobjects/Skill_Group__c/a0O28000000DMjlEAG\\"},\\"Name\\":\\"Development Services Skill\\",\\"Id\\":\\"a0O28000000DMjlEAG\\"}},{\\"attributes\\":{\\"type\\":\\"Skill__c\\",\\"url\\":\\"/services/data/v37.0/sobjects/Skill__c/a0Q28000000FGhUEAW\\"},\\"Skill_Group__c\\":\\"a0O28000000DMjmEAG\\",\\"Name\\":\\"Institutional Banking\\",\\"Id\\":\\"a0Q28000000FGhUEAW\\",\\"Ext_Id__c\\":\\"S000905\\",\\"Skill_Group__r\\":{\\"attributes\\":{\\"type\\":\\"Skill_Group__c\\",\\"url\\":\\"/services/data/v37.0/sobjects/Skill_Group__c/a0O28000000DMjmEAG\\"},\\"Name\\":\\"Infrastructure Services Skill\\",\\"Id\\":\\"a0O28000000DMjmEAG\\"}},{\\"attributes\\":{\\"type\\":\\"Skill__c\\",\\"url\\":\\"/services/data/v37.0/sobjects/Skill__c/a0Q28000000FGhzEAG\\"},\\"Skill_Group__c\\":\\"a0O28000000DMjmEAG\\",\\"Name\\":\\"DWDM\\",\\"Id\\":\\"a0Q28000000FGhzEAG\\",\\"Ext_Id__c\\":\\"S000936\\",\\"Skill_Group__r\\":{\\"attributes\\":{\\"type\\":\\"Skill_Group__c\\",\\"url\\":\\"/services/data/v37.0/sobjects/Skill_Group__c/a0O28000000DMjmEAG\\"},\\"Name\\":\\"Infrastructure Services Skill\\",\\"Id\\":\\"a0O28000000DMjmEAG\\"}}]"}}';
        scPeople.RecordTypeId = saveSearchRecordTypeId;
        scPeople.Unique_Search_Record_Name__c = scPeople.Name + '_' + UserInfo.getUserId();
        scPeople.Active__c = true;
        scPeople.templateActive__c =true;
        insert scPeople;
        Test.startTest();
        scPeople = [SELECT Id, Name, ascomponents__c, CreatedDate FROM StyleCategory__c WHERE Id =: scPeople.Id limit 1];
        List<StyleCategory__c> scList = SaveSearchConvertor.convertGoogleSearchToPeopleSearch(new List<StyleCategory__c>{scPeople});
        System.assertEquals(1, scList.size());
        for (StyleCategory__c sc : scList) {
            System.assertEquals('{"skillsSearch":{"verifiedSkills":false,"query":"a0Q28000000FGf2EAG OR a0Q28000000FGgVEAW AND a0Q28000000FGgpEAG NOT a0Q28000000FGggEAG AND a0Q28000000FGhUEAW AND a0Q28000000FGhzEAG"},"availability":{"availabilitycriteria":[],"availabilitysearchmode":""},"resumeKeywords":"a","criteria":[]}', sc.ascomponents__c);
        } 
        Test.stopTest();     
    	}   
        
    }
}