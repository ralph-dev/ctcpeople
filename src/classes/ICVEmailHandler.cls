/**
* This interface define handle() method to handle InboundEmail. all of the classes that can be referenced
* as email handler in Email Service should implement this interface, such as VacancyMaker, AttachmentMaker
*
*/

public interface ICVEmailHandler {
    
    void handle();
    
    
    Messaging.InboundEmail getEmail();
    void setEmail(Messaging.InboundEmail email);
    
}