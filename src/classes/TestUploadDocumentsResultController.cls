@isTest
private class TestUploadDocumentsResultController{
    public static String getTestCandidateId(){
    	RecordType candRT=DaoRecordType.indCandidateRT; 
    	Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',email='Cand1@test.ctc.test');
    	return cand1.Id;
    }
    
    
    public static testmethod void test(){
    	System.runAs(DummyRecordCreator.platformUser) {
   		String candidateId = getTestCandidateId();
    	Test.setCurrentPage(Page.uploadDocuments);
    	ApexPages.currentPage().getParameters().put('srcId', candidateId);
    	ApexPages.currentPage().getParameters().put('errorId', 'abcdefg');
    	
    	ApexPages.currentPage().getParameters().put('srcObj', 'Contact');
   		ApexPages.currentPage().getParameters().put('errorCode', '10009');
   		UploadDocumentsResultController udrc10009 = new UploadDocumentsResultController();
   		
   		ApexPages.currentPage().getParameters().put('srcObj', 'Client');
   		ApexPages.currentPage().getParameters().put('errorCode', '10006');
   		UploadDocumentsResultController udrc10006 = new UploadDocumentsResultController();
   		
   		ApexPages.currentPage().getParameters().put('srcObj', 'Vacancy');
   		ApexPages.currentPage().getParameters().put('errorCode', '10007');
   		UploadDocumentsResultController udrc10007 = new UploadDocumentsResultController();
   		
   		ApexPages.currentPage().getParameters().put('srcObj', 'dummy');
   		ApexPages.currentPage().getParameters().put('errorCode', '10008');
   		UploadDocumentsResultController udrc10008 = new UploadDocumentsResultController();
   		
   		ApexPages.currentPage().getParameters().put('srcObj', 'dummy');
   		ApexPages.currentPage().getParameters().put('errorCode', '99999');
   		UploadDocumentsResultController udrc99999 = new UploadDocumentsResultController();
   
   		ApexPages.currentPage().getParameters().put('srcObj', 'Candidate');
   		ApexPages.currentPage().getParameters().put('errorCode', '00000');
   		UploadDocumentsResultController udrc = new UploadDocumentsResultController();
   		System.assert(udrc.message!=null);
   		System.assert(udrc.message2ndLine!=null);
   		System.assert(udrc.resultTitle!=null);
   		System.assert(udrc.returnLinkValue!=null);
   		
   		udrc.returnToPreviousPage();
   		
   		udrc.returnToUploadDocumentsPage();
    	}
   		
    }


}