/**********************************************************************
*
*Check the ad status and return relevent code
*
**********************************************************************/

public with sharing class CheckAdStatus {
	public static Integer CheckPostingAdStatus(Advertisement__c currentAd){
		Integer result = 0;
		if(currentAd.Status__c != null){
        	if(currentAd.Status__c.equalsIgnoreCase('Deleted') || currentAd.Status__c.equalsIgnoreCase('Expired')){
            	result = PeopleCloudErrorInfo.ADVERTISEMT_HAS_BEEN_DELETED ;
            }
            else if(currentAd.Status__c.equalsIgnoreCase('Active')){ //** to be posted by scheduler
            	if(currentAd.Job_Posting_Status__c != null){
            		system.debug('Job_Posting_Status__c =' + currentAd.Job_Posting_Status__c);
                	if(currentAd.Job_Posting_Status__c.equalsIgnoreCase('In Queue') ){
                    	result = PeopleCloudErrorInfo.ADVERTISEMT_AD_STATUS_INQUEUE;  
                     }
                     else if(currentAd.Job_Posting_Status__c.equalsIgnoreCase('active')){ //** posted online already, just for update
                     	result = PeopleCloudErrorInfo.ADVERTISEMT_AD_STATUS_CORRECT;
                     }
                     else{
                     	result = PeopleCloudErrorInfo.ADVERTISEMT_AD_STATUS_ERROR;
                     }
                 }
                 else{
                     result = PeopleCloudErrorInfo.ADVERTISEMT_AD_STATUS_ERROR;  
                 }
             }
             else if(currentAd.Status__c.equalsIgnoreCase('Ready to Post') ||currentAd.Status__c.equalsIgnoreCase('Clone to Post')
             			||currentAd.Status__c.equalsIgnoreCase('Archived') || currentAd.Status__c.equalsIgnoreCase('Recalled')){ //* never post before
             	//** post on line depending on the remainingquota
             	result = PeopleCloudErrorInfo.ADVERTISEMT_AD_STATUS_CORRECT;
             }
             else{
             	result = PeopleCloudErrorInfo.ADVERTISEMT_AD_STATUS_ERROR;
             }
       }else{
       		result = PeopleCloudErrorInfo.ADVERTISEMT_AD_STATUS_ERROR;
       }
	return result;
	}
}