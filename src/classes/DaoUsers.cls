/*******************************************************
	
	Fetch Users' details from User Object.
	Author by Jack on 25/06/2013

********************************************************/

public class DaoUsers extends CommonSelector{
	
	public DaoUsers(){
		super(User.sObjectType);
		
	}
	
	private static DaoUsers dao = new DaoUsers();
	
	private static String defaultFields = 'Name, Email, UserType, UserRole.Name, UserRoleId, Profile.Name, ProfileId, IsActive, Id, AmazonS3_Folder__c, Job_Posting_Email__c,AstutePayrollAccount__c,'
						+'Job_Posting_Company_Name__c , JobBoards__c, Seek_Account__c, Default_Seek_Account__c, LinkedIn_Account__c, Indeed_Account__c, Monthly_Quota_Seek__c, Monthly_Quota_CareerOne__c,'
						+'Seek_Usage__c, CareerOne_Usage__c';
	
	Public static List<User> getActiveUsers(){
		List<User> results = new List<User>();
		try{
			
			dao.checkRead(defaultFields);
			results = [Select Name, Email, UserType, UserRole.Name, UserRoleId, Profile.Name, ProfileId, IsActive, Id, AmazonS3_Folder__c, Job_Posting_Email__c,AstutePayrollAccount__c,
						Job_Posting_Company_Name__c , JobBoards__c, Seek_Account__c, Default_Seek_Account__c, LinkedIn_Account__c, Indeed_Account__c, Monthly_Quota_Seek__c, Monthly_Quota_CareerOne__c,
						Seek_Usage__c, CareerOne_Usage__c From User where IsActive = true and UserType = 'Standard'];
		}catch(exception e){
			results = new List<User>();
		}
		return results;
	} 
	
	public static User getActiveUser(String currentid){
		User results = new User();
		try{
			dao.checkRead(defaultFields);
			results = [Select Name, Email, UserType, UserRole.Name, UserRoleId, Profile.Name, ProfileId, IsActive, Id, AmazonS3_Folder__c, Job_Posting_Email__c,AstutePayrollAccount__c,
						Job_Posting_Company_Name__c , JobBoards__c, Seek_Account__c, Default_Seek_Account__c, LinkedIn_Account__c,Indeed_Account__c, Monthly_Quota_Seek__c, Monthly_Quota_CareerOne__c,
						Seek_Usage__c, CareerOne_Usage__c From User where IsActive = true and UserType = 'Standard' and id =: currentid];
		}catch(exception e){
			results = new User();
		}
		return results;
	} 
	
	public static User getUserSearchFileName(String currentid){
		User results = new User();
		try{
			dao.checkRead('Id, Name,Email, Username, View_CTC_Admin__c, Profile.Name, Manage_Quota__c,Profile.Id,PeopleSearchFile__c, VacancySearchFile__c, Job_Posting_Email__c,AstutePayrollAccount__c,ClientSearchFile__c');
			results = [select Id, Name,Email, Username, View_CTC_Admin__c, Profile.Name, Manage_Quota__c,
        				Profile.Id,PeopleSearchFile__c, VacancySearchFile__c, Job_Posting_Email__c,AstutePayrollAccount__c,
        				ClientSearchFile__c from User where isActive = true and id =: currentid];
		}catch(exception e){
			results = new User();
		}
		return results;
	}
}