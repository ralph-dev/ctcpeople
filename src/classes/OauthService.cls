//*************************************************************************************************************************
//***************************Class OauthService is extension class of the SearchContact page*******************************
//**************************If OauthToken is exist, check the isAccess and belong to the user******************************
//**************************Do the search function. If not return to AuthPage to Authoration*******************************
//***********************************************************************************************************************//

public with sharing class OauthService{
    public OAuth_Service__c service{get; set;}
    public OAuth_Token__c token{get; set;}
    public String message { get; set; }
    public String authUrl { get; set; }
    public OAuth oa { get; set; }
    public boolean authSuccess { get; set; }
    public String fn ;
    public String firstname;
    public String ln ;
    public String lastname;
    public String mc ;
    public String mailcountry;
    public String oc ;
    public String othercountry;
    public String tl ;
    public String title;
    public String searchcriteria ;
    public String bodies { get; set; }
    public String body { get; set; }
    public List<Map<String,LinkedinPersonProfileXml.peopleList>> SearchList {get; set;}
    public String url;
    public String selectedServices { get; set; }
    public String authorationUrl {get; set; }
    private final Contact cont;
    public Contact new_cont;
    public String contactUrl {get; set;}
    public String name {get; set;}   
    public String publicurl {get; set;} 
    public String totalPeopleList {get; set;}
    public String countPeopleList {get; set;}
    public String startPeopleList {get; set;}
    public String count {get; set;}
    public String start {get; set;}
    public String startnumber {get; set;}
    public String starter {get; set;}
    public String previousbutton {get; set;}
    public String nextbutton {get; set;}
    public String searchresult {get; set;}
    public String shortsearchresult;
    public String mentionresult {get; set;}    
    public String shortsearchcriteria {get; set;}
    public integer i;
    /*Add by Jack, As the IFrame can not be load, use flag and URL to jump page*/
    public String linkedInAuthPageUrl {get; set;}
    public String linkedInProfilePageUrl {get; set;}
    public boolean jumpauthpage {get; set;}
    public boolean jumpprofilepage {get; set;}
    public boolean jumpoauthpage {get; set;}
    
        public OauthService(ApexPages.StandardController controller){//Put ContactId to contactUrl to nextpage and get the whole Contact FirstName, LastName to search contact from linkedin.
            contactUrl = ApexPages.currentPage().getParameters().get('contactUrl');
            this.cont = (Contact)controller.getRecord();
            if(contactUrl == null){//if the url doesn't include the contactid, get the contactid from pages
                contactUrl = cont.id; 
                }
            else{
                contactUrl = contactUrl;
            }
            
            CommonSelector.checkRead(Contact.sObjectType, 'FirstName, LastName, mailingcountry , Othercountry, title, public_Profile_Url__c');
            new_cont = [select FirstName, LastName, mailingcountry , Othercountry, title, public_Profile_Url__c from Contact where Id = :contactUrl];//get the detail of contact according the accountID
        }
        /**********************************************************
        /**************start search function***********************
        /**********************************************************/
        
        public PageReference search(){
        jumpoauthpage = true;
       	jumpauthpage = false;
       	jumpprofilepage = false;
        publicurl = new_cont.public_Profile_Url__c;//judge the public url field is empty or not. if the url is not empty, use this url to get profile from Linkedin     
        if(publicurl!=null){  
        	jumpoauthpage = false;
	       	jumpauthpage = false;
	       	jumpprofilepage = true;        
            PageReference proPage = Page.ProfilePage;
            proPage.setRedirect(true);
            proPage.getParameters().put('contactUrl',contactUrl); //add the contactID and publicUrl to profile page
            proPage.getParameters().put('ProfilePage',publicurl); 
            linkedInProfilePageUrl = proPage.getUrl();
            return null;
        }
        else{
        	
            searchresult = 'These are the Search results using ';
            mentionresult = '';
            starter = ApexPages.currentPage().getParameters().get('start'); //if the publicUrl field is empty, get the search detail from Contact
            searchcriteria = ApexPages.currentPage().getParameters().get('searchcriteria');
            fn = new_cont.FirstName;
            ln = new_cont.LastName;
            mc = new_cont.mailingcountry;
            oc = new_cont.Othercountry;
            tl = new_cont.title;
            if(starter == null){//first time to search people, start from '0'
                starter = '0';
            }
            if(searchcriteria == null){
	            searchresult += 'Last Name ('+ln+')';            
	            ln = EncodingUtil.urlEncode(ln,'utf-8');//encode the search criteria and replace the '+' as '%20'
	            ln = ln.replaceAll('\\+', '%20');            
	            searchcriteria = 'last-name='+ln;// add the lastname to searchcriteria+'&current-title=true'	                        
	            if(fn!=null){
	            	searchresult += ', First Name ('+fn+')';
	                fn = EncodingUtil.urlEncode(fn,'utf-8');
	                fn = fn.replaceAll('\\+', '%20');
	                searchcriteria += '&first-name='+fn;  	                
	            }
	            shortsearchresult = searchresult;
	            if(fn == null){
	                mentionresult = 'First Name; ';
	            }
	            if(mc==null){
	                if(oc!=null){
	                	searchresult += ', Other Country ('+oc+')';
	                    LinkedinCountryCode countrycode = new LinkedinCountryCode();
	                    countrycode.linkedinCountrycontroller(oc);
	                    searchcriteria += '&country-code='+countrycode.Code;                     
	                }
	                else{
	                    mentionresult += 'Mailling Country; ';
	                }
	            }
	            if(mc!=null){
	                LinkedinCountryCode countrycode = new LinkedinCountryCode();
	                countrycode.linkedinCountrycontroller(mc);
	                searchcriteria += '&country-code='+countrycode.Code;
	                searchresult += ', Mailling Country ('+mc+')'; 
	            }
	            shortsearchcriteria = searchcriteria;
	            shortsearchresult = searchresult;
	            if(tl!=null){
	            	searchresult += ' and Title ('+tl+')';
	                tl = EncodingUtil.urlEncode(tl,'utf-8');
	                tl = tl.replaceAll('\\+', '%20');           
	                searchcriteria += '&title='+tl;                 
	            }
	            if(tl == null){
	                mentionresult += 'Title ';
	            }
	            if(mentionresult!=null){
	                mentionresult = 'Update the contact record with '+mentionresult+ ' to narrow the search result!';
	            }
            }
            else if(searchcriteria.contains('title=')){
            	searchresult += 'Last Name ('+ln+')'; 
            	if(fn!=null){
	            	searchresult += ', First Name ('+fn+')';
            	}
            	if(fn == null){
	                mentionresult = 'First Name; ';
	            }
	            if(mc==null){
	                if(oc!=null){
	                	searchresult += ', Other Country ('+oc+')';	                                        
	                }
	                else{
	                    mentionresult += 'Mailling Country; ';
	                }
	            }
	            if(mc!=null){	                
	                searchresult += ', Mailling Country ('+mc+')'; 
	            }
	            if(tl!=null){
	            	searchresult += ' and Title ('+tl+')';	                            
	            }
	            if(tl == null){
	                mentionresult += 'Title ';
	            }
	            if(mentionresult!=null){
	                mentionresult = 'Update the contact record with '+mentionresult+ ' to narrow the search result!';
	            }
            }
            else {
            	searchresult += 'Last Name ('+ln+')'; 
            	if(fn!=null){
	            	searchresult += ', First Name ('+fn+')';
            	} 
            	if(mc==null){
	                if(oc!=null){
	                	searchresult += ', Other Country ('+oc+')';	                                        
	                }
	            }
	            if(mc!=null){	                
	                searchresult += ', Mailling Country ('+mc+')'; 
	            }           	
            }
            try{
            	i = 0;
            	do{
            	if(i == 1){
            		searchcriteria = shortsearchcriteria; 
            		searchresult = shortsearchresult; 
            		mentionresult = '';          		
            	}            	
                url = 'https://api.linkedin.com/v1/people-search:(people:(id,first-name,last-name,public-profile-url,headline,picture-url,industry,location:(name)))?'+searchcriteria+'&start='+starter+'&count=6'; //Oauth search URL
            	Http h = new Http();//send the URL to Linkedin to get the XML
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setEndpoint(url);
                OAuth oa = new OAuth();        
                
                if(!oa.setService('linkedin')) {//If oa.setService is false, return to the AuthPage
                   // System.debug(oa.message);
                    message=oa.message;
                    jumpoauthpage = false;
	       			jumpauthpage = true;
	       			jumpprofilepage = false; 
                    linkedInAuthPageUrl = Page.AuthPage.getURL()+'?contactUrl='+contactUrl;
                    //system.debug('linkedInAuthPageUrl'+linkedInAuthPageUrl);
                    /*PageReference secondPage = Page.AuthPage; 
                    secondPage.setRedirect(true);
                    secondPage.getParameters().put('contactUrl',contactUrl); 
                    return secondPage; */
                }
                oa.sign(req);
                HttpResponse res = null;
                if(Test.isRunningTest()) {
                    // testing
                  res = new HttpResponse();
                } else {
                  res = h.send(req);
                }                
                //System.debug('Received response ('+res.getStatusCode()+' '+res.getStatus()+body+')');
                if(res.getStatusCode()>299 && res.getStatusCode()!=403){
                    body = 'Wrong Token! Please Click the Sign out Linkedin and Re-sign the Token!';
                }
                else if(res.getStatusCode()==403){
                    message = 'You have already exceeded 100 search request limit for today. The limit will be reset at midnight UTC!';
                }
                else{
                    try{
                    if(Test.isRunningTest()){
                        bodies ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><people-search>  <people total="110" count="10" start="11">    <person>      <id>jFzJJ-flUS</id>      <first-name>JUN</first-name>      <last-name>ZHOU</last-name>      <public-profile-url>http://www.linkedin.com/in/zhoujun</public-profile-url>      <headline>Director of Tech at Viacom</headline>    </person>    <person>      <id>AAhxUuGY1b</id>      <first-name>Jun</first-name>      <last-name>Zhou</last-name>      <public-profile-url>http://www.linkedin.com/pub/jun-zhou/3/488/238</public-profile-url>      <headline>Principal Software Engineer at MarketLive</headline>    </person>    <person>      <id>VszGMcBnqs</id>      <first-name>Jun</first-name>      <last-name>Zhou</last-name>      <public-profile-url>http://www.linkedin.com/pub/jun-zhou/9/495/231</public-profile-url>      <headline>Sr member of consulting staff at Magma Design Automation</headline>    </person>    <person>      <id>jHs9M-nNWh</id>      <first-name>Jun</first-name>      <last-name>Zhou</last-name>      <public-profile-url>http://www.linkedin.com/pub/jun-zhou/16/334/188</public-profile-url>      <headline>Entrepreneur and Private Investor</headline>    </person>    <person>      <id>Xbb73uEG4E</id>      <first-name>Jun</first-name>      <last-name>Zhou</last-name>      <public-profile-url>http://www.linkedin.com/pub/jun-zhou/28/768/90a</public-profile-url>      <headline>Research at Peking University</headline>    </person>    <person>      <id>MuQHxm5iDs</id>      <first-name>Jun Michael</first-name>      <last-name>Zhou</last-name>      <public-profile-url>http://www.linkedin.com/pub/jun-michael-zhou/16/866/91b</public-profile-url>      <headline>Pacific Marketing Manager - Strategical Marketing at W.L. Gore &amp; Associates</headline>    </person>    <person>      <id>CZvBQU4ILx</id>      <first-name>Jun</first-name>      <last-name>Zhou</last-name>      <public-profile-url>http://www.linkedin.com/pub/jun-zhou/21/398/167</public-profile-url>      <headline>Student at University of California, Davis</headline>    </person>    <person>      <id>kE3kMi_i96</id>      <first-name>Jun (GE Healthcare)</first-name>      <last-name>Zhou</last-name>      <public-profile-url>http://www.linkedin.com/pub/jun-ge-healthcare-zhou/33/655/70a</public-profile-url>      <headline>Account manager at GE Healthcare</headline>    </person>    <person>      <id>gqnj-gP9Vf</id>      <first-name>Jun</first-name>      <last-name>ZHOU</last-name>      <public-profile-url>http://www.linkedin.com/in/junzhou</public-profile-url>      <headline>Senior Managing Consultant at HP</headline>    </person>    <person>      <id>mk_GrF_9h1</id>      <first-name>Jun</first-name>      <last-name>Zhou</last-name>      <public-profile-url>http://www.linkedin.com/pub/jun-zhou/1a/119/70a</public-profile-url>      <headline>Postdoctoral Research Fellow at Harvard University</headline>    </person>  </people></people-search>';}
                    else{   
                        bodies = res.getBody();//body is the content of the request, XML                        
                    }//body is the content of the request, XML
                    LinkedinXml rx = new LinkedinXml();
                    rx.readXml(bodies);
                    SearchList  = rx.peopleSearch;
                    totalPeopleList = rx.totalPeopleList;
                    countPeopleList = '6'; //each page contain 6 contact  
                    if(rx.startPeopleList == null){//if the total number is less then the count number the XML doesn't include the start and count number
                        startPeopleList = '0';
                    }
                    else{
                        startPeopleList = rx.startPeopleList;
                    }              
                    if(integer.valueof(totalPeopleList) < = integer.valueof(countPeopleList)){//when the total number is less then the count number, the previous button and next button can't be clicked.
                        previousbutton = 'true';
                        nextbutton = 'true';
                        
                    }
                    else{//if the page is the first page, the Previous button can't be clicked and if the page is the last page, the next page can't be clicked.
                        integer totalpages = integer.valueof(totalPeopleList)/integer.valueof(countPeopleList);
                        integer pagenumbermod = math.mod(integer.valueof(totalPeopleList),integer.valueof(countPeopleList));                        
                        integer pagenumber = integer.valueof(startPeopleList)/integer.valueof(countPeopleList);
                        if (pagenumbermod == 0){
                        	totalpages = totalpages-1;
                        }
                        if(pagenumber == 0){
                            previousbutton = 'true';
                            nextbutton = 'false';
                        }
                        if(pagenumber == totalpages){
                            previousbutton = 'false';
                            nextbutton = 'true';
                        }  
                    }             
                   message = rx.message;
                    }
                   catch(system.exception e){
                        NotificationClass.notifyErr2Dev('OauthService/Search/ReadXML', e);
                        return null;
                    }
                }
                i++;            		
            	}while(i<=1&&message=='There is no search result!');
            	return null;
            }
            catch (system.exception e){
                NotificationClass.notifyErr2Dev('OauthService/Search', e);                
               return null;
            }
        }
    }
    public PageReference signout(){
        url = 'https://api.linkedin.com/uas/oauth/invalidateToken'; //Token Invalidation
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(url);
        OAuth oa = new OAuth();
        try{
        	oa.sign(req);
        	}
        catch (system.exception e){
            NotificationClass.notifyErr2Dev('OauthService/Signout', e);
            return null;
            }
        HttpResponse res = null;
        if(Test.isRunningTest()) {
           // testing
           res = new HttpResponse();
        } else {
           res = h.send(req);
        }
        body = res.getBody();
        if(!Test.isRunningTest()){
        	
        	CommonSelector.checkRead(OAuth_Service__c.sObjectType, 'Id');
        	CommonSelector.checkRead(OAuth_Token__c.sObjectType, 'Id');
	        service = [SELECT id,(select id FROM tokens__r WHERE Owner__c=:UserInfo.getUserId() ) //Delete the Token Record
	                          FROM OAuth_Service__c WHERE name = 'linkedin' and Active__c=true];
	               
	         try{
                OAuth_Token__c t = service.tokens__r;
               //
                if(t != null){
                    CredentialSettingService.deleteProtectedAccount(t);
                }
            }catch(Exception e){
                System.debug(e);
            }
	        
        }
        //authorationUrl = 'https://'+ApexPages.currentPage().getHeaders().get('Host')+page.AuthPage.getUrl();
       jumpauthpage= true;
       jumpoauthpage = false;
	   jumpprofilepage = false;  
       linkedInAuthPageUrl = Page.AuthPage.getURL()+'?contactUrl='+contactUrl;
       return null;
    }
    
    public PageReference linkedprofile(){
        name = ApexPages.CurrentPage().getParameters().get('pub_url');//Get the public URL and put the ID to the URL
        setstartPeoplelist(startPeoplelist);     
        PageReference proPage = Page.ProfilePage;
        proPage.setRedirect(true);
        proPage.getParameters().put('contactUrl',contactUrl); 
        proPage.getParameters().put('ProfilePage',name); 
        proPage.getParameters().put('start',start); 
        proPage.getParameters().put('searchcriteria',searchcriteria);   
        return proPage; 
    }
        
    public void setstartPeoplelist(String startPeoplelist) {
      start = startPeoplelist;           
   }
   
   public void setcountPeoplelist(String countPeoplelist) {
      count = countPeoplelist;        
   }
    public PageReference setnextPage(){//go to the next page
        setstartPeoplelist(startPeoplelist);  
        setcountPeopleList(countPeopleList);    
        integer a = integer.valueOf(start) + integer.valueOf(count);
        startnumber = String.valueOf(a);
        PageReference secondPage = Page.OauthService;
        secondPage.setRedirect(true);
        secondPage.getParameters().put('contactUrl',contactUrl); 
        secondPage.getParameters().put('start',startnumber); 
        secondPage.getParameters().put('searchcriteria',searchcriteria);     
        return secondPage;
        
    }
    public PageReference setpreviousPage(){//go to the Previous page
        setstartPeoplelist(startPeoplelist);  
        setcountPeopleList(countPeopleList);    
        integer a = integer.valueOf(start) - integer.valueOf(count);
        startnumber = String.valueOf(a);
        PageReference secondPage = Page.OauthService;
        secondPage.setRedirect(true);
        secondPage.getParameters().put('contactUrl',contactUrl); 
        secondPage.getParameters().put('start',startnumber); 
        secondPage.getParameters().put('searchcriteria',searchcriteria);        
        return secondPage;
        
    }
    static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){//Send error message
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());    
    }
}