@isTest
public with sharing class EmailDummyRecordCreator {

	public static List<Messaging.InboundEmail> createEmails() {
        String subject = 'www.jxt.solutions test';
        String fromAddress = 'test@test.com';
        String plainTextBody = 'test content';
        
        List<Messaging.InboundEmail.BinaryAttachment> attachments1 = createBinaryAttachment(new Map<String, String>() );
        List<Messaging.InboundEmail.BinaryAttachment> attachments2 = createBinaryAttachment(new Map<String, String>{'Resume' => 'test content'} ); 
        List<Messaging.InboundEmail.BinaryAttachment> attachments3 = createBinaryAttachment(new Map<String, String>{'Resume' => 'test content', 'CoverLetter' => 'test content'} ); 
        List<Messaging.InboundEmail.BinaryAttachment> attachments4 = createBinaryAttachment(new Map<String, String>{'Resume' => 'test content', 'CoverLetter' => 'test content', 'OtherDocument' => 'test content'}); 
	    
	    List<Messaging.InboundEmail> emails = new List<Messaging.InboundEmail>();
	    emails.add(createEmail(subject, plainTextBody, fromAddress, attachments1 ));
	    emails.add(createEmail(subject, plainTextBody, fromAddress, attachments2 ));
	    emails.add(createEmail(subject, plainTextBody, fromAddress, attachments3 ));
	    emails.add(createEmail(subject, plainTextBody, fromAddress, attachments4 ));

       return emails;
       
    }
    
    public static List<Messaging.InboundEmail.BinaryAttachment> createBinaryAttachment(Map<String, String> attachmentsMap) {
        List<Messaging.InboundEmail.BinaryAttachment> binaryAttachments = new List<Messaging.InboundEmail.BinaryAttachment>();
        if(attachmentsMap!=null && attachmentsMap.size()>0) {
            for(String a : attachmentsMap.keySet()) {
                Messaging.InboundEmail.BinaryAttachment attach = new Messaging.InboundEmail.BinaryAttachment();
        		attach.fileName = a;
        		attach.body = Blob.valueof(attachmentsMap.get(a));
                binaryAttachments.add(attach);
            }
        }
        return binaryAttachments;
    }
    
    public static Messaging.InboundEmail createEmail(String subject, String plainTextBody, String fromAddress, List<Messaging.InboundEmail.BinaryAttachment> attachments ) {
        Messaging.InboundEmail mail = new Messaging.InboundEmail();
        mail.plainTextBody = plainTextBody;
		mail.subject = subject;
		mail.fromAddress = fromAddress;
		if(attachments!=null && attachments.size()>0 ) {
		    mail.binaryAttachments = new List<Messaging.InboundEmail.BinaryAttachment>(attachments);
		}
        return mail;
    }
}