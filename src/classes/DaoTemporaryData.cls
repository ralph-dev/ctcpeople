public with sharing class DaoTemporaryData extends CommonSelector{
	
	public DaoTemporaryData(){
		super(Temporary_Data__c.sObjectType);
	}
	
	private static DaoTemporaryData dao = new DaoTemporaryData();
	

	public Temporary_Data__c saveScreenCandidateTemporaryData(String name, String dataContent, String secondaryDataContent){
		Temporary_Data__c tempData = new Temporary_Data__c();
		tempData.Name = name;
		tempData.Data_Content__c = dataContent;
		tempData.Secondary_Data_Content__c = secondaryDataContent;
		tempData.RecordTypeId = DaoRecordType.temporaryDataRT.Id;
		//checkFLS
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			Temporary_Data__c.Name,
			Temporary_Data__c.Data_Content__c,
			Temporary_Data__c.Secondary_Data_Content__c,
			Temporary_Data__c.RecordTypeId
		};
		fflib_SecurityUtils.checkInsert(Temporary_Data__c.SObjectType, fieldList);
		insert tempData;
		return tempData;
	}
	
	public void deleteOldScreenCandidateTemporaryData(Id userId){
		dao.checkRead('Id');
		List<Temporary_Data__c> tempDataToDel = [select Id from Temporary_Data__c where RecordTypeId =:DaoRecordType.temporaryDataRT.Id
			and CreatedDate < TODAY and ownerId =:  userId];
		if(tempDataToDel.size()>0){
			fflib_SecurityUtils.checkObjectIsDeletable(Temporary_Data__c.SObjectType);
			delete tempDataToDel;
		}
	}
	
	public Temporary_Data__c retrieveScreenCandidateTemporaryDataById(Id tempDataId){
		dao.checkRead('Name, Data_Content__c, Secondary_Data_Content__c');
		return [select Name, Data_Content__c, Secondary_Data_Content__c from Temporary_Data__c where Id=:tempDataId and RecordTypeId =:DaoRecordType.temporaryDataRT.Id];
	}
	
	public Temporary_Data__c retrieveLatestScreenCandidateTemporaryDataByName(String tempDataName){
		dao.checkRead('Name, Data_Content__c, Secondary_Data_Content__c');
		return [select Name, Data_Content__c, Secondary_Data_Content__c from Temporary_Data__c where Name=:tempDataName 
			and RecordTypeId =:DaoRecordType.temporaryDataRT.Id order by CreatedDate limit 1];
	}
	
}