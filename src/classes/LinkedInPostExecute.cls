//Send request to LinkedIn service
public with sharing class LinkedInPostExecute {
	public static Integer fetchLinkedInPostExecute(String body, String Method, String url){
		Integer linkedinresponsecode;
		String linkedinresponseheader;
		
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        url = url.replaceALL('~','%7E');
        
        //System.debug('Method: '+method+',URL: '+url+',body: '+ body);
        req.setMethod(method);
        req.setEndpoint(url);
        
        if(method=='POST' || method=='PUT') {
        	req.setHeader('Content-Type', 'text/xml');
        	req.setBody(body);
        }
        
        OAuth oa = new OAuth();
        if(!Test.isRunningTest()&&!oa.setService('linkedin')) {          
            return null;
        }
        //system.debug('req ='+ req);
        HttpResponse res = new HttpResponse();
        if(Test.isRunningTest()){
        	res.setStatusCode(200);
        	res.setBody('test');
        	system.debug('status = '+ res.getStatusCode());
	        
        }else{
        	oa.sign(req);
	        res = h.send(req);
        }
        String header = res.getHeader('Location');
        body = res.getBody();
        linkedinresponsecode = res.getStatusCode();
        System.debug('Received response ('+res.getStatusCode()+' '+res.getStatus()+body+')'+ 'header ='+ header);
        return linkedinresponsecode;
	}
}