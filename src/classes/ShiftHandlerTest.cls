@isTest
private class ShiftHandlerTest {
	final static String ROSTER_STATUS_FILLED = 'Filled';
	
    static testMethod void myUnitTest() {
        system.runAs(DummyRecordCreator.platformUser) {
            Account acc = TestDataFactoryRoster.createAccount();
        	Contact con = TestDataFactoryRoster.createContact();
            Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
            Shift__c shift = TestDataFactoryRoster.createShift(vac);
            Days_Unavailable__c roster = TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_FILLED);
           	ShiftHandler sHandler = new ShiftHandler();
           	sHandler.bulkBefore();
           	sHandler.bulkAfter();
           	sHandler.beforeInsert(shift);
           	sHandler.beforeUpdate(shift, shift);
           	sHandler.beforeDelete(shift);
           	sHandler.afterInsert(shift);
           	sHandler.afterUpdate(shift, shift);
           	sHandler.afterDelete(shift);
           	sHandler.afterUnDelete(shift);
           	sHandler.andFinally();
           	PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
           	pcSwitch.Delete_Roster_After_Delete_Shift_Trigger__c = true;
            upsert pcSwitch;
            delete shift;
            system.assertEquals([select id from Days_Unavailable__c].size(),0);
        }
    }
}