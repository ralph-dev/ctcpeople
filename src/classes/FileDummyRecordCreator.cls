public with sharing class FileDummyRecordCreator {

	public static List<ContentDocumentLink> createFilesForActivity(Task t) {
	    List<ContentDocumentLink> files = new List<ContentDocumentLink>();
	    files.add(createFileForActivity(t.Id, 'resume', 'resume.doc', 'test resume content'));
        files.add(createFileForActivity(t.Id, 'coverletter', 'coverletter.doc', 'test coverletter content'));
        files.add(createFileForActivity(t.Id, 'otherDocument', 'otherDocument.doc', 'test otherDocument content'));
        return files;
    }
    
    public static ContentDocumentLink createFileForActivity(String taskId, String title, String pathOnClient, String body) {
        ContentVersion contentVersion_1 = new ContentVersion(
          Title = title,
          PathOnClient = pathOnClient,
          VersionData = EncodingUtil.Base64Decode(body)
        );
        //check FLS
        List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
            ContentVersion.Title,
            ContentVersion.PathOnClient,
            ContentVersion.VersionData
        };
        fflib_SecurityUtils.checkInsert(ContentVersion.SObjectType, fieldList);
        insert contentVersion_1;
        
        CommonSelector.checkRead(ContentVersion.sObjectType, 'Id, Title, ContentDocumentId');
        String contentDocumentId = [SELECT Id, Title, ContentDocumentId FROM ContentVersion where Id =: contentVersion_1.Id].ContentDocumentId;
        ContentDocumentLink cdl2 = new ContentDocumentLink(LinkedEntityId=TaskId, ContentDocumentId=contentDocumentId, ShareType='I' );
        List<Schema.SObjectField> cdlFieldList = new List<Schema.SObjectField>{
            ContentDocumentLink.LinkedEntityId,
            ContentDocumentLink.ContentDocumentId,
            ContentDocumentLink.ShareType
        };
        fflib_SecurityUtils.checkInsert(ContentDocumentLink.SObjectType, cdlFieldList);
        insert cdl2;
        return cdl2;
    }

}