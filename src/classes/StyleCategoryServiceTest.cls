@isTest
private class StyleCategoryServiceTest {

	private static testMethod void test() {
		
		DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
		
		System.runAs(DummyRecordCreator.platformUser) {
	    Id saveSearchRecordTypeId = DaoRecordType.GoogleSaveSearchRT.Id;
	    
        
		
		List<Map<String, Object>> sobjs = new List<Map<String, Object>>();
		Map<String, Object> sobj = new Map<String, Object>();
		
		sobj.put('RecordTypeId', saveSearchRecordTypeId);
		sobj.put('Name' , 'testStyleCategoryRecord');
		sobj.put('Active__c', true);
		sobj.put('templateActive__c', true);
		
		sobjs.add(sobj);
		
		StyleCategoryService sService = new StyleCategoryService();
		List<SObject> styleCategoryRecords = sService.createRecords(sobjs);
		system.assert(styleCategoryRecords.size()>0);
		
		List<Map<String, Object>> sobjsUpdate = new List<Map<String, Object>>();
		Map<String, Object> sobjUpdate = new Map<String, Object>();
		sobjUpdate = sobjs[0];
		sobjUpdate.put('Id', styleCategoryRecords[0].Id);
		sobjsUpdate.add(sobjUpdate);
		
		styleCategoryRecords = sService.updateRecords(sobjsUpdate);
		system.assert(styleCategoryRecords.size()>0);
		
		styleCategoryRecords = sService.updateSaveSearchRecords(sobjsUpdate);
		system.assert(styleCategoryRecords.size()>0);
		
		sobj.put('Name' , 'testStyleCategoryRecordSave');
		styleCategoryRecords = sService.createSaveSearchRecords(sobjs);
		system.assert(styleCategoryRecords.size()>0);
		}
	}

}