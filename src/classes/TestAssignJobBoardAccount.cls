/**
 * This is the test class for AssignJobBoardAccount
 *
 * Updated by: Lina Wei
 * Updated on: 27/03/2017
 */

@isTest
private class TestAssignJobBoardAccount {
	static testmethod void TestAssignJobBoardAccount(){
	    system.runAs(DummyRecordCreator.platformUser) {
			// create data
	        Advertisement_Configuration__c seekAccount = AdConfigDummyRecordCreator.createDummySeekAccount();
			Advertisement_Configuration__c indeedAccount = AdConfigDummyRecordCreator.createDummyIndeedAccount();
    	    StyleCategory__c LinkedInAccount = StyleCategoryDummyRecordCreator.createLinkedInAccount();

    		AdvertisementConfigurationSelector acSelector = new AdvertisementConfigurationSelector();
            List<Advertisement_Configuration__c> seekAccountList = acSelector.getActiveAccountByRecordTypeId( DaoRecordType.seekAccountRT.Id);

    		AssignJobBoardAccount testAccount = new AssignJobBoardAccount();
    		system.assertEquals(seekAccountList.size(), testAccount.jobBoardAccountDetails.size());
    		testAccount.updateuserdetails();
    		Boolean msgSignalhasmessage = AssignJobBoardAccount.customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CANDIDATE_SELECTED);
    		system.assert(msgSignalhasmessage);
	    }
	}

	static testMethod void testGeneralPicklistValueWithoutActiveAccount() {
		 System.runAs(DummyRecordCreator.platformUser) {
		AssignJobBoardAccount controller = new AssignJobBoardAccount();
		controller.generalJobPostingAccountPicklistValue();
		System.assertEquals(1, controller.jobBoardAccountDetails.size());
		System.assertEquals(1, controller.linkedInjobBoardAccountDetails.size());
		System.assertEquals(1, controller.IndeedJobBoardAccountDetails.size());
		 }
	}

	static testMethod void testUserAssignedAccountClass() {
		System.runAs(DummyRecordCreator.platformUser) {
			AssignJobBoardAccount.UserAssignedAccount userAccount = new AssignJobBoardAccount.UserAssignedAccount(DummyRecordCreator.platformUser, null, null);
			System.assertNotEquals(null, userAccount.getAssignedSeekAccounts());
			userAccount.addAssignedSeekAccount('test');
			System.assertEquals(1, userAccount.assignedSeekAccounts.size());
		}
	}

	static testMethod void testSetDefaultSeekAccount() {
		User admin = DummyRecordCreator.admin;
		User platUser = DummyRecordCreator.platformUser;
		System.runAs(admin) {
			// prepare data
			Advertisement_Configuration__c seekAccount1 = AdConfigDummyRecordCreator.createDummySeekAccount();
			Advertisement_Configuration__c seekAccount2 = AdConfigDummyRecordCreator.createDummySeekAccountWithoutInsert('default seek account');
			insert seekAccount2;

			platUser.Seek_Account__c = seekAccount1.Id + ';' + seekAccount2.Id;
			platUser.Default_Seek_Account__c = seekAccount2.Id;
			update platUser;

			AssignJobBoardAccount controller = new AssignJobBoardAccount();

			for (AssignJobBoardAccount.UserAssignedAccount uaa : controller.userAssignedAccounts) {
				if (uaa.user.Id.equals(platUser.Id)) {
					System.assertEquals(2, uaa.assignedSeekAccounts.size());
					System.assertEquals(3, uaa.availableSeekAccounts.size());
				} else if (uaa.user.Id.equals(admin.Id)){
					System.assertEquals(0, uaa.assignedSeekAccounts.size());
					System.assertEquals(1, uaa.availableSeekAccounts.size());
					uaa.assignedSeekAccounts.add(seekAccount1.Id);
				}
			}

			controller.userToChange = admin.Id;
			controller.updateAvailableSeekAccounts();

			for (AssignJobBoardAccount.UserAssignedAccount uaa : controller.userAssignedAccounts) {
				if (uaa.user.Id.equals(platUser.Id)) {
					System.assertEquals(2, uaa.assignedSeekAccounts.size());
					System.assertEquals(3, uaa.availableSeekAccounts.size());
					uaa.assignedSeekAccounts = new List<String>();
				} else if (uaa.user.Id.equals(admin.Id)) {
					System.assertEquals(1, uaa.assignedSeekAccounts.size());
					System.assertEquals(2, uaa.availableSeekAccounts.size());
				}
			}

			controller.userToChange = platUser.Id;
			controller.updateAvailableSeekAccounts();

			for (AssignJobBoardAccount.UserAssignedAccount uaa : controller.userAssignedAccounts) {
				if (uaa.user.Id.equals(platUser.Id)) {
					System.assertEquals(0, uaa.assignedSeekAccounts.size());
					System.assertEquals(1, uaa.availableSeekAccounts.size());
				}
			}
		}
	}

}