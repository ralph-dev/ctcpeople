/*
    This is a test code for the class CustomfieldList.
    Assuming that you have already have a fieldset which is 
    in Contact Object. 
    
    The name of fieldset is "RevMarketCandiateFieldSet"; 
    If there is no such fieldset, please manually create one.

*/
@isTest  
public class TestCustomfieldlist{
static testmethod void testcustomfieldlist(){
	System.runAs(DummyRecordCreator.platformUser) {
        CustomFieldList cufl=new CustomFieldList('Contact','RevMarketCandiateFieldSet');
        cufl.setfieldsets('RevMarketCandiateFieldSet');
        cufl.setobjects('Contact');
        System.assertEquals(cufl.getobjects(),'Contact');
        System.assertEquals(cufl.getfieldsets(),'RevMarketCandiateFieldSet');
        
	}
    }
    
 }