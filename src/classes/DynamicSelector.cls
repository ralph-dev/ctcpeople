/**
 * 
 * This class allow dynamically specify object from which selector will query
 * during the run time. During initialization of DynamicSelector, sobject type
 * need to be specify. In addition, a template id is required in initializtion
 * which will be used for fields to query.
 * 
 * @Author Raymond Zhu
 * 
 * 
 * */
public with sharing class DynamicSelector extends CommonSelector{

    private SObjectType targetSObjectType;
    private List<String> fieldsToQuery;
    private String objectName;
    
    public class CouldNotInitializeSelectorException extends Exception {

    }
    
    
  
    
    /**
     * This private constructor encapsulate common initialization code that are used
     * by other public constructors.
     * 
     * */
    private DynamicSelector(Boolean includeFieldSetFields, Boolean enforceCRUD, Boolean enforceFLS){
        // Initialize security setting
        if (!Test.isRunningTest()) {
            super(includeFieldSetFields, enforceCRUD, enforceFLS);
        } else {
            super(includeFieldSetFields, enforceCRUD, false);
        }
    }
    
    private Boolean isTemplateIdProvided(String templateId) {
        return templateId != null;
    }
    
    /**
     * 
     * @param sobjectType 
     *          SObject from which queries will be executed from
     * @param templateId
     *          Template id binds to specific sets of field.
     * 
     * */
    public DynamicSelector(String sobjectType, String templateId, Boolean includeFieldSetFields, Boolean enforceCRUD, Boolean enforceFLS) {
        this(includeFieldSetFields, enforceCRUD, enforceFLS);
        objectName = sobjectType;
        List<Schema.DescribeSObjectResult> describeSObjectResult = Schema.describeSObjects(new String[]{
                sobjectType});
        if (describeSObjectResult.size() == 0) {
            throw new CouldNotInitializeSelectorException('Fail to initialize selector. Have you provided a valid sobject type token?');
        } else {
            Schema.DescribeSObjectResult describe = describeSObjectResult.get(0);
            targetSObjectType = describe.SObjectType;
            FieldResolver fieldResolver = new DefaultFieldResolver();
            // If template not provided, make all fields available for selection during SOQL query
            if (isTemplateIdProvided(templateId)) {
                fieldsToQuery = fieldResolver.getDisplayFieldsInStringList(templateId);    
            } else {
                fieldsToQuery = new List<String>(describe.fields.getMap().keySet());
            }            
        }
    }
    
    public DynamicSelector(String sobjectType, Boolean includeFieldSetFields, Boolean enforceCRUD, Boolean enforceFLS) {
        this(sobjectType, null, includeFieldSetFields, enforceCRUD, enforceFLS);
    } 

    /**
     *  Return fields to query. Reference field is extended by querying its name field.
     * 
     * */
    public List<String> getExtendedSObjectFieldList() {
        fflib_SObjectDescribe sobjDescribe = fflib_SObjectDescribe.getDescribe(targetSObjectType);
        List<String> actualFieldsToQuery = new List<String>();
        Map<String, Schema.SObjectField> fieldsMap = sobjDescribe.getDescribe().fields.getMap();
        // Returned fields are determined during the initialization of this dynamic selector
        for (String f : fieldsToQuery) {
            /**
             *  There is an unknown issue relate to address compound field 
             *  while doing code migration using Salesforce migration tool (API v33.0).
             *  
             *  Because the root cause is not known due to the lack of visibility of backend
             *  execution of Salesforce migration tool, the problem seems to be fflib_SObjectDescribe
             *  is using API version higher than 30.0 regardless the actual API version that was set in the class meta data file 
             *  during the test executaion in migration which then make sobjDescribe.getDescribe().fields 
             *  return compound address fields. When these compound address field is referenced in classes 
             *  that are in version lower than 30.0, they couldn't be referenced because it's not supported
             *  in that API version.
             * 
             *  Below code is a temporary solution which ignore address field during test.
             *  
             * */
            if (Test.isRunningTest() &&  isAddressCompoundField(f)) {
                continue;
            }            
            Schema.SObjectField field = f.indexOf('.') >=0?sobjDescribe.getField(f.split('.', -1)[0]):sobjDescribe.getField(f);
            //system.debug('field des:'+field);
             Schema.DescribeFieldResult dfResult = field.getDescribe();
            if (dfResult.isAccessible()) {
                actualFieldsToQuery.add(f);
                if(isNotRecordTypeNorIdNorRelationship(f)) {
                    /*
                        Add name field of a relationship as additional query field. Some reference fields
                        are buggy to include, like person account fields. For those fields, they will 
                        be ignored while executing SOQL query.
                    */
                    if (isReferenceFieldButNotPersonAccountField(dfResult) && isReferenceFieldButNotPartnerNetwork(dfResult)) {                        
                        String additionalField = dfResult.getRelationshipName();
                        additionalField += '.Name';
                        actualFieldsToQuery.add(additionalField);
                    }
                }
            
            }
        }
        
        return actualFieldsToQuery;
    }
    

    private Boolean isReferenceFieldButNotPartnerNetwork(Schema.DescribeFieldResult dfResult) {
       return (dfResult.getType() == Schema.DisplayType.Reference) 
            && !dfResult.getName().endsWithIgnoreCase('__pc') 
            &&  (
                !dfResult.getRelationshipName().contains('PartnerNetworkConnection')
                    && !dfResult.getRelationshipName().contains('PartnerNetworkRecordConnection')
                    && !dfResult.getRelationshipName().equalsIgnoreCase('ConnectionReceived')
                    && !dfResult.getRelationshipName().equalsIgnoreCase('ConnectionSent')
                );
            //&& !dfResult.getRelationshipName().equalsIgnoreCase('ConnectionReceived') && !dfResult.getRelationshipName().equalsIgnoreCase('ConnectionSent')
    }
    /**
     *  Since API 30.0, Salesforce introduce address compound field.
     *  (https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/compound_fields_address.htm) 
     * 
     *  There are limits and undocumented issues while using address compound field. 
     *  (http://salesforce.stackexchange.com/questions/29304/with-json-how-to-serialize-the-accounts-shipping-and-billing-address)
     *  To ensure program can execute correctly, there is a need to idenfity if a given field
     *  is a address compound field which may cause issue during program execution.
     * */
    private boolean isAddressCompoundField(String fieldName) {
        return fieldName.equalsIgnoreCase('BillingAddress') 
            || fieldName.equalsIgnoreCase('Address')
            || fieldName.equalsIgnoreCase('ShippingAddress')
            || fieldName.equalsIgnoreCase('PersonMailingAddress')
            || fieldName.equalsIgnoreCase('PersonOtherAddress');
            
    }    
    
    private boolean isNotRecordTypeNorIdNorRelationship(String f) {
        return !f.equalsIgnoreCase('recordtypeinfo') && !f.equalsIgnoreCase('id') 
                && !f.contains('.');
    }

    /**
     * @override
     *
     **/
    private Boolean isReferenceFieldButNotPersonAccountField(Schema.DescribeFieldResult dfResult) {
        return (dfResult.getType() == Schema.DisplayType.Reference) 
            && !dfResult.getName().endsWithIgnoreCase('__pc') &&  !dfResult.getName().equalsIgnoreCase('PersonContactId');
    }
    
    /**
     * @override
     *
     **/
    public override Schema.SObjectType getSObjectType(){
        return targetSObjectType;
    }

    /**
     * @override
     *
     **/
    public override LIST<Schema.SObjectField> getSObjectFieldList(){
        fflib_SObjectDescribe sobjDescribe = fflib_SObjectDescribe.getDescribe(targetSObjectType);
        // Convert field string to object
        List<Schema.SObjectField> fieldsToQueryInObj = new List<Schema.SObjectField>();
        for (String f : fieldsToQuery) {
            if (f.indexOf('.') >=0 ) {
                continue;
            }
            Schema.SObjectField sobjField = sobjDescribe.getField(f);
            if (sobjField != null) {
                if (sobjField.getDescribe().isAccessible()){
                    fieldsToQueryInObj.add(sobjField);    
                }
            } else {
                throw new CouldNotInitializeSelectorException('Fail to initialize selector. Can not find field "'+f+'" indicated by the template.');        
            }
        }
        return fieldsToQueryInObj;
    }
    

    public List<SObject> selectSObjectByIds(Set<Id> idSet) {
         SimpleQueryFactory qf = simpleQuery();
         qf.selectFields(getExtendedSObjectFieldList());
         qf.setCondition('Id in :idSet');
         String soqlStr = qf.toSOQL();
         //system.debug('@@:' + soqlStr);
         return Database.query(soqlStr);
    }
    
    public List<SObject> selectSObjectByOffsetLimit(Integer offset, Integer limitCount) {
         SimpleQueryFactory qf = simpleQuery();
         qf.selectFields(getExtendedSObjectFieldList());
         qf.setlimit(limitCount);
         qf.setOffset(offset);
         String soqlStr = qf.toSOQL();
         //system.debug('@@:' + soqlStr);
         return Database.query(soqlStr);
    }
   
    public List<SObject> selectSObjectByForeignKeyIds(String foreignKey, Set<String> idSet) {
         SimpleQueryFactory qf = simpleQuery();
         qf.selectFields(getExtendedSObjectFieldList());
         qf.setCondition(String.escapeSingleQuotes(foreignKey) + ' in :idSet');
         String soqlStr = qf.toSOQL();
         return Database.query(soqlStr);
    }
    public List<SObject> selectSObjectByForeignKeyIdsWithLimit(String foreignKey, Set<String> idSet, Integer limitCount) {
         SimpleQueryFactory qf = simpleQuery();
         qf.selectFields(getExtendedSObjectFieldList());
         qf.setCondition(String.escapeSingleQuotes(foreignKey) + ' in :idSet');
         qf.setLimit(limitCount);
         String soqlStr = qf.toSOQL();
         return Database.query(soqlStr);
    }
    
    public String constructQueryForSelectFirstRecord(Integer limitCount){
         SimpleQueryFactory qf = simpleQuery();
         qf.selectFields(getExtendedSObjectFieldList());
         qf.setlimit(limitCount);
         String soqlStr = qf.toSOQL();
         //system.debug('@@:' + soqlStr);
         return soqlStr;
    }
    
}