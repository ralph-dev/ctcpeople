public class GoogleSearchUtil2 {
	// tranfer ids of set to (id1, id2, id3, ....);
	public static String setString(Set<Id> idSet){
		if(idSet.size()==0){
			CareerOneDetail.notificationsendout('must choose at least one candidate','GSANoRecordException','GoogleSearchUtil2', 'setString');
			//throw new GSANoRecordException('must choose at least one candidate');
		} 
		String temp = '(';
		for(String s:idSet){
			temp+='\''+s+'\''+',';
		}
		return temp.substring(0, temp.length()-1)+')';
	}
	
	// transfer a list of ids to (id1, id2, id3, ....);
	public static String generateStringFromList(List<Id> idList){
		if(idList.size() == 0 ){
			CareerOneDetail.notificationsendout('must choose at least one candidate','GSANoRecordException','GoogleSearchUtil2', 'setString');
			//throw new GSANoRecordException('must choose at least one candidate');	
		}
		String temp = '(';
		for(Id id: idList){
			temp+='\''+id+'\''+',';
		}
		return temp.substring(0, temp.length()-1)+')';
	}
	
	// transfer string of list to a string joined with "::"
	public static String getListString(List<String> stringList){
		if(stringList.size()==0){
			CareerOneDetail.notificationsendout('contact xml parse error','GSANoRecordException','GoogleSearchUtil2', 'getListString');
			//throw new GSANoRecordException('contact xml parse error');
		}
		String temp = '';
		for(String s:stringList){
			temp+=s+'::';
		}
		return temp.subString(0, temp.length()-2);
	}
	
	//tranform the json object Array to String format
	public static String getJsonArrayString(List<JSONObject> jobjlist){
		if(jobjlist.size()==0){
			//CareerOneDetail.notificationsendout('parse contact xml error','GSANoRecordException','GoogleSearchUtil2', 'getJsonArrayString');
			//throw new GSANoRecordException('parse contact xml error');
			return '';
		}
		String temp = '[';
		for(JSONObject jobj:jobjlist){
			temp+=jobj.valueToString()+', ';
		}
		String temp2 = temp.subString(0, temp.length()-2);
		return temp2+']';
	}
	
	//tranform a map to json string 
	public static String generateJsonStringFromMap(Map<String,Map<String,String>> map2Convert){
		if(map2Convert.size()==0){
			return '';
		}
		
		String newJsonString = JSON.serialize(map2Convert);
		System.debug('newJsonString = ' + newJsonString);
		
		return newJsonString;
	}
	
	
	
	public static String getCustomSetting(String name){
		String collectionName = '';
		GSA__c collection = GSA__c.getValues(name);
        if(collection==null || collection.name__c==null || collection.name__c==''){
            CareerOneDetail.notificationsendout('Please input the GSA collection name','GSANoRecordException','GoogleSearch2', 'GoogleSearch2');
            //throw new GSANoRecordException('Please input the GSA collection name'); 
        }else{
            collectionName = collection.name__c;
        }
        return collectionName;
	}		
	
	//get return url
    public static String returnUrl(string vid, String scjid, String cjid){
    	String tempreturnUrl;
    	if(vid!=null && vid!= ''){
    		tempreturnUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/'+ PeopleCloudHelper.getPackageNamespace()+'advancedContactSearch?pid='+vid+'&scjid='+scjid+'&cjid='+cjid;
    	}
    	else{
    		tempreturnUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/'+ PeopleCloudHelper.getPackageNamespace()+'advancedContactSearch?scjid='+scjid+'&cjid='+cjid;
    	}
    	return tempreturnUrl;
    }
    
    //Fetch the doc and convert to Map<String, String>
    public static Map<String, String> JSONDeserializeCandidateJsonDoc(String CandidateJsonDocId){
    	Map<String, String> tempjsondocbodyMap = new Map<String, String>();
    	Document tempjsondoc = new Document();
    	String tempjsondocbody = '';
    	tempjsondoc = DaoDocument.getCandidateRecordDummyDoc(CandidateJsonDocId);
    	tempjsondocbody = tempjsondoc.Body.toString();
    	tempjsondocbodyMap = (Map<String, String>)JSON.deserialize(tempjsondocbody,Map<String, String>.class);
    	
    	return tempjsondocbodyMap;
    }
    
    //Fetch the candidate id list
    public static List<Id> fetchCandidateIdList(String CandidateJsonDocId){
    	List<Id> CandidateList =  new List<Id>();
    	Map<String, String> tempjsondocbodyMap = new Map<String, String>();
    	List<String> tempjsondocbodyMapKeySet = new List<String>();
    	tempjsondocbodyMap = JSONDeserializeCandidateJsonDoc(CandidateJsonDocId);
    	tempjsondocbodyMapKeySet.addAll(tempjsondocbodyMap.keySet());
    	for(Integer i=0;i<tempjsondocbodyMap.size();i++) {
    		List<Id> sublist = new List<Id>();
    		sublist = tempjsondocbodyMap.get(tempjsondocbodyMapKeySet.get(i)).split(':');
    		CandidateList.addAll(sublist);
    	}
    	return CandidateList;
    }
    
    public static String getgooglesearchdoc(String SelectedObject){
    	String namestr = '';
    	String NameSpacePrefix = PeopleCloudHelper.getPackageNamespace();
    	if(SelectedObject.endsWith('__c')){ //SelectedObject is Placement__c
	    	Integer l = SelectedObject.length() -3 ;
	        namestr = SelectedObject.substring(0,l);
	        if(NameSpacePrefix != null && NameSpacePrefix != ''){
	        	if(namestr.contains(NameSpacePrefix)){
	            	Integer index1 = NameSpacePrefix.length();
					namestr = namestr.substring(index1);
	            }
	        }
	    }
	    return namestr;
    }
    
    public static List<Component> generalGoogleSearchComponent(String SelectedObject){
    	List<Component> tempcomponentlist = new List<Component>();
    	String folderName = GoogleSearchUtil2.getgooglesearchdoc(SelectedObject);
    	Folder ctcFolder = DaoFolder.getFolderByDevName('CTC_Admin_Folder');
        Document jsondoc = DaoDocument.fetchdocuments(ctcFolder.id, folderName);
        String docbody = jsondoc.Body.toString();
        if(docbody!= null && docbody != ''){
        	
        }
        return tempcomponentlist;
    }
    
    public static List<JSONObject> googleSearchPageRequestJsonString(String SelectedObject, String location){
    	String JSONdocname = 'ClicktoCloud_JSON_'+SelectedObject;
    	Folder ctcFolder = DaoFolder.getFolderByDevName('CTC_Admin_Folder');
    	
		String folderId = ctcFolder.id;
		system.debug('folderId =' + folderId);
		List<Component> componentlist = new List<Component>();
        String returnJsonString = '';
        List<JSONObject> jobjlist = new List<JSONObject>();
        
        Document JSONSdoc = DaoDocument.fetchdocuments(folderId, JSONdocname);
       	if(JSONSdoc != null){
	        String JSONSbody =JSONSdoc.Body.toString();
	        componentlist = (List<Component>)JSON.deserialize(JSONSbody, List<Component>.class);
	        //List<Component> newcomponenetlist = new List<Componenet>();
	        if(componentlist!= null && componentlist.size()>0){
	        	for(Component com : componentlist){
	        		com.htmlbody = generalComponentString(com, SelectedObject);
	        		JSONObject jsonobj = new JSONObject();
	        		if(com.location.equalsIgnoreCase(location)){
	        			jsonobj.putOpt('component', new JSONObject.value((com.htmlbody).replaceAll('"', '\\\\"')));
	        			jsonobj.putOpt('location',new JSONObject.value(com.location));
	        			jsonobj.putOpt('filterable', new JSONObject.value('true'));
	        			jsonobj.putOpt('apiname', new JSONObject.value(com.apiname));
	        			jsonobj.putOpt('sequence', new JSONObject.value(com.sequence));
	        			jsonobj.putOpt('label', new JSONObject.value(com.label));
	        			jsonobj.putOpt('type', new JSONObject.value(com.fieldtype));
	        			jobjlist.add(jsonobj);
	        		}       		
	        	}
	        	system.debug('jobjlist ='+ jobjlist);
	        }
        }
    	return jobjlist;
    }
    
    //General the html code for JSON String
	public static String generalComponentString(Component com, String SelectedObject){
		String compStr = '';
		String googlesearchfieldoperator = '';
		List<String> picklistvalues = new List<String>();
		List<sObjectType> relatedobj = new List<sObjectType>();
		if(!com.apiname.contains('.')){
			if(com.fieldtype.toLowerCase() == 'picklist' && com.apiname.toLowerCase()!= 'recordtypeid'){// Get Picklist field XML, not include the recordtype field
				picklistvalues = SearchAdminControllerUtils.getThisPicklistValues(SelectedObject, com.apiname);
				if(picklistvalues != null){
					googlesearchfieldoperator = '<div><SELECT class="CTC_XML_OPERATOR" SIZE="1" id="CTC_OPERATOR_' + com.apiname + '" ><OPTION VALUE="Include">Includes</OPTION><OPTION VALUE="excludes">Excludes</OPTION></SELECT>';
					compStr ='<SELECT  name="CTC_XML_COMPONENT" multiple SIZE="5" id="CTC_' +  com.apiname + '" >';
		           
		            for(String ss: picklistvalues ){
		            	compStr = compStr + '<OPTION VALUE="'+ ss +'" >' + ss ;
		                }
		            compStr = compStr + '</SELECT></div>';
		            }
		        }
		        else if(com.fieldtype.toLowerCase() == 'picklist' && com.apiname.toLowerCase() == 'recordtypeid'){// Get Picklist field XML for the recordtype field
		        	
		        	RecordType[] rts = DaoRecordType.getActiveRecordTypesByObjectType(SelectedObject);//[select id,name from RecordType where SobjectType=: SelectedObject and IsActive = true];
		            if(rts != null){
		            	if(rts.size()>0){
		                	googlesearchfieldoperator = '<div><SELECT class="CTC_XML_OPERATOR" SIZE="1" id="CTC_OPERATOR_' + com.apiname + '" ><OPTION VALUE="Include">Includes</OPTION><OPTION VALUE="excludes">Excludes</OPTION></SELECT>';
							compStr ='<SELECT  name="CTC_XML_COMPONENT" multiple SIZE="3" id="CTC_' + com.apiname + '" >';
		                                
		                    for(RecordType rt: rts){
		                    	compStr = compStr + '<OPTION VALUE="'+ rt.Id +'" >' + rt.Name ;                                   
		                    }
		                    compStr = compStr + '</SELECT></div>';
		                }
		            }
		        }
		         else if(com.fieldtype.toLowerCase().contains('date')){//General Date Field XML
              		 googlesearchfieldoperator = '<div><SELECT class="CTC_XML_OPERATOR" SIZE="1" id="CTC_OPERATOR_' + com.apiname + '" >'+
              		 '<OPTION VALUE="equals">Equals</OPTION>'+
             		 '<OPTION VALUE="greaterorequal">Greater or equal</OPTION>'+
             		 '<OPTION VALUE="lessorequal">Less or equal</OPTION>'+
              		 '</SELECT>';
               		 compStr ='<input  name="CTC_XML_COMPONENT" class="datepicker" id="CTC_' +  com.apiname + '" autocomplete="off" /></div>';
             }
		         else if(com.fieldtype.toLowerCase() == 'textarea'){//General textarea field
		         	googlesearchfieldoperator = '<div><SELECT class="CTC_XML_OPERATOR" SIZE="1" id="CTC_OPERATOR_' + com.apiname + '" ><OPTION VALUE="equals">Equals</OPTION><OPTION VALUE="notequals">Not Equals</OPTION></SELECT>';
		       		compStr = '<input  name="CTC_XML_COMPONENT" type="text" id="CTC_' +  com.apiname + '" value="" style="width:350px;" /></div>';
		         }
		         else if(com.fieldtype.toLowerCase() == 'multipicklist'){   
		         	googlesearchfieldoperator = '<div><SELECT  class="CTC_XML_OPERATOR" SIZE="1" id="CTC_OPERATOR_' + com.apiname + '" ><OPTION VALUE="Include">Includes</OPTION><OPTION VALUE="excludes">Excludes</OPTION></SELECT>';
					picklistvalues = SearchAdminControllerUtils.getThisPicklistValues(SelectedObject, com.apiname);
		            if(picklistvalues != null){
		            	compStr ='<SELECT  name="CTC_XML_COMPONENT" multiple SIZE="5" id="CTC_' + com.apiname + '" >';
		           
		                for(String ss: picklistvalues ){
		                	compStr = compStr + '<OPTION VALUE="'+ ss +'" >' + ss ;
		                }
		            compStr = compStr + '</SELECT></div>';
		            }
		         }
		         else if(com.fieldtype.toLowerCase() == 'reference'){
		         	if(com.fieldtype.toLowerCase() == 'recordtypeid'){
		         		RecordType[] rts = DaoRecordType.getActiveRecordTypesByObjectType(SelectedObject);//[select id,name from RecordType where SobjectType=: SelectedObject and IsActive = true];
		                if(rts != null){
		                	if(rts.size()>0){
		                		googlesearchfieldoperator = '<div><SELECT class="CTC_XML_OPERATOR" SIZE="1" id="CTC_OPERATOR_' + com.apiname + '" ><OPTION VALUE="Include">Includes</OPTION><OPTION VALUE="excludes">Excludes</OPTION></SELECT>';
								compStr ='<SELECT  name="CTC_XML_COMPONENT" multiple SIZE="5" id="CTC_' + com.apiname + '" >';
		                                
		                        for(RecordType rt: rts){
		                        	compStr = compStr + '<OPTION VALUE="'+ rt.Id +'" >' + rt.Name ;                                   
		                        }
		                        compStr = compStr + '</SELECT></div>';
		                    }
		                }
		           	}
		            else{
		            	/*List<FieldsClass.AvailableField> temp_all_fields = new List<FieldsClass.AvailableField>{};
		                        
		                relatedobj = s.getRelatedObject();
		                temp_all_fields = s.getRelatedObjectFields();
		                if(relatedobj!= null && relatedobj.size() ==1 ){
		                // ** if the relatedobj has more than one entry, the s.getRelationshipName()should be not correct. in study
		                	if(temp_all_fields.size()>0){
		                    	compStr = '<div><SELECT  name="CTC_XML_COMPONENT_SELECT"  SIZE="1" id="CTC_SELECT_' + s.getFieldApi() + '" >';
		                        for(FieldsClass.AvailableField af: temp_all_fields ){
		                        	compStr = compStr + '<OPTION VALUE="'+ s.getRelationshipName() + '.' + af.getFieldApi() +'" >' + af.getFieldLabel() ;
		                        }
		                        compStr = compStr + '</SELECT>';
		                        compStr = compStr + '&nbsp;&nbsp;<input  name="CTC_XML_COMPONENT_INPUT" type="text" id="CTC_INPUT_' + s.getFieldApi() + '" value=""/></div>';
		                    }
		                    else{
		                    	compStr = null;
		                    }
		                 }
		                 else{
		                 	compStr = '<div>This Reference field is related to more than one object. That can not be searchable so far. Please contact your system administrator. </div>';
		                 }*/
		            }
		       }else{
		       		googlesearchfieldoperator = '<div><SELECT class="CTC_XML_OPERATOR" SIZE="1" id="CTC_OPERATOR_' + com.apiname + '" ><OPTION VALUE="equals">Equals</OPTION><OPTION VALUE="notequals">Not Equals</OPTION></SELECT>';
		       		compStr = '<input  name="CTC_XML_COMPONENT" type="text" id="CTC_' + com.apiname + '" value=""/></div>';
		       }
	    }
	    return googlesearchfieldoperator+compStr;
	}

	public static String generateColumnString(String columnJsonString,Map<String,DisplayColumn> relationNameMap){
		system.debug('generateColumnString --- columnJsonString ='+ columnJsonString);
		String result = '';
		List<DisplayColumn> tempDisplayColumn =  new List<DisplayColumn>();
		tempDisplayColumn = (List<DisplayColumn>)JSON.deserialize(columnJsonString, List<DisplayColumn>.class);
		system.debug('generateColumnString --- tempDisplayColumn ='+ tempDisplayColumn);
		
		for(DisplayColumn displaycol : tempDisplayColumn){
			system.debug('displaycol ='+ displaycol);
			String tempFieldName= displaycol.Field_api_name;
			DisplayColumn column = relationNameMap.get(tempFieldName);
			system.debug('generateColumnString-------column ='+ column);
			if(displaycol.fieldtype=='Schema.DisplayType.REFERENCE'){
				tempFieldName = column.referenceFieldName + '.Name';
			}
			system.debug('tempFieldName ='+ tempFieldName);
			result = result + ',' + tempFieldName;
		}  
		
		return result;
	}
	
	//Generate new display column with reference field name.Author by Kevin, changed by Jack
	public static List<DisplayColumn> generateNewDisplayColumnWithReferenceField(String columnJsonString, Map<String,DisplayColumn> relationNameMap){
		system.debug('generateColumnJSON --- columnJsonString ='+ columnJsonString);
		List<DisplayColumn> tempDisplayColumn =  new List<DisplayColumn>();
		List<DisplayColumn> newDisplayColumn  = new List<DisplayColumn>();
		tempDisplayColumn = (List<DisplayColumn>)JSON.deserialize(columnJsonString, List<DisplayColumn>.class);
		system.debug('generateColumnJSON --- tempDisplayColumn ='+ tempDisplayColumn);
		
		for(DisplayColumn displaycol : tempDisplayColumn){
			system.debug('displaycol ='+ displaycol);
			/*
			if(displaycol.fieldtype != 'REFERENCE'){
				newDisplayColumn.add(displaycol);
			}else{
				DisplayColumn column = new DisplayColumn();
				column = relationNameMap.get(displaycol.Field_api_name);
				newDisplayColumn.add(column);
			}
			*/
			newDisplayColumn.add(relationNameMap.get(displaycol.Field_api_name));
		}
		return newDisplayColumn;
	}
	
	public static String generateColumnJSON(String columnJsonString, Map<String,DisplayColumn> relationNameMap){
		List<DisplayColumn> newDisplayColumn  = new List<DisplayColumn>();
		newDisplayColumn = generateNewDisplayColumnWithReferenceField(columnJsonString, relationNameMap);
		system.debug('newDisplayColumn = ' + newDisplayColumn);
		String newJsonString = JSON.serialize(newDisplayColumn);
		return newJsonString;
	}


}