/**
* modified by andy for security review II
*/
public with sharing class AccountSelector extends CommonSelector{
	//final static String ACCOUNT_QUERY_BASE_1 = 'SELECT Id, Name, Phone, BillingCity, BillingCountry,'
	//										 + 'BillingPostalCode, BillingState, BillingStreet FROM Account';
	
	final static String ACCOUNT_FIELDS_BASE_1 = ' Id, Name, Phone, BillingCity, BillingCountry,'
											 + 'BillingPostalCode, BillingState, BillingStreet ';
	
	public AccountSelector(){
		super(Account.sObjectType);
	}
	public List<Account> fetchAccountListByKeyword(String keyword){
		List<Account> accList = new list<Account>();
		String likeArg = '%'+keyword+'%';
		
		//String query = ACCOUNT_QUERY_BASE_1+' WHERE Name LIKE : likeArg';
		//accList = Database.query(query); 
		accList = this.setParam('likeArg',likeArg).get(
								ACCOUNT_FIELDS_BASE_1,
								'Name LIKE : likeArg');
		return accList;
	}
	
	public List<Account> fetchAccountInfoById(String Id){
		List<Account> accList = new List<Account>();
		
		//String query = ACCOUNT_QUERY_BASE_1 + ' WHERE Id =: Id';
		//accList = Database.query(query);
		accList = this.setParam('Id',Id).get(
								ACCOUNT_FIELDS_BASE_1,
								'Id =: Id');
								
		return accList;
	}
}