/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAdvancedContactSearch {


    //static testMethod void myUnitTest() {
    //	 System.runAs(DummyRecordCreator.platformUser) {
    //    // TO DO: implement unit test
    //    Placement__c placement = new Placement__c(Name='ThisIsATest');
    //    String FolderId = UserInfo.getUserId();  
    //     Document doc1=new Document();
    //    doc1.Name='GS_005900000019PZJAA2_1381378548899';
    //    doc1.Body=Blob.valueOf('{"3":"0039000000Id544AAB:0039000000GLIlHAAX:0039000000RSe5SAAT","2":"0039000000P7HzeAAF:0039000000NQswcAAD:0039000000NPvfBAAT","1":"0039000000Id5s3AAB:0039000000Id5rtAAB:0039000000P7H3zAAF"}');
    //    doc1.FolderId=FolderId;
   
    //    insert doc1;
    //    StyleCategory__c style1=new StyleCategory__c();
    //    style1.name='Bulk_SEND_TEMP_1381378585340';
    //    style1.ascomponents__c='{"keywords":"email","objsingle":{},"objmulti":{},"objselectedskills":"[]","operatorMap":"{}"}';
    //    insert style1;
    //    Apexpages.currentPage().getParameters().put('cjid',doc1.id);
    //    Apexpages.currentPage().getParameters().put('scjid',style1.id);
        
    //    ApexPages.StandardController stdController = new ApexPages.StandardController(placement);
    //    AdvancedContactSearch ads=new AdvancedContactSearch();
    //    System.assertEquals(ads.googlesearchurl,'googleSearch2?pid='+ads.pid+'&scjid='+style1.id+'&cjid='+doc1.id);
    //	 }
        
    //}
}