/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PlacementCandEventOnInterviewTest { 
	static testMethod void runTest_Insert() {
		System.runAs(DummyRecordCreator.platformUser) {
		Placement_Candidate__c pc1 = initTest(); 	
		pc1.Date_and_Time_Internal__c = 	datetime.newInstance(2011, 12, 20, 23, 00, 0); 
		pc1.Internal_Interview_Details__c = 'Internal_Interview_Details test 1';
		insert pc1;		
		System.assertEquals(				datetime.newInstance(2011, 12, 20, 23, 00, 0), 	testInternalInterviewEvent(pc1).ACTIVITYDATETIME); 
		System.assertEquals(				'Internal_Interview_Details test 1', 			testInternalInterviewEvent(pc1).DESCRIPTION); 
		}
    }
    
	static Event testInternalInterviewEvent(Placement_Candidate__c pc1) {
		//------ InternalInterview				
		List<Event> events1 = [
			SELECT Id, ACTIVITYDATETIME, DESCRIPTION, SUBJECT, WHATID 
			FROM Event 
			WHERE WHATID = :pc1.id
			AND SUBJECT	 = 'Internal Interview with Candidate test Last Name 2 TestPlacementCandidateEventOnInterviewTest'];
		Event lastEvent = null;
		for (Event ev1 : events1) { 
			//System.debug('##################### PlacementCandEventOnInterviewTest.testInternalInterviewEvent.SUBJECT= ' + ev1.SUBJECT + ',WHATID = ' + ev1.WHATID);
			lastEvent = ev1;
		}		
		System.assertNotEquals(null, lastEvent);
		return lastEvent;
	}	  
	
    static testMethod void runPositiveTestCases() {
    	System.runAs(DummyRecordCreator.platformUser) {
		Placement_Candidate__c pc1 = initTest(); 
		//system.debug('pc1 ='+ pc1);	
		pc1.Date_and_Time_Internal__c  = 	datetime.newInstance(2011, 12, 20, 23, 00, 0); 
		pc1.Internal_Interview_Details__c = 'Internal_Interview_Details test 1';
		insert pc1;		
		System.assertEquals(				datetime.newInstance(2011, 12, 20, 23, 00, 0), 	testInternalInterviewEvent(pc1).ACTIVITYDATETIME);
		System.assertEquals(				'Internal_Interview_Details test 1', 			testInternalInterviewEvent(pc1).DESCRIPTION);
		
		pc1.Date_and_Time_Internal__c = 	datetime.newInstance(2011, 12, 21, 23, 00, 0); 
		pc1.Internal_Interview_Details__c = 'Internal_Interview_Details test 1B';
		TriggerHelper.PlacementCandidateEventOnInterviewTrigger = true;
		update pc1;	
		System.assertEquals(				datetime.newInstance(2011, 12, 21, 23, 00, 0), 	testInternalInterviewEvent(pc1).ACTIVITYDATETIME);	
		System.assertEquals(				'Internal_Interview_Details test 1B', 			testInternalInterviewEvent(pc1).DESCRIPTION);
    	}
    }       
    
    static Placement_Candidate__c initTest() {
		Account acc1 = new Account(Name='Client 1PlacementCandidateEventOnInterviewTest');
		insert acc1;		
		//----------		
		Contact con1 = new Contact(Lastname='test Last Name 2 TestPlacementCandidateEventOnInterviewTest');
		insert con1;		
		//----------		
		Placement__c pl1 = new Placement__c(Name = 'vacancy test 1PlacementCandidateEventOnInterviewTest');  
		insert pl1;		
		//**********		
		Placement_Candidate__c pc1 = new Placement_Candidate__c();
		pc1.Candidate__c = con1.id;
		pc1.Placement__c = pl1.id;
		
		//...
		/*List<User> users1 = [
			SELECT Id 
			FROM User
			WHERE IsActive = true];
		User firstUser = null;
		for (User us1 : users1) { 
			if(firstUser == null) {
				firstUser = us1;
			}
		}		
		System.assertNotEquals(null, firstUser);*/		
		//...
		pc1.Internal_Interview_Consultant__c = userinfo.getUserId(); //firstUser.id; //'00590000000iiR8';
		
		return pc1;		
	}
}