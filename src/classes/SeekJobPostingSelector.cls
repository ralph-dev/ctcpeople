public with sharing class SeekJobPostingSelector {
	
	/*
		@Author Jack 07 Oct 2014
		@Param
		@Return JobPostingSettings__c
	*/
	public static JobPostingSettings__c getJobPostingCustomerSetting() {
		JobPostingSettings__c returnResult = JobPostingSettings__c.getInstance(UserInfo.getUserId());
		return returnResult;
	}
	
	/*
		@Return String
		@Action return user prefer Email address
	*/
	public static String getUserPreferEmailAddress() {
		User u = new UserSelector().getCurrentUser();
		if (String.isEmpty(u.Job_Posting_Email__c)) {
			return u.Email;
		} else {
			return u.Job_Posting_Email__c;
		}
	}
}