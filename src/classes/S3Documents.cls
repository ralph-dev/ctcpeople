//get documents from S3; Author by Jack

public with sharing class S3Documents {

    public static S3AdminController S3Con;
    public static String orgName;

    static {
        if (S3Con == null) {
            try {
                S3Con = new S3AdminController();
                S3Con.initS3();
            } catch(system.exception e) {
                NotificationClass.notifyErr2Dev('S3Documents/getS3Doc', e);
                S3Con = null;
            }
        }
        orgName = UserInfo.getOrganizationName();
    }

	static public Blob getS3Doc(String bucket, String objkey){
        if (S3Con == null) {
            return null;
        }
        Blob blobdocument = null;
        try {
            S3Con.bucketToList = bucket;
            String url = Endpoint.getcpewsEndpoint() + '/ctcutil/Attachment';
            PageReference pageref = S3Con.getDirectory(objkey);
            String docurl = pageref.getUrl();
            String aurl = EncodingUtil.urlEncode(docurl, 'UTF-8');

            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url + '?aurl=' + aurl);

            //system.debug('request url:' + url + '?aurl=' + aurl);



            req.setMethod('GET');
            HttpResponse res = h.send(req);
            blobdocument = EncodingUtil.base64Decode(res.getBody());
        } catch (Exception e){
            NotificationClass.notifyErr2Dev('S3Documents/getS3Doc', e);
            return null;
        }
        return blobdocument;
    }

    static public Blob getlocalDoc(String localdocid){
    	Blob bloblocaldocument = null;
    	//Folder ctcFolder = DaoFolder.getFolderByDevName('Quick_Send_Resume_Demo');
    	Document result = null;
    	try{

    		CommonSelector.checkRead(Document.SObjectType,'Id,body,type');
            result = [select Id,body,type from Document where id = :localdocid and FolderId = : userinfo.getUserId()];
			bloblocaldocument = result.Body;
    	}
    	catch(system.exception e){
    		NotificationClass.notifyErr2Dev('S3Documents/getlocalDoc', e);
            bloblocaldocument = null;
    	}
    	return bloblocaldocument;
    }

    /**
     * Generate S3 links with specified expiration time for a web document
     * The link will used to open or download the document from AWS S3
     *
     * @param docs            Web_Document__c in DocRowRec type
     * @param expireTime      expiration time in seconds
     * @param docRelatedTo    either candidate or user
     * Add by Lina
     *
     **/
   /** public static String generateS3Link(DocRowRec drr, Integer expireTime, String docRelatedTo) {
        String link = '';
        if (S3Con != null) {
            try { 
                if (docRelatedTo == 'User') {
                    link = S3Con.createS3URL(drr.doc.S3_Folder__c, drr.doc.ObjectKey__c, expireTime);
                    link = '<a href="' + link + '">' + orgName + ' - ' + drr.doc.Document_Type__c + '</a><br>';
                } else {
                    link = S3Con.createS3URL(drr.doc.S3_Folder__c, drr.doc.ObjectKey__c, expireTime);
                    link = '<a href="' + link + '">' + drr.doc.Document_Type__c + ' - ' + drr.owner.contactRec.Name + '</a><br>';
                }
            } catch (Exception e) {
                NotificationClass.notifyErr2Dev('S3Documents/getlocalDoc', e);
            }
        }
        return link;
    }
    */
     public static String generateS3Link(DocRowRec drr, Integer expireTime, String docRelatedTo) {
        
        String name =generateLinkName(drr, docRelatedTo);
        
        return generateS3Link(name,drr,expireTime,docRelatedTo);
        
    }
    
     public static String generateS3LinkPrefix(String prefix, DocRowRec drr, Integer expireTime, String docRelatedTo) {
         
         
         String name =generateLinkName(drr, docRelatedTo);
         
         return generateS3Link(prefix + ' ' + name,drr,expireTime,docRelatedTo);
         
         
    }
    
    
    private static String generateLinkName(DocRowRec drr,  String docRelatedTo){
        String name ='';
        if (docRelatedTo == 'User') {
            name = orgName + ' - ' + drr.doc.Document_Type__c;
            
        } else {
            name = drr.doc.Document_Type__c + ' - ' + drr.owner.contactRec.Name;
            
        }
        return name;
    }
   
    
    public static String generateS3Link(String name,DocRowRec drr, Integer expireTime, String docRelatedTo){
        String link ='';
        if (S3Con != null) {
            link = S3Con.createS3URL(drr.doc.S3_Folder__c, drr.doc.ObjectKey__c, expireTime);
            if (docRelatedTo == 'User') {
                        
                link = '<a href="' + link + '">' + name + '</a><br>';
            } else {
                
                link = '<a href="' + link + '">' + name + '</a><br>';
            }
        }
        
        
        return link;
    }
   	
    static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){//Send error message
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());    
    }  
}