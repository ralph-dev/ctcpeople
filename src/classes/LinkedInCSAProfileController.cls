/**
 * This controller class is to get the LinkedIn CSA API Key
 * pass to visual force page for LinkedIn account matching
 * then save the LinkedIn member Id returned by Linkedin in the system
 * Created by: Lina
 */

global with sharing class LinkedInCSAProfileController {
    private String parentId { get; set; }
    public Contact con { get; set; }
    public LinkedIn_CSA__c csa { get; set; }
    public String contact_url { get; set; } 
    public PageReference prf= null;
    
    /**
	 * Constructor 
	 */ 
	public LinkedInCSAProfileController(ApexPages.StandardController stdController) {
	    this.parentId = stdController.getId();
	    // Get LinkedIn CSA
	    csa = LinkedInCSASelector.getLinkedInCSA();
	    ContactSelector conSelector = new ContactSelector();
        con = conSelector.getContactForLinkedIn(parentId);
        contact_url = System.URL.getSalesforceBaseURL().toExternalForm() + '/' + parentId;
    }
    
    // save LinkedIn member Id for the contact
    @RemoteAction
    global static void saveLinkedInId(String linkedInId, String conId){
        Contact candidate = new Contact();
        ContactSelector conSelector = new ContactSelector();
        candidate = conSelector.getContactForLinkedIn(conId);
        candidate.LinkedIn_Member_Id__c = linkedInId;
        fflib_SecurityUtils.checkFieldIsUpdateable(Contact.SObjectType, Contact.LinkedIn_Member_Id__c);
        update candidate;
    } 
    
}