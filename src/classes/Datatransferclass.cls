public with sharing class Datatransferclass {
	private String labelname;
	private String apiname;
	public Datatransferclass(String label,String apiname){
		setlabelname(label);
		setapiname(apiname);
	}
	
	public void setlabelname(String label){
		this.labelname=label;
	}
	public void setapiname(String apiname){
		this.apiname=apiname;
	}
	public String getlabelname(){
		return this.labelname;
	}
	public String getapiname(){
		return this.apiname;
	}
	
	public  static  testmethod void testDatatransferclass(){
		System.runAs(DummyRecordCreator.platformUser) {
		Datatransferclass datatransunit=new Datatransferclass('Label','apiname');
		datatransunit.setlabelname('Label');
		datatransunit.setapiname('Second Name');
		System.assertEquals(datatransunit.getlabelname(),'Label');
		System.assertEquals(datatransunit.getapiname(),'Second Name');
		}
	}

}