public with sharing class PlacementSelector extends CommonSelector{
	
	private String queryFields = 'id, candidate__c';
	private String queryStr='select '+queryFields+' from Placement_Candidate__c ';
	
	public PlacementSelector(){
		super(Placement_Candidate__c.sObjectType);
	}
	
	
	public list<Placement_Candidate__c> getPlacedCMFromCandidates(list<String> candIds){
		list<Placement_Candidate__c> placements=new list<Placement_Candidate__c>();
		String queryString=queryStr+' where candidate__c in : candIds and Employee_User_Name__c  !=null ';
        //System.debug('THe query is '+queryString);
        checkRead(queryFields);
		placements=database.query(queryString);
		return placements;
	}
    
    public list<Placement_Candidate__c> getPlacedCMFromAccounts(list<String> accountIds){
		list<Placement_Candidate__c> placements=new list<Placement_Candidate__c>();
		String queryString=queryStr+' where Placement__r.Company__c in : accountIds and Employee_User_Name__c  !=null ';
        //System.debug('THe query is '+queryString);
        checkRead(queryFields);
		placements=database.query(queryString);
        return placements;
	}
    public list<Placement_Candidate__c> getPlacedCMFromBillingContact(list<String> contactIds){
		list<Placement_Candidate__c> placements=new list<Placement_Candidate__c>();
		String queryString=queryStr+' where Placement__r.Biller_Contact__c in : contactIds  and Employee_User_Name__c  !=null ';
        //System.debug('THe query is '+queryString);
        checkRead(queryFields);
		placements=database.query(queryString);
        return placements;
	}
 	public list<Placement_Candidate__c> getPlacedCMFromApprover(list<String> contactIds){
		list<Placement_Candidate__c> placements=new list<Placement_Candidate__c>();
		String queryString=queryStr+' where (Approver__C in : contactIds   or Second_Approver__c  in: contactIds) and Employee_User_Name__c  !=null ';
            
        //System.debug('THe query is '+queryString);
        checkRead(queryFields);
		placements=database.query(queryString);
        return placements;
	}
	public list<Placement_Candidate__c> getPlacedCMFromWorkplaces(list<String> workplaceIds){
		list<Placement_Candidate__c> placements=new list<Placement_Candidate__c>();
		String queryString=queryStr+' where Work_place__c in : workplaceIds and Employee_User_Name__c  !=null ';
        //System.debug('THe query is '+queryString);
        checkRead(queryFields);
		placements=database.query(queryString);
        return placements;
	}
}