/**
* This is a DTO class for user documents
* 
* Created by: Lorena
* Created on: 15/07/2016
* 
**/
public with sharing class UsrRowRec {
    
    public User usrRec{get;set;}
    public boolean isSelected{get;set;}
    public List<DocRowRec> docList{get;set;}
    
    public UsrRowRec(User u) {
        this(u, true);
    }
    
    public UsrRowRec(User u, boolean containDocs) {
        usrRec=u;
        docList = new List<DocRowRec>();
        isSelected=true;
        if(containDocs){
            // all docs (resume/cover letter) are initially not included in email
           for(Web_Document__c docTmp: usrRec.Resume_and_Files__r){
                docList.add(new DocRowRec(docTmp,this));
            }
        }
        
    }
    
    
    
    

}