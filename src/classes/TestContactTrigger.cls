/**

 * 
 * Author: Raymond
 * 
 * This test case is for all contact triggers
 * 
 * ATTENTION: Before running this test please make suer you have set up and enable UpdateAccountField in Admin tab
 *
 */
@isTest
private class TestContactTrigger {
	// Variables for testUpdateAccountFieldTriggerUnderBulkIndependantCandInsertion
	private static final Integer NUM_CANDIDATE_CREATE=1300;
	private static final Integer NUM_CANDIDATE_IN_A_BATCH = 200;
	private static final String triggerName = 'UpdateAccountField';

	/**
        testUpdateAccountFieldTriggerUnderBulkIndependantCandInsertiontesting is for testing 
        UpdateAccountField trigger which is used to link candidates to candidate pool
        if no account or a candidate pool account has been given to these candidates.
        
        'Default Name Of Candidate Pool' in PCAppSwitch should be set to empty
    */
    static testMethod void testUpdateAccountFieldTriggerUnderBulkIndependantCandInsertion() {
    	System.runAs(DummyRecordCreator.platformUser) {
        
        TriggerDetail triggerdetail = new TriggerDetail();
        triggerdetail.getTriggerDetails();
        system.debug('Has UpdateAccountField trigger enabled? ' + triggerdetail.IsActive(triggerName));
        // UpdateAccountField trigger should be always active.
        system.assert(triggerdetail.IsActive(triggerName));
        
        // Prepare insertion batches;
        RecordType candRT = DaoRecordType.indCandidateRT;       
        List<List<Contact>> insertBatches = new List<List<Contact>>();
        List<Contact> candList = null;
        for(Integer i=0;i<NUM_CANDIDATE_CREATE;i++){
            if(Math.mod(i,NUM_CANDIDATE_IN_A_BATCH)==0){
                    candList = new List<Contact>();
                    insertBatches.add(candList);
            }
            Contact cand = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand'+i+'@testcanddispatcher.ctc.test',Candidate_From_Web__c=true);
            candList.add(cand);
        }
        system.debug('No. of insert batch:'+insertBatches.size());
        
        // We need to create this class so that later we can use it to determine what will be the limit
        // on number of contact a candidate pool can contain when it's in test environment
        CandidateDispatcher cd = new CandidateDispatcher((Id)triggerdetail.getRecordTypeId(triggerName));
        
        // Retrieve all old candidate pools
        List<Account> candPools = DaoAccount.getAccsByRecordTypeIdInDescCreatedDate(DaoRecordType.candidatePoolRT.Id);
                
        system.debug('Print out recent cand pools size:'+candPools.size());
        /*
        for(Account cp:candPools){
            system.debug('cand pool:'+cp);
            //List<Contact> candstest = [select id from Contact where AccountId=:cp.Id];
            //system.debug('size:'+candstest.size());
            // uncomment this line may cause "System.TypeException:Aggregate query has too many rows for direct assignment, use FOR loop"
            system.debug('cand pool size:'+cp.Contacts.size());
        }*/
        
        Integer noOfCandPoolBeforeInsert = candPools.size();
        system.debug('No. of candidate pool before insert:'+noOfCandPoolBeforeInsert);
        Integer expectedNoOfNewCandPool = 0;
        // Calculate the number of new candidate pools that will be created during this insertion
        expectedNoOfNewCandPool = (Integer)Math.ceil((Double)NUM_CANDIDATE_CREATE/NUM_CANDIDATE_IN_A_BATCH);
        system.debug('Expected no of new candidate pools before adjustment:'+expectedNoOfNewCandPool);
        if(noOfCandPoolBeforeInsert != 0){
              Account newestCandPool = candPools.get(0);
              Integer noOfExistingContact = DaoContact.countContactForComp(newestCandPool.Id);
              system.debug('No. of candidate in newest candidate pool before insert:'+noOfExistingContact);
              
              // if the newest candidate pool contains contacts that less than limit, then
              // the first batch of contacts will be inserted under this candidate pool.
              // therefore, number of candidate pools that needed to be created will be one less than
              // we expected.
              if(noOfExistingContact < cd.getContactLimit())
                expectedNoOfNewCandPool-=1;
        }
        system.debug('Expected no of new candidate pools after adjustment:'+expectedNoOfNewCandPool);
        
        Test.startTest();
        Integer batchNo=1;
        for(List<Contact> insertBatch:insertBatches){
            system.debug('Start Batch No.'+batchNo);
            insert insertBatch;
            system.debug('Finish Batch No.'+batchNo);
            batchNo++;
        }       
        system.debug('Print out the size of old cand pools after insertion');
        for(Account cp:candPools){
            system.debug('after insert, cand pool:'+cp);
            Integer noOfCand =  DaoContact.countContactForComp(cp.Id);
            system.debug('after insert, cand pool size:'+noOfCand);
        }
        if(noOfCandPoolBeforeInsert != 0){
            Account newestCandPool = candPools.get(0);
            Integer noOfExistingContact = DaoContact.countContactForComp(newestCandPool.Id);
            system.debug('No. of candidate in newest candidate pool after insert:'+noOfExistingContact);
        }
        
        Integer noOfCandPoolAfterInsert = DaoAccount.getAccsByRecordTypeIdInDescCreatedDate(DaoRecordType.candidatePoolRT.Id).size();
        Integer actualNoOfNewCandPool = noOfCandPoolAfterInsert - noOfCandPoolBeforeInsert;
        system.debug('No. of candidate pool after insert:'+noOfCandPoolAfterInsert);
        system.debug('No. of candidate pool before insert:'+noOfCandPoolBeforeInsert);
        system.debug('No. of expected candidate pool careted:'+expectedNoOfNewCandPool);
        //system.assertEquals(actualNoOfNewCandPool,expectedNoOfNewCandPool);
        Test.stopTest();
        }
    }
    
    /**
        Test scenario that when excluded id isn't provided, duplicate records that are created
        from SF will be blocked.
    
    **/
    @isTest
    static void testPeopleDeduplicationBlockDuplicateContact(){
        
        PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
        pcSwitch.DeduplicationOnVCW_Active__c=false;
        pcSwitch.Deduplicate_PC_Active__c = false;
        pcSwitch.PeopleDeduplication_Active__c = false;     
        upsert pcSwitch;
        
        prepareData();
        
        pcSwitch.DeduplicationOnVCW_Active__c=true;
        pcSwitch.Deduplicate_PC_Active__c = true;
        pcSwitch.PeopleDeduplication_Active__c = true;
        upsert pcSwitch;
        
        RecordType candRT=DaoRecordType.indCandidateRT; 
        RecordType conCandRT = DaoRecordType.corpCandidateRT;
        
        // cand 1 should be blocked
        Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',email='Cand1@test.ctc.test',jobApplication_Id__c=123456789,Resume_Source__c='abc1');
        // cand 2 should be inserted successfully
        Contact cand2 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 123',email='Cand2@test.ctc.test',jobApplication_Id__c=123456789,Resume_Source__c='abc2');

        try{
            insert cand2;
        }catch(Exception e){
            // cand2 should be inserted without any exception
            system.debug('Exception Raised:'+e);
            system.assert(false);
        }
        
        try{
            insert cand1;
        }catch(Exception e){
            system.debug('Exception Raised:'+e);
            return;
        }
        // cand1 should be inserted with a exception, therefore program shouldn't reach here
        system.assert(false);
        
        
    }
    
    
     /**
        Test scenario that when excluded id is provided, duplicate records that are created
        from SF will be blocked.
    
    **/
    @isTest
    static void testPeopleDeduplicationCreateDuplicateContact(){
        
        PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
        pcSwitch.DeduplicationOnVCW_Active__c=false;
        pcSwitch.Deduplicate_PC_Active__c = false;
        pcSwitch.PeopleDeduplication_Active__c = false;     
        upsert pcSwitch;
        
        prepareData();
        
        // Prepare excluded Ids.
        List<Id> excludedContactRTIds = new List<Id>();
        excludedContactRTIds.add(DaoRecordType.corpCandidateRT.Id);
        excludedContactRTIds.add(DaoRecordType.corpContactRT.Id);
        String excludedIdString = '';
        for(Integer j=0;j<excludedContactRTIds.size();j++){
            if(j==0)
                excludedIdString = excludedContactRTIds.get(j);
            else
                excludedIdString += ';'+excludedContactRTIds.get(j);
        }
        
        pcSwitch.DeduplicationOnVCW_Active__c=true;
        pcSwitch.Deduplicate_PC_Active__c = true;
        pcSwitch.PeopleDeduplication_Active__c = true;
        pcSwitch.Exclude_From_Dedup__c=excludedIdString;
        upsert pcSwitch;
        
        RecordType candRT=DaoRecordType.indCandidateRT; 
        RecordType conCandRT = DaoRecordType.corpCandidateRT;
        
        // cand 1 should be inserted successfully
        Contact cand1 = new Contact(RecordTypeId=conCandRT.Id,LastName='candidate 1',email='Cand1@test.ctc.test',jobApplication_Id__c=123456789,Resume_Source__c='abc1');
        // cand 2 should be inserted successfully
        Contact cand2 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 123',email='Cand2@test.ctc.test',jobApplication_Id__c=123456789,Resume_Source__c='abc2');

        try{
            insert cand2;
        }catch(Exception e){
            // cand2 should be inserted without any exception
            system.debug('Exception Raised:'+e);
            system.assert(false);
        }
        
        try{
            insert cand1;
        }catch(Exception e){
            // cand1 should be inserted as well since it's of record type that is excluded from dedup
            system.debug('Exception Raised:'+e);
            system.assert(false);
        }
        
    }
    
 
      /**
        Test scenario that when excluded id is provided, duplicate records that are created
        from SF will be blocked. In this scenario, excluded record type id reside in existing contact record
        rather than in incoming new contact record
    
    **/
    @isTest
    static void testPeopleDeduplicationCreateDuplicateContactScenario2(){
        
        PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
        pcSwitch.DeduplicationOnVCW_Active__c=false;
        pcSwitch.Deduplicate_PC_Active__c = false;
        pcSwitch.PeopleDeduplication_Active__c = false;     
        upsert pcSwitch;
        
        prepareData();
        
        // Prepare excluded Ids.
        List<Id> excludedContactRTIds = new List<Id>();
        excludedContactRTIds.add(DaoRecordType.corpCandidateRT.Id);
        excludedContactRTIds.add(DaoRecordType.corpContactRT.Id);
        String excludedIdString = '';
        for(Integer j=0;j<excludedContactRTIds.size();j++){
            if(j==0)
                excludedIdString = excludedContactRTIds.get(j);
            else
                excludedIdString += ';'+excludedContactRTIds.get(j);
        }
        
        pcSwitch.DeduplicationOnVCW_Active__c=true;
        pcSwitch.Deduplicate_PC_Active__c = true;
        pcSwitch.PeopleDeduplication_Active__c = true;
        pcSwitch.Exclude_From_Dedup__c=excludedIdString;
        upsert pcSwitch;
        
        RecordType candRT=DaoRecordType.indCandidateRT; 
        RecordType conCandRT = DaoRecordType.corpCandidateRT;
        
        // cand 1 should be inserted successfully
        Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',email='Cand1@test.ctc.test',jobApplication_Id__c=123456789,Resume_Source__c='abc1');
        // cand 2 should be inserted successfully
        Contact cand2 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 123',email='Cand2@test.ctc.test',jobApplication_Id__c=123456789,Resume_Source__c='abc2');

        // cand2Update is a duplicate record of cand1, we update it so it has excluded record type id.
        Contact cand2Update = [select id,RecordTypeId from Contact where email=:cand1.email and LastName=:cand1.LastName];
        cand2Update.RecordTypeId = conCandRT.Id;
        update cand2Update;

        try{
            insert cand2;
        }catch(Exception e){
            // cand2 should be inserted without any exception
            system.debug('Exception Raised:'+e);
            system.assert(false);
        }
        
        try{
            insert cand1;
        }catch(Exception e){
            // cand1 should be inserted as well since its duplicate counterpart is excluded from dedup
            system.debug('Exception Raised:'+e);
            system.assert(false);
        }
        
    }

    @isTest
    static void testUpdateAccountFieldWitSettingFileDeleted(){
    	List<Document> settings = [select Id,Name,DeveloperName, Body 
							from Document where Type ='xml' and DeveloperName like '%Trigger_Admin%'];
		if(settings != null && settings.size()>0){
			delete settings;
		}else{
			system.debug('There is no file with developername called "Trigger_Admin" in the system');
		}
		RecordType candRT=DaoRecordType.indCandidateRT;
    	Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',email='Cand1@test.ctc.test',jobApplication_Id__c=123456789,Resume_Source__c='abc1');
    	try{
    		insert cand1;
    	}catch(Exception e){
    		// If insertion fail, it could be som
    		system.assert(false);
    		system.debug('Error');
    	}
    }
    
    static private List<Account> compList;
	static private Placement__c p1; 
	static private Placement__c p2;
	static private Advertisement__c  ad1;
	static private Advertisement__c  ad2;
	static private List<Contact> conInsertList;
	static private List<Placement_Candidate__c> cmList;
    	
	static void prepareData(){
		//insert companies
        compList = new List<Account>();
        compList.add(new Account(Name='comp1'));
        compList.add(new Account(Name='comp2'));
        insert compList;
        
        //insert vacancies
        RecordType rt = [select Id from RecordType where SobjectType =: PeopleCloudHelper.getPackageNamespace()+'Placement__c' limit 1];
        p1=new Placement__c(RecordTypeId= rt.Id,Company__c=compList.get(0).Id,Name='placement 1');
        p2=new Placement__c(RecordTypeId= rt.Id,Company__c=compList.get(1).Id,Name='placement 2');
        insert p1;
        insert p2;
        
        // Insert Advertisements
        ad1 = new Advertisement__c(Job_Title__c='ad 1',RecordTypeId=DaoRecordType.seekAdRT.Id,Vacancy__c=p1.Id,WebSite__c='Seek');
        ad2 = new Advertisement__c(Job_Title__c='ad 2',RecordTypeId=DaoRecordType.seekAdRT.Id,Vacancy__c=p2.Id,WebSite__c='Seek');
        insert ad1;
        insert ad2;
		
		// Create candidates
		RecordType candRT=DaoRecordType.indCandidateRT;		
		conInsertList = new List<Contact>(); 
		for(Integer i=0;i<201;i++){
			Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',email='Cand'+i+'@test.ctc.test', Candidate_From_Web__c=true,jobApplication_Id__c=123456789,Resume_Source__c='abc'+i);
			conInsertList.add(cand1);
		}
		insert conInsertList;
		
		RecordType cmRT = [select Id from RecordType where SobjectType =: PeopleCloudHelper.getPackageNamespace()+'Placement_Candidate__c' limit 1];
		cmList = new List<Placement_Candidate__c>();
        for(Contact con:conInsertList){
        	Placement_Candidate__c cm1=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=con.Id,Online_Ad__c=ad1.Id);
        	cmList.add(cm1);
        }
        // Prepare Candidate Management data, no deduplication happen
        insert cmList;
	}
}