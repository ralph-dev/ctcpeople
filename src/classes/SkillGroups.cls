public with sharing class SkillGroups {

    //selected skill group attribute
    public class SelectedSkillGroup {
      
        public String skillGroupId;
        public String skillGroupName;
        public Boolean isSelected;
        
        public SelectedSkillGroup(String skillGroupId, String skillGroupName, Boolean isSelected) {
            this.skillGroupId=skillGroupId;
            this.skillGroupName=skillGroupName;
            this.isSelected=isSelected;
        }
    }
    
	public static List<Skill_Group__c> getSkillGroups(){
		List<Skill_Group__c> tempskillgrouplist = new List<Skill_Group__c>();
        CommonSelector.checkRead(Skill_Group__c.SObjectType,'Name,Id, Enabled__c, Default__c, Skill_Group_External_Id__c');
		tempskillgrouplist = [select Name,Id, Enabled__c, Default__c, Skill_Group_External_Id__c from Skill_Group__c 
        					 where Enabled__c=true];
		return tempskillgrouplist;
	}
	
	public static boolean isSkillParsingEnabled(){
        PCAppSwitch__c cs = PCAppSwitch__c.getInstance();
        return cs.Enable_Daxtra_Skill_Parsing__c; 
    }
    
    public static List<Skill_Group__c> getDefaultSkillGroups(){
		List<Skill_Group__c> defaultskillgrouplist = new List<Skill_Group__c>();
        CommonSelector.checkRead(Skill_Group__c.SObjectType,'Name,Id, Enabled__c, Default__c, Skill_Group_External_Id__c');
		defaultskillgrouplist = [select Name,Id, Enabled__c, Default__c, Skill_Group_External_Id__c from Skill_Group__c 
        					 where Enabled__c=true and Default__c=true ];
		return defaultskillgrouplist;
	}
	
	public static List<Skill_Group__c> getSkillGroupsByExtIds(Set<String> extIds){
		List<Skill_Group__c> tempskillgrouplist = new List<Skill_Group__c>();
        CommonSelector.checkRead(Skill_Group__c.SObjectType,'Name,Id, Enabled__c, Default__c, Skill_Group_External_Id__c');
		tempskillgrouplist = [select Name,Id, Enabled__c, Default__c, Skill_Group_External_Id__c from Skill_Group__c 
        					 where Skill_Group_External_Id__c In: extIds ];
		return tempskillgrouplist;
	} 
    
    public static List<SkillGroups.SelectedSkillGroup> getSelectedSkillGroup() {
        List<SkillGroups.SelectedSkillGroup> selectedSkillGroups = new List<SkillGroups.SelectedSkillGroup>();
        List<Skill_Group__c> tempskillgrouplist = getSkillGroups();
        if(tempskillgrouplist!=null && tempskillgrouplist.size()>0 ) {
            for(Skill_Group__c tempskillgroup : tempskillgrouplist){
                SkillGroups.SelectedSkillGroup ssg = new SkillGroups.SelectedSkillGroup(tempskillgroup.Skill_Group_External_Id__c, tempskillgroup.Name, tempskillgroup.Default__c);
                selectedSkillGroups.add(ssg);
            }
        }
        return selectedSkillGroups;
    }
}