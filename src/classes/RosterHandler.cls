public with sharing class RosterHandler implements ITrigger {
    final static String ROSTER_STATUS_FILLED = 'Filled';
    static List<String> shiftIdList = new List<String>();
    static List<Days_Unavailable__c> daysUnavailable = new List<Days_Unavailable__c>();  //add the roster record to list and update candidates
    static String rosterRT;
    
     // Constructor
    public RosterHandler() {}
    public void bulkBefore() {}  

    public void bulkAfter() {
        RecordTypeServices rtServices = new RecordTypeServices();
        Map<String, String> rtMap = rtServices.getRecordTypeMap();
        rosterRT = rtMap.get('Roster');
    }

    public void beforeInsert(SObject so) {}

    public void beforeUpdate(SObject oldSo, SObject so) {}
    
    public void beforeDelete(SObject so) {}

    public void afterInsert(SObject so) {
        Days_Unavailable__c roster = (Days_Unavailable__c)so;
        if(roster.Event_Status__c == ROSTER_STATUS_FILLED && roster.recordTypeId == rosterRT){
            shiftIdList.add(roster.Shift__c);
        }
        daysUnavailable.add(roster);
    }

    public void afterUpdate(SObject oldSo, SObject so) {
        Days_Unavailable__c rosterNew = (Days_Unavailable__c)so;
        Days_Unavailable__c rosterOld = (Days_Unavailable__c)oldSo;
        //System.debug('rosterNew: '+rosterNew);
        if( rosterOld.Event_Status__c != rosterNew.Event_Status__c 
            && (rosterNew.Event_Status__c == ROSTER_STATUS_FILLED || rosterOld.Event_Status__c == ROSTER_STATUS_FILLED)
            && rosterNew.recordTypeId == rosterRT){
            shiftIdList.add(rosterNew.Shift__c);
        }

        daysUnavailable.add(rosterNew);   
    }

    public void afterDelete(SObject so) {
        Days_Unavailable__c roster = (Days_Unavailable__c)so;
        if(roster.Event_Status__c == ROSTER_STATUS_FILLED && roster.recordTypeId == rosterRT){
            shiftIdList.add(roster.Shift__c);
        }

        daysUnavailable.add(roster);   
    }
    
    public void afterUndelete(SObject so){
        Days_Unavailable__c roster = (Days_Unavailable__c)so;
        if(roster.Event_Status__c == ROSTER_STATUS_FILLED && roster.recordTypeId == rosterRT){
            shiftIdList.add(roster.Shift__c);
        }
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public void andFinally() {
        String pid = UserInfo.getProfileId();
        PCAppSwitch__c  PCAppSwitch = PCAppSwitch__c.getinstance(pid);
        
        if(test.isRunningTest() || trigger.isAfter){   
            if(PCAppSwitch.Update_Placed_Positions_Under_Shift__c){         
                ShiftSelector sSelector = new ShiftSelector();
                //System.debug('shiftIdList: '+shiftIdList);
                
                List<Shift__c> shiftList = sSelector.fetchShiftListByShiftIdList(shiftIdList);
                
               // System.debug('shiftIdList: '+shiftList);
                
                if(shiftList != null && shiftList.size() != 0 && !RecursiveTriggerHelper.isAlreadyModified()){
                    RecursiveTriggerHelper.setAlreadyModified();
                    ShiftDomain sDomain = new ShiftDomain(shiftList);
                    sDomain.updateShiftPlacedPositionsNumber();
                }
            }

            if(!PCAppSwitch.Disable_Update_Contact_at_Avl_Record_CUD__c) {
                if(Trigger.isDelete){
                    ContactUpdater.updateCandLastModifyDateByAvailability(daysUnavailable);
                }else{
                    ContactUpdater.updateCandLastModifyDateByAvailability(daysUnavailable);
                }
            }
        }
    }
}