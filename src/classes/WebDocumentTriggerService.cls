/**
 * This is a service class for WebDocumentTriggerHandler
 * 
 * Created by: Lina
 * Created on: 31/05/2016
 */

public with sharing class WebDocumentTriggerService {
   
    /**
     * Given a list of web document, if some of the documents have same Document_Related_To__c Id, 
     * only add the one with latest LastModifiedDate in return Set
     * For add/delete trigger
     */
    private static String orgId = UserInfo.getOrganizationId().subString(0,15);
     
    public static Set<Id> getResumeDedupe(List<Web_Document__c> docList) {
        Set<Id> singleResumeSet = new Set<Id>();
        Map<Id, Id> candSingleResume = new Map<Id, Id>();  

        // Sort the list of web documents in ASC order based on Last Modified Date
        List<WebDocumentWrapper> docWrapperList = new List<WebDocumentWrapper>();
        for (Web_Document__c doc : docList) {
            docWrapperList.add(new WebDocumentWrapper(doc));
        }
        docWrapperList.sort();
        for (WebDocumentWrapper docWrapper : docWrapperList) {
            if (docWrapper.doc.Document_Related_To__c != null) {
                candSingleResume.put(docWrapper.doc.Document_Related_To__c, docWrapper.doc.Id);
            }
        }
        
        List<Id> singleResumeList = candSingleResume.values();
        singleResumeSet.addAll(singleResumeList);
        return singleResumeSet;
    }  
    

    /**
     * Given a list of web document, if some of the documents have same Document_Related_To__c Id, 
     * only add the one with latest LastModifiedDate in return Set
     * For update trigger
     */
    public static Set<Id> getResumeDedupe(List<Web_Document__c> triggerNew, List<Web_Document__c> triggerOld) {
        Set<Id> singleResumeSet = new Set<Id>();
        Map<Id, Id> candSingleResume = new Map<Id, Id>();
        
        // Sort the list of web documents in ASC order based on Last Modified Date
        List<WebDocumentWrapper> docWrapperList = new List<WebDocumentWrapper>();
        for (Web_Document__c doc : triggerOld) {
            docWrapperList.add(new WebDocumentWrapper(doc));
        }
        docWrapperList.sort();
        
        // Get document Id and new Document_Related_To__c Id mapping
        Map<Id, Id> docIdCandId = new Map<Id, Id>();
        for (Web_Document__c doc : triggerNew) {
            docIdCandId.put(doc.Id, doc.Document_Related_To__c);
        }
        for (WebDocumentWrapper docWrapper : docWrapperList) {
            if (docIdCandId.get(docWrapper.doc.Id) != null) {
                candSingleResume.put(docIdCandId.get(docWrapper.doc.Id), docWrapper.doc.Id);    
            }
        }
        
        List<Id> singleResumeList = candSingleResume.values();
        singleResumeSet.addAll(singleResumeList);
        return singleResumeSet;
    }


    public static void sendReparseRequest(List<Web_Document__c> webDocList) {
  
        List<String> requestBodyList = new List<String>();
        String url = EndPoint.getcpewsEndpoint()+'/ctcenhanceduploadfiles/Service/reparse-resume';
        for(Web_Document__c webDoc : webDocList) {
            String requestBody = createBody(webDoc);
            requestBodyList.add(requestBody);
        }
        sendRequest(requestBodyList, url);
    }

    public static String createBody(Web_Document__c doc){
        String namespace = PeopleCloudHelper.getPackageNamespace();
        boolean enabledSkillParsing = SkillGroups.isSkillParsingEnabled();
        String baseURL = '?orgId=' + encode(orgId)+ '&candidateId=' +encode(doc.Document_Related_To__c)+ '&resumeName=' +encode(doc.Name)+ '&resumeKey=' +encode(doc.ObjectKey__c)+'&namespace='+encode(namespace)+'&enabledSkillParsing='+enabledSkillParsing;
        if(doc.S3_Folder__c != null ){
            baseURL = baseURL+'&bucketName='+encode(doc.S3_Folder__c); 
        }
        return baseURL;
    }

    @future(callout = true)
    public static void sendRequest(List<String> params, String url){
        for(String param: params){
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url + param);
            req.setMethod('GET');
            req.setTimeout(120000);
            req.setHeader('Content-Type', 'UTF-8');
            try{
                if(!Test.isRunningTest()){
                    //System.debug('HttpRequest:'+req);
                    HttpResponse res = h.send(req);
                }
            }catch(System.CalloutException e){
                System.debug('error message: '+e);
            }

        }       
    }

    private static String encode(String value){
        return EncodingUtil.urlEncode(value, 'UTF-8');
    }
    
    public static void cleanRecords(List<Id> conIds, Map<Id, Boolean> delResumeField){
        EducationHistorySelector edSelector = new EducationHistorySelector();
        List<Education_History__c> eduList = edSelector.getContactEducationHistory(conIds);
        fflib_SecurityUtils.checkObjectIsDeletable(Education_History__c.SObjectType);
        delete eduList;

        EmploymentHistorySelector emSelector = new EmploymentHistorySelector();
        List<Employment_History__c> empList = emSelector.getContactEmploymentHistory(conIds);
        fflib_SecurityUtils.checkObjectIsDeletable(Employment_History__c.SObjectType);
        delete empList;

        List<Id> conIdDelResume = new List<Id>();
        for(Id conId: conIds){
            if(delResumeField.get(conId) != null && delResumeField.get(conId)){
                conIdDelResume.add(conId);
            }
        }
        if(conIdDelResume != null || !conIdDelResume.isEmpty()){
            ContactSelector conSel = new ContactSelector();
            List<Contact> conList = conSel.getContacts(conIdDelResume);
            for(Contact con:conList){
                con.Resume__c = null;
                con.Resume_Source__c = null;
            }
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                Contact.Resume__c,
                Contact.Resume_Source__c
            };
            fflib_SecurityUtils.checkUpdate(Contact.SObjectType, fieldList);
            update conList;
        }
    } 
        
}