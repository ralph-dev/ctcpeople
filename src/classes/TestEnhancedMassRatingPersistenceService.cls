/**
 * Test persistence service for saving workbench status of enhanced mass rating
 * (aka screen candidate) function
 */
@isTest
private class TestEnhancedMassRatingPersistenceService {
	
    
    static testMethod void testPersistCurrentWorkbenchStatus() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	Map<String,Object> testData = prepareTestData();
    	EnhancedMassRatingPersistenceServie persistenceService = new EnhancedMassRatingPersistenceServie();
    	Id statusId = persistenceService.persistCurrentWorkbenchStatus('test',testData);
    	system.assert(statusId != null);
    	}
    	
    }
    
    static testMethod void testRetrieveWorkbenchStatus(){
    	System.runAs(DummyRecordCreator.platformUser) {
     	Map<String,Object> testData = prepareTestData();
    	EnhancedMassRatingPersistenceServie persistenceService = new EnhancedMassRatingPersistenceServie();
    	Id statusId = persistenceService.persistCurrentWorkbenchStatus('test',testData);
    	Map<String,Object> savedStatus = persistenceService.retrieveWorkbenchStatus(statusId);
    	
    	system.assertEquals((String)testData.get(EnhancedMassRatingPersistenceServie.NAME), (String)savedStatus.get(EnhancedMassRatingPersistenceServie.NAME));
    	system.assertEquals((String)testData.get(EnhancedMassRatingPersistenceServie.SEARCH_KEYWORD), (String)savedStatus.get(EnhancedMassRatingPersistenceServie.SEARCH_KEYWORD));
    	system.assertEquals((Integer)testData.get(EnhancedMassRatingPersistenceServie.CURRENT_PAGE), (Integer)savedStatus.get(EnhancedMassRatingPersistenceServie.CURRENT_PAGE));
    	system.assertEquals((String)testData.get(EnhancedMassRatingPersistenceServie.VISIBLE_TAB_ID), (String)savedStatus.get(EnhancedMassRatingPersistenceServie.VISIBLE_TAB_ID));
    	String cmIdsFromTestData = (String)testData.get(EnhancedMassRatingPersistenceServie.COLUMN_TO_SORT);
    	String cmIdsFromSavedStatus = (String)savedStatus.get(EnhancedMassRatingPersistenceServie.COLUMN_TO_SORT);
		system.assertEquals(cmIdsFromTestData,cmIdsFromSavedStatus);
    	}
    }
    
     static testMethod void testUpdateCurrentWorkbenchStatus(){
     	System.runAs(DummyRecordCreator.platformUser) {
     	Map<String,Object> testData = prepareTestData();
    	EnhancedMassRatingPersistenceServie persistenceService = new EnhancedMassRatingPersistenceServie();
    	Id statusId = persistenceService.persistCurrentWorkbenchStatus('test',testData);
    	testData.put(EnhancedMassRatingPersistenceServie.NAME, 'changedName');
    	persistenceService.updateCurrentWorkbenchStatus(statusId, testData);
    	
    	Map<String,Object> updatedStatus = persistenceService.retrieveWorkbenchStatus(statusId);
    	system.assertEquals('changedName', (String)updatedStatus.get(EnhancedMassRatingPersistenceServie.NAME));
     	}
    }   
    
    private static Map<String,Object> prepareTestData(){
    	Map<String,Object> testData = new Map<String, Object>();
    	testData.put(EnhancedMassRatingPersistenceServie.NAME,'test');
    	testData.put(EnhancedMassRatingPersistenceServie.SEARCH_KEYWORD,'test');
    	testData.put(EnhancedMassRatingPersistenceServie.CURRENT_PAGE,10);
    	testData.put(EnhancedMassRatingPersistenceServie.DETAIL_SECTION_ID,'detail-section-id');
    	testData.put(EnhancedMassRatingPersistenceServie.VISIBLE_TAB_ID,'visible-tab-id');
    	testData.put(EnhancedMassRatingPersistenceServie.COLUMN_TO_SORT,'column-to-sort');
    	List<Id> cmIds = new List<Id>();
    	cmIds.add('01IE00000010CVt');
    	cmIds.add('01IE00000010CVv');
    	cmIds.add('01IE00000010CVg');
    	cmIds.add('01IE00000010CVd');
    	testData.put(EnhancedMassRatingPersistenceServie.CANDIDATE_MANAGEMENT_IDS,cmIds);
    	
    	return testData;
    }
    
    static testMethod void testGeneralSortableColumnMap(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Map<String,DisplayColumn> relationNameMap = FieldsClass.getReferenceFieldRelationshipMap('Placement_Candidate__c');
    	String tempDisplayColumn = '[{"sortable":null,"referenceFieldName":"CreatedBy","Order":null,"isCustomField":false,"fromasc":null,"fieldtype":"REFERENCE","Field_api_name":"CreatedById","defaultorder":null,"ColumnName":"Created By ID"},{"sortable":null,"referenceFieldName":null,"Order":null,"isCustomField":false,"fromasc":null,"fieldtype":"DATETIME","Field_api_name":"CreatedDate","defaultorder":null,"ColumnName":"Created Date"},{"sortable":null,"referenceFieldName":"LastModifiedBy","Order":null,"isCustomField":false,"fromasc":null,"fieldtype":"REFERENCE","Field_api_name":"LastModifiedById","defaultorder":null,"ColumnName":"Last Modified By ID"}]';
    	List<DisplayColumn> testDisplayColumn = new List<DisplayColumn>();
    	testDisplayColumn = GoogleSearchUtil2.generateNewDisplayColumnWithReferenceField(tempDisplayColumn, relationNameMap);
    	Map<String, boolean> testGeneralSortableColumnMap = EnhancedMassRatingPersistenceServie.GeneralSortableColumnMap(testDisplayColumn);
    	System.assertEquals(testGeneralSortableColumnMap.size(),3);
    	}
    }
}