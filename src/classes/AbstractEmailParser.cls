/**
* This abstract class implements IEmailParser and as a template class, it creates parse() method 
* to parse email to CVContent object
*
*/


public abstract class AbstractEmailParser implements ICVEmailParser{
	
	protected Messaging.InboundEmail email;
	
	
	public AbstractEmailParser(){
		
	}
	
	
	public AbstractEmailParser(Messaging.InboundEmail inEmail){
		this.email = inEmail;
	}
	
	public void setEmail( Messaging.InboundEmail inEmail ){
		this.email = inEmail;
	}
	
	/**
	* Parse and extract the details of enquiry email to CvContent object
	* return :
	* 	CvContent object
	*/
	public abstract CVContent parse();

	/**
	* Parse and extract the details of incoming email to JXTContent object
	* 
	* return : JXTContent object
	*/
	public abstract JXTContent parseJxtEmail();
	
    
}