public with sharing class AstutePayrollAccountController {
	public List<User> currentActiveUsers{get; set;}
	public List<SelectOption> astutePayrollAccountDetails{get; set;}
	public boolean hiddenupdatebutton{get; set;}
	public String message{set;get;} 
	public Boolean msgSignalhasmessage{get ;set;} //show error message or not
    
	public AstutePayrollAccountController(){
		hiddenupdatebutton = true;
		currentActiveUsers = DaoUsers.getActiveUsers();
		getAstutePayrollAccountDetails();
	}
	
	public void getAstutePayrollAccountDetails(){
		astutePayrollAccountDetails=new list<SelectOption>();
		
		list<StyleCategory__c> apAccounts=new list<StyleCategory__c>();
		try{
			apAccounts=AstutePayrollUtils.GetAstutePayrollAccounts();
		}catch(Exception ex){
			message= 'Please set up an active AstutePayroll account.';
			customErrorMessage();
			hiddenupdatebutton = false; 
		}
		
		if(apAccounts.size()>0){
			astutePayrollAccountDetails.add(new SelectOption(' ','--Select--'));
			for(StyleCategory__c apAccount : apAccounts){
					astutePayrollAccountDetails.add(new SelectOption(apAccount.id, apAccount.Name));
				}
		}else{
			hiddenupdatebutton = false; 
			astutePayrollAccountDetails.add(new SelectOption('','No Activie AstutePayroll account'));
		}
		
	}
	
	public PageReference updateUserAPAccount(){
		try{
			if(currentActiveUsers.size()>0){
				//check FLS
				List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
					User.Email,
					User.AstutePayrollAccount__c
				};
				fflib_SecurityUtils.checkUpdate(User.SObjectType, fieldList);
				update currentActiveUsers;
			}
			
			msgSignalhasmessage = true;
			message='Update Successfully!';
			customSuccessMessage();
		}
		catch(Exception e){
			message = 'Update Unsuccessfully!';
			customErrorMessage();	
		}
		
		
		return null;
	}
	public void customErrorMessage(){
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, message);
		ApexPages.addMessage(myMsg);
		msgSignalhasmessage = true;
	}
	public void customSuccessMessage(){
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, message);
		ApexPages.addMessage(myMsg);
		msgSignalhasmessage = true;
	}

}