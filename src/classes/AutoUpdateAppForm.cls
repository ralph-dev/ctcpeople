public class AutoUpdateAppForm{
//** auto(Scheduler) update application form for dropdown lists only, other fields have not been applied with this functionality
//** field description call has 10 limitation for one transaction
//** so assume each application form has 10 dropdown picklist 
//** all div(application form body) files will be stored in Document object and uploaded to EC2
//----------------------------------------------------------------------------------------------

xmldom theXMLDom;

String NameSpace = 'PeopleCloud1__';
public AutoUpdateAppForm(){}

public AutoUpdateAppForm(Map<string, SObjectField> allfields, String theId){
    CommonSelector.checkRead(Document.SObjectType, 'Id,type, Name, body');
    document divtoupload = [select Id,type, Name, body From document where id =: theId];
    List<Document> docList = new List<Document>();
    docList.add(divtoupload);
    boolean has_update = CheckFileFields(docList,allfields);
    system.debug('has_update =' + has_update );
}

public boolean CheckFileFields(List<Document> docList, Map<string, SObjectField> o_fields){
    boolean is_toupdate = false;
    try{
        //* * * * * * * * * get html div document body* * * * * * * * *
        String bodyStr = '';
        //Document d = [select Id , body from Document where Id =: docId];
        //system.debug('doc =' + d.Id);
        for(Document d: docList){
            bodyStr += d.body.toString();
        }
        //System.debug('bodyStr: '+ bodyStr);
        theXMLDom = new xmldom(bodyStr);
        //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        if(theXMLDom != null)
        {
            //***************** check the dropdown list ********************************
            List<xmldom.Element> selects = this.theXMLDom.getElementsByTagName('select');
            if(selects != null)
            {
                if(selects.size() >0)
                {
                    is_toupdate = true;
                
                }
            
            }
            for(xmldom.Element e : selects)
            {
                if(e.hasAttributes())
                {
                    String theapi = e.getAttribute('name');
                    
                    if(theapi != null && theapi != '')
                    {
                        /*if(theapi.toLowerCase().indexOf('duoxuan') != -1)
                        {
                            theapi = theapi.substring(0,theapi.indexOf('duoxuan'));
                        }
                        */
                        if(theapi.toLowerCase().indexOf('_table') != -1)
                        {
                            theapi = theapi.substring(0,theapi.indexOf('_table'));
                        }
                        if(theapi.indexOf(NameSpace) != -1)
                        {
                            theapi = theapi.substring(theapi.indexOf(NameSpace)+ NameSpace.length() );
                        }    
                        //system.debug('theapi =' + theapi);
                        List<String> alloptions = describePickList(o_fields, theapi);
                        List<xmldom.Element> newElements = new List<xmldom.Element>(); 
                        String optiontemp ='';
                        if(alloptions.size() >0)
                        {
                            for(String s : alloptions)
                            {
                                s = AdvertisementUtilities.ConvertString(s);
                                optiontemp = 'option';
                                xmldom.Element ne = new xmldom.Element(optiontemp);
                                ne.nodeValue = s;
                                ne.attributes.put('value', s);
                                newElements.add(ne);
                            }
                            
                            e.removeAllChildren();
                            e.appendChildren(newElements);
                            //system.debug('the new select =' + e);
                        }

                    }
                
                }
            
            
            }
           //****************************************************************************
        }
        if(is_toupdate)
        {
            List<xmldom.Element> docroot = theXMLDom.getElementsByTagName('table');
            
            String newbody = docroot[0].toHtmlString();
            List<Document> docListToUpdate = new List<Document>();
            for(Document d: docList){
                d.body = Blob.valueOf(newbody);
                ApplicationForm EC2update = new ApplicationForm();
                string divfileec2 = EC2update.EC2Callout('div',d);
                if(divfileec2 != null){
                    docListToUpdate.add(d);
                }else{
                    //** error notification
                    return false;
                }
            }
        fflib_SecurityUtils.checkFieldIsUpdateable(Document.SObjectType, Document.Body);
        update docListToUpdate;    
        return true;
        }
    }catch(system.Exception e){
        //** error notification
    }
    return false;
    
}



public List<String> describePickList(Map<string, SObjectField> all_fields, String fieldapi){
//*** some apex using this kind of describe call method as well. maybe put into one class in future. 
        //system.debug('all_fields='+ all_fields );
        DescribeFieldResult fdr = all_fields.get(fieldapi).getDescribe();

        List <Schema.PicklistEntry> pickValList;
        List<String> options = new List<String>();
        
        pickValList = fdr.getPicklistValues();
        if(pickValList!= null && pickValList.size() >0){
            
            for(Schema.PicklistEntry entry:pickValList){
                if(entry.isActive()){
                        
                        options.add(entry.getLabel()); 
                }
            }
        } 
        return options;

}

}