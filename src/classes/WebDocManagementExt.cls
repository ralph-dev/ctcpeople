public with sharing class WebDocManagementExt {

	public Contact theContact; 
	public String userBucket {get;set;}
	public String activeSkillParsing {get; set;}
	public String endpointwebsite {get; set;}
	
	public WebDocManagementExt(ApexPages.StandardController controller) {
		theContact = (Contact) controller.getRecord();
	    String companyID = Userinfo.getOrganizationId();
	    PCAppSwitch__c CS = PCAppSwitch__c.getInstance();//Get the customer setting value
	    //system.debug('CS ='+ CS);
	    if(Test.isRunningTest()){
	    	CS.Enable_Daxtra_Skill_Parsing__c = true;
	    }
	    try{
		    if (CS.Enable_Daxtra_Skill_Parsing__c == false){
		    	activeSkillParsing = 'false';
		    }else{
		    	activeSkillParsing = companyID.substring(0, 15);	
		    }
	    }
	    catch(Exception e){
	    	activeSkillParsing = 'false';
	    }
	    
	    //endpointwebsite = Endpoint.PEOPLECLOUDWEBSITE;
	    endpointwebsite = Endpoint.getcpewsEndpoint();
	}
	
	public void pageinit(){		
		UserInformationCon usercls = new UserInformationCon();
		User u = usercls.getUserDetail();
	    
	    if(u != null){
	    	userBucket = u.AmazonS3_Folder__c;
	    }
	    //system.debug('user Bucket =' + userBucket );
	        
	}
}