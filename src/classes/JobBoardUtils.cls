public class JobBoardUtils {
	public final static String SEEK = 'Seek';
	public final static String MYCAREER = 'MyCareer';
	public final static String CAREERONE = 'CareerOne';
	public final static String TRADEME = 'TradeMe';
	public final static String JXT = 'Jxt';
	public final static String JXT_NZ = 'JXT_NZ';
	public final static String INDEED = 'Indeed';
	public final static String LINKEDIN = 'LinkedIn';


	public final static String JXTACCOUNT = 'JXT Account';
	public final static String CAREERONEACCOUNT = 'CareerOne Account';
	public final static String TRADEMEACCOUNT = 'TradeMe Account';
	public final static String JXT_NZACCOUNT = 'Jobx Account';
	public final static String INDEEDACCOUNT = 'Indeed Account';
	public final static String LINKEDINACCOUNT = 'LinkedIn Account';

	public static Id getRecordId(String developerName){
		return DaoRecordType.getRecordTypeByDeveloperName(developerName).Id;//[Select Id, DeveloperName From RecordType  where DeveloperName=:developerName].Id;
	}

	public static String wrappedWithCDATA(String str){
		return '<![CDATA['+str+']]>';
	}
	
	public static String removebreakcharacteratrichtextarea(String str){
		return str.replaceAll('\\n','');
	}
	
	public static String blankValue(String str){
		if(isBlank(str)){
			return '';
		}else{
			return str;
		}
	}
	
	public static boolean isBlank(String str){
		if(str==''||str==null||str=='null'){
			return true;
		}else{
			return false;
		}
	}

	public static String removeAmpersand(String str){
		if(str==null) return '';
		if(str.contains('&')){
			return str.replaceAll('&(?!amp)','&amp;');
		}else{
			return str;			
		}
	}

	public static String booleanValue(String str){
		if(str==null || str=='' || str=='FALSE'){
			return 'FALSE';
		}else{
			return 'TRUE';
		}
	}

	//remove List element blank character
	public static String removeListTrim(List<String> previousList){
		String returnresult = '';
		for (integer i = 0; i < previousList.size(); i++){
			if(returnresult == ''){
				returnresult = previousList[i].trim();
			}else{
				returnresult = returnresult +','+String.valueOf(previousList[i].trim());
			}
		}
		return returnresult;
	}
	
	public static List<SelectOption> getTemplateOptions(String jobBoard){
		List<SelectOption> templateOptions = new List<SelectOption>();
		StyleCategory__c[] templates = CTCService.getOptions(1,jobBoard); 
        templateOptions.add(new SelectOption('', '--none--'));
        if(templates != null){
            for(StyleCategory__c ts : templates){
            	if(jobBoard.toLowerCase()=='jxt'){
            		templateOptions.add(new SelectOption(ts.templateJXT__c, ts.Name));
            	}else if(jobBoard.toLowerCase()=='jxt_nz'){
            		System.debug('option = '+ts.templateJXTNZ__c);
            		System.debug('name = '+ts.Name);
            		templateOptions.add(new SelectOption(ts.templateJXTNZ__c, ts.Name));
            	}
            }
        }
        return templateOptions;
	}

	public static List<SelectOption> getStyleOptions(String jobBoard){
	 	List<SelectOption> styleOptions = new List<SelectOption>();
    	 StyleCategory__c[] styles = CTCService.getOptions(2,null); 
        if(styles != null){          
            for(StyleCategory__c sc : styles){
                styleOptions.add(new SelectOption(sc.Id, sc.Name));
            }
        } 
        return styleOptions;
    }
    
    public static boolean insertDetailTable(String jobRefCode, String iid, String aid, String bucketname, String hname, String htype,
       		 String dname, String dtype, String fname, String ftype, String divisionName, String webSite, string jobTitle){
       	if(Test.isRunningTest()){
       	    return true;
       	}
        CTCWS.PeopleCloudWSDaoPort port = new CTCWS.PeopleCloudWSDaoPort();
        port.timeout_x = 60000;
        return  port.insertJobPostingDetail(jobRefCode, iid, aid, bucketname, hname, htype, dname, dtype, fname, ftype, divisionName, webSite, jobTitle);
    }
    //to be deprecated
    public static Integer updateDetailTable(String aid, String hname, String htype, String dname, String dtype, String fname, String ftype, String iid){
    	 CTCWS.PeopleCloudWSDaoPort port = new CTCWS.PeopleCloudWSDaoPort();
         port.timeout_x = 60000;
         if(Test.isRunningTest()){
         	return 1;
         }else{
         	return port.updateJobPostingByVid(aid, hname, htype, dname, dtype, fname, ftype,iid);
         }
    }

    public static Integer updateDetailTable(String aid, String hname, String htype, String dname, String dtype, String fname, String ftype, String iid, String jobTitle){
    	 CTCWS.PeopleCloudWSDaoPort port = new CTCWS.PeopleCloudWSDaoPort();
         port.timeout_x = 60000;
         if(Test.isRunningTest()){
         	return 1;
         }else{
         	return port.updateJobPostingWithTitleByVid(aid, hname, htype, dname, dtype, fname, ftype,iid, jobTitle);
         }
    }

	/**
	 * This method check if the assigned accounts for the current user is active
	 * @param account             a list of account id separated by ;
	 * @param recordTypeId        record type id of the account
	 * @return true       		  The current user has active job board account
	 *         false              The current user does not active job board account
	 */
	public static Boolean checkUserHasActiveJobBoardAccount(String account, Id recordTypeId) {
		if (String.isNotEmpty(account)) {
			List<String> ids = account.split(';');
			List<Advertisement_Configuration__c> adAccounts = new AdvertisementConfigurationSelector().getActiveAccountByIds(ids);
			if (adAccounts != null && adAccounts.size() > 0) {
				for (Advertisement_Configuration__c adAccount : adAccounts) {
					if (adAccount.RecordTypeId.equals(recordTypeId)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * This method check if the there is active account for a given record type
	 * @param recordTypeId        record type id of the account
	 * @return true       		  there is active account for the give record type
	 *         false              there is not active account for the give record type
	 */
	public static Boolean checkUserHasActiveJobBoardAccount(Id recordTypeId) {
		List<Advertisement_Configuration__c> adAccounts = new AdvertisementConfigurationSelector().getActiveAccountByRecordTypeId(recordTypeId);
		if (adAccounts != null && adAccounts.size() > 0) {
			return true;
		}
		return false;
	}


	/************************************** Old privilege method ***********************************************/
	/*
	 * These method need to be replaced with the above two method after these account change
	 * from StyleCategory object to Advertisement Configuration Object
	 */

	//Enable Job Account for current user
	public static boolean JobBoardAvailableCheck(String jobBoardId){
		boolean enableJobBoard = false;
		if(jobBoardId!=null && jobBoardId!=''){
			enableJobBoard = DaoStyleCategory.checkJobBoardAccountActive(jobBoardId);//check the account active or not
		}
		return enableJobBoard; 
	}
	 
	public static boolean available(String website){
		boolean result = false;
		CommonSelector.checkRead(StyleCategory__c.sObjectType, 'Name');
		StyleCategory__c[] srs = [select Name from StyleCategory__c where Account_Active__c=true and WebSite__c=:website limit 5];
		if(srs!=null && srs.size()>0){
			result = true;
		}
		return result;
	}
	
	public static boolean whiteLabelAvailable(String website){
		boolean result = false;
		CommonSelector.checkRead(StyleCategory__c.sObjectType, 'Name');
		StyleCategory__c[] srs = [select Name from StyleCategory__c where Account_Active__c=true and WebSite__c=:website and White_labels__c!=null limit 5];
		if(srs!=null && srs.size()>0){
			result = true;
		}
		return result;
	}
	
	public static boolean trademeAvailable(){
		boolean result = false;
		CommonSelector.checkRead(StyleCategory__c.sObjectType, 'Name');
		StyleCategory__c[] srs = [select Name from StyleCategory__c where Account_Active__c=true and 
		 		WebSite__c='TradeMe' and OfficeCodes__c!=null and
		 		 ProviderCode__c!=null and CompanyCode__c!=null limit 5];
		//system.debug('srs ='+ srs);
		if(srs!=null && srs.size()>0){
			result = true;
		}
		return result;
	}

	/************************************** Old priviliage method ***********************************************/
	
	public static List<SelectOption> getStyleApplicationFormSelectOption(){
		List<SelectOption> StyleApplicationFormSelectOption = new List<SelectOption>();
		List<StyleCategory__c> getStyleCategory = new List<StyleCategory__c>();
		getStyleCategory = DaoStyleCategory.getApplicationFormStyle();
		if(getStyleCategory != null){
			StyleApplicationFormSelectOption.add(new SelectOption('','--None--'));
			for(StyleCategory__c sc : getStyleCategory){
	        	StyleApplicationFormSelectOption.add(new SelectOption(sc.Id, sc.Name));
	        } 
		}else{
			StyleApplicationFormSelectOption = new List<SelectOption>();
		}
		return StyleApplicationFormSelectOption;
	}
	
	//insert the skill group picklist value option
    public static List<SelectOption> getSelectSkillGroupList(){
	  	List<Skill_Group__c> tempskillgrouplist = SkillGroups.getSkillGroups();
	  	List<SelectOption> displaySelectOption = new List<SelectOption>();
	  	for(Skill_Group__c tempskillgroup : tempskillgrouplist){
	  		displaySelectOption.add(new SelectOption(tempskillgroup.Skill_Group_External_Id__c , tempskillgroup.Name));
	  	}
	  	return displaySelectOption;
	}

}