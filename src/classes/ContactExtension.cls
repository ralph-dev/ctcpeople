/**
 * This is the extension class for Contact Object
 *
 * Created by: Lina Wei
 * Created on: 18/04/2017
 */

public with sharing class ContactExtension {

    public ContactExtension(){}

    public ContactExtension(Object obj){}

    @RemoteAction
    public static Contact getPeopleInfo(String Id) {
        return new CandidateSelector().selectContactById(Id);
    }

    @RemoteAction
    public static List<Contact> savePeopleInfo(List<Map<String, Object>> contacts) {
        return new ContactServices().updateRecords(contacts);
    }

    @RemoteAction
    public static Contact getCompactPeopleInfo(String Id) {
        return new CandidateSelector().selectContactWithBasicFieldsById(Id);
    }

    /**
     *  Provide additional information on the remote action API
     **/
    @RemoteAction
    public static Map<String, RemoteAPIDescriptor.RemoteAction> apiDescriptor(){
        RemoteAPIDescriptor descriptor = new RemoteAPIDescriptor();
        descriptor.description('CRUD remote action for Contact Object');

        descriptor.action('getPeopleInfo').param(String.class);
        descriptor.action('savePeopleInfo').param(List<Map<String, Object>>.class);
        descriptor.action('getCompactPeopleInfo').param(String.class);

        return descriptor.paramTypesMap;
    }
}