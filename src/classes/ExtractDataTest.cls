@isTest
private class ExtractDataTest {
		public static testmethod void test(){
			System.runAs(DummyRecordCreator.platformUser) {
			list<Placement_Candidate__c> cmList = DataTestFactory.createCandidateManagements();
			Placement_Candidate__c pc = [select Id from Placement_Candidate__c where candidate__c != null limit 1];
			Apexpages.StandardController accCon = new Apexpages.StandardController(pc);
			ExtractData4SRController ed1 = new ExtractData4SRController(accCon);
			PageReference p1=ed1.inits();
			system.assert(p1.getParameters().get('arg0') !=null );
	
			List<Placement_Candidate__c> vacpc = [select Id from Placement_Candidate__c where Placement_Candidate__c.Candidate__c<>null limit 5];
			Apexpages.StandardsetController accConvac = new Apexpages.StandardsetController(vacpc);
			ExtractData4SRController ed7 = new ExtractData4SRController(accConvac);
			PageReference p7=ed7.inits();
			system.assert(p7.getParameters().get('arg0') !=null );
			
			Contact con = [select Id,recordtypeid from Contact where recordtypeid<>null and Email<>null limit 1];
			Apexpages.StandardController conCon = new Apexpages.StandardController(con);
			ExtractData4SRController ed2 = new ExtractData4SRController(conCon);
			PageReference p2=ed2.inits();			
			system.assert(p2.getParameters().get('arg0') !=null );
			
			List<Contact> setcon = [select id from Contact where recordtypeid<>null limit 10];
			Apexpages.Standardsetcontroller setCons = new Apexpages.Standardsetcontroller(setcon);
			ExtractData4SRController ed3 = new ExtractData4SRController(setCons);
			PageReference p3=ed3.inits();			
			system.assert(p3.getParameters().get('arg0') !=null );
			
			Test.setCurrentPage(Page.Extract4SR);
			ExtractData4SRController ed4 = new ExtractData4SRController();
			PageReference p4=ed4.inits();
			system.assert(p4.getParameters().get('arg0') ==null );
			
			Test.setCurrentPage(Page.Extract4SR);
			String cmids = '';
			Placement__c vacancy = [select id from Placement__c limit 1];
			List<Placement_Candidate__c> pcs = [select id from Placement_Candidate__c where Placement_Candidate__c.Candidate__c<>null limit 5];
			for(Placement_Candidate__c newpc: pcs){
				cmids = cmids + newpc.id + ',';
			}
			ApexPages.currentPage().getParameters().put('sourcepage','test');
			ApexPages.currentPage().getParameters().put('id',vacancy.id);
			ApexPages.currentPage().getParameters().put('cmids',cmids);
			ApexPages.currentPage().getParameters().put('cp','1');
			ApexPages.currentPage().getParameters().put('sc','Rating__c');
			ApexPages.currentPage().getParameters().put('asc','DESC');
			ExtractData4SRController ed5 = new ExtractData4SRController();
			PageReference p5=ed5.inits();
			system.assert(p5.getParameters().get('arg0') !=null );
			
			List<RecordType> testrecordType = DaoRecordType.nonCandidateRTs;
			Contact newcon = new Contact(LastName='testtest', Recordtypeid=testrecordType[0].id);
			insert newcon;
			Contact connoemail = [select Id,recordtypeid from Contact where recordtypeid in: testrecordType and Email=null limit 1];
			Apexpages.StandardController conConnoemail = new Apexpages.StandardController(connoemail);
			ExtractData4SRController ed6 = new ExtractData4SRController(conConnoemail);
			PageReference p6=ed6.inits();				
			system.assert(p6 == null );
			
			Test.setCurrentPage(Page.Extract4SMS);ApexPages.currentPage().getParameters().put('id',vacancy.id);
			ApexPages.currentpage().getParameters().put('sourcepage','test');
			ApexPages.currentPage().getParameters().put('cmids',cmids);
			ApexPages.currentPage().getParameters().put('cp','1');
			ApexPages.currentPage().getParameters().put('sc','Rating__c');
			ApexPages.currentPage().getParameters().put('asc','DESC');
			ExtractData4SRController ed8 = new ExtractData4SRController();
			PageReference p8 = ed8.SMSinits();
			system.assert(p8.getParameters().get('retURL') !=null );
			}
		}
		
	
}