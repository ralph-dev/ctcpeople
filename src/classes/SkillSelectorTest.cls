/**
 * This is the test class for SkillSelector
 * 
 * @Created by: Lina
 * @Created on: 10/08/2016
 * 
 */

@isTest 
private class SkillSelectorTest {

    static testmethod void testGetAllSkills() {
    	System.runAs(DummyRecordCreator.platformUser) {
        Skill__c s = DummyRecordCreator.createSkill();
        List<Skill__c> skillList = SkillSelector.getAllSkills();
        system.assertEquals(1, skillList.size());
    	}
    }
    
    static testmethod void getSkillsForSkillGroup() {
    	System.runAs(DummyRecordCreator.platformUser) {
        //create data
        Skill_Group__c sg = DummyRecordCreator.createSkillGroup();
        Skill__c s = new Skill__c(Name='test', Skill_Group__c=sg.Id, Ext_Id__c = 'S001');
        insert s;
        List<Skill__c> skillList = SkillSelector.getSkillsForSkillGroup(sg.Id);
        system.assertEquals(1, skillList.size());
        system.assertEquals('S001', skillList[0].Ext_Id__c);
    	}
    }
}