public class CTCAccessClass {
	public static boolean CTCAccessChecking(String website){
		boolean acchaswebsite = false;
		JobboardsController jobboardschecking = new JobboardsController();
	    jobboardschecking.checkPrivilege();
	    if(jobboardschecking.hasError == false){
	        if(website.toLowerCase() == 'seek'){
	        	acchaswebsite = jobboardschecking.client_has_seek;
	        }
	        if(website.toLowerCase() == 'careerone'){
	        	acchaswebsite = jobboardschecking.client_has_c1;
	        }
	        if(!acchaswebsite){
	         //   ApexPages.Message error = new ApexPages.Message(ApexPages.severity.WARNING,'Currently you have no privilege to update this Seek.com Ad.!');              
	         //   ApexPages.addMessage(error); 
	         return true;
	        }
	        else{
	            return false;
	        }
	                                 
	    }
	 	return true;
	}
}