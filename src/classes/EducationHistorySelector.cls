public with sharing class EducationHistorySelector extends CommonSelector{

	public EducationHistorySelector(){
		super(Education_History__c.sObjectType);
	}

	/*private Schema.SObjectType getSObjectType(){
		return Education_History__c.SObjectType;
	}*/

	public override List<Schema.SObjectField> getSObjectFieldList(){
		return new List<Schema.SObjectField>{
			Education_History__c.Id,
			Education_History__c.Contact__c
		};
	}

	public List<Education_History__c> getContactEducationHistory(List<Id> conIds){
		SimpleQueryFactory qf = simpleQuery();
		List<Education_History__c> result = new List<Education_History__c>();
		String condition = 'Contact__c IN :conIds';
		qf.selectFields(getSObjectFieldList());
		qf.setCondition(condition);
		qf.getOrderings().clear();
		String soqlStr = qf.toSOQL();
		try{
			result = Database.query(soqlStr);
		}catch(Exception e){
			NotificationClass.notifyErr2Dev('SOQL on Education History fail', e);
			return null;
		}
		return result;
	}
}