public class TrademeUtil {
	public static final String SEPERATOR = ';';

	public static List<SelectOption> getOfficeCodes(String officeCodeString){
		List<SelectOption> options = new List<SelectOption>();
        String[] officeCodes = officeCodeString.split(';');
        for(String officeCode:officeCodes){
            options.add(new SelectOption(officeCode,officeCode));
        }
        return options;
	}
	
	public static List<SelectOption> getPhotos(){
		
		List<SelectOption> options = new List<SelectOption>();
		CommonSelector.checkRead(Photo__c.sObjectType, 'Name, url__c ');
        Photo__c[] photos = [select Name, url__c from Photo__c limit 200];
        if(photos!=null && photos.size()>0){
        	for(Photo__c photo:photos){
	            options.add(new SelectOption(photo.url__c, photo.Name));
	        }
        }
        return options;
	}
	
	public static String makeUrlString(String[] urls){
		if(urls!=null && urls.size()!=0){
			String temp = '';
			for(String url:urls){
				temp+=url+TrademeUtil.SEPERATOR;
			}
			return temp.substring(0, temp.lastIndexOf(TrademeUtil.SEPERATOR));
		}else{
			return '';
		}
	}
	public static String filter(String s){
		if(s==null) return '';
		return s.replaceAll('\\x94', '"').replaceAll('\\x93', '"').replaceAll('\\x92', '\'').replaceAll('\\x91', '\'');
	}
	
	public static testmethod void test001(){
		System.runAs(DummyRecordCreator.platformUser) {
		system.assertEquals(TrademeUtil.getOfficeCodes('a;b').size(),2);
		Photo__c[] photos = [select Name, url__c from Photo__c limit 200];
		system.assert(TrademeUtil.getPhotos().size()==photos.size());
		system.assert(TrademeUtil.filter('abc')!='');
		String[] ss = new String[]{'abc', 'bdc'};
		system.assert(TrademeUtil.makeUrlString(ss)!= '');
		String[] ss2 = null;
		system.assertEquals(TrademeUtil.makeUrlString(ss2), '');
		}
	}
}