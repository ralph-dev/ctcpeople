public with sharing class AvailabilitySelector extends CommonSelector{
	
	public AvailabilitySelector(){
		super(Days_Unavailable__c.sObjectType);
	}
	
	final static String QUERY_PART1 = 'select Id, Start_Date__c, Candidate_Management__r.name, Candidate_Management__r.Placement__r.name, Candidate_Management__r.Placement__r.Company__r.name,End_Date__c, Event_Status__c, EventReason__c, Contact__c from Days_Unavailable__c';
	final static String QUERY_PART1_FIELDS = 'Id, Start_Date__c, Candidate_Management__r.name, Candidate_Management__r.Placement__r.name, Candidate_Management__r.Placement__r.Company__r.name,End_Date__c, Event_Status__c, EventReason__c, Contact__c';
	
	final static String ROSTER_QUERY_FIELDS = 'Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,' 
											+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
											+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
											+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
											+ 'Recurrence_End_Date__c, RecordTypeId, Rate__c';
	final static String ROSTER_QUERY_BASE1 	= 'SELECT Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,' 
											+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
											+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
											+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
											+ 'Recurrence_End_Date__c, RecordTypeId, Rate__c FROM Days_Unavailable__c';

	final static String ROSTER_QUERY_BASE2 	= 'SELECT Accounts__c, Accounts__r.Name, Contact__c, Contact__r.Name,' 
											+ 'Shift__c, Shift__r.Vacancy__r.Name, Start_Date__c, End_Date__c, '
											+ 'Candidate_Management__c, Shift_Type__c, Start_Time__c, End_Time__c,'
											+ 'Event_Status__c, Location__c, Weekly_Recurrence__c, Recurrence_Weekdays__c,'
											+ 'Recurrence_End_Date__c, RecordTypeId, Rate__c FROM Days_Unavailable__c';	

	private static AvailabilitySelector selector = new AvailabilitySelector();
	
	public static List<Days_Unavailable__c>  getAllRelatedAvlRecsByCanIdNDateRange(List<AvailabilityEntity> avlList){
		String query = generateQueryString(null, avlList);
		
		List<Days_Unavailable__c> relatedAvlList = Database.query(query);

		//System.debug('AvailabilitySelector---relatedAvlList:' + relatedAvlList);
		return relatedAvlList;	
	} 
	
	public static List<Days_Unavailable__c>  getAllRelatedAvlRecsByCanIdAvlIdNDateRange(String avlId, List<AvailabilityEntity> avlList){
		String query = generateQueryString(avlId, avlList);

		List<Days_Unavailable__c> relatedAvlList = Database.query(query);

		//System.debug('AvailabilitySelector---relatedAvlList:' + relatedAvlList);
		return relatedAvlList;	
	} 
	
	public static Days_Unavailable__c getAvlRecordById(String avlId){
		String query = QUERY_PART1 + ' where Id = \'' + avlId + '\' and Event_Status__c!=NULL ';
		
		selector.checkRead(QUERY_PART1_FIELDS);
		List<Days_Unavailable__c> avlList = Database.query(query);
		
		if(avlList != null && avlList.size() > 0 ){
			return avlList.get(0);
		}else{
			return null;	
		}
		
	}
	
	//Fetch list days unavailable record according the start date, end date and google search result
	public static List<Days_Unavailable__c> getAllRelatedAvlRecordsByStartDateEndDateGoogleResult(List<AvailabilityEntity> avlList, List<String> googleSearchCandiateIds){
		String query = generateQueryString(null, avlList);
		if(query!= null && query != '' && googleSearchCandiateIds!= null && googleSearchCandiateIds.size()>0){
			//system.debug('googleSearchCandiateIds ='+ googleSearchCandiateIds);
			query = query + ' and Contact__c in :googleSearchCandiateIds ORDER BY Contact__c ASC, Start_Date__c ASC , End_Date__C DESC'; //Sort the records by Start date ASC and End Date Desc
		}else{
			query = query + ' ORDER BY Contact__c ASC, Start_Date__c ASC , End_Date__C DESC';
		}
		//system.debug('query ='+ query);
		List<Days_Unavailable__c> relatedAvlList = Database.query(query);
		//System.debug('AvailabilitySelector---relatedAvlList:' + relatedAvlList);
		return relatedAvlList;	
	}
	
	//General Contact map for wrapclass
	/*
	public static Map<String, Contact> getAllContactInfo(List<String> CandidateIdList, String searchcolumnString, String sortByColumnApiName){
		String sortByString = null;
		if(sortByColumnApiName == null){
			sortByString = '';
		}else{
			sortByString = ' order by ' + sortByColumnApiName;
		}
		String queryString = 'select id, Name '+ searchcolumnString +' from Contact where id in: CandidateIdList' + sortByString;
		//system.debug('query string to get candidate info: ' + queryString);
		
		Map<String, Contact> conMap = new Map<String, Contact>();
		List<Contact> tempContactList = new List<Contact>();
		
		tempContactList = Database.query(queryString);
		//system.debug('------ tempContactList: ' + tempContactList);
		if(tempContactList!= null && tempContactList.size()>0){
			for(Contact con:tempContactList){ 
	            conMap.put(con.id, con); //Save the contact to a map     
	        }
	        return conMap;
		}else{
			return null;
		}
	}
	*/
	
	//Convert Contact list to map
	public static Map<String, Contact> convertToAllContactInfoMap(List<Contact> contactList){
		Map<String, Contact> contactMap = new Map<String, Contact>();
		if(contactList!= null && contactList.size()>0){
			for(Contact con:contactList){ 
	            contactMap.put(con.id, con); //Save the contact to a map     
	        }
	        return contactMap;
		}else{
			return null;
		}
	}
	
	// generate a dummy list of contact when column view is being used
	public static List<Contact> getAllContactInfoList(List<String> CandidateIdList, String searchcolumnString, String sortByColumnApiName, String sortingOrder){
		String sortByString = null;
		if(sortByColumnApiName == null){
			sortByString = '';
		}else{
            sortingOrder = String.escapeSingleQuotes(sortingOrder);
            sortByColumnApiName = String.escapeSingleQuotes(sortByColumnApiName);
			sortByString = ' order by ' + sortByColumnApiName + ' ' + sortingOrder + ' NULLS LAST'; 
		}
		String queryString = 'select Name '+ searchcolumnString +' from Contact where id in: CandidateIdList' + sortByString;
		//system.debug('query string to get candidate info: ' + queryString);

		List<Contact> contactList = new List<Contact>();		
        queryString = String.escapeSingleQuotes(queryString);
		contactList = Database.query(queryString);
		//system.debug('------ contactList: ' + contactList);
		return contactList;
	}
	
	
	// generate a dummy list of contact when traditional view is being used
	public static List<Contact> generateDummyContactList(List<String> CandidateIdList){
		List<Contact> dummyContactList = new List<Contact>();
		
		for(String cid:CandidateIdList){
			Contact con = new Contact();
			con.Id = cid;
			dummyContactList.add(con);
		}
		return dummyContactList;
	}
	
	
	
	public static String generateQueryString(String avlId, List<AvailabilityEntity> avlList){
		 
		String queryPart2 = '';
		
		for(AvailabilityEntity avl: avlList){
			queryPart2 = generateQuertCondiction(queryPart2,avl.startDate,avl.endDate);					
		}
		
		String candidateId = avlList.get(0).candidateId;	// get candidate id
		queryPart2 = '(' + queryPart2 + ')';

		String query = '';
		if(avlId != null){
			if(candidateId != null && candidateId != ''){
				query = QUERY_PART1 + ' where Contact__c = \'' + candidateId + '\' and Id != \''+ avlId + '\' and ' + queryPart2 +' and Event_Status__c!=NULL';	
			}else{
				query = QUERY_PART1 + ' where Id != \''+ avlId + '\' and ' + queryPart2+' and Event_Status__c!=NULL';	
			}	
		}else{
			if(candidateId != null && candidateId != ''){
				query = QUERY_PART1 + ' where Contact__c = \'' + candidateId + '\' and ' + queryPart2+' and Event_Status__c!=NULL';	
			}else{
				query = QUERY_PART1 + ' where '+ queryPart2+' and Event_Status__c!=NULL';	
			}
		}
		
		//System.debug('AvailabilitySelector---query :' + query);
		
		selector.checkRead(QUERY_PART1_FIELDS);
		return query; 	
	}
	
	
	private static String generateQuertCondiction(String queryPart, String sd, String ed){
		if(queryPart != ''){
			queryPart = queryPart + ' or ';
		}

		//queryPart = queryPart + '((not End_Date__c< ' + sd + ') and (not Start_Date__c > ' + ed + ')) ';
		
		queryPart = queryPart + '(End_Date__c >= ' + sd + ' and End_Date__c <= ' + ed + ')' + 
								' or ' +	
								'(Recurrence_End_Date__c >= ' + sd + ' and Recurrence_End_Date__c <= ' + ed + ')' + 
								' or ' +		
								'(Start_Date__c >= ' + sd + ' and Start_Date__c <= ' + ed + ')' + 
								' or ' +
								'(Start_Date__c <= ' + sd + ' and End_Date__c >= ' + ed + ')' +
								' or ' +
								'(Start_Date__c <= ' + sd + ' and Recurrence_End_Date__c >= ' + ed + ')';
		return queryPart;
	} 

	public String generateShiftQueryString(List<ShiftDTO> shiftDTOs) {
		String shiftConditionQueryString = '';
		for(ShiftDTO sDTO: shiftDTOs) {
			
			String sDateString = sDTO.getStartDate();
			Date sDate = Date.valueOf(sDateString);
			String eDateString = sDTO.getEndDate();
			Date eDate = Date.valueOf(eDateString);
			
			if(!Test.isRunningTest()){
				if(eDate>=Date.today()) {
					if(Date.today()>=sDate) {
						sDateString = String.valueOf(Date.today());
					}
					shiftConditionQueryString = generateQuertCondiction(shiftConditionQueryString, sDateString, eDateString);
				}				
			}else{
				shiftConditionQueryString = generateQuertCondiction(shiftConditionQueryString, sDateString, eDateString);
			}
			
		}

		if(shiftConditionQueryString!=''){
			shiftConditionQueryString = ROSTER_QUERY_BASE2 + ' Where (' + shiftConditionQueryString + ') and Contact__c in: candidatesId';
		}
		//system.debug('queryString ='+ shiftConditionQueryString);
		
		selector.checkRead(ROSTER_QUERY_FIELDS);
		return shiftConditionQueryString;
	}

	public List<Days_Unavailable__c> getAllAvailabiltyRecordByShifts(List<ShiftDTO> shiftDTOs, Set<String> candidatesId){
		List<Days_Unavailable__c> dUnaviableList = new List<Days_Unavailable__c>();
		String queryString = generateShiftQueryString(shiftDTOs);
		
		if(queryString!=''){
			dUnaviableList = Database.query(queryString);
		}

		//Only split roster records
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');
		String atId = rtMap.get('Availability');

		system.debug('atId ='+ atId);
		List<Days_Unavailable__c> dUnaviableTempList = new List<Days_Unavailable__c>();
		for(integer i=0;i<dUnaviableList.size();i++) {
			
			if(dUnaviableList[i].RecordTypeId == rtId) {
				RosterDomain rDomain = new RosterDomain(dUnaviableList[i]);
				if(dUnaviableTempList!=null&&dUnaviableTempList.size()>0) {
					dUnaviableTempList.addAll(rDomain.splitRosterForDisplay());
				}else{
					dUnaviableTempList = rDomain.splitRosterForDisplay();
				}
				
				dUnaviableList.remove(i);
			}
		}
		if(dUnaviableTempList!=null)
			dUnaviableList.addAll(dUnaviableTempList);
		//system.debug('dUnaviableList ='+ dUnaviableList);
		return dUnaviableList;
	}

	//Get Picklist Value
	public static List<Schema.PicklistEntry> getAvailabilityPickListValue(String fieldAPIName){
     	Map<String, Schema.DescribeFieldResult> resultMap = DescribeHelper.getDescribeFieldResult(Days_Unavailable__c.sObjectType.getDescribe().fields.getMap().values());
    	Map<String,List<Schema.PicklistEntry>> describeResultMap = new Map<String,List<Schema.PicklistEntry>>();
        List<Schema.PicklistEntry> picklistValue = new List<Schema.PicklistEntry>();
        picklistValue = resultMap.get(fieldAPIName).picklistValues;
       
        //System.debug('picklistValue ='+picklistValue);
        return picklistValue;
    } 
}