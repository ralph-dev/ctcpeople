@isTest
private class WizardUtilsTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
    	//test method WizardUtils.convertDateToDateTime(Date inputDate)
        Date inputDate = Date.today();
        DateTime dateTimeConverted = WizardUtils.convertDateToDateTime(inputDate);
        System.assertNotEquals(dateTimeConverted,null);
        
        //test method WizardUtils.adjustTimeZone(DateTime inputDT)
        DateTime inputDateTime = DateTime.now();
        DateTime dateTimeAdjusted = WizardUtils.adjustTimeZone(inputDateTime);
        System.assertNotEquals(dateTimeAdjusted,null);
        
        //test method WizardUtils.convertStringToMap(String inputString)
        String inputString = 'test;test';
        Map<String,String> outputMapFromString = WizardUtils.convertStringToMap(inputString);
        System.assertNotEquals(outputMapFromString,null);
        
        //test method WizardUtils.convertListToMap(List<String> inputList)
        List<String> strList = new List<String>();
        strList.add(inputString);
        Map<String,String> outputMapFromList = WizardUtils.convertListToMap(strList);
        System.assertNotEquals(outputMapFromList,null);
        
        //test method WizardUtils.nextDT(DateTime inputDT)
        DateTime nextDateTime = WizardUtils.nextDT(inputDateTime);
        System.assertNotEquals(nextDateTime,null);
        
        //test method WizardUtils.getDateStrListForDateRange(Date startDate, Date endDate)
        Date startDate = Date.today();
        Date endDate = startDate.addDays(2);
        List<String> dateStrList = WizardUtils.getDateStrListForDateRange(startDate,endDate);
        System.assertNotEquals(dateStrList,null);
        
        //test method WizardUtils.getDiffBetweenTwoDates(Date startDate, Date endDate)
        Integer diff = WizardUtils.getDiffBetweenTwoDates(startDate,endDate);
        System.assertNotEquals(diff,0);
    	}
        
    }
}