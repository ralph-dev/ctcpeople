@isTest
public with sharing class CandidateManagementDummyRecordCreator{

	private List<Placement_Candidate__c> cms {get; set;}
	private Set<Id> cmIdSet {get; set;}
    private Map<Id, Placement_Candidate__c> cmMap {get; set;}
	
	
	public CandidateManagementDummyRecordCreator() {
		cms = new List<Placement_Candidate__c>();
		cmIdSet = new Set<Id>();
		cmMap = new Map<Id, Placement_Candidate__c>();
	}

	public List<Placement_Candidate__c> generateCandidateManagementDummyRecord(DummyRecordCreator.DefaultDummyRecordSet rs) {
		Integer jobApplicationId = 1000001;
		for(Integer i=0;i<rs.vacancies.size();i++) {
			Placement_Candidate__c cm = new Placement_Candidate__c(Candidate__c=rs.candidates[i].Id, Placement__c=rs.vacancies[i].Id);
			cms.add(cm);
		}
		//check FLS
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			Placement_Candidate__c.Candidate__c,
			Placement_Candidate__c.Placement__c
		};
		fflib_SecurityUtils.checkInsert(Placement_Candidate__c.SObjectType, fieldList);
		insert cms;
		return cms;
	}
}