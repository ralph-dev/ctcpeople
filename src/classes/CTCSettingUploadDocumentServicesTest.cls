@isTest
private class CTCSettingUploadDocumentServicesTest {
    public static String getTestCandidateId(){
        RecordType candRT=DaoRecordType.indCandidateRT; 
        Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',email='Cand1@test.ctc.test');
        return cand1.Id;
    }
    static testMethod void myUnitTest() {
    	//User u = DataTestFactory.createUser();
        //u.profileId = Userinfo.getProfileId();
        //insert u;
        System.runAs(DummyRecordCreator.platformUser) {
		    String candidateId = getTestCandidateId();
		    Test.setCurrentPage(Page.uploadDocuments);
		    ApexPages.currentPage().getParameters().put('srcId', candidateId);
		    ApexPages.currentPage().getParameters().put('lookupField', 'Document_Related_To__c');
		    
		    ApexPages.currentPage().getParameters().put('srcObj', 'dummy');
		    UploadDocumentsController udcNoSource = new UploadDocumentsController();   
		    
		    ApexPages.currentPage().getParameters().put('srcObj', 'Vacancy');
		    UploadDocumentsController udcVacancy = new UploadDocumentsController();     
		    
		    ApexPages.currentPage().getParameters().put('srcObj', 'Contact');
		    UploadDocumentsController udcContact = new UploadDocumentsController();     
		    
		    ApexPages.currentPage().getParameters().put('srcObj', 'Client');
		    UploadDocumentsController udcClient = new UploadDocumentsController();      
		    
		    ApexPages.currentPage().getParameters().put('srcObj', 'candidate');     
		    
		    UploadDocumentsController udc = new UploadDocumentsController();        
		    System.assert(udc.serviceEndPoint != null);
		    System.assert(udc.orgId != null);
		    System.assert(udc.bucketName != null);
		    System.assert(udc.nsPrefix != null);
		    System.assert(udc.sessionId != null);
		    System.assert(udc.srcId == candidateId);
		    System.assert(udc.srcObj == 'candidate');
		    System.assert(udc.retURLNotEncoded != null);
		    System.assert(udc.returnLinkValue != null);
		    //System.assert(udc.fieldSetName != null);      
		    
		    
		    udc.returnToPreviousPage();
        }
    }
}