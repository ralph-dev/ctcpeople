/**
    * This is the JXTEmailParser which is parsing email content
    * 
    * @author: Lorena
    * @createdDate: 6/10/2016
    * @lastModify: 
    */
public with sharing class JXTEmailParser extends AbstractEmailParser{
	
	public JXTEmailParser(){
		
	}
	public JXTEmailParser(Messaging.InboundEmail inEmail){
		super(inEmail);
	}
	
	public override CVContent parse() {
	    return new CVContent();
	}
	
	public override JXTContent parseJxtEmail(){
		
		if(this.email == null) return null;
		
		String fromAddress = this.email.fromAddress;
		Messaging.InboundEmail.Header[] contentType = this.email.headers;
		String bodyContent='';
		String tempFirstName='';
		String tempLastName='';
		String tempEmailAddress='';
		String tempAdIdFromEmail='';
		
		
		if(String.isNotBlank(this.email.plainTextBody)) {
		    bodyContent = this.email.plainTextBody;
		    tempFirstName = getValueInlineByTitle('Firstname', ':', bodyContent );
		    tempLastName = getValueInlineByTitle('Lastname', ':', bodyContent );
		    tempEmailAddress = getValueInlineByTitle('Email Address', ':', bodyContent );
		    tempAdIdFromEmail = getValueInlineByTitle('Reference Number', ':', bodyContent );
		    
		} else {
		    bodyContent = this.email.htmlBody.stripHtmlTags();
		    tempFirstName = getValueInblockByTitle('Firstname', ':', 'Lastname' ,bodyContent );
		    tempLastName = getValueInblockByTitle('Lastname', ':', 'has' ,bodyContent );
		    tempEmailAddress = getValueInblockByTitle('Email Address', ':', 'Should the applicant' ,bodyContent );
		    tempAdIdFromEmail = getValueInblockByTitle('Reference Number', ':', 'Email Address:', bodyContent );
		    
		}
		//Get JXTcontent 
		String firstName = tempFirstName!=null ? tempFirstName : 'Unparsed FirstName';
		String lastName = tempLastName!=null ? tempLastName :'Unparsed LastName';
		String emailAddress = tempEmailAddress!=null ? tempEmailAddress : '';
		String adIdFromEmail = tempAdIdFromEmail!=null ? tempAdIdFromEmail.split(':',-2).get(1) : null;
		AdvertisementSelector adSelector = new AdvertisementSelector();
		Advertisement__c ad = adIdFromEmail!=null ? adSelector.getVacIdByAdId(adIdFromEmail) : null;
		String adId = ad!=null ? ad.Id : null;
		String vacId = ad!=null ? ad.Vacancy__c : null;
		String vacOwnerId = ad!=null ? ad.Vacancy__r.ownerId : null;
		String skillGroupString = ad!=null ? ad.Skill_Group_Criteria__c : null;
		//String mobileNumber = getValueInlineByTitle('Mobile', ':', bodyContent )!=null ? getValueInlineByTitle('Mobile' , ':', bodyContent ) : 'Unparsed MobileNumber';
		//String status = getValueInlineByTitle('Status' , ':', bodyContent )!=null ? getValueInlineByTitle('Status' , ':', bodyContent ) : 'Unparsed Status';
		//Date applicationReceivedDate = getValueInlineByTitle('Application received date' , ':', bodyContent )!=null ? Date.valueof(getValueInlineByTitle('Application received date' , ':', bodyContent )) : Date.today();
		//String source = getValueInlineByTitle('Source' , ':', bodyContent )!=null ? getValueInlineByTitle('Source' , ':', bodyContent ) : 'Unparsed Source';
		                         
		List<CVContent> files = new List<CVContent>(); // list of attachment of email
		
		//Gei email attachment
		if( this.email.binaryAttachments != null && this.email.binaryAttachments.size() > 0){
			
			for(Integer i = 0; i < this.email.binaryAttachments.size() ; i++){
			    CVContent file = new CVContent();
				if(CandidateParserFactory.isValidAttachment(this.email.binaryAttachments[i].filename)){
					file.setCvFilename( this.email.binaryAttachments[i].filename);
					file.setContent( this.email.binaryAttachments[i].body);
					files.add(file);
				}
			}	
		}
		
		JXTContent content = new JXTContent(fromAddress, firstName, lastName, emailAddress, adId, vacId, vacOwnerId, skillGroupString, files);
		
		return content;
	}
	
	//parse email content by keyword and delimiter when the keyword and content are in same line
    public static String getValueInlineByTitle(String title, String delimiter, String src){

        if(title == null || src ==  null) return null;
        
        String ptn = '(?im)(?<=\\b' + title+ '\\s{0,5}' + delimiter + '\\s{0,5}' + ').+$';
        
        String ret = substringByRegex( src, ptn);
        
        if(ret != null) ret = ret.trim();
        
        if(ret!=null && ret!='') {
            return ret;
        } else {
            return null;
        }
        
    }
    
    //parse email content by keyword and delimiter when the keyword and content are in different line
    public static String getValueInblockByTitle(String title, String delimiter, String endKeyword, String src){
    
        if(title == null || src ==  null) return null;
        
        String ptn = '(?is)(?<=\\b' + title+ '\\s{0,5}' + delimiter + '\\s{0,5}' + ').+';
        
        if(endKeyword != null && endKeyword !=''){
            
            ptn = ptn + '(?=' + endKeyword + ')';
        }
        
        String ret = substringByRegex( src, ptn);
        
        if(ret != null) ret = ret.trim();
        
        if(ret!=null && ret!='') {
            return ret;
        } else {
            return null;
        }
    }
    
    public static String substringByRegex( String src, String regexp){
    
        String ret = '';
        try{
            
            Pattern ptn = Pattern.compile(regexp);
            Matcher matcher = ptn.matcher(src);
        
            if( matcher.find() ) {
                ret = matcher.group( );
            }
            
        } catch(Exception e) {
        
            System.debug(e);
        }
        
        return ret ;
    
    }
    
}