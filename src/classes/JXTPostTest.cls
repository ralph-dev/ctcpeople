@isTest
private class JXTPostTest {

	// Test for JXT Application Form Method
	// Modified by Lina
	testMethod static void testJxtEmailServiceDisabled(){
		System.runAs(DummyRecordCreator.platformUser) {
			StyleCategory__c sc = new StyleCategory__c();
			sc = StyleCategoryDummyRecordCreator.setJobboardStyleCategory();	
			Placement__c v = new Placement__c();
			insert v;
			Advertisement__c ad = AdvertisementDummyRecordCreator.generateJXTAdDummyRecord();
			ad.Vacancy__c=v.Id; 
			ad.Application_Form_Style__c = sc.Id;
			insert ad;
			DummyRecordCreator.createSkillGroup();
			
			
		    JobPostingSettings__c setting = new JobPostingSettings__c();
		    setting.SetupOwnerId = UserInfo.getUserId();
		    setting.Enable_JXT_Email_Service__c = false;
		    setting.Default_Email_Service_Address__c = 'test@test.com';
            insert setting;		    
            
		    JXTPost jp = new JXTPost(new ApexPages.StandardController(ad));
		    system.assertEquals(jp.getReferralItems().size(),2);  
		    List<SelectOption> JXTgetStyleOptions = jp.getStyleOptions();
		    system.assert(JXTgetStyleOptions.size()>0);
		    List<SelectOption> JXTgetTemplateOptions = jp.getTemplateOptions();
		    system.assert(JXTgetTemplateOptions.size()>0);
		    List<SelectOption> JXTgetSelectSkillGroupList = jp.getSelectSkillGroupList();
		    system.assert(JXTgetSelectSkillGroupList.size()>0);
		    PageReference savepage = jp.saveAd();
		    system.assert(savepage == null);  
		    PageReference testpage = jp.feedXml();
		    system.assert(testpage == null);
		    system.assertEquals('Active', jp.newAd.Status__c);
		    system.assertEquals('In Queue', jp.newAd.Job_Posting_Status__c);
		}
	}
	
	// Test for JXT Email Service Method
	// Add by Lina
	static testmethod void testJxtEmailServiceEnabled() {
		System.runAs(DummyRecordCreator.platformUser) {
		    //create data
		    Placement__c v = new Placement__c();
			insert v;
			Advertisement__c ad = AdvertisementDummyRecordCreator.generateJXTAdDummyRecord();
			ad.Vacancy__c=v.id; 
			ad.Skill_Group_Criteria__c = 'G00001;G00002';
			insert ad;

		
		    JobPostingSettings__c setting = new JobPostingSettings__c();
		    setting.SetupOwnerId = UserInfo.getUserId();
		    setting.Enable_JXT_Email_Service__c = true;
		    setting.Default_Email_Service_Address__c = 'test@test.com';
            insert setting;
		
		    JXTPost jp = new JXTPost(new ApexPages.StandardController(ad));
		    jp.saveAd();
		    system.assertEquals(jp.getReferralItems().size(),2); 
		    system.assertEquals(true, jp.enableEmailService);
		    system.assertEquals('test@test.com', jp.applicationEmail);
		
		    jp.applicationEmail = 'applicationEmail@test.com';
		    List<SelectOption> JXTgetTemplateOptions = jp.getTemplateOptions();
		    system.assert(JXTgetTemplateOptions.size()>0);
		    PageReference savepage = jp.saveAd();
		    system.assert(savepage == null);  
		    jp.feedXml();
		    system.assertEquals('Active', jp.newAd.Status__c);
		    system.assertEquals('In Queue', jp.newAd.Job_Posting_Status__c);
		}
	}

}