/**

AWSKeys is a class to retrieve Amazon S3 account info by
given configure file name and server name. 

Called by S3AdminController.cls

**/

public class AWSKeys {       
    public static String getRandomString(String str, Datetime now){
        Integer i,j;
        String tokenstr = str +'_' +now.year()+now.month()+now.day()+now.hour()+ now.minute()+ now.second();
        return tokenstr ;
    }

    public static String getUserS3Folder(){
    
        //User u =[select Id,AmazonS3_Folder__c from User where Id=:UserInfo.getUserId()]; 
        UserInformationCon usercls = new UserInformationCon();
        User u =   usercls.getUserDetail();
        if(u.AmazonS3_Folder__c != null && u.AmazonS3_Folder__c != '')
        {
            return u.AmazonS3_Folder__c.trim();
        }
        return null;
    
    }

    //******************testmethods***************************************

    public static testMethod void test1() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		String randomString = AWSKeys.getRandomString('ab', Datetime.now());
	        AWSKeys.getUserS3Folder();
	        system.assert(randomString.contains('_'));
    	}
        
        
    }
}