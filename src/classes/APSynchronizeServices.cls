public with sharing class APSynchronizeServices {
	public void synchroniseCompanyEntity(){
		CTCPeopleSettingHelperServices ctcServices=new CTCPeopleSettingHelperServices();
		CTCPeopleSettingHelperSelector ctcSelector=new CTCPeopleSettingHelperSelector();
		list<CTCPeopleSettingHelper__c> companyEntities=ctcServices.pullCompanyEntities();
		String rTID=ctcServices.getRecordTypeId('Company_Entity');
		list<CTCPeopleSettingHelper__c> originalCompanyEntities=ctcSelector.fetchCTCPeopleHelperSettingByRT(rTID);
		list<CTCPeopleSettingHelper__c> updatedList=new list<CTCPeopleSettingHelper__c>();
		for(CTCPeopleSettingHelper__c singleSettingHelper: originalCompanyEntities){
			singleSettingHelper.isActive__c=false;
			updatedList.add(singleSettingHelper);
		}
		checkFLS();
		update updatedList;
		upsert companyEntities  Company_Entity_ID__c;
		
		
	}
	public void synchroniseRuleGroups(){
		CTCPeopleSettingHelperServices ctcServices=new CTCPeopleSettingHelperServices();
		CTCPeopleSettingHelperSelector ctcSelector=new CTCPeopleSettingHelperSelector();
		list<CTCPeopleSettingHelper__c> ruleGroups=ctcServices.pullRuleGroups();
		String rTID=ctcServices.getRecordTypeId('Rule_Group');
		list<CTCPeopleSettingHelper__c> originalRuleGroups=ctcSelector.fetchCTCPeopleHelperSettingByRT(rTID);
		list<CTCPeopleSettingHelper__c> updatedList=new list<CTCPeopleSettingHelper__c>();
		for(CTCPeopleSettingHelper__c singleSettingHelper: originalRuleGroups){
			singleSettingHelper.isActive__c=false;
			updatedList.add(singleSettingHelper);
		}
		checkFLS();
		update updatedList;
		upsert ruleGroups Rule_Group_ID__c;
		
		
	}
	public void synchroniseRateCards(){
		CTCPeopleSettingHelperServices ctcServices=new CTCPeopleSettingHelperServices();
		CTCPeopleSettingHelperSelector ctcSelector=new CTCPeopleSettingHelperSelector();
		list<CTCPeopleSettingHelper__c> rateCards=ctcServices.pullRateCards();
		String rTID=ctcServices.getRecordTypeId('Rate_Card');
		list<CTCPeopleSettingHelper__c> originalRateCards=ctcSelector.fetchCTCPeopleHelperSettingByRT(rTID);
		list<CTCPeopleSettingHelper__c> updatedList=new list<CTCPeopleSettingHelper__c>();
		for(CTCPeopleSettingHelper__c singleSettingHelper: originalRateCards){
			singleSettingHelper.isActive__c=false;
			updatedList.add(singleSettingHelper);
		}
		checkFLS();
		update updatedList;
		upsert rateCards Rate_Card_ID__c;	
	}
	
	public void synchroniseOccupLib(){
		CTCPeopleSettingHelperServices ctcServices=new CTCPeopleSettingHelperServices();
		CTCPeopleSettingHelperSelector ctcSelector=new CTCPeopleSettingHelperSelector();
		list<CTCPeopleSettingHelper__c> occupLibs=ctcServices.pullOccupLibs();
		String rTID=ctcServices.getRecordTypeId('Occupation_Libraries');
		list<CTCPeopleSettingHelper__c> originalRateCards=ctcSelector.fetchCTCPeopleHelperSettingByRT(rTID);
		list<CTCPeopleSettingHelper__c> updatedList=new list<CTCPeopleSettingHelper__c>();
		for(CTCPeopleSettingHelper__c singleSettingHelper: originalRateCards){
			singleSettingHelper.isActive__c=false;
			updatedList.add(singleSettingHelper);
		}
	    checkFLS();
		update updatedList;
		upsert occupLibs Occupation_Libraries_Id__c;		
	}

	private void checkFLS(){
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			CTCPeopleSettingHelper__c.IsActive__c,
			CTCPeopleSettingHelper__c.Occupation_Libraries_Id__c,
			CTCPeopleSettingHelper__c.Rate_Card_ID__c,
			CTCPeopleSettingHelper__c.Rule_Group_ID__c,
			CTCPeopleSettingHelper__c.Company_Entity_ID__c
		};
		fflib_SecurityUtils.checkInsert(CTCPeopleSettingHelper__c.SObjectType, fieldList);
		fflib_SecurityUtils.checkUpdate(CTCPeopleSettingHelper__c.SObjectType, fieldList);
	}

}