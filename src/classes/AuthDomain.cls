/*
 *  Description: This is a domain class for authentication for all internal or external applications used
 *  
 *  Author: Kevin Wu
 *  Date: 21.04.2015
 */
public with sharing class AuthDomain {

  private final String AUTH_SERVICE_URL = Endpoint.getOauthEndpoint() + '/CTCAuth/auth/authService';

  public AuthDomain() {
    
  }

  // authenticate app, generate OAuth detail by redirect user to external Java program
  public String authApp(String instanceUrl, String appId, String returnUrl){
    //system.debug('authApp,appId=' + appId);
    //system.debug('authApp,instanceUrl=' + instanceUrl);
    //system.debug('authApp,returnUrl=' + returnUrl);
    String authUrl = authUrlConstructor(AUTH_SERVICE_URL, instanceUrl, appId, returnUrl);
    return authUrl;
  }

  // Construct url 
  private String authUrlConstructor(String serviceUrl, String instanceUrl, String appId, String returnUrl){
    String url = serviceUrl + 
          '?app_id=' + EncodingUtil.urlEncode(appId, 'UTF-8') + 
          '&instance_url=' + EncodingUtil.urlEncode(instanceUrl, 'UTF-8') + 
          '&return_url=' + EncodingUtil.urlEncode(returnUrl, 'UTF-8') +
          '&org_id=' + EncodingUtil.urlEncode(UserInfo.getOrganizationId(), 'UTF-8'); 
    return url;
  }
}