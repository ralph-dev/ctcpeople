public virtual class SelectorBase {	
	private Schema.SObjectType type;
	private Boolean enforceFieldLevelAccess;
	
	public SelectorBase(Schema.SObjectType t, Boolean enforceFieldLevelAccess){
		type = t;
		this.enforceFieldLevelAccess = enforceFieldLevelAccess;
	}
    
    public Schema.SObjectType getType(){
    	return type;
    }
    
    public virtual LIST<Schema.SObjectField> getSObjectFieldList(){
    	return type.getdescribe().fields.getmap().values();
    }
    
    public virtual List<SObject> selectByIds(Set<Id> ids){
    	SelectorQueryFactory qf = newQueryFactory();
    	qf.setCondition('Id IN :ids');
		fflib_SecurityUtils.checkRead(type,getSObjectFieldList());
    	return Database.query(qf.toString());
    }
    
    protected SelectorQueryFactory newQueryFactory(){
    	SelectorQueryFactory qf = new SelectorQueryFactory(type, enforceFieldLevelAccess);
    	qf.selectFields(getSObjectFieldList());
    	return qf;
    }
}