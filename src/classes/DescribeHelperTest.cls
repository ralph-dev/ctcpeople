@isTest
public class DescribeHelperTest {

    static testMethod void getDescribeFieldResult_success(){
    	System.runAs(DummyRecordCreator.platformUser) {
        Map<String, Schema.DescribeFieldResult> result = DescribeHelper.getDescribeFieldResult(Contact.sObjectType.getDescribe().fields.getMap().values());
        system.assert(result.get('Name') != null);
        system.assert(result.get('Phone') != null);
        system.debug('result ='+ result.get('Name'));
    	}
    }
    
    static testMethod void hasValueInPicklist_success(){
    	System.runAs(DummyRecordCreator.platformUser) {
        system.assert(DescribeHelper.hasValueInPicklist('Any', Contact.Desired_Salary__c));
    	}
    
	}

	static testMethod void getDescribeSelectFieldResult() {
		System.runAs(DummyRecordCreator.platformUser) {
		List<String> apiNameList = new List<String>{'Name','Id'};
		Map<String, Schema.DescribeFieldResult> result = DescribeHelper.getDescribeSelectFieldResult(Contact.sObjectType.getDescribe().fields.getMap().values(), apiNameList);
        system.assertEquals(result.size(),2);
		}
	}
}