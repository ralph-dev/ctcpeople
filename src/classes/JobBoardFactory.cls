/**
 * This is the Factory class for generating concrete JobBoard
 *
 * Created by: Lina Wei
 * Created on: 01/03/2017
 */

public with sharing class JobBoardFactory {

    public static string seekAdRecordTypeId;

    static {
        seekAdRecordTypeId = DaoRecordType.seekAdRT.Id;
    }

    /**
     * This method will return concrete JobBoard object based on the ad record type
     * This factory method is mainly used by controllers for posting, editing, archiving pages for job posting
     * @param: ad             Advertisement record
     * @return specific JobBoard
     */
    public static JobBoard getJobBoard(Advertisement__c ad){
        if (String.isEmpty(ad.RecordTypeId)) {
            return null;
        }
        if (ad.RecordTypeId == seekAdRecordTypeId) {
            return new JobBoardSeek(ad); 
        }
        return null;
    }

    /**
     * This method will return concrete JobBoard object based on the ad record type
     * This factory method is mainly used by job posting extensions
     * and other functions that do not need to specify ad record
     * @param: ad             Advertisement record type id
     * @return specific JobBoard
     */
    public static JobBoard getJobBoard(String recordTypeId){
        if (String.isEmpty(recordTypeId)) {
            return null;
        }
        if (recordTypeId == seekAdRecordTypeId) {
            return new JobBoardSeek();
        }
        return null;
    }

}