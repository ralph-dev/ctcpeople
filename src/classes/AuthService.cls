/*
 *  Description: This is a service class for authentication for all internal or external applications used
 *  
 *  Date: 21.04.2015
 */
public with sharing class AuthService {
  public AuthService() {
    
  }

  // Authenticate Salesforce connected app by calling the external Java program 
  // to generate OAuth detail including refresh token and access token
  public static String authSFWebConnectorApp(){
    String instanceUrl = System.URL.getSalesforceBaseUrl().toExternalForm();
    String appId = SFOauthTokenSelector.querySFOauthToken('oAuthToken').App_Id__c;
    String returnUrl = instanceUrl + '/apex/AppsAuth';

    AuthDomain authDomain = new AuthDomain();
    String orgId = UserInfo.getOrganizationId();
    system.debug('Org id: ' + orgId);
    String authUrl = authDomain.authApp(instanceUrl,appId,returnUrl);
   // system.debug('authSFWebConnectorApp,authUrl=' + authUrl);
    return authUrl;
  }
  
}