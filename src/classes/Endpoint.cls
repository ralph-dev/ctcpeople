public with sharing class Endpoint {
	public static final String PEOPLECLOUDWEBSITE = 'https://www.clicktocloud.com';
	
	public static final String PEOPLECLOUDSEARCH = 'https://search.clicktocloud.com';
	public static final String CPEWSDEFAULTENDPOINT = 'https://cpews.clicktocloud.com';
	public static final String OAUTH_DEFAULT_ENDPOINT = 'https://cpews.clicktocloud.com';
    public static final String OAUTH_SANDBOX_DEFAULT_ENDPOINT = 'https://testament.clicktocloud.com';
	public static final String ASTUTEDEFAULTENDPOINT = 'https://cpews.clicktocloud.com';
	public static final String PEOPLESEARCHDEFAULTENDPOINT = 'https://peoplesearch.clicktocloud.com';
	public static final String BULKEMAILDEFAULTENDPOINT = 'https://ws.clicktocloud.com';
	  
	public static String getPeoplecloudWebSite(){
		
		String webServiceDomain = Endpoint.PEOPLECLOUDWEBSITE;
		GSA__c peopleCloudEndPoint = GSA__c.getValues('peoplecloudEndpoint');
        if(peopleCloudEndPoint!=null && peopleCloudEndPoint.name__c!=null && peopleCloudEndPoint.name__c!=''){
            webServiceDomain = peopleCloudEndPoint.name__c;
        }
        return webServiceDomain;  
	}
	
	public static String getcpewsEndpoint() {
	    String cpewsEndpoint = Endpoint.CPEWSDEFAULTENDPOINT;
	    Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('cpews');
	    if (customEndpoint != null && customEndpoint.Endpoint__c != null && customEndpoint.Endpoint__c != '') {
	        cpewsEndpoint = customEndpoint.Endpoint__c;
	    }
	    return cpewsEndpoint;
	}

	public static String getOauthEndpoint() {
        String oauthEndpoint = OauthSettings.isSandbox() ? OAUTH_SANDBOX_DEFAULT_ENDPOINT : OAUTH_DEFAULT_ENDPOINT;
        Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('oauth');
        if (customEndpoint != null && customEndpoint.Endpoint__c != null && customEndpoint.Endpoint__c != '') {
            oauthEndpoint = customEndpoint.Endpoint__c;
        }
        return oauthEndpoint;
    }
	
	public static String getAstuteEndpoint() {
        String astuteEndpoint = Endpoint.ASTUTEDEFAULTENDPOINT;
        Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('astute');
        if (customEndpoint != null && customEndpoint.Endpoint__c != null && customEndpoint.Endpoint__c != '') {
            astuteEndpoint = customEndpoint.Endpoint__c;
        }
        return astuteEndpoint;
    }
    
    public static String getPeopleSearchEndpoint() {
        String peopleSearchEndpoint = Endpoint.PEOPLESEARCHDEFAULTENDPOINT;
        Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('peoplesearch');
        if (customEndpoint != null && customEndpoint.Endpoint__c != null && customEndpoint.Endpoint__c != '') {
            peopleSearchEndpoint = customEndpoint.Endpoint__c;
        }
        return peopleSearchEndpoint;
    }
	   
    public static String getBulkemailEndpoint() {
        String bulkemailEndpoint = Endpoint.BULKEMAILDEFAULTENDPOINT;
        Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('bulkemail');
        if (customEndpoint != null && customEndpoint.Endpoint__c != null && customEndpoint.Endpoint__c != '') {
            bulkemailEndpoint = customEndpoint.Endpoint__c;
        }
        return bulkemailEndpoint;
    }     

    public static String getJXTDefaultEndpoint() {
        String jxtWebservice = Endpoint.CPEWSDEFAULTENDPOINT;
        Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('jxtWebservice');
        if (customEndpoint != null && customEndpoint.Endpoint__c != null && customEndpoint.Endpoint__c != '') {
            jxtWebservice = customEndpoint.Endpoint__c;
        }
        return jxtWebservice;
    }

    public static String getSeekDefaultEndpoint() {
        String seekWebservice = Endpoint.CPEWSDEFAULTENDPOINT;
        Web_Service_Endpoint__c customEndpoint = Web_Service_Endpoint__c.getValues('seekWebservice');
        if (customEndpoint != null && customEndpoint.Endpoint__c != null && customEndpoint.Endpoint__c != '') {
            seekWebservice = customEndpoint.Endpoint__c;
        }
        return seekWebservice;
    }
     
}