/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=true)
private class TestTriggerDetail {

    static testMethod void testTriggerDetail() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		
    	Account candPool = PeopleCloudHelper.createCandidatePool();
    	RecordType candRT = DaoRecordType.indCandidateRT;
        String triggerAdminContent = '<fileroot><trigger><apiname>UpdateAccountField</apiname><targetObject>Contact</targetObject><status>InActive</status><accountid>'+candPool.Id+'</accountid><recordtypeid>'+candRT.Id+'</recordtypeid><note>This trigger is used to enable automation of updating client/account of people.<br/> If a person has not a client related to such as independent candidate, this trigger will automatically assign a client to it. </note></trigger><trigger><apiname>UpdateRemining</apiname><targetObject>Placement_Candidate__c</targetObject><status>Active</status><accountid/><recordtypeid/><note>This trigger is used to enable a reminder which pops up an alert once a candidate placed. </note></trigger></fileroot>';
        
        Document triggerAdminConfigFile = PeopleCloudHelper.triggerAdminConfigFile;
        triggerAdminConfigFile.Body = Blob.valueOf(Encodingutil.base64Encode(Blob.valueOf(triggerAdminContent)));
        update triggerAdminConfigFile;
        TriggerDetail thecontroller = new TriggerDetail();
	    thecontroller.getDetails();
	    thecontroller.getTriggerDetails();
	    try{
		    system.assertEquals(candPool.Id,thecontroller.getAccountid('UpdateAccountField'));
		    system.assertEquals(candRT.Id,thecontroller.getRecordTypeId('UpdateAccountField'));
		    system.assertEquals(false,thecontroller.IsActive('UpdateAccountField'));
		    
		    system.assertEquals('',thecontroller.getAccountid('UpdateRemining'));
		    system.assertEquals('',thecontroller.getRecordTypeId('UpdateRemining'));
		    system.assertEquals(true,thecontroller.IsActive('UpdateRemining'));
	    }
	    catch(Exception ex){
	    	system.debug(ex);
	    
	    }
    	}
    }
}