public with sharing class ProtectedAccount {
	
	public SObject account;
	public ProtectedCredential credential;
	
	public String fieldNames; //splitted with comma
	
	public ProtectedAccount(SObject account, ProtectedCredential credential){
		this.account = account;
		this.credential  = credential;
	}
	
	public ProtectedAccount(SObject account, ProtectedCredential credential, String fieldNames){
		this.account = account;
		this.credential  = credential;
		this.fieldNames = fieldNames;
	}
	
	 public Object getAccountFieldValue(String name){
    	if( account != null){
    		return account.get(name);
    	}
    	
    	return null;
    }
    
     public String getUsername(){
    	if(this.credential != null){
    		return this.credential.username;
    	}
    	
    	return null;
    }
    
     public String getPassword(){
    	if(this.credential != null){
    		return this.credential.password;
    	}
    	
    	return null;
    }
    
     public String getToken(){
    	if(this.credential != null){
    		return this.credential.token;
    	}
    	
    	return null;
    }
    
    public String getSecret(){
    	if(this.credential != null){
    		return this.credential.secret;
    	}
    	
    	return null;
    }
    
   
    
    public String getKey(){
    	if(this.credential != null){
    		return this.credential.key;
    	}
    	
    	return null;
    }
	
	/*
    public ProtectedAccount setAccount(SObject account){
    	this.account = account;
    	return this;
    }
    
   
    
    public ProtectedAccount setUsername(String username){
    	if(this.credential == null){
    		this.credential  = new ProtectedCredential();
    	}
    	
    	this.credential.username = username;
    	
    	return this;
    }
   
    public ProtectedAccount setToken(String token){
    	if(this.credential == null){
    		this.credential  = new ProtectedCredential();
    	}
    	
    	this.credential.token = token;
    	
    	return this;
    }
    
    
    public ProtectedAccount setSecret(String secret){
    	if(this.credential == null){
    		this.credential  = new ProtectedCredential();
    	}
    	
    	this.credential.secret = secret;
    	
    	return this;
    }
    
    
    
    public ProtectedAccount setPassword(String password){
    	if(this.credential == null){
    		this.credential  = new ProtectedCredential();
    	}
    	
    	this.credential.password = password;
    	
    	return this;
    }
    
    
    
    public ProtectedAccount setKey(String key){
    	if(this.credential == null){
    		this.credential  = new ProtectedCredential();
    	}
    	
    	this.credential.key = key;
    	
    	return this;
    }
    */
    
}