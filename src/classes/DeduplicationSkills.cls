public class DeduplicationSkills {  
    
    public static List<String> duplicateCSCIdList;//return the duplicate ID list
    
    public static List<String> getduplicateCSCIdList(List<Candidate_Skill__c> templist){
        Map<String, String> CandidateSkillMap = new Map<String, String>();  //Map structure: <CandidateId:SkillId, Candidate_Skill__cId>
        Map<String, String> OldCandidateSkillMap = new Map<String, String>();
        Set<String> CandidateListString = new Set<String>();
        duplicateCSCIdList = new List<String>();    
        
        CandidateSkillMap = fetchcandidateskillmap(templist, true);// Fetch Trigger.new Map
        CandidateListString = fetchcandidateslist(templist); // Fetch Candidate Set 
        
        if(!CandidateSkillMap.isEmpty() && CandidateListString.size()>0){ //Fetch the Candidate Skill records according Candidate Id.
            List<Candidate_Skill__c> tempcsc = new List<Candidate_skill__c>();
            try{
            	
            	CommonSelector.checkRead(Candidate_Skill__c.sObjectType, 'id, Skill__c, Candidate__c' );
                tempcsc = [select id, Skill__c, Candidate__c from Candidate_Skill__c where Candidate__c in: CandidateListString and id not in: templist];
            }
            catch(Exception e){
                NotificationClass.notifyErr2Dev('duplicateCSCIdList/getduplicateCSCIdList/SOQL', e);
                return null;
            }
            OldCandidateSkillMap = DeduplicationSkills.fetchcandidateskillmap(tempcsc, false);
        }
        
        for(String candidateskillstring: CandidateSkillMap.keySet()){//Loop Trigger.new CandidateSkill map
            
            //if Trigger.new CandidateId+SkillId key exit, check the candidate skill id isequal.
            // As after insert trigger the new insert record will be in the old skill list.
            if(OldCandidateSkillMap.containsKey(candidateskillstring)){//&& OldCandidateSkillMap.get(candidateskillstring)!= CandidateSkillMap.get(candidateskillstring)
                duplicateCSCIdList.add(CandidateSkillMap.get(candidateskillstring));
            }
        }
        return duplicateCSCIdList;
    }
    
    //Delete the candidate Duplicate Skill
    public static void deleteDuplicateCandidateSkill(List<String> deleteDuplcateCandidateSkillList){
    	CommonSelector.checkRead(Candidate_Skill__c.sObjectType, 'id');
        List<Candidate_Skill__c> deletelist = [select id from Candidate_Skill__c where id in: deleteDuplcateCandidateSkillList];
        fflib_SecurityUtils.checkObjectIsDeletable(Candidate_Skill__c.SObjectType);
        delete deletelist;
    }   
        
    //Fetch insert Candidate Skll record list
    private static Map<String, String> fetchcandidateskillmap(List<Candidate_Skill__c> tempcsc , boolean addtoDeleteList){
        try{
            Map<String, String> insertcandidateskillmap = new Map<String, String>();
            for(Candidate_Skill__c candidateskill : tempcsc){
                String insertcontact = candidateskill.Candidate__c;
                String insertcontactskill  = candidateskill.Skill__c;
                String insertcandidateskillstring = candidateskill.Candidate__c+':'+candidateskill.Skill__c;    
                
                if(insertcandidateskillmap.containsKey(insertcandidateskillstring)){
                    if(addtoDeleteList)
                    duplicateCSCIdList.add(candidateskill.id);
                }
                else{
                    insertcandidateskillmap.put(insertcandidateskillstring , candidateskill.id);    
                }           
            }
            return insertcandidateskillmap;
        }
        catch(Exception e){
            NotificationClass.notifyErr2Dev('duplicateCSCIdList/fetchcandidateskillmap', e);
            return null;
        }
    }
    
    //Fetch insert Candidate Skill Record candidate list
    
    private static Set<String> fetchcandidateslist(List<Candidate_Skill__c> tempcsc){
        try{
            Set<String> insertCSCCandidateList = new Set<String>();
            for(Candidate_Skill__c candidateskill : tempcsc){
                String insertcontact = candidateskill.Candidate__c;             
                insertCSCCandidateList.add(insertcontact);              
            }
            return insertCSCCandidateList;
        }
        catch(Exception e){
            NotificationClass.notifyErr2Dev('duplicateCSCIdList/fetchcandidateslist', e);
            return null;
        }
    }   
    
    public static boolean updateCandidateLastModifyDate(List<Candidate_Skill__c> tempcsc){
        Set<String> CandidateIdList = fetchcandidateslist(tempcsc);
        CommonSelector.checkRead(Contact.sObjectType,'Id, LastModifiedDate');
        List<Contact> ContactList = [select Id, LastModifiedDate from Contact where id in: CandidateIdList];
        try{
            update ContactList;
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
}