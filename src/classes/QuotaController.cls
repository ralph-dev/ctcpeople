public with sharing class QuotaController{
    Public User[] AllUsers {get;set;}
    public Integer UserNum_Seek {get;set;}
    public Integer UserNum_C1 {get;set;}
    String thenamespace = PeopleCloudHelper.getPackageNamespace();
    User currentuser;
    public StyleCategory__c[] companyQuota {get;set;}
    public boolean show_page {get;set;}
    public Integer SeekUsage{get;set;}
    public Integer C1Usage{get;set;}
    public boolean showSeek{get;set;}
    public boolean showC1{get;set;}

    public Integer seekstd {get;set;}
    public Integer c1std {get;set;}

    public QuotaController(){
        SeekUsage =0;
        C1Usage=0;
        UserNum_Seek = 0;
        UserNum_C1 = 0;
        try{
            //checksites();
            showSeek = true;
            showC1 = true;
            currentuser = new UserSelector().getCurrentUser();
            if(currentuser.Manage_Quota__c)
            {
                List<String> sites = new String[]{};
                system.debug('showSeek ='+ showSeek);
                system.debug('showC1 ='+ showC1);
                if(showSeek)
                {
                    sites.add('Seek');
                }
                if(showC1)
                {
                    sites.add('CareerOne');
                }
                if(sites.size()>0)
                {
                    show_page = true;
                    CommonSelector.checkRead(StyleCategory__c.SObjectType,'Id, website__c,Monthly_Quota__c, Standard_Quota_Per_User__c');
                    companyQuota = [Select Id, website__c,Monthly_Quota__c, Standard_Quota_Per_User__c from StyleCategory__c 
                                    where Recordtype.DeveloperName = 'WebSite_Admin_Record'
                                    and WebSite__c in :sites limit 3];
                    
                    if(setQuotaRecord('Seek') != null && showSeek) //** existing job board account
                    {
                        Decimal thestdquota = ((StyleCategory__c)setQuotaRecord('Seek')).Standard_Quota_Per_User__c;
                        if(thestdquota != null)
                        {
                            seekstd = thestdquota.intValue();
                            system.debug('seekstd='+ seekstd);
                        }
                        
                    }
                    if(setQuotaRecord('CareerOne') != null && showC1)
                    {
                        Decimal thestdquota3 = ((StyleCategory__c)setQuotaRecord('CareerOne')).Standard_Quota_Per_User__c;
                        if(thestdquota3 != null)
                        {
                            c1std = (((StyleCategory__c)setQuotaRecord('CareerOne')).Standard_Quota_Per_User__c).intValue();
                            system.debug('CareerOne='+ c1std);
                        }
                        
                    }

                    getalluserData();
                    
                }
                else
                {
                    show_page = false;
                }
            
                
            }
            else
            {
                show_page = false;
            }
        
        }
        catch(System.Exception e)
        {
            system.debug(e);
        }
    
    
    }

    public String msg{get;set;}
    public String msg2{get;set;}
    public String msg3{get;set;}
    public void saveCompanyInfo(){
        msg = '';
        system.debug('error');
        try{
            system.debug('companyQuota =' +companyQuota);
            if(companyQuota != null)
            {
            //check FLS
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                StyleCategory__c.WebSite__c,
                StyleCategory__c.Monthly_Quota__c,
                StyleCategory__c.Standard_Quota_Per_User__c
            };
            fflib_SecurityUtils.checkUpdate(StyleCategory__c.SObjectType, fieldList);
            update companyQuota;
            msg = 'Saved Successfully!';
            }
        }
        catch(system.Exception e)
        {
        
            msg = 'Fail to update! Please contact your system admin';
            system.debug('exception='+e);
        }
        
    }
    public void refresh(){
    
        getalluserData();
    }
    public void getalluserData(){
            AllUsers = DaoUsers.getActiveUsers();
            
            for(User u: AllUsers)
            {
                if(u.JobBoards__c != null)
                {
                    if(u.JobBoards__c.contains('seek'))
                    {
                        UserNum_Seek += 1;
                    }
                    if(u.JobBoards__c.contains('careerone'))
                    {
                        UserNum_C1 += 1;
                    }
                }
                
            
            }
            
            SeekUsage= getTotalUsage(thenamespace +'Seek_Usage__c');
            C1Usage= getTotalUsage(thenamespace +'CareerOne_Usage__c');
    
    
    }
    public void saveUserInfo(){
    
        msg2 ='';
        try{
            if(AllUsers != null)
            {
            //check FLS
             List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                    User.Email,
                    User.JobBoards__c,
                    User.Monthly_Quota_Seek__c,
                    User.Seek_Usage__c,
                    User.Monthly_Quota_CareerOne__c,
                    User.CareerOne_Usage__c
            };
            fflib_SecurityUtils.checkUpdate(User.SObjectType, fieldList);
            update AllUsers; 
            msg2 ='Saved Successfully!';
            }
        }
        catch(system.exception e)
        {
            msg2 = 'Fail to update! Please contact your system admin';
        }
    }
    
    public Integer getTotalUsage(String fieldname){
        Integer thetotal = 0;
        String theusage;
        for(User u : AllUsers)
        {   theusage = (String)u.get(fieldname);
            if(theusage != null && theusage != '')
            { 
                thetotal = thetotal + Integer.valueOf(theusage);
            
            }
        
        
        }     
    
        return thetotal;
    }
    
    public void savestdquota(){
        StyleCategory__c[] toupdates = new StyleCategory__c[]{};
        try
        {
            if(companyQuota != null)
            {
                if(companyQuota.size() > 0)
                {
                    
                    if(seekstd != null)
                    {
                        StyleCategory__c s1 = setQuotaRecord('Seek');
                        if(s1 != null)
                        {
                            s1.Standard_Quota_Per_User__c = seekstd;
                            toupdates.add(s1);
                        }

                    }
                    if(c1std != null)
                    {
                        StyleCategory__c s3 = setQuotaRecord('CareerOne');
                        if(s3 != null)
                        {
                            s3.Standard_Quota_Per_User__c = c1std;
                            toupdates.add(s3);
                        }
                    }
                    if(toupdates.size() >0)
                    {
                        fflib_SecurityUtils.checkFieldIsUpdateable(StyleCategory__c.SObjectType, StyleCategory__c.Standard_Quota_Per_User__c);
                        update toupdates;
                        msg3 = 'Successfully Set up Standard Quota for each User!';
                    }
                }
            }
            
        }
        catch(system.exception e)
        {
        
            msg3 = 'Fail to submit! Please contact your system admin';
        }
    }
    public StyleCategory__c setQuotaRecord(String sitename){
        StyleCategory__c ss; 
        for(StyleCategory__c s : companyQuota)
        {
            if(s.WebSite__c == sitename)
            {
            
                ss = s;
                break;
            }
        }
        return ss;
    }
}