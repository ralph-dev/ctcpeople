/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestObjectTranslater {

    static testMethod void myUnitTest() {
    	 System.runAs(DummyRecordCreator.platformUser) {
        ObjectTranslater obj=new  ObjectTranslater();
        obj.Setobjectn('Contact');
        obj.setmaxcriterianumber(25);
         List<SelectOption> test_options_field_List=new List<SelectOption>();
        test_options_field_List.add(new SelectOption('','--SELECT Field--'));
        SObjectType objToken = Schema.getGlobalDescribe().get('Contact'); 
        DescribeSObjectResult objDef = objToken.getDescribe();     
        SObject con=objToken.newSObject();
        
        Map<String,String> map_type=new Map<String,String>();  
        Map<string, SObjectField> fields_all = GetfieldsMap.getFieldMap('Contact');       
        List<String> optionvalues=new List<String>();
        for (Schema.SObjectField f : fields_all.values()){
        	
        	DescribeFieldResult fdr;
        	try{
        	 fdr= f.getDescribe();
        	if(fdr.getName()!='LastReferencedDate'){	        
	            String apiname=fdr.getName();
	            String labelname=fdr.getLabel();
	            //The selectOption will not take the TEXTAREA type field.
	            if(fdr.gettype().name()!='TEXTAREA'){
	            	String optionvalue=labelname+'};'+apiname;
	            	optionvalues.add(optionvalue);	           			                                 
	          }
        	         
        	}}catch(Exception e){
        		continue;
        	}            
        	}         
        
       optionvalues.sort();
       for(String va: optionvalues){
	       	try{
	       		String[] nameapilabel=va.split('};');
	       		String namelabel=nameapilabel[0];
	       		String nameapi=nameapilabel[1];
	       		test_options_field_List.add(new SelectOption(nameapi,namelabel));
	       	}catch(Exception ex){
	       		continue;
	       	}  
       }
        
        
        
        System.assertEquals(obj.options_field_List,test_options_field_List);
    	}
    }
}