@isTest
public class GetContactJsonTest {
    
    private static testMethod void testGenerateSearchCriterial() {
    	System.runAs(DummyRecordCreator.platformUser) {
        List<Account> tempaccount = DaoAccount.getAccsContainNameSR('test');
		List<Contact> tempcontact = DaoContact.getClientContactsbyNameSR('test');
        
        System.assertEquals(null, GetContactJson.generateSearchCriterial('test', tempaccount, tempcontact));
        
        DataTestFactory.createAccountList();
        System.assertNotEquals('and Account.id in: tempaccount', GetContactJson.generateSearchCriterial('test', tempaccount, tempcontact));
        
        List<Contact> testContacts = DataTestFactory.createContacts();
        System.debug(testContacts);
        System.debug(GetContactJson.generateSearchCriterial('testFirstName', tempaccount, tempcontact));
    	}
    }
    
}