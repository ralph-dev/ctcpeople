@isTest
private class DaoAttachmentTest {

	private static testMethod void test() {
		System.runAs(DummyRecordCreator.platformUser) {
	    system.assertNotEquals(null, DaoAttachment.getAttachmentsByEmailTemplateId('a049000000u5iOJ'));
        system.assertEquals(null, DaoAttachment.getAttachmentIdsByEmailTemplateId('test'));
        system.assertNotEquals(null, DaoAttachment.getAttachmentsDetailByAttachment(new List<Attachment>{new Attachment()}));
		}
	}

}