/**
 * This is a customise error info class
**/
public with sharing class PeopleCloudCustomErrorInfo {
    
    public Boolean successStatus = false;
    public String msg = '';

}