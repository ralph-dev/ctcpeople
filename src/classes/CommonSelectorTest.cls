@isTest
private class CommonSelectorTest {

	private static testMethod void testGet() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	        
	        CommonSelector selector = new CommonSelector(StyleCategory__c.SObjectType);
	    
    	    StyleCategory__c s = new StyleCategory__c();
    	    s.name = 'test';
    	    selector.doInsert( s );
    	    
    	    List<StyleCategory__c> ss = selector.get(' id, name ',' name=\'test\' ');
    	    
    	    System.assertEquals(1, ss.size());
    	    
    	    ss = selector.get(' id, name ','');
    	    
    	    System.assertEquals(1, ss.size());
    	    
    	    
    	    
    	    List<Schema.SObjectField> fs = new List<Schema.SObjectField>{StyleCategory__c.Id, StyleCategory__c.Name};
    	    
    	    ss = selector.get(fs,' name=\'test\' ');
    	    
    	    System.assertEquals(1, ss.size());
    	    
    	    ss = selector.get(fs,'');
    	    
    	    System.assertEquals(1, ss.size());
    	    
    	    
    	    
    	    //ss = selector.get('*',' name=\'test\' ');
    	    //System.assertEquals(1, ss.size());
    	    
    	    
    	    System.assertEquals(true,selector.validateFieldList(new List<String>{'Id','Name'}));
    	    
    	    System.assertEquals(false,selector.validateFieldList(new List<String>{'Id','Ffffffff'}));
	        
	    }
	}
	
	private static testMethod void testGetNotNullFieldNamesWithComma() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	    	CommonSelector selector = new CommonSelector(StyleCategory__c.SObjectType);
	    	
	    	List<StyleCategory__c> scs = new List<StyleCategory__c>();
	    	
	    	StyleCategory__c sc1 = new StyleCategory__c();
	    	sc1.Name ='test 1';
	    	sc1.ProviderCode__c = 'pc001';
	    	scs.add(sc1);
	    	
	    	StyleCategory__c sc2 = new StyleCategory__c();
	    	sc2.Name ='test 2';
	    	sc2.OfficeCode__c = 'oc001';
	    	scs.add(sc2);
	    	
	    	
	    	String names = selector.getNotNullFieldNamesWithComma(scs);
	    	
	    	System.debug(names);
	    	
	    	System.assertNotEquals(null, names);
	    	
	    	List<String> nameList = names.split(',');
	    	
	    	Set<String> nameSet = new Set<String>(nameList);
	    	
	    	System.assertEquals(3, nameSet.size());
	    	//System.assertEquals(true,nameSet.contains('name') && nameSet.contains('providercode__c') && nameSet.contains('officecode__c') );
	    }
	}
	
	private static testMethod void testDoInsertUpdateUpsert() {
	    
	    System.runAs(DummyRecordCreator.platformUser) {
	    	CommonSelector selector = new CommonSelector(StyleCategory__c.SObjectType);
	    	
	    	//do insert
	    	List<StyleCategory__c> scs = new List<StyleCategory__c>();
	    	
	    	StyleCategory__c sc1 = new StyleCategory__c();
	    	sc1.Name ='test 1';
	    	sc1.ProviderCode__c = 'pc001';
	    	scs.add(sc1);
	    	
	    	StyleCategory__c sc2 = new StyleCategory__c();
	    	sc2.Name ='test 2';
	    	sc2.OfficeCode__c = 'oc001';
	    	scs.add(sc2);
	    	
	    	
	    	Boolean s = CommonSelector.quickInsert(scs);
	    	
	    	System.assertEquals(true, s);
	    	
	    	List<StyleCategory__c> rets = selector.get('Id,Name,ProviderCode__c,OfficeCode__c', 'name=\'test 1\'');
	    	
	    	System.assertEquals(1, rets.size());
	    	System.assertEquals('pc001', rets[0].ProviderCode__c);
	    	System.assertEquals(null, rets[0].OfficeCode__c);
	    	
	    	rets = selector.get('Id,Name,ProviderCode__c,OfficeCode__c', 'name=\'test 2\'');
	    	
	    	
	    	//do update
	    	System.assertEquals(1, rets.size());
	    	System.assertEquals(null, rets[0].ProviderCode__c);
	    	System.assertEquals('oc001', rets[0].OfficeCode__c);
	    	
	    	rets[0].ProviderCode__c = 'pc002';
	    	s = CommonSelector.quickUpdate(rets);
	    	
	    	System.assertEquals(true, s);
	    	
	    	rets = selector.get('id,name,ProviderCode__c,OfficeCode__c', 'name=\'test 2\'');
	    	
	    	System.assertEquals(1, rets.size());
	    	System.assertEquals('pc002', rets[0].ProviderCode__c);
	    	System.assertEquals('oc001', rets[0].OfficeCode__c);
	    	
	    	
	    	//do upsert
	    	StyleCategory__c sc3 = new StyleCategory__c();
	    	sc3.Name ='test 3';
	    	sc3.ProviderCode__c = 'pc003';
	    	sc3.OfficeCode__c = 'oc003';
	    	
	    	s = CommonSelector.quickUpsert(sc3);
	    	
	    	System.assertEquals(true, s);
	    	
	    	rets = selector.get('id,name,ProviderCode__c,OfficeCode__c', 'name=\'test 3\'');
	    	
	    	System.assertEquals(1, rets.size());
	    	System.assertEquals('pc003', rets[0].ProviderCode__c);
	    	System.assertEquals('oc003', rets[0].OfficeCode__c);
	    	
	    	
	    	//rets = selector.get('*',2);
	    	//System.assertEquals(2, rets.size());
	    	
	    	//rets = selector.get('*','name like \'test%\'',3);
	    	//System.assertEquals(3, rets.size());
	    	
	    	rets = selector.get('ProviderCode__c,OfficeCode__c','',4);
	    	System.assertEquals(3, rets.size());
	    	
	    }
	}
	
	private static testMethod void testSubqueries(){
		
		System.runAs(DummyRecordCreator.platformUser) {
			//String sql = new CommonSelector(OAuth_Service__c.sObjectType)
	        //				.addSubselect(OAuth_Token__c.sObjectType,'Id,isAccess__c','Owner__c=:userId')
	        //				.toSOQL('Id,Request_Token_URL__c','name =\'test1234\'');
	        //System.debug(sql);
	        
	        //Database.query(sql);
	        
	       String userId = UserInfo.getUserId();
	       
	        CommonSelector selector = new CommonSelector(OAuth_Service__c.sObjectType);
	        
	        OAuth_Service__c os = new OAuth_Service__c();
	        os.name = 'testname';
	        os.Request_Token_URL__c = 'http://';
	        selector.doInsert(os);
	        List<OAuth_Service__c> oservices = new CommonSelector(OAuth_Service__c.sObjectType)
	        				.setParam('name','testname')
	        				.get('Id,Request_Token_URL__c','Request_Token_URL__c != null and  name =:name');
	        
	        System.assertEquals(1 , oservices.size());
	        
	        OAuth_Token__c ot = new OAuth_Token__c();
	        ot.Owner__c = userId;
	        ot.isAccess__c = true;
	        ot.OAuth_Service__c = os.Id;
	        
	        new CommonSelector(OAuth_Token__c.sObjectType).doInsert(ot);
	        
	        oservices = new CommonSelector(OAuth_Service__c.sObjectType)
	        				.addSubselect(OAuth_Token__c.sObjectType,'Id,isAccess__c','isAccess__c=:isAccess and Owner__c=: userId')
	        				.setParam('userId',userId)
	        				.setParam('name','testname')
	        				.setParam('isAccess',true)
	        				.get('Id,Request_Token_URL__c','Request_Token_URL__c != null and  name =:name');
	        				
	        System.assertEquals(1 , oservices.size());
	        
	        
	        
	        
	        List<SObject> otokens = new CommonSelector(OAuth_Token__c.sObjectType)
	        				.setParam('userId',userId)
	        				.get('OAuth_Service__r.Id, OAuth_Service__r.name, OAuth_Service__r.Access_Token_URL__c,Id, isAccess__c',
	        					'Owner__c=:userId  AND isAccess__c = true');
	        System.assertEquals(1,otokens.size());
	        
	        
	        
	        
	        String likename = '123%';
	        
	        System.debug(new CommonSelector(Contact.sObjectType).setParam('name',likename).get('account.type','name like :name'));
	        
	        RecordTypeInfo r = null;
	        
			//fflib_QueryFactory qf = new fflib_QueryFactory(RecordTypeInfo).selectFields(new String[]{'id'});
			//System.debug(qf.toSOQL());
				
				
				
	        
	        
	        //System.debug( new CommonSelector(Task.sObjectType)//.addSubselect(Attachment.sObjectType,'Id, Name','')
	        //    	.getOne(
	        //    		'Id, OwnerId, WhoId, WhatId,Who.name,Who.type, What.name,What.type',
	        //     		'Id =: id'));
		}
                          
                 
         
        
            		
	}
	
	private static  testMethod void testSimpleQueryFactory(){
		System.runAs(DummyRecordCreator.platformUser) {
			String userId = UserInfo.getUserId();
       
	        CommonSelector selector = new CommonSelector(OAuth_Service__c.sObjectType);
	        
	        OAuth_Service__c os = new OAuth_Service__c();
	        os.name = 'testname';
	        os.Request_Token_URL__c = 'http://';
	        selector.doInsert(os);
	        
	        List<OAuth_Service__c> oservices = selector.simpleQuery()
	        				.setParam('name','testname')
	        				.selectFields('Id,Request_Token_URL__c')
	        				.setCondition('Request_Token_URL__c != null and  name =:name')
	        				.get();
	        
	        System.assertEquals(1 , oservices.size());
	        
	        OAuth_Token__c ot = new OAuth_Token__c();
	        ot.Owner__c = userId;
	        ot.isAccess__c = true;
	        ot.OAuth_Service__c = os.Id;
	        
	        new CommonSelector(OAuth_Token__c.sObjectType).doInsert(ot);
	        
	        oservices = CommonSelector.simpleQuery(OAuth_Service__c.sObjectType )
	                        .addSubselect(OAuth_Token__c.sObjectType,'Id,isAccess__c','isAccess__c=:isAccess and Owner__c=: userId')
	                        .addOrdering('Request_Token_URL__c',commonSelector.SortOrder.DESCENDING)
	        				.setParam('userId',userId)
	        				.setParam('name','testname')
	        				.setParam('isAccess',true)
	        				.get('Id,Request_Token_URL__c','Request_Token_URL__c != null and  name =:name');
	        				
	        System.assertEquals(1 , oservices.size());
	        
	        
	        
	        
	        OAuth_Token__c otoken = (OAuth_Token__c) new CommonSelector(OAuth_Token__c.sObjectType)
	                        .simpleQuery()
	                        .selectFields('OAuth_Service__r.Id, OAuth_Service__r.name, OAuth_Service__r.Access_Token_URL__c,Id, isAccess__c')
	                        .setCondition('Owner__c=:userId  AND isAccess__c = true')
	        				.setParam('userId',userId)
	        				.getOne( );
	        System.assert(otoken != null);
		}
	    	   
	}

	private static testMethod void tesNotifySecurityException(){
		String msg = '[sf:deploy] 14.  ContactServicesTest.myUnitTest4 -- fflib_SecurityUtils.FlsException: You do not have permission to read the field Default_Skill_Groups__c on CTCPeopleSettingHelper__c';
		fflib_SecurityUtils.FlsException e = new fflib_SecurityUtils.FlsException(msg);	
		
		BasicSelectorException bse  = new BasicSelectorException(e);
		System.assertEquals(true,bse.getComponentName().contains('ContactServicesTest'));
		System.assertEquals('myUnitTest4',bse.getMethodName());
		System.assertEquals('read',bse.getOperation());
		System.assertEquals('Default_Skill_Groups__c',bse.getFieldName());
		System.assertEquals('CTCPeopleSettingHelper__c',bse.getObjectName());
		
		
		msg = '[sf:deploy] 14.  ContactServicesTest.myUnitTest4 -- fflib_SecurityUtils.FlsException: You do not have permission to insert field Default_Skill_Groups__c on CTCPeopleSettingHelper__c';
		e = new fflib_SecurityUtils.FlsException(msg);	
		
		bse  = new BasicSelectorException(e);
		System.assertEquals(true,bse.getComponentName().contains('ContactServicesTest'));
		System.assertEquals('myUnitTest4',bse.getMethodName());
		System.assertEquals('insert',bse.getOperation());
		System.assertEquals('Default_Skill_Groups__c',bse.getFieldName());
		System.assertEquals('CTCPeopleSettingHelper__c',bse.getObjectName());
		
		Boolean ret = NotificationClass.notifySecurityException(e);
		System.assertEquals(true, ret);
	
	}
	/**
	private static testMethod void testCheck(){
		
		System.runAs(DummyRecordCreator.platformUser) {
			//CommonSelector.checkRead(OAuth_Token__c.sObjectType,
			//	'OAuth_Service__r.Id, OAuth_Service__r.name, OAuth_Service__r.Access_Token_URL__c,Id, isAccess__c');
			
			Schema.SObjectField token = fflib_SObjectDescribe.getDescribe(Contact.sObjectType).getField('type');
			System.debug(token);
			
			//CommonSelector.checkRead(Task.sObjectType,'Id, OwnerId, WhoId, WhatId,Who.name, Who.Type,What.name');
			
			
			//CommonSelector.checkRead(Placement_Candidate__c.sObjectType, 'Candidate__r.Name, Candidate__c');
			
			//CommonSelector selector = new CommonSelector(Placement_Candidate__c.sObjectType );
			
			//fflib_queryfactory qf = selector.newQueryFactory();
			//qf.selectFields(CommonSelector.convertStringToList('Candidate__r.Name,Candidate__c'));
			
			//qf.toSOQL();
			
			//CommonSelector.checkRead(ContentDocumentLink.sObjectType,'id, LinkedEntityId, ContentDocumentId, shareType, ContentDocument.title');
			
		}

		
	}*/ 
	
	
	
	
}