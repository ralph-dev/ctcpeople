public with sharing class ClientExtensionGlobal {
	public ClientExtensionGlobal(Object obj){
    }
    
    @RemoteAction
    public static list<Account> searchClient(String keyword){
    	AccountSelector accSelector = new AccountSelector();
		List<Account> accList = accSelector.fetchAccountListByKeyword(keyword);
		//System.debug('ClientExtensionGlobal searchClient accList : '+JSON.serialize(accList));
		return accList;
    }
    
    @RemoteAction
    public static Account getClient(String Id){
    	AccountSelector accSelector = new AccountSelector();
    	List<Account> accList = accSelector.fetchAccountInfoById(Id);
    	if(accList == null || accList.size() == 0){
    		return null;
    	}
    	return accList[0];
    } 
}