/**

**/
@isTest
private class TestCandManagementTrigger {
    private static final Integer NUM_CANDIDATE_CREATE=3;
    
    private static void before() {
        TriggerHelper.disableAllTrigger();
        TriggerHelper.Deduplication = true;
    }

    static testmethod void testPopulateCompOnVCWTrigger(){
    	
    	configureSetting();
    	
    	System.runAs(DummyRecordCreator.platformUser) {
        
        /***************************************************
            Test insert candidate management without company
        ****************************************************/
        //insert companies
        List<Account> compList = new List<Account>();
        compList.add(new Account(Name='comp1'));
        compList.add(new Account(Name='comp2'));
        insert compList;
        
        //insert vacancies
        RecordType rt = [select Id from RecordType where DeveloperName = 'Contract_Vacancy' limit 1];
        Placement__c p1=new Placement__c(RecordTypeId= rt.Id,Company__c=compList.get(0).Id,Name='placement 1');
        Placement__c p2=new Placement__c(RecordTypeId= rt.Id,Company__c=compList.get(1).Id,Name='placement 2');
        insert p1;
        insert p2;
        
        //insert candidates
        RecordType candRT=[select Id from RecordType where SobjectType = 'Contact' limit 1];
        Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1');
        Contact cand2 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 2');
        Contact cand3 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 3');
        insert cand1;
        insert cand2;
        insert cand3;
        
        //create candiate management
        RecordType cmRT = [select Id from RecordType where DeveloperName = 'Company_Led_Placement_Candidate' limit 1];
        List<Placement_Candidate__c> batch1 = new List<Placement_Candidate__c>();
        List<String> idBatch1 = new List<String>();  
        Placement_Candidate__c cm1=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand1.Id);
        Placement_Candidate__c cm2=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand1.Id);
        Placement_Candidate__c cm3=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand1.Id);
        Placement_Candidate__c cm4=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand1.Id);
        
        //insert candidate management & 
        batch1.add(cm1);
        batch1.add(cm2);
        batch1.add(cm3);
        batch1.add(cm4);
        insert batch1;
        idBatch1.add(cm1.Id);
        idBatch1.add(cm2.Id);
        idBatch1.add(cm3.Id);
        idBatch1.add(cm4.Id);
        
        //assert result
        List<Placement_Candidate__c> chkList=[select Company__c from Placement_Candidate__c where Id in :idBatch1];
        for(Placement_Candidate__c temp:chkList){
            system.assertEquals(temp.Company__c, p1.Company__c);
        }
        
        /***************************************************
            Test insert candidate management WITH company
        ****************************************************/
        
        cm1=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand1.Id,Company__c=p2.Company__c);
        cm2=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand1.Id,Company__c=p2.Company__c);
        cm3=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand1.Id,Company__c=p2.Company__c);
        cm4=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand1.Id,Company__c=p2.Company__c);
        List<Placement_Candidate__c> batch2 = new List<Placement_Candidate__c>();
        List<String> idBatch2 = new List<String>();
        
        //insert candidate management & 
        batch2.add(cm1);
        batch2.add(cm2);
        batch2.add(cm3);
        batch2.add(cm4);
        insert batch2;
        idBatch2.add(cm1.Id);
        idBatch2.add(cm2.Id);
        idBatch2.add(cm3.Id);
        idBatch2.add(cm4.Id);  
        
        //assert result
        chkList=[select Company__c from Placement_Candidate__c where Id in :idBatch2];
        for(Placement_Candidate__c temp:chkList){
            system.assertEquals(temp.Company__c, p2.Company__c);
        }
    
    	}
    }
    


    static testmethod void testDeduplicationExecuteScenario(){
    	
    	
    	/**
		* added by andy for setting SQS credential
		*/
		SQSCredentialSelectorTest.createDummySQSCredential();
		
		
		
        configureSetting();
        
        
        System.runAs(DummyRecordCreator.platformUser) {
        //insert companies
        List<Account> compList = new List<Account>();
        compList.add(new Account(Name='comp1'));
        compList.add(new Account(Name='comp2'));
        insert compList;
        
        //insert vacancies
        RecordType rt = [select Id from RecordType where DeveloperName = 'Contract_Vacancy' limit 1];
        Placement__c p1=new Placement__c(RecordTypeId= rt.Id,Company__c=compList.get(0).Id,Name='placement 1');
        Placement__c p2=new Placement__c(RecordTypeId= rt.Id,Company__c=compList.get(1).Id,Name='placement 2');
        insert p1;
        insert p2;
        
        Advertisement__c ad1 = new Advertisement__c(Job_Title__c='ad 1',RecordTypeId=DaoRecordType.seekAdRT.Id,Vacancy__c=p1.Id,WebSite__c='Seek');
        insert ad1;
        
        
        RecordType candRT=DaoRecordType.indCandidateRT;
        Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand1@test.ctc.test',Candidate_From_Web__c=true,Resume__c='abc1');
        Contact cand2 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 2',Email='Cand2@test.ctc.test',Candidate_From_Web__c=true,Resume__c='abc2');
        Contact cand3 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 3',Email='Cand3@test.ctc.test',Candidate_From_Web__c=true,Resume__c='abc3');
        Contact cand4 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand1@test.ctc.test',Candidate_From_Web__c=true,Resume__c='abc1');
        insert cand1;
        insert cand2;
        insert cand3;
        insert cand4;
        
        
        Web_Document__c wd1=new Web_Document__c(Name='wd1',Document_Related_To__c=cand1.Id);
        Web_Document__c wd2=new Web_Document__c(Name='wd2',Document_Related_To__c=cand1.Id);
        Web_Document__c wd3=new Web_Document__c(Name='wd3',Document_Related_To__c=cand2.Id);
        Web_Document__c wd4=new Web_Document__c(Name='wd4',Document_Related_To__c=cand2.Id);
        Web_Document__c wd5=new Web_Document__c(Name='wd5',Document_Related_To__c=cand3.Id);
        Web_Document__c wd6=new Web_Document__c(Name='wd6',Document_Related_To__c=cand3.Id);
        insert wd1;
        insert wd2;
        insert wd3;
        insert wd4;
        insert wd5;
        insert wd6;
        
        RecordType cmRT = [select Id from RecordType where DeveloperName = 'Company_Led_Placement_Candidate' limit 1];
        Placement_Candidate__c cm1=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand1.Id,Online_Ad__c=ad1.Id);
        Placement_Candidate__c cm2=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand2.Id,Online_Ad__c=ad1.Id);
        Placement_Candidate__c cm3=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand3.Id,Online_Ad__c=ad1.Id);
        Placement_Candidate__c cm4=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand4.Id,Online_Ad__c=ad1.Id);
        
        Test.startTest();
        List<Placement_Candidate__c> insertList = new List<Placement_Candidate__c>();
        insertList.add(cm1);
        insertList.add(cm2);
        insertList.add(cm3);
        insertList.add(cm4);
        insert insertList;
        system.debug('Insert cm list:'+insertList);

          // Get PC trigger custome setting which will be used to check which trigger is enabled
     	PCAppSwitch__c cs=PCAppSwitch__c.getInstance();
      
        system.debug('Send_Out_Notification__c_3: '+cm1.Send_Out_Notification__c);
        cm1 = [select Id,Candidate__c,Send_Out_Notification__c               
                from Placement_Candidate__c
                where Id =: cm1.Id];
        Contact testCand1 = [select id, Candidate_From_Web__c,Email,FirstName,LastName, 
                  Resume__c, Resume_Source__c,Resume_Link__c,
                  (select Document_Related_To__c from Web_Documents__r)
                  from Contact where id=:cm1.Candidate__c];      
        cm2 = [select Id,Candidate__c,Send_Out_Notification__c 
                from Placement_Candidate__c
                where Id =: cm2.Id];
        Contact testCand2 = [select id, Candidate_From_Web__c,Email,FirstName,LastName, 
                  Resume__c, Resume_Source__c,Resume_Link__c,
                  (select Document_Related_To__c from Web_Documents__r)
                  from Contact where id=:cm2.Candidate__c];                   
        cm3 = [select Id,Candidate__c,Send_Out_Notification__c 
                from Placement_Candidate__c
                where Id =: cm3.Id];
        Contact testCand3 = [select id, Candidate_From_Web__c,Email,FirstName,LastName, 
                  Resume__c, Resume_Source__c,Resume_Link__c,
                  (select Document_Related_To__c from Web_Documents__r)
                  from Contact where id=:cm3.Candidate__c];
                  
        // Check if candidate managment recrod dedup trigger is enabled.
        // If it's enabled, cm4 will be deleted during the progress of dedup.
        // So we don't need to check the correctness of cm4.
        if (cs==null || !cs.Deduplicate_PC_Active__c){
          cm4 = [select Id,Candidate__c,Send_Out_Notification__c 
                from Placement_Candidate__c
                where Id =: cm4.Id];
        }      
        
        List<Placement_Candidate__c> allCMs= [select Id,Candidate__c,Send_Out_Notification__c 
                from Placement_Candidate__c
                where Placement__c=:p1.Id];    
        
        system.debug('after insert cm list,cm1:'+cm1);
        system.assertEquals(cm2.Candidate__c, cand2.Id);
        system.assertEquals(cm3.Candidate__c, cand3.Id);
        system.assertEquals(cm1.Candidate__c, cand1.Id);
        if(cs!=null && cs.Deduplicate_PC_Active__c){
          system.assertEquals(allCMs.size(), 3);
        }else{
             system.debug('after insert cm list,cm4:'+cm4);
             system.assertEquals(cm4.Candidate__c, cand1.Id);
          system.assertEquals(allCMs.size(), 4);
        }
        
        
        // workflow "Application Confirmation" should be disabled when testing this, otherwise
        // it will fail
        system.debug('cm1id=' + cm1.id);
        
        system.assertEquals(cand1.Id,testCand1.Id);
        system.assertEquals(cand2.Id,testCand2.Id);
        system.assertEquals(cand3.Id,testCand3.Id);
 
        system.assertEquals(testCand1.Web_Documents__r.size(),2);
        system.assertEquals(testCand2.Web_Documents__r.size(),2);
        system.assertEquals(testCand3.Web_Documents__r.size(),2);
        
        Test.stopTest();
        }
    	
    }

   
    static testmethod void testDeduplicationNotExecuteScenario(){
    	before();
    	
    	 configureSetting();
    	 
    	System.runAs(DummyRecordCreator.platformUser) {
       

        //insert companies
        List<Account> compList = new List<Account>();
        compList.add(new Account(Name='comp1'));
        compList.add(new Account(Name='comp2'));
        insert compList;
        
        //insert vacancies
        RecordType rt = [select Id from RecordType where DeveloperName = 'Contract_Vacancy' limit 1];
        Placement__c p1=new Placement__c(RecordTypeId= rt.Id,Company__c=compList.get(0).Id,Name='placement 1');
        Placement__c p2=new Placement__c(RecordTypeId= rt.Id,Company__c=compList.get(1).Id,Name='placement 2');
        insert p1;
        insert p2;
        
        Advertisement__c ad1 = new Advertisement__c(Job_Title__c='ad 1',RecordTypeId=DaoRecordType.seekAdRT.Id,Vacancy__c=p1.Id,WebSite__c='Seek');
        insert ad1;
        
        RecordType candRT=DaoRecordType.indCandidateRT;
        Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',FirstName='candF1',Email='Cand1@test.ctc.test',Candidate_From_Web__c=true);
        Contact cand2 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',FirstName='candF2',Email='Cand1@test.ctc.test',Candidate_From_Web__c=true);
        Contact cand3 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',FirstName='candF3',Email='Cand1@test.ctc.test',Candidate_From_Web__c=true);
        insert cand1;
        insert cand2;
        insert cand3;
        
        RecordType cmRT = [select Id from RecordType where DeveloperName = 'Company_Led_Placement_Candidate' limit 1];
        List<Placement_Candidate__c> insertCMList = new List<Placement_Candidate__c>();
        
        Web_Document__c wd1=new Web_Document__c(Name='wd1',Document_Related_To__c=cand1.Id);
        Web_Document__c wd2=new Web_Document__c(Name='wd2',Document_Related_To__c=cand1.Id);
        insert wd1;
        insert wd2;
        Placement_Candidate__c cm1=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand1.Id,Online_Ad__c=ad1.Id);
        insertCMList.add(cm1);

        Web_Document__c wd3=new Web_Document__c(Name='wd3',Document_Related_To__c=cand2.Id);
        Web_Document__c wd4=new Web_Document__c(Name='wd4',Document_Related_To__c=cand2.Id);
        insert wd3;
        insert wd4;
        Placement_Candidate__c cm2=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand2.Id,Online_Ad__c=ad1.Id);
        insertCMList.add(cm2);
        
        Web_Document__c wd5=new Web_Document__c(Name='wd5',Document_Related_To__c=cand3.Id);
        Web_Document__c wd6=new Web_Document__c(Name='wd6',Document_Related_To__c=cand3.Id);
        insert wd5;
        insert wd6;
        Placement_Candidate__c cm3=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=cand3.Id,Online_Ad__c=ad1.Id);
        insertCMList.add(cm3);
        
        insert insertCMList;

        
        Test.startTest();
        system.debug('Send_Out_Notification__c_3: '+cm1.Send_Out_Notification__c);
        cm1 = [select Id,Candidate__c,Send_Out_Notification__c               
                from Placement_Candidate__c
                where Id =: cm1.Id];
        Contact testCand1 = [select id, Candidate_From_Web__c,Email,FirstName,LastName, 
                  Resume__c, Resume_Source__c,Resume_Link__c,
                  (select Document_Related_To__c from Web_Documents__r)
                  from Contact where id=:cm1.Candidate__c];      
        cm2 = [select Id,Candidate__c,Send_Out_Notification__c 
                from Placement_Candidate__c
                where Id =: cm2.Id];
        Contact testCand2 = [select id, Candidate_From_Web__c,Email,FirstName,LastName, 
                  Resume__c, Resume_Source__c,Resume_Link__c,
                  (select Document_Related_To__c from Web_Documents__r)
                  from Contact where id=:cm2.Candidate__c];
        cm3 = [select Id,Candidate__c,Send_Out_Notification__c 
                from Placement_Candidate__c
                where Id =: cm3.Id];
        Contact testCand3 = [select id, Candidate_From_Web__c,Email,FirstName,LastName, 
                  Resume__c, Resume_Source__c,Resume_Link__c,
                  (select Document_Related_To__c from Web_Documents__r)
                  from Contact where id=:cm3.Candidate__c];
                        
        List<Placement_Candidate__c> allCMs= [select Id,Candidate__c,Send_Out_Notification__c 
                from Placement_Candidate__c
                where Placement__c=:p1.Id];              
                         
        system.assertEquals(cm1.Candidate__c, cand1.Id);
        system.assertEquals(cm2.Candidate__c, cand2.Id);
          system.assertEquals(cm3.Candidate__c, cand3.Id);        

          system.assertEquals(allCMs.size(),3);       
        
        system.assertEquals(cand1.Id,testCand1.Id);
        system.assertEquals(cand2.Id,testCand2.Id);
        system.assertEquals(cand3.Id,testCand3.Id);
 
        system.assertEquals(testCand1.Web_Documents__r.size(),2);
        system.assertEquals(testCand2.Web_Documents__r.size(),2);

        Test.stopTest();
    	}
    }
    
    static testmethod void testAutoCreateUnavailableDay(){
    	
    	configureSetting();
    	
    	System.runAs(DummyRecordCreator.platformUser) {
        
        //insert companies
        List<Account> compList = new List<Account>();
        compList.add(new Account(Name='comp1'));
        compList.add(new Account(Name='comp2'));
        insert compList;
        
        //insert vacancies
        RecordType vacRT = [select Id from RecordType where DeveloperName = 'Contract_Vacancy' limit 1];
          // Set up a job with start and end date
        Date startDate = Date.today();
        Date endDate = startDate.addMonths(1);
        Placement__c p1=new Placement__c(RecordTypeId= vacRT.Id,Company__c=compList.get(0).Id,Name='placement 1', Start_Date__c=startDate,End_Date__c=endDate);
        
        // Set up a job without start and end date
        Placement__c p2=new Placement__c(RecordTypeId= vacRT.Id,Company__c=compList.get(1).Id,Name='placement 2');
        insert p1;
        insert p2;
        system.debug('Placement 1 :'+p1);
         
        RecordType candRT = DaoRecordType.indCandidateRT;
        List<Contact> insertContactList = new List<Contact>();
        for(Integer i=1;i<=NUM_CANDIDATE_CREATE;i++) {
              Contact cand = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',Email='Cand'+i+'@testautocreate.ctc.test',Candidate_From_Web__c=true);
              insertContactList.add(cand);
        }
        insert insertContactList;
         
        // Ensure AutoCreateUnavailableDay is enabled
        PCAppSwitch__c cs=PCAppSwitch__c.getOrgDefaults();
        cs.Unavailable_Days_Auto_Creation__c=true;
        update cs;
              
        RecordType cmRT = [Select Id, Name From RecordType where DeveloperName = 'Candidate_Led_Placement_Candidate'];
        List<Placement_Candidate__c> insertCMList = new List<Placement_Candidate__c>();
        for(Integer i=0;i<NUM_CANDIDATE_CREATE;i++){
              Placement_Candidate__c cm1=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=insertContactList.get(i).Id);
              insertCMList.add(cm1);
        }
        insert insertCMList;
    
        List<Id> ids = new List<Id>();
        for(Placement_Candidate__c cm:insertCMList){
              cm.Status__c = 'Placed';
              ids.add(cm.Id);
        }
     
     // There is a workflow to auto populate start & end date from vacancy to candidate management
     // check if this workflow is working fine
     Map<Id,Placement_Candidate__c> cmListWithDatePopulated = new Map<Id,Placement_Candidate__c>([select Id,Placement__c,Placement__r.End_Date__c,placement__r.Start_Date__c,Candidate__c, 
                    Status__c,Candidate_Status__c,Start_Date__c, End_Date__c
                    from Placement_Candidate__c where Id in:ids]);
          List<Id> candIds = new List<Id>();
          Map<Id, Placement_Candidate__c>    candIdMapToCM = new Map<Id,Placement_Candidate__c>();
          for(Id key:cmListWithDatePopulated.keySet()){
               Placement_Candidate__c cm = cmListWithDatePopulated.get(key);
              system.debug('Inserted Candidate Management rec:'+cm);
          system.assertEquals(p1.Start_Date__c,cm.Start_Date__c);
          system.assertEquals(p1.End_Date__c,cm.End_Date__c);
          candIds.add(cm.Candidate__c);
          candIdMapToCM.put(cm.Candidate__c,cm);
          }
          update insertCMList;
     
          List<Days_Unavailable__c> unavailableDays = [select Id,End_Date__c,Start_Date__c,Reason__c,Contact__c from Days_Unavailable__c where Contact__c in :candIds];
          for(Days_Unavailable__c unavaDay:unavailableDays){
               Placement_Candidate__c cm=candIdMapToCM.get(unavaDay.Contact__c);
               system.assertEquals(unavaDay.Start_Date__c,cm.Start_Date__c);
          system.assertEquals(unavaDay.End_Date__c,cm.End_Date__c);
          system.assert(unavaDay.Reason__c!=null);
          }
          
    	}
    }
    
     /*
          This will test when excluded record type ids is given, Dedup will
          ignore records assgined with excluded record type ids.
          
          In this case only incoming contacts have excluded record type
     */
     static testMethod void testDedupExcludingCorpCandidateRecordScenario1(){
     	prepareData();   
     	
     	System.runAs(DummyRecordCreator.platformUser) {
                 
          Id excludedContactRTId = DaoRecordType.corpCandidateRT.Id;
          Id candidateRTId =DaoRecordType.indCandidateRT.Id;
          Integer noActualInsertedContacts = 0;
          
          // Disable the dedup trigger initially to prepare data.
          PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
          pcSwitch.DeduplicationOnVCW_Active__c=false;
          pcSwitch.Deduplicate_PC_Active__c = false;
          // Exclude Corporate Candidate from dedup
          pcSwitch.Exclude_From_Dedup__c=excludedContactRTId;
          update pcSwitch;
          
          // Re-enable to test trigger
          pcSwitch.DeduplicationOnVCW_Active__c=true;
          update pcSwitch;
          
          // 2/3 of incoming contact will be excluded from dedup, even though each of them can find a duplicate in the system.
          // That's because those records have same record type id as pcSwitch.Deduplicate_PC_Active__c.
          // The 1/3 will be dedup since they are of candidate record type
          List<Contact> incomingContactList = new List<Contact>();
          for(Integer i=0;i<201;i++){
               Contact cand1 = new Contact(RecordTypeId=excludedContactRTId,LastName='candidate 1',email='Cand'+i+'@test.ctc.test', Candidate_From_Web__c=true,jobApplication_Id__c=123456789,Resume_Source__c='abc'+i);
               Integer mod =  Math.mod(i,3);
               if(mod==0){
                    cand1.RecordTypeId = candidateRTId;
               }else{
                    noActualInsertedContacts++;
               }
               incomingContactList.add(cand1);
          }
          insert incomingContactList;
          
          // Create applications for above candidates to simulate standard application process
          RecordType cmRT = [select Id from RecordType where SobjectType =: PeopleCloudHelper.getPackageNamespace()+'Placement_Candidate__c' limit 1];
          List<Placement_Candidate__c> cmList4ExcludedCandidates = new List<Placement_Candidate__c>();
          Integer j=0;
        for(Contact con:incomingContactList){
          Placement_Candidate__c cm=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=con.Id,Online_Ad__c=ad1.Id);
          cmList4ExcludedCandidates.add(cm);
          j++;
        }
        
        Test.startTest();  
        insert cmList4ExcludedCandidates;
        noActualINsertedContacts +=conInsertList.size();
          system.debug('inserted contact:'+[select Id,LastName,jobApplication_Id__c,Resume_Source__c from contact].size());
          Test.stopTest();
          
          List<Contact> resultList = [Select id,jobApplication_Id__c from Contact where jobApplication_Id__c = 123456789];
          system.assertEquals(noActualInsertedContacts, resultList.size());      
     	}    
     }
    
    
    /**
        This will test when excluded record type ids is given, Dedup will
        ignore records assgined with excluded record type ids.
        
        In this case only existing contacts have excluded record type
    
    */
    static testMethod void testDedupExcludingCorpCandidateRecordScenario2(){
    	
    	
    	/**
		* added by andy for setting SQS credential
		*/
		SQSCredentialSelectorTest.createDummySQSCredential();
		
        List<Id> excludedContactRTIds = new List<Id>();
        excludedContactRTIds.add(DaoRecordType.corpCandidateRT.Id);
        excludedContactRTIds.add(DaoRecordType.corpContactRT.Id);
        prepareData();
        // Disable the dedup trigger initially to prepare data.
        PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
        pcSwitch.DeduplicationOnVCW_Active__c=false;
        pcSwitch.Deduplicate_PC_Active__c = false;
        // Exclude contact with recordtypeid in excludedContactRTIds from dedup
        String excludedIdString = '';
        for(Integer j=0;j<excludedContactRTIds.size();j++){
            if(j==0)
                excludedIdString = excludedContactRTIds.get(j);
            else
                excludedIdString += ';'+excludedContactRTIds.get(j);
        }
        pcSwitch.Exclude_From_Dedup__c=excludedIdString;
        update pcSwitch;
        
		
        // Since originally prepared contact are of type candidate,
        // we need to update their record type to simulate recent scenario
        List<Contact> updateList = new List<Contact>();
        Integer i =0;
        Integer numOfRecordExcFromDedup = 0;
        List<Contact> testData = new List<Contact>();
        
        System.runAs(DummyRecordCreator.platformUser) {
        for(Contact con:conInsertList){
            // Update 2/3 of these records to make them having one of the excluded IDs.
            // The other 1/3 of these records will remain as candidate
            Integer index = Math.mod(i,3);
            if(index < excludedContactRTIds.size()){
                con.RecordTypeId = excludedContactRTIds.get(index);
                numOfRecordExcFromDedup++;
                updateList.add(con);
            }
            i++;
        }
        update updateList;
        system.debug('candidate to be updated:'+updateList.size());
		}
                
        // Re-enable trigger to test dedup trigger
        pcSwitch.DeduplicationOnVCW_Active__c=true;
        update pcSwitch;
        
        System.runAs(DummyRecordCreator.platformUser) {
        
        // Some of following contact will be excluded from dedup, even though each of them can find a duplicate in the system.
        // That's because their existing counterpart have same record type id as pcSwitch.Deduplicate_PC_Active__c
        Id CandidateId = DaoRecordType.indCandidateRT.Id;
        List<Contact> incomingContactList = new List<Contact>();
        for(i=0;i<201;i++){
            Contact cand1 = new Contact(RecordTypeId=CandidateId,LastName='candidate 1',email='Cand'+i+'@test.ctc.test', Candidate_From_Web__c=true,jobApplication_Id__c=123456789,Resume_Source__c='abc incoming'+i);
            incomingContactList.add(cand1);
        }
        insert incomingContactList;
        system.debug('candidate to be updated:'+updateList.size());
        
        // Create applications for above candidates to simulate standard application process
        RecordType cmRT = [select Id from RecordType where SobjectType =: PeopleCloudHelper.getPackageNamespace()+'Placement_Candidate__c' limit 1];
        List<Placement_Candidate__c> cmList = new List<Placement_Candidate__c>();
        for(Contact con:incomingContactList){
            Placement_Candidate__c cm=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=con.Id,Online_Ad__c=ad1.Id);
            cmList.add(cm);
        }
        Test.startTest();
        insert cmList;
        Test.stopTest();
        
        List<Contact> resultList = [Select id,jobApplication_Id__c from Contact where jobApplication_Id__c = 123456789];
        system.assertEquals(numOfRecordExcFromDedup+conInsertList.size(), resultList.size());
    	}
    }    

  /*
    This test is to test the dedup scenario where multiple duplicate candidates apply to a job.
    Those duplicate candidates have same first name, last name and email with one of the applicants of that job.
    But not all duplicate candidates have same set of skills as that existing record, which will trigger skill dedup. 
  */  
  static testmethod void testCandidateManagementDedupWithDuplicateCandidatesButNoDuplicateSkills(){
  	
  	
  		/**
		* added by andy for setting SQS credential
		*/
		SQSCredentialSelectorTest.createDummySQSCredential();
    // Disable the dedup trigger to avoid hitting SF limit during test data preparation
        PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
        if(pcSwitch==null){
        	pcSwitch=new PCAppSwitch__c();
        }
        pcSwitch.Placed_Candidate_Status__c = true;
        pcSwitch.DeduplicationOnVCW_Active__c=false;
        pcSwitch.Deduplicate_PC_Active__c = false;
        // Exclude Corporate Candidate from dedup
        upsert pcSwitch;
        
    PeopleCloudHelper.configureSetting();
    PeopleCloudHelper.PeopleCloudTestDataSet testDataSet = PeopleCloudHelper.prepareData();
    
    // Finish data preparation, re-enable dedup trigger
    pcSwitch.DeduplicationOnVCW_Active__c=true;
        pcSwitch.Deduplicate_PC_Active__c = true;
        // Exclude Corporate Candidate from dedup
        upsert pcSwitch;
        
    System.runAs(DummyRecordCreator.platformUser) {
    Test.startTest();
    PeopleCloudHelper.ApplicationList appList = PeopleCloudHelper.candidatesApplyJob(10, testDataSet.advertisements.get(0), new Map<String,Object>(), testDataSet.skills, true);
    PeopleCloudHelper.Application applicantToClone = appList.applications.get(0);
    system.debug('applicant to clone:'+applicantToClone);
    
    // Clone 10 candidates using applicantToClone, and let them apply same job the
    // that applicantToClone applys. This should trigger candidate and skills dedup.  
    PeopleCloudHelper.ApplicationList duplicateApplication = PeopleCloudHelper.duplicateCandidatesApplyJob(10, applicantToClone, 
      testDataSet.advertisements.get(0),  new Map<String,Object>(), testDataSet.skills, true, false);
    Test.stopTest();
    
    Map<Id,Employment_History__c> ehMap = new Map<Id,Employment_History__c>([select Id,Name,Title__c from Employment_History__c where Contact__c =: applicantToClone.candidate.Id]);
    Map<Id,Candidate_Skill__c> csMap = new Map<Id,Candidate_Skill__c>([select Id,Name,Skill__c from Candidate_Skill__c where Candidate__c =: applicantToClone.candidate.Id]);
    Map<Id,Placement_Candidate__c> cmMap = new Map<Id,Placement_Candidate__c>([select Id,Name,Candidate__c from Placement_Candidate__c where Candidate__c =: applicantToClone.candidate.Id]);
    
    system.debug('map:'+ehMap);
    system.debug('applicantToCLone eh:'+applicantToClone.employmentHistory);
    // During dedup, old employment history will be cleaned but same amount of new history will be merged, 
    // therefore total number of employment history entries should not increase.
    system.assertEquals(applicantToClone.employmentHistory.size(),ehMap.keySet().size());
    for(Employment_History__c eh: applicantToClone.employmentHistory){
      system.debug(ehMap.get(eh.Id));
      system.assert(ehMap.get(eh.Id) == null);
    }
    // During dedup, old candidate skills will be kept, candidate skills from duplicates will be 
    // kept if they are new and not added to existing candidate.
    // In this case, the number of candidate skills of existing candidate will increase, as not all duplicates
    // have same skill set as that existing candidate has. After dedup, each skills linked to old candidate are unique.
    system.assert(applicantToClone.candidateSkills.size() <= csMap.keySet().size());
    Map<Id,Candidate_Skill__c> dupSkillCheckMap = new Map<Id,Candidate_Skill__c>(); 
    for(Id csId : csMap.keySet()){
      Candidate_Skill__c cs = csMap.get(csId);
      if(dupSkillCheckMap.get(cs.Skill__c)!=null){
        // There are duplicate skills linked to old candidates
        system.assert(false);
      }
      dupSkillCheckMap.put(cs.Skill__c,cs);
    }
    system.assertEquals(1, cmMap.keySet().size());
  	}
    
  }
    
    static private List<Account> compList;
  static private Placement__c p1; 
  static private Placement__c p2;
  static private Advertisement__c  ad1;
  static private Advertisement__c  ad2;
  static private List<Contact> conInsertList;
  static private List<Placement_Candidate__c> cmList;
  
  static void configureSetting(){
    PeopleCloudHelper.configureSetting();
    
    //config SQS credential
    SQSCredentialSelector selector = new SQSCredentialSelector();
	SQSCredential__c sqsCred = selector.getSQSCredential('Daxtra Feed SQS');
    if (sqsCred == null) {
    	sqsCred = new SQSCredential__c();
    	sqsCred.Name = 'Daxtra Feed SQS';
        sqsCred.URL__c = 'https://sqs.us-east-1.amazonaws.com/474781809688/CandidateFeedQueue';
        
        sqsCred.Access_Key__c = '123456789';
        sqsCred.Secret_Key__c = '123456789';  
        insert sqsCred;       
    } 
  }
  
  static void prepareData(){
    configureSetting();
    //insert companies
        compList = new List<Account>();
        compList.add(new Account(Name='comp1'));
        compList.add(new Account(Name='comp2'));
        insert compList;
        
        //insert vacancies
        RecordType rt = [select Id from RecordType where SobjectType =: PeopleCloudHelper.getPackageNamespace()+'Placement__c' limit 1];
        p1=new Placement__c(RecordTypeId= rt.Id,Company__c=compList.get(0).Id,Name='placement 1');
        p2=new Placement__c(RecordTypeId= rt.Id,Company__c=compList.get(1).Id,Name='placement 2');
        insert p1;
        insert p2;
        
        // Insert Advertisements
        ad1 = new Advertisement__c(Job_Title__c='ad 1',RecordTypeId=DaoRecordType.seekAdRT.Id,Vacancy__c=p1.Id,WebSite__c='Seek');
        ad2 = new Advertisement__c(Job_Title__c='ad 2',RecordTypeId=DaoRecordType.seekAdRT.Id,Vacancy__c=p2.Id,WebSite__c='Seek');
        insert ad1;
        insert ad2;
    
    // Create candidates
    RecordType candRT=DaoRecordType.indCandidateRT;    
    conInsertList = new List<Contact>(); 
    for(Integer i=0;i<201;i++){
      Contact cand1 = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1',email='Cand'+i+'@test.ctc.test', Candidate_From_Web__c=true,jobApplication_Id__c=123456789,Resume_Source__c='abc'+i);
      conInsertList.add(cand1);
    }
    insert conInsertList;
    
    RecordType cmRT = [select Id from RecordType where SobjectType =: PeopleCloudHelper.getPackageNamespace()+'Placement_Candidate__c' limit 1];
    cmList = new List<Placement_Candidate__c>();
        for(Contact con:conInsertList){
          Placement_Candidate__c cm1=new Placement_Candidate__c(RecordTypeId = cmRT.id,Placement__c=p1.Id,Candidate__c=con.Id,Online_Ad__c=ad1.Id);
          cmList.add(cm1);
        }
        // Prepare Candidate Management data, no deduplication happen
        insert cmList;
  }



         
 }