/**
 * This is the test class for AdvertisementConfigurationSelector
 */

@isTest
private class AdvertisementConfigurationSelectorTest{


    static Advertisement_Configuration__c indeedAccount;
    static Advertisement_Configuration__c jxtAccount;
    static Advertisement_Configuration__c seekAccount;
    static AdvertisementConfigurationSelector acs;

    static {
        indeedAccount = AdConfigDummyRecordCreator.createDummyIndeedAccount();
        jxtAccount = AdConfigDummyRecordCreator.createDummyJxtAccount();
        seekAccount = AdConfigDummyRecordCreator.createDummySeekAccount();
        acs = new AdvertisementConfigurationSelector();
    }

	static testMethod void getIndeedAccounts() {
		System.runAs(DummyRecordCreator.platformUser) {

			User currentUser = DaoUsers.getActiveUser(UserInfo.getUserId());
			currentUser.indeed_account__c = indeedAccount.Id;
			update currentUser;

			List<Advertisement_Configuration__c> acslist = acs.getIndeedAccounts();
			system.assertEquals(acslist.size(),1);

			Advertisement_Configuration__c cia = acs.getIndeedAccountById(indeedAccount.Id);
			system.assertEquals(cia.Id, indeedAccount.Id);
		}
	}

	static testMethod void testGetJXTAccount() {
		System.runAs(DummyRecordCreator.platformUser) {
			Advertisement_Configuration__c ac = acs.getJXTAccount();
			system.assert(ac!=null);
			system.assertEquals('132456', ac.Advertisement_Account__c);
		}
	}

    static testMethod void testGetActiveAccountByRecordTypeId() {
        System.runAs(DummyRecordCreator.platformUser) {
            List<Advertisement_Configuration__c> jxtAccountList = acs.getActiveAccountByRecordTypeId(DaoRecordType.jxtAccountRT.Id);
            System.assertEquals(1, jxtAccountList.size());
            System.assertEquals(jxtAccount.Id, jxtAccountList.get(0).Id);

            Advertisement_Configuration__c inactiveSeek = AdConfigDummyRecordCreator.createInactiveSeekAccount();
            List<Advertisement_Configuration__c> seekAccountList = acs.getActiveAccountByRecordTypeId(DaoRecordType.seekAccountRT.Id);
            System.assertEquals(1, seekAccountList.size());
            System.assertEquals(seekAccount.Id, seekAccountList.get(0).Id);
        }
    }

    static testMethod void testGetActiveAccountByIds() {
        System.runAs(DummyRecordCreator.platformUser) {
            Advertisement_Configuration__c inactiveSeek = AdConfigDummyRecordCreator.createInactiveSeekAccount();
            List<Id> ids = new List<Id>{
                    indeedAccount.Id, jxtAccount.Id, seekAccount.Id, inactiveSeek.Id
            };
            List<Advertisement_Configuration__c> adConfigs = acs.getActiveAccountByIds(ids);
            System.assertEquals(3, adConfigs.size());
        }
    }


    static testMethod void testGetAccountByRecordTypeId() {
        System.runAs(DummyRecordCreator.platformUser) {
            List<Advertisement_Configuration__c> jxtAccountList = acs.getAccountByRecordTypeId(DaoRecordType.jxtAccountRT.Id);
            System.assertEquals(1, jxtAccountList.size());
            System.assertEquals(jxtAccount.Id, jxtAccountList.get(0).Id);

        }
    }
}