@isTest
private with sharing class TaskSelectorTest {

	private static testMethod void getTop5TaksByContactIdTest() {
		system.runAs(DummyRecordCreator.platformUser) {
			Account acc = TestDataFactoryRoster.createAccount();
		    
		    Contact con = TestDataFactoryRoster.createContact();
		    
		    Profile p = [SELECT Id FROM Profile].get(0);
		    		    
		    List<Task> tList = new List<Task>();
		    for(integer i=0; i<100; i++) {
		        Task t = new Task();
		        t.whoId = con.Id;
		        t.OwnerId = UserInfo.getUserId();
		        t.subject = 'test subject' + i;
		        tList.add(t);
		    }
		    insert tList;
		    
		    TaskSelector taskSel = new TaskSelector();
		    List<Task> tTop5 = taskSel.getTop5TaksByContactId(con.Id);
		    system.assertEquals(tTop5.size(), 5);
		}
	}
	
	private static testMethod void getTaskByIdTest() {
	    
	   	DummyRecordCreator.DefaultDummyRecordSet rs = DummyRecordCreator.createDefaultDummyRecordSet();
	    ActivityDummyRecordCreator taskCreator = new ActivityDummyRecordCreator();
		List<Task> tasks = taskCreator.generateActivityDummyRecord(rs);
	    
		system.runAs(DummyRecordCreator.platformUser) {
		   
		   
			TaskSelector taskSel = new TaskSelector();
		    Task t = taskSel.getTaskById(tasks[0].Id);
		    system.assertEquals(t.whoId, rs.candidates[0].Id);
		}
	}
}