//Author by Jack, the new send resume function

public with sharing class ExtractData4SRController {
	private ApexPages.Standardsetcontroller setCon;
	private ApexPages.StandardController con;	
	public Id sendresumeid {get; set;}  //capture the previous page id
	public String retURL{get; set;}    //generate the previous page URL
	private Contact selectcan;	
	public Schema.sObjectType sobjecttype{get; set;}
	public PageReference pr;  		    //return to SendResume page
	public PageReference ErrorPage;
	public String sobjecttypestring;	
	public String backto;
	//get error message
	public PeopleCloudErrorInfo errormesg; 
	public Boolean msgSignalhasmessage{get ;set;}
	public Integer msgcode;
		
	//Init the param from ShowAll Page
	public string cmids;
    public List<Id> vids;
    public string currentpage;
    public String currentSelectCrieria;
    private String currentAscSelectCrietria;
    
    //Init the flag for SMS button 26/09/13
    public Boolean issmsfunction;
    
    //Add Vacancy Activity Record 18/09/12
    public list<id> vacancyid; // put the vacancy id into the set
    
    //Add sourcepage param 03 Oct 13
    public String sourcepage;
	/**
	   Init controller, get retURL and id
	   cm & cms & Home QSR Link & ShowAll page-->Placement_Candidate__c RecordType;
	   Contact & Candidate & Candidates --> Contact RecordType; 
	**/
	
	public ExtractData4SRController(ApexPages.StandardSetController setCon){
	    
	    
		sendresumeid = PeopleCloudHelper.getIdParam(Apexpages.currentPage());
	
		retURL = PeopleCloudHelper.makeRetURL(Apexpages.currentPage());
		
	
		sobjecttype = setCon.getRecord().getSObjectType();
		sobjecttypestring = String.valueOf(sobjecttype);
		this.setCon=setCon;		
		con = null;
	}
	
	public ExtractData4SRController(ApexPages.StandardController con){
		sendresumeid = Apexpages.currentPage().getParameters().get('id');		
		retURL = PeopleCloudHelper.secureUrl('/'+sendresumeid);

		sobjecttype  = con.getRecord().getSObjectType();
		sobjecttypestring = String.valueOf(sobjecttype);	
		this.con=con;		
		setCon=null;		
	}
	
	public ExtractData4SRController(){
		//capture the param from URL when the info from ShowAll page	
		sourcepage 	= ApexPages.currentPage().getParameters().get('sourcepage');
		sendresumeid = ApexPages.currentPage().getParameters().get('id');
        cmids = ApexPages.currentPage().getParameters().get('cmids');
        currentpage = ApexPages.currentPage().getParameters().get('cp');
        currentSelectCrieria = ApexPages.currentPage().getParameters().get('sc');
        currentAscSelectCrietria = ApexPages.currentPage().getParameters().get('asc');
        vids = new List<Id>();
        retURL = '';
                
	    setCon=null;	
		con = null;
        if(sourcepage != null){
        	if(cmids != null){	        	
	            vids = cmids.split(',');
	            sobjecttypestring =  PeopleCloudHelper.getPackageNamespace()+'Placement_Candidate__c';
				//General the absoulte Return URL
				String pageurl = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/'+ PeopleCloudHelper.getPackageNamespace()+'iframeparentstandard';
				PageReference showallpage = new PageReference(pageurl);
				
				//PageReference showallpage = page.IframeParentStandard;			
	            showallpage.setRedirect(true);
	            showallpage.getParameters().put('sc',currentSelectCrieria);
	            showallpage.getParameters().put('cp',currentpage);
	            showallpage.getParameters().put('id',sendresumeid);
	            showallpage.getParameters().put('asc',currentAscSelectCrietria);            
	            retURL = showallpage.getURL();
	            
	  
	            backto = 'backtoshowall';
	        }else if(sourcepage == 'googlesearch' ){
	        	String candidatejsonid = ApexPages.currentPage().getParameters().get('cjid');
	        	String searchcriteriajsonid = ApexPages.currentPage().getParameters().get('searchcriteriajsonid');
	        	retURL = PeopleCloudHelper.secureUrl( ApexPages.currentPage().getParameters().get('returnurl'));
	   
	        	backto = 'backtogooglesearch';
	        	sobjecttypestring = 'Contact';//config the source record is Contact or CM
	        	//system.debug('candidatejsonid='+ candidatejsonid);
	        	if(candidatejsonid!=null && candidatejsonid!= ''){
	        		vids = GoogleSearchUtil2.fetchCandidateIdList(candidatejsonid);	        		
	        	}else{
	        		customerErrorMessage(PeopleCloudErrorInfo.SYSTEM_PROCESS_ERROR);
	        	}
	        }
			// Both PeopleSearch and ScreenCandidate are using this as sourcepage
	        else if(sourcepage == 'rostering' ){
	        	String candidatejsonid = ApexPages.currentPage().getParameters().get('cjid');
	        	retURL = '/home/home.jsp';
	  
	        	backto = 'backtohomepage';
	        	sobjecttypestring = 'Contact';//config the source record is Contact or CM
	        	//system.debug('candidatejsonid='+ candidatejsonid);
	        	if(candidatejsonid!=null && candidatejsonid!= ''){
	        		vids = (List<Id>)JSON.deserialize(candidatejsonid,List<Id>.class);
	        	}else{
	        		customerErrorMessage(PeopleCloudErrorInfo.SYSTEM_PROCESS_ERROR);
	        	}
	        }
            // ScreenCandidate is using this as sourcepage
            else if (sourcepage == 'screencandidate') {
                String cmjsonid = ApexPages.currentPage().getParameters().get('cjid');
                retURL = '/home/home.jsp';
               
                backto = 'backtohomepage';
                sobjecttypestring = PeopleCloudHelper.getPackageNamespace() + 'Placement_Candidate__c';//config the source record is Contact or CM
                system.debug('cmjsonid=' + cmjsonid);
                if (cmjsonid != null && cmjsonid != '') {
                    vids = (List<Id>) JSON.deserialize(cmjsonid, List<Id>.class);
                } else {
                    customerErrorMessage(PeopleCloudErrorInfo.SYSTEM_PROCESS_ERROR);
                }
            }
        }        
        else{
			retURL = '/home/home.jsp';

			sobjecttypestring = 'Contact';
			setCon=null;	
			con = null;
			backto = 'backtohomepage';
        }
	}
	
		
	public PageReference inits(){
		issmsfunction = false;
		vacancyid = new List<id>();
		errormesg = null;
		msgcode = null;
		msgSignalhasmessage = false;

		if(sobjecttypestring.equals(PeopleCloudHelper.getPackageNamespace()+'Placement_Candidate__c')){
			initCMS();
		}
		else if(sobjecttypestring.equals('Contact')){			
			initContacts();
		}
		//system.debug('pr ='+ pr);
		return pr;//redirect the VF to SendResume Page
	}
	
	//Init SMS button
	public PageReference SMSinits(){
		issmsfunction = true;
		vacancyid = new List<id>();
		errormesg = null;
		msgcode = null;
		msgSignalhasmessage = false;
		if(sobjecttypestring.equals(PeopleCloudHelper.getPackageNamespace()+'Placement_Candidate__c')){
			initCMS();
		}
		else if(sobjecttypestring.equals('Contact')){			
			initContacts();
		}
		return pr;//redirect the VF to SMS Page
	}
	
	//Init the Candidate Management(s) inforamtion and return the Contact list
	public void initCMS(){
		try{
			List<Placement_Candidate__c> selectCMs;
			List<Placement_Candidate__c> selectCMsCans = new List<Placement_Candidate__c>();
			List<Contact> selectedCan = new List<Contact>();			
			if(setCon != null && con == null){
				if(test.isRunningTest()){
					selectCMs = (List<Placement_Candidate__c>)setCon.getRecords();
				}
				else{
					selectCMs = (List<Placement_Candidate__c>)setCon.getSelected();					
				}
				if(selectCMS.size() == 0){
					customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CMS_SELECTED);	
				}
				backto = 'backtovacancy';
			}
			else if(setCon ==null && con!= null){
				selectCMs = new List<Placement_Candidate__c>();
				selectCMs.add((Placement_Candidate__c)con.getRecord());
				backto = 'backtocm';
			}
			else if(setCon == null && con == null){
				selectCMs = new List<Placement_Candidate__c>();
				CommonSelector.checkRead(Placement_Candidate__c.SObjectType,'Id');
				selectCMs = [select Id from Placement_Candidate__c where id in:vids];
			}
			else{
				selectCMs = null;
			}
			//system.debug('selectCMs =' + selectCMs );
			if(selectCMs != null && selectCMS.size() >0){
				CommonSelector.checkRead(Placement_Candidate__c.SObjectType,'id, Candidate__r.id, Candidate__r.RecordTypeId, Placement__r.id');
				selectCMsCans = [select id, Candidate__r.id, Candidate__r.RecordTypeId, Placement__r.id from Placement_Candidate__c where id in: selectCMs and Candidate__c != null];
				for(Placement_Candidate__c p : selectCMsCans){
					//system.debug('vacancyid ='+p.Placement__r.id);
					selectedCan.add(p.Candidate__r);
					if(p.Placement__r.id != null){
						vacancyid.add(p.Placement__r.id);
					}			
				}
				//system.debug('selectedCan ='+selectedCan);
				
				if(selectedCan != null && selectedCan.size()>0){
					sendResumeUrlParams(selectedCan , SendResumeController.EXTRACT_CAN, retURL , backto, vacancyid);
				}
				else{
					customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CAN_IN_SELECTED_CM);
				}	
			}			
			else{
				pr = null;
			}
		}
		catch(Exception e){
			NotificationClass.notifyErr2Dev('ExtractData4SRController/initCMS' + backto, e); 
		}
	}
	
	//Init the Contact(s) inforamtion and return the Contact list	
	public void initContacts(){
		try{
			List<Contact> selectedCon;
			List<Contact> selectedCan;
			List<Contact> selectedConCan;
			if(setCon !=null && con== null){	
				if(test.isRunningTest()){
					selectedConCan=(List<Contact>)setCon.getRecords();
				}	
				else{
					selectedConCan=(List<Contact>)setCon.getSelected();
				}
				selectedCan = new List<Contact>{};
				for(Contact selectcan : selectedConCan){
					selectedCan.add(selectcan);
				}
				backto = 'backtoContactview';
				if(selectedCan.size() == 0) {								
					customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CONTACT_SELECTED);			
				}									
			}
			else if(setCon ==null && con!= null){
				selectedCon = new List<Contact>();
				selectedCan = new List<Contact>();
				if(contactsetrecordtype.contains(((Contact)con.getRecord()).RecordTypeId)){
					//Only transfer the Contact which has Email value
					/*GetContactJson contactEmailJson = new GetContactJson();
					contactEmailJson.dynamicContactEmailField();
					String tempEmailQueryCondition = contactEmailJson.tempEmailQueryCondition;
					system.debug('tempEmailQueryCondition =' + tempEmailQueryCondition);*/
					//String contactEmailJsonSql = 'select id from contact where id = \'' + ((Contact)con.getRecord()).Id + '\' and Email != null and contact.AccountId != null'; //+ tempEmailQueryCondition;
					String contactEmailJsonSql = 'select id from contact where id = \'' + ((Contact)con.getRecord()).Id + '\' and Email != null ';
					CommonSelector.checkRead(Contact.SObjectType,'id');
					List<Contact> contactContainEmail = Database.Query(contactEmailJsonSql);
					if(ContactContainEmail.size() >0){
						selectedCon.add((Contact)con.getRecord());
					}
					else{
						msgcode = PeopleCloudErrorInfo.ERR_CONTATCT_NO_EMAIL;							
						errormesg = new PeopleCloudErrorInfo(msgcode,true);					
						msgSignalhasmessage = errormesg.returnresult;
						for(ApexPages.Message newMessage:errormesg.errorMessage){
							ApexPages.addMessage(newMessage);
						}				
					}
					backto = 'backtocontact';
				}
				else if(candidatesetrecordtype.contains(((Contact)con.getRecord()).RecordTypeId)){
					selectedCan.add((Contact)con.getRecord());
					backto = 'backtocandidate';
				}
				
			}
			else if(setCon == null && con == null){
				if(vids!=null && vids.size()>0){
					selectedCan = new List<Contact>();
					CommonSelector.checkRead(Contact.SObjectType,'Id');
					selectedCan = [select Id from Contact where id in:vids];
				}else{
					selectedCon = null;
					selectedCan = null;
				}
			}
			else{
				pr = null;
			}	
			//system.debug('selectedCan'+selectedCan);
			//system.debug('selectedCon'+selectedCon);
			if(selectedCon != null && selectedCon.size()>0){		
				sendResumeUrlParams(selectedCon , SendResumeController.EXTRACT_CON , retURL, backto, vacancyid);
			}
			else if(selectedCan != null && selectedCan.size()>0){
				sendResumeUrlParams(selectedCan , SendResumeController.EXTRACT_CAN , retURL, backto, vacancyid);
			}
			else if(selectedCon == null && selectedCan == null){
				sendResumeUrlParams(selectedCon , SendResumeController.EXTRACT_CAN , retURL, backto, vacancyid);
			}
			else{			
				pr = null;
			}
		}
		catch(Exception e){
			NotificationClass.notifyErr2Dev('ExtractData4SRController/initContacts' + backto, e); 
		}
	}
	
	//Generate the redirect URL
	public void sendResumeUrlParams(List<Contact> selectedCon, String paramsURL, String returnurl, String backto, list<id> vanid){
		Integer index=0;
		if(!issmsfunction){
			pr=new ApexPages.Pagereference('/apex/SendResume');
			Map<String,String> params=pr.getParameters();
			params.put('redirectType',paramsURL);
			params.put('returnurl' , returnurl);
			params.put('backto', backto);
			if(selectedCon != null){
				//system.debug('sendResumeUrlParamsselectedCon = ' +selectedCon );
				for(Contact c:selectedCon){
					String argKey='arg'+index;
					params.put(argKey,c.Id);
					index++;
				}
			}
			if(vanid != null && vanid.size()>0){
				params.put('vanids',vanid.get(0));
			}
		}else{
			pr=new ApexPages.Pagereference('/apex/smagicinteract__BulkSendSMS');
			Map<String,String> params=pr.getParameters();
			params.put('retURL' , returnurl);
			params.put('phoneField', 'MobilePhone');
			params.put('nameField', 'Name');
			params.put('optOutField', 'smagicinteract__SMSOptOut__c');
			params.put('objectType', 'contact');
			//system.debug('selectedCon ='+ selectedCon);
			if(selectedCon!= null){
				CommonSelector.checkRead(Contact.SObjectType,'id, MobilePhone');
				List<Contact> selectConwithMobilePhone = [select id, MobilePhone from Contact where id in: selectedCon and MobilePhone!= null];
				//system.debug('selectConwithMobilePhone ='+ selectConwithMobilePhone);
				if(selectConwithMobilePhone!= null){
					if(selectConwithMobilePhone.size()>0 && selectConwithMobilePhone.size()<201){
						String selectConWithMobilePhoneString = '';
						for(Contact c:selectConwithMobilePhone){
							if(selectConWithMobilePhoneString == ''){
								selectConWithMobilePhoneString = c.id;
							}else{
								selectConWithMobilePhoneString = selectConWithMobilePhoneString + ',' + c.id;
							}
						}
						params.put('recordIds', selectConWithMobilePhoneString);
					}else if(selectConwithMobilePhone.size()>201){
						customerErrorMessage(PeopleCloudErrorInfo.MORE_THAN_200_CONTACT_SELECTED);
						pr = null;
					}
					else{
						customerErrorMessage(PeopleCloudErrorInfo.NO_CONTACT_SELECTED);
						pr = null;
					}
				}else{
					customerErrorMessage(PeopleCloudErrorInfo.NO_CONTACT_SELECTED);
					pr = null;
				}				
			}else{
				customerErrorMessage(PeopleCloudErrorInfo.NO_CONTACT_SELECTED);
				pr = null;
			}
		}
		//system.debug('pr ='+ pr);
	}
	
	//Create the Candidate Record Type ID set
	public static set<id> candidatesetrecordtype{
		get{
			set<Id> candidatesetrecordtypeId = new set<Id>{};
			List<RecordType> candidaterecordtype;	
			candidaterecordtype = DaoRecordType.candidateRTs;
			for(RecordType rectypcan : candidaterecordtype ){
				candidatesetrecordtypeId.add(rectypcan.id);
				}
			return candidatesetrecordtypeId;
		}
	}
	
	//Create the Contact Record Type ID set
	public static set<id> contactsetrecordtype{
		get{
			set<Id> contactsetrecordtypeId = new set<Id>{};
			List<RecordType> contactrecordtype;	
			//contactrecordtype = DaoRecordType.nonCandidateRTs;
			contactrecordtype = DaoRecordType.contactRTs;
			for(RecordType rectypcan : contactrecordtype ){
				contactsetrecordtypeId.add(rectypcan.id);
				}
			return contactsetrecordtypeId;
		}
	}
		
	//Return to Previous Page
	public PageReference returntoPreviousPage(){
		//system.debug('retURL ='+ retURL);
		PageReference pr = new PageReference(retURL);
        return pr;
	}
	
	//Genereate Error message
	public void customerErrorMessage(Integer msgcode){
        errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        msgSignalhasmessage = errormesg.returnresult;
        for(ApexPages.Message newMessage:errormesg.errorMessage){
        	ApexPages.addMessage(newMessage);
        }               
    }
	
	static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){//Send error message
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());    
    }
}