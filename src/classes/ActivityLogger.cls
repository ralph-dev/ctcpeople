public with sharing class ActivityLogger {
	
	/**
		Warning: This method is deprecated and should not be used. It's still kept in place since
		there are other function using it. However, it's not suitable for future use.
		
		This method will create a list of tasks by carrying out cartesian product on given 'who' and 'what' list.
		This list of taks then will be inserted into system.
	*/
	public static List<Task> logActivities(String subject,List<SObject> whos, List<SObject> whats,String description){
		List<Task> insertActivities = new List<Task>();
		for(SObject who:whos){
			if(whats == null){
				List<Task> activity = genActivity(subject,who,null,description);
				insertActivities.add(activity.get(0));
			}else{
				for(SObject what:whats){
					List<Task> activity=genActivity(subject,who,what,description);
					insertActivities.add(activity.get(0));
				}
			}

		}
		checkFLS();
		insert insertActivities;
		return insertActivities;
	}
	
	/**
		Create and insert a list of tasks using given 'who' list and the 'what' sobject. All created tasks will be
		linked to same 'what' sobject, which could be any non-contact SF object.
	*/
	public static List<Task> logActivitiesWithSameWhatId(String subject,List<SObject> whos, SObject what,String description){
		List<Task> insertActivities = new List<Task>();
		for(SObject who:whos){
			List<Task> activity=genActivity(subject,who,what,description);
			insertActivities.add(activity.get(0));
		}
		checkFLS();
		insert insertActivities;
		return insertActivities;
	}

	public static Task logSingleActivity(String subject,SObject who, SObject what,String description){
		List<Task> activity=genActivity(subject,who,what,description);
		checkFLS();
		insert activity;
		return activity.get(0);
	}


	public static List<Task> genActivity(String subject,SObject who,SOBject what,String description){
		String woid=(String)who.get('Id');
		List<Task> activity = new list<Task>();
		Task activityO = new Task(Subject=subject,WhoId=woid,Description=description);
		activity.add(activityO);
		if(what!=null){
			String waid=(String)what.get('Id');
			activity.get(0).WhatId=waid;			
		}
		activity.get(0).Priority='Normal';
		activity.get(0).ActivityDate=Date.today();
		activity.get(0).Status='Completed';
		return activity;
	}

	private static void checkFLS(){
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
			Task.Subject,
			Task.WhoId,
			Task.Description,
			Task.WhatId,
			Task.Priority,
			Task.ActivityDate,
			Task.Status
		};
		fflib_SecurityUtils.checkInsert(Task.SObjectType, fieldList);
	}
	
	static testmethod void testLogger(){
		
			List<Account> accs = new List<Account>();
			List<Contact> cons = new List<Contact>();
			TriggerHelper.disableAllTrigger();
	        // Disable the dedup trigger to avoid hitting SF limit during test data preparation
	        
	        
	        
	        PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
	        if(pcSwitch==null){
	        	pcSwitch=new PCAppSwitch__c();
	        }
	        pcSwitch.Placed_Candidate_Status__c = true;
	        pcSwitch.DeduplicationOnVCW_Active__c=false;
	        pcSwitch.Deduplicate_PC_Active__c = false;
	        // Exclude Corporate Candidate from dedup
	        upsert pcSwitch;		
			
			// Prepare test data
			Account candPool = PeopleCloudHelper.createCandidatePool();
			// Create some candidates
			PeopleCloudHelper.PeopleCloudTestDataSet testDataSet = PeopleCloudHelper.prepareData(); // prepare vacancy, job
			PeopleCloudHelper.ApplicationList appList = PeopleCloudHelper.candidatesApplyJob(10,testDataSet.advertisements[0],new Map<String,Object>(),testDataSet.skills,false);
			Map<Id, PeopleCloudHelper.Application> appMap = appList.getApplicationMap(); // a map which map candidate id to his job application
			accs.add(candPool);
			
			// Finish data preparation, re-enable dedup trigger
			pcSwitch.DeduplicationOnVCW_Active__c=true;
	        pcSwitch.Deduplicate_PC_Active__c = true;
	        // Exclude Corporate Candidate from dedup
	        upsert pcSwitch;
	        
	        System.runAs(DummyRecordCreator.platformUser) {
			// Start testing this function
			Test.startTest();
			Task sglTask = ActivityLogger.logSingleActivity('Insert a single task', appList.candidates[0], candPool, 'description for sgl task');
			system.assertEquals('Insert a single task', sglTask.Subject);
			system.assertEquals('description for sgl task', sglTask.Description);
			system.assertEquals(appList.candidates[0].id, sglTask.WhoId);
			system.assertEquals(candPool.id, sglTask.WhatId);
			system.assertNotEquals(null, sglTask.Id);
			
			List<Task> taskList = ActivityLogger.logActivities('abc', appList.candidates, accs, 'test');
			for(Task tmpTask:taskList){
				system.assertEquals('abc', tmpTask.Subject);
				system.assertEquals('test', tmpTask.Description);
				system.assertNotEquals(null,appMap.get(tmpTask.WhoId));
				system.assertEquals(candPool.id, tmpTask.WhatId);
				system.assertNotEquals(null, tmpTask.Id);			
			}
			
			List<Task> taskList2 = ActivityLogger.logActivitiesWithSameWhatId('abc', appList.candidates, candPool, 'test');
			for(Task tmpTask:taskList2){
				system.assertEquals('abc', tmpTask.Subject);
				system.assertEquals('test', tmpTask.Description);
				system.assertNotEquals(null,appMap.get(tmpTask.WhoId));
				system.assertEquals(candPool.id, tmpTask.WhatId);
				system.assertNotEquals(null, tmpTask.Id);			
			}
			Test.stopTest();
		}
		
	}
}