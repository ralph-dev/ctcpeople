@isTest
private class AppsAuthControllerTest {

    @isTest static void test_checkAppsAuth() {
        SFOauthToken__c sfToken = new SFOauthToken__c();
        sfToken.name = 'oAuthToken';
        sfToken.ConsumerKey__c = 'testKey';
        sfToken.SecretKey__c = 'testSecret';
        sfToken.OAuthToken__c = 'testToken';
        sfToken.RefreshToken__c = 'testRefresh';
        insert sfToken;
        AppsAuthController aac = new AppsAuthController();
        aac.checkAppsAuth();
        system.assert(aac.sfAppAuth);
    }
    
    @isTest static void test_authSFWebConnectorApp() {
        SFOauthToken__c token = new SFOauthToken__c();
        token.name = 'oAuthToken';
        token.App_Id__c = 'CTC_Connected_App';
        insert token;
        AppsAuthController aac = new AppsAuthController();
        PageReference pr =  aac.authSFWebConnectorApp();
        system.assert(pr!=null);
    }
    
    @isTest static void test_fillInConsumerInfo() {
        SFOauthToken__c token = new SFOauthToken__c();
        token.name = 'oAuthToken';
        token.App_Id__c = 'CTC_Connected_App';
        insert token;
        AppsAuthController aac = new AppsAuthController();
        PageReference pr =  aac.fillInConsumerInfo();
        system.assert(pr!=null);
    }

    @isTest static void test_reset() {
        SFOauthToken__c token = new SFOauthToken__c();
        token.name = 'oAuthToken';
        token.App_Id__c = 'CTC_Connected_App';
        insert token;
        Test.startTest();
        AppsAuthController aac = new AppsAuthController();
        PageReference pr = aac.reset();
        system.assert(pr != null);
        try {
            SFOauthToken__c tokenLeft = [select Id from SFOauthToken__c where name = 'oAuthToken'];
            system.assert(false, 'Token is not deleted successfully.');
        }
        catch (Exception e) {
            system.assert(e.getMessage().equals('List has no rows for assignment to SObject'));
        }
        Test.stopTest();
    }
    
}