/**
 * This is a test class for WebDocumentWrapper
 * 
 * Created by: Lina
 * Created on: 31/05/2016
 */

@isTest 
private class WebDocumentWrapperTest {
    
    static testmethod void unitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        // Create test data
        Web_Document__c doc1 = new Web_Document__c(name='doc1', Document_Type__c='Resume');
        insert doc1;
        sleep(1500);
        Web_Document__c doc2 = new Web_Document__c(name='doc2', Document_Type__c='Resume');
        insert doc2;
        sleep(1500);
        Web_Document__c doc3 = new Web_Document__c(name='doc3', Document_Type__c='Resume');
        insert doc3;
        
        Test.startTest();
        doc1 = [SELECT Id, Name, LastModifiedDate FROM Web_Document__c WHERE Id =: doc1.Id];
        doc2 = [SELECT Id, Name, LastModifiedDate FROM Web_Document__c WHERE Id =: doc2.Id];
        doc3 = [SELECT Id, Name, LastModifiedDate FROM Web_Document__c WHERE Id =: doc3.Id];
        
        List<WebDocumentWrapper> docList = new List<WebDocumentWrapper>();
        docList.add(new WebDocumentWrapper(doc3));
        docList.add(new WebDocumentWrapper(doc1));
        docList.add(new WebDocumentWrapper(doc2));

        // Sort the wrapper objects using the implementation of the 
        // compareTo method.        
        docList.sort();
        
        Test.stopTest();
        // Verify the sort order
        System.assertEquals('doc1', docList[0].doc.Name);
        System.assertEquals('doc2', docList[1].doc.Name);
        System.assertEquals('doc3', docList[2].doc.Name);
    	}
    }
    
    // Allow thread to sleep for 1s 
    // @param sleepTime = 1000
    public static void sleep(Integer sleepTime) {
        Long startTime = DateTime.now().getTime();
        Long finishTime = DateTime.now().getTime();
        while ((finishTime - startTime) < sleepTime) {
            //sleep for 9s
            finishTime = DateTime.now().getTime();
        }
    }    
}