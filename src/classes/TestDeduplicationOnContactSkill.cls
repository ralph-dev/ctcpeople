@isTest
public class TestDeduplicationOnContactSkill {
	private static testmethod void TestDeduplicationOnContactSkill(){
		System.runAs(DummyRecordCreator.platformUser) {
		List<Candidate_Skill__c> csclist = testCandidateSkillList();
		Set<String> cscliststring = new Set<String>();
		system.debug(' csclist = '+ csclist);
		for(Candidate_Skill__c csc : csclist){
			String cscstring = csc.Candidate__c + ':' +csc.Skill__c;
			cscliststring.add(cscstring);
		}
		system.debug(' cscliststring = '+ cscliststring);
		insert(csclist);
		if(cscliststring.size()>0){
			system.assert(csclist.size() > cscliststring.size());
		}
		else{
			system.assert(csclist.size() == cscliststring.size());
		}
		}
	}
	
	private static testmethod void TestDeduplicationOnContactSkillContainSkill(){
		System.runAs(DummyRecordCreator.platformUser) {
		List<Candidate_Skill__c> csclist = testCandidateListSkillContainsSkill();
		Set<String> cscliststring = new Set<String>();
		for(Candidate_Skill__c csc : csclist){
			String cscstring = csc.Candidate__c + ':' +csc.Skill__c;
			cscliststring.add(cscstring);
		}
		insert(csclist);
		system.assert(csclist.size() >= cscliststring.size());
		}
	}
	
	public static List<Candidate_Skill__c> testCandidateSkillList(){
		List<Candidate_Skill__c> testcscList = new List<Candidate_Skill__c>();
		RecordType CandidateRecordId = DaoRecordType.indCandidateRT;
		List<Candidate_Skill__c> ContacthasSkillList = [select Candidate__c from Candidate_Skill__c];
		
		List<Contact> testContactList = testInsertContact();
			
		List<Skill__c> testSkillList = testInsertSkills();
		for(Integer i=0; i<200; i++){
			Integer randomContact = (Integer)Math.rint(Math.random()*10);
			Candidate_Skill__c tempcsc = new Candidate_Skill__c();
			tempcsc.Candidate__c = (testContactList.get(randomContact)).Id;
			tempcsc.Skill__c = (testSkillList.get(randomContact)).Id;
			testcscList.add(tempcsc);
		}
		return testcscList;
	}
	
	public static List<Candidate_Skill__c> testCandidateListSkillContainsSkill(){
		List<Candidate_Skill__c> testcscList = new List<Candidate_Skill__c>();
		RecordType CandidateRecordId = DaoRecordType.indCandidateRT;
		List<Candidate_Skill__c> ContacthasSkillList = testCandidateSkillList();
		
		List<Contact> testContactList = new List<Contact>();
		List<Skill__c> testSkillList = new List<Skill__c>();
		if(ContacthasSkillList.size()>0){
			List<String> hasskillcandidateid = new List<String>();
			List<String> hasSkillIdList = new List<String>();
			for(Candidate_Skill__c hasskillcandidate : ContacthasSkillList){
				hasskillcandidateid.add(hasskillcandidate.Candidate__c);
				hasSkillIdList.add(hasskillcandidate.Skill__c);
			}
			testContactList = [select id from Contact where RecordTypeId =: CandidateRecordId.Id and id in: hasskillcandidateid limit 11 ];
			testSkillList = [select id from Skill__c where id in: hasSkillIdList];
		}
		else{
			testContactList =  testInsertContact();
			testSkillList = testInsertSkills();
		}		
		for(Integer i=0; i<200; i++){
			Integer randomContact = (Integer)Math.rint(Math.random()*10);
			Candidate_Skill__c tempcsc = new Candidate_Skill__c();
			tempcsc.Candidate__c = (testContactList.get(randomContact)).Id;
			tempcsc.Skill__c = (testSkillList.get(randomContact)).Id;
			testcscList.add(tempcsc);
		}
		return testcscList;
	}
	
	public static List<Contact> testInsertContact(){
		RecordType CandidateRecordType = DaoRecordType.indCandidateRT;
		List<Contact> testContactList = new List<Contact>();
		for(Integer i=0 ; i<11 ; i++){
			Contact testContact = new Contact(RecordTypeId = CandidateRecordType.id, LastName = 'Candidate'+i , Email = 'Candidate@test.ctc.com');
			testContactList.add(testContact);
		}
		insert testContactList;
		return testContactList;
	}
	
	public static List<Skill__c> testInsertSkills(){
		List<Skill__c> testSkillList = new List<Skill__c>();
		for(Integer i=0 ; i<11 ; i++){
			Skill__c testSkill = new Skill__c(Name = 'skill'+i , Ext_Id__c = 'skill_ext'+i);
			testSkillList.add(testSkill);			
		}
		insert testSkillList;
		return testSkillList;
	}
}