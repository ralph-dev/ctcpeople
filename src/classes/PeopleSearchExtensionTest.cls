@isTest
public class PeopleSearchExtensionTest {
	static testMethod void getContactFieldsList(){
		System.runAs(DummyRecordCreator.platformUser) {
		String fieldListString = PeopleSearchExtension.getContactFieldsList();
		system.assertNotEquals(null, fieldListString);
		}
	}
	
	
    static testMethod void searchCandidate(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	PeopleSearchDTO.SearchQuery search = new PeopleSearchDTO.SearchQuery();
    	search.criteria = new List<PeopleSearchDTO.SearchCriteria>();
    	
    	PeopleSearchOperatorFactory.Contain likeOp = new PeopleSearchOperatorFactory.Contain();
    	
    	PeopleSearchDTO.SearchCriteria crit = new PeopleSearchDTO.SearchCriteria();
    	crit.field = 'FirstName';
    	crit.operator = likeOp.value;
    	crit.type = 'STRING';
    	crit.values = '\"Emily\"';
    	search.criteria.add(crit);
    	search.resumeKeywords = 'test';
    	
    	List<PeopleSearchDTO.ContactSearchResult> contactSearchResult = PeopleSearchExtension.searchCandidate(search);
    	system.assert(contactSearchResult.size()==0);
    	}
    }
    
    static testMethod void getContactCriteriaList(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	List<PeopleSearchDTO.Field> fields = PeopleSearchExtension.getContactCriteriaList();
    	system.assert(fields.size()>0);
    	}
    }
    
    static testMethod void getFieldTypeList(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	List<PeopleSearchOperatorFactory.FieldType> fieldTypes = PeopleSearchExtension.getFieldTypeList();
    	system.assert(fieldTypes.size()>0);
    	}
    }
    
    static testMethod void testIsSkillSearchEnabled() {
    	System.runAs(DummyRecordCreator.platformUser) {
        PCAppSwitch__c pcSwitch = PCAppSwitch__c.getOrgDefaults();
        pcSwitch.Enable_Skill_Search__c = true;
        upsert pcSwitch;
        system.assertEquals(true, PeopleSearchExtension.isSkillSearchEnabled());
    	}
    }
    
    static testMethod void testGetAllSkillsList() {
    	System.runAs(DummyRecordCreator.platformUser) {
        DummyRecordCreator.createSkill();
        List<SkillSearchDTO.Skill> skills = PeopleSearchExtension.getAllSkillsList();
        system.assert(skills.size() > 0);
    	}
    }    
    
    static testMethod void apiDescriptor(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Map<String, RemoteAPIDescriptor.RemoteAction> actions = PeopleSearchExtension.apiDescriptor();
    	system.assert(actions.size()>0);
    	}
    }
    
}