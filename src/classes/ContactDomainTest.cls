@isTest//(seeAllData=true)
public class ContactDomainTest {
    private static  List<Contact> getTestContacts() {
    	
        Contact c1 = new Contact(lastName = 'test1', Birthdate = Date.newInstance(1990, 01, 02), AssistantName = 'dummy', Department = 'dummy');
        Contact c2 = new Contact(lastName = 'test2', Birthdate = Date.newInstance(1990, 01, 03), AssistantName = 'dummy1');
        Contact c3 = new Contact(lastName = 'test3', Birthdate = Date.newInstance(1990, 01, 04), AssistantName = 'expected', Department = 'dummy1');
        return new List<Contact>{c3, c2, c1};
    }
    
    private static testMethod void testSortContactsByCreateDate() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		List<Contact> cl = getTestContacts();
        
	        Test.startTest();
	        ContactDomain cd = new ContactDomain(cl);
	        List<Contact> ret = cd.sortContactsByDate('Birthdate');
	        System.assertEquals('test1', ret.get(0).lastName);
	        Test.stopTest();
    	}
    	
        
    }
    
    /**
     * the data type are tested: string, date
     */
    private static testMethod void testMergeContacts() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		List<Contact> cl = getTestContacts();
	        Date curTime = Date.today();
	        Contact baseContact = new Contact(lastName = 'test1', Birthdate = Date.newInstance(1990, 01, 01), Department = 'expected');
	        Contact lastContact = new Contact(lastName = 'testN', Department = 'expected', Birthdate = curTime);
	        cl.add(lastContact);
	        
	        Test.startTest();
	        ContactDomain cd = new ContactDomain(cl);
	        Contact c = cd.mergeContacts(baseContact, new Set<String>{'AssistantName', 'Birthdate'}, 'Birthdate');
	        System.assertEquals('test1', c.lastName);
	        System.assertEquals('expected', c.AssistantName);
	        System.assertEquals('expected', c.Department);
	        System.assertEquals(curTime, c.Birthdate);
	        Test.stopTest();
    	}
        
    }
    
    private static testMethod void testGetFieldSetKeyByName() {
    	System.runAs(DummyRecordCreator.platformUser) {
    		List<String> fns;
        
	        try {
	            fns = ContactDomain.getFieldSetKeyByName('deduplication_update_se');
	            fns = ContactDomain.getFieldSetKeyByName('deduplication_update_set');
	        } catch (Exception e){
	            System.assert(false);
	        }
    	}
        
    }
}