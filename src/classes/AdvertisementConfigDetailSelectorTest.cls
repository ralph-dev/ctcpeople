/**
 * This is the test class for AdvertisementConfigurationDetailSelector
 *
 * Created by: Lina Wei
 * Created on: 24/03/2017
 */

@isTest
private class AdvertisementConfigDetailSelectorTest {


    static testMethod void testGetActiveAdConfigDetailsByAccountIds (){
        // Create Data
        System.runAs(DummyRecordCreator.platformUser) {
            Test.startTest();
            Advertisement_Configuration__c activeSeekAccount = AdConfigDummyRecordCreator.createDummySeekAccount();
            AdConfigDetailDummyRecordCreator.createActiveTemplate(activeSeekAccount);
            AdConfigDetailDummyRecordCreator.createActiveLogo(activeSeekAccount);
            AdConfigDetailDummyRecordCreator.createActiveTemplate(activeSeekAccount);
            AdConfigDetailDummyRecordCreator.createInactiveLogo(activeSeekAccount);
            Advertisement_Configuration__c jxtAccount = AdConfigDummyRecordCreator.createDummyJxtAccount();
            AdConfigDetailDummyRecordCreator.createInactiveTemplate(jxtAccount);
            AdConfigDetailDummyRecordCreator.createInactiveLogo(jxtAccount);
            Set<String> accountIds = new Set<String>{
                    activeSeekAccount.Id, jxtAccount.Id
            }; 

            List<Advertisement_Configuration_Detail__c> acdList =
                    new AdvertisementConfigurationDetailSelector().getActiveAdConfigDetailsByAccountIds(accountIds);

            System.assertEquals(3, acdList.size());
            Test.stopTest();
        }
    }

    static testMethod void testGetTemplateById() {
        System.runAs(DummyRecordCreator.platformUser) {
            Advertisement_Configuration__c activeSeekAccount = AdConfigDummyRecordCreator.createDummySeekAccount();
            Advertisement_Configuration_Detail__c template =
                    AdConfigDetailDummyRecordCreator.createActiveTemplate(activeSeekAccount);

            Advertisement_Configuration_Detail__c selected =
                    new AdvertisementConfigurationDetailSelector().getTemplateById(template.Id);

            System.assertEquals(template.Id, template.Id);
        }
    }
}