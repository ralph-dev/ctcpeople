@isTest
private class JobBoardAccountClassTest {

    private static testMethod void testUpdate() {

        System.runAs(DummyRecordCreator.platformUser) {

            JobBoardAccountClass jbac = new JobBoardAccountClass();

            // Update CareerOne account
            StyleCategory__c scCareerOne = StyleCategoryDummyRecordCreator.createCareerOneAccount();
            jbac.careerOneAcc = ProtectedCredentialDummyRecordCreator.createCareerOneCredential(scCareerOne.Id);
            jbac.careerOneUName= 'newacc';
            System.assertEquals(null, jbac.getcareerOnePwd());
            jbac.setcareerOnePwd('newpss');
            jbac.updateAcc();
            ProtectedCredential coAccUp = CredentialSettingService.getCredential(scCareerOne.Id);
            System.assertEquals('newacc',coAccUp.username);
            System.assertEquals('newpss',coAccUp.password);

            // Update Trademe Account
            StyleCategory__c scTradeMe = StyleCategoryDummyRecordCreator.createTradeMeAccount();
            jbac.updateAcc();

            // Update LinkedIn account
            StyleCategory__c scLinkedIn = StyleCategoryDummyRecordCreator.createLinkedInAccount();
            OAuth_Service__c oauthServiceLinkedIn = OAuthServiceDummyRecordCreator.createOAuthServiceLinkedin();
            jbac.linkedinAcc = ProtectedCredentialDummyRecordCreator.createCareerOneCredential(oauthServiceLinkedIn.Id);
            System.assertNotEquals(null,jbac.linkedinAcc);
            jbac.setlinkedInCS('newCS');
            jbac.setlinkedInCK('newCK');
            jbac.updateAcc();
            ProtectedCredential linkedInUp = CredentialSettingService.getCredential(oauthServiceLinkedIn.Id);
            System.assertNotEquals(null,linkedInUp);
            System.assertEquals('newCS',linkedInUp.secret);
            System.assertEquals('newCK',linkedInUp.key);

            // Update JXTNZ account
            StyleCategory__c scJXTNZ = StyleCategoryDummyRecordCreator.createJXTNZAccount();
            jbac.jxtNZAcc = ProtectedCredentialDummyRecordCreator.createJXTNZCredential(scJXTNZ.Id);
            jbac.jxtnzUName = 'newAcc';
            jbac.setjobxPwd('newPwdJXTNZ');
            jbac.updateAcc();
            ProtectedCredential jxtNZUp = CredentialSettingService.getCredential(scJXTNZ.Id);
            System.assertEquals('newAcc',jxtNZUp.username);
            System.assertEquals('newPwdJXTNZ',jxtNZUp.password);

            // Update Indeed account
            Advertisement_Configuration__c adcIndeed = AdConfigDummyRecordCreator.createDummyIndeedAccount();
            jbac.indeedAcc = ProtectedCredentialDummyRecordCreator.createIndeedCredential(adcIndeed.Id);
            jbac.setindeedToken('newToken');
            jbac.updateAcc();
            ProtectedCredential indeedAccUp = CredentialSettingService.getCredential(adcIndeed.Id);
            System.assertEquals('newToken',indeedAccUp.token);

            // Update JXT account
            Advertisement_Configuration__c adcJXT = AdConfigDummyRecordCreator.createDummyJxtAccount();
            jbac.jxtAcc = ProtectedCredentialDummyRecordCreator.createJXTCredential(adcJXT.Id);
            jbac.jxtUName = 'newACC';
            jbac.setjxtPwd('newPWD');
            jbac.updateAcc();
            ProtectedCredential jxtAccUp = CredentialSettingService.getCredential(adcJXT.Id);
            System.assertEquals('newACC',jxtAccUp.username);
            System.assertEquals('newPWD',jxtAccUp.password);

        }
    }

    private static testMethod void testCreate() {

        System.runAs(DummyRecordCreator.platformUser) {

            JobBoardAccountClass jbac = new JobBoardAccountClass();

            system.assertNotEquals(null, jbac.getOptions());
            system.assertEquals(null, jbac.checkAcc());
            system.assertEquals(null, jbac.displayDetail());

            // Create CareerOne account
            jbac.selected_website = 'CareerOne';
            jbac.setString1('testCO1');
            jbac.setString2('testCO2');
            jbac.setString3('testCO3');
            jbac.seekApplicationForm = true;
            jbac.createnew();
            System.assertEquals('testCO1',jbac.careerOneAcc.username);
            System.assertEquals('testCO3',jbac.careerOneAcc.password);

            // Create LinkedIn account
            jbac.selected_website = 'LinkedIn';
            jbac.setString1('test1');
            jbac.setString2('test2');
            jbac.setString3('test3');
            jbac.setString4('test4');
            jbac.setString5('test5');
            jbac.setString6('test6');
            jbac.setString7('test7');
            jbac.createnew();
            System.assertEquals('test3',jbac.linkedinAcc.key);
            System.assertEquals('test4',jbac.linkedinAcc.secret);

            // Create Trademe account
            jbac.selected_website = 'TradeMe';
            jbac.setString1('test1');
            jbac.setString2('test2');
            jbac.setString3('test3');
            jbac.createnew();

            // Create JXT account
            jbac.selected_website = 'Jxt';
            jbac.setString1('test1');
            jbac.setString2('test2');
            jbac.setString3('test3');
            jbac.createnew();
            System.assertEquals('test2',jbac.jxtAcc.username);
            System.assertEquals('test3',jbac.jxtAcc.password);

            // Create JXTNZ account
            jbac.selected_website = 'JXT_NZ';
            jbac.setString1('test1');
            jbac.setString2('test2');
            jbac.setString3('test3');
            jbac.setString4('test4');
            jbac.createnew();
            System.assertEquals('test3',jbac.jxtNZAcc.username);
            System.assertEquals('test4',jbac.jxtNZAcc.password);

            // Create Indeed account
            jbac.selected_website = 'Indeed';
            jbac.setString1('test1');
            jbac.setString2('test2');
            jbac.setString3('test3');
            jbac.createnew();
            System.assertEquals('test3',jbac.indeedAcc.token);

        }
    }
}