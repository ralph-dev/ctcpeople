@isTest
private with sharing class JXTEmailParserTest {

   	private static Messaging.InboundEmail email;
    private static JXTEmailParser parser; 
    
    static testMethod void testParse(){
    	System.runAs(DummyRecordCreator.platformUser) {
        email = EmailDummyRecordCreator.createEmail('www.jxt.solutions CV  - hello world', 'test content  firstName:test', 'test@test.com', 
                                                EmailDummyRecordCreator.createBinaryAttachment(new Map<String, String>{ 'latest-CV-resume.doc' => 'test resume' } ));
    	parser = new JXTEmailParser(email);
    	JXTContent content = parser.parseJxtEmail();
    	System.assertEquals(1,content.files.size());
    	}
    	
    }
    
    static testMethod void testGetValueInblockByTitle() {
        System.runAs(DummyRecordCreator.platformUser) {
        String firstName = JXTEmailParser.getValueInblockByTitle('firstName', ':', 'lastName' ,'test content  firstName:\ntest \nlastName:test') ;
        system.assertEquals('test', firstName);
        }
    }
    
}