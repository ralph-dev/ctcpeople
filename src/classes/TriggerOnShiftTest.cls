@isTest
private class TriggerOnShiftTest {
	final static String ROSTER_STATUS_FILLED = 'Filled';
	
    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        Account acc = TestDataFactoryRoster.createAccount();
        
        System.debug(acc.Id);
        
    	Contact con = TestDataFactoryRoster.createContact();
        
        System.debug(con.Id);
        
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        
        System.debug(vac.Id);
        
        Shift__c shift = TestDataFactoryRoster.createShift(vac);
        
        System.debug(shift.Id);
        
        Days_Unavailable__c roster = TestDataFactoryRoster.createRoster(con, shift, acc,ROSTER_STATUS_FILLED);
        
        System.debug(roster.Id);
        
       	delete shift;
        
        System.assertEquals(0, [select count() from Shift__c where Id=: shift.Id]);
    	}
        
        
    }
}