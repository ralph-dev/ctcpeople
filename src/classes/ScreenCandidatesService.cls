/**
 * This is the service class for screen candidates
 *
 * Created by: Lina Wei
 * Created on: 18/04/2017
 */

public with sharing class ScreenCandidatesService {

    public String getScreenCandidatesDisplayColumns(){
        return new PeopleSearchService().initUserDetail(true).getScreenCandidateColumnJSONFromUserPreference();
    }

    public String saveScreenCandidatesDisplayColumn(String jsonColumn){
        DisplayColumnHandler handler = new PeopleSearchService().initUserDetail(true);
        handler.updateScreenCandidateColumnJSON(jsonColumn);
        return getScreenCandidatesDisplayColumns();
    }
    public Map<String,String> getObjectsLabelName(){
        return new PeopleSearchService().initUserDetail(true).getObjectsLabelName();
	}
}