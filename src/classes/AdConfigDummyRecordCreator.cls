/**
 * This is the dummy record creator for Advertisement_Configuration__c object
 * This class is used for testing purpose
 *
 * Created by: Lina Wei
 * Created on: 24/03/2017
 *
 */
@isTest
public with sharing class AdConfigDummyRecordCreator {

    public static Advertisement_Configuration__c createDummyIndeedAccount() {
        Advertisement_Configuration__c ac = new Advertisement_Configuration__c();
        ac.name = 'Test ad account';
        ac.recordTypeId = DaoRecordType.IndeedAdRT.Id;
        ac.Advertisement_Account__c = '132456';
        ac.Active__c = true;
       // ac.Indeed_API_Token__c = 'indeedtoken';
        //insert ac;
        
        new AdvertisementConfigurationSelector().insertIndeedProtectedAccount(ac,'indeedtoken' );
        return ac;
    }

    public static Advertisement_Configuration__c createDummyJxtAccount() {
        Advertisement_Configuration__c ac = new Advertisement_Configuration__c();
        ac.name = 'Test ad account';
        ac.recordTypeId = DaoRecordType.jxtAccountRT.Id;
        ac.Advertisement_Account__c = '132456';
        ac.Active__c = true;
        
        //commented by andy for security review II
        //ac.Indeed_API_Token__c = 'test token';
        //insert ac;
        
        //added by andy for security review II
        new AdvertisementConfigurationSelector().insertJXTProtectedAccount(ac,'test user' ,'test password');
        
        return ac;
    }

    public static Advertisement_Configuration__c createDummySeekAccount() {
        Advertisement_Configuration__c ac = new Advertisement_Configuration__c();
        ac.name = 'Test ad account';
        ac.recordTypeId = DaoRecordType.seekAccountRT.Id;
        ac.Advertisement_Account__c = '132456';
        ac.Active__c = true;
        ac.Account_Type__c = 'Selective';
        ac.Seek_Application_Export_Enabled__c = true;
        insert ac;
        return ac;
    }

    public static Advertisement_Configuration__c createDummySeekAccountWithoutInsert(String name) {
        Advertisement_Configuration__c ac = new Advertisement_Configuration__c();
        ac.name = name;
        ac.recordTypeId = DaoRecordType.seekAccountRT.Id;
        ac.Advertisement_Account__c = '132456';
        ac.Active__c = true;
        ac.Account_Type__c = 'Selective';
        ac.Seek_Application_Export_Enabled__c = true;
        return ac;
    }

    public static Advertisement_Configuration__c createInactiveSeekAccount() {
        Advertisement_Configuration__c ac = new Advertisement_Configuration__c();
        ac.name = 'Test ad account';
        ac.recordTypeId = DaoRecordType.seekAccountRT.Id;
        ac.Advertisement_Account__c = '132456';
        ac.Active__c = false;
        ac.Account_Type__c = 'Selective';
        ac.Seek_Application_Export_Enabled__c = true;
        insert ac;
        return ac;
    }


}