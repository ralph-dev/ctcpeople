public with sharing class ApplicationQuestionSetSelector extends CommonSelector{


	public ApplicationQuestionSetSelector(){
		super(Application_Question_Set__c.SObjectType);
	}
    

    public override List<Schema.SObjectField> getSObjectFieldList(){
        return new List<Schema.SObjectField>{
                Application_Question_Set__c.Id,
                Application_Question_Set__c.OwnerId,
                Application_Question_Set__c.Name,
                Application_Question_Set__c.Advertisement_Account__c,
                Application_Question_Set__c.Active__c,
                Application_Question_Set__c.ID_API_Name__c, 
                Application_Question_Set__c.Default__c
        };
    }

	/**
     * Get a list of application question set by given Advertisement Configuration ids
     * @param ids      list of application question set
     * @return List<Application_Question_Set__c>
     * Added by Lina on 08/03/2017
	 */
    public List<Application_Question_Set__c> getActiveSeekScreenByAccountIds(Set<Id> accountIds) {
        String seekScreenRTId = DaoRecordType.seekScreenRT.Id;
        SimpleQueryFactory qf = simpleQuery()
                                    .selectFields(getSObjectFieldList())
                                    .setCondition('Advertisement_Account__c IN: accountIds  AND RecordTypeId =: seekScreenRTId AND Active__c = true');
        return Database.query(qf.toSOQL());
    }
}