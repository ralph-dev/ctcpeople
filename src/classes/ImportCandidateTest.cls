@isTest
private class ImportCandidateTest
{
	@isTest
	static void testImportCandidate() {
		DummyRecordCreator.DefaultDummyRecordSet dms = DummyRecordCreator.createDefaultDummyRecordSet();

		PCAppSwitch__c pcAppSwitch = PCAppSwitch__c.getinstance();
		pcAppSwitch.Enable_Daxtra_Skill_Parsing__c = true;
		upsert pcAppSwitch;
        
        ApexPages.Standardsetcontroller setController = new ApexPages.Standardsetcontroller(dms.candidates); 
        ImportCandidate ic = new ImportCandidate(setController);
        boolean enableskillgroup = ic.isSkillParsingEnabled();       
        if(enableskillgroup){
        	system.assert(ic.JSONSringSkillGroups!=null);
        }
        else{
        	system.assertEquals(ic.JSONSringSkillGroups, 'null');
        }    
	}
}