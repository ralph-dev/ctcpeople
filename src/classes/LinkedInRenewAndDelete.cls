public with sharing class LinkedInRenewAndDelete {
	public static String linkedInPostURL = 'https://api.linkedin.com/v1/jobs';
	public Boolean showExtraErrorInfo{get; set;}
	
	//Init Error Message for customer
    public PeopleCloudErrorInfo errormesg; 
    public Boolean msgSignalhasmessage{get ;set;} //show error message or not
    
    public String adtemplateid;

    private ApexPages.StandardController stdCon;
	
	public LinkedInRenewAndDelete(Apexpages.StandardController stdController){
		showExtraErrorInfo = false;
		adtemplateid = ApexPages.currentPage().getParameters().get('id');
        stdCon = stdController;
	}
	
	//Edit LinkedIn Ad
	public PageReference renewAd(){		
		Advertisement__c tempad = new Advertisement__c();
        CommonSelector.checkRead(Advertisement__c.SObjectType, 'id, Job_Title__c, Country__c, LinkedIn_Postal_code__c, Job_content__c, Location__c,Status__c,'
					+'Skills_Experience__c, Company_Description__c, Job_Function__c,Industry__c,Job_Posting_Status__c,' 
					+'Job_Type_Text__c, Experience_Level__c, Show_Poster__c, Poster_Role__c, Salary_Description__c, Has_Referral_Fee__c,'
					+'Application_URL__c, LinkedIn_Account__r.Advertiser_Id__c, LinkedIn_Account__r.LinkedIn_Partner_ID__c ');
		tempad = [select id, Job_Title__c, Country__c, LinkedIn_Postal_code__c, Job_content__c, Location__c,Status__c,
					Skills_Experience__c, Company_Description__c, Job_Function__c,Industry__c,Job_Posting_Status__c, 
					Job_Type_Text__c, Experience_Level__c, Show_Poster__c, Poster_Role__c, Salary_Description__c, Has_Referral_Fee__c,
					Application_URL__c, LinkedIn_Account__r.Advertiser_Id__c, LinkedIn_Account__r.LinkedIn_Partner_ID__c from Advertisement__c where id=: adtemplateid];
		
		String requestbody = LinkedInGenerateXMLFeed.generateRenewXMLFeed(tempad);
		String url = linkedInPostURL + '/partner-job-id='+UserInfo.getOrganizationId().subString(0, 15)+':'+String.valueOf(tempad.Id).subString(0, 15);
		String method = 'PUT';
		Integer postLinked = LinkedInPostExecute.fetchLinkedInPostExecute(requestbody, method, url);
		//system.debug('postLinked ='+ postLinked);
		if(postLinked == 200){
			tempad.Placement_Date__c = system.today();
			fflib_SecurityUtils.checkFieldIsUpdateable(Advertisement__c.SObjectType, Advertisement__c.Placement_Date__c);
			update tempad;
			customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAP_JOB_POST_SUCCESS);
       		return stdCon.view();
		}else{ 
			showExtraErrorInfo = true;
			if(postLinked == 400){
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_BAD_REQUEST);
			}else if(postLinked == 401){
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_UNAUTHORIZED);
			}else if(postLinked == 402){
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_PAYMENT_REQUIRED);
			}else if(postLinked == 404){
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_NO_JOB_AD_FOUND);
			}else if(postLinked == 409){
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_DUPLICATE_JOB);
			}else {
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_JOB_IS_CLOSED);
			}
			return null;
		}
	}
	
	//Delete LinkedIn Ad
	public PageReference deleteAd(){
		Advertisement__c tempad = new Advertisement__c();
        CommonSelector.checkRead(Advertisement__c.SObjectType, 'id, Job_Title__c, Country__c, LinkedIn_Postal_code__c, Job_content__c, Location__c,Status__c,'
					+'Skills_Experience__c, Company_Description__c, Job_Function__c,Industry__c,Job_Posting_Status__c,' 
					+'Job_Type_Text__c, Experience_Level__c, Show_Poster__c, Poster_Role__c, Salary_Description__c, Has_Referral_Fee__c,'
					+'Application_URL__c, LinkedIn_Account__r.Advertiser_Id__c, LinkedIn_Account__r.LinkedIn_Partner_ID__c ');
		tempad = [select id, Job_Title__c, Country__c, LinkedIn_Postal_code__c, Job_content__c, Location__c,Status__c,
					Skills_Experience__c, Company_Description__c, Job_Function__c,Industry__c,Job_Posting_Status__c, 
					Job_Type_Text__c, Experience_Level__c, Show_Poster__c, Poster_Role__c, Salary_Description__c, Has_Referral_Fee__c,
					Application_URL__c, LinkedIn_Account__r.Advertiser_Id__c, LinkedIn_Account__r.LinkedIn_Partner_ID__c from Advertisement__c where id=: adtemplateid];
					
		String url = linkedInPostURL + '/partner-job-id='+UserInfo.getOrganizationId().subString(0, 15)+':'+String.valueOf(tempad.Id).subString(0, 15);
		String method = 'DELETE';
		String requestbody = '';
		Integer postLinked = LinkedInPostExecute.fetchLinkedInPostExecute(requestbody, method, url);
		//system.debug('postLinked ='+ postLinked);
		if(postLinked == 204){
			tempad.Status__c = 'Archived';
			tempad.Job_Posting_Status__c = 'deleted';
			//check FLS
			List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
				Advertisement__c.Status__c,
				Advertisement__c.Job_Posting_Status__c
			};
			fflib_SecurityUtils.checkUpdate(Advertisement__c.SObjectType, fieldList);
			update tempad;
			customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAP_JOB_POST_SUCCESS);
			//system.debug('tempad.Status__c ='+ tempad.Status__c+ 'tempad.Job_Posting_Status__c ='+ tempad.Job_Posting_Status__c);
			return stdCon.view();
		}else{ 
			showExtraErrorInfo = true;
			if(postLinked == 400){
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_BAD_REQUEST);
			}else if(postLinked == 401){
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_UNAUTHORIZED);
			}else if(postLinked == 402){
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_PAYMENT_REQUIRED);
			}else if(postLinked == 404){
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_NO_JOB_AD_FOUND);
			}else if(postLinked == 409){
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_DUPLICATE_JOB);
			}else {
				customerErrorMessage(PeopleCloudErrorInfo.LINKEDINAD_JOB_IS_CLOSED);
			}
			return null;
		}
	}
	
	public void customerErrorMessage(Integer msgcode){
   		errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        	msgSignalhasmessage = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
            	ApexPages.addMessage(newMessage);
            } 
    }              
    
    
    //Return to Previous Page
	public PageReference returntoPreviousPage(){
		return stdCon.view();
	}

}