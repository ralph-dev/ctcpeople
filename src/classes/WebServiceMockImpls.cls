@isTest
global with sharing class WebServiceMockImpls {


	global class S2SJobSiteCheckClassMockImpl4FailureScene implements WebServiceMock{
		global void doInvoke(Object stub,
							Object request,
							Map<String,Object> response, 
							String endpoint,
							String soapAction,
							String requestName,
							String responseNS,
							String responseName,
							String responseType){
		
			// in this mock, always response 0 which mean users are not authorized for job posting
			S2SJobSiteCheckClass.checkJobSitesResponse_element resEle= new S2SJobSiteCheckClass.checkJobSitesResponse_element();
			resEle.result = '0';
			response.put('response_x',resEle);
		}	
	}

	global class S2SJobSiteCheckClassMockImpl4SuccessScene implements WebServiceMock{
		global void doInvoke(Object stub,
							Object request,
							Map<String,Object> response, 
							String endpoint,
							String soapAction,
							String requestName,
							String responseNS,
							String responseName,
							String responseType){
		
			// in this mock, always response 'mycareer.com;seek.com;careerone.com;trademe.com;jxt;jobx.co.nz' which mean 
			// users are authorized for job posting to all job boards
			S2SJobSiteCheckClass.checkJobSitesResponse_element resEle= new S2SJobSiteCheckClass.checkJobSitesResponse_element();
			resEle.result = 'mycareer.com;seek.com;careerone.com;trademe.com;jxt;jobx.co.nz';
			response.put('response_x',resEle);
		}	
	}
	
	global class S2SAuthorizationVerificationServiceMockImpl4SuccessScene implements WebServiceMock{
		global void doInvoke(Object stub,
							Object request,
							Map<String,Object> response, 
							String endpoint,
							String soapAction,
							String requestName,
							String responseNS,
							String responseName,
							String responseType){
		
			
			// This is a test method for legacy program which was done long long time ago, description of code 0~3 are copied from the
			// original web service code.
		    //** code : 1 -- account information provided empty 
			//** code : 2 -- no record in CTC org instance for this account  
			//** code : 0 -- Authorization successfully 
			//** code : 3 -- not in service period
			// code 4,5,6, not sure what they are for. These codes are in the web service side with no documentation.
			// No one knows what these codes stand for, not even god.
    		// In this mock, always response 0 which mean successful authorization
			S2SAuthorizationVerificationService.checkAccountResponse_element resEle= new S2SAuthorizationVerificationService.checkAccountResponse_element();
			resEle.result = '0';
			response.put('response_x',resEle);
		}	
	}
	
	global class S2SAuthorizationVerificationServiceMockImpl4FailureScene implements WebServiceMock{
		global void doInvoke(Object stub,
							Object request,
							Map<String,Object> response, 
							String endpoint,
							String soapAction,
							String requestName,
							String responseNS,
							String responseName,
							String responseType){
		
			
			// This is a test method for legacy program which was done long long time ago, description of code 0~3 are copied from the
			// original web service code.
		    //** code : 1 -- account information provided empty 
			//** code : 2 -- no record in CTC org instance for this account  
			//** code : 0 -- Authorization successfully 
			//** code : 3 -- not in service period
			// code 4,5,6, not sure what they are for. These codes are in the web service side with no documentation.
			// No one knows what these codes stand for, not even god.
    		// In this mock, always response 1 which mean failure authorization
			S2SAuthorizationVerificationService.checkAccountResponse_element resEle= new S2SAuthorizationVerificationService.checkAccountResponse_element();
			resEle.result = '1';
			response.put('response_x',resEle);
		}	
	}
	
	
	global class CTCWSMockImpl4UploadApplicationFormSuccessScene implements WebServiceMock{
		global void doInvoke(Object stub,
							Object request,
							Map<String,Object> response, 
							String endpoint,
							String soapAction,
							String requestName,
							String responseNS,
							String responseName,
							String responseType){
		
			
    		// In this mock, always response true which mean successful upload of application form
			CTCWS.transferResponse resEle= new CTCWS.transferResponse();
			resEle.return_x = true;
			response.put('response_x',resEle);
		}	
	}
	
	global class CTCWSMockImpl4UpdateJobPostingDetailSuccess implements WebServiceMock{
		global void doInvoke(Object stub,
							Object request,
							Map<String,Object> response, 
							String endpoint,
							String soapAction,
							String requestName,
							String responseNS,
							String responseName,
							String responseType){
		
			
    		// In this mock, always response true which mean successful 
			CTCWS.updateJobPostingWithTitleByVidResponse resEle= new CTCWS.updateJobPostingWithTitleByVidResponse();
			resEle.return_x = 1;
			response.put('response_x',resEle);
		}	
	}
	
	global class CTCWSMockImpl4InsertJobPostingDetailSuccess implements WebServiceMock{
		global void doInvoke(Object stub,
							Object request,
							Map<String,Object> response, 
							String endpoint,
							String soapAction,
							String requestName,
							String responseNS,
							String responseName,
							String responseType){
		
			
    		// In this mock, always response true which mean successful 
			CTCWS.insertJobPostingDetailResponse resEle= new CTCWS.insertJobPostingDetailResponse();
			resEle.return_x = true;
			response.put('response_x',resEle);
		}	
	}
	
	
	global class PartnerAPIMockImpl4LoginSuccessScene implements WebServiceMock{
		global void doInvoke(Object stub,
							Object request,
							Map<String,Object> response, 
							String endpoint,
							String soapAction,
							String requestName,
							String responseNS,
							String responseName,
							String responseType){
		
			
    		// In this mock, always response true which mean successful upload of application form
			/*PartnerAPI.LoginResult resEle= new PartnerAPI.LoginResult();
			resEle.metadataServerUrl='';
		    resEle.passwordExpired=false;
		    resEle.sandbox=false;
		    resEle.serverUrl='';
		    resEle.sessionId='1231452123';
		    resEle.userId='123';
			response.put('response_x',resEle);*/
		}	
	}		
	
	
}