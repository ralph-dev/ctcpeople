/*
	
	Author Jack 15 Aug 2013
	This Class is for PopulateCompOnVCW Trigger

*/
public with sharing class PopulateCompanyOnVCW {
	public static void PopulateCompanyOnVCW(List<Placement_Candidate__c> inputCM){
		List<Placement_Candidate__c> updateList = new List<Placement_Candidate__c>();
	    Set<String> vacIdList = new Set<String>();
	    
	    for(Placement_Candidate__c cm:inputCM){
	        if(cm.Placement__c!=null && cm.Company__c == null){
	            updateList.add(cm);
	            vacIdList.add(cm.Placement__c);
	        }
	    }
	    List<Placement__c> vacObjList = new List<Placement__c>();
	    Map<String,String> compMap = new Map<String,String>();
	    if(vacIdList.size()>0){
	    	CommonSelector.checkRead(Placement__c.sObjectType, ' Id,Company__c');
	        vacObjList =[select Id,Company__c from Placement__c where id in :vacIdList];
	        
	        //put selected object in a map for an easy access
	        for(Placement__c vac:vacObjList){
	            compMap.put(vac.Id,vac.Company__c);
	        }
	    }else{
	        return;
	    }
	    for(Placement_Candidate__c cm:updateList){
	        cm.Company__c = compMap.get(cm.Placement__c); 
	    }
	}
}