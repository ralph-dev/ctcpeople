@isTest
private class PeopleSearchServiceTest {

	private static testMethod void testGetPicklistValue() {
	    system.runAs(DummyRecordCreator.platformUser) {
	        PeopleSearchService pSearchService = new PeopleSearchService();
	        pSearchService.getAllContactFields();
            List<PeopleSearchDTO.NamedValue> namedValues = pSearchService.getPicklistValue(PeopleSearchDTO.RECORDTYPEID);
            system.assert(namedValues.size()>0);
	    }
	}

}