@isTest
private class TestDisplayColumnHandler {
	
	
	public static testmethod void testCreatingNewUserPreference() {  
		System.runAs(DummyRecordCreator.platformUser) {
		String userid = Userinfo.getUserId();
		String recordTypeId = DaoRecordType.googleSearchUserPerformacne.id;
		String googleSearchUserPreferenceName = 'GoogleSearchUserPreferenceName_'+userid;
		String newGoogleSearchUserPreferenceName = 'GoogleSearchUserPreferenceName_'+userid + '_new';
		
		DisplayColumnHandler handler = new DisplayColumnHandler(userid,recordTypeId,newGoogleSearchUserPreferenceName);
		User_Preference__c up = new User_Preference__c();
		up = handler.createNewUserPreference();
		system.assert(up != null);
		}
	} 

}