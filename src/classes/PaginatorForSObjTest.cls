@isTest
private class PaginatorForSObjTest {

    private static void testPaginator(){
        PaginatorForSObj p = new PaginatorForSObj(new List<Sobject>());
        system.assertEquals(p.getCurrentPage(),0);
        system.assertNotEquals(p.getFirstPageCollection(),null);
        system.assertEquals(p.getFirstPageCollection().size(),0);
       
       RecordType candRT = DaoRecordType.indCandidateRT;
       List<Contact> conList = new List<Contact>(); 
       for(Integer i=0;i<1000;i++){
       		Contact cand = new Contact(RecordTypeId=candRT.Id,LastName='candidate 1'+i,Email='Cand1'+i+'@testcanddispatcher.ctc.test');
        	conList.add(cand);
        }
        insert conList;
        
        PaginatorForSObj pagina = new PaginatorForSObj(conList);
        pagina.changerecPerpage(100);
        System.assertEquals(pagina.getTotalRecNumber(),1000);
        System.assertEquals(pagina.totalPages,10);
        pagina.hasNextPage();
        pagina.hasPrevPage();
        pagina.getFirstPageCollection();
        pagina.getLastPageCollection();
        pagina.getNthPageCollection(1);
        pagina.getNextPageCollection();
        pagina.getPrevPageCollection();
    }
}