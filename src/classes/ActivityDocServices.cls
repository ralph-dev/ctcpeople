public with sharing class ActivityDocServices {
    public static Integer UploaddocumentsByEc2(String jsonString, String sessionid,String surl){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        String searchEndpoint = EndPoint.getcpewsEndpoint()+'/ctcenhanceduploadfiles/Service/candidate-docs';
        
        jsonString='activityParams=' + '{"activityDocs":' + jsonString + '}'; //Wrap Json string to activityDocs JSON string, according Alvin Request
        
        req.setEndpoint(searchEndpoint);
        req.setMethod('POST');        
        //commented by andy yang for security review
        //req.setHeader('salesforcesession', sessionid);
        req.setHeader('surl', surl);
        req.setTimeout(120000);
        req.setBody(jsonString); 
        HttpResponse res = new HttpResponse();
        if(!Test.isRunningTest()) {
            res = h.send(req);
        }else {
            res.setStatusCode(200);
            res.setBody('Successfully');
        }          
        Integer responsecode = res.getStatusCode();
        return responsecode;
    }
    
    @future(callout=true) 
     public static void ImportCandidateByEc2(String jsonString, String sessionid,String surl){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        String searchEndpoint = EndPoint.getcpewsEndpoint()+'/CTCPeopleUploadFiles/docs';
        
        jsonString='activityParams=' + '{"activityDocs":' + jsonString + '}'; //Wrap Json string to activityDocs JSON string, according Alvin Request
        
        req.setEndpoint(searchEndpoint);
        req.setMethod('POST');      
        //commented by Ralph, no longer use sessionID
        //req.setHeader('salesforcesession', sessionid);
        req.setHeader('surl', surl);
        req.setTimeout(120000);
        req.setBody(jsonString); 
        HttpResponse res = new HttpResponse();
        if(!Test.isRunningTest()) {
            res = h.send(req);
        }else {
            res.setStatusCode(200);
            res.setBody('Successfully');
        }          
        Integer responsecode = res.getStatusCode();
    }
    
    @future(callout=true) 
     public static void uploadDocumentByActivityAtt(String jsonString, String sessionid,String surl){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        String searchEndpoint = EndPoint.getcpewsEndpoint()+'/ctcenhanceduploadfiles/Service/candidate-docs';
        
        jsonString='activityParams=' + '{"activityDocs":' + jsonString + '}'; //Wrap Json string to activityDocs JSON string, according Alvin Request
        
        req.setEndpoint(searchEndpoint);
        req.setMethod('POST');     
        //commented by andy yang for security review   
        //req.setHeader('salesforcesession', sessionid);
        req.setHeader('surl', surl);
        req.setTimeout(120000);
        req.setBody(jsonString);
        HttpResponse res = new HttpResponse();
        if(!Test.isRunningTest()) {
            res = h.send(req);
        }else {
            res.setStatusCode(200);
            res.setBody('Successfully');
        }          
        Integer responsecode = res.getStatusCode();
    }
}