/**
 * This is the test class for LinkedINCSAAdminController
 * Created by: Lina
 * Created Date: 08/04/2016
 **/

@isTest
public class LinkedInCSAAdminControllerTest {
    static testmethod void unitTest() {
    	
        // Create data
        LinkedIn_CSA__c csa = new LinkedIn_CSA__c();
        csa.API_Key__c = 'test';
        csa.Customer_Id__c = 'test';
        csa.Name = UserInfo.getOrganizationName();
        insert csa;
        CTCPeopleSettingHelper__c linkedInReg = new CTCPeopleSettingHelper__c();
        CTCPeopleSettingHelperServices ctcServices = new CTCPeopleSettingHelperServices();
		CTCPeopleSettingHelperSelector ctcSelector = new CTCPeopleSettingHelperSelector();
		String rTID=ctcServices.getRecordTypeId('LinkedIn_CSA_Registration');
		linkedInReg.Name = 'test';
		linkedInReg.LinkedIn_Contact_Email__c = 'test@test.com';
		linkedInReg.recordTypeId = rTID;
		insert linkedInReg;
	
		LinkedIn_CSA__c csaBefore = LinkedInCSASelector.getLinkedInCSA();
		system.assertEquals('test', csaBefore.API_Key__c);
		system.assertEquals('test', csaBefore.Customer_Id__c);
        
        LinkedInCSAAdminController csaAdminCon = new LinkedInCSAAdminController();
        csaAdminCon.setapiKey('12345');
        csaAdminCon.customerId = '12345';
        csaAdminCon.testFlag = true;
        csaAdminCon.saveCSA();
        List<ApexPages.Message> msgList = ApexPages.getMessages();
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals(ApexPages.Severity.CONFIRM, msg.getSeverity());
        }
        LinkedIn_CSA__c csaAfter = LinkedInCSASelector.getLinkedInCSA();
		system.assertEquals('12345', csaAfter.API_Key__c);
		system.assertEquals('12345', csaAfter.Customer_Id__c);
    	
    
    }
    
    static testmethod void testNoCSAAndSendEmailFail() {
    	
        // Create data
        CTCPeopleSettingHelper__c linkedInReg = new CTCPeopleSettingHelper__c();
        CTCPeopleSettingHelperServices ctcServices = new CTCPeopleSettingHelperServices();
		CTCPeopleSettingHelperSelector ctcSelector = new CTCPeopleSettingHelperSelector();
		String rTID=ctcServices.getRecordTypeId('LinkedIn_CSA_Registration');
		linkedInReg.Name = 'test';
		linkedInReg.LinkedIn_Contact_Email__c = 'test@test.com';
		linkedInReg.recordTypeId = rTID;
		insert linkedInReg;
        
        LinkedInCSAAdminController csaAdminCon = new LinkedInCSAAdminController();
        csaAdminCon.setapiKey('12345');
        csaAdminCon.customerId = '12345';
        csaAdminCon.testFlag = false;
        csaAdminCon.saveCSA();
        
        LinkedIn_CSA__c csaAfter = LinkedInCSASelector.getLinkedInCSA();
		system.assertEquals('12345', csaAfter.API_Key__c);
		system.assertEquals('12345', csaAfter.Customer_Id__c);
		List<ApexPages.Message> msgList = ApexPages.getMessages();
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
        }
    	
    }
    
    static testmethod void testNoEmail() {
    	System.runAs(DummyRecordCreator.platformUser) {
        LinkedInCSAAdminController csaAdminCon = new LinkedInCSAAdminController();
        csaAdminCon.setapiKey('12345');
        csaAdminCon.customerId = '12345';
        csaAdminCon.saveCSA();
        List<ApexPages.Message> msgList = ApexPages.getMessages();
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            if (msg.getSummary().contains('email')) {
                System.assertEquals('There is no email address for LinkedIn CSA Registration!', msg.getSummary());
                System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());                
            } else {
                System.assertEquals('LinkedIn CSA Setting has not been updated!', msg.getSummary());
                System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
            }
        }
    	}
    }
    
    static testmethod void testNoApiKeyOrCustomerId() {
    	System.runAs(DummyRecordCreator.platformUser) {
        LinkedInCSAAdminController csaAdminCon = new LinkedInCSAAdminController();
        csaAdminCon.saveCSA();
        List<ApexPages.Message> msgList = ApexPages.getMessages();
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertEquals('Please enter both API key and Customer ID!', msg.getSummary());
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
        }
    	}
    }
    
}