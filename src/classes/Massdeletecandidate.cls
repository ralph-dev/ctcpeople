public with sharing class Massdeletecandidate {
    public boolean showsteps;
    public String clauses;
    public Flow.Interview.Scheduled_jobs scheduleflow{set;get;}
    public String jsonvalues{set;get;}
    public boolean getshowsteps(){
        showsteps=false;
        if(scheduleflow!=null){
            
            String values=(String)scheduleflow.getVariableValue('isCriteriaalvailable');
            //System.debug('The values is '+values);
            
            if(values!=null){
                showsteps=true;
            }
        }
        return showsteps;
    }
    
    public Massdeletecandidate(){
        jsonvalues='';
    }
     
    public PageReference jsondatavalue(){
        showsteps=false;
        //system.debug('The json value is '+jsonvalues);        
        List<String> clauselist=new List<String>();
        List<Object> jsonlist=new List<Object>();
        String queryclause='';
        Boolean firsttime=true;
        OperatorsHandle operh=new OperatorsHandle(jsonvalues);
        queryclause=operh.getqueries();
        clauses=queryclause;      
        
       // System.debug('the data is ####'+queryclause);
        
        //store the query as a style category record 
        if(scheduleflow!=null && queryclause!='' || Test.isRunningTest()){ 
            String jobname;
        String dayofweekstr;
        DateTime startdate;
        DateTime enddate;
        String monthweek;
        Double dayofmonth;
         if(Test.isRunningTest()){
        dayofweekstr='Sun;Mon';
        jobname='Test job';
                dayofmonth=15.0;
        monthweek='weekly';
        startdate=Date.today();
        enddate=Date.today();   

        }else{           
            jobname=(String)scheduleflow.getVariableValue('Jobname');
            startdate=(DateTime)scheduleflow.getVariableValue('StartDate');
            enddate=(DateTime)scheduleflow.getVariableValue('EndDate');   
            monthweek=(String)scheduleflow.getVariableValue('MonthlyWeekly');
            
            if(monthweek.equals('weekly')){
                String dayofmonthstr=(String)scheduleflow.getVariableValue('Dayofmonth');
                dayofmonthstr='0';
                dayofmonth=Double.valueOf(dayofmonthstr);
            }else{
                dayofmonth=(Double)scheduleflow.getVariableValue('Dayofmonth');
            }
            
            dayofweekstr=(String)scheduleflow.getVariableValue('Dayofweek');
        }
            String Weeks=dayofweekstr;
            if(Weeks==Null){
                Weeks='';
            }
            //System.debug('the weeks is '+Weeks+'the jobname is  '+jobname+' the monthweek is '+ monthweek+' the dayofmonth is '+dayofmonth+' startdate'+startdate+' Enddate'+enddate);
            StyleCategory__c style=new StyleCategory__c();            
            style.Name= jobname;
            style.Start_Date__c=startdate.date();
            style.End_Date__c=enddate.date();
            style.Monthly_Weekly__c= monthweek;            
            style.Date_of_each_Month__c=dayofmonth.intValue();            
            style.Week__c= Weeks;
            style.ascomponents__c=jsonvalues;
            style.asquerystring__c=queryclause;
            //set the time;
            String regularexpression='0 0 4 ';
           
            if(monthweek.equals('monthly')){
                regularexpression=regularexpression+dayofmonth.intValue()+' * ?';
            }else{
                regularexpression=regularexpression+'? ';
                regularexpression=regularexpression+'* ';
                String weeksvalue=Weeks.replace('; ',',');
                regularexpression=regularexpression+weeksvalue;
            }
            Scheduleddeletejob schdejob=new Scheduleddeletejob();            
            //System.debug('The string value is '+String.escapeSingleQuotes(queryclause));
            try{
                
                 
                String schedulejobid=system.schedule(jobname, regularexpression, schdejob);            
                style.schedulejobId__c=schedulejobid; 
                
                RecordType rdt=new RecordType();
                rdt=DaoRecordType.scheduleJobRT;//[select id from RecordType where developername='schedule_job'];
                style.RecordTypeId=rdt.id;
                //check FLS
                List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                    StyleCategory__c.Name,
                    StyleCategory__c.Start_Date__c,
                    StyleCategory__c.End_Date__c,
                    StyleCategory__c.Monthly_Weekly__c,
                    StyleCategory__c.Date_of_each_Month__c,
                    StyleCategory__c.Week__c,
                    StyleCategory__c.ascomponents__c,
                    StyleCategory__c.asquerystring__c,
                    StyleCategory__c.schedulejobId__c,
                    StyleCategory__c.RecordTypeId
                };
                fflib_SecurityUtils.checkInsert(StyleCategory__c.SObjectType, fieldList);           
                insert style;
                 
            }catch(Exception ex){
                System.debug(ex);
                Integer ERR_CREATE_MASSDELETE_FAILURE=30;
                PeopleCloudErrorInfo errormsg=new PeopleCloudErrorInfo(ERR_CREATE_MASSDELETE_FAILURE,true);
                return null; 
            }                                       
            Integer Successcreate_Criteria=221;
            PeopleCloudErrorInfo errormsg=new PeopleCloudErrorInfo(Successcreate_Criteria,true); 
           PageReference pg=new pagereference('/home/home.jsp');
            return  pg; 
                              
            
         }else{
            Integer ERR_CRITERIA=21;
            PeopleCloudErrorInfo errormsg=new PeopleCloudErrorInfo(ERR_CRITERIA,true);
            
            
           	return null;
         }    
                
    }
    
    
         
}