/**
 * This is the Controller class for visualforce page ExtractData4SBEController.
 * This page is used for the Send Bulk Email button on people list view.
 * It will show error message if there are no contact or too many contact being selected,
 * if no error occur, it will redirect to BulkEmailCandidateList Page
 * Created by: Lina
 * Created date: 05/04/2016
 **/ 

public with sharing class ExtractData4SBEController {
    
	private ApexPages.Standardsetcontroller setCon;
	public Id sendbulkemailid { get; set; }  //capture the previous page id
	public String retURL{ get; set; }    //generate the previous page URL
	public Schema.sObjectType sobjecttype{ get; set; }
	public PageReference pr;  		    //return to SendResume page
	public PageReference ErrorPage;
	public String sobjecttypestring;	
	public String backto;
	public String sourcepage;
	//get error message
	public PeopleCloudErrorInfo errormesg; 
	public Boolean msgSignalhasmessage{ get; set; }

	/**
	   Init controller, get retURL and id
	   Contact & Candidate & Candidates --> Contact RecordType; 
	**/
	
	public ExtractData4SBEController(ApexPages.StandardSetController setCon){
		sendbulkemailid = Apexpages.currentPage().getParameters().get('id');
		//retURL = Apexpages.currentPage().getParameters().get('retURL');
		retURL = PeopleCloudHelper.makeRetURL( Apexpages.currentPage());
		sobjecttype = setCon.getRecord().getSObjectType();
		sobjecttypestring = String.valueOf(sobjecttype);
		this.setCon=setCon;		
	}
	
	public PageReference inits(){
		errormesg = null;
		msgSignalhasmessage = false;
		if (sobjecttypestring.equals('Contact')){			
			initContacts();
		}
		return pr;//redirect the VF to SendResume Page
	}
	
	//Init the Contact(s) inforamtion and return the Contact list	
	public void initContacts(){
		try{
			List<Contact> selectedCan;
			//List<Contact> selectedConCan;
			if(setCon !=null){	
				selectedCan=(List<Contact>)setCon.getSelected();
				if(selectedCan.size() <= 0) {								
					customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CONTACT_SELECTED);			
				} 
				else if(selectedCan.size() > 100){
				    customerErrorMessage(PeopleCloudErrorInfo.ERROR_BULK_EMAIL_TooManyCandSelectError);
				} 
				else {
				    sourcepage = 'peopleListView';
				    sendBulkEmailUrlParams(selectedCan, retURL, sourcepage);
				}
			}
			else{
				pr = null;
			}	
		}
		catch(Exception e){
			NotificationClass.notifyErr2Dev('ExtractData4SRController/initContacts' + backto, e); 
		}
	}
	
	//Generate the redirect URL
	public void sendBulkEmailUrlParams(List<Contact> selectedCon, String returnurl, String sourcepage){
		Integer index=0;
		pr=new ApexPages.Pagereference('/apex/BulkEmailCandidateListPage');
		Map<String,String> params=pr.getParameters();
		params.put('returnurl' , returnurl);
		params.put('sourcepage', sourcepage);
		if(selectedCon != null){
			for(Contact c:selectedCon){
				String argKey='arg'+index;
				params.put(argKey,c.Id);
				index++;
			}
		}
	}
		
	//Return to Previous Page
// 	public PageReference returntoPreviousPage(){
// 		PageReference pr = new PageReference(retURL);
//         return pr;
// 	}
	
	//Genereate Error message
	public void customerErrorMessage(Integer msgcode){
        errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
        msgSignalhasmessage = errormesg.returnresult;
        for(ApexPages.Message newMessage:errormesg.errorMessage){
        	ApexPages.addMessage(newMessage);
        }               
    }
	
	static void notificationsendout(String msg,String msgtype, String apex_code, String apex_method){//Send error message
        
        NotificationClass.sendNodification(msg,msgtype,UserInfo.getOrganizationId(),
        UserInfo.getOrganizationName(),apex_code,apex_method, UserInfo.getName()+':'+UserInfo.getUserName());    
    }    


}