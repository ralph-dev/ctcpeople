public with sharing class SearchAdminControllerUtils {
	//Init the picklist value
    public Static List<String> initPicklistValues(String fieldapi, Map<string, SObjectField> o_fields){
    	String NameSpacePrefix = PeopleCloudHelper.getPackageNamespace();         
        if(NameSpacePrefix != null && NameSpacePrefix != ''){
            if(fieldapi.toLowerCase().contains(NameSpacePrefix.toLowerCase()))
            {
                Integer index1 = NameSpacePrefix.length();    
                fieldapi= fieldapi.substring(index1);//get the field api name without namespace prefix                
            }
        }
        
        String tempStr;  
        List<String> options = new List<String>();
        
        if(fieldapi!=null && fieldapi.length()>0 ){  
        	DescribeFieldResult fdr = o_fields.get(fieldapi).getDescribe();
			List <Schema.PicklistEntry> pickValList;
            tempStr = '';
            pickValList = fdr.getPicklistValues();          //salesforce Describe Field Result Method
            if(pickValList!= null && pickValList.size()>0){ //get the pick list value to the select option
            	for(Schema.PicklistEntry entry:pickValList){
                	if(entry.isActive()){
                    	options.add(entry.getLabel()); 
                    }
                }
            } 
         }
         return options;
    }
    
    public Static String[] getThisPicklistValues(String obj, String fieldname){
        Integer index1 ;
        String NameSpacePrefix = PeopleCloudHelper.getPackageNamespace();   
        if(NameSpacePrefix != null && NameSpacePrefix != ''){    
            index1 = NameSpacePrefix.length();
            if(obj.toLowerCase().contains(NameSpacePrefix.toLowerCase())){
                obj= obj.substring(index1);
            }
            if(fieldname.toLowerCase().contains(NameSpacePrefix.toLowerCase()))
            {
                fieldname= fieldname.substring(index1);
            }
        }
            
            String[] tempStr = new String[]{}; 
            SObjectType objToken = Schema.getGlobalDescribe().get(obj);         
            DescribeSObjectResult objDef = objToken.getDescribe();          
            Map<string, SObjectField> fields = objDef.fields.getMap();   
            DescribeFieldResult fdr = fields.get(fieldname).getDescribe();
            for(Schema.PicklistEntry entry:fdr.getPicklistValues()){
                if(entry.isActive()){
                    tempStr.add(entry.getValue());  
                }
            }            
            return tempStr;
    }
    
     /*
		@Author Jack ZHOU
		@Input String Object Name, String Field Name
		@return List<SelectOption>    
    	@Operation convert SF pickList Field Value to select option
    */
    public static List<SelectOption> ConvertPickListtoSelectOption(String obj, String fieldname) {
    	Integer nameSpaceLength;
    	List<SelectOption> newPickListSelectOption = new List<SelectOption>();
    	newPickListSelectOption.add(new SelectOption('', 'Choose a value'));
    	String NameSpacePrefix = PeopleCloudHelper.getPackageNamespace();
    	nameSpaceLength = NameSpacePrefix.length();
    	
    	if(NameSpacePrefix != null && NameSpacePrefix != '') {
    		/*if(obj.toLowerCase().contains(NameSpacePrefix.toLowerCase())) {
                obj= obj.substring(nameSpaceLength);
            }*/
            
            if(fieldname.toLowerCase().contains(NameSpacePrefix.toLowerCase())) {
                fieldname= fieldname.substring(nameSpaceLength);
            }
    	}
    	
    	SObjectType objToken = Schema.getGlobalDescribe().get(obj);
    	DescribeSObjectResult objDesR = objToken.getDescribe();
    	map<string, SObjectField> objFields = objDesR.fields.getMap();
    	DescribeFieldResult desField = objFields.get(fieldname).getDescribe();
    	for(Schema.PicklistEntry picklistEntry : desField.getPicklistValues()) {
    		if(picklistEntry.isActive()) {
    			newPickListSelectOption.add(new SelectOption(picklistEntry.getValue(), picklistEntry.getValue()));
    		}
    	}
    	return newPickListSelectOption;
    }
}