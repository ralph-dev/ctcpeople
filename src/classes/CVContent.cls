public with sharing class CVContent {
	
	private String cvFilename;
	private Blob content;
	
	public String getCvFilename(){
		return this.cvFilename;
	}
	
	public void setCvFilename(String name){
		this.cvFilename = name;
	}
	
	public Blob getContent(){
		return this.content;
	}
	
	public void setContent(Blob c){
		this.content = c;
	}
    
}