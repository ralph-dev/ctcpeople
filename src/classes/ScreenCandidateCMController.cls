public with sharing class ScreenCandidateCMController{

	public ApexPages.StandardSetController setCtrl{get; set;}
	public ApexPages.StandardSetController setCtrlTemp;
	public List<Id> cmIdList{get; set;}
	public String JSONString{get; set;}

	public ScreenCandidateCMController(ApexPages.StandardSetController stdSetController){
		cmIdList = new List<Id>();
		setCtrlTemp = stdSetController;
	}

	public void init(){
		setCtrl = setCtrlTemp;
		for(Placement_Candidate__c cm: (List<Placement_Candidate__c>)setCtrl.getSelected()){
			cmIdList.add(cm.Id);
		}
		JSONString = JSON.serialize(cmIdList);
	}

}