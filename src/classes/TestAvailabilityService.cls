@isTest
private class TestAvailabilityService{
    private static String ROSTER_STATUS_PENDING = 'Pending';

	// test exceptions on deleting avl records
    static testMethod void testDeleteAvlRecordWithException(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	String result = AvailabilityService.deleteAvlRecord(null);
    	System.assert(result.contains('Exception during deleting records'));
    	}
    }
    
    // test exceptions on creating new avl records
     static testMethod void testCreateNewAvlRecordWithException(){
     	System.runAs(DummyRecordCreator.platformUser) {
     	String result = AvailabilityService.createNewAvlRecords(null, null, null);
     	//System.debug('================= result:' + result);
    	System.assert(result.contains('Exception during creating new records'));
     	}
     }
     
    // sd < sfsd < ed < sfed
    //       |---------|
    //  |---------|
    static testMethod void testResolveSingleConflictSituation1(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 		
    	Days_Unavailable__c duvl = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
        AvailabilityEntity avl = declareAvailabilityRec('2013-11-10', '2013-11-14');
        
        List<Days_Unavailable__c> relatedAvlList = new List<Days_Unavailable__c>{duvl};
    	AvailabilityService.resolveAllConflicts4SingleAvl(avl, relatedAvlList, true);


		Days_Unavailable__c avlRec = [select Id, Start_Date__c, End_Date__c, Event_Status__c, Contact__c
										from Days_Unavailable__c where Id =: duvl.Id ];
		//System.debug('Situation1 avlRec:' + avlRec);	
		System.assert(avlRec.Start_Date__c == Date.valueOf('2013-11-15')); 
		System.assert(avlRec.End_Date__c == Date.valueOf('2013-11-20'));	
    	}						
    }
    
    // sfsd < sd = sfed < ed
    //              |---------|
    //    |---------|
    static testMethod void testResolveSingleConflictSituation2(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 		
    	Days_Unavailable__c duvl = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
    	AvailabilityEntity avl = declareAvailabilityRec('2013-11-10', '2013-11-13');
        
        List<Days_Unavailable__c> relatedAvlList = new List<Days_Unavailable__c>{duvl};
        AvailabilityService.resolveAllConflicts4SingleAvl(avl, relatedAvlList, true);
    			
		Days_Unavailable__c avlRec = [select Id, Start_Date__c, End_Date__c, Event_Status__c, Contact__c
										from Days_Unavailable__c where Id =: duvl.Id ];
		//System.debug('Situation2 avlRec:' + avlRec);	
		System.assert(avlRec.Start_Date__c == Date.valueOf('2013-11-14')); 
		System.assert(avlRec.End_Date__c == Date.valueOf('2013-11-20'));	
    	}
    }
    
    // sfsd < sd < sfed < ed
    //   |---------|
    //        |---------|
    static testMethod void testResolveSingleConflictSituation3(){   	
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 		
    	Days_Unavailable__c duvl = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
    	AvailabilityEntity avl = declareAvailabilityRec('2013-11-18', '2013-11-22');
        
        List<Days_Unavailable__c> relatedAvlList = new List<Days_Unavailable__c>{duvl};
        AvailabilityService.resolveAllConflicts4SingleAvl(avl, relatedAvlList, true);

    	Days_Unavailable__c avlRec = [select Id, Start_Date__c, End_Date__c, Event_Status__c, Contact__c
										from Days_Unavailable__c where Id =: duvl.Id ];
		//System.debug('Situation3 avlRec:' + avlRec);	
		System.assert(avlRec.Start_Date__c == Date.valueOf('2013-11-13')); 
		System.assert(avlRec.End_Date__c == Date.valueOf('2013-11-17'));
    	}
    }
   
    // sfsd < sd = sfed < ed
    //   |---------|
    //             |---------| 
	static testMethod void testResolveSingleConflictSituation4(){   	
		System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 		
    	Days_Unavailable__c duvl = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
    	AvailabilityEntity avl = declareAvailabilityRec('2013-11-20', '2013-11-22');
        
        List<Days_Unavailable__c> relatedAvlList = new List<Days_Unavailable__c>{duvl};
        AvailabilityService.resolveAllConflicts4SingleAvl(avl, relatedAvlList, true);

    	Days_Unavailable__c avlRec = [select Id, Start_Date__c, End_Date__c, Event_Status__c, Contact__c
										from Days_Unavailable__c where Id =: duvl.Id ];
		//System.debug('Situation4 avlRec:' + avlRec);	
		System.assert(avlRec.Start_Date__c == Date.valueOf('2013-11-13')); 
		System.assert(avlRec.End_Date__c == Date.valueOf('2013-11-19'));
		}
    }
    
    // sd < sfsd < sfed = ed
    //       |---------|
    //  |--------------|
    static testMethod void testResolveSingleConflictSituation5(){    	
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 		
    	Days_Unavailable__c duvl = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
    	AvailabilityEntity avl = declareAvailabilityRec('2013-11-10', '2013-11-20');
    	
        List<Days_Unavailable__c> relatedAvlList = new List<Days_Unavailable__c>{duvl};
        AvailabilityService.resolveAllConflicts4SingleAvl(avl, relatedAvlList, true);
        
    	List<Days_Unavailable__c> avlRecList = [select Id, Start_Date__c, End_Date__c, Event_Status__c, Contact__c
										from Days_Unavailable__c where Id =: duvl.Id ];
		//System.debug('Situation5 avlRecList:' + avlRecList);	
		System.assert(avlRecList.isEmpty()); 
    	}

    }
    
    
    // sd = sfsd < sfed < ed
    //   |---------|
    //   |-------------|
    static testMethod void testResolveSingleConflictSituation6(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 		
    	Days_Unavailable__c duvl = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
    	AvailabilityEntity avl = declareAvailabilityRec('2013-11-13', '2013-11-24');

        List<Days_Unavailable__c> relatedAvlList = new List<Days_Unavailable__c>{duvl};
        AvailabilityService.resolveAllConflicts4SingleAvl(avl, relatedAvlList, true);
        
    	List<Days_Unavailable__c> avlRecList = [select Id, Start_Date__c, End_Date__c, Event_Status__c, Contact__c
										from Days_Unavailable__c where Id =: duvl.Id ];
		//System.debug('Situation6 avlRecList:' + avlRecList);	
		System.assert(avlRecList.isEmpty());  
    	}
    }
    
    // sd = sfsd < sfed = ed 
    //   |---------|
    //   |---------|
    static testMethod void testResolveSingleConflictSituation7(){   
    	System.runAs(DummyRecordCreator.platformUser) {	
    	Contact candidate = createTestCandidate(); 		
    	Days_Unavailable__c duvl = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
    	AvailabilityEntity avl = declareAvailabilityRec('2013-11-13', '2013-11-20');

        List<Days_Unavailable__c> relatedAvlList = new List<Days_Unavailable__c>{duvl};
        AvailabilityService.resolveAllConflicts4SingleAvl(avl, relatedAvlList, true);
        
    	List<Days_Unavailable__c> avlRecList = [select Id, Start_Date__c, End_Date__c, Event_Status__c, Contact__c
										from Days_Unavailable__c where Id =: duvl.Id ];
		//System.debug('Situation7 avlRecList:' + avlRecList);	
		System.assert(avlRecList.isEmpty());  
    	}
    }
    
    // sd < sfsd < sfed < ed
    //      |-----|
    //  |-------------|    
    static testMethod void testResolveSingleConflictSituation8(){
    	System.runAs(DummyRecordCreator.platformUser) {
       	Contact candidate = createTestCandidate(); 		
    	Days_Unavailable__c duvl = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
    	AvailabilityEntity avl = declareAvailabilityRec('2013-11-10', '2013-11-24');
    	
        List<Days_Unavailable__c> relatedAvlList = new List<Days_Unavailable__c>{duvl};
        AvailabilityService.resolveAllConflicts4SingleAvl(avl, relatedAvlList, true);

    	List<Days_Unavailable__c> avlRecList = [select Id, Start_Date__c, End_Date__c, Event_Status__c, Contact__c
										from Days_Unavailable__c where Id =: duvl.Id ];
		//System.debug('Situation8 avlRecList:' + avlRecList);	
		System.assert(avlRecList.isEmpty());  
    	}
    }
   
    // sfsd < sd < ed < sfed
    //   |-------------|
    //      |-----|    
    static testMethod void testResolveSingleConflictSituation9(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 		
    	Days_Unavailable__c duvl = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
    	AvailabilityEntity avl = declareAvailabilityRec('2013-11-15', '2013-11-18'); 
    	
        List<Days_Unavailable__c> relatedAvlList = new List<Days_Unavailable__c>{duvl};
        AvailabilityService.resolveAllConflicts4SingleAvl(avl, relatedAvlList, true);

    	List<Days_Unavailable__c> avlRecList = [select Id, Start_Date__c, End_Date__c, Event_Status__c, Contact__c
										from Days_Unavailable__c 
										where Start_Date__c = 2013-11-13 or End_Date__c=2013-11-20 ];
		//System.debug('Situation9 avlRecList:' + avlRecList);	
		System.assert(avlRecList.size() == 2);   
    	}	
    }
    
    // sfsd < sd < ed = sfed
    //   |-------------|
    //        |--------|    
    static testMethod void testResolveSingleConflictSituation10(){	
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 		
    	Days_Unavailable__c duvl = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
    	AvailabilityEntity avl = declareAvailabilityRec('2013-11-15', '2013-11-20');
    	
        List<Days_Unavailable__c> relatedAvlList = new List<Days_Unavailable__c>{duvl};
        AvailabilityService.resolveAllConflicts4SingleAvl(avl, relatedAvlList, true);

    	Days_Unavailable__c avlRec = [select Id, Start_Date__c, End_Date__c, Event_Status__c, Contact__c
										from Days_Unavailable__c where Id =: duvl.Id ];
		//System.debug('Situation10 avlRec:' + avlRec);	
		System.assert(avlRec.Start_Date__c == Date.valueOf('2013-11-13')); 
		System.assert(avlRec.End_Date__c == Date.valueOf('2013-11-14'));	
    	}
    }
      
    // sfsd = sd < ed < sfed
    //   |-------------|
    //   |--------|     
    static testMethod void testResolveSingleConflictSituation11(){   	
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 		
    	Days_Unavailable__c duvl = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
    	AvailabilityEntity avl = declareAvailabilityRec('2013-11-13', '2013-11-15');
    	
        List<Days_Unavailable__c> relatedAvlList = new List<Days_Unavailable__c>{duvl};
        AvailabilityService.resolveAllConflicts4SingleAvl(avl, relatedAvlList, true);

    	Days_Unavailable__c avlRec = [select Id, Start_Date__c, End_Date__c, Event_Status__c, Contact__c
										from Days_Unavailable__c where Id =: duvl.Id ];
		//System.debug('Situation11 avlRec:' + avlRec);	
		System.assert(avlRec.Start_Date__c == Date.valueOf('2013-11-16')); 
		System.assert(avlRec.End_Date__c == Date.valueOf('2013-11-20'));
    	}
    }
    
    // sfsd < sfed < sd < ed 
    //   |---------|
    //                |--------|      
    static testMethod void testResolveSingleConflictSituation12(){
    	System.runAs(DummyRecordCreator.platformUser) {
    	Contact candidate = createTestCandidate(); 		
    	Days_Unavailable__c duvl = createDaysUnavailableRec(candidate.Id,'2013-11-13', '2013-11-20');
    	AvailabilityEntity avl = declareAvailabilityRec('2013-11-22', '2013-11-24');
    	
        List<Days_Unavailable__c> relatedAvlList = new List<Days_Unavailable__c>{duvl};
        AvailabilityService.resolveAllConflicts4SingleAvl(avl, relatedAvlList, true);

    	Days_Unavailable__c avlRec = [select Id, Start_Date__c, End_Date__c, Event_Status__c, Contact__c
										from Days_Unavailable__c where Id =: duvl.Id ];
		//System.debug('Situation12 avlRec:' + avlRec);	
		System.assert(avlRec.Start_Date__c == Date.valueOf('2013-11-13')); 
		System.assert(avlRec.End_Date__c == Date.valueOf('2013-11-20'));
    	}
    }
    
    // create a days unavailable record
    private static Days_Unavailable__c createDaysUnavailableRec(String candidateId, String startDate, String endDate){
    	Days_Unavailable__c duvl = new Days_Unavailable__c();
    	duvl.Start_Date__c = Date.valueOf(startDate);
    	duvl.End_Date__c = Date.valueOf(endDate);
    	duvl.Contact__c = candidateId;
    	insert duvl;
    	return duvl;
    }

	// declare an avl record
	private static AvailabilityEntity declareAvailabilityRec(String startDate, String endDate){
		AvailabilityEntity avl = new AvailabilityEntity();
		avl.startDate = startDate;
		avl.endDate = endDate;
		return avl;
	} 
	
	 
	// helper method
    // to create a dummy contact record
    private static Contact createTestCandidate(){
        Contact candidate = new Contact(LastName='candidate 1',email='Cand1@test.ctc.test');
    	insert candidate;
    	return candidate;
    }

    static testMethod void testRankingCandidatesByAvailability() {
    	System.runAs(DummyRecordCreator.platformUser) {
        Account acc = TestDataFactoryRoster.createAccount();          //create an account
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);  //create a vacancy
        
        Contact dummycon = TestDataFactoryRoster.createContact();     //create a contact
        Set<String> candidateIds = new Set<String>{dummycon.id};
        Shift__c shift = TestDataFactoryRoster.createShift(vac);      //create a shift
        List<Days_Unavailable__c> rosters =                           //create a roster
            TestDataFactoryRoster.createRostersForSplitRosterFunction(dummycon, shift, acc,ROSTER_STATUS_PENDING);
        //system.debug('rosters ='+rosters);
        
        List<ShiftDTO> shiftDTOs = new List<ShiftDTO>();              //create a shiftDTO
        ShiftDTO shiftDTO = new ShiftDTO();
        shiftDTO = TestDataFactoryRoster.createShiftDTO(acc, vac, shift);
        shiftDTOs.add(shiftDTO);
        //system.debug('shiftDTOs ='+ shiftDTOs);

        candidateDTO conDTO = new candidateDTO();
        conDTO.setCandidate(dummycon);
        conDTO.setRadiusRanking(1);
        
        Map<String, candidateDTO> conDTOMap = new Map<String, candidateDTO>();
        conDTOMap.put(dummycon.id,conDTO);
        AvailabilityService aService = new AvailabilityService();

        conDTOMap = aService.rankingCandidatesByAvailability(shiftDTOs, conDTOMap);
        system.assert(conDTOMap.size()==1);
    	}
    }
}