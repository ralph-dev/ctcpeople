/**
 * Created by zoechen on 25/7/17.
 */

@isTest
public with sharing class OAuthServiceSelectorTest {

    static testMethod void testGetOAuthServiceByRecordTypeId() {
        System.runAs(DummyRecordCreator.platformUser) {

            OAuth_Service__c os = OAuthServiceDummyRecordCreator.createOAuthServiceLinkedin();
            OAuth_Service__c result = new OAuthServiceSelector().getOAuthServiceByRecordTypeId(DaoRecordType.linkedinOauthRT.Id);
            System.assertNotEquals(null,result);
        }
    }

}