public with sharing class DeleteDocsAndstyleCategory {
    public static String docids;
    @future (callout=true)
    public static void deletedocandstyleCategory(){
        try{
            List<StyleCategory__c> searchcriteriabulksmslist=new List<StyleCategory__c>();
            List<Document> peopleidsjsondoclist=new List<Document>();
            String docname='GS_'+Userinfo.getUserId()+'%';
            try{                
                System.debug('docname is #######'+ docname);   
                CommonSelector.checkRead(Document.sObjecttype,'Id');
                peopleidsjsondoclist=[Select Id From Document where name  like : docname and createddate < today order by id limit 5000];  
                CommonSelector.checkRead(StyleCategory__c.sObjecttype,'Id');
                searchcriteriabulksmslist=[select id from StyleCategory__c where Name like 'Bulk_SEND_TEMP_%' and createddate < today limit 5000];     
                CommonSelector.quickDelete(searchcriteriabulksmslist);         
                //delete searchcriteriabulksmslist;
                CommonSelector.quickDelete(peopleidsjsondoclist);   
                //delete peopleidsjsondoclist;
            }catch(Exception ex){
                    docname='error';
            }
        docids=docname;
        }catch(Exception ex){
       	 	System.debug('The doc name is '+ex);
        
        }
    }
    
    
    // delete outdated documents that store gsa search results (prior to today) 
    @future (callout=true)
    public static void deleteGSASearchResultDocumentByFileNamePrefix(String fileNamePrefix){
    	System.debug('inside deleteGSASearchResultDocumentByFileNamePrefix');
    	List<Document> documents2Del =new List<Document>();	
    	fileNamePrefix = fileNamePrefix + '%';
    	System.debug('fileNamePrefix = '+ fileNamePrefix);   
    	CommonSelector.checkRead(Document.sObjectType,'Id');
    	documents2Del = [Select Id From Document where name  like : fileNamePrefix  and createddate < today order by id limit 5000]; 
    	//System.debug('documents2Del size = ' + documents2Del.size());
    	//System.debug('documents2Del '); 	
    	CommonSelector.quickDelete(documents2Del);   
    	//delete documents2Del;	
    }

}