/**
 * This class is the DTO of ad request for web service
 *
 * Author: Lina
 * Date: 22/02/2017
 *
**/
public class JobPostingRequest {
    public String orgId;
    public String namespace;
    public String referenceNo;
    public String postingStatus;
    public String jobContent;
    public String jobBoard;
    public String jobBoardSfApiName;
    public String externalAdId;

    /************* Job Board Account Params *************/
    public String advertiserId;
    public String username;
    public String password;
    /****************************************************/

    public JobPostingRequest() {
        orgId = UserInfo.getOrganizationId();
        namespace = PeopleCloudHelper.getPackageNamespace();
    }

}