public with sharing class AstutePayrollUtils {
    public static String getEndPoint(){
        CTCPeopleSettingHelperServices ctcPeopleService=new CTCPeopleSettingHelperServices();
        String endPointStr=ctcPeopleService.getEndpoint();
        if(endPointStr==''){
        	endPointStr=Endpoint.getPeoplecloudWebSite(); 
        }
        if(endpointStr==''){
        	 endpointStr='https://www.clicktocloud.com';
        }
        return endpointStr;
    }
    
    public static Boolean isNumeric(String str){
        Pattern p = Pattern.compile('^[0-9]+$');
		Matcher MyMatcher = p.matcher(str);
		return MyMatcher.matches();
    }
	public static StyleCategory__c GetAstutePayrollAccount(){
		User currentUser=new User();
		currentUser=(User)new CommonSelector(User.sObjectType)
					.setParam('userAccountId',Userinfo.getUserId())
					.getOne('id, AstutePayrollAccount__c',
						'id =: userAccountId'); //[select id, AstutePayrollAccount__c from User where id =: Userinfo.getUserId()];
		
		RecordType APRecordType= DaoRecordType.AstutePayrollAccountRT;//[select id from RecordType where developername='AstutePayrollAccount'];
		
		StyleCategory__c apAccount = (StyleCategory__c) new StyleCategorySelector()
				.setParam('recordtypeid',APRecordType.id)
				.setParam('userAccountId',currentUser.AstutePayrollAccount__c )
				.getOne('id',
				'recordtypeid=: recordtypeid  and id =: userAccountId');
		//StyleCategory__c apAccount=[select id,AstutePayroll_api_username__c,AstutePayroll_api_password__c, 
		//AstutePayroll_api_key__c from StyleCategory__c where recordtypeid=: APRecordType.id and id =: userAccountId];
		return apAccount;
	}
	public static list<StyleCategory__c> GetAstutePayrollAccounts(){
		RecordType APRecordType= DaoRecordType.AstutePayrollAccountRT;
		
		list<StyleCategory__c> apAccount = new StyleCategorySelector()
											.setParam('recordtypeid',APRecordType.Id)
											.get('id,name',
												'recordtypeid=: recordtypeid');
		//list<StyleCategory__c> apAccount=new list<StyleCategory__c>();
		//apAccount=[select id,name, AstutePayroll_api_username__c,AstutePayroll_api_password__c, 
		//AstutePayroll_api_key__c from StyleCategory__c where recordtypeid=: APRecordType.Id ];
		return apAccount;
	}
	
	public static String GetloginSession(){
		String sessionID = UserInfo.getSessionId();
		//System.debug('Session is '+sessionID);
		return sessionID;
	} 
	public static String ReturnQuery(map<String,String> maps){
		String query='';
		Set <String> apfieldset = new Set<String>();
		apfieldset=maps.keySet();
		set<String> uniqueFields=new set<String>();
		for(String key: apfieldset){
			if(!uniqueFields.contains(maps.get(key))){							
				query=maps.get(key)+','+query;
				uniqueFields.add(maps.get(key));
			}
		}
		query=query.substring(0,query.length()-1);
		
		return query;
	} 

	public static String WriteRequest(map<String,String> fieldsMap,Sobject record){
		set<String> fieldSet=fieldsMap.keySet();
		String jsonStr='';
		for(String fieldUnitStr: fieldSet){
			String fieldname=fieldsMap.get(fieldUnitStr);
			String value= String.valueOf(record.get(fieldname));
			if(value==null && fieldname=='firstname'){
				value='.';
			}
			if(value!=null){
				value=value.replaceAll('\\r', ' ').replaceAll('\\n',' ').replace(' 00:00:00','');
				if(fieldname.endsWith('state')){
					value=value+', AU';
					value=value.toUpperCase();					
					//System.debug('The value si '+value);
					
				}
				if(fieldUnitStr.equals('gender')){
					if(value.equals('Male')){
						value='M';
					}
					if(value.equals('Female')){
						value='F';
					}
				}
				jsonStr=jsonStr+'\''+fieldUnitStr+'\':\''+value+'\',';			
			}
		} 
		if(jsonStr.length()>1){
			jsonStr=jsonStr.substring(0,jsonStr.length()-1);
		}
		return jsonStr; 
	}
	public static void BillerContactMap(map<String,String> BillerContactFieldsMap){
		//Fill Workplace Fields
	 	BillerContactFieldsMap.put('name','name');
	 	BillerContactFieldsMap.put('phone','phone');
	 	BillerContactFieldsMap.put('email','email');
	 	BillerContactFieldsMap.put('address_street','mailingstreet');
	 	BillerContactFieldsMap.put('address_region','mailingstate');
	 	BillerContactFieldsMap.put('address_locality','mailingcity');
	 	BillerContactFieldsMap.put('address_postcode','mailingPostalCode');
	 	BillerContactFieldsMap.put('remoteid','id');
	}
	public static void BillerMap(map<string,String> BillerFieldsMap){
		//Fill Biller fields
	 	BillerFieldsMap.put('name','name');
	 	BillerFieldsMap.put('phone','phone');
	 	BillerFieldsMap.put('billing_street','billingstreet');
	 	BillerFieldsMap.put('billing_locality','billingcity');
	 	BillerFieldsMap.put('billing_region','billingstate');
	 	BillerFieldsMap.put('billing_postcode','billingPostalcode');
	 	BillerFieldsMap.put('remoteid','id');
	}
	public static void EmployeeMap(map<String,String> EmployeeFieldsMap){
		EmployeeFieldsMap.put('name_first','firstname');
	 	EmployeeFieldsMap.put('name_last','lastname');
	 	EmployeeFieldsMap.put('email','email');
	 	EmployeeFieldsMap.put('phone','phone');
	 	EmployeeFieldsMap.put('address_street','mailingstreet');
	 	EmployeeFieldsMap.put('address_locality','mailingcity');
	 	EmployeeFieldsMap.put('address_region','mailingstate');
	 	EmployeeFieldsMap.put('address_postcode','mailingPostalCode');
	 	EmployeeFieldsMap.put('remoteid','id');
	 	EmployeeFieldsMap.put('date_birth','birthdate');
	 	EmployeeFieldsMap.put('gender','gender__c');
	 	
	}
	public static void PlacementMap(map<String,String> PlacementFieldsMap){
		PlacementFieldsMap.put('job_code','id');
		PlacementFieldsMap.put('date_start','start_date__c');
		PlacementFieldsMap.put('date_finish','end_date__c');
		PlacementFieldsMap.put('pay_rate_interval','timesheet_type__c');
		PlacementFieldsMap.put('job_title','job_title__c');
		PlacementFieldsMap.put('timesheet_enabled','timesheetenabled__c');
		PlacementFieldsMap.put('fixedpayitem_enabled','fixedpayitemenabled__c');		
	}
	public static void ApproverMap(map<String,String> ApproverFieldsMap){
		ApproverFieldsMap.put('remoteid','id');
		ApproverFieldsMap.put('name_first','firstname');
	 	ApproverFieldsMap.put('name_last','lastname');
	 	ApproverFieldsMap.put('email','email');
	}
}