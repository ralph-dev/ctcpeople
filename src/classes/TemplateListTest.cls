@isTest
public class TemplateListTest {
    public static testMethod void testController(){
    	system.runAs(DummyRecordCreator.platformUser) {
    	StyleCategory__c tempstypecategory = new StyleCategory__c();
    	tempstypecategory = DataTestFactory.createStyleCategory();
    	tempstypecategory.Name = 'Test';
    	tempstypecategory.templateSeek__c = '123467';
    	tempstypecategory.recordTypeId = DaoRecordType.adTemplateRT.id;
    	update tempstypecategory;
    	List<StyleCategory__c> scList = new List<StyleCategory__c>();
        scList.add(tempstypecategory);
        TemplateList thecontroller = new TemplateList();
        system.assert(thecontroller.tid != null);
        List<StyleCategory__c> allExistingRecords = thecontroller.getAllTemplates();
        system.assertNotEquals(allExistingRecords.size(), 0);
        thecontroller.setAllTemplates(scList);
        thecontroller.updateTemplates();
        system.assertEquals(thecontroller.err2,'Updated Successfully!');
        PageReference newTemplate = thecontroller.newTemplate();
        system.assert(thecontroller.show_new);
        thecontroller.setSc(tempstypecategory);
        thecontroller.getSc();
        thecontroller.save();
        system.assertEquals(thecontroller.err,'Created Unsuccessfully!');
        thecontroller.save();
    	}
    }

}