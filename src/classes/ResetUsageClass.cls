public class ResetUsageClass{
//** a apex scheduler should run this to reset monthly usage to 0 for each user. 
String[] toAddresses = new String[]{'log@clicktocloud.com'};
Boolean runControl ;
Integer mycareer_std, c1_std, seek_std;

    public User getUpdatedUser(User u) {
        if(u.JobBoards__c != null && u.JobBoards__c != '') {
            if(u.JobBoards__c.contains('seek')) {
                u.Monthly_Quota_Seek__c = seek_std == null ? '' : String.valueOf(seek_std);
            }
            if(u.JobBoards__c.contains('mycareer')) {
                u.Monthly_Quota_MyCareer__c = mycareer_std == null ? '' : String.valueOf(mycareer_std);
            }
            if(u.JobBoards__c.contains('careerone')) {
                u.Monthly_Quota_CareerOne__c = c1_std == null ? '' : String.valueOf(c1_std);
            }
        }
        return u;
    }


public ResetUsageClass(){
    runControl = false;
    try{
        if(Test.isRunningTest()) {
            runControl = true;
        } else {
            runControl = PCAppSwitch__c.getInstance().Enable_Standard_Quota_Assignment__c;
        }
    }
    catch(system.exception e){
    }
    //system.debug('runControl='+runControl);
    if(runControl) {
        getStandardQuota();
    }

    system.debug('seek_std=' + seek_std);
    system.debug('mycareer_std=' + mycareer_std);
    system.debug('c1_std=' + c1_std);
    try{
      /*User[] AllUsers = [Select Id, 
                Seek_Usage__c, CareerOne_Usage__c, MyCareer_Usage__c,
                Email, Name, Profile.Name, JobBoards__c, Monthly_Quota_CareerOne__c,
                Monthly_Quota_MyCareer__c, Monthly_Quota_Seek__c
                from User where (Seek_Usage__c <> null and Seek_Usage__c <> '0') or  
                (CareerOne_Usage__c <> null and CareerOne_Usage__c <> '0') or
                (MyCareer_Usage__c <> null and MyCareer_Usage__c <> '0') limit 1000]; */
        CommonSelector.checkRead(User.SObjectType,'Id,Seek_Usage__c, CareerOne_Usage__c, MyCareer_Usage__c,Email, Name, Profile.Name, JobBoards__c, Monthly_Quota_CareerOne__c,Monthly_Quota_MyCareer__c, Monthly_Quota_Seek__c');
        User[] AllUsers = [Select Id,
                Seek_Usage__c, CareerOne_Usage__c, MyCareer_Usage__c,
                Email, Name, Profile.Name, JobBoards__c, Monthly_Quota_CareerOne__c,
                Monthly_Quota_MyCareer__c, Monthly_Quota_Seek__c
                from User where isActive = true limit 1000];
      User[] recordstoupdate = new User[]{};
      if(AllUsers != null)
      {
        for(User u: AllUsers)
        {
            if(runControl)
            {
                // if(u.JobBoards__c != null && u.JobBoards__c != '')
                // {
                //     if(u.JobBoards__c.contains('seek'))
                //     {
                //         u.Monthly_Quota_Seek__c = seek_std == null ? '' : String.valueOf(seek_std);
                //     }
                //     if(u.JobBoards__c.contains('mycareer'))
                //     {
                //         u.Monthly_Quota_MyCareer__c = mycareer_std == null ? '' : String.valueOf(mycareer_std);
                //     }
                //     if(u.JobBoards__c.contains('careerone'))
                //     {
                //         u.Monthly_Quota_CareerOne__c = c1_std == null ? '' : String.valueOf(c1_std);
                //     }
                
                // }
                u = getUpdatedUser(u);
            }
            u.Seek_Usage__c = '0';
            u.MyCareer_Usage__c = '0';
            u.CareerOne_Usage__c = '0';
            recordstoupdate.add(u);
        }
        if(recordstoupdate.size()>0)
        {
            //check FLS
            List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{
                User.Seek_Usage__c,
                User.MyCareer_Usage__c,
                User.CareerOne_Usage__c
            };
            fflib_SecurityUtils.checkUpdate(User.SObjectType, fieldList);
            update recordstoupdate;
            //send email
            
            Messaging.SingleEmailMessage alertmail = new Messaging.SingleEmailMessage();
            alertmail.setToAddresses(toAddresses);
            alertmail.setSubject('Usage reset Scheduler Update Successfully for instance ' +UserInfo.getOrganizationName() +'!');
            String body = getBody();
            alertmail.setHtmlBody(body);
            if(!Test.isRunningTest()){
            	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { alertmail });
            }else{
            	system.debug(alertmail);
            }
            
        }
      }


    }
    catch(system.Exception e)
    {
        
        Messaging.SingleEmailMessage errormail = new Messaging.SingleEmailMessage();
        errormail.setToAddresses(toAddresses);
        errormail.setSubject('Usage reset Scheduler Error for company:' +UserInfo.getOrganizationName()+ '!');
        String body = 'Hi CTC Support Team, <br/> Error happened for instance -' +String.valueOf(UserInfo.getOrganizationId()).substring(0,15)+ '. <br/> ';
        body = body + 'Error: ' + e.getMessage() + '<br/><br/> Regards!<br/>' + system.now();
        errormail.setHtmlBody(body);
        if(!Test.isRunningTest()){
        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { errormail });
        }else{
        	system.debug(errormail);
        }
        
        
    }
}

public static string getBody() {
    String body = '-----------------Auto Generated by Apex Scheduler----------------------<br/>';
            body += '-----------------This email will be sent out on 1st each month.----------------------<br/>';
            body += '------If you do not recieve this mail on 1st, please check user job board usage whether reseted to 0.------<br/>';
            body += '-----------If NOT, please contact your admin or log@clicktocloud.com.------------<br/>';
            body += '-----------Successfully update user job board usage to 0.-----------------  <br/> ';
            body += '<br/><br/> Regards!<br/>' + system.now();
    return body;
}

public void getStandardQuota(){
    // mycareer_std, c1_std, seek_std;

    try{
        CommonSelector.checkRead(StyleCategory__c.SObjectType,'Id, Name, website__c,Standard_Quota_Per_User__c ');
        for(StyleCategory__c sc :[select Id, Name, website__c,Standard_Quota_Per_User__c 
                from StyleCategory__c 
                where RecordType.DeveloperName =:'WebSite_Admin_Record'])
        {
            
            system.debug('Standard_Quota_Per_User__c='+sc.Standard_Quota_Per_User__c);
            if(sc.Standard_Quota_Per_User__c != null)
            {
                
                if(sc.website__c != null)
                {
                    if(sc.website__c.toLowercase() == 'seek')
                    {
                        seek_std = sc.Standard_Quota_Per_User__c.intValue();
                        system.debug('seek_std=' + seek_std);
                    }
                    else if(sc.website__c.toLowercase() == 'mycareer')
                    {
                        mycareer_std =sc.Standard_Quota_Per_User__c.intValue();
                        system.debug('mycareer_std=' + mycareer_std);
                    }
                    else if(sc.website__c.toLowercase() == 'careerone')
                    {
                        c1_std = sc.Standard_Quota_Per_User__c.intValue();
                        system.debug('c1_std=' + c1_std);
                    }
                }
            }
        }
    }
    catch(system.exception e)
    {
        system.debug(e);
    }
    

}
	
}