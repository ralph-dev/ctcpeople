/*** Controller ***/
// Project: PeopleCloud
// Used for: Search Admin 
// Search Admin App: in development
// Other involved: apex class: FieldsClass,  XmlWriterClass 
// has namespaceprefix :  Yes
// log@clicktocloud.com


public with sharing class SearchAdminController{
    String NameSpacePrefix = PeopleCloudHelper.getPackageNamespace();//get Namespace prefix from PeopleCloudHelper class

    Boolean isNext;
    Boolean isPreview;
    Boolean Show_Option_List;
    Boolean Show_FieldsPanel;
    Boolean Show_PicklistPanel;
    Boolean has_lookup;
    public Boolean IsOverLimit {get;set;}
    public Boolean getIsNext(){
        return isNext;
    } 
    public Boolean getIsPreview(){
        return isPreview;
    }
    public Boolean getShow_PicklistPanel(){
        return Show_PicklistPanel;
    }  
    public Boolean getShow_Option_List(){
        return Show_Option_List;
    }
    public Boolean getShow_FieldsPanel(){
        return Show_FieldsPanel;
    } 
//************************************************
    Boolean none_Selected;
    Boolean Selected_Referencefields;
    String warning_msg ;
    String msg_1 ;
    public String errmsg {get;set;}
    public Boolean getNone_Selected(){
        return none_Selected;
    }
    public Boolean getSelected_Referencefields(){
        return Selected_Referencefields;
    }
    public String getWarning_msg(){
        return warning_msg;
    }
    public String getMsg_1(){
        return msg_1;
    }
//************************************************
    String Param ;  
    String Side ;
    public String getParam (){
    	return Param ;
    }
    public void setParam (String s){
    	Param = s ;
    }
    public String getSide (){
    	return Side;
    }
    public void setSide (String s){
    	Side= s ;
    }
//************************************************
    List<SelectOption> left_fields = new List<SelectOption>{};
    List<SelectOption> right_fields = new List<SelectOption>{};
    List<SelectOption> bottom_fields = new List<SelectOption>{};
    SelectOption temp_field;
    public List<SelectOption> getLeft_fields (){
    	return left_fields;
    } 
    public List<SelectOption> getRight_fields (){
        return right_fields;
    } 
    public List<SelectOption> getBottom_fields (){
        return bottom_fields;
    } 
    
    String selectedRecordType ;
    public String getSelectedRecordType(){
        return selectedRecordType;
    } 
    public void setSelectedRecordType (String s){
        selectedRecordType = s;
    } 
//*************************SearchAdminSelection Page***********************
    public void initObjectSelection(){}
    
    public PageReference onObjectSelect(){//Jump to SearchAdmin page
        PageReference acctPage = new PageReference('/apex/'+NameSpacePrefix+'SearchAdmin?o=' + SelectedObject);
        //PageReference acctPage = new PageReference('/apex/'+NameSpacePrefix +'RecordTypeSelection?o=' + SelectedObject);
        acctPage.setRedirect(true);
        return acctPage;     
    }
	
	public void init(){//Init SearchAdmin page 
        IsOverLimit = false;
        isNext = false;
        SelectedObject = ApexPages.currentPage().getParameters().get('o');
        if(SelectedObject == null || SelectedObject == ''){
         	SelectedObject = 'Contact';
        }       
        
        getSelectedFileds(); //get default selected field
        ObjectFields = new FieldsClass(SelectedObject);
        allfields = ObjectFields.getAvailableFields();
        //system.debug('allfields=' + allfields);
        getColumnFields(allfields);

        if(allfields != null)
        {
            for(FieldsClass.AvailableField af:allfields  )
            {
                if(fields_in_file.containsKey(af.getFieldApi().toLowerCase()))
                {
                	af.setSelected(true);
                }
                if(!af.getIsCustom())//split the fields to custom field and standard fields
                {
                    allStdfields.add(af);
                    }
                else
                {
                    allCumfields.add(af); 
                }
            }
        }
        
        SObjectType objToken = Schema.getGlobalDescribe().get(SelectedObject);     
        DescribeSObjectResult objDef = objToken.getDescribe();  
           
        o_fields = objDef.fields.getMap(); 
    }
	//************************************************
    String SelectedObject;
    public String getSelectedObject (){
        return SelectedObject ;
    }
    public void setSelectedObject (String s){
        SelectedObject= s ;
    }
    FieldsClass ObjectFields; 
    
    List<FieldsClass.AvailableField> allfields = new List<FieldsClass.AvailableField>{};
    List<FieldsClass.AvailableField> allStdfields = new List<FieldsClass.AvailableField>{};
    List<FieldsClass.AvailableField> allCumfields = new List<FieldsClass.AvailableField>{};
    List<FieldsClass.AvailableField> allpicklistfields = new List<FieldsClass.AvailableField>{};
    Map<string, SObjectField> o_fields;
     
    xmldom theXMLDom;
    Map<String,Map<String,String>> fields_in_file = new Map<String,Map<String,String>>(); // *** This map is to store fields already in the configuration file.
   
    //*************Object fields information********************
    
    public List<FieldsClass.AvailableField> getAllfields(){
    	return allfields ;
    }
    public List<FieldsClass.AvailableField> getAllStdfields(){
    	return allStdfields;
    }
    public List<FieldsClass.AvailableField> getAllCumfields(){
    	return allCumfields;
    }   
   
    //********** Next page
    public Map<String, FieldsClass.SelectedField> selectedFields = new Map<String,FieldsClass.SelectedField>{};
    public Map<String, FieldsClass.SelectedField> selectedReferenceFields = new Map<String,FieldsClass.SelectedField>{}; //for long text area field
    public List<FieldsClass.SelectedField> Final_Fields = new List<FieldsClass.SelectedField>{};
    List<FieldsClass.AvailableField> SelectedColumns = new List<FieldsClass.AvailableField>{};
    public List<String> fieldlist = new List<String>{}; // in left and right sections
    public List<String> bottomlist = new List<String>{}; // all long text field in bottom section
    public List<FieldsClass.SelectedField> getResult(){
    	return selectedFields.values() ;
    } 
    public PageReference Next(){
        has_lookup = false;
        if(allStdfields!=null){
            for(FieldsClass.AvailableField stdf:allStdfields){
                if(stdf.getSelected()){
                    if (stdf.getFieldType().toLowerCase() == 'picklist' || stdf.getFieldType().toLowerCase() == 'multipicklist')
                    {
                        if(Limits.getPicklistDescribes()+ 1 >  Limits.getLimitPicklistDescribes())//deal with the picklist describe call limits. The total number of the picklist and multipicklist should be less than the limit(100)
                        {
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'The number of total selected Picklists/Multi-Picklists can not be larger than the System limit: ' + Limits.getLimitPicklistDescribes());
                            IsOverLimit = true;
                            ApexPages.addMessage(myMsg);
                            //system.assertEquals(IsOverLimit, null);
                            return null;
                        }
                        //******************************************************************
                         
                        selectedFields.put(stdf.getFieldApi(), new FieldsClass.SelectedField(stdf.getFieldLabel(), stdf.getFieldApi(), stdf.getFieldType(), SearchAdminControllerUtils.initPicklistValues(stdf.getFieldApi(),o_fields), stdf.getRelatedObject(),stdf.getRelationshipName(),stdf.getIsFilterable())); 
                    }                    
                    else
                    {
                        selectedFields.put(stdf.getFieldApi(), new FieldsClass.SelectedField(stdf.getFieldLabel(), stdf.getFieldApi(), stdf.getFieldType(), null, stdf.getRelatedObject(), stdf.getRelationshipName(),stdf.getIsFilterable())); 
                    }
                    
                    if(stdf.getFieldType().toLowerCase() != 'reference' && stdf.getFieldType().toLowerCase() != 'textarea'){ //textarea should be in bottom_fields
                        fieldlist.add(stdf.getFieldApi());
                    }
                    else
                    {
                        if(stdf.getFieldType().toLowerCase() == 'reference')
                        {   
                        	if(stdf.getFieldApi().toLowerCase() == 'recordtypeid')//add the record type to upper block 
                            {
                                fieldlist.add(stdf.getFieldApi());
                            }
                            else{
                            	has_lookup = true; 
                            	}
                        }
                        if(stdf.getFieldType().toLowerCase() == 'textarea')
                        {
                            bottomlist.add(stdf.getFieldApi());                        
                        }
                    }   
                }
            }
        }
        //system.assertEquals(allCumfields ,null);

        if(allCumfields !=null)
        {
            for(FieldsClass.AvailableField cumf:allCumfields )
            {
                if(cumf.getSelected())
                {
                    if (cumf.getFieldType().toLowerCase() == 'picklist' || cumf.getFieldType().toLowerCase()== 'multipicklist'){
                        //******** deal with the picklist describe call limits ************
                        if(Limits.getPicklistDescribes()+ 1 >  Limits.getLimitPicklistDescribes())
                        {
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'The number of total selected Picklists/Multi-Picklists can not be larger than the System Limit: ' + Limits.getLimitPicklistDescribes());
                            ApexPages.addMessage(myMsg);
                            IsOverLimit = true;
                            //system.assertEquals(IsOverLimit, null);
                            return null;
                        }
                        //******************************************************************
                        selectedFields.put(cumf.getFieldApi(), new FieldsClass.SelectedField(cumf.getFieldLabel(), cumf.getFieldApi(), cumf.getFieldType(), SearchAdminControllerUtils.initPicklistValues(cumf.getFieldApi(),o_fields), cumf.getRelatedObject(), cumf.getRelationshipName(),cumf.getIsFilterable())); 
                    }
                    else{
                        selectedFields.put(cumf.getFieldApi(), new FieldsClass.SelectedField(cumf.getFieldLabel(), cumf.getFieldApi(), cumf.getFieldType(), null, cumf.getRelatedObject(),cumf.getRelationshipName(),cumf.getIsFilterable())); 
                    
                    }
                    
                    if(cumf.getFieldType().toLowerCase() != 'reference' && cumf.getFieldType().toLowerCase() != 'textarea'){
                    	fieldlist.add(cumf.getFieldApi());
                    	}
                    else
                    {
                        if(cumf.getFieldType().toLowerCase() == 'reference')
                        {
                        	has_lookup = true;
                        	}
                        if(cumf.getFieldType().toLowerCase() == 'textarea')
                        {
                            bottomlist.add(cumf.getFieldApi());
                        
                        }
                 	}
                }
            }
        }
        //system.assertEquals(selectedFields ,null);
        
		if(selectedFields != null){
			if(selectedFields.size()>0){
				isNext = true;
				}
			}
       //system.assertEquals(selectedFields ,null);
       if(isNext == false)
       {
           none_Selected = true;
           warning_msg = msg_1;
       }
       else
       {
           none_Selected = false;
       }
       //system.assertEquals(fieldlist ,null);
       if(fieldlist != null)
       {
           if(fieldlist.size()>0)
           {
               for(String s: fieldlist){
                   left_fields.add(new SelectOption(s,selectedFields.get(s).getFieldLabel()));//show the selected field list
               }
           }       
       }
       if(bottomlist != null)
       {
           if(bottomlist.size()>0)
           {
               for(String s: bottomlist){
                   bottom_fields.add(new SelectOption(s,selectedFields.get(s).getFieldLabel()));//show the seleted textarea field list             
               }
           }       
       }
    	return null;
    }
    
    List<AvailableOption> availableoptions = new List<AvailableOption>{};
    public List<AvailableOption> getAvailableoptions (){
    	return availableoptions  ; //set the position for different field
    } 
    List<FieldsClass.AvailableField> field_options = new List<FieldsClass.AvailableField>{};
    public List<FieldsClass.AvailableField> getField_options (){
        return field_options;
    } 
    
    String thePicklistField;
    FieldsClass.SelectedField sf;
    public FieldsClass.SelectedField getSf (){
        return sf;
    }
    
    //Set the picklist value in the CTC admin page
    public PageReference getPicklistValues(){
    	String tempStr; 
        availableoptions.clear();
        Show_FieldsPanel = false;
        Show_PicklistPanel = true;
        thePicklistField = ApexPages.currentPage().getParameters().get('f');
       // system.debug('thePicklistField = ' + thePicklistField);
        if(thePicklistField != null && selectedFields !=null){
        	sf = selectedFields.get(thePicklistField); 
            if(sf.getSelectedPicklistValues() != null){
            	if(sf.getSelectedPicklistValues().size()>0){
                	for(String s: sf.getSelectedPicklistValues()){
                    	availableoptions.add(new AvailableOption(s, true)); 
                    }
                }
            }
      	}
       	return null;
    }
    
    //Save the picklist value changed
    public PageReference Confirm(){
        List<String> values = new List<String>{}; 
        for(AvailableOption a :availableoptions)
        {
            if(a.getSelected())
            {
                values.add(a.optionvalue);            
            }        
        }
        if(values.size()>0){
            sf.setSelectedPicklistValues(values);
            }
        else{//if no value is checked, set the all option to the list
            for(AvailableOption a :availableoptions){
            	values.add(a.optionvalue);
            }
            sf.setSelectedPicklistValues(values);
        }        
        return null;
    } 
    
    //Get Reference fields option
    public PageReference getReferenceFields(){
    	String tempStr; 
        field_options.clear();
        Show_FieldsPanel = true;
        Show_PicklistPanel = false;
        String theReferenceField = ApexPages.currentPage().getParameters().get('f');
            //system.assertEquals(theReferenceField, 'aa');
        String temp='';
        Map<String,String> Selected_f = new Map<String,String>{};
        if(theReferenceField != null && selectedFields !=null){
        	sf = selectedFields.get(theReferenceField); 
            if(sf.getRelatedObject() != null){
            	if(sf.getRelatedObject().size()==1){
            		temp = sf.getFieldApi();
                    //system.assertEquals(temp ,'aa');
                    for(sObjectType s: sf.getRelatedObject()){
                    	/*if(!temp.toLowerCase().contains('__c'))
                            {temp = temp + 'Id';}*/
                            //system.assertEquals(s,null);
                        if(fields_in_file.containsKey(temp.toLowerCase())){
                        	field_options.addAll(FieldsClass.getRelatedObjFields(s,fields_in_file.get(temp.toLowerCase()))); 
                        }
                        else{
                        	field_options.addAll(FieldsClass.getRelatedObjFields(s,null)); }
                          }
                      }
                }
            }
		return null;
    }
        
    //Save Reference field value
    public PageReference SaveRefSelection(){
        List<FieldsClass.AvailableField> temp_options = new List<FieldsClass.AvailableField>{};
        //system.debug('field_options ='+ field_options);
        for(FieldsClass.AvailableField a :field_options){
            if(a.getSelected()){
                temp_options.add(a);
            }
        }
        //system.debug('temp_options ='+ temp_options);
        if(temp_options.size()>0){
            sf.setRelatedObjectFields(temp_options);
            for(FieldsClass.AvailableField f: temp_options){
            	selectedReferenceFields.put(sf.getRelationshipName() + '.' + f.getFieldApi(), 
            								new FieldsClass.SelectedField(f.getFieldLabel()+'('+ sf.getFieldLabel()+')', sf.getRelationshipName() + '.' + f.getFieldApi(), f.getFieldType(), SearchAdminControllerUtils.getThisPicklistValues(f.getObjectname(),f.getFieldApi()), null,null,f.getIsFilterable())); 
                left_fields.add(new SelectOption(sf.getRelationshipName() + '.' + f.getFieldApi(),f.getFieldLabel()+'('+ sf.getFieldLabel()+')'));
            }
        }
        else{
        }
		return null;
	}
    
    //General search criteria doc according user selected.
    public PageReference Submit(){
    	Final_Fields = new List<FieldsClass.SelectedField>{};
    	SelectedColumns = new List<FieldsClass.AvailableField>{};
    	
        FieldsClass.SelectedField so;
        FieldsClass.SelectedField so2;
        if(left_fields != null){
            if(left_fields.size()>0){
                for(Integer i=0; i< left_fields.size(); i++ ){
                    so = selectedFields.get(left_fields[i].getValue());
                    if(so != null){//put fields into the final_Fields list
                        so.setLocation('left');
                        so.setSequence(i); 
                        Final_Fields.add(so);  
                    }  
                    else{
                        so2 = selectedReferenceFields.get(left_fields[i].getValue());
                        if(so2 != null){//put reference field to Final_Fields list
                        	so2.setLocation('left');
                            so2.setSequence(i);  
                            Final_Fields.add(so2);  
                        }
                    }
                }                
            }
        }
        if(right_fields != null)
        {
            if(right_fields.size()>0)
            {
                for(Integer j=0; j< right_fields.size(); j++ )
                {
                    so = selectedFields.get(right_fields[j].getValue());
                    if(so != null){
                        so.setLocation('right');
                        so.setSequence(j); 
                        Final_Fields.add(so);  
                    }  
                    else{
                        so2 = selectedReferenceFields.get(right_fields[j].getValue());
                        if(so2 != null){
                            so2.setLocation('right');
                            so2.setSequence(j);  
                            Final_Fields.add(so2);  
                        }
                    }
                    
                }
            }
        }
        if(bottom_fields != null){
            if(bottom_fields.size()>0){
                for(Integer k=0; k< bottom_fields.size(); k++ ){
                    so = selectedFields.get(bottom_fields[k].getValue());
                    if(so != null){
                        so.setLocation('bottom');
                        so.setSequence(k); 
                        Final_Fields.add(so);  
                    }  
                }
            }
        }
        SelectedColumns.clear();
        populateSelectedColumns(Column_1);
        populateSelectedColumns(Column_2);
        populateSelectedColumns(Column_3);
        populateSelectedColumns(Column_4);
        populateSelectedColumns(Column_5);
        populateSelectedColumns(Column_6);
        if(SelectedColumns.size() <= 0)
        {
            errmsg = 'Error: Update Unsuccessfully! You have NOT selected any columns for search result table!';
            return null;
        }
        if(map_columns.size() < SelectedColumns.size())
        {
            errmsg = 'Error: Update Unsuccessfully! You have selected duplicate columns for search result table!';
            return null;
        
        }
		//General XML document and JSON document at XmlWriterClass Controller
		/**
		system.debug('Submit ----- Final_Fields='+Final_Fields);
		system.debug('Submit ----- SelectedObject='+SelectedObject);
		system.debug('Submit ----- SelectedColumns='+SelectedColumns);
		*/
		
        XmlWriterClass newXmlWriter = new XmlWriterClass(Final_Fields,SelectedObject,null, SelectedColumns);
        errmsg = 'Infomation: Update Successfully!';
        return null;
     
    }
    Map<String,FieldsClass.AvailableField> map_columns = new Map<String,FieldsClass.AvailableField>();
    public void populateSelectedColumns(String col){
        if(Col != null && Col != '')
        {
            for(FieldsClass.AvailableField af: allfields)
            {
                if(af.getFieldApi().trim() == col.trim())
                {
                    SelectedColumns.add(af);
                    map_columns.put(af.getFieldApi().trim(),af);
                }
            }  
        }
    }
    
    public PageReference Preview(){
    
        if((selectedReferenceFields == null || selectedReferenceFields.size()<=0) && has_lookup == true)
        {
        
            Selected_Referencefields = false;
            msg_1 = 'Your have selected Lookup field(s). Please do "Set Reference Fields" first. Otherwise desired fields will not appear on the Search Page!';
            return null;
        }
        else
        {
            Selected_Referencefields = true;}
        Show_FieldsPanel = false;
        Show_PicklistPanel = false;
        isPreview = true;
        //***********
        //PageReference previewPage = Page.SearchLayout;
        //previewPage.setRedirect(true);
        //return previewPage;
       
        return null;
    }
    public PageReference nonAction(){
    
    
       
        return null;
    }    
    public PageReference moveUp(){
        Boolean flag;
        
        //system.assertEquals(Pa + ':' + s , 'a');
        if(Param != null && Side !=null)
        {   Integer count = 0;
            flag= false;
            if(Side.toLowerCase() == 'left')
            {
                for(SelectOption so: left_fields)
                {
                    if(so.getValue().toLowerCase() == Param.toLowerCase())
                    {
                        
                       flag = true;
                       break; 
                    }
                    count = count  +1;
                }
                if(flag){
                    
                    if(count == 0 )
                    {
                        return null;
                        
                    }
                    else
                    {
                        SelectOption temp = left_fields.remove(count);
                        left_fields.add(count-1, temp);
                    
                    }
                    
                }
            }
            else
            {
                for(SelectOption so: right_fields)
                {
                    if(so.getValue().toLowerCase() == Param.toLowerCase())
                    {
                        
                       flag = true; 
                       break; 
                    }
                    count = count  +1;
                }
                if(flag){
                    
                    if(count == 0 )
                    {
                        return null;
                        
                    }
                    else
                    {
                        SelectOption temp = right_fields.remove(count);
                        right_fields.add(count-1, temp);
                    
                    }
                    
                }
            
            }
        
        }
        
       
        return null;
    }
    public PageReference moveDown(){
        Boolean flag;
        if(Param != null && Side !=null)
        {   Integer count = 0;
            flag= false;
            if(Side.toLowerCase() == 'left')
            {
                for(SelectOption so: left_fields)
                {
                    if(so.getValue().toLowerCase() == Param.toLowerCase())
                    {
                        
                       flag = true; 
                       break; 
                    }
                    count = count  +1;
                }
                if(flag){
                    if(count == left_fields.size() -1)
                    {return null;}

                    else
                    {
                         SelectOption temp = left_fields.remove(count);
                        if(count == left_fields.size() )
                        {
                            left_fields.add( temp);}
                        else{
                            left_fields.add(count + 1, temp);
                        }
                    
                    }
                    
                }
            }
            else
            {
                for(SelectOption so: right_fields)
                {
                    if(so.getValue().toLowerCase() == Param.toLowerCase())
                    {
                        
                       flag = true; 
                       break; 
                    }
                    count = count  +1;
                }
                if(flag){
                    if(count == right_fields.size() -1)
                    {return null;}

                    else
                    {
                        SelectOption temp = right_fields.remove(count);
                        if(count == right_fields.size() )
                        {
                        right_fields.add( temp);}
                        else{
                            right_fields.add(count + 1, temp);
                        }
                    
                    }
                    
                }
            
            }
        
        }
        
       
        return null;
    }
    public PageReference SwitchToRight(){
        String SelectedField = ApexPages.currentPage().getParameters().get('f');
        if(SelectedField == null)
        {return null;}

        temp_field = null;
        
        if(right_fields == null)
        {   right_fields = new List<SelectOption>{};}
        Integer counter =0;
        
        if(left_fields.size()>0 )
        {   
            for(SelectOption so: left_fields){
                if(so.getValue().toLowerCase() == SelectedField.toLowerCase())
                {
                
                    break;
                }
                counter = counter +1;
                
            
            }
            
            temp_field = left_fields.remove(counter);
        }
        
        
        if(temp_field != null)
        {right_fields.add(temp_field);}
        
    
        return null;
    }
    public PageReference SwitchToLeft(){
        String SelectedField = ApexPages.currentPage().getParameters().get('f');
        if(SelectedField == null)
        {return null;}
        
        temp_field = null;
        
        if(left_fields == null)
        {   left_fields = new List<SelectOption>{};}
        Integer counter =0;
        
        if(right_fields.size()>0 )
        {
            for(SelectOption so: right_fields){
                if(so.getValue().toLowerCase() == SelectedField.toLowerCase())
                {
                
                    break;
                }
                counter = counter +1;
                
            
            }
            
            temp_field = right_fields.remove(counter);
        }
        
        if(temp_field != null)
        {left_fields.add(temp_field);}
        
        
    
        return null;
    }
//************************************************************************
//AvailableOption: picklist option list class

    public class AvailableOption{
        private String optionvalue;
       
        private boolean selected;
        public String getOptionvalue(){
         
            return this.optionvalue;    
        }
        
        public Boolean getSelected(){
         
            return this.selected;    
        }
        public void setSelected(Boolean b){
         
            this.selected = b;    
        } 
        public AvailableOption(String a, boolean b){
        
            this.selected = b;
            this.optionvalue = a;
        }
        
    }

    public void getSelectedFileds(){        
        //genereate the XML document name according the SelectedObject Name.
        //Such as ClicktoCloud_Xml_Account_, ClicktoCloud_Xml_Placement_ or ClicktoCloud_Xml_Contact_
        //Get the fields which have been input to the XML document
        String namestr = '';
        Document d;
        if(SelectedObject != null){
            if(SelectedObject.endsWith('__c')) //SelectedObject is PeopleCloud1__Placement__c
            {
                Integer l = SelectedObject.length() -3 ;
                namestr = SelectedObject.substring(0,l);
                if(NameSpacePrefix != null && NameSpacePrefix != ''){
                    if(namestr.contains(NameSpacePrefix))
                    {
                        Integer index1 = NameSpacePrefix.length();
                        namestr = namestr.substring(index1);
                    }
                }
            }
            else{            
                namestr = SelectedObject;
            }   
    		namestr = 'ClicktoCloud_Xml_'+namestr;
    		d = DaoDocument.getCTCConfigFileByDevName(namestr);  
        }
        
        Map<String,String> tempmap = new Map<String,String>();
        String temp ='';
        if(d!=null)
        {
            Blob bodyBlob = d.Body;
            String bodyStr = bodyBlob.toString();
            if(bodyStr != null)
            {
            	this.theXMLDom = new xmldom(bodyStr);
            }
            if (this.theXMLDom != null )
            {
                List<xmldom.Element> apielements = this.theXMLDom.getElementsByTagName('apiname');
                if(apielements != null)
                {
                    for(xmldom.Element e :apielements)
                    {
                    	if(e.nodeValue.contains('.')) // ** this is a lookup field, eg:account.id
                        {
                        	temp = e.nodeValue.substring(0,e.nodeValue.indexOf('.'));
                         	if(temp.contains('__r'))
                          	{
                          		temp =temp.replace('__r','__c');
                          	}
                          	else
                          	{
                          		temp = temp+'Id'; //eg: FirstNameId
                          	}
                            if(fields_in_file.containsKey(temp.toLowerCase()))
                            {
                                tempmap = fields_in_file.remove(temp.toLowerCase());
                                tempmap.put(e.nodeValue.substring(e.nodeValue.indexOf('.')+1).toLowerCase(),null);
                            }
                            else
                            {
                                tempmap.put(e.nodeValue.substring(e.nodeValue.indexOf('.')+1).toLowerCase(),null);
                            }
                            fields_in_file.put(temp.toLowerCase(),tempmap );
                        }
                        else
                        {
                            fields_in_file.put(e.nodeValue.toLowerCase(),null);
                        }
                    }
                    //system.assertEquals(fields_in_file,null);
                }
                else{//show error messsage author by Jack
                	
                }
            }
        }
    }

    //********** for visual force page: RecordTypeSelection**************************
    List<SelectOption> rt_options = new List<SelectOption>();
    public List<SelectOption> getRt_options(){
        return rt_options;
    }
    
    public PageReference pageinit(){
        SelectedObject = ApexPages.currentPage().getParameters().get('o');
        rt_options.add(new SelectOption('0','----------All---------'));
        //SelectedObject = 'Account';
        if(SelectedObject != null && SelectedObject != '')
        {
            SObjectType objToken = Schema.getGlobalDescribe().get(SelectedObject);
            Schema.DescribeSObjectResult r = objToken.getDescribe();
            List<Schema.RecordTypeInfo> RTs = r.getRecordTypeInfos();
            
            if(RTs != null)
            {
                if(RTs.size()>1){
                    for(Schema.RecordTypeInfo rt: RTs){
                        if(!rt.getName().equalsIgnoreCase('master')){
                        rt_options.add(new SelectOption(rt.getRecordTypeId(),rt.getName()));
                        }
                    }
                }
                else
                {
                    PageReference acctPage = new PageReference('/apex/'+NameSpacePrefix+'SearchAdmin?o=' + SelectedObject  + '&rtid=' + selectedRecordType);
                    acctPage.setRedirect(true);
                    return acctPage;
                }
                
            }
            else
            {
                PageReference acctPage = new PageReference('/apex/'+NameSpacePrefix+'SearchAdmin?o=' + SelectedObject  + '&rtid=' + selectedRecordType);
                acctPage.setRedirect(true);
                return acctPage;
            
            }

                
        }
        return null;
    
    }
    public PageReference doRecordTypeSelection(){
    
        PageReference acctPage = new PageReference('/apex/'+NameSpacePrefix+'SearchAdmin?o=' + SelectedObject + '&rtid=' + selectedRecordType );
        acctPage.setRedirect(true);
        return acctPage;
    
    
    }
    //******************************** result table columns selection ******************************
    List<SelectOption> columnItems = new List<SelectOption>();
    //** the max num of columns displayed in search result table
    Public String Column_1 {get;set;}
    Public String Column_2 {get;set;}
    Public String Column_3 {get;set;}
    Public String Column_4 {get;set;}
    Public String Column_5 {get;set;}
    Public String Column_6 {get;set;}
    public List<SelectOption> getColumnItems(){
    
        return columnItems;
    
    }
    
    // **** this is to populate drop down list for search result columns selection 
    public void getColumnFields(List<FieldsClass.AvailableField> allcolumns){
        columnItems = new List<SelectOption>();
        List<SelectOption> tempColumnItems = new List<SelectOption>();
        columnItems.add(new SelectOption('null','--None--'));
        if(allcolumns != null)
        {
        	for(FieldsClass.AvailableField a : allcolumns)
            {
                if(a.getFieldType().toLowerCase().trim() != 'textarea')
                {
                    tempColumnItems.add(new SelectOption(a.getFieldApi(),a.getFieldLabel()));
                }
            }
            tempcolumnItems.sort();//add by Jack, sort the dropdown list value        
        }
        columnItems.addAll(tempcolumnItems);
    }
    
    
}