global with sharing class candidateDTO implements Comparable{
	private Contact candidate;
	private Integer radiusRanking = 0;
	private Integer availablityRanking = 0;
	private Id candidateId;
	private Map<Integer, Integer> unavailableBinaryInteger = new Map<Integer, Integer>();
	private Map<Integer, Integer> availableBinaryInteger = new Map<Integer, Integer>();
	private Integer numberOfShifts = 0;
	private Integer numberOfRoster = 0;
	private decimal availabilityPercentage = 0;
	private Boolean isSelect = false;
	private String eventStatus = '';
	private Boolean isPlaced = false;
	private String createRosteringStatus = '';

	public Id getCandidateId()	{
		return candidateId;
	}

	public void setCandidateId(Id candidateId) {
		this.candidateId = candidateId;
	}

	public Map<Integer, Integer> getUnavailableBinaryInteger()	{
		return unavailableBinaryInteger;
	}

	public void setUnavailableBinaryInteger(Map<Integer, Integer> unavailableBinaryInteger) {
		this.unavailableBinaryInteger = unavailableBinaryInteger;
	}

	public Map<Integer, Integer> getAvailableBinaryInteger()	{
		return availableBinaryInteger;
	}

	public void setAvailableBinaryInteger(Map<Integer, Integer> availableBinaryInteger) {
		this.availableBinaryInteger = availableBinaryInteger;
	}

	public Integer getNumberOfShifts() {
		return numberOfShifts;
	}

	public void setNumberOfShifts(Integer numberOfShifts) {
		this.numberOfShifts = numberOfShifts;
	}

	public Integer getNumberOfRoster() {
		return numberOfRoster;
	}

	public void setNumberOfRoster(Integer numberOfRoster) {
		this.numberOfRoster = numberOfRoster;
	}

	public decimal getAvailabilityPercentage() {
		return availabilityPercentage;
	}

	public void setAvailabilityPercentage(decimal availabilityPercentage) {
		this.availabilityPercentage = availabilityPercentage;
	}

	public Contact getCandidate() {
		return candidate;
	}

	public void setCandidate(Contact candidate) {
		this.candidate = candidate;
	}
	
	public Integer getRadiusRanking() {
		return radiusRanking;
	}

	public void setRadiusRanking(Integer radiusRanking) {
		this.radiusRanking = radiusRanking;
	}

	public Integer getAvailablityRanking() {
		return availablityRanking;
	}

	public void setAvailablityRanking(Integer availablityRanking) {
		this.availablityRanking = availablityRanking;
	}

	public Boolean getIsSelect() {
		return isSelect;
	}

	public void setIsSelect(Boolean isSelect) {
		this.isSelect = isSelect;
	}

	public String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}

	public Boolean getIsPlaced() {
		return isPlaced;
	}

	public void setIsPlaced(Boolean isPlaced) {
		this.isPlaced = isPlaced;
	}

	public String getCreateRosteringStatus() {
		return createRosteringStatus;
	}

	public void setCreateRosteringStatus(String createRosteringStatus) {
		this.createRosteringStatus = createRosteringStatus;
	}

	global Integer compareTo(Object compareTo) {
        // Cast argument to candidateDTO
        candidateDTO compareToCDTO = (candidateDTO)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (availablityRanking> compareToCDTO.availablityRanking) {
            // Set return value to a positive value.
            returnValue = -1;
        } else if (availablityRanking < compareToCDTO.availablityRanking) {
            // Set return value to a negative value.
            returnValue = 1;
        }        
        return returnValue;
	}     
}