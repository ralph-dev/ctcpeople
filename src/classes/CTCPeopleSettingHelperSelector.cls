/**
* modified by andy for security review II
*/
public with sharing class CTCPeopleSettingHelperSelector extends CommonSelector {
	private String queryfields1=' id, Default_Skill_Groups__c, AP_Approver_Fields__c,AP_Owner_Fields__c,AP_Invoice_Item_Fields__c, AP_Biller_Contact_Fields__c, AP_Biller_Fields__c,AP_Employee_Fields__c, LinkedIn_Contact_Email__c, Name  ';
	private String queryfields2=' name, Resume_And_Files_Document_Type__c  ';
	
	private List<CTCPeopleSettingHelper__c> ctcPeopleSettingHelper;

	public CTCPeopleSettingHelperSelector() {
		super(CTCPeopleSettingHelper__c.sObjectType);
		ctcPeopleSettingHelper= new list<CTCPeopleSettingHelper__c>();
	}

	public list<CTCPeopleSettingHelper__c> fetchCTCPeopleHelperSettingByRT(String recordTypeIdValue,String apAccountId){
		
		ctcPeopleSettingHelper = this.setParam('recordTypeIdValue',recordTypeIdValue)
			.setParam('apAccountId',apAccountId)
			.get(queryfields1, 'recordTypeId =: recordTypeIdValue and CTCPeopleSetting__c=: apAccountId');
		
		//String query=queryStr+'where recordTypeId =: recordTypeIdValue and CTCPeopleSetting__c=: apAccountId';
		
		//ctcPeopleSettingHelper=database.query(query);
		return ctcPeopleSettingHelper;		
	}
	
	public list<CTCPeopleSettingHelper__c> fetchCTCPeopleHelperSettingByRT(String recordTypeIdValue){
		ctcPeopleSettingHelper = this.setParam('recordTypeIdValue',recordTypeIdValue)
			.get(queryfields1, 'recordTypeId =: recordTypeIdValue');
		
		
		//String query=queryStr+'where recordTypeId =: recordTypeIdValue ';
		//ctcPeopleSettingHelper=database.query(query);
		return ctcPeopleSettingHelper;	
		
	}

	public list<CTCPeopleSettingHelper__c> fetchResAndFilesDocTypeListByRTAndObjName(String recordTypeName,String objName){
		list<CTCPeopleSettingHelper__c> resumeAndFilesDocTypeList=new list<CTCPeopleSettingHelper__c> ();
        RecordTypeSelector rTselector=new RecordTypeSelector();
        list<RecordType> rTypes=rTselector.fetchRecordTypesByName(recordTypeName);
        String recordTypeId='';
        if(rTypes.size()>0){
            recordTypeId=rTypes.get(0).Id;
        }else{
            return resumeAndFilesDocTypeList;
        }
        
        resumeAndFilesDocTypeList = this.setParam('recordTypeId',recordTypeId)
        	.setParam('objName',objName)
        	.get(queryfields2,'recordTypeId =: recordTypeId and Name =: objName');
        	
		//String query=queryStr2+'where recordTypeId =: recordTypeId and Name =: objName';
		//resumeAndFilesDocTypeList=database.query(query);
		return resumeAndFilesDocTypeList;	
		
	}
}