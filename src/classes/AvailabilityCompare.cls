public with sharing class AvailabilityCompare {
	private static String ONCALL_KEY = 'on call';
	private static String NAME_SPACE_PREFIX = PeopleCloudHelper.getPackageNamespace();

	public AvailabilityCompare() {
		
	}

	//Compare the shift and roster record
	public Map<String, candidateDTO> compareShiftAndAvailability(List<ShiftDTO> shiftDTOs, List<Days_Unavailable__c> availabilities, Map<String, candidateDTO> candidateDTOMap) {
		Map<Integer, Integer> shiftWeekMap = new Map<Integer, Integer>();
		Map<String , String> shiftBinaryMapping = new Map<String , String>();
		Map<String, String> recordTypeMap = availabilityObjectRecordTypeMap();
		
		shiftBinaryMapping = setupShiftBinaryMapping();
		for(ShiftDTO sDTO : shiftDTOs) {
			shiftWeekMap = convertShifttoMap(sDTO , shiftBinaryMapping, shiftWeekMap);
			//system.debug('shiftWeekMap ='+ shiftWeekMap);
		}
		//system.debug('availabilities ='+ availabilities);
		for(Days_Unavailable__c dUnavailable : availabilities) {
			Map<Integer, Integer> availabilityWeekMap = new Map<Integer, Integer>();
			Map<Integer, Integer> unavailabilityWeekMap = new Map<Integer, Integer>();
			String recordTypeId = dUnavailable.recordTypeId;
			candidateDTO cDTO = new candidateDTO();

			if(candidateDTOMap.containsKey(dUnavailable.Contact__c)) {
				cDTO = candidateDTOMap.get(dUnavailable.Contact__c);
				availabilityWeekMap = cDTO.getAvailableBinaryInteger();
				unavailabilityWeekMap = cDTO.getUnavailableBinaryInteger();
			}
			if(recordTypeId==null || recordTypeMap.get(recordTypeId).equalsignorecase('availability')) {
				if(dUnavailable.Event_Status__c.equalsignorecase('Available')) {
					availabilityWeekMap = covnertRostertoMap(dUnavailable,  shiftBinaryMapping, availabilityWeekMap);
				}else {
					unavailabilityWeekMap = covnertRostertoMap(dUnavailable,  shiftBinaryMapping, unavailabilityWeekMap);
				}
			}else {
				unavailabilityWeekMap = covnertRostertoMap(dUnavailable,  shiftBinaryMapping, unavailabilityWeekMap);
			}
			cDTO.setAvailableBinaryInteger(availabilityWeekMap);
			cDTO.setUnavailableBinaryInteger(unavailabilityWeekMap);
			cDTO.setCandidateId(dUnavailable.Contact__c);

			candidateDTOMap.put(dUnavailable.Contact__c, cDTO);
		}
		AvailabilityCompareUtils aCompareUtils = new AvailabilityCompareUtils();
		aCompareUtils.compareCandidateAvailAndShifts(shiftWeekMap, candidateDTOMap);
		
		return candidateDTOMap;
	}

	//convert the shifts to binary map
	public Map<Integer, Integer> convertShifttoMap(ShiftDTO sDTO, Map<String, String> shiftBinaryMap, Map<Integer, Integer> shiftWeekMap) {
		Map<Integer , Integer> currentShiftBinaryMap = new Map<Integer , Integer>();

		Date sDate = Date.valueOf(sDTO.getStartDate());
		Date eDate = Date.valueOf(sDTO.getEndDate());
		String type = sDTO.getsType();
		
		AvailabilityCompareUtils aCompareUtils = new AvailabilityCompareUtils();
		currentShiftBinaryMap = aCompareUtils.generateShiftAndAvailToMap(sDate, eDate, type, shiftBinaryMap, shiftWeekMap);
		return currentShiftBinaryMap;
	}

	//convert Roster to Map
	public Map<Integer, Integer> covnertRostertoMap(Days_Unavailable__c availability, Map<String, String> shiftBinaryMap, 
														Map<Integer, Integer> rosterWeekMap) {
		Map<Integer, Integer> currentAvailabilityBinaryMap = new Map<Integer, Integer>();
		
		Date sDate = availability.Start_Date__c;
		Date eDate = availability.End_Date__c;
		String type = availability.Shift_Type__c;

		AvailabilityCompareUtils aCompareUtils = new AvailabilityCompareUtils();
		currentAvailabilityBinaryMap = aCompareUtils.generateShiftAndAvailToMap(sDate, eDate, type, shiftBinaryMap, rosterWeekMap);
		return currentAvailabilityBinaryMap;
	}
	
	//setup shift Binary Mapping 
	public Map<String , String> setupShiftBinaryMapping() {
		List<Schema.PicklistEntry> pickListValues = new List<Schema.PicklistEntry>();
		Map<String , String> shiftBinaryMapping = new Map<String , String>();
		pickListValues = AvailabilitySelector.getAvailabilityPickListValue(NAME_SPACE_PREFIX+'Shift_Type__c');
		
		//init binary string
		String shiftBinaryString = '0001';
		String onCallBinaryString = '1111';

		shiftBinaryMapping.put(ONCALL_KEY, onCallBinaryString); //always add on call to shift Matrix
		for(integer i=0; i<pickListValues.size();i++) {
			if(!pickListValues[i].getValue().equalsignorecase(ONCALL_KEY)) {
				if(i!=0){
					shiftBinaryString = shiftBinaryString.substring(1,shiftBinaryString.length())+ shiftBinaryString.substring(0,1);
				}
				shiftBinaryMapping.put(pickListValues[i].getValue(), shiftBinaryString);
			}
		}
		//system.debug('shiftBinaryMapping ='+ shiftBinaryMapping);
		return shiftBinaryMapping;
	}

	//Set up the shift Matrix, Max accept 4 different types.
	public static Map<String,Integer> setupShiftMatrix(){
		Map<String,Integer> shiftMatrix = new Map<String,Integer>();
		shiftMatrix.put('0001', 17895697);   //Type 1 -> 0001000100010001000100010001
		shiftMatrix.put('0010', 35791394);   //Type 2 -> 0010001000100010001000100010
		shiftMatrix.put('0100', 71582788);   //Type 3 -> 0010001000100010001000100010
		shiftMatrix.put('1000', 143165576);  //Type 4 -> 0010001000100010001000100010
		shiftMatrix.put('1111', 268435455);  //on call-> 0010001000100010001000100010
		
		return shiftMatrix;
	}

	//Set up the weekday binary map
	public static Map<String,Integer> setupOperatorMatrix() {
		Map<String,Integer> operatorMatrix = new Map<String,Integer>();
		operatorMatrix.put('Mon', 251658240); //Mon -> 1111000000000000000000000000
		operatorMatrix.put('Tue', 15728640);  //Tue -> 0000111100000000000000000000
		operatorMatrix.put('Wed', 983040);	  //Wed -> 0000000011110000000000000000
		operatorMatrix.put('Thu', 61440);     //Thu -> 0000000000001111000000000000
		operatorMatrix.put('Fri', 3840);      //Fri -> 0000000000000000111100000000
		operatorMatrix.put('Sat', 240);       //Sat -> 0000000000000000000011110000
		operatorMatrix.put('Sun', 15);        //Sun -> 0000000000000000000000001111
		return operatorMatrix;
	}

	//Get Availability Object Record Type Map
	public static Map<String, String> availabilityObjectRecordTypeMap(){
		Map<String, String> availObjectRecordTypeMap = new Map<String, String>();
		
		RecordTypeServices rtServices = new RecordTypeServices();
		Map<String, String> rtMap = rtServices.getRecordTypeMap();
		String rtId = rtMap.get('Roster');
		String atId = rtMap.get('Availability');

		availObjectRecordTypeMap.put(rtId, 'Roster');
		availObjectRecordTypeMap.put(atId, 'Availability');

		return availObjectRecordTypeMap;
	}
}