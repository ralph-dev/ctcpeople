/**
	Author by Jack
**/

public with sharing class ConRowRec {
		public Contact contactRec{get;set;}
		public Account accountRec{get;set;}
        public boolean isSelected{get;set;}
        public List<DocRowRec> docList{get;set;}
        //public AccRowRec company{get;set;}
        public Integer sendMailCode{get;set;} 
        public String ContactAccountName {get; set;}
        public String ContactAccountId {get; set;}
        public Integer divids {get; set;}
        public String addemails {get; set;}
        public String ContactEmail {get; set;}
        public String PrimaryEmail {get; set;}
        public ConRowRec(Contact c){
            this(c,true);
        }
        
       public ConRowRec(Contact c,boolean containDocs){
            contactRec=c;
            docList = new List<DocRowRec>();
            isSelected=false;
            if(containDocs){
                // all docs (resume/cover letter) are initially not included in email
               for(Web_Document__c docTmp:contactRec.Web_Documents__r){
                    docList.add(new DocRowRec(docTmp,this));
                }
            }
        }
        
       public ConRowRec(Contact c,Account a, Integer divid, String pEmail, String conEmail, String addemail){
            this(c,false);
            ContactAccountName = c.Name + ' , ' + a.Name;
            ContactAccountId = c.id + ':' + a.id;
            divids = divid;
            PrimaryEmail = pEmail;
            ContactEmail = conEmail;
            addemails = addemail;
        } 
        
        public String sendMailMsg{
			get{
				return SendMailService.getSendMailMsg(sendMailCode);
			}
		}      
}