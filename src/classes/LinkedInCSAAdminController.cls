/**
 * This controller is used to update LinkedIn CSA settings for LinkedInCSAAdmin page input 
 * Created by: Lina
 * Created date: 04/04/16
 **/ 
public with sharing class LinkedInCSAAdminController{
    
    private transient String apiKey;
    
    public String getapiKey(){
        return null;
    }

    public void setapiKey(String apiKey){
        this.apiKey = apiKey;
    }

    public String customerId { get; set; }
    public boolean testFlag { get; set; }
    
    public void saveCSA() {
        if (apiKey != null && apiKey != '' && customerId != null && customerId != '' ) {
            // Get the linkedIn Email for in CTCPeopleSettingHelper__c
            String linkedInEmail = getLinkedInEmail();
            if (linkedInEmail != null && linkedInEmail != '') {
                String companyName = UserInfo.getOrganizationName();
                LinkedIn_CSA__c csa = LinkedInCSASelector.getLinkedInCSA();
	            if (csa != null) {
                    csa.API_Key__c = apiKey;
                    csa.Customer_Id__c = customerId;
                    update csa;
	            } else {
	                LinkedIn_CSA__c csaToInsert = new LinkedIn_CSA__c();
	                csaToInsert.Name = companyName;
	                csaToInsert.API_Key__c = apiKey;
                    csaToInsert.Customer_Id__c = customerId;
                    insert csaToInsert;
	            }
	            
	            // Send API key and customer Id to LinkedIn
	            if (sendEmail(apiKey, customerId, companyName, linkedInEmail)) {
	                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'LinkedIn CSA Setting has been updated successfully!\n' 
	                                                                                    + 'An email with your new API Key and Contract Id has been send to ' + linkedInEmail + '.'));
	            } else {
	                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'LinkedIn CSA Setting has been updated, '
	                                                                                   + 'but email with your new API Key and Contract Id has not been send to ' + linkedInEmail + ' !'));
	                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please send email manually or try to update again.'));
	            }
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'There is no email address for LinkedIn CSA Registration!'));
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'LinkedIn CSA Setting has not been updated!'));
            }
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter both API key and Customer ID!'));
        }
    }
    
    public String getLinkedInEmail() {
        CTCPeopleSettingHelper__c linkedInReg = new CTCPeopleSettingHelper__c();
        CTCPeopleSettingHelperServices ctcServices = new CTCPeopleSettingHelperServices();
		CTCPeopleSettingHelperSelector ctcSelector = new CTCPeopleSettingHelperSelector();
		String rTID=ctcServices.getRecordTypeId('LinkedIn_CSA_Registration');
		list<CTCPeopleSettingHelper__c> linkedInRegList = ctcSelector.fetchCTCPeopleHelperSettingByRT(rTID);
		if (linkedInRegList != null && !linkedInRegList.isEmpty()) {
		    linkedInReg = linkedInRegList[0];
		}
		return linkedInReg.LinkedIn_Contact_Email__c;
    }
    
    public boolean sendEmail(String apiKey, String customerId, String companyName, String email) {
        String contactEmail = UserInfo.getUserEmail();
       // Messaging.reserveSingleEmailCapacity(1);
        try {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[]{email.trim()});
            mail.setCcAddresses(new String[]{'support@clicktocloud.com'});
            mail.setSubject('CSA Contract');
            mail.setPlainTextBody('Please complete the setup for ctcPeople Cross System Awareness for:\n\n'
                                + 'Company: ' + companyName + '\n'
                                + 'Primary Contact Name: ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + '\n'
                                + 'Primary Contact Email: ' + contactEmail + '\n'
                                + 'Customer API Key: ' + apiKey + '\n' 
                                + 'Customer Contract ID: ' + customerId + '\n\n'
                                + 'Thank you.');
                                
            if (!Test.isRunningTest()) {
                Messaging.SendEmailResult[] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            } else {
                system.debug(mail);
                return testFlag;
            }
            return true;
        } catch (system.Exception e){
            //system.debug(e);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
        return false;
    }
	
}