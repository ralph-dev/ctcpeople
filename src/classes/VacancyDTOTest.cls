@isTest
private class VacancyDTOTest {

    static testMethod void myUnitTest() {
    	System.runAs(DummyRecordCreator.platformUser) {
        Account acc = TestDataFactoryRoster.createAccount();
        Placement__c vac = TestDataFactoryRoster.createVacancy(acc);
        VacancyDTO vacDTO = TestDataFactoryRoster.createVacancyDTO(acc,vac);
        
        System.assertNotEquals(vacDTO.getVacancyCompanyName(),'');
        System.assertNotEquals(vacDTO.getVacancyName(),'');
        System.assertNotEquals(vacDTO.getVacancyEndDate(),'');
        System.assertNotEquals(vacDTO.getVacancyStartDate(),'');
        System.assertNotEquals(vacDTO.getVacancyId(),'');
        System.assertNotEquals(vacDTO.getVacancyCompanyId(),'');
        System.assertNotEquals(vacDTO.getVacancyCategory(),'');
        System.assertNotEquals(vacDTO.getVacancySpecialty(),'');
        System.assertNotEquals(vacDTO.getVacancySeniority(),'');
        System.assertEquals(vacDTO.getVacancyRecordTypeId(),'');
        System.assertNotEquals(vacDTO.getVacancyCompanyShippingState(),'');
        System.assertNotEquals(vacDTO.getVacancyCompanyShippingStreet(),'');
        System.assertNotEquals(vacDTO.getVacancyCompanyShippingCity(),'');
        System.assertNotEquals(vacDTO.getVacancyCompanyShippingPostalCode(),'');
        System.assertNotEquals(vacDTO.getVacancyCompanyShippingCountry(),'');
        System.assertNotEquals(vacDTO.getVacancyCompanyShippingLatitude(),'');
        System.assertNotEquals(vacDTO.getVacancyCompanyShippingLongitude(),'');
    	}
        
    }
}