/**************************************
 *                                    *
 * Deprecated Class, 13/06/2017 Ralph *
 *                                    *
 **************************************/
global with sharing class EnhancedMassRatingController{
	
	// Number of fixed columns which can't be changed
//	private Integer NUMBER_OF_DEFAULT_COLUMNS{
//		get{
//			Integer columns = 6;
//			if(enableSCNote){
//				columns++;
//			}
//			if(enableSCRating){
//				columns++;
//			}
//			if(enableSCProgress){
//			    columns++;
//			}
//			return columns;
//		}
//	}
//	// Number of displayt columns if screen candidate user preference is not there
//	private Integer NUMBER_OF_COLUMNS_IF_NO_SETTING = 9; 
//	private String CANDIDATE_INFO_SECTION_ID_PREFIX = 'candidateInfoSection-';
	
	
///** this class is used to rank vacancy candidate workflow*/
///** used in page : massrating*/
//    public Placement_Candidate__c vcwf{get;set;}
//    public List<Placement_Candidate__c> vcwfs{get;set;}
//    public List<Placement_Candidate__c> allvcwf;
//    public List<Placement_Candidate__c> temp;
//    public ApexPages.StandardSetController thecontrollers {get;set;}
//    public Integer totalPage {get;set;}
//    public Id selectedId {get;set;}
//    public string vid {get; set;}
//    public ApexPages.StandardSetController stdSetControllerTemp;
    
//    //URL params
//    public string cmids;
//    public List<String> vids;
//    public string currentpage;
//    public String currentSelectCrieria;
//    public String tempdata;
    
//    private String currentAscSelectCrietria;
//    public String commandlinkvalue {get; set;}
//    public String idListOfRecentPageRecords{get;set;}
//    public enum GoBackOption {NONE,SHOW_ALL,VACANCY_DETAIL}
//    private GoBackOption goBackTo=GoBackOption.NONE;
//    private PageReference goBackPage = null;
//    public transient String contactMapJSON{get;set;}// A JSON string to store resume content so that later they can be accessed by contact Id
//   	public String keywords{get; set;}
//   	public String retUrl;                           // set current url as return url
//   	public String retUrlfromCMList;                 // get return url from CM list
   	
//   	//Enable customer setting
//   	public Boolean enableSOSLSearch{get; set;}
//   	public Boolean enableSCNote{get; set;}
//	public Boolean enableSCRating{get; set;}
//	public Boolean enableSCProgress{get; set;}
	
//	//User Preference function
//    public User_Preference__c currentUserPreference {get; set;}
//    public String searchcolumnString;
//    public String columnsJSON {get; set;}
//    public static String maditerycolumnsstring = 'Id, Name, Status__c,Candidate_Status__c, Candidate__r.Id, Candidate__r.Name, Candidate__c ,Rating__c ,Recruiter_Notes__c ';
//	public List<DisplayColumn> scdisplaycolumn {get;set;}
//	public Map<String, Boolean> scDisplayColumnSortableMap;
//	public Boolean columnejsonnotexist{get; set;}
//	public String basequerystring;
	
//	// Sorting search result by column
//    public String sortBycolumnApiName{get;set;}
//    public static String DEFAULT_SORT_BY_COLUMN_API_NAME = 'CreatedDate'; 
//    public String sortingOrder{get;set;}
//    public static String DEFAULT_SORTING_ORDER = 'DESC'; 
    
//	//Init Error Message for customer
//    public PeopleCloudErrorInfo errormesg; 
//    public Boolean msgSignalhasmessage{get ;set;} //show error message or not
    
//    public Integer numberOfDisplayColumns{get;set;} // Determine colspan of candidate detail section 
//    public Id candidateIdToRerenderSection{get;set;} // Candidate Id used for renderring candidate info section  
//    public Id cmIdToAppendDetailSection{get;set;} // Append candidate info to detail section with this candidate management Id 
//    public List<String> candidateInfoSectionIds{get;set;} // Ids of candidate info section which will be used for rerendering
//    public Map<String, boolean> checkthisrecords{get;set;}
//    public Map<String, String> cmcontactMap;
//    public PageReference currentPageTemp; //keep current page 
//    public String previousSortBycolumnApiName;
    
//    public String namespace{
//    	get{
//            return PeopleCloudHelper.getPackageNamespace();
//        }
//    }
    
//    // use for page redirect
//    public String candId{get;set;}
//    public static Integer RECORD_PER_PAGE = 100;
//    //public static Integer RECORD_PER_PAGE = 20; //20 for test
//    public EnhancedMassRatingController(ApexPages.StandardSetController stdSetController) {
//    	currentPageTemp = ApexPages.currentPage();	//get return url
//    	keywords = '';								//Search Keyword
//    	checkthisrecords = new Map<String, boolean>();
//       	stdSetControllerTemp = stdSetController;
       	
//       	//Check SOSL search is available 
//       	PCAppSwitch__c pcSwitch=PCAppSwitch__c.getOrgDefaults();
//       	enableSOSLSearch = pcSwitch.Enable_SOSL_Search_for_Screen_Candidate__c;
//       	enableSCNote = pcSwitch.Enable_Screen_Candidate_Note__c;
//       	enableSCRating = pcSwitch.Enable_Screen_Candidate_Rating__c;
//       	enableSCProgress = !pcSwitch.enable_Screen_Candidate_Progress__c;
//       	//get the info from query string 
//       	vid = ApexPages.currentPage().getParameters().get('id');
//      	cmids = ApexPages.currentPage().getParameters().get('cmids');
//       	currentpage = ApexPages.currentPage().getParameters().get('cp');
//       	currentSelectCrieria = ApexPages.currentPage().getParameters().get('sc');
//       	currentAscSelectCrietria = ApexPages.currentPage().getParameters().get('asc');
//		tempdata = ApexPages.currentPage().getParameters().get('tempdata');
//		retUrlfromCMList = ApexPages.currentPage().getParameters().get('retUrl');
//    }
    
//    //Init Enhanced Mass Rating Controller
//    public void Init(){
//    	String tempsoqlqueryString; 				//Set init temp soql query string
//    	//Delete old SC temp data record
//       	EnhancedMassRatingPersistenceServie enhancedMassRatingPersistenceServie = new EnhancedMassRatingPersistenceServie();
//       	try{
//       		enhancedMassRatingPersistenceServie.cleanOldWorkbenchStatuses();
//       	}catch(Exception e){
       	
//       	}
       	
//       	InitEnhancedMassRatingController();  //Init Screen Candidates column and check Screen Candidate column basequerystring
//       	if(tempdata!= null){//Source from Add to other vacancy page
//        	DaoTemporaryData daotempdata = new DaoTemporaryData();
//        	Temporary_Data__c tempdatarecord = daotempdata.retrieveScreenCandidateTemporaryDataById((Id)tempdata);
//        	checkthisrecords = (Map<String, boolean>)JSON.deserialize(tempdatarecord.Secondary_Data_Content__c,Map<String, boolean>.class);
//        	//system.debug('checkthisrecords ='+ checkthisrecords);
//        }
        
//        List<String> cmIdList = new List<String>();
//       	if(cmids!=null){
//       		if(checkthisrecords == null || checkthisrecords.size()<1){
//           		cmIdList = cmids.split(',');
//           	}else{
//           		cmIdList.addAll(checkthisrecords.keySet());
//           	}
//           	commandlinkvalue = '<<< Back to Show All Page'; 
//           	if(basequerystring == null || basequerystring =='')              
//           		basequerystring = 'select Id, Name,Status__c,Candidate_Status__c,Candidate_Source1__c,Rating__c,Candidate__r.Id,Recruiter_Notes__c, Candidate__r.Name,Candidate__c,candidate__r.LastName,candidate__r.FirstName from Placement_Candidate__c';     
//           	goBackPage = reConstructShowAll();
//        }else{
//           	if(checkthisrecords == null || checkthisrecords.size()<1){
//	           	thecontrollers = stdSetControllerTemp;
//	           	for(Placement_Candidate__c vm:(List<Placement_Candidate__c>)thecontrollers.getSelected()){
//	            	cmIdList.add(vm.Id);
//	           	}
//           	}else{
//           		cmIdList.addAll(checkthisrecords.keySet());
//           	}
//            /**
//           	If recruiter didnot choose any candidate management. He/She would get the Candidate management whose status is new. edited by Alvin
//           	**/
//           	if(cmIdList.size()==0){
//           		List<Placement_Candidate__c> testa=new List<Placement_Candidate__c>();
//				CommonSelector.checkRead(placement_Candidate__c.SObjectType,'Id, Name, Status__c');
//           		testa=[select Id, Name, Status__c from placement_Candidate__c where Placement__c=:vid and Status__c='New' limit 500];
           		
//           		for(Placement_Candidate__c pcl: testa){
//           			cmIdList.add(pcl.Id);
//           		}
           	
//           	}
//           	if(basequerystring == null || basequerystring =='')
//           		basequerystring = 'select Id, Name,Status__c,Candidate_Status__c,Candidate_Source1__c,Rating__c,Candidate__r.Id,Recruiter_Notes__c, Candidate__r.Name,Candidate__c,candidate__r.LastName,candidate__r.FirstName from Placement_Candidate__c'; 
           	
//           	if(vid != null && vid != ''){
//           		commandlinkvalue = '<<< Back to Vacancy';
//           	}else{
//           		commandlinkvalue = '<<< Back to Candidate Mangement List';
//           	}
//           	goBackpage= new PageReference('/'+retUrlfromCMList);
//       	}
//       	//replace the generateQueryString() method.
//       	PlacementCandidateSelector pcs = new PlacementCandidateSelector();
//        allvcwf = pcs.getAllVacancyCandidateWorkflow(searchcolumnString,sortBycolumnApiName, sortingOrder, cmIdList);
//       	//system.debug('previouscheckthisrecords ='+ checkthisrecords);
//       	if(checkthisrecords == null || checkthisrecords.size()<1){
//       		checkthisrecords = generalCheckboxMap(allvcwf,true);
//       	}else{
//       		generalCheckboxMap(allvcwf,false);
//       	}
//       //	system.debug('checkthisrecords ='+ checkthisrecords);
//       	//system.debug('allvcwf ='+ allvcwf);
//       	generateVCWFControler(allvcwf);
//    }
    
//    //Get User Preference Details
//    public void InitEnhancedMassRatingController(){
//    	currentUserPreference = GoogleSearchValidate.checkUserPerformanceValidate();
    	
//    	if(currentUserPreference == null){
//            customerErrorMessage(PeopleCloudErrorInfo.ERR_NO_CONTACT_SELECTED);
//        }else{
//        	if(currentUserPreference.Sort_Column_Api_Name__c != null && currentUserPreference.Sort_Column_Api_Name__c != '' ){
//                sortBycolumnApiName = currentUserPreference.Screen_Candidate_Sort_Column_API_Name__c;
//                //system.debug('retrieved from user preference: sortBycolumnApiName ='+ sortBycolumnApiName);
//            }else{
//                sortBycolumnApiName = DEFAULT_SORT_BY_COLUMN_API_NAME;
//                //system.debug('initializeing sortBycolumnApiName ='+ sortBycolumnApiName);
//            }
            
//            // set sort by column order from user preference
//            if(currentUserPreference.Sort_Order__c != null && currentUserPreference.Sort_Order__c != '' ){
//                sortingOrder = currentUserPreference.Sort_Order__c;
//                //system.debug('retrieved from user preference: sortingOrder ='+ sortingOrder);
//            }else{
//                sortingOrder = DEFAULT_SORTING_ORDER;
//                //system.debug('initializeing sortingOrder ='+ sortingOrder);
//            }
            
//            if(currentUserPreference.Screen_Candidate_Column__c != null && currentUserPreference.Screen_Candidate_Column__c != ''){
//            	columnejsonnotexist = true;
//                Map<String,DisplayColumn> relationNameMap = FieldsClass.getReferenceFieldRelationshipMap('Placement_Candidate__c');
//                //system.debug('relationNameMap ='+ relationNameMap);
                
//                String tempColumnsJSON = currentUserPreference.Screen_Candidate_Column__c;
//                //system.debug('tempColumnsJSON ='+ tempColumnsJSON);       
                 
//                scdisplaycolumn = GoogleSearchUtil2.generateNewDisplayColumnWithReferenceField(tempColumnsJSON, relationNameMap);
//                scDisplayColumnSortableMap = EnhancedMassRatingPersistenceServie.GeneralSortableColumnMap(scdisplaycolumn);
//                //system.debug('scdisplaycolumn ='+ scdisplaycolumn);    
                 
//                columnsJSON = GoogleSearchUtil2.generateColumnJSON(tempColumnsJSON, relationNameMap);
//                //system.debug('columnsJSON ='+ columnsJSON);    
                
//                searchcolumnString = GoogleSearchUtil2.generateColumnString(columnsJSON, relationNameMap);
//                if(searchcolumnString != null && searchcolumnString != ''){
//                	searchcolumnString = maditerycolumnsstring +searchcolumnString;
//                }else{
//                	searchcolumnString = maditerycolumnsstring;
//                }
//                //system.debug('searchcolumnString ='+ searchcolumnString);
//                numberOfDisplayColumns = NUMBER_OF_DEFAULT_COLUMNS + scdisplaycolumn.size();               
//            }else{
//            	columnejsonnotexist = false;
//            	numberOfDisplayColumns = NUMBER_OF_COLUMNS_IF_NO_SETTING;
//            	searchcolumnString = maditerycolumnsstring + ', candidate__r.LastName, candidate__r.FirstName, candidate__r.public_Profile_Url__c, Candidate_Source1__c ';
//            }
//        }
//        basequerystring = 'Select '+ searchcolumnString + ' from Placement_Candidate__c';
//    }
    
//    //Generate cm controller and number of pages
//    private void generateVCWFControler(List<Placement_Candidate__c> displayvcwf){
//    	thecontrollers = new ApexPages.StandardSetController(displayvcwf);
//      	thecontrollers.setPageSize(RECORD_PER_PAGE);
//       	if(displayvcwf.size()>0){
//        	if(math.mod(displayvcwf.size(), RECORD_PER_PAGE) > 0){
//            	totalPage = displayvcwf.size() /RECORD_PER_PAGE + 1;
//            }else{
//            	totalPage = displayvcwf.size() /RECORD_PER_PAGE ;
//            }
//        }else{
//            totalPage =0;
//        }
//        getCurrVCWF();
//        candidateInfoSectionIds = generateCandidateInfoSectionIds(vcwfs);
//    }
    
//    public void getCurrVCWF(){
//    	List<Id> candIdList = new List<Id>();
//       	List<Id> cmIdList = new List<Id>();
//       	vcwfs = (List<Placement_Candidate__c>)thecontrollers.getRecords();
//       	for(Placement_Candidate__c cm: vcwfs){
//       		if(cm.Candidate__c!=null)
//                candIdList.add(cm.Candidate__c); // candIdList will be used in keywords searching to limit the records that we search on
//            cmIdList.add(cm.Id);
//       }
//		CommonSelector.checkRead(Contact.SObjectType,'Id,FirstName,LastName, Resume__c');
//		CommonSelector.checkRead(Web_Document__c.SObjectType,'Id, OwnerId, CreatedDate, Link_to_File__c,Link_to_File_Sec__c ');
//	   Map<Id,Contact> contactMap = new Map<Id,Contact>([select Id,FirstName,LastName, Resume__c, 
//	   					(Select Id, OwnerId, CreatedDate, Link_to_File__c,Link_to_File_Sec__c 
//	   					From Web_Documents__r where Type__c='resume' order by CreatedDate DESC limit 1)
//	   				from Contact where Id in:candIdList]);
	   
//	   contactMapJSON = JSON.serialize(contactMap);
//       idListOfRecentPageRecords = System.JSON.serialize(candIdList);
//    }
    
//    public void rerenderCandidateInfo(){
    	
//    }
    
//    /**
//    	Generate candidate info section ids which will be used for reredering apex:detail component, when
//    	section is expanded or the display results have been changed.
//    */
//    private List<String> generateCandidateInfoSectionIds(List<Placement_Candidate__c> cmsToAppendTo){
//    	List<String> candidateInfoSectionIds = new List<String>();
//    	for(Placement_Candidate__c cmToAppendTo : cmsToAppendTo){
//    		candidateInfoSectionIds.add(CANDIDATE_INFO_SECTION_ID_PREFIX + cmToAppendTo.Id);
//    	}
//    	return candidateInfoSectionIds;
//    }
    
    
//    public pageReference updateColumnAndQueryCriteriaActionFunction(){
//    	InitEnhancedMassRatingController();
//    	searchkeywords();
//    	return null;
//    }
    
//    public pageReference massupdate(){
//        //TriggerHelper.SyncTimeSheetApproverEnable=false;
//        //TriggerHelper.UpdateReminingEnable=false;
//        thecontrollers.save();
//        getCurrVCWF();
//        return null;
//    }
    
//    public pageReference massUpdateAndReturn(){
//        massupdate();
//        return goBack();
//    }
    
//    public void previous(){
//    	massupdate();
//      	thecontrollers.previous();
//      	getCurrVCWF();
//    }
    
//    public void first(){
//       	massupdate();
//       	thecontrollers.first();
//       	getCurrVCWF();
//    }
    
//    public void last(){
//       	massupdate();
//       	thecontrollers.last();
//       	getCurrVCWF();
//    }
    
//    public void next(){
//       	massupdate();
//       	thecontrollers.next();
//       	getCurrVCWF();
//    }
//    public void updateSelectedId(){
    
//    }
    
//    public PageReference redirectToCand(){
//        PageReference redirectPage = null;
//        if(candId != null){
//            redirectPage = new PageReference('/'+candId);
//        } 
//        return redirectPage;
//    }

//    private PageReference reConstructShowAll(){
//        PageReference showallpage = page.IframeParentStandard;
//        showallpage.setRedirect(true);
//        showallpage.getParameters().put('sc',currentSelectCrieria);
//        showallpage.getParameters().put('cp',currentpage);
//        showallpage.getParameters().put('id',vid);
//        showallpage.getParameters().put('asc',currentAscSelectCrietria);
//        showallpage.setRedirect(true);
//        return showallpage;
//    }

//    public PageReference goBack(){
//        return goBackPage;
//    }
    
//    public PageReference cancel(){
//        return goBack();
//    }
    
   @RemoteAction
//    /**
//    	This method is deprecated. No page or button is using this.
//    */
    global static String searchCandidates(String keywords,String idListOfRecentPageRecord){
//        List<Id> candIds = new List<Id>();
//        /*
//         idListOfRecentPageRecord is a JSON formatted array,
//         code below is to covert this JSON array to a id List.
//        */
//        JSONParser parser=JSON.createParser(idListOfRecentPageRecord);
//        // Advance to the start object marker.
//        parser.nextToken();
//        // Advance to the next value.
//        parser.nextValue();
//        while(parser.hasCurrentToken() && parser.getCurrentToken()!= Jsontoken.END_ARRAY && parser.getCurrentToken()!= Jsontoken.END_OBJECT){
//            ID candId = parser.getIdValue();
//            candIds.add(candId);
//            //system.debug('fieldValue:'+candId);
//            // Advance to the next value.
//            parser.nextValue();
//        }
//        /*
//            TODO: add code to handle empty keywords situation
//        */

//        if(candIds.size()==0)
//            return '[]';
            
//        // Search keywords upon all contact text and textare fields 
//        List<List<Contact>> findResult = (List<List<Contact>>)[find :keywords returning Contact(Id,Resume__c where id in :candIds) ];
//        List<Contact> conList=findResult[0];
//        String jsonResult = JSON.serialize(conList);        
//        //system.debug('ids of recent page record:'+candIds);
//        return jsonResult;
//        //return '[]';
      return null;
    } 
	
//	//Search keywords
//	public PageReference searchkeywords(){
//		//system.debug('keywords ='+ keywords);
//		List<Placement_Candidate__c> sosldisplaycmlist = new List<Placement_Candidate__c>();
//		List<Placement_Candidate__c> displaycmlist = new List<Placement_Candidate__c>();
//		//system.debug('basequerystring ='+ basequerystring);
//		sosldisplaycmlist = EnhancedMassRatingPersistenceServie.searchCandidatesByKeywords(keywords , allvcwf);
		
//    //Replace generateQueryString() method
//    PlacementCandidateSelector pcs = new PlacementCandidateSelector();      
//    displaycmlist = pcs.getAllVacancyCandidateWorkflow(searchcolumnString,sortBycolumnApiName, sortingOrder, sosldisplaycmlist);

//		system.debug('displaycmlist ='+ displaycmlist.size());
//		generateVCWFControler(displaycmlist);
//		if(displaycmlist!= null && displaycmlist.size()>0){
			
//		}else{
//			customerErrorMessage(PeopleCloudErrorInfo.NO_CANDIDATE_MATCHED);
//		}
//		//system.debug('thecontrollers ='+ thecontrollers);
//		return null;
//	}
	
//	//Error message
//	public void customerErrorMessage(Integer msgcode){
//        errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
//        	msgSignalhasmessage = errormesg.returnresult;
//            for(ApexPages.Message newMessage:errormesg.errorMessage){
//            	ApexPages.addMessage(newMessage);
//            }               
//    }
    
	
//	//Sort Column by API Name
//	public PageReference sortColumn(){
//		//system.debug('checkthisrecords ='+ checkthisrecords);
//		String currentsortBycolumnApiName = sortBycolumnApiName;  //set sortBycolumnApiName to a temp string
		
//		List<Placement_Candidate__c> displaycmlist = new List<Placement_Candidate__c>();
		
//		if(sortingOrder == null || sortingOrder == '' || sortingOrder == DEFAULT_SORTING_ORDER   //If sortingOrder is null or Previous Sorting Column is null or Previous Soring column not equals current sotring column, set Sort by ASC
//				|| previousSortBycolumnApiName == null || previousSortBycolumnApiName == '' || (previousSortBycolumnApiName != null && previousSortBycolumnApiName != currentsortBycolumnApiName)){
//			sortingOrder = 'ASC';
//		}else{
//			sortingOrder = DEFAULT_SORTING_ORDER;
//		}
//		previousSortBycolumnApiName = currentsortBycolumnApiName;
//		if(currentsortBycolumnApiName == null || currentsortBycolumnApiName == ''){
//			currentsortBycolumnApiName = sortBycolumnApiName;
//		}else{
//			if(scDisplayColumnSortableMap!=null && scDisplayColumnSortableMap.size()>0 && //if customer column not exist, do nothing
//				scDisplayColumnSortableMap.get(currentsortBycolumnApiName)!=null && !scDisplayColumnSortableMap.get(currentsortBycolumnApiName)){
//				currentsortBycolumnApiName = '';
//				customerErrorMessage(PeopleCloudErrorInfo.COLUMN_NOT_SORTABLE);
//				return null;
//			}
//		}
		
//		displaycmlist = EnhancedMassRatingPersistenceServie.searchCandidatesByKeywords(keywords , allvcwf);
//    //replace the generateQueryString() method.
//    PlacementCandidateSelector pcs = new PlacementCandidateSelector();
//    displaycmlist = pcs.getAllVacancyCandidateWorkflow(searchcolumnString,currentsortBycolumnApiName, sortingOrder, displaycmlist);
	
//		generateVCWFControler(displaycmlist);
//		return null;
//	}
	
//	//General Checkbox map
//	public Map<String, Boolean> GeneralCheckboxMap(List<Placement_Candidate__c> vcwfs, boolean reGeneralCheckBoxMap){
//		cmcontactMap = new Map<String, String>();
//		Map<String, Boolean> tempMap = new Map<String, Boolean>();
//			for(Placement_Candidate__c cm: vcwfs){
//				if(reGeneralCheckBoxMap){
//					tempMap.put(cm.id, false);
//				}
//				if(cm.Candidate__c != null){
//					cmcontactMap.put(cm.id, cm.Candidate__c);
//				}
//		}
//		return tempMap;
//	}
	
//	//Add to other vacancy
//	public PageReference AddToAnotherVacancy(){
//		//system.debug('checkthisrecords ='+ checkthisrecords);
//		List<String> selectcontactidlist = new List<String>();
//		Temporary_Data__c generalTemporaryDataForScreenCandidate = new Temporary_Data__c();
		
//		for(String cmid:checkthisrecords.keyset()){
//			system.debug('istrue ='+ checkthisrecords.get(cmid));
//			if(checkthisrecords.get(cmid) == true && cmcontactMap.get(cmid)!= null){
//				selectcontactidlist.add(cmcontactMap.get(cmid));
//			}
//		}
//		if(selectcontactidlist != null && selectcontactidlist.size()>0){
//			generalTemporaryDataForScreenCandidate = GeneralTemporaryData.generalTemporaryDataForScreenCandidate(selectcontactidlist, checkthisrecords);
//			if(generalTemporaryDataForScreenCandidate != null){
//				currentPageTemp.getParameters().put('tempdata',generalTemporaryDataForScreenCandidate.id);
//				currentPageTemp.getParameters().put('retUrl',retUrlfromCMList);
//				retUrl = currentPageTemp.getURL();	
//				//system.debug('baseretUrl ='+ retUrl);			
//				retUrl = EncodingUtil.urlEncode(retUrl,'UTF-8');
//				//system.debug('retUrl ='+ retUrl);
//				PageReference forwardpage = Page.AddCandidatestoVacancy;
//				forwardpage.setRedirect(true);
//				forwardpage.getParameters().put('tempdata',generalTemporaryDataForScreenCandidate.id);
//				forwardpage.getParameters().put('returnurl',retUrl);
//				forwardpage.getParameters().put('sourcepage','screencandidate');
//				//system.debug('forwardurl ='+ forwardpage.getUrl());
//				return forwardpage;
//			}else{
//				customerErrorMessage(PeopleCloudErrorInfo.CREATE_TEMPDATA_RECORD_FAILED);
//				return null;
//			}
//		}else{
//			customerErrorMessage(PeopleCloudErrorInfo.NO_AVAILABLE_CANDIDATE_TO_ADD_VACANCY);
//			return null;
//		}
//	}
}