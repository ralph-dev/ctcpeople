public with sharing class ActivityDocController {
    
    private Task task;
    public List<ContentDocumentLink> files {get; set;}
    public List<ActivityDoc> activityDocs {get; set;}
    public List<SelectOption> docTypePickListValue {get; set;}
    public Contact contact {get; set;}
    public boolean enableskillparsing {get; set;}
    public List<String> defaultskillgroups {get;set;} //get the default skill group value
    public List<SelectOption> selectSkillGroupList {get; set;}
    public String whoUrl{get;set;} //added by emily to replace contactUrl
    public String whatUrl{get;set;} //added by emily to replace contactUrl
    public String whoName{get;set;} //added by emily to replace contactName
    public String whatName{get;set;} //added by emily to replace contactName
    public String activityUrl {get; set;}
    public boolean isvalid {get; set;}
    
    //Init Error Message for customer
    public PeopleCloudErrorInfo errormesg; 
    public Boolean msgSignalhasmessage{get ;set;} //show error message or not
    
    public ActivityDocController(ApexPages.StandardController activity) {
        isvalid = true;
        String skillParsing;
        
        enableskillparsing = SkillGroups.isSkillParsingEnabled();//Check Skill function is enable or not
        skillParsing = String.Valueof(enableskillparsing);
        
        selectSkillGroupList = ActivityDocSelector.GetSelectSkillGroupList(); //Get Skill Group Select Option List
        User currentUser = UserInformationCon.getUserDetails;
        
        activityDocs = new List<ActivityDoc>();
        task = (Task)activity.getRecord();
        String id = task.id;
        activityUrl = '/'+id;
        
        task = ActivityDocSelector.getTaskById(id);
        files = ContentDocumentLinkSelector.getFileByLinkedEntityId(id);
        
        //added by emily to replace contactUrl
        if(task.Who.name !=null && task.Who.name !='')
        {
            whoUrl='/' + task.WhoId;    
            whoName=task.Who.name;
        }
       
       //added by emily to replace contactUrl
       if(task.What.name !=null && task.What.name !='' )
        {
            whatUrl='/' + task.WhatId;
            whatName=task.What.name;
        }

        if(currentUser.AmazonS3_Folder__c != null && currentUser.AmazonS3_Folder__c != '') {
            
            if(task.Attachments != null &&  task.Attachments.size()>0) {
                for(Attachment att : task.Attachments) {
                    ActivityDoc activityDoc = new ActivityDoc();
                    //added by andy yang
                    activityDoc.userId = currentUser.Id;
                    //
                    
                    activityDoc.orgId = Userinfo.getorganizationid().subString(0,15);
                    activityDoc.activityId = task.Id;
                    activityDoc.whoMap.put(task.WhoId,getLookUpFieldNameViaObjectName(task.Who.type));
                    activityDoc.whatMap.put(task.WhatId,getLookUpFieldNameViaObjectName(task.What.type));
                    activityDoc.resourceType = 'Attachment';
                    activityDoc.resourceName = att.Name;
                    activityDoc.resourceId = att.Id;
                    activityDoc.amazonS3Folder = currentUser.AmazonS3_Folder__c;
                    activityDoc.nameSpace = PeopleCloudHelper.getPackageNamespace();
                    activityDoc.enableSkillParsing = skillParsing;
                    activityDocs.add(activityDoc);
                }
            }
            
            if(files!=null && files.size()>0) {
                for(ContentDocumentLink f : files) {
                    ActivityDoc activityDoc = new ActivityDoc();
                    //added by andy yang
                    activityDoc.userId = currentUser.Id;
                    //
                    
                    activityDoc.orgId = Userinfo.getorganizationid().subString(0,15);
                    activityDoc.activityId = task.Id;
                    activityDoc.whoMap.put(task.WhoId,getLookUpFieldNameViaObjectName(task.Who.type));
                    activityDoc.whatMap.put(task.WhatId,getLookUpFieldNameViaObjectName(task.What.type));
                    activityDoc.resourceType = 'File';
                    activityDoc.resourceName = f.ContentDocument.title;
                    activityDoc.resourceId = f.ContentDocumentId;
                    activityDoc.amazonS3Folder = currentUser.AmazonS3_Folder__c;
                    activityDoc.nameSpace = PeopleCloudHelper.getPackageNamespace();
                    activityDoc.enableSkillParsing = skillParsing;
                    activityDocs.add(activityDoc);
                    
                }
                
            }
        }else {
            isvalid = false;
            if(currentUser.AmazonS3_Folder__c == null && currentUser.AmazonS3_Folder__c == '') {
                customerErrorMessage(PeopleCloudErrorInfo.ACTIVITY_DOC_NO_AMAZON_FOLDER);
            }else if(task.Attachments == null ||  task.Attachments.size()<1) {
                customerErrorMessage(PeopleCloudErrorInfo.ACTIVITY_HAVE_NO_ATTACHMENT);
            }else {
                customerErrorMessage(PeopleCloudErrorInfo.ACTIVITY_NOT_VALID);
            }
        }
        
        docTypePickListValue = ActivityDocSelector.GetResumeFileSelectOption();
    }
    
    public PageReference uploadDocumentServices() {
        List<ActivityDoc> selectedActivityDocs = new List<ActivityDoc>();
        Integer returnResponseCode;
        String taskId;
        for(ActivityDoc doc : activityDocs) {
            if(Test.isRunningTest()){
                doc.isSelected = true;
            }
            
            if(doc.isSelected == true){
                taskId = doc.activityId;
                doc.skillGroupIds = defaultskillgroups;
                selectedActivityDocs.add(doc);
            }
        }
        if(selectedActivityDocs != null && selectedActivityDocs.size()>0){
            String jsonString = JSON.serialize(selectedActivityDocs);
            //Added by Alvin for fixing the encoding issues when there are some special characters.
            jsonString=EncodingUtil.urlEncode(jsonString,'UTF-8');
            String sessionId = EncodingUtil.urlEncode(UserInfo.getSessionId(), 'UTF-8');
            String surl = EncodingUtil.urlEncode(URL.getSalesforceBaseUrl().toExternalForm() +'/services/Soap/u/25.0/'+UserInfo.getOrganizationId(),'UTF-8');
            returnResponseCode = ActivityDocServices.UploaddocumentsByEc2(jsonString, sessionId, surl);
            
            if(returnResponseCode == 200) {
                ActivityDocOperator.updateActivityStatus('Uploading','Request has been sent out', taskId);
                customerErrorMessage(PeopleCloudErrorInfo.ACTIVITY_SEND_REQUEST_SUCCESS);
            }else {
                ActivityDocOperator.updateActivityStatus('Error','Server error', taskId);
                customerErrorMessage(PeopleCloudErrorInfo.ACTIVITY_SEND_REQUEST_ERROR);
            }
        }
        return null;
    }
    
    public void customerErrorMessage(Integer msgcode){
        errormesg = new PeopleCloudErrorInfo(msgcode,true);                                     
            msgSignalhasmessage = errormesg.returnresult;
            for(ApexPages.Message newMessage:errormesg.errorMessage){
                ApexPages.addMessage(newMessage);
            }               
    }
    
    
    public String getLookUpFieldNameViaObjectName(String objectApiName)
    {
        //all fields of resume and files 
        //key is the field api name 
        Map<String,Schema.SObjectField> resumeObjectFields 
                    = Web_Document__c.SObjectType.getDescribe().fields.getMap();
                    
        //map contains resume lookup object api name and corresponding filed api name
        Map<String,String> resumeLookUpObjectFieldMap=new Map<String,String>();
        String fieldApiName='';
        for(String key:resumeObjectFields.KeySet())
        {
            Schema.SObjectField s=resumeObjectFields.get(key);
            String resumefieldApiName = s.getDescribe().getName();
            String resumeRelatedOjectName='';
            
            //Check if the field is lookup field
            if(s.getDescribe().getReferenceTo().size()>0)
            {
                resumeRelatedOjectName
                    =s.getDescribe().getReferenceTo().get(0).getDescribe().getName();
            }
            
            //put lookup object name and field name in the map
            if(resumeRelatedOjectName != '' && resumeRelatedOjectName != null)
            {
                resumeLookUpObjectFieldMap.put(resumeRelatedOjectName,resumefieldApiName);
            }
        }
        
        fieldApiName=resumeLookUpObjectFieldMap.get(objectApiName);
        
        return fieldApiName;
    }
}