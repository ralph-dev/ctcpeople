/**
 * This is the extension controller for Seek Job Posting function pages
 * The main function of this extension is to set the ad record type to seek advertisement
 *
 * The test code is included in JobBoardFactoryTest.cls
 */

public with sharing class JobPostingExtensionSeek {

    public JobPostingExtensionSeek(ApexPages.StandardController stdController){
        Advertisement__c ad = (Advertisement__c) stdController.getRecord();
        ad.RecordTypeId = DaoRecordType.seekAdRT.Id;
    }

}