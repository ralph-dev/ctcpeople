@isTest
private class TestAstutePayrollAction {
	
	/**
	* modified by andy for security review II
	*/
    private static testmethod void uniTest(){
    	System.runAs(DummyRecordCreator.platformUser) {
        RecordType APRecordType=new RecordType();
        APRecordType=DaoRecordType.AstutePayrollAccountRT;
        
        //insert AstutePayroll Account
        StyleCategory__c apAccount=new StyleCategory__c();
        apAccount.RecordTypeId=APRecordType.id;
        //apAccount.AstutePayroll_api_key__c='key';
        //apAccount.AstutePayroll_api_password__c='password';
        //apAccount.AstutePayroll_api_username__c='name';
        
        CommonSelector.quickInsert(apAccount);
        CredentialSettingService.insertAstutePayrollCredential(apAccount.id,'name','password','key');
        
         
        //assign user with AP account
        
         User currentuser=(User) new CommonSelector(User.sObjectType).getOne('id, AstutePayrollAccount__c','id =\''+Userinfo.getUserId()+'\'');//[select id, AstutePayrollAccount__c from User where id =: Userinfo.getUserId()];
         currentuser.AstutePayrollAccount__c=apAccount.id;
         
         CommonSelector.quickUpdate(currentuser);
         
        //Insert Account and Contact
       
        
        Account accountTest=new Account();
        accountTest.Name='TestAcc';
        CommonSelector.quickInsert(accountTest);
        
        Contact testCon=new Contact();
        testCon.lastname='Goran';
        testCon.firstname='tstesdfsda1212e';
        testCon.email='tesdsfsdfsft@example.com';
        testCon.AccountId=accountTest.Id;
        
        CommonSelector.quickInsert(testCon);
        
        
//insert candidate
        Contact cand1=new Contact();
        cand1.firstname='testfirstname';
        cand1.lastname='testLastName';
        
        CommonSelector.quickInsert(cand1);
        
// insert vacancy
        Placement__c vacancy=new Placement__c();
        vacancy.name='Test';
        vacancy.Company__c=accountTest.Id;
        vacancy.biller_contact__c=testCon.id;
        
        CommonSelector.quickInsert(vacancy);
        
//insert Candidate Management.
                
        Placement_Candidate__c cmTest=new Placement_Candidate__c();
        cmTest.candidate__c=cand1.id;
        cmTest.Placement__c=vacancy.id;
        
        CommonSelector.quickInsert(cmTest);
       
        
//Test AstutePayroll CheckActive User function. 
                
        list<Placement_Candidate__c> placements=new list<Placement_Candidate__c>();
        placements.add(cmTest);
        AstutePayrollAction apAction=new AstutePayrollAction();
        String jsonStr=apAction.CheckActiveUserJson(placements,apAccount);
        System.assert(jsonStr.length()>0);
        HttpResponse res=new HttpResponse();
        res=apAction.AstutePayrollCheckActiveUsers(jsonstr);
        System.assertEquals(res.getStatusCode(),200);
        
        
//Test AstutePayroll Upload candidate function.     
        
        HttpResponse resSecond=new HttpResponse();
        resSecond=apAction.AstutePayrollUpload(cmTest.id);
        
        System.assertEquals(resSecond.getStatusCode(),200);
        
        
//end Test    

//Test wrong mapping
		list<CTCPeopleSettingHelper__c> fieldsMaps=new list<CTCPeopleSettingHelper__c>();
		list<RecordType> APRecordTypes=new list<RecordType>();
		APRecordTypes = DaoRecordType.getRecordTypesIn(new Set<String>{'Biller_Contacts_Mapping','Biller_Mapping' ,'Employee_Mapping','Placement_Mapping'});
		//APRecordTypes=[select id, developername from RecordType where developername ='Biller_Contacts_Mapping'
		// 	or developername='Biller_Mapping' or developername='Employee_Mapping' or developername='Placement_Mapping'];
  		map<String,String> recordTypeMap=new map<String,String>();
		for(RecordType recordTypeunit: APRecordTypes){
		 	recordTypeMap.put(recordTypeunit.developername,recordTypeunit.id);
		 	CTCPeopleSettingHelper__c ctcsetting=new CTCPeopleSettingHelper__c();
		 	ctcsetting.recordtypeid=recordTypeunit.id;
		 	ctcsetting.AP_Biller_Fields__c='abn';
		 	ctcsetting.AP_Biller_Contact_Fields__c='email';
		 	ctcsetting.AP_Employee_Fields__c='address_locality';
		 	ctcsetting.name='name';
		 	ctcsetting.CTCPeopleSetting__c=apAccount.id;
		 	fieldsMaps.add(ctcsetting);
		 		
		 }
		 
		 CommonSelector.quickInsert(fieldsMaps);
       
		 resSecond=apAction.AstutePayrollUpload(cmTest.id);
          
    }
    }

}