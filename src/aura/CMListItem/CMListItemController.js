({
	CMSelected : function(component) {

	},

	showDetail : function(component, event, helper){
		//console.log("in the showDetail");
		var closeItem = component.get("v.openItem");
		if(closeItem){
			$A.util.removeClass(closeItem.firstChild, 'selected');
			closeItem = closeItem.querySelector('[data-details]');
			$A.util.addClass(closeItem, 'slds-hide');
		}
		var selectedItem = event.currentTarget;
		component.set("v.openItem", selectedItem); //set open item
		var itemDetails = selectedItem.querySelector('[data-details]'); // find the detail
		$A.util.addClass(selectedItem.firstChild, 'selected');
		$A.util.removeClass(itemDetails, 'slds-hide');

		// send event
		var event = $A.get("e.c:CMSelected");

        event.setParams({
            "CM":component.get("v.CM"),
            "openItem":selectedItem 
        });
        event.fire();
	}


})