({
	doInit : function(component, event, helper) {
        console.log("in the cmlist");
		var action = component.get("c.findAll");
        var recId = component.get("v.recordId");
        var main = component.find('main');
        $A.util.removeClass(main, 'small');
        $A.util.addClass(main, component.get("v.designHeight"));
        action.setParams({
            "recId":recId
        });
        action.setCallback(this,function(response){
            component.set("v.CMs", response.getReturnValue());
        });
        $A.enqueueAction(action);
	},

    updateSearch : function(component, event, helper){
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'slds-hide');
        var recId = component.get("v.recordId");
        helper.getUpdatedCMList(component, recId);
        
    },

    passValue : function(component, event, helper){
        var openItem = event.getParam("openItem");
        component.set("v.openItem", openItem);
    }
})