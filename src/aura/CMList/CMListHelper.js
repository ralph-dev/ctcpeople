({
	getUpdatedCMList : function(component, recId) {
		var searchTerm = component.find("searchTerm").get("v.value");
		var action = component.get("c.searchCM");
		action.setParams({
			"recId":recId,
			"searchTerm":searchTerm
		});
		action.setCallback(this, function(response){
			var data = response.getReturnValue();
			component.set("v.CMs", data);
			var spinner = component.find('spinner')
			$A.util.addClass(spinner, "slds-hide");
		});
		$A.enqueueAction(action);
	}
})