({	
    initMap : function(component,recId){
        var action = component.get("c.getGeoCode");
        var objName = component.get("v.sObjectName");
        var uluru = {lat: -25.363, lng: 131.044};
        action.setParams({
            "recId":recId,
            "objName":objName
        });
        action.setCallback(this, function(response){
            var data = JSON.parse(response.getReturnValue());
            if(data){
                if(data.status ==='OK'){
                    var latlng = data.results[0].geometry.location;
                    component.set("v.vacLatlng", latlng);
                    if(objName=="Placement__c"){
                        var formattedAddress = data.results[0].formatted_address;
                        component.set("v.vacAddress", formattedAddress);
                    }
                    this.loadMap(component,latlng,16);
                }else{
                    this.loadMap(component,uluru,4);
                }
            }else{
                this.loadMap(component,uluru,4);
            }
        });
        $A.enqueueAction(action);
    },
    
    loadMap : function(component, latlng, zoom) {
        // skip to the next iteration if the map already exists
        var container = L.DomUtil.get('map');
        if (!container || container._leaflet_id) {
            //return false;
            container.remove();
        }
        
        var map = L.map('map',{zoomContoller:false});
        map.setView(latlng,zoom);
        
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        
        L.marker(latlng).addTo(map);
        component.set("v.map",map);
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-hide');
    },
    
    goToRec : function(component, cmSel){
        var map = component.get("v.map");
        var recId = component.get("v.recordId");
        console.log(recId);
        var sObjectName = component.get("v.sObjectName");
        if(sObjectName === "Contact"){
            return;
        }
        var marker = component.get("v.marker");
        var polyline = component.get("v.polyline");
        if(marker!==undefined && marker!==null){
            map.removeLayer(marker);
        }
        if(polyline!==undefined && polyline!==null){
            map.removeLayer(polyline);
        }
        var action = component.get("c.getDirections");
        action.setParams({
            "conId":cmSel.Candidate__r.Id,
            //"vacId":recId
            "vacAddress":component.get("v.vacAddress")
        });
        action.setCallback(this, function(a){
            var data = JSON.parse(a.getReturnValue());
            if(data.status=="OK"){
                this.doLayout(component,map,data,cmSel);
            }else{
                var message = "Cannot find the address";
                var type = "error";
                this.showToast(message,type);
                var spinner = component.find('spinner');
                $A.util.addClass(spinner, 'slds-hide');
            }
        });
        $A.enqueueAction(action);
    },

    doLayout : function(component,map,data,cmSel) {
        var result = data.routes[0].legs[0];
        var encoded = data.routes[0].overview_polyline.points;
        var points = L.PolylineUtil.decode(encoded, 5);
        //
        var marker = new L.marker(result.start_location);
        
        component.set("v.marker", marker);
        map.addLayer(marker).on('dblclick', function(){

            var event = $A.get("e.force:navigateToSObject");
            event.setParams({
                "recordId":cmSel.Candidate__r.Id
            });
            event.fire();
        });
        marker.bindPopup("<b>"+cmSel.Candidate__r.Name+"</b><br />Distance : "+result.distance.text
         +"<br/>Duration : "+result.duration.text+
         "<br/>Travel Mode : Driving").openPopup();
        var polyline = L.polyline(points ,{color: 'red'}).addTo(map);
        component.set("v.polyline", polyline);
        map.fitBounds(polyline.getBounds());
        map.panTo(result.start_location);
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-hide');
    },

    showToast : function(message, type){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type":type,
            "title":"Bad request",
            "message":message
        });
        toastEvent.fire();
    }
    
})