({  
	jsLoaded : function(component, event, helper) {
        console.log("jsLoaded");
        var recId = component.get("v.recordId");
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, 'slds-hide');
        helper.initMap(component,recId);
	},
    
    CMSelected : function(component, event, helper){
      var cmSel = event.getParam("CM");
      var spinner = component.find('spinner');
      $A.util.removeClass(spinner, 'slds-hide');
      if(cmSel.Candidate__r.MailingAddress){
        helper.goToRec(component, cmSel);
      }else{
        $A.util.addClass(spinner, 'slds-hide');
        var message = "This candidate does not have address";
        helper.showToast(message, "warning");
      }
      
    },
    
    polyLineLoaded : function(){
    	console.log("polylineloaded");
	}

    
})