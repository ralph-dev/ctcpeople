<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Notification_to_Submitter</fullName>
        <description>Approval Notification to Submitter</description>
        <protected>true</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PeopleCloud_Email_Templates/Timesheet_Approved</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Notification_to_Submitter</fullName>
        <description>Rejection Notification to Submitter</description>
        <protected>true</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PeopleCloud_Email_Templates/Timesheet_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Time_Sheet_Awaiting_Approval</fullName>
        <description>Time Sheet Awaiting Approval</description>
        <protected>false</protected>
        <recipients>
            <field>Approver_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Backup_Approver__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PeopleCloud_Email_Templates/Timesheet_Approval</template>
    </alerts>
    <alerts>
        <fullName>Timesheet_Approved</fullName>
        <description>Timesheet Approved</description>
        <protected>false</protected>
        <recipients>
            <field>Candidate__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PeopleCloud_Email_Templates/Timesheet_Approved</template>
    </alerts>
    <alerts>
        <fullName>Timesheet_Rejected</fullName>
        <description>Timesheet Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Candidate__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PeopleCloud_Email_Templates/Timesheet_Rejection</template>
    </alerts>
    <fieldUpdates>
        <fullName>ApproveTS</fullName>
        <field>TimeSheet_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>ApproveTS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>true</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RejectTS</fullName>
        <field>TimeSheet_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>RejectTS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>true</protected>
    </fieldUpdates>
    <rules>
        <fullName>Timesheet Approval</fullName>
        <actions>
            <name>Time_Sheet_Awaiting_Approval</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>TimeSheet__c.TimeSheet_Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <description>Email sent when a time sheet has been submitted for approval</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Timesheet Approved</fullName>
        <actions>
            <name>Timesheet_Approved</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>TimeSheet__c.TimeSheet_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Email sent to candidate when a time sheet has been approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Timesheet Rejected</fullName>
        <actions>
            <name>Timesheet_Rejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>TimeSheet__c.TimeSheet_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Email sent to candidate when a time sheet has been rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
