<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>LinkedIn_Member_ID_Reset</fullName>
        <description>When Email changes, reset the member ID to match again.</description>
        <field>LinkedIn_Member_Id__c</field>
        <name>LinkedIn Member ID Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Preferred_Email</fullName>
        <field>Email</field>
        <formula>Home_Email__c</formula>
        <name>Update Preferred Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Work_Email</fullName>
        <field>Email</field>
        <formula>Work_Email__c</formula>
        <name>Update Work Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>LinkedIn Member ID Reset</fullName>
        <actions>
            <name>LinkedIn_Member_ID_Reset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When candidate&apos;s email is changed, set LinkedIn Member ID to blank for LinkedIn Profile matching again.</description>
        <formula>ISCHANGED( Email )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Home Email</fullName>
        <actions>
            <name>Update_Preferred_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Preferred_Email__c</field>
            <operation>equals</operation>
            <value>Home</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Work Email</fullName>
        <actions>
            <name>Update_Work_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Preferred_Email__c</field>
            <operation>equals</operation>
            <value>Work</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
