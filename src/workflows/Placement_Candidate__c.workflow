<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Application_Confirmation</fullName>
        <description>Application Confirmation</description>
        <protected>false</protected>
        <recipients>
            <field>Candidate__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PeopleCloud_Email_Templates/Application_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>Not_Suitable_Email_Sent</fullName>
        <description>Not Suitable Email Sent</description>
        <protected>false</protected>
        <recipients>
            <field>Candidate__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PeopleCloud_Email_Templates/Candidate_Not_Suitable</template>
    </alerts>
    <fieldUpdates>
        <fullName>CV_Sent_Date</fullName>
        <field>CV_Sent_Date_Reporting_Only__c</field>
        <formula>Today()</formula>
        <name>CV Sent Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_2nd_Interview_Date</fullName>
        <field>Client_Interview_Date_2nd_Reporting_Only__c</field>
        <formula>Today()</formula>
        <name>Client 2nd Interview Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_Interview_1st_Date</fullName>
        <field>Client_Interview_Date_1st_Reporting_Only__c</field>
        <formula>Today()</formula>
        <name>Client Interview 1st Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inform_AP_Upload_is_ready</fullName>
        <description>To tell Astute Payroll that the upload is ready</description>
        <field>Is_Ready_For_Astute_Payroll__c</field>
        <literalValue>1</literalValue>
        <name>Inform AP upload is ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Internal_Interview_Date</fullName>
        <field>Internal_Interview_Date_Reporting_Only__c</field>
        <formula>Today()</formula>
        <name>Internal Interview Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New</fullName>
        <field>Status__c</field>
        <literalValue>New</literalValue>
        <name>New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offer</fullName>
        <field>Offer_Date_Reporting_Only__c</field>
        <formula>Today()</formula>
        <name>Offer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Placed_Date</fullName>
        <field>Placed_Date__c</field>
        <formula>Today()</formula>
        <name>Placed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Placement__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reference_Check_Date</fullName>
        <field>Reference_Check_Date_Reporting_Only__c</field>
        <formula>Today()</formula>
        <name>Reference Check Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateFlag</fullName>
        <field>Send_Out_Notification__c</field>
        <literalValue>0</literalValue>
        <name>UpdateFlag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Placement_End_Date</fullName>
        <field>End_Date__c</field>
        <formula>Placement__r.End_Date__c</formula>
        <name>Update Placement End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Placement_Start_Date</fullName>
        <field>Start_Date__c</field>
        <formula>Placement__r.Start_Date__c</formula>
        <name>Update Placement Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Vacancy_Placed</fullName>
        <field>Stage__c</field>
        <literalValue>Placed</literalValue>
        <name>Vacancy Placed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Placement__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updatewebsite</fullName>
        <field>From_WebSite__c</field>
        <formula>Candidate_Source1__c</formula>
        <name>updatewebsite</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>1st Interview Date</fullName>
        <actions>
            <name>Client_Interview_1st_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Client_1st_Interview</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Placement_Candidate__c.Status__c</field>
            <operation>equals</operation>
            <value>Client Interview (First)</value>
        </criteriaItems>
        <description>Automatically populates the Client 1st Interview Date and creates a task - for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>2nd Interview Date</fullName>
        <actions>
            <name>Client_2nd_Interview_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Client_2nd_Interview</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Placement_Candidate__c.Status__c</field>
            <operation>equals</operation>
            <value>Client Interview (Second)</value>
        </criteriaItems>
        <description>Automatically populates the Client 2nd Interview Date and creates a task - for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Application Confirmation</fullName>
        <actions>
            <name>Application_Confirmation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>New</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateFlag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Application_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Placement_Candidate__c.Candidate_Source1__c</field>
            <operation>notEqual</operation>
            <value>Candidate Search</value>
        </criteriaItems>
        <description>This workflow will send confirmation email to new candidates who come into system by applying through Job Board or customer&apos;s web site.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CV Sent</fullName>
        <actions>
            <name>CV_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CV_Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Placement_Candidate__c.Status__c</field>
            <operation>equals</operation>
            <value>CV Sent</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Canidates by Websites</fullName>
        <actions>
            <name>updatewebsite</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Placement_Candidate__c.Candidate_Source1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>in peoplecloud package</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>First ready for Astute Payroll</fullName>
        <actions>
            <name>Inform_AP_Upload_is_ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Placement_Candidate__c.Status__c</field>
            <operation>equals</operation>
            <value>Placed</value>
        </criteriaItems>
        <description>Check if the status is placed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Internal Interview</fullName>
        <actions>
            <name>Internal_Interview_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Internal_Interview</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Placement_Candidate__c.Status__c</field>
            <operation>equals</operation>
            <value>Internal Interview</value>
        </criteriaItems>
        <description>Automatically populates the Internal Interview Date and creates a task - for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Not Suitable</fullName>
        <actions>
            <name>Not_Suitable_Email_Sent</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Not_Suitable_EmailSent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Placement_Candidate__c.Status__c</field>
            <operation>equals</operation>
            <value>Not Suitable</value>
        </criteriaItems>
        <description>Email sent automatically  from the candidate management when the status is &apos;Not Suitable&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Offer</fullName>
        <actions>
            <name>Offer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Offer</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Placement_Candidate__c.Status__c</field>
            <operation>equals</operation>
            <value>Offer</value>
        </criteriaItems>
        <description>Automatically populates the Offer Date and creates a task - for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Placed</fullName>
        <actions>
            <name>Placed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Vacancy_Placed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Placement_Candidate__c.Candidate_Status__c</field>
            <operation>equals</operation>
            <value>Placed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Placement Start%2FEnd Date</fullName>
        <actions>
            <name>Update_Placement_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Placement_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Placement__c.Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Placement__c.End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Placement_Candidate__c.Start_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Placement_Candidate__c.End_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Once a candidate management is created or updated, it will have its start &amp; end date updated with the start &amp; end date from vacancy, if they are not populated yet.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reference Check</fullName>
        <actions>
            <name>Reference_Check_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reference_Check</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Placement_Candidate__c.Status__c</field>
            <operation>equals</operation>
            <value>Reference Check</value>
        </criteriaItems>
        <description>Automatically populates the Reference Check  Date and creates a task - for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Upload Related Objects for Astute Payroll</fullName>
        <actions>
            <name>Inform_AP_Upload_is_ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>(ISCHANGED(Approver__c  )   || ISCHANGED( Second_Approver__c ) ||ISCHANGED( Work_place__c )) &amp;&amp;  ISPICKVAL(Status__c , &apos;Placed&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Application_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Application Confirmation Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>CV_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>CV Sent</subject>
    </tasks>
    <tasks>
        <fullName>Client_1st_Interview</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Client 1st Interview</subject>
    </tasks>
    <tasks>
        <fullName>Client_2nd_Interview</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Client 2nd Interview</subject>
    </tasks>
    <tasks>
        <fullName>Internal_Interview</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Internal Interview</subject>
    </tasks>
    <tasks>
        <fullName>Not_Suitable_EmailSent</fullName>
        <assignedToType>owner</assignedToType>
        <description>Not Suitable Email Sent to candidate</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Not Suitable Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Offer</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Offer</subject>
    </tasks>
    <tasks>
        <fullName>Reference_Check</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Reference Check</subject>
    </tasks>
</Workflow>
