/**
* Author: Raymond
* 
* Prevent users from deleting or changing "candidate pool" account record by mistake
*/
trigger PreventCandPoolFromChange on Account (before delete, before insert,before update,after update) {
    if(trigger.isBefore){
        if(trigger.isUpdate){
            ABNValidator.validateAccABN(trigger.new);
        }

        if(trigger.isInsert){
            ABNValidator.validateAccABN(trigger.new);
        }
    }

    if(Trigger.isBefore && Trigger.isUpdate){
        //making push step ready.
         AccountAstutePayrollImplementation.astutePayrollTrigger(trigger.new);
    }
    if(PCAppSwitch__c.getInstance()==null || !PCAppSwitch__c.getInstance().Protect_Candidate_Pool__c)
        return;
    // Get candidate Pool record type
    RecordType candidatePoolRT = null;  
    if(!Trigger.isInsert)
    { 
        try{
            candidatePoolRT = DaoRecordType.candidatePoolRT;
        }catch(Exception e){
            NotificationClass.notifyErr2Dev('Cannot retrieve Candidate Pool record type from db', e);
            return;
        }   
    }
    if(Trigger.isBefore && Trigger.isUpdate){
        for(Account acc:Trigger.new){
            if(acc.RecordTypeId == candidatePoolRT.Id)
                acc.addError('You cannot update this account because it\'s marked as a People Cloud candidate pool');
        }
    }else if(Trigger.isBefore && Trigger.isDelete){
        for(Account acc:Trigger.old){
            if(acc.RecordTypeId == candidatePoolRT.Id)
                acc.addError('You cannot delete this account because it\'s marked as a People Cloud candidate pool');
        }
    }
    
}