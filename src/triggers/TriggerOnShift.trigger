trigger TriggerOnShift on Shift__c (before delete, after delete, after update) {
	String pid = UserInfo.getProfileId();
    PCAppSwitch__c  PCAppSwitch = PCAppSwitch__c.getinstance(pid);
    
    if(PCAppSwitch.Delete_Roster_After_Delete_Shift_Trigger__c){
    	TriggerFactory.createAndExecuteHandler(Shift__c.sObjectType);
    }
}