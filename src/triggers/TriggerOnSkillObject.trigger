/*
	Author: Jack
	
	This trigger is Combine different action under the candidate skill object. 
	
	CandiateSkillChangeUpdateCandidate trigger is to update skill(s) and update related Candidate last modified date
    
    Update Candidate Last Modified date: When candidate skill record insert, update or delete, update the candidate last modified date.
    
    DeduplicationOnContactSkill trigger is to de-duplicate the duplicate Skill
    when insert or update skill(s)
    
    Insert Skill: Duplicate skill(s) will be deleted
    Update Skill: Update skill in PeopleCloud system, new duplicate skill will be deleted.  
      
*/

trigger TriggerOnSkillObject on Candidate_Skill__c (after delete, after insert, after update) {
	Boolean SkillDeDuplicate = true;
	Boolean CandiateSkillChangeUpdateCandidate = true;
	PCAppSwitch__c CS = PCAppSwitch__c.getInstance();
    if(CS == null || CS.Enable_Skill_De_Duplicate__c == false){//Get the customer setting value
        SkillDeDuplicate = false;
    }
    if(Trigger.isDelete){
		Boolean updateCandidate = DeduplicationSkills.updateCandidateLastModifyDate(Trigger.old);//update candidate last modified date
	}
	else{
		if(SkillDeDuplicate){
			List<String> duplicateCSCIdList = new List<String>();               //store duplicate Candidate skill list  
    
		    duplicateCSCIdList = DeduplicationSkills.getduplicateCSCIdList(Trigger.new);
		    
		    //If insert trigger, delete the record directly
		    if(duplicateCSCIdList.size()>0){
		        if(Trigger.isInsert){   
		            //system.debug('duplicateCSCIdList =' + duplicateCSCIdList);
		            DeduplicationSkills.deleteDuplicateCandidateSkill(duplicateCSCIdList);
		        }
		            
		        if(Trigger.isUpdate){
		            DeduplicationSkills.deleteDuplicateCandidateSkill(duplicateCSCIdList);
		        }
		    }   
		}
		Boolean updateCandidate = DeduplicationSkills.updateCandidateLastModifyDate(Trigger.new);//update candidate last modified date
	}
}