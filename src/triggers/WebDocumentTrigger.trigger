/**
 * This trigger is created for deleting physical resume from Amazon S3 when a webdoc record is deleted from SF.
 * 
 * After insert, update or delete
 * This trigger also send relevant SQS request to Daxtra for keywords search function
  
  When this trigger is initially created, there is hardly any comment. Therefore not sure why this trigger was
  initially designed to execute after a web doc record is created.
**/
trigger WebDocumentTrigger on Web_Document__c (after insert, after update, after delete) {
    if(PCAppSwitch__c.getInstance()==null)
        return;

    if(!TriggerHelper.DisableTriggerOnWebDocument) {
        TriggerFactory.createAndExecuteHandler(Web_Document__c.sObjectType); 
    }
    
    if (PCAppSwitch__c.getInstance().WebDocumentTrigger_Active__c) {
        if(Trigger.isInsert && Trigger.isAfter){
            WebDocumentTriggerHandler.afterInsertWebDoc(Trigger.new);  
        }else if(Trigger.isDelete && Trigger.isAfter){
            WebDocumentTriggerHandler.afterDelWebDoc(Trigger.old); 
        }else{
            //do nothing
        }
    }
}