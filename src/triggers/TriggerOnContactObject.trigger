/**
    Author by Jack,
    This trigger is used to combine the trigger under Contact Object.
    PeopleDeduplication trigger is to merge new candidates to existing candidates if a match is found
    UpdateAccountField trigger is to update account value under Contact record.
*/

trigger TriggerOnContactObject on Contact (after insert,after delete,before insert,before delete, before update, after update  ) {
    
    Boolean BooleanPeopleDeduplication = true;
    Boolean UpdateAccountField = true;
    Boolean BooleanCreateSampleResumeforContact = true;
    Boolean SendSQSRequest = true;
    
    String triggerName = 'UpdateAccountField';
    String aId ;
    String recordtypeId ; 
    Set<ID> masterrecordids=new Set<ID>();
    
    if(TriggerHelper.DisableTriggerOnContact) {
        return;
    }
    
    if(PCAppSwitch__c.getInstance()==null || !PCAppSwitch__c.getInstance().Enable_Create_Sample_Resume_for_Contact__c){
        BooleanCreateSampleResumeforContact = false;
    }  
    
    if(PCAppSwitch__c.getInstance()==null || !PCAppSwitch__c.getInstance().PeopleDeduplication_Active__c){
        BooleanPeopleDeduplication = false;
    }
    
    if(PCAppSwitch__c.getInstance()==null || PCAppSwitch__c.getInstance().Disable_Daxtra_SQS_Request__c) {
        SendSQSRequest = false;
    }

   
  	if(Trigger.isBefore){
  		if(Trigger.isInsert){
	  		if(BooleanPeopleDeduplication){
	            if(Trigger.new.size()==1){
	                PeopleDeduplication.PeopleDeduplication(Trigger.new);
	            }
	        }
  		}
  		if(Trigger.isDelete){
  			//Mark the docs which is going to be deleted.
  			 ContactTriggerHandler.afterUpdateDocs(trigger.old);
  			 
  			 CommonSelector.checkRead(Web_Document__c.sObjectType, 'Id');
           	 ContactTriggerHandler.WebdocumentList = [select Id from Web_Document__c where Document_Related_To__c in: trigger.old];
  		}
  		 //Astute Payroll Implementation
	    if(Trigger.isUpdate){
	    //making push step ready.
	        ContactAstutePayrollImplementation.astutePayrollTrigger(trigger.new);
	    }
  	}else{
  		if(Trigger.isDelete){
            for(Contact con: trigger.old){
                if(con.MasterRecordId!=NULL){
                    masterrecordids.add(con.MasterRecordId);
                }
            }
            //Remove the marks from those docs which is merged to other contact     
            if(masterrecordids.size()>0){
                
                ContactTriggerHandler.UndeleteDocs(masterrecordids); 
            }
            //Delete marked docs.
            ContactTriggerHandler.afterDelContact(ContactTriggerHandler.WebdocumentList);
            if (SendSQSRequest && !TriggerHelper.DisableTriggerOnWebDocument) {
                ContactTriggerHandler.deleteDaxtraSearch(trigger.old);
            }
            
        }
        if(Trigger.isInsert){
		    	TriggerDetail thetriggerdetail = new TriggerDetail();
	        	thetriggerdetail.getTriggerDetails();
	        
		        if(!thetriggerdetail.IsActive(triggerName) || !TriggerHelper.UpdateAccountField){
		            UpdateAccountField = false;
		        }
		    	if(UpdateAccountField){
			    	Id independentCandRT = (Id)thetriggerdetail.getRecordTypeId(triggerName);   
			        CandidateDispatcher cd = new CandidateDispatcher(independentCandRT);
			        List<Id> conIds = new List<Id>();
			        conIds.addAll(Trigger.newMap.keySet());
			        List<Contact> newContacts = DaoContact.getContactsByIds(conIds);
			            
			        cd.dispatchCand(newContacts);
		    }
        }
  		if(Trigger.isInsert|| Trigger.isUpdate){
  			 if(BooleanCreateSampleResumeforContact && ContactTriggerHandler.isDocumentTriggerFired) {
  			 	ContactTriggerHandler.isDocumentTriggerFired=false;
		        ContactDocumentController cdc = new ContactDocumentController();
		        cdc.createContactDocumentController(trigger.new);
		    }
  		}
  	}
}