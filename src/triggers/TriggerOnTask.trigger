trigger TriggerOnTask on Task (after insert, after update, after delete) {
	if(PCAppSwitch__c.getInstance()==null)
        return;

    if(!TriggerHelper.DisableTriggerOnTask && PCAppSwitch__c.getInstance().Enable_Last_Contact_Date__c){
    	TriggerFactory.createAndExecuteHandler(Task.SObjectType);
    }
	
}