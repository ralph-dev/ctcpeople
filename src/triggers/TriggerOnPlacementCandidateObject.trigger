trigger TriggerOnPlacementCandidateObject on Placement_Candidate__c (after insert, after update, before insert, 
before update) {
	if(PCAppSwitch__c.getInstance()==null)
		return;
		
	Boolean isTriggerFire = TriggerHelper.PlacementCandidateEventOnInterviewTrigger;//PlacementCandUpdateEventOnInterviewTrigger
	Boolean UpdateRemining = TriggerHelper.UpdateReminingEnable;//UpdateRemining trigger
	
	//De-deuplicate trigger
	Boolean isDedeuplicateTriggerFire=PCAppSwitch__c.getInstance().DeduplicationOnVCW_Active__c && TriggerHelper.Deduplication;
	Boolean isEnableCMDedup = PCAppSwitch__c.getInstance().Deduplicate_PC_Active__c;
	
	//PlacedCandidate Trigger
	Boolean disableplacedcandidatetrigger = PCAppSwitch__c.getInstance().Placed_Candidate_Status__c;
	
	//AutoCreateUnavailable
	Boolean isunavailabledaystrigger = PCAppSwitch__c.getInstance().Unavailable_Days_Auto_Creation__c;
	
	if(Trigger.isBefore){//Trigger is before
	    //PopulateCompOnVCW trigger
	    try{
			PopulateCompanyOnVCW.PopulateCompanyOnVCW(Trigger.new);
		}
		catch(Exception e){
			NotificationClass.notifyErr2Dev('PopulateCompOnVCW', e);
		}
        //For Astute Payroll Employee Update
            if(Trigger.isUpdate){
                PlacementAstutePayrollImplementation.astutePayrollTrigger(trigger.new);
            }
	}else{//Trigger is after
		if(Trigger.isUpdate){
			if(isTriggerFire){//trigger.PlacementCandUpdateEventOnInterviewTrigger.Start
				List<Placement_Candidate__c> ids = Trigger.new;    
	   			CreatEventOnInterviewTrigger.CreateUpdateEventOnInterviewTrigger(ids);
			}if(!disableplacedcandidatetrigger){//Placed Canidate trigger
				try{
			   		CandidatePlaced.CandidatePlaced(Trigger.new);
			     }
			     catch(system.Exception e){
			         NotificationClass.notifyErr2Dev('CandidatePlaced', e);
			     } 
			}if(isunavailabledaystrigger){//AutoCreateUnavailableDays trigger
		    	try{
					AutoCreateUnavailableDays.AutoCreateUnavailableDays(Trigger.newMap.keySet(), Trigger.old);
				}catch(Exception e){
					NotificationClass.notifyErr2Dev('AutoCreateUnavailableDays', e);
				}
		    }if(UpdateRemining){//update remining trigger
		    	UpdateReminingTrigger.UpdateRemingTrigger(Trigger.new, Trigger.old, true);
		    }
		}
		else if(Trigger.isInsert){
			if(isTriggerFire){//trigger.PlacementCandInsertEventOnInterviewTrigger
				List<Placement_Candidate__c> ids = Trigger.new;
    			CreatEventOnInterviewTrigger.CreateInsertEventOnInterviewTrigger(Trigger.new); 
			}			
			if(!disableplacedcandidatetrigger){//Placed Canidate trigger
				try{
			   		CandidatePlaced.CandidatePlaced(Trigger.new);
			     }
			     catch(system.Exception e){
			         NotificationClass.notifyErr2Dev('CandidatePlaced', e);
			     } 
			}
			if(isunavailabledaystrigger){//AutoCreateUnavailableDays trigger
		    	try{
					AutoCreateUnavailableDays.AutoCreateUnavailableDays(Trigger.newMap.keySet(), Trigger.old);
				}catch(Exception e){
					NotificationClass.notifyErr2Dev('AutoCreateUnavailableDays', e);
				}
		    }
		    if(UpdateRemining){//update remining trigger
		    	UpdateReminingTrigger.UpdateRemingTrigger(Trigger.new, Trigger.old, false);
		    }
			if(isDedeuplicateTriggerFire){//Deduplicate trigger work after insert
				try{
			    	List<Id> incomingCMIds = new List<Id>();
			    	for(Placement_Candidate__c cm:trigger.new){
			    		incomingCMIds.add(cm.Id);
			    	}
			    	
			    	CommonSelector.checkRead(Placement_Candidate__c.sObjectType, 'Id,Candidate_Source__c,Candidate__c, Placement__c,Job_Application_Id__c, Job_Application_Ext_Id__c');
			    	List<Placement_Candidate__c> incomingApplications = [select Id,Candidate_Source__c,Candidate__c, Placement__c,Job_Application_Id__c, Job_Application_Ext_Id__c from Placement_Candidate__c where Id in:incomingCMIds];
			    	Deduplication.enhancedDedupApplications(incomingApplications,Deduplication.getExcludedRecordTypeIds(),isEnableCMDedup);
			    }catch (system.Exception e){
			    	NotificationClass.notifyErr2Dev('DeduplicationOnVCW encounter problem.', e);
			    }
				
			}
		}
		
	}
}