trigger TriggerOnWorkplace on Workplace__c ( before update) {
    if(Trigger.isBefore && Trigger.isUpdate){
		//making push step ready.
        WorkplaceAstutePayrollImplementation.astutePayrollTrigger(trigger.new);
   }
}