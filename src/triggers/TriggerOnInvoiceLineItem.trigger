trigger TriggerOnInvoiceLineItem on Invoice_Line_Item__c (before insert, before update) {
	list<Invoice_Line_Item__c> invoiceLineItems=trigger.new;			
	if(Trigger.isBefore){
		//making push step ready.
		list<String> contactIds=new list<String>();
		
		for(Invoice_Line_Item__c invoiceLineItem: invoiceLineItems){
			if(invoiceLineItem.account__c==null){
				invoiceLineItem.account__c=invoiceLineItem.Account_ID__c;
			}
			if(invoiceLineItem.Billing_Contact__c==null){
				invoiceLineItem.Billing_Contact__c=invoiceLineItem.Billing_Contact_ID__c;
			}
			if(invoiceLineItem.Billing_Contact__c!=null){
				contactIds.add(invoiceLineItem.Billing_Contact__c);
			}
			
			
		}
		if(contactIds.size()>0){
			list<Contact> conts=new list<Contact>();
			CommonSelector.checkRead(Contact.sObjectType, 'id, accountId');
			conts=[select id, accountId from Contact where id in : contactIds];
			map<String,Contact> contMap=new map<String,Contact>();
			for(Contact cond: conts){
				contMap.put(cond.Id, cond);
			}
			for(Invoice_Line_Item__c invoiceLineItem: invoiceLineItems){
				if(invoiceLineItem.Billing_Contact__c!=null){
					if(contMap.get(invoiceLineItem.Billing_Contact__c).AccountId!=invoiceLineItem.Account__c){
						invoiceLineItem.Billing_Contact__c.addError('The Account in the billing contact must be exactly the same as Account listed in the invoice line item');
					}
				}
			}
		}
		
		
        InvoiceItemAstutePayrollImplementation.astutePayrollTrigger(invoiceLineItems);
    }
    
    

}