/**
This trigger is to update timesheet approver field in 
TimeSheet. Only those with pending status will be updated.

This trigger is planned to work only when single vacancy
is updating. Mass update will not have this trigger properly
or be ignored.
**/

trigger SyncTimeSheetApprover on Placement__c (before update) {

    List<Placement_Candidate__c> vcwList = new List<Placement_Candidate__c>();
    Map<String,Placement__c> vMap = new Map<String,Placement__c>();
    Set<String> idSet = new Set<String>();
    
    if(Trigger.new.size()==0 || !TriggerHelper.SyncTimeSheetApproverEnable){
            system.debug('enable:'+TriggerHelper.SyncTimeSheetApproverEnable);
            return;
    }
    for(Integer i=0;(i<Trigger.new.size() && i<=10);i++){
        Placement__c v = Trigger.new.get(i);
        vMap.put(v.Id,v);
        idSet.add(v.Id);
    }
        
/*  for(String id:idSet){
        system.debug('id:'+ id);
    }*/
//  system.debug('trigger reach here2');
    if(!TriggerHelper.DisableTriggeronPlacement){
        List<TimeSheet__c> tsList= DaoTimeSheet.getPendingTSByVIds(idSet);
        List<TimeSheet__c> updateTSList = new List<TimeSheet__c>(); 
        for(TimeSheet__c ts : tsList){
            Placement__c v = vMap.get(ts.Vacancy__c);
            ts.Approver__c = v.Internal_Approver__c;
            updateTSList.add(ts);
        }
        CommonSelector.quickUpdate( updateTSList);
    }
/*  for(TimeSheet__c ts:DaoTimeSheet.getPendingTSByVIds(idSet)){
        system.debug('approver:'+ts.Approver__c);
    }*/
    
}