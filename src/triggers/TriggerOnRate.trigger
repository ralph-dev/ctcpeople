trigger TriggerOnRate on Rate__c (after insert,after update,before delete) {
    list<Rate__c> rates=trigger.new;
    
    
    RateAstutePayrollImplementation rateImpl=new RateAstutePayrollImplementation();
    if(Trigger.isAfter){
    	rateImpl.addRates(trigger.new);
    }else{
        rateImpl.deleteRates(trigger.old);
    }
    
	
}