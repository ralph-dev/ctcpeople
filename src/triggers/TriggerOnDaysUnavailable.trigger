trigger TriggerOnDaysUnavailable on Days_Unavailable__c (before update,after delete, after insert, after update, after undelete) {
  
  TriggerFactory.createAndExecuteHandler(Days_Unavailable__c.sObjectType);
}