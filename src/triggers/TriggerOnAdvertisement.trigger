/**
 * This is the trigger on Advertisement__c Object
 * Updated by: Lina Wei
 * Updated on: 28/03/2017
 */
trigger TriggerOnAdvertisement on Advertisement__c (before update) {
    if (!TriggerHelper.DisableTriggerOnAdvertisement) {
        TriggerFactory.createAndExecuteHandler(Advertisement__c.sObjectType);
    }
}